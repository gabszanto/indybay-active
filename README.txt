This is unsupported pre-alpha software.  Use at your own risk.


GitLab repo: (current Sept 2015)
- https://gitlab.com/indybay/indybay-active/

Subversion repository: (OLD)
- https://dev.indybay.org/svn/indybay/

Bugzilla:
- https://dev.indybay.org/bugzilla/

WebSVN view:
- http://dev.indybay.org/websvn/listing.php?repname=indybay


-----------------------------------------------------------------

ADDITIONAL INFO FOR INSTALLATION:

-- add the following to httpd.conf or vhosts file [for include_once calls to /classes/ subdirectories]:

	php_value include_path '.:/home/indybay/indybay/indybay-active/classes'

-- make directories writable: cache, uploads, website/newsitems (and more? see new subdirectories notes below)

-- to generate newswires, click "Edit Page Definition" links on right side of "Feature Pages" admin to generate single feature pages, or click "Regenerate All Newswires" at bottom of "Feature Pages" admin to generate all newswires at once.

-- the following subdirectories must exist.  if, for some reason, they do not:
--- create /newswires/ within /cache/ and make all writable
--- create /session/ in root and make all writable
--- create /recent_blurbs/ within /cache/feature_page/ and make all writable
--- create /HTMLPurifier/ within /cache/ and make all writable

--> create symlink for non-existent repo/website/uploads to repo/uploads/
