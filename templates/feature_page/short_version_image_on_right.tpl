<div class="storyshort">
<div class="hed"><a class="headline-text" href="TPL_LOCAL_SINGLE_ITEM_VIEW_LINK" 
name="TPL_LOCAL_NEWS_ITEM_ID">
<strong>TPL_LOCAL_TITLE1</strong></a></div>
<div class="feature-blurb-background">

<div class="feature-blurb-subhead"><span class="feature-blurb-date">
TPL_LOCAL_FORMATTED_DATE
</span>
<a title="Permanent link to this story" class="feature-blurb" rel="bookmark"
href="TPL_LOCAL_SINGLE_ITEM_VIEW_LINK">TPL_LOCAL_TITLE2</a></div>

</div>
<div class="blurb">
<div class="shortstoryimage">
<a class="headline-image" title="TPL_LOCAL_ALT_TAG" href="TPL_LOCAL_SINGLE_ITEM_VIEW_LINK">
<img src="TPL_LOCAL_IMAGE_URL"
  align="right" border="0" hspace="3" vspace="3" alt="TPL_LOCAL_ALT_TAG" /></a>
  </div>
TPL_LOCAL_SUMMARY

TPL_LOCAL_READ_MORE_LINKS
</div>
</div>
<!-- template was short_version_image_on_right -->