<?php
//this page is the template for all category index.php
//It displays a cached center column and a cached summary list
//of the latest posts.

include_once("config/indybay.cfg");

$sftr = new Translate();

$GLOBALS['page_title']='TPL_TITLE';
$bottom = "cities";
$left   = INCLUDE_PATH . "/left.inc";
$feature_name="TPL_FEATURE_NAME";
$GLOBALS["category_id"]="TPL_CATID";
$category_id="TPL_CATID";

include_file (INCLUDE_PATH, 'content-header.inc');
include(INCLUDE_PATH. '/index_center.inc');

?> 

<?php
include(INCLUDE_PATH.'/featurepage_event_list.inc');
include(INCLUDE_PATH.'/center_right.inc'); 
include(INCLUDE_PATH.'/footer.inc');

?>
