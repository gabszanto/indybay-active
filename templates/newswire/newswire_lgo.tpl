<?php
require_once(CLASS_PATH."/cache/newswire_cache_class.inc");
$newswire_cache_class= new NewswireCache();
?>
<div class="newswirehead">
<a class="publink" name="local" 
href="/search/?news_item_status_restriction=3&amp;include_events=1&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID"><span 
class="newswirehead">Local News</span></a></div>
<div class="localglobal"><a class="localgloballinks" 
href="#global">Global</a> | <a 
class="localgloballinks" href="#breaking">Breaking</a></div> 
<?php
echo $newswire_cache_class->load_local_newswire_for_page(TPL_LOCAL_PAGE_ID);
?>
<small><a class="publink" href="/search/?news_item_status_restriction=3&amp;include_events=1&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID" >More Local News...</a></small>

<div class="newswirehead" style="margin-top: 6px"><a class="publink" 
href="/search/?news_item_status_restriction=5&amp;include_events=0&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID" name="global"><span class="newswirehead">Global News</span></a>
</div>
<div class="localglobal">
<a class="localgloballinks" href="#local">Local</a> | 
<a class="localgloballinks" href="#breaking">Breaking</a>
</div>
<?php
echo $newswire_cache_class->load_nonlocal_newswire_for_page(TPL_LOCAL_PAGE_ID);
?>
<small><a class="publink" href="/search/?news_item_status_restriction=5&amp;include_events=0&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID">More Global News...</a></small>
<div class="newswirehead" style="margin-top: 6px"><a class="publink" 
href="/search/?news_item_status_restriction=<?php
echo NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;
			
?>&amp;include_events=1&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID" name="breaking"><span 
class="newswirehead">Other/Breaking News</span></a></div>
<div class="localglobal">
<a class="localgloballinks" href="#local">Local</a> |
<a class="localgloballinks" href="#global">Global</a>
</div>
<?php
echo $newswire_cache_class->load_other_newswire_for_page(TPL_LOCAL_PAGE_ID);
?>
<small><a class="publink" href="/search/?news_item_status_restriction=<?php
echo NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;
			
?>&amp;include_events=1&amp;include_posts=1&amp;page_id=TPL_LOCAL_PAGE_ID">Open Newswire...</a></small>
