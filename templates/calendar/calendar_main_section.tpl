TPL_LOCAL_HIDE_LINK_TO_WEEK1
<strong>
<a href="/calendar/?year=TPL_LOCAL_DISPLAYED_YEAR&month=TPL_LOCAL_DISPLAYED_MONTH&day=TPL_LOCAL_DISPLAYED_DAY">View other events for the week of TPL_LOCAL_DISPLAYED_MONTH/TPL_LOCAL_DISPLAYED_DAY/TPL_LOCAL_DISPLAYED_YEAR</a>
</strong>

<br /><br />
TPL_LOCAL_HIDE_LINK_TO_WEEK2
<table border="0" class="bgult" cellspacing="1" cellpadding="4" width="500">
	<tr>
		<td class="bgaccent">
			<b>Title:</b>

		</td>
		<td class="bgsearchgrey">
			TPL_LOCAL_TITLE1
		</td>
	</tr>
	<tr>
		<td class="bgaccent">
			<b>START DATE:</b>
		</td>

		<td class="bgsearchgrey">
			TPL_LOCAL_FORMATTED_DISPLAYED_DATE
		</td>
	</tr>
	<tr>
		<td class="bgaccent">
			<b>TIME:</b>
		</td>
		<td class="bgsearchgrey">

			TPL_LOCAL_FORMATTED_DISPLAYED_START_TIME
			-
			TPL_LOCAL_FORMATTED_DISPLAYED_END_TIME
		</td>
	</tr>

	<tr>

		<td class="bgaccent" colspan="2">
			<b>Location Details:</b>
		</td>
	</tr>
	<tr>
		<td class="bgsearchgrey" colspan="2">
			TPL_LOCAL_SUMMARY
		</td>

	</tr>

	<tr>
		<td class="bgaccent">
			<b>Event Type:</b>
		</td>
		<td class="bgsearchgrey">
			TPL_LOCAL_KEYWORD_NAME
		</td>
	</tr>
	TPL_LOCAL_CONTACT_INFO_ROWS
	<tr>
		<td class="bgsearchgrey" colspan="2">
			TPL_LOCAL_TEXT
			<br />
			<center><div class="media">TPL_LOCAL_MEDIA</div></center><br />
			<br />
			<div class="link"><a href="TPL_LOCAL_RELATED_URL">TPL_LOCAL_SHORTENED_RELATED_LINK</a></div>
			<br />
			<small>Added to the calendar on <em>TPL_LOCAL_CREATED</em></small>
		</td>

	</tr>
</table>
TPL_LOCAL_HIDE_LINK_TO_WEEK1
<br />
<img src="/images/feed-icon-16x16.png" alt="iCal" align="top" />
<a title="you must have a program on your computer that supports iCal for this to work"
href="/calendar/ical_single_item.php?news_item_id=TPL_LOCAL_NEWS_ITEM_ID">Import this event into your personal calendar.</a>
<br />
TPL_LOCAL_HIDE_LINK_TO_WEEK2

<!-- /TEMPLATE -->
