<table width="90%"><tr><td>
<div align="left"><img src="/im/bittorrent.white.png" 
width="16" height="16" /><big><b> indybay 
torrents </b></big><small><br /><!--
Seed our torrents! We will try to keep many
 of the files uploaded to Indybay seeded, but you're 
encouraged to help out by making use of our <a href="/syn/torrents.rss">Torrent RSS feed</a>.
-->
If you upload a large file, you can help seed it by
downloading the associated torrent, opening it in your <a 
href="http://en.wikipedia.org/wiki/BitTorrent">BitTorrent</a> client, and selecting the existing media file on 
your hard drive as the download location. 
 At this point you 

will connect to a "tracker" and 
help seed the 
file for as long as you remain connected. 
 <a href="http://azureus.sourceforge.net/">Some clients</a> 
allow you to easily seed a large number of 
files. <b><a 
href="http://docs.indymedia.org/view/Global/BitTorrent">
What the heck is BitTorrent?</a></b></small>
<!--
<a href="/syn/torrents.rss"><img 
src="http://www.indybay.org/images/iconRSS.gif" border="0" /></a>
-->
</div>
</tr></tr></table>




<form style="margin-top: 0; margin-bottom: 0.5em" method="get" action="index.php">
TPL_TOP_SEARCH_FORM

TPL_NAV
</form>

<table class="torrent" width="80%">
TPL_TABLE_MIDDLE
</table>


<form style="margin-top: 0; margin-bottom: 0.5em" method="get" action="index.php">
TPL_NAV
TPL_BOTTOM_SEARCH_FORM
</form>

