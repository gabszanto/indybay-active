<!-- publish form part 1 -->
TPL_HIDE1
TPL_LOCAL_DISPLAY_PREVIEW
TPL_HIDE2

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" action="/calendar/event_add.php" accept-charset="UTF-8">

TPL_VALIDATION_MESSAGES
TPL_STATUS_MESSAGES
TPL_HIDE1
<table width="600" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr><td class="bgaccent">

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
<tr><td>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr class="bgpenult" valign="top">

<td width="150">
<strong>Event Title:</strong><br /><small>(required)</small>
</td>

<td width="75%">
<input type="text" name="title1" style="width: 90%" maxlength="90" value="TPL_LOCAL_TITLE1" />
</td>
</tr>
<tr valign="top" class="bgpenult"> 

<td >
<strong>Event Date:</strong> <small>(required)</small>
</td>

<td width="75%">
TPL_CAL_SELECT_DATE_DISPLAYED_DATE
</td>

</tr>

<tr valign="top" class="bgpenult"> 

<td >
<strong>Event Duration:</strong> <small>(required)</small>
</td>

<td width="75%">
TPL_LOCAL_SELECT_DURATION (hours:minutes)
</td>

</tr>
<tr><td colspan=2><hr /></td></tr>



<tr><td >Event Location:</td><td>TPL_CAT_REGION_SELECT<td></td></tr>
<tr><td >Event Topic:</td><td>TPL_CAT_TOPIC_SELECT<td></td></tr>

<tr><td >Event Type:</td><td>TPL_CAT_TYPE_SELECT<td></td></tr>

<tr>

<td >
<strong>Location Details</strong> <small>(TPL_REQUIRED)</small>
</td>

<td width="75%">
<textarea name="summary" rows="3" style="width: 90%" wrap="virtual">TPL_LOCAL_SUMMARY</textarea>
<br />
</td>

</tr>



<tr><td colspan=2><hr /></td></tr>
<tr valign="top" class="bgpenult"> 

<td >
<strong>Contact Name:</strong>
</td>

<td width="75%">
<input type="text" name="displayed_author_name" size="25" maxlength="45" value="TPL_LOCAL_DISPLAYED_AUTHOR_NAME" />
</td>

</tr>
<tr>
<td colspan=2>
<strong>Additional Contact Info:</strong>
<table>
<tr>
<td>TPL_CONTACT_EMAIL</td>
<td colspan=3><input type="text" name="email" size="25" maxlength="45" value="TPL_LOCAL_EMAIL" />
</td></tr>
<tr>
<td>
TPL_CONTACT_PHONE
</td>
<td colspan=3>
 <input type="text" name="phone" size="25" maxlength="45" value="TPL_LOCAL_PHONE" />
</td>
</tr>
<tr>
<td width=150>TPL_CONTACT_ADDRESS</td>
<td colspan=3>
<input type="text" name="address" size="45" maxlength="45" value="TPL_LOCAL_ADDRESS" /></td>
</tr>
<td colspan=4>
<strong>TPL_DISPLAY_ADDITIONAL_CONTACT_INFO:</strong>
TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO
</td>
<td >
&nbsp;
</td>
</tr><tr><td colspan=3>
<small>
TPL_DISPLAY_ADDITIONAL_ADDITIONAL_TEXT
</small>
</td>
</tr>
</table>
</td>
</tr>




<td  valign="top">
<strong>TPL_EVENT_DESCRIPTION</strong>
</td>

<td width="75%">
<textarea name="text" rows="10" style="width: 90%" wrap="virtual">TPL_LOCAL_TEXT</textarea>
<br />
</td>

</tr>


<tr><td>&nbsp;</td><td><strong>TPL_IS_EVENT_DESCRIPTION_HTML</strong> TPL_LOCAL_CHECKBOX_IS_TEXT_HTML
</td></tr>
<tr class="bgpenult">

<td >TPL_EVENT_LINK_TO_MORE_DETAILS</td>

<td width="75%">
<input type="text" name="related_url" style="width: 90%" maxlength="255" value="TPL_LOCAL_RELATED_URL"/>
</td>

</tr>
</table>

</td></tr></table>

</td></tr></table>

<!-- publish form part 3 -->

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr><td class="bgaccent">
<strong>&nbsp;Upload Attachments</strong>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr class="bgpenult"><td>

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
<tr><td>

<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr class="bgpenult" valign="top">

<td width="25%">
<strong>How many files to upload?</strong>
</td>

<td width="75%">

<div id="upload_warning">(A Title, Author and Location Must Be Entered Before You Can Choose Uploads)<br /></div>
TPL_SELECT_FILECOUNT

<input type="submit" name="action" value="Enter" id="upload_submit_button">



<br />
Maximum file upload size TPL_UPLOAD_MAX_FILESIZE; total size limit of all files TPL_POST_MAX_SIZE; TPL_MAX_EXECUTION_TIME hours maximum upload time.
<br />Accepted File Types: 3GP, AVI, FLV, GIF, JPG, M3U, M4A, M4V, MOV, MP3, MP4, MPG, OGG, PDF, PLS, PNG, RAM, RM, SWF, TORRENT, WAV, WMA, WMV
</td>

</tr></table>
  <div id="file_boxes2"></div>
</td></tr></table>
</td></tr></table>

<div id="nonjscript_file_boxes">
TPL_FILE_BOXES
</div>
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;CAPTCHA</strong><table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
      <tr class="bgpenult">
        <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
              <tr class="bgpenult" valign="top">
                <td width="25%"><strong>Anti-spam questions</strong> (Required)</td>
                <td width="75%">TPL_CAPTCHA_FORM</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<p align="center">
<span id="preview_button">TPL_LOCAL_PREVIEW</span><input type="submit" name="publish" value="Publish" /></p>
TPL_HIDE2
</form>


<div id="files_select_template" style="visibility: hidden;">
<HR />
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td valign="top" width="25%"><b>Title #::uploadnum::</b> <small></small></td><td><input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" value="" /></td></tr>
<tr><td width="25%" valign="top"><b> Upload #::uploadnum::</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_::uploadnum::"  /></td></tr>
<!--endoffirstupload-->
<tr><td valign="top" width="25%"> Optional Text #::uploadnum::</td>
<td><textarea name="linked_file_comment_::uploadnum::" rows="3" cols="50" wrap="virtual"></textarea></td></tr>
</table>	             
</div>

<div id="files_select_template_1" style="visibility: hidden;">
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td width="25%" valign="top"><b> Upload #1</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_1"  /></td></tr>
</table>	             
</div>
<!-- end publish template -->
