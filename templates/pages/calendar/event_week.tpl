<table cellpadding="6" cellspacing="0" border="0" width="100%" style="margin-top: -6px">
	<tr>
		<td valign="top">TPL_CAL_EVENT_MONTH_VIEW_PREV</td>
		<td align="center"><div class="eventNav">
				<a href="event_week.php?day=TPL_LOCAL_LAST_WEEK_DAY&amp;month=TPL_LOCAL_LAST_WEEK_MONTH&amp;year=TPL_LOCAL_LAST_WEEK_YEAR&amp;topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID&amp;news_item_status_restriction=TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION">
					TPL_PREV_WEEK
				</a> 
				&nbsp; 
				TPL_WEEK_OF
				TPL_CAL_CUR_DATE 
				&nbsp;
				<a href="event_week.php?day=TPL_LOCAL_NEXT_WEEK_DAY&amp;month=TPL_LOCAL_NEXT_WEEK_MONTH&amp;year=TPL_LOCAL_NEXT_WEEK_YEAR&amp;topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID&amp;news_item_status_restriction=TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION">
					TPL_NEXT_WEEK
				</a>
			</div>
			<div class="eventNav2">
				<a href="/calendar/event_add.php?topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID">
				TPL_CAL_ADD_AN_EVENT</a>
				&nbsp;
				&nbsp;
				&nbsp;
				&nbsp;
<a href="#ical" title="Add to calendar application"><img align="top" border="0" width="16" height="16" alt="iCal feed" src="/images/feed-icon-16x16.png" /></a>
				&nbsp;
				&nbsp;
				&nbsp;
				&nbsp;
				<a href="/search/search_results.php?news_item_status_restriction=690690&amp;include_events=1&amp;search_date_type=displayed_date&amp;submitted_search=1&amp;topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID">				
					TPL_CAL_LIST_FUTURE_EVENTS
				</a>
			</div>
			<form action="event_week.php" method="GET" style="font-size: x-small; margin-top: 1em; margin-bottom: 0">
			<div class="eventNav3">
				
					<strong>TPL_TOPIC:</strong>	TPL_CAL_EVENT_TOPIC_DROPDOWN
					
			</div>
			<div class="eventNav3">
				
					<strong>TPL_REGION:</strong>	TPL_CAL_EVENT_LOCATION_DROPDOWN
				
			</div>
			<input type="hidden" name="day" value="TPL_LOCAL_DAY"/>
			<input type="hidden" name="month" value="TPL_LOCAL_MONTH"/>
			<input type="hidden" name="year" value="TPL_LOCAL_YEAR"/>
			<input type=submit name="Filter" style="font-size: x-small" value="TPL_FILTER" />
			</form>
</td>
		<td align="right" valign="top">TPL_CAL_EVENT_MONTH_VIEW_NEXT</td>
	</tr>
</table>

<table width="100%" cellspacing="1" cellpadding="3" class="bodyClass">
	<tr>
		TPL_CAL_EVENT_MONTH_DAYTITLE
	</tr>
	TPL_CAL_EVENT_MONTH_VIEW_FULL
</table>
<a name="ical"></a>
<br />
<table cellpadding="3" cellspacing="3"><tr><th align="right"><a 
href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID" 
title="iCal feed allows you to subscribe to this calendar"><img align="top" border="0" width="16" height="16" alt="iCal feed" 
src="/images/feed-icon-16x16.png" /></a> Subscribe to this calendar:</th>
<td><a href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID" 
title="Add to iCal or compatible calendar application">All events</a></td> 
<td><a 
href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id=TPL_LOCAL_TOPIC_ID&amp;region_id=TPL_LOCAL_REGION_ID&amp;news_item_status_restriction=105"
title="Add featured events to calendar application">Featured events</a></td>
</tr><tr>
<th align="right">Add this calendar to your Google Calendar:</th>
<td><a 
href="http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3DTPL_LOCAL_TOPIC_ID%26region_id%3DTPL_LOCAL_REGION_ID"
title="Add this calendar to your Google calendar">All events</a></td>
<td><a 
href="http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3DTPL_LOCAL_TOPIC_ID%26region_id%3DTPL_LOCAL_REGION_ID%26news_item_status_restriction%3D105"
title="Add featured events to your Google calendar">Featured events</a></td></tr></table>
