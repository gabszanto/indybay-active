BEGIN:VCALENDAR
VERSION:2.0
X-WR-CALNAME:www.indybay.org
PRODID:-//indybay/ical// v1.0//EN
BEGIN:VEVENT
UID:Indybay-TPL_LOCAL_NEWS_ITEM_ID
SEQUENCE:TPL_LOCAL_NEWS_ITEM_VERSION_ID
CREATED:TPL_LOCAL_ICAL_CREATED
DESCRIPTION:TPL_LOCAL_DESCRIPTION
SUMMARY:TPL_LOCAL_TITLE
LOCATION:TPL_LOCAL_SUMMARY
URL:TPL_LOCAL_URL
DTSTART:TPL_LOCAL_ICAL_START
DTEND:TPL_LOCAL_ICAL_END
END:VEVENT
END:VCALENDAR
