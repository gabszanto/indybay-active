<!-- Mailing List Template -->
<table align="right" style="width: 25%">
<tr><td><ul><script type="text/javascript" 
src="/syn/jscript.php">
</script></ul></td></tr></table>

<h3>Weekly Email Of News And Events</h3>
<p>Subscribe or manage your subscription at <br />
<a href="https://lists.riseup.net/www/info/indybay-news">https://lists.riseup.net/www/info/indybay-news</a></p>

<h3>Indybay Event Calendar</h3> Browse the <a href="/calendar/">calendar</a> for iCal feeds filtered by topic and/or region.  These iCal feeds work with a variety of 
calendar applications for your computer or mobile device as well as online calendar apps. Here's a <a 
href="http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D0%26region_id%3D36" 
title="Add a calendar feed to your Google calendar">sample link</a> to add the City of San Francisco calendar to your Google Calendar.</p>

<h3>Audio and Video feeds ("podcasts")</h3>
<table><tr>
<td rowspan="2">(iTunes 4.9+)</td>
<td><a href="/syn/audio.pcast"><img src="/im/pcast.png" width="45" 
height="15" border="0" alt="pcast" /></a></td><td><a 
href="/syn/audio.pcast">Subscribe to Indybay's audio channel</a></td></tr>
<tr><td><a href="/syn/video.pcast"><img src="/im/pcast.png" 
width="45" height="15" border="0" alt="pcast" /></a></td><td><a 
href="/syn/video.pcast">Subscribe to Indybay's video channel</a></td></tr>
<tr><td colspan="2"><a href="http://subscribe.getmiro.com/?url1=http%3A%2F%2Fwww.indybay.org%2Fsyn%2Fvideo.rss" title="Miro: Internet TV"><img src="/im/miro-wes1.png" alt="Miro Video Player" border="0" /></a></td><td><a href="http://subscribe.getmiro.com/?url1=http%3A%2F%2Fwww.indybay.org%2Fsyn%2Fvideo.rss">Subscribe to Indybay's video channel</a></td></tr>
</table>

<p><b>Javascript Newsfeed:</b></p>

<p>
Indymedia is on the FBI's watch list -- put it on yours too! 
Simply include this HTML in your webpage to add our headlines to 
your site:<br /><code> &lt;script
type="text/javascript"
src="http://www.indybay.org/syn/jscript.php"&gt;&lt;/script&gt;</code>.</p>

<br clear="all" />
<div class="subscribe_container">
<div class="subscribe_header">RSS/XML Newsfeeds:</div>
<div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?include_posts=0&amp;include_blurbs=1">feature stories</a> (rss)</div>

<div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155">Newswire</a> (rss)</div>
<div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155&amp;rss_version=2">Newswire with enclosures</a> (rss)</div>
<div class="subscribe_item"><a class="subscribe_link" href="/syn/">Complete Syndication Index</a> <b>includes topical and regional feeds!</b></div>
</div>
You can add the above URLs to your blog, web portal (e.g. <a 
href="http://e.my.yahoo.com/config/promo_content?.module=ycontent&amp;.url=http%3a//www.indybay.org">Yahoo</a>), 
PDA, desktop applet, or browser extension. Feel free to <!-- <a 
href="mailto:sfbay-tech@indymedia.org"> --><a 
href="mailto:indybay@lists.riseup.net">let us know</a> if 
you find it useful or if there's a problem.


<div class="subscribe_container">
<div class="subscribe_header">3rd party syndication services:</div>
<div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/frontpage">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/frontpage">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/frontpage">Frontpage featured stories</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/media">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/media">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/media">Newswire with media enclosures</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/features">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/features">add to google</a></span>

<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/features">Featured stories for all sections of the site</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/newswire">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/newswire">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/newswire">Open-publishing newswire</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/video">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/video">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/video">Video</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/radio">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/radio">add to google</a></span>

<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/radio">Radio</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/photos">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/photos">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/photos">Photo</a></div><div class="subscribe_item">
<span><a href="http://add.my.yahoo.com/rss?url=http://feeds.feedburner.com/indybay/torrents">add to yahoo</a></span> <span><a href="http://fusion.google.com/add?feedurl=http://feeds.feedburner.com/indybay/torrents">add to google</a></span>
<a class="subscribe_link" href="http://feeds.feedburner.com/indybay/torrents">BitTorrent torrents</a></div>
</div>

<!-- <h3>Indybay <a href="/breaking/">Breaking News</a> TxtMob</h3>

Register your mobile phone at <a href="http://www.txtmob.com/">www.TxtMob.com</a> and join the 
"Indybay-breaking" TxtMob to receive breaking news updates from Indybay and ECR via text messaging. -->  
