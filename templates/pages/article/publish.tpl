<!-- publish form part 1 -->
TPL_HIDE1
TPL_LOCAL_DISPLAY_PREVIEW
TPL_HIDE2

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" action="/publish.php" accept-charset="UTF-8" id="publish-form">

TPL_VALIDATION_MESSAGES
TPL_STATUS_MESSAGES

TPL_HIDE1

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;TPL_PUB_STEPONE</strong><table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
      <tr>
        <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
          <tr class="bgpenult" valign="top">
            <td width="25%"><strong>TPL_TITLE</strong> <small>(TPL_REQUIRED)</small></td>
            <td width="75%"><input type="text" name="title1" id="title1" style="width: 90%" maxlength="90" value="TPL_LOCAL_TITLE1" /></td>
          </tr>
          <tr>
            <td>TPL_TOPIC</td>
            <td>TPL_CAT_TOPIC_SELECT</td>
          </tr>
          <tr>
            <td>TPL_REGION</td>
            <td>TPL_CAT_REGION_SELECT</td>
          </tr>
          <tr valign="top" class="bgpenult"> 
            <td width="25%"><strong>TPL_PUB_AUTHOR</strong> <small>(TPL_REQUIRED)</small></td>
            <td width="75%"><input type="text" name="displayed_author_name" id="displayed_author_name" size="25" maxlength="45" value="TPL_LOCAL_DISPLAYED_AUTHOR_NAME" /></td>
          </tr>
          <tr valign="top" class="bgpenult"> 
            <td width="25%">TPL_EMAIL</td>
            <td width="75%"><input type="text" name="email" size="25" maxlength="45" value="TPL_LOCAL_EMAIL" />&nbsp;&nbsp;TPL_DISPLAY_EMAIL TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO</td>
          </tr>
          <tr valign="top" class="bgpenult">
            <td width="25%"><strong>TPL_SUMMARY</strong> <small>(TPL_REQUIRED)</small></td>
            <td width="75%"><textarea name="summary" id="summary" rows="3" style="width: 90%" cols="80">TPL_LOCAL_SUMMARY</textarea><br />TPL_PUB_SUM</td>
          </tr>
          <tr class="bgpenult">
            <td width="25%">TPL_PUB_URL</td>
            <td width="75%"><input type="text" name="related_url" style="width: 90%" maxlength="255" value="TPL_LOCAL_RELATED_URL"/></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- publish form part 2 -->

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;TPL_PUB_STEPTWO</strong><table width="100%" border="0" cellspacing="0" cellpadding="4"  class="bgpenult">
      <tr>
        <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
          <tr>
            <td width="25%" valign="top"><strong>TEXT/HTML</strong></td>
            <td width="75%"><textarea name="text" id="text" rows="10" style="width: 90%" cols="80">TPL_LOCAL_TEXT</textarea><br />TPL_PUB_ART</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><strong>TPL_IS_TEXT_HTML</strong> TPL_LOCAL_CHECKBOX_IS_TEXT_HTML</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
<!-- publish form part 3 -->

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;TPL_PUB_STEPTHREE</strong><table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
      <tr class="bgpenult">
        <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
              <tr class="bgpenult" valign="top">
                <td width="25%"><strong>TPL_PUB_NUMBER_OF_UPLOADS</strong></td>
                <td width="75%"><div id="upload_warning">(A title, author and summary must be entered before you can choose uploads)
                  </div>
                  TPL_SELECT_FILECOUNT
                  <input type="submit" name="action" value="Enter" id="upload_submit_button"/>
                  <div class="fasttip">
                    <div class="trigger"><b>Please note the allowed file types and file sizes!</b></div>
                    <div class="target">
                      <br />
                      TPL_MAX_UPLOAD TPL_UPLOAD_MAX_FILESIZE; TPL_TOTAL_LIMIT TPL_POST_MAX_SIZE; TPL_MAX_EXECUTION_TIME TPL_HOURS
                      <br />
                      TPL_ACCEPTED_TYPES
                    </div>
                  </div>
                  <div id="file_boxes2"></div>
                  
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>
    
    <div id="nonjscript_file_boxes">TPL_FILE_BOXES</div>
    
    </td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;CAPTCHA</strong><table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
      <tr class="bgpenult">
        <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
              <tr class="bgpenult" valign="top">
                <td width="25%"><strong>Anti-spam questions</strong> (Required)</td>
                <td width="75%">TPL_CAPTCHA_FORM</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<p align="center">
  <span id="preview_button">TPL_LOCAL_PREVIEW</span>
  <input type="submit" name="publish" value="TPL_BUTTON_PUBLISH" />
</p>

TPL_HIDE2

</form>

<div id="files_select_template" style="visibility: hidden;">
<hr />
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td valign="top" width="25%"><b>Title #::uploadnum::</b> <small></small></td><td><input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" value="" /></td></tr>
<tr><td width="25%" valign="top"><b> Upload #::uploadnum::</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_::uploadnum::"  /></td></tr>
<!--endoffirstupload-->
<tr><td valign="top" width="25%"> Optional Text #::uploadnum::</td>
<td><textarea name="linked_file_comment_::uploadnum::" rows="3" cols="50"></textarea></td></tr>
</table>	             
</div>

<div id="files_select_template_1" style="visibility: hidden;">
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td width="25%" valign="top"><b> Upload #1</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_1"  /></td></tr>
</table>	             
</div>

<!-- end publish template -->
