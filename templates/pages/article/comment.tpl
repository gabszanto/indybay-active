<h3>Add comment on:</h3>
<strong>TPL_LOCAL_PARENT_TITLE1</strong>
<br />
<i>TPL_LOCAL_PARENT_DISPLAYED_AUTHOR_NAME</i>
<br />
TPL_LOCAL_PARENT_SUMMARY


TPL_HIDE1
TPL_LOCAL_DISPLAY_PREVIEW
TPL_HIDE2

<h3>Guidelines for commenting on news articles:</h3>

<p>Thanks for contributing to Indybay's open publishing newswire. You may use any format for your response article, from traditional academic discourse to 
subjective personal account. Please keep it on topic and concise. And please read our <a href="/newsitems/2002/08/04/1395001.php">editorial policy</a>, <a 
href="/newsitems/2003/12/15/16659061.php">privacy</a>, and <a href="/newsitems/2003/12/15/16659051.php">legal</a> statements before continuing. Or <a 
href="javascript:history.back()">go back to the article</a>.</p>

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" accept-charset="UTF-8" action="/comment.php?top_id=TPL_LOCAL_PARENT_ITEM_ID" id="publish-form">
<input type="hidden" name="parent_item_id" value="TPL_LOCAL_PARENT_ITEM_ID" />
TPL_VALIDATION_MESSAGES
TPL_STATUS_MESSAGES

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr><td class="bgaccent">


<table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
<tr><td>
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr class="bgpenult" valign="top">

<td width="25%">
<strong>TPL_TITLE3</strong> <small>(TPL_REQUIRED)</small>
</td>

<td width="75%">
<input type="text" name="title1" style="width: 90%" maxlength="90" value="TPL_LOCAL_TITLE1" />
</td>

</tr>
<tr valign="top" class="bgpenult"> 

<td width="25%">
<strong>TPL_PUB_AUTHOR</strong> <small>(TPL_REQUIRED)</small>
</td>

<td width="75%">
<input type="text" name="displayed_author_name" size="25" maxlength="45" value="TPL_LOCAL_DISPLAYED_AUTHOR_NAME" />
</td>

</tr>

<tr valign="top" class="bgpenult"> 
<td width="25%">
TPL_EMAIL

</td>
<td width="75%">
<input type="text" name="email" size="25" maxlength="45" value="TPL_LOCAL_EMAIL" />
&nbsp;
&nbsp;
TPL_DISPLAY_EMAIL
TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO
</td>
</tr>

<tr class="bgpenult">

<td width="25%">TPL_PUB_URL</td>

<td width="75%">
<input type="text" name="related_url" style="width: 90%" maxlength="255" value="TPL_LOCAL_RELATED_URL"/>
</td>

</tr></table>

</td></tr></table>


<table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
<tr><td>

<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr>

<td width="25%" valign="top">
<strong>TEXT/HTML</strong>
</td>

<td width="75%">
<textarea name="text" rows="10" cols="80" style="width: 90%">TPL_LOCAL_TEXT</textarea>
<br />TPL_PUB_ART
</td>

</tr>

<tr><td>&nbsp;</td><td><strong>TPL_IS_TEXT_HTML</strong> TPL_LOCAL_CHECKBOX_IS_TEXT_HTML
</td></tr>

</table>

</td></tr></table>

</td></tr></table>

<!-- publish form part 3 -->

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr><td class="bgaccent">

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
<tr class="bgpenult"><td>

<table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
<tr><td>

<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr class="bgpenult" valign="top">

<td width="25%">
<strong>TPL_PUB_CUANTOFILES</strong>
</td>

<td width="75%">
TPL_SELECT_FILECOUNT

<input type="submit" name="action" value="Enter" id="upload_submit_button" /><br />
TPL_MAX_UPLOAD TPL_UPLOAD_MAX_FILESIZE; TPL_TOTAL_LIMIT TPL_POST_MAX_SIZE; TPL_MAX_EXECUTION_TIME TPL_HOURS
<br />TPL_ACCEPTED_TYPES
</td>

</tr></table>
<div id="file_boxes2"></div>
</td></tr></table>
</td></tr></table>

<div id="nonjscript_file_boxes">TPL_FILE_BOXES</div>
 
</td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
  <tr>
    <td class="bgaccent"><strong>&nbsp;CAPTCHA</strong><table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
      <tr class="bgpenult">
        <td><table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td><table width="100%" border="0" cellspacing="2" cellpadding="4">
              <tr class="bgpenult" valign="top">
                <td width="25%"><strong>Anti-spam questions</strong> (Required)</td>
                <td width="75%">TPL_CAPTCHA_FORM</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>

<p align="center">
<span id="preview_button">TPL_LOCAL_PREVIEW</span>
<input type="submit" name="publish" value="TPL_BUTTON_PUBLISH" /></p>
</form>
<!-- end publish template -->


<div id="files_select_template" style="visibility: hidden;">
<hr />
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td valign="top" width="25%"><b>Title #::uploadnum::</b> <small></small></td><td><input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" value="" /></td></tr>
<tr><td width="25%" valign="top"><b> Upload #::uploadnum::</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_::uploadnum::"  /></td></tr>
<!--endoffirstupload-->
<tr><td valign="top" width="25%"> Optional Text #::uploadnum::</td>
<td><textarea name="linked_file_comment_::uploadnum::" rows="3" cols="50"></textarea></td></tr>
</table>	             
</div>


<div id="files_select_template_1" style="visibility: hidden;">
<table width="100%" border="0" cellspacing="2" cellpadding="4">
<tr><td width="25%" valign="top"><b> Upload #1</b></td>
<td width="75%">            	 
 <input type="file" name="linked_file_1"  /></td></tr>
</table>	             
</div>
