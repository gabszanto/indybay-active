<form name="search" action="/search/search_results.php">
<h2>TPL_SEARCH</h2>
<br />
TPL_KEYWORD: <input name="search" type="text" size="14"  value="TPL_LOCAL_SEARCH" />
<div>Multiple search words imply <em>or</em> logic.  A leading plus sign indicates that the word <em>must</em> be present.
To search for an exact phrase, enclose it in double quotes.</div>
<dl>
<dt><em>apple banana</em></dt><dd>Find articles that contain at least one of the two words.</dd>
<dt><em>+apple +juice</em></dt><dd>Find articles that contain both words.</dd>
<dt><em>+apple -macintosh</em></dt><dd>Find articles that contain the word <q>apple</q> but not <q>macintosh</q>.</dd>
<dt><em>apple*</em></dt><dd>Find articles that contain words such as <q>apple</q>, <q>apples</q>, <q>applesauce</q>, or <q>applet</q>.</dd>
<dt><em>"some words"</em></dt><dd>Find articles that contain the exact phrase <q>some words</q> (for example, <q>some words of wisdom</q> but not <q>some noise words</q>).</dd>
</dl>
<br /><br />
TPL_MEDIA_TYPE TPL_SEARCH_MEDIUM
&nbsp;
TPL_DISPLAY_OPTIONS TPL_SEARCH_DISPLAY
<br /><br /><br /><br />
TPL_TOPIC TPL_CAT_TOPIC_SELECT &nbsp;
TPL_REGION TPL_CAT_REGION_SELECT
<br /><br /><br /><br />

TPL_SEARCH_DATE_TYPE_SELECT

<br />
TPL_SEARCH_BETWEEN
TPL_DATE_BETWEEN_START
TPL_SEARCH_AND
TPL_DATE_BETWEEN_END
<br /><br /><br /><br />
TPL_INCLUDE_ATTACHMENTS
TPL_LOCAL_CHECKBOX_ATTACHMENTS
TPL_INCLUDE_EVENTS
TPL_LOCAL_CHECKBOX_EVENTS
TPL_INCLUDE_POSTS
TPL_LOCAL_CHECKBOX_POSTS
TPL_INCLUDE_BLURBS
TPL_LOCAL_CHECKBOX_BLURBS
<br /><br />
<input type="submit" value="TPL_GO" name="submitted_search" />
</form>
