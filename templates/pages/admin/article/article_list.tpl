<strong>
TPL_VALIDATION_MESSAGES
TPL_STATUS_MESSAGES
</strong>
TPL_PDA_HIDE_BEGIN
<h3>TPL_ARTICLES_ADMIN</h3>

(<a href="#aboutclass">Read About New Classification choices</a>)
TPL_PDA_HIDE_END
<hr />
TPL_PDA_HIDE_BEGIN
<table><tr>
<td>

<form action="article_edit.php">TPL_ENTER_ARTICLE_ID<br />
<input type="text" name="id" value="">
<input type="submit" name="TPL_ARTICLE_EDIT" value="Submit"></form>
</Form>
</td><td>
<form action="index.php">TPL_ENTER_COMMENT_ID<br />
<input type="text" name="parent_item_id" value="TPL_LOCAL_PARENT_ITEM_ID">
<input type="hidden" name="comments" value="yes">
<input type="submit" name="force_parent_id_search" value="Submit"></form>
</Form>
</td>
</tr></table>

<hr />
TPL_PDA_HIDE_END
<form style="margin-top: 0; margin-bottom: 0.5em" method="get" action="index.php">
TPL_TOP_SEARCH_FORM

TPL_NAV
</form>
<FORM method="post" name="order_form">
<INPUT type="submit" name="classify" value="TPL_SAVE"/><br >
<INPUT type="hidden" name="num_rows" value="TPL_NUM_ROWS" />
<INPUT type="hidden" name="bulk_classify" value="1" />
<br >
TPL_TABLE_MIDDLE
TPL_NAV
<INPUT type=submit name="classify" value="TPL_SAVE"/>
<p/>
<a href="JavaScript:hideall();">Set All Dropdowns to Hidden</a>
|
<a href="JavaScript:document.forms['order_form'].reset()">Reset</a>
<SCRIPT LANGUAGE="JavaScript">
function changeStatusSelect(cell_counter, state){
	document.getElementById("value_"+cell_counter).value=state;
}
function hideall(){
	var i=0;
	for (i=0;i<200 && i<document.forms["order_form"].elements.length;i++){
		var nextelement=document.forms["order_form"].elements[i];
		if (nextelement.type=="select-one"){
			for (j=0;j<200 && j<nextelement.options.length;j++){
				if (nextelement.options[j].text=="Hidden")
					nextelement.selectedIndex = j;
			}
		}
	}
}
</SCRIPT>

</FORM>


TPL_PDA_HIDE_BEGIN
<br ><br /><br >
<strong><a name="aboutclass">ABOUT CLASSIFICATIONS</a></strong>
<br ><br />
The new article admin system gives you a choice of more ways to classify posts than the old system.
<br /><br />
NEW is the status when anything new comes in. 
<br /><br />
The HIGHLIGHTED statuses are used to make posts what we call "local" and "global" 
in the right column. If you make an event HIGHLIGHTED (local or nonlocal) it will appear in the redlinks.
<br /><br />

OTHER acts the same as NEW but is a way to mark something as having been examined.
The corporate reports choices are similar to OTHER on most pages 
but they make posts appear in the right column on international pages.
<br /><br />
HIDDEN hides a post or comment
<br /><br />
QUESTIONABLE means you are not sure if something should be hidden (QUESTIONABLE/HIDE will hide it and QUESTIONABLE/DONTHIDE will not hide it)
<br ><br /><br />
<strong>Classifying Comments</strong>
<br ><br >
To make it easier for editors to know if comments have been reviewed it is worthwhile to classify comments. 
The "Needs Attention" page only shows NEW and QUESTIONABLE comments so you will want to classify any comment that shouldn't be hidden as OTHER.
Classifying a comment as hidden will hide it but the other statuses will not have an effect on how the comment is seen.
<br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
TPL_PDA_HIDE_END


