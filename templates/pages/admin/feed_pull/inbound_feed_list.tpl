
<h3>RSS Feeds</h3>
This RSS feed pulling system is intended to make it easier to keep the site up to date with content from local and nonlocal alternative blogs and news site.
<p/>
<strong><a href="/admin/feed_pull/inbound_feed_add.php">Add A Feed</a> </strong>
<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>
<form method="post">
<h4><a name="local">Highlighted Local Feeds</a></h4>
<a href="/admin/feed_pull/feed_item_list.php?feed_id=0&amp;status_id=3">View All Highlighted Local Pulled Items</a>
<TABLE >
<tr style="background-color: #e0e0e0;">
<td><strong>id</strong></td>
<td><strong>name</strong></td>
<td>&nbsp;</td><td>&nbsp;</td>
<td><strong>items</strong></td>
<td><strong>problems</strong></td>
<td><strong>more info</strong></td>
</tr>
TPL_LOCAL_HIGHLIGHTEDLOCAL_FEED_LIST
</TABLE>
<input type="submit"  name="pull_local" value="Pull All Local Feeds">
<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>
<h4><a name="global">Highlighted NonLocal Feeds</a></h4>
<a href="/admin/feed_pull/feed_item_list.php?feed_id=0&amp;status_id=5">View All Highlighted NonLocal Pulled Items</a>
<TABLE >
<tr style="background-color: #e0e0e0;" >
<td><strong>id</strong></td>
<td><strong>name</strong></td>
<td>&nbsp;</td><td>&nbsp;</td>
<td><strong>items</strong></td>
<td><strong>problems</strong></td>
<td><strong>more info</strong></td>
</tr>
TPL_LOCAL_HIGHLIGHTEDNONLOCAL_FEED_LIST
</TABLE>
<input type="submit"  name="pull_nonlocal" value="Pull All NonLocal Feeds">

<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>
<h4><a name="corplocal">Local Corporate Repost Feeds</a></h4>
<a href="/admin/feed_pull/feed_item_list.php?feed_id=0&amp;status_id=7">View All Local Corporate Pulled Items</a>
<TABLE >
<tr style="background-color: #e0e0e0;">
<td><strong>id</strong></td>
<td><strong>name</strong></td>
<td>&nbsp;</td><td>&nbsp;</td>
<td><strong>items</strong></td>
<td><strong>problems</strong></td>
<td><strong>more info</strong></td>
</tr>
TPL_LOCAL_CORPLOCAL_FEED_LIST
</TABLE>
<input type="submit"  name="pull_corplocal" value="Pull All Local Corporate Feeds">
<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>

<h4><a name="corpglobal">NonLocal Corporate Repost Feeds</a></h4>
<a href="/admin/feed_pull/feed_item_list.php?feed_id=0&amp;status_id=11">View All NonLocal Corporate Pulled Items</a>
<TABLE >
<tr style="background-color: #e0e0e0;">
<td><strong>id</strong></td>
<td><strong>name</strong></td>
<td>&nbsp;</td><td>&nbsp;</td>
<td><strong>items</strong></td>
<td><strong>problems</strong></td>
<td><strong>more info</strong></td>
</tr>
TPL_LOCAL_CORPNONLOCAL_FEED_LIST
</TABLE>
<input type="submit"  name="pull_corpnonlocal" value="Pull All NonLocal Corporate Feeds">

<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>

<h4><a name="other">Other Feeds</a> (Still being Configured, Or Reference Ones That Should Rarely Get Posted)</h4>
<a href="/admin/feed_pull/feed_item_list.php?feed_id=0&amp;status_id=2">View All Other Pulled Items</a>
<TABLE >
<tr style="background-color: #e0e0e0;">
<td><strong>id</strong></td>
<td><strong>name</strong></td>
<td>&nbsp;</td><td>&nbsp;</td>
<td><strong>items</strong></td>
<td><strong>problems</strong></td>
<td><strong>more info</strong></td>
</tr>
TPL_LOCAL_OTHER_FEED_LIST
</TABLE>
<input type="submit"  name="pull_other" value="Pull Other Feeds">


<hr />
<small>
[
<a href="#local">Highlighted Local</a>
|
<a href="#global">Highlighted NonLocal</a>
|
<a href="#corplocal">Corp Local</a>
|
<a href="#corpglobal">Corp NonLocal</a>
|
<a href="#all">Other</a>
|
<a href="#all">All</a>
]
</small>
<p/>

<h4><a name ="all" href="/admin/feed_pull/feed_item_list.php?feed_id=0">View Recently Pulled Items From All Feeds</a></h4>

<input type="submit"  name="pull_all" value="Pull Everything">
<input type="submit"  name="delete_all" value="Delete All Pulled Items">
</FORM>



