<h3>Feed Edit Page For "TPL_LOCAL_NAME"</h3>
<strong>
<a href="/admin/feed_pull/inbound_feed_list.php">Return to Feed List</a>
|
<a href="/admin/feed_pull/feed_item_list.php?feed_id=TPL_LOCAL_RSS_FEED_ID">View Pulled Items</a>
</strong>
<br /><br />
<form method="post">
<input type="hidden" name="feed_id" value="TPL_LOCAL_RSS_FEED_ID">
<table>
<tr>
<td><strong>Name:</strong></td>
<td> <input type="text"  name="name" value="TPL_LOCAL_NAME"></td>
</tr>
<tr>
<td><strong>URL:</strong></td>
<td><input size=60  type="text" name="url" value="TPL_LOCAL_URL"></td>
</tr>
<tr>
<td><strong>Feed Type:</strong></td>
<td>TPL_LOCAL_FEED_TYPE_SELECT</td>
</tr>
<tr>
<td valign="top"><strong>Restrict Item URLs:</strong><br /><small>Needed for HTML Type Feeds</small></td>
<td><input size=60  type="text" name="restrict_urls" value="TPL_LOCAL_RESTRICT_URLS"></td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2"><h3>Templates</h3></td></tr>
<tr>
<td><strong>Author Template:</strong></td>
<td> <input type="text" size=60  name="author_template" value="TPL_LOCAL_AUTHOR_TEMPLATE"></td>
</tr>
<tr>
<td><strong>Summary Template:</strong></td>
<td> <textarea type="text" cols=60 rows=6 name="summary_template">TPL_LOCAL_SUMMARY_TEMPLATE</textarea></td>
</tr>
<tr>
<td><strong>Text Template:</strong></td>
<td> <textarea type="text" cols=60 rows=6 name="text_template">TPL_LOCAL_TEXT_TEMPLATE</textarea></td>
</tr>
<tr><td ><strong>Default Topic:</strong></td><td>TPL_CAT_TOPIC_SELECT<td></td></tr>
<tr><td ><strong>Default Region:</strong></td><td>TPL_CAT_REGION_SELECT<td></td></tr>
<tr><td ><strong>Default Status:</strong></td><td>TPL_STATUS_SELECT<td></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr><td colspan="2"><h3>HTML Scraping Options</h3>None of these are required, and are mainly useful if items are missing from the rss feeds
<br />
<small>Use [string1]&&[string2] to have code look for first occurance of string2 after string1.<br />
Use [expression1]||[expression2] to use the search for expression1 and if that fails do the one for expression2.</small>
<br />
When searching for text or subject yourt can use the keywords AUTHOR, SUBJECT and TITLE to begin search after the code looks for a match on the previously set field.
</small>
</td></tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td valign="top"><strong>Scrape Summary:</strong></td>
<td>after <input type="text" size=60 name="summary_scrape_start" value="TPL_LOCAL_SCRAPE_SUMMARY_START">
<br />but before<br />
<input type="text" size=60 name="summary_scrape_end" value="TPL_LOCAL_SCRAPE_SUMMARY_END">
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td valign="top"><strong>Scrape Text:</strong></td>
<td>after <input type="text" size=60 name="text_scrape_start" value="TPL_LOCAL_SCRAPE_TEXT_START">
<br />but before<br />
<input type="text" size=60 name="text_scrape_end" value="TPL_LOCAL_SCRAPE_TEXT_END">
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td valign="top"><strong>Scrape Author:</strong></td>
<td>after <input type="text" size=60 name="author_scrape_start" value="TPL_LOCAL_SCRAPE_AUTHOR_START">
<br />but before<br />
<input type="text" size=60 name="author_scrape_end" value="TPL_LOCAL_SCRAPE_AUTHOR_END">
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td valign="top"><strong>Scrape Title:</strong></td>
<td>after <input type="text" size=60 name="title_scrape_start" value="TPL_LOCAL_SCRAPE_TITLE_START">
<br />but before<br />
<input type="text" size=60 name="title_scrape_end" value="TPL_LOCAL_SCRAPE_TITLE_END">
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td valign="top"><strong>Scrape Date:</strong></td>
<td>after <input type="text" size=60 name="date_scrape_start" value="TPL_LOCAL_SCRAPE_DATE_START">
<br />but before<br />
<input type="text" size=60 name="date_scrape_end" value="TPL_LOCAL_SCRAPE_DATE_END">
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr>
<td><strong>Item URL Replace:</strong></td>
<td><input type="text"  name="replace_url" value="TPL_LOCAL_REPLACE_URL_FROM">
with
<input type="text" name="replace_url_with" value="TPL_LOCAL_REPLACE_URL_TO">
<br />
<small>Use this if the url returned for items in the rss feed isnt the one that needs to be scraped.</small>
</td>
</tr>
<tr><td colspan="2"><hr></td></tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
<td colspan="2">
<input type="submit" name="update" value="update">
&nbsp;
<input type="submit" name="delete" value="delete">
&nbsp;
<input type="submit" name="delete_items" value="delete items">
&nbsp;
<input type="submit" name="clone" value="clone feed">
</td>
</tr>
</table>
</form>
