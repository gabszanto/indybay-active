<!-- publish form part 1 -->

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" action="/admin/feature_page/blurb_add.php">

TPL_VALIDATION_MESSAGES
TPL_STATUS_MESSAGES

<table width="80%" border="0" cellspacing="2" cellpadding="4" class="bgsearchgrey">
<tr  valign="top">

<td width="25%">
<strong>TPL_TITLE1</strong> 
</td>

<td width="75%">
<input type="text" name="title1" style="width: 90%" maxlength="90" value="TPL_LOCAL_TITLE1" />
</td>

</tr>

<tr  valign="top">

<td width="25%">
<strong>TPL_TITLE2</strong> <small>(TPL_REQUIRED)</small>
</td>

<td width="75%">
<input type="text" name="title2" style="width: 90%" maxlength="90" value="TPL_LOCAL_TITLE2" />
</td>

</tr>


<tr><td ><strong>TPL_TOPIC:</strong></td><td>TPL_CAT_TOPIC_SELECT<td></td></tr>
<tr><td ><strong>TPL_REGION:</strong></td><td>TPL_CAT_REGION_SELECT<td></td></tr>
<tr><td ><strong>TPL_OTHER:</strong></td><td>TPL_CAT_OTHER_SELECT<td></td></tr>
</table>





<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td  colspan=2>
<strong>TPL_SHORT_VERSION</strong>
</td></tr>
<tr><td colspan=2>
<textarea name="summary" rows="6" style="width: 90%" wrap="virtual">TPL_LOCAL_SUMMARY</textarea>
<br />
</td>

</tr>

<tr><td coslpan=2><strong>TPL_IS_SHORT_VERSION_HTML</strong> TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML
</td></tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
<strong>TPL_THUMBNAIL_MEDIA_ATTACHMENT:</strong><br />
<small>
TPL_IMAGE_SELECT_DESC
</small>
<br />
TPL_ENTER_ID
<input type="text" name="thumbnail_media_attachment_id" size=10 maxlength="15" value="TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID" />
<br />
TPL_OR
<br />
TPL_CHOOSE_RECENT_ADMIN_UPLOAD
TPL_LOCAL_THUMBNAIL_RECENT_ADMIN_UPLOADS_SELECT
<br />
TPL_OR
<br />
TPL_UPLOAD_A_FILE
<input type="file" name="thumbnail_upload">
</td>
</tr>
<tr ><td colspan=2><hr /></td></tr>
</table>





<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td coslpan=2 >
<strong>TPL_LONG_VERSION</strong>
</td>
</tr>
<tr>
<td colspan=2>
<textarea name="text" rows="16" style="width: 90%" wrap="virtual">TPL_LOCAL_TEXT</textarea>
</td>

</tr>

<tr><td colspan=2><strong>TPL_IS_LONG_VERSION_HTML</strong> TPL_LOCAL_CHECKBOX_IS_TEXT_HTML
</td></tr>
<tr >

<td width="25%"><strong>TPL_PUB_URL</strong></td>

<td width="75%">
<input type="text" name="related_url" style="width: 90%" maxlength="255" value="TPL_LOCAL_RELATED_URL"/>
</td>

</tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
<strong>TPL_MEDIA_ATTACHMENT:</strong><br />
<small>
TPL_IMAGE_SELECT_DESC
</small>
<br />
TPL_ENTER_ID
<input type="text" name="media_attachment_id" size=10 maxlength="15" value="TPL_LOCAL_MEDIA_ATTACHMENT_ID" />
<br />
TPL_OR
<br />
TPL_CHOOSE_RECENT_ADMIN_UPLOAD
TPL_LOCAL_RECENT_ADMIN_UPLOADS_SELECT
<br />
TPL_OR
<br />
TPL_UPLOAD_A_FILE
<input type="file" name="main_upload">
</td>
</tr>
<tr ><td colspan=2><hr/></td></tr>



</table>


<input type="submit" name="publish" value="TPL_BUTTON_PUBLISH" />
</form>
<!-- end publish template -->
