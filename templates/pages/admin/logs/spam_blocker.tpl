<h3>TPL_SPAM_BLOCKS</h3>
<i>WARNING: This Spam Blocking System Has Largely Been Replaced By A New System <a href="spam_identification.php">Available Here</a>
</i>
<br />
Blocks work by taking all the parameters you define and only block where all of the conditions are met.
POST means HTTP post which only applies on the site for previews and posts (searches are done via HTTP GETs)
<br />
This section will be changed in a few months to make it easier to use (mainly through links in from logs and edit pages), but if you need to get more details about how to use blocks now you can email zogren@yahoo.com

<br /><br />
<form method="post">

Block specific IP: <input type=text name="ip" value="TPL_LOCAL_IP">
<br />
<small>block whole subnets:</small><br />
(
<input type=text name="ip_part1" value="TPL_LOCAL_IP1">
.
<input type=text name="ip_part2" value="TPL_LOCAL_IP2">
.
<input type=text name="ip_part3" value="TPL_LOCAL_IP3">
.
*
)
<br />
Restrict block to specific parent id: <input type=text name="parent_news_item_id" value="TPL_LOCAL_PARENT_NEWS_ITEM_ID">
<br />
Restrict to TPL_LOCAL_METHOD_SELECT <br />
<small>(blocking POST will disable to publish button and publish page, wheras blocking "DB Post" will post but make post or attachment save as hidden)
</small>
<br />
Where requested url contains
<input name="url" value="TPL_LOCAL_URL"/> and referring url contains <input name="referring_url" value="TPL_LOCAL_REFERRING_URL"/>
<br />
Where HTTP headers, title, author, email or summary contains:
<input name="keyword" value="TPL_LOCAL_KEYWORD"/>
<br /><br />
When blocked redirect user to the following url:
<input name="redirect_url" value="TPL_LOCAL_REDIRECT_URL"/>
<br />
note (for other admins) :
<input name="note" value="TPL_LOCAL_NOTE"/>
<br />
<input type="submit" name="add block" value="Add Block">
</form>

<table width=100%>
<tr bgcolor="#000000">
<td><font color="#ffffff">&nbsp;</font></td>
<td><font color="#ffffff">ip</font></td>
<td><font color="#ffffff">ip_part1</font></td>
<td><font color="#ffffff">ip_part2</font></td>
<td><font color="#ffffff">ip_part3</font></td>
<td><font color="#ffffff">method</font></td>
<td><font color="#ffffff">url</font></td>
<td><font color="#ffffff">ref url</font></td>
<td><font color="#ffffff">parent id</font></td>
<td><font color="#ffffff">more</font></td>
<td><font color="#ffffff">block_destination</font></td>
<td><font color="#ffffff">note</font></td>
<td><font color="#ffffff">added info</font></td>

</tr>
TPL_LOCAL_BLOCK_LIST
</table>
<form method="post">
<input type="submit" name="clear_all_blocks" value="Clear Blocks">

</form>