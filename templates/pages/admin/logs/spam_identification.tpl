<h3>Spam Identification</h3>
<i>This system is intended to replace the old block system, but since that did provide some additional options (like redirecting based off referring and desired url, it is still available <a href="spam_blocker.php">here</a><br />
<p/><p/>
<form method="post">
IPs to block from site (only use this for spam engines and spam attacks not trolls or those manually reposting right after you rehide):<br />
<textarea name="ip_block_list" cols="50" rows="10">TPL_LOCAL_BLOCK_IPS</textarea>
<br />
<input type ="submit" name="save_ip_block_list" value="Save">
</form>
<p/>
<form method="post">
IPs & Subnets That May Be Spam (posts may get hidden or prevented from posting based off other factors):<br />
<textarea name="ip_spam_list" cols="50" rows="10">TPL_LOCAL_SPAM_IPS</textarea>
<br />
<input type ="submit" name="save_ip_spam_list" value="Save">
</form>
<p/>
<form method="post">
Title Keywords That Could Indicate Spam (posts may get hidden or prevented from posting based off other factors):<br />
<textarea name="title_spam_list" cols="50" rows="10">TPL_LOCAL_SPAM_TITLE</textarea>
<br />
<input type ="submit" name="save_title_spam_list" value="Save">
</form>
<p/>
<form method="post">
Subject Or Text Keywords That Could Indicate Spam (posts may get hidden or prevented from posting based off other factors):<br />
<textarea name="text_spam_list" cols="50" rows="10">TPL_LOCAL_SPAM_TEXT</textarea>
<br />
<input type ="submit" name="save_text_spam_list" value="Save">
</form>
<p/>
<form method="post">
Specific Strings In Text That Should Be Blocked During Validation:<br />
<textarea name="validation_string_list" cols="50" rows="10">TPL_LOCAL_VALIDATION_STRINGS</textarea>
<br />
<input type ="submit" name="save_validation_string_list" value="Save">
</form>