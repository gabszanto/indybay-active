<!-- breaking news template -->
<a href="../index.php">TPL_ADMIN_INDEX</a> : 
<a href="../article/index.php?news_item_status_restriction=0&amp;submitted_search=Go">List Breaking News</a> | 
<a href="create.php?parent_item_id=TPL_PARENT_ITEM_ID">Create Breaking News</a>

<h3><i>TPL_CREATE_RESULT</i></h3>

<form action="create.php?parent_item_id=TPL_PARENT_ITEM_ID" method="post">
<input type="hidden" name="method" value="create" />
<p><label for="title1">News Text:<br />
<input type="text" maxlength="140" size="140" name="title1" id="title1" />
<br />140 characters max; no HTML!<br />
Include appropriate <a target="_blank" href="http://en.wikipedia.org/wiki/Tag_%28metadata%29#Hashtags">hash tags</a>.</label></p>
<p><label for="htm">Additional HTML (links etc.):<br />
<textarea rows="5" cols="80" name="htm" id="htm"></textarea>
<br />Additional HTML will not be syndicated to micro-blogging services!</label></p>
<p>
<label for="t"><input type="radio" name="display" value="t" id="t" /> Publish</label>
<br />
<label for="f"><input type="radio" name="display" value="f" id="f" checked="checked" /> Keep hidden</label></p>

<p><label for="s"><input type="radio" name="send" value="t" id="s" /> Syndicate to third-party services now</label>
<br />
<label for="d"><input type="radio" name="send" value="f" id="d" checked="checked" /> Do not syndicate now</label></p>

<p><label for="dis"><input type="radio" name="dispatch" value="1" id="dis" /> Dispatch to reporters</label>
<br />
<label for="nod"><input type="radio" name="dispatch" value="0" checked="checked" id="nod" /> Do not dispatch to reporters</label></p>

<input type="hidden" name="parent_item_id" value="TPL_PARENT_ITEM_ID" />

<p><input type="submit" value="Submit" /></p>

</form>

<!-- / breaking news template -->
