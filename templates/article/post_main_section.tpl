<!-- TEMPLATE -->
<div class="webcast">
<div class="headers">
<div class="heading"><strong class="heading">TPL_LOCAL_TITLE1</strong></div>
<div class="author">by TPL_LOCAL_DISPLAYED_AUTHOR_NAME
TPL_LOCAL_EMAIL
<br /><em>TPL_LOCAL_CREATED</em></div>
</div>
<blockquote class="summary">TPL_LOCAL_SUMMARY</blockquote>
TPL_LOCAL_NOMEDIA1
<center><div class="media">TPL_LOCAL_MEDIA</div></center><br />
TPL_LOCAL_NOMEDIA2
<div class="article">TPL_LOCAL_TEXT</div>
<div class="link"><a href="TPL_LOCAL_RELATED_URL">TPL_LOCAL_SHORTENED_RELATED_LINK</a></div>
</div>
<!-- /TEMPLATE -->
