<!-- TEMPLATE -->
<div class="webcast">
<div class="headers">
<div class="heading"><strong class="heading"><strike>TPL_LOCAL_TITLE1</strike></strong></div>
<div class="author">by TPL_LOCAL_DISPLAYED_AUTHOR_NAME <em>TPL_LOCAL_CREATED</em></div>
</div>
<blockquote class="summary"><strike>TPL_LOCAL_SUMMARY</strike></blockquote>
<div class="link"><strike>TPL_LOCAL_SHORTENED_RELATED_LINK</strike></div>
</div>
<!-- /TEMPLATE -->
