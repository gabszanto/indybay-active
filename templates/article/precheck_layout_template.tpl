require_once (CLASS_PATH."/renderer/article_renderer_class.inc");
require_once (CLASS_PATH."/db/article_db_class.inc");
require_once (CLASS_PATH."/cache/article_cache_class.inc");
$article_renderer=new ArticleRenderer();
$article_db=new ArticleDB();
$article_cache=new ArticleCache();
$article_info=$article_db->get_article_info_from_news_item_id(TPL_LOCAL_NEWS_ITEM_ID);
$directory=NEWS_PATH."/".$article_cache->create_date_based_relative_directory(NEWS_PATH."/",$article_info["creation_timestamp"]);
$relative_url=$article_renderer->find_recent_duplicate_nonhidden_urls_by_title("TPL_LOCAL_TITLE1", TPL_LOCAL_NEWS_ITEM_ID);

if ($relative_url!=""){
	$new_html="<?php \nheader(\"HTTP/1.0 301 Moved Permanently\");\n";
	$new_html.="header(\"Location: ".SERVER_URL.$relative_url."\");\n";
	$new_html.="?>";
	$article_cache->cache_file($directory."/"."TPL_LOCAL_NEWS_ITEM_ID.php", $new_html);
	header("HTTP/1.0 301 Moved Permanently");
	header("Location: ".SERVER_URL.$relative_url."");
	exit;
}else{
	$article_info["not_duplicate"]="1";
	$article_cache->cache_main_include_for_article($directory, $article_info);
	echo "<HTML><HEAD><meta http-equiv=\"refresh\" content=\"0\"></HEAD></HTML>";
	exit;
}