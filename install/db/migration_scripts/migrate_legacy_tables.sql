
DROP TABLE  indybay.legacy_sfactive_webcast;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_webcast (
  `id` int(10) NOT NULL auto_increment,
  `heading` varchar(100) default NULL,
  `author` varchar(45) default NULL,
  `date_entered` varchar(45) default NULL,
  `article` text,
  `contact` varchar(80) default NULL,
  `link` varchar(160) default NULL,
  `address` varchar(160) default NULL,
  `phone` varchar(80) default NULL,
  `parent_id` int(10) default NULL,
  `mime_type` varchar(50) default NULL,
  `summary` text,
  `numcomment` int(5) default NULL,
  `arttype` varchar(50) default NULL,
  `html_file` varchar(160) default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `linked_file` varchar(160) default NULL,
  `mirrored` enum('f','t') default NULL,
  `display` enum('f','t','g','l','r') default 't',
  `rating` decimal(9,2) default NULL,
  `artmime` enum('h','t') default NULL,
  `language_id` tinyint(3) unsigned default '1',
  PRIMARY KEY  (`id`),
  KEY `parent_id_index` (`parent_id`),
  KEY `display_index` (`display`),
  KEY `mime_type_index` (`mime_type`),
  KEY `display_parent_id_index` (`display`,`parent_id`),
  KEY `date_index` (`created`,`parent_id`),
  KEY `photo_gallery` (`mime_type`,`display`,`arttype`,`created`)
) ENGINE=myisam ;
        
        
DROP TABLE  indybay.legacy_sfactive_catlink;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_catlink (
  catid int(10) default NULL,
  id int(10) default NULL,
  KEY catid_id (catid,id),
  KEY id_catid (id,catid)
) ENGINE=MyISAM ;

DROP TABLE  indybay.legacy_sfactive_event;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_event (
  `event_id` int(11) NOT NULL auto_increment,
  `title` varchar(80) NOT NULL default '',
  `start_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `duration` int(11) default NULL,
  `location_id` int(11) default NULL,
  `location_other` varchar(60) default NULL,
  `location_details` text,
  `event_type_id` int(11) default NULL,
  `event_type_other` varchar(60) default NULL,
  `event_topic_id` int(11) default NULL,
  `event_topic_other` varchar(60) default NULL,
  `contact_name` varchar(60) default NULL,
  `contact_email` varchar(60) default NULL,
  `contact_phone` varchar(60) default NULL,
  `description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_by` tinyint(4) default NULL,
  `confirmation_number` int(11) default NULL,
  PRIMARY KEY  (`event_id`)
) ENGINE=MyISAM ;
        
        

DROP TABLE  indybay.legacy_sfactive_category;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_category (
  `name` varchar(200) default NULL,
  `order_num` int(5) default NULL,
  `category_id` int(11) NOT NULL auto_increment,
  `template_name` varchar(200) NOT NULL default '0',
  `creation_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creator_id` int(8) NOT NULL default '0',
  `default_feature_template_name` varchar(200) NOT NULL default '0',
  `shortname` varchar(20) default NULL,
  `summarylength` int(5) default NULL,
  `parentid` int(11) default NULL,
  `newswire` enum('n','s','a','f') default NULL,
  `center` enum('t','f') default NULL,
  `description` text,
  `catclass` enum('m','t','l','h','i') default NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM ;
        
        
DROP TABLE  indybay.legacy_sfactive_feature;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_feature (
  `feature_version_id` int(11) NOT NULL auto_increment,
  `feature_id` int(11) NOT NULL default '0',
  `summary` text,
  `title1` varchar(200) default NULL,
  `title2` varchar(200) default NULL,
  `display_date` varchar(100) default NULL,
  `order_num` int(5) default NULL,
  `category_id` int(5) default NULL,
  `template_name` varchar(200) NOT NULL default '0',
  `creator_id` int(8) NOT NULL default '0',
  `status` char(2) default 'c',
  `tag` varchar(100) default NULL,
  `image` varchar(100) default NULL,
  `version_num` int(5) default '1',
  `is_current_version` int(1) default '1',
  `modifier_id` int(11) default NULL,
  `image_link` varchar(200) default NULL,
  `modification_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creation_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `language_id` tinyint(3) unsigned default '1',
  `pushed_live` tinyint(1) unsigned default NULL,
  PRIMARY KEY  (`feature_version_id`),
  KEY `status_index` (`status`),
  KEY `is_current_version_index` (`is_current_version`),
  KEY `category_id_index` (`category_id`),
  KEY `feature_id_index` (`feature_id`),
  KEY `pushed_live` (`pushed_live`,`is_current_version`,`status`,`feature_id`)
) ENGINE=MyISAM ;
               

DROP TABLE indybay.legacy_sfactive_user;
CREATE TABLE IF NOT EXISTS indybay.legacy_sfactive_user (
  `username` varchar(50) NOT NULL default '',
  `password` varchar(32) default NULL,
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `phone` varchar(20) NOT NULL default '',
  `middle_name` varchar(20) NOT NULL default '',
  `user_id` int(8) NOT NULL auto_increment,
  PRIMARY KEY  (`user_id`),
  KEY `username_index` (`username`)
) ENGINE=MyISAM ;
        



insert into indybay.legacy_sfactive_user select * from indymedia.user;
insert into indybay.legacy_sfactive_category select * from indymedia.category;
insert into indybay.legacy_sfactive_feature select * from indymedia.feature;
insert into indybay.legacy_sfactive_event select * from indymedia.event;
insert into indybay.legacy_sfactive_catlink select * from indymedia.catlink;
insert into  indybay.legacy_sfactive_webcast select * from indymedia.webcast;
