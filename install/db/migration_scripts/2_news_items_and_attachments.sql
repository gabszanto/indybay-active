truncate data_migration_news_item;
truncate media_attachment;
truncate news_item;
truncate news_item_version;
truncate news_item_category;
truncate news_item_page_order;
truncate news_item_keyword;

insert into data_migration_news_item
(is_migrated, news_item_id, legacy_item_type_id,news_item_type_id,legacy_item_info_id,
legacy_item_id, old_cachefile_location,title1,title2,display_author,
email,phone,address,summary,text,upload_path,legacy_created_date,
legacy_updated_date,legacy_parent_item_id,legacy_display_status,mime_type,related_url,file_name )
select 0,(id*10)+1,1,1,(id*10)+1,id,null,heading,heading,author,trim(contact),phone,address,summary,article,
linked_file,created,created, parent_id,display,mime_type,link, REPLACE(linked_file,'/u1/indy/indybay/local/webcast/uploads/','')
from legacy_sfactive_webcast where arttype='webcast';

insert into data_migration_news_item
(is_migrated, news_item_id, legacy_item_type_id,news_item_type_id,legacy_item_info_id,
legacy_item_id, old_cachefile_location,title1,title2,display_author,
email,phone,address,summary,text,upload_path,legacy_created_date,
legacy_updated_date,legacy_parent_item_id,legacy_display_status,mime_type,related_url, file_name )
select 0,(id*10)+1,2,2,(id*10)+1,id,null,heading,heading,author,trim(contact),phone,address, summary,article,
linked_file,created,created, parent_id,display,mime_type,link, REPLACE(linked_file,'/u1/indy/indybay/local/webcast/uploads/','')
from legacy_sfactive_webcast where arttype='attachment'
;
 
insert into data_migration_news_item
(is_migrated, news_item_id, legacy_item_type_id, news_item_type_id, legacy_item_info_id,
legacy_item_id, old_cachefile_location,title1,title2,display_author,
email,phone,address,summary,text,upload_path,legacy_created_date,
legacy_updated_date,legacy_parent_item_id,legacy_display_status,mime_type,related_url,file_name )
select 0,(id*10)+1,3,3,(id*10)+1,id,null,heading,heading,author,trim(contact),phone,address,summary,article,
linked_file,created,created, parent_id,display,mime_type,link, REPLACE(linked_file,'/u1/indy/indybay/local/webcast/uploads/','')
from legacy_sfactive_webcast where arttype='news-response'
;
 
update data_migration_news_item
set is_text_html=1 where legacy_item_id in
(select id from legacy_sfactive_webcast where artmime='h');

update data_migration_news_item set news_item_status_id=13 where
legacy_display_status='t' and news_item_status_id is null;
update data_migration_news_item set news_item_status_id=17 where
legacy_display_status='f' and news_item_status_id is null;
update data_migration_news_item set news_item_status_id=11 where
legacy_display_status='r' and news_item_status_id is null;
update data_migration_news_item set news_item_status_id=5 where
legacy_display_status='g' and news_item_status_id is null;
update data_migration_news_item set news_item_status_id=3 where
legacy_display_status='l' and news_item_status_id is null;
update data_migration_news_item set news_item_status_id=2 where news_item_status_id=0;


update data_migration_news_item dm, media_type m set dm.media_type_id=m.media_type_id
where dm.mime_type=m.mime_type ;

insert into media_attachment (media_attachment_id, relative_path ,file_name,
media_type_id,
creation_date, upload_status_id, upload_type_id)
SELECT news_item_id, 'uploads/', 
file_name, media_type_id, legacy_created_date, 0, 1
FROM data_migration_news_item WHERE upload_path is not null and upload_path<>''
and media_attachment_id is NULL;

insert into news_item
(news_item_id, parent_item_id, creation_date, current_version_id, 
news_item_type_id,  news_item_status_id,media_type_grouping_id)
select
news_item_id, (legacy_parent_item_id*10)+1, legacy_created_date, news_item_id, 
news_item_type_id,  news_item_status_id,media_type_grouping_id
from data_migration_news_item;

update news_item set parent_item_id=news_item_id where parent_item_id=1;

insert into news_item_version (news_item_version_id,news_item_id,title1,title2,displayed_author_name, email,
 address, phone,
 displayed_date, summary,is_summary_html,text, is_text_html,related_url,
 display_contact_info ,media_attachment_id,version_creation_date)
 select news_item_id,news_item_id,title1,title2,display_author, email, address,phone,
 legacy_created_date, summary,is_summary_html ,text, is_text_html, related_url,
 1, news_item_id,legacy_created_date
 from data_migration_news_item;
 
 
truncate data_migration_news_item;

insert into data_migration_news_item
(is_migrated, news_item_id, legacy_item_type_id,news_item_type_id,legacy_item_info_id,
legacy_item_id, old_cachefile_location,title1,title2,display_author, 
email,phone,address,summary,text,upload_path,file_name,legacy_created_date,
legacy_updated_date,last_updated_by_user_id ,legacy_parent_item_id,related_url, event_start_time,
alt_tag )
select 0,(feature_id*10)+2,6,6,(feature_id*10)+2,feature_id,null,title2,title1,"","","",
"",summary,summary,image,image,creation_date, modification_date,modifier_id,0,image_link,  display_date,
tag
from legacy_sfactive_feature where is_current_version=1
and status!='h';

update data_migration_news_item set
file_name=REPLACE(file_name,'/imcenter/','');
update data_migration_news_item set
file_name=REPLACE(upload_path,'http://www.indybay.org/imcenter/','');
update data_migration_news_item set
upload_path=REPLACE(upload_path,'http://www.indybay.org','');

insert into news_item_page_order (news_item_id, page_id, order_num, display_option_id)
select (feature_id*10)+2, category_id, order_num, 0 from legacy_sfactive_feature 
where is_current_version=1 and status='c';

insert into news_item
(news_item_id, parent_item_id, creation_date, current_version_id, 
news_item_type_id,  news_item_status_id, created_by_id,media_type_grouping_id)
select
news_item_id, news_item_id, legacy_created_date, news_item_id, 
6,  2,last_updated_by_user_id,2
from data_migration_news_item;

insert into news_item_version (news_item_version_id,news_item_id,title1,title2,
	displayed_author_name, email, address, phone,
 displayed_date, summary,is_summary_html,text,is_text_html,related_url,
 display_contact_info ,media_attachment_id,thumbnail_media_attachment_id ,version_creation_date,
 version_created_by_id )
 select news_item_id,news_item_id,title1,title2,display_author, email, address,phone,
 event_start_time, summary,1 ,text, 1, related_url,
 1, news_item_id,news_item_id,legacy_created_date,last_updated_by_user_id
 from data_migration_news_item;
 
insert into news_item_category 
(news_item_id, category_id)
select (feature_id*10)+2, max(category_id) from legacy_sfactive_feature group by feature_id;

insert into media_attachment 
( 
media_attachment_id, upload_name, file_name, original_file_name ,
relative_path , alt_tag , media_type_id ,
upload_status_id, upload_type_id, 
external_source,  created_by_id, creation_date)
select news_item_id, file_name, file_name, file_name,
 "/imcenter/", alt_tag, 15, 
0 , 4, 
CONCAT('http://www.indybay.org/',file_name), last_updated_by_user_id , legacy_created_date
from data_migration_news_item where (file_name like '%.jpeg%' or file_name like '%.jpg%');

insert into media_attachment 
( 
media_attachment_id, upload_name, file_name, original_file_name ,
relative_path , alt_tag , media_type_id ,
upload_status_id, upload_type_id, 
external_source,  created_by_id, creation_date)
select news_item_id, file_name, file_name, file_name,
 "/imcenter/", alt_tag, 14, 
 0 , 4, 
CONCAT('http://www.indybay.org/',file_name), last_updated_by_user_id , legacy_created_date
from data_migration_news_item where (file_name like '%.gif%');


insert into news_item_category 
(news_item_id, category_id)
select (max(id)*10)+1, max(catid) from legacy_sfactive_catlink group by concat(catid,id);

update media_attachment
set external_source =
Concat('http://indybay.org/uploads/',file_name)
where relative_path="uploads/";

update news_item_version set media_attachment_id=0 where media_attachment_id
not in (select media_attachment_id from media_attachment) ;

update news_item_version set thumbnail_media_attachment_id =0 where thumbnail_media_attachment_id
not in (select media_attachment_id from media_attachment);

delete from news_item_version where news_item_id in
(select news_item_id from news_item where news_item_type_id=4);

delete from news_item_category where news_item_id in
(select news_item_id from news_item where news_item_type_id=4);

delete from news_item_keyword where news_item_id in
(select news_item_id from news_item where news_item_type_id=4);

delete from news_item where news_item_type_id=4;

insert into news_item
(news_item_id, parent_item_id, creation_date, current_version_id, 
news_item_type_id,  news_item_status_id, created_by_id)
select
(event_id*10)+3, (event_id*10)+3,  created_date , (event_id*10)+3, 
4,  13,0
from legacy_sfactive_event;

insert into news_item_version
(news_item_id, news_item_version_id, version_creation_date, 
summary,text, displayed_author_name,email, phone,
title1, title2, displayed_date, event_duration, is_text_html,  is_summary_html) 
select
(event_id*10)+3, (event_id*10)+3,  created_date , location_details,
description, contact_name,contact_email, contact_phone,
title, title, start_date, duration, 1, 1  
from legacy_sfactive_event;

insert into news_item_category (news_item_id, category_id)
select (event_id*10)+3,location_id from legacy_sfactive_event;

insert into news_item_category (news_item_id, category_id)
select (event_id*10)+3,event_topic_id from legacy_sfactive_event
where (event_id, event_topic_id) not in 
(select event_id, location_id from legacy_sfactive_event);

insert into news_item_keyword (news_item_id, keyword_id)
select (event_id*10)+3, event_type_id+20 from legacy_sfactive_event;


update media_attachment ma, news_item_version niv set ma.upload_name=Concat(Concat(left(title1,30),' by '),
 left(niv.displayed_author_name,30)) where niv.media_attachment_id=ma.media_attachment_id;


update news_item_page_order
set display_option_id=10
where news_item_id in ( select
feature_id*10+2 from legacy_sfactive_feature where template_name='html_only');

update news_item ni, media_attachment ma,
news_item_version niv, media_type mt set
ni.media_type_grouping_id=mt.media_type_grouping_id
where ma.media_attachment_id=niv.media_attachment_id
and ni.current_version_id=niv.news_item_version_id and
mt.media_type_id=ma.media_type_id;

update news_item set news_item_status_id=3 where news_item_id in ( select news_item_id from news_item_version where displayed_date>now());

insert into news_item_fulltext (news_item_id, normalized_text)
select ni.news_item_id, Concat(niv.title1,' ',niv.title2,' ',displayed_author_name,' ',email,' ',summary,' ',text)
from news_item ni, news_item_version niv
where ni.current_version_id=niv.news_item_version_id


