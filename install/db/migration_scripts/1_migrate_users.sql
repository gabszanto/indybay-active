truncate user;
truncate contact_info;
truncate user_password;
truncate user_group;

insert into user (user_id, username,has_login_rights,contact_info_id) select user_id, username,1,user_id from legacy_sfactive_user;

insert into user_password (user_id, password) select user_id,password from legacy_sfactive_user;

update user set has_login_rights=0 where username like '%DISABLED%';

INSERT INTO contact_info ( contact_info_id, first_name, last_name, email, phone )
SELECT 
user_id, first_name, last_name, email, phone from legacy_sfactive_user;

insert into user_group (user_id, group_id, is_group_admin) select user_id, 10,1 from user where has_login_rights=1;