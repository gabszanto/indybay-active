SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `action` (
  `action_id` smallint(11) unsigned NOT NULL default '0',
  `action_type_id` tinyint(11) unsigned NOT NULL default '0',
  `classname` varchar(128) NOT NULL default '',
  `methodname` varchar(128) NOT NULL default '',
  `url` varchar(128) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `action_type` (
  `action_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`action_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `category` (
  `category_id` smallint(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL default '',
  `category_type_id` tinyint(11) unsigned NOT NULL default '0',
  `parent_category_id` smallint(11) unsigned default '0',
  `include_in_dropdowns` tinyint(4) NOT NULL default '1',
  `created_by_id` smallint(11) unsigned NOT NULL default '0',
  `creation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_modification_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `status` tinyint(1) default '1',
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `name` (`name`),
  KEY `category_type_id` (`category_type_id`),
  KEY `include_in_dropdowns` (`include_in_dropdowns`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `category_type` (
  `category_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`category_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `center_column_type` (
  `center_column_type_id` tinyint(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`center_column_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `city` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(31) default NULL,
  `county` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `contact_info` (
  `contact_info_id` mediumint(11) unsigned NOT NULL auto_increment,
  `first_name` varchar(200) default NULL,
  `last_name` varchar(200) default NULL,
  `phone` varchar(64) default NULL,
  `cellphone` varchar(64) default NULL,
  `email` varchar(64) default NULL,
  `url` varchar(200) default NULL,
  `last_modified_by_id` smallint(11) unsigned NOT NULL default '0',
  `last_modification_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`contact_info_id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `country` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(31) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `county` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(31) default NULL,
  `state` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `data_migration_news_item` (
  `is_migrated` smallint(6) NOT NULL default '0',
  `news_item_id` int(11) default NULL,
  `legacy_item_type_id` int(11) NOT NULL default '0',
  `legacy_item_id` int(11) NOT NULL default '0',
  `legacy_item_info_id` int(11) default NULL,
  `old_cachefile_location` varchar(128) default NULL,
  `title1` varchar(128) NOT NULL default '',
  `title2` varchar(128) default NULL,
  `display_author` varchar(128) default NULL,
  `last_updated_by_user_id` int(11) default NULL,
  `email` varchar(128) default NULL,
  `phone` varchar(64) default NULL,
  `address` varchar(200) NOT NULL default '',
  `summary` text,
  `is_summary_html` smallint(6) NOT NULL default '0',
  `text` text,
  `is_text_html` smallint(6) NOT NULL default '0',
  `upload_path` varchar(64) default NULL,
  `file_name` varchar(128) default NULL,
  `thumbnail_path` varchar(128) default NULL,
  `mime_type` varchar(64) default NULL,
  `media_type_id` int(11) default NULL,
  `media_attachment_id` int(11) default NULL,
  `legacy_created_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `legacy_updated_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `legacy_event_type_id` int(11) default NULL,
  `event_type_id` int(11) default NULL,
  `event_start_time` datetime default NULL,
  `event_end_time` datetime default NULL,
  `legacy_template_name` varchar(128) default NULL,
  `template_id` int(11) default NULL,
  `legacy_display_status` varchar(4) default NULL,
  `news_item_status_id` int(11) default NULL,
  `news_item_type_id` int(11) default NULL,
  `media_attachment_alt_tag` varchar(64) default NULL,
  `legacy_parent_item_id` int(11) default NULL,
  `parent_news_item_id` int(11) NOT NULL default '0',
  `related_url` varchar(128) default NULL,
  `alt_tag` varchar(200) NOT NULL default '',
  `contact_info_id` int(11) default NULL,
  `media_type_grouping_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`legacy_item_id`),
  KEY `file_name` (`file_name`),
  KEY `legacy_item_id` (`legacy_item_id`),
  KEY `news_item_status_id` (`news_item_status_id`),
  KEY `legacy_item_type_id` (`legacy_item_type_id`),
  KEY `mime_type` (`mime_type`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `deposit` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `location` smallint(5) unsigned default NULL,
  `quantity` smallint(6) default NULL,
  `distributor` varchar(31) default NULL,
  `issue` tinyint(3) unsigned default NULL,
  `comment` varchar(255) default NULL,
  `updated` datetime default NULL,
  `created` datetime default NULL,
  `dropdate` date default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=670 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `event_list_type` (
  `event_list_type_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY  (`event_list_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `group` (
  `group_id` smallint(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `group_action` (
  `group_id` smallint(11) unsigned NOT NULL default '0',
  `action_id` smallint(11) unsigned NOT NULL default '0',
  `restrict_to_category_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`group_id`,`action_id`,`restrict_to_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `group_relationship` (
  `inherit_permission_from_group_id` smallint(11) unsigned NOT NULL default '0',
  `child_group_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`inherit_permission_from_group_id`,`child_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `inbound_rss_feed_items` (
  `rss_item_id` int(11) NOT NULL auto_increment,
  `rss_feed_id` int(11) NOT NULL,
  `author` varchar(64) collate utf8_unicode_ci NOT NULL,
  `title` varchar(256) collate utf8_unicode_ci NOT NULL,
  `summary` varchar(4000) collate utf8_unicode_ci NOT NULL,
  `text` mediumtext collate utf8_unicode_ci NOT NULL,
  `related_url` varchar(200) collate utf8_unicode_ci NOT NULL,
  `is_text_html` tinyint(1) NOT NULL,
  `creation_date` datetime NOT NULL,
  `pull_date` datetime NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `external_unique_id` varchar(512) collate utf8_unicode_ci NOT NULL,
  `categories` varchar(64) collate utf8_unicode_ci NOT NULL,
  `news_item_status_id` int(11) NOT NULL,
  `parent_news_item_id` int(11) NOT NULL,
  `published_news_item_id` int(11) NOT NULL,
  PRIMARY KEY  (`rss_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85398 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `inbound_rss_feeds` (
  `rss_feed_id` int(11) NOT NULL auto_increment,
  `name` varchar(64) collate utf8_unicode_ci NOT NULL,
  `url` varchar(512) collate utf8_unicode_ci NOT NULL,
  `feed_format_id` int(11) NOT NULL,
  `author_template` varchar(100) collate utf8_unicode_ci NOT NULL,
  `summary_template` varchar(600) collate utf8_unicode_ci NOT NULL,
  `text_template` varchar(600) collate utf8_unicode_ci NOT NULL,
  `text_scrape_start` varchar(512) collate utf8_unicode_ci NOT NULL,
  `text_scrape_end` varchar(512) collate utf8_unicode_ci NOT NULL,
  `default_region_id` int(11) NOT NULL,
  `default_topic_id` int(11) NOT NULL,
  `default_status_id` int(11) NOT NULL,
  `replace_url` varchar(256) collate utf8_unicode_ci NOT NULL,
  `replace_url_with` varchar(256) collate utf8_unicode_ci NOT NULL,
  `summary_scrape_start` varchar(512) collate utf8_unicode_ci NOT NULL,
  `summary_scrape_end` varchar(512) collate utf8_unicode_ci NOT NULL,
  `author_scrape_start` varchar(512) collate utf8_unicode_ci NOT NULL,
  `author_scrape_end` varchar(512) collate utf8_unicode_ci NOT NULL,
  `last_pull_date` datetime NOT NULL,
  `last_pull_duration_usecs` int(11) NOT NULL,
  `title_scrape_start` varchar(512) collate utf8_unicode_ci NOT NULL,
  `title_scrape_end` varchar(512) collate utf8_unicode_ci NOT NULL,
  `date_scrape_start` varchar(512) collate utf8_unicode_ci NOT NULL,
  `date_scrape_end` varchar(512) collate utf8_unicode_ci NOT NULL,
  `exclude_tags` varchar(512) collate utf8_unicode_ci NOT NULL,
  `required_tags` varchar(512) collate utf8_unicode_ci NOT NULL,
  `max_to_pull` int(11) NOT NULL,
  `max_pull_days` int(11) NOT NULL,
  `restrict_urls` varchar(512) collate utf8_unicode_ci NOT NULL,
  `allow_links` tinyint(1) NOT NULL,
  PRIMARY KEY  (`rss_feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `issue` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `released` date default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `keyword` (
  `keyword_id` smallint(11) unsigned NOT NULL default '0',
  `keyword` varchar(64) NOT NULL default '',
  `keyword_type_id` tinyint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`keyword_id`),
  KEY `keyword_type_id` (`keyword_type_id`),
  KEY `keyword` (`keyword`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `keyword_type` (
  `keyword_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`keyword_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_category` (
  `name` varchar(200) default NULL,
  `order_num` int(5) default NULL,
  `category_id` int(11) NOT NULL auto_increment,
  `template_name` varchar(200) NOT NULL default '0',
  `creation_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creator_id` int(8) NOT NULL default '0',
  `default_feature_template_name` varchar(200) NOT NULL default '0',
  `shortname` varchar(20) default NULL,
  `summarylength` int(5) default NULL,
  `parentid` int(11) default NULL,
  `newswire` enum('n','s','a','f') default NULL,
  `center` enum('t','f') default NULL,
  `description` text,
  `catclass` enum('m','t','l','h','i') default NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_catlink` (
  `catid` int(10) default NULL,
  `id` int(10) default NULL,
  KEY `catid_id` (`catid`,`id`),
  KEY `id_catid` (`id`,`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_event` (
  `event_id` int(11) NOT NULL auto_increment,
  `title` varchar(80) NOT NULL default '',
  `start_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `duration` int(11) default NULL,
  `location_id` int(11) default NULL,
  `location_other` varchar(60) default NULL,
  `location_details` text,
  `event_type_id` int(11) default NULL,
  `event_type_other` varchar(60) default NULL,
  `event_topic_id` int(11) default NULL,
  `event_topic_other` varchar(60) default NULL,
  `contact_name` varchar(60) default NULL,
  `contact_email` varchar(60) default NULL,
  `contact_phone` varchar(60) default NULL,
  `description` text,
  `created_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created_by` tinyint(4) default NULL,
  `confirmation_number` int(11) default NULL,
  PRIMARY KEY  (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_feature` (
  `feature_version_id` int(11) NOT NULL auto_increment,
  `feature_id` int(11) NOT NULL default '0',
  `summary` text,
  `title1` varchar(200) default NULL,
  `title2` varchar(200) default NULL,
  `display_date` varchar(100) default NULL,
  `order_num` int(5) default NULL,
  `category_id` int(5) default NULL,
  `template_name` varchar(200) NOT NULL default '0',
  `creator_id` int(8) NOT NULL default '0',
  `status` char(2) default 'c',
  `tag` varchar(100) default NULL,
  `image` varchar(100) default NULL,
  `version_num` int(5) default '1',
  `is_current_version` int(1) default '1',
  `modifier_id` int(11) default NULL,
  `image_link` varchar(200) default NULL,
  `modification_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `creation_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `language_id` tinyint(3) unsigned default '1',
  `pushed_live` tinyint(1) unsigned default NULL,
  PRIMARY KEY  (`feature_version_id`),
  KEY `status_index` (`status`),
  KEY `is_current_version_index` (`is_current_version`),
  KEY `category_id_index` (`category_id`),
  KEY `feature_id_index` (`feature_id`),
  KEY `pushed_live` (`pushed_live`,`is_current_version`,`status`,`feature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_user` (
  `username` varchar(50) NOT NULL default '',
  `password` varchar(32) default NULL,
  `first_name` varchar(50) NOT NULL default '',
  `last_name` varchar(50) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `phone` varchar(20) NOT NULL default '',
  `middle_name` varchar(20) NOT NULL default '',
  `user_id` int(8) NOT NULL auto_increment,
  PRIMARY KEY  (`user_id`),
  KEY `username_index` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_sfactive_webcast` (
  `id` int(10) NOT NULL auto_increment,
  `heading` varchar(100) default NULL,
  `author` varchar(45) default NULL,
  `date_entered` varchar(45) default NULL,
  `article` text,
  `contact` varchar(80) default NULL,
  `link` varchar(160) default NULL,
  `address` varchar(160) default NULL,
  `phone` varchar(80) default NULL,
  `parent_id` int(10) default NULL,
  `mime_type` varchar(50) default NULL,
  `summary` text,
  `numcomment` int(5) default NULL,
  `arttype` varchar(50) default NULL,
  `html_file` varchar(160) default NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `linked_file` varchar(160) default NULL,
  `mirrored` enum('f','t') default NULL,
  `display` enum('f','t','g','l','r') default 't',
  `rating` decimal(9,2) default NULL,
  `artmime` enum('h','t') default NULL,
  `language_id` tinyint(3) unsigned default '1',
  PRIMARY KEY  (`id`),
  KEY `parent_id_index` (`parent_id`),
  KEY `display_index` (`display`),
  KEY `mime_type_index` (`mime_type`),
  KEY `display_parent_id_index` (`display`,`parent_id`),
  KEY `date_index` (`created`,`parent_id`),
  KEY `photo_gallery` (`mime_type`,`display`,`arttype`,`created`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `legacy_users` (
  `uid` int(5) NOT NULL default '0',
  `login` varchar(10) default NULL,
  `pass` varchar(20) default NULL,
  `email` varchar(30) default NULL,
  `host_ip` varchar(15) default NULL,
  `phone` varchar(15) default NULL,
  `address0` varchar(30) default NULL,
  `address1` varchar(30) default NULL,
  `state` varchar(20) default NULL,
  `post_code` varchar(20) default NULL,
  `country` varchar(20) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `location` (
  `location_id` int(11) NOT NULL default '0',
  `name` varchar(40) default NULL,
  `description` text,
  `url` varchar(100) default NULL,
  PRIMARY KEY  (`location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL auto_increment,
  `start_time` datetime NOT NULL,
  `ip` varchar(20) default NULL,
  `ip_part1` tinyint(3) unsigned NOT NULL,
  `ip_part2` tinyint(3) unsigned NOT NULL,
  `ip_part3` tinyint(3) unsigned NOT NULL,
  `ip_part4` tinyint(3) unsigned NOT NULL,
  `http_method` varchar(9) NOT NULL,
  `url` varchar(512) NOT NULL,
  `referring_url` varchar(512) NOT NULL,
  `news_item_id` int(11) default NULL,
  `parent_news_item_id` int(11) default NULL,
  `full_headers` text NOT NULL,
  `end_time` datetime default NULL,
  PRIMARY KEY  (`log_id`),
  KEY `ip` (`ip`),
  KEY `ip_part1` (`ip_part1`,`ip_part2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `media_attachment` (
  `media_attachment_id` int(11) unsigned NOT NULL auto_increment,
  `upload_name` varchar(128) NOT NULL default '',
  `file_name` varchar(128) NOT NULL default '',
  `original_file_name` varchar(64) NOT NULL default '',
  `relative_path` varchar(128) NOT NULL default '',
  `alt_tag` varchar(200) default NULL,
  `media_type_id` tinyint(11) unsigned NOT NULL default '0',
  `upload_status_id` smallint(6) NOT NULL default '0',
  `upload_type_id` smallint(6) NOT NULL default '0',
  `file_size` int(11) NOT NULL default '0',
  `image_width` smallint(6) NOT NULL default '0',
  `image_height` smallint(6) NOT NULL default '0',
  `parent_attachment_id` int(11) unsigned NOT NULL default '0',
  `external_source` varchar(200) default NULL,
  `created_by_id` smallint(11) unsigned NOT NULL default '0',
  `creation_date` datetime default '0000-00-00 00:00:00',
  PRIMARY KEY  (`media_attachment_id`),
  KEY `file_name` (`original_file_name`),
  KEY `upload_path` (`relative_path`),
  KEY `media_type_id` (`media_type_id`),
  KEY `media_attachment_id` (`media_attachment_id`),
  KEY `parent_attachment_id` (`parent_attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18457445 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `media_type` (
  `media_type_id` smallint(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) default NULL,
  `extension` varchar(16) NOT NULL default '',
  `description` varchar(128) default NULL,
  `mime_type` varchar(64) NOT NULL default '',
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`media_type_id`),
  KEY `mime_type` (`mime_type`),
  KEY `media_type_id` (`media_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `media_type_grouping` (
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(128) NOT NULL default '',
  PRIMARY KEY  (`media_type_grouping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item` (
  `news_item_id` int(11) unsigned NOT NULL auto_increment,
  `parent_item_id` int(11) unsigned default NULL,
  `creation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `created_by_id` smallint(11) unsigned NOT NULL default '0',
  `current_version_id` int(11) unsigned default NULL,
  `news_item_type_id` tinyint(11) unsigned NOT NULL default '0',
  `news_item_status_id` tinyint(11) unsigned NOT NULL default '0',
  `num_comments` smallint(11) unsigned NOT NULL default '0',
  `media_type_grouping_id` tinyint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_item_id`),
  KEY `creation_date` (`creation_date`,`created_by_id`),
  KEY `news_item_type_id` (`news_item_type_id`),
  KEY `current_version_id` (`current_version_id`),
  KEY `news_item_status_id` (`news_item_status_id`),
  KEY `parent_item_id` (`parent_item_id`),
  KEY `media_type_grouping_id` (`media_type_grouping_id`),
  KEY `media_type_status_index` (`media_type_grouping_id`,`news_item_type_id`,`news_item_status_id`),
  KEY `newswire_index` (`news_item_status_id`,`news_item_type_id`),
  KEY `mediawire_index` (`news_item_status_id`,`news_item_type_id`,`media_type_grouping_id`),
  KEY `media_type_status_id` (`media_type_grouping_id`,`news_item_type_id`,`news_item_status_id`,`news_item_id`),
  KEY `media_status_type_id` (`media_type_grouping_id`,`news_item_status_id`,`news_item_type_id`,`news_item_id`),
  KEY `id_mediawire_index` (`news_item_id`,`news_item_status_id`,`news_item_type_id`,`media_type_grouping_id`),
  KEY `id_newswire_index` (`news_item_id`,`news_item_status_id`,`news_item_type_id`),
  KEY `news_item_id` (`news_item_id`,`current_version_id`,`news_item_type_id`,`parent_item_id`,`news_item_status_id`,`creation_date`,`num_comments`,`media_type_grouping_id`),
  KEY `news_item_id_2` (`news_item_id`,`current_version_id`,`news_item_status_id`,`news_item_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18509576 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_category` (
  `news_item_id` int(11) unsigned NOT NULL default '0',
  `category_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_item_id`,`category_id`),
  KEY `category_id` (`category_id`,`news_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_fulltext` (
  `news_item_id` int(11) NOT NULL,
  `normalized_text` text,
  PRIMARY KEY  (`news_item_id`),
  FULLTEXT KEY `normalized_text` (`normalized_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_keyword` (
  `news_item_id` int(11) unsigned NOT NULL default '0',
  `keyword_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_item_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_page_order` (
  `news_item_id` int(11) unsigned NOT NULL default '0',
  `page_id` smallint(11) unsigned NOT NULL default '0',
  `order_num` smallint(11) unsigned NOT NULL default '0',
  `display_option_id` tinyint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_item_id`,`page_id`),
  KEY `ordernum` (`order_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_status` (
  `news_item_status_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  `sort_order` tinyint(3) unsigned NOT NULL default '0',
  `show_in_user_search` binary(1) NOT NULL default '\0',
  `is_hidden` binary(1) NOT NULL default '0',
  `is_highlighted` binary(1) NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_type` (
  `news_item_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`news_item_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `news_item_version` (
  `news_item_version_id` int(11) unsigned NOT NULL auto_increment,
  `news_item_id` int(11) unsigned NOT NULL default '0',
  `title1` varchar(140) NOT NULL default '',
  `title2` varchar(200) default NULL,
  `displayed_author_name` varchar(64) default NULL,
  `email` varchar(128) default NULL,
  `phone` varchar(64) default NULL,
  `address` varchar(200) default NULL,
  `displayed_date` datetime default NULL,
  `event_duration` float NOT NULL default '0',
  `summary` text,
  `text` text NOT NULL,
  `is_summary_html` tinyint(4) NOT NULL default '0',
  `is_text_html` binary(1) NOT NULL default '0',
  `related_url` varchar(200) default NULL,
  `display_contact_info` binary(1) NOT NULL default '0',
  `media_attachment_id` int(11) unsigned default NULL,
  `thumbnail_media_attachment_id` int(11) unsigned default '0',
  `version_creation_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `version_created_by_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`news_item_version_id`),
  KEY `displayed_date` (`displayed_date`),
  KEY `news_item_id` (`news_item_id`),
  KEY `media_attachment_id` (`media_attachment_id`),
  KEY `thumbnail_media_attachment_id` (`thumbnail_media_attachment_id`),
  KEY `news_item_id_3` (`news_item_id`,`news_item_version_id`,`title1`,`displayed_author_name`),
  KEY `displayed_date_2` (`displayed_date`,`news_item_version_id`,`title1`,`news_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18540755 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `newswire_type` (
  `newswire_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`newswire_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `page` (
  `page_id` smallint(11) unsigned NOT NULL default '0',
  `long_display_name` varchar(128) NOT NULL default '',
  `short_display_name` varchar(16) NOT NULL default '',
  `css_news_item_id` int(11) NOT NULL,
  `banner_section_news_item_id` int(11) default NULL,
  `overrided_banner_image_src` varchar(128) NOT NULL,
  `center_column_type_id` tinyint(11) unsigned NOT NULL default '0',
  `newswire_type_id` tinyint(11) unsigned NOT NULL default '0',
  `event_list_type_id` tinyint(3) unsigned NOT NULL default '0',
  `max_events_in_highlighted_list` int(11) NOT NULL,
  `max_timespan_in_days_for_highlighted_event_list` int(11) NOT NULL,
  `max_events_in_full_event_list` int(11) NOT NULL,
  `max_timespan_in_days_for_full_event_list` int(11) default NULL,
  `header1_news_item_id` int(11) NOT NULL,
  `header2_news_item_id` int(11) NOT NULL,
  `footer1_news_item_id` int(11) NOT NULL,
  `footer2_news_item_id` int(11) NOT NULL,
  `items_per_newswire_section` smallint(11) unsigned NOT NULL default '0',
  `relative_path` varchar(64) NOT NULL default '',
  `include_in_readmore_links` binary(1) NOT NULL default '1',
  `created_by_id` smallint(11) unsigned NOT NULL default '0',
  `creation_date` datetime default '0000-00-00 00:00:00',
  `modified_by_id` smallint(11) unsigned NOT NULL default '0',
  `modification_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `last_pushed_date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`page_id`),
  KEY `include_in_readmore_links` (`include_in_readmore_links`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `page_category` (
  `page_id` smallint(11) unsigned NOT NULL default '0',
  `category_id` smallint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`page_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `spot` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(63) default NULL,
  `address` varchar(63) default NULL,
  `city` tinyint(3) unsigned default NULL,
  `zip` mediumint(5) unsigned zerofill default NULL,
  `contact` varchar(63) default NULL,
  `phone` varchar(31) default NULL,
  `email` varchar(63) default NULL,
  `comment` varchar(255) default NULL,
  `updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `url` tinytext,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=327 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `state` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(31) default NULL,
  `abbrev` char(3) default NULL,
  `country` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `subscribers` (
  `id` smallint(6) NOT NULL auto_increment,
  `name` varchar(63) default NULL,
  `organization` varchar(63) default NULL,
  `mailing_address` varchar(63) default NULL,
  `mailing_city` varchar(63) default NULL,
  `mailing_state` varchar(5) default NULL,
  `mailing_zip` varchar(10) default NULL,
  `mailing_country` varchar(31) default 'USA',
  `billing_address` varchar(63) default NULL,
  `billing_city` varchar(63) default NULL,
  `billing_state` varchar(5) default NULL,
  `billing_zip` varchar(10) default NULL,
  `billing_country` varchar(31) default NULL,
  `phone` varchar(31) default NULL,
  `fax` varchar(31) default NULL,
  `email` varchar(63) default NULL,
  `website` varchar(63) default NULL,
  `status` enum('subscribed','unsubscribed','complimentary') default NULL,
  `start_date` date default NULL,
  `rate` varchar(63) default NULL,
  `end_date` date default NULL,
  `cancellation_reason` varchar(255) default NULL,
  `comment` varchar(255) default NULL,
  `updated` datetime default NULL,
  `created` datetime default NULL,
  `copies` smallint(6) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `system_variables` (
  `system_var_id` smallint(11) NOT NULL auto_increment,
  `key1` varchar(64) NOT NULL default '',
  `key2` varchar(64) default NULL,
  `key3` varchar(64) default NULL,
  `int_val` int(11) NOT NULL default '0',
  `string_val` text NOT NULL,
  `time_val` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`system_var_id`),
  KEY `key1` (`key1`,`key2`,`key3`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `template` (
  `template_id` smallint(11) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL default '',
  `html` text NOT NULL,
  `template_type_id` tinyint(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `template_type` (
  `template_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `testing` (
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `upload_type` (
  `upload_type_id` tinyint(11) unsigned NOT NULL default '0',
  `name` varchar(64) NOT NULL default '',
  PRIMARY KEY  (`upload_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(64) NOT NULL default '',
  `last_login_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_admin_activity` datetime NOT NULL default '0000-00-00 00:00:00',
  `last_failed_login_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `num_failed_tries_since_last_login` int(11) NOT NULL default '0',
  `has_login_rights` tinyint(4) NOT NULL default '0',
  `contact_info_id` int(11) NOT NULL default '0',
  `created_by_id` int(11) NOT NULL default '0',
  `creation_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_group` (
  `user_id` smallint(11) unsigned NOT NULL default '0',
  `group_id` smallint(11) unsigned NOT NULL default '0',
  `is_group_admin` binary(1) NOT NULL default '0',
  PRIMARY KEY  (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_log` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `user_id` smallint(11) unsigned NOT NULL default '0',
  `action_id` smallint(11) unsigned default NULL,
  `additional_info` text NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `associated_object_id` int(11) unsigned default NULL,
  PRIMARY KEY  (`log_id`),
  KEY `associated_object_id` (`associated_object_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_password` (
  `user_id` smallint(11) unsigned NOT NULL default '0',
  `password` varchar(200) NOT NULL default '',
  `temp_password` varchar(200) NOT NULL default '',
  `temp_password_expiration` datetime NOT NULL default '0000-00-00 00:00:00',
  `changed` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;
