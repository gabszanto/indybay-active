-- phpMyAdmin SQL Dump
-- version 2.6.0-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jan 09, 2006 at 07:24 PM
-- Server version: 5.0.18
-- PHP Version: 5.1.1
-- 
-- Database: `indybay`
-- 

-- 
-- Dumping data for table `action`
-- 


-- 
-- Dumping data for table `action_type`
-- 

INSERT INTO `action_type` (`action_type_id`, `name`) VALUES (1, 'admin_display');
INSERT INTO `action_type` (`action_type_id`, `name`) VALUES (2, 'admin_update');
INSERT INTO `action_type` (`action_type_id`, `name`) VALUES (4, 'admin_delete');
INSERT INTO `action_type` (`action_type_id`, `name`) VALUES (3, 'admin_add');
INSERT INTO `action_type` (`action_type_id`, `name`) VALUES (0, 'user_update');

-- 
-- Dumping data for table `category`
-- 

INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (16, 'Health, Housing, and Public Services', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (15, 'Racial Justice', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (14, 'Environment & Forest Defense', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (12, 'Front Page', 3, NULL, 1, 0, '2005-12-11 11:25:45', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (13, 'Police State', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (18, 'Anti-War and Militarism', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (19, 'Labor & Workers', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (48, 'Iraq', 2, 44, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (49, 'Palestine', 2, 44, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (50, 'Haiti', 2, 44, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (51, 'Afghanistan', 2, 44, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (22, 'Global Justice and Anti-Capitalism', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (27, 'Drug War', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (28, 'legal support', 2, NULL, 0, 0, '2005-12-11 11:40:41', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (29, 'LGBTI / Queer', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (30, 'Education & Student Activism', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (31, 'Womyn', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (32, 'Independent Media', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (33, 'En Espa&ntilde;ol', 2, NULL, 1, 0, '2005-12-11 11:41:32', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (34, 'Arts + Action', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (35, 'Central Valley', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (36, 'City of San Francisco', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (37, 'South Bay Area', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (38, 'East Bay Area', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (39, 'Peninsula', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (40, 'North Bay / Marin', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (41, 'North Coast', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (42, 'California', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (43, 'U.S.', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (44, 'International', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (45, 'Government & Elections', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (53, 'Americas', 2, 44, 1, 0, '2005-12-11 11:38:35', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (54, 'Fault Lines', 2, NULL, 0, 0, '2005-12-11 11:38:18', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (56, 'Immigrant Rights', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (58, 'Animal Liberation', 2, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);
INSERT INTO `category` (`category_id`, `name`, `category_type_id`, `parent_category_id`, `include_in_dropdowns`, `created_by_id`, `creation_date`, `last_modification_date`) VALUES (60, 'Santa Cruz', 1, NULL, 1, 0, '2005-12-10 20:02:10', 0x303030302d30302d30302030303a30303a3030);

-- 
-- Dumping data for table `category_type`
-- 

INSERT INTO `category_type` (`category_type_id`, `name`) VALUES (1, 'region');
INSERT INTO `category_type` (`category_type_id`, `name`) VALUES (2, 'topic');
INSERT INTO `category_type` (`category_type_id`, `name`) VALUES (3, 'other');

-- 
-- Dumping data for table `center_column_type`
-- 

INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (1, 'long versions (alternating sides)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (2, 'long versions (images on left)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (3, 'long versions (images on right)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (4, 'short versions (alternating)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (5, 'short versions (images on left)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (6, 'short versions (images on right)');
INSERT INTO `center_column_type` (`center_column_type_id`, `name`) VALUES (7, 'no center column');

-- 
-- Dumping data for table `group`
-- 

INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (2, 'admin_news_items', '');
INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (3, 'admin_pages', '');
INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (4, 'admin_user', '');
INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (5, 'admin_login', '');
INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (6, 'user_edit_news_item', '');
INSERT INTO `group` (`group_id`, `name`, `description`) VALUES (10, 'bayarea_indybay_admin', '');

-- 
-- Dumping data for table `group_action`
-- 


-- 
-- Dumping data for table `group_relationship`
-- 

INSERT INTO `group_relationship` (`inherit_permission_from_group_id`, `child_group_id`) VALUES (2, 10);
INSERT INTO `group_relationship` (`inherit_permission_from_group_id`, `child_group_id`) VALUES (3, 10);
INSERT INTO `group_relationship` (`inherit_permission_from_group_id`, `child_group_id`) VALUES (4, 10);
INSERT INTO `group_relationship` (`inherit_permission_from_group_id`, `child_group_id`) VALUES (5, 10);

-- 
-- Dumping data for table `keyword`
-- 

INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (20, 'Other', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (21, 'Meeting', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (22, 'Teach-In', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (23, 'Screening', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (24, 'Vigil/Ritual', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (25, 'Training', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (26, 'Conference', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (27, 'Press Conference', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (28, 'Concert/Show', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (29, 'Panel Discussion', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (30, 'Critical Mass', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (31, 'Protest', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (32, 'Radio Broadcast', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (33, 'Speaker', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (34, 'Fundraiser', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (35, 'Party/Street Party', 1);
INSERT INTO `keyword` (`keyword_id`, `keyword`, `keyword_type_id`) VALUES (36, 'Court Date', 1);

-- 
-- Dumping data for table `keyword_type`
-- 

INSERT INTO `keyword_type` (`keyword_type_id`, `name`) VALUES (1, 'event type');

-- 
-- Dumping data for table `media_type`
-- 

INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (2, 'pdf', 'pdf', NULL, 'application/pdf', 5);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (3, 'smil', 'smil', NULL, 'application/smil', 7);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (4, 'bittorrent', 'bittorrent', NULL, 'application/x-bittorrent', 7);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (5, 'flash', 'flash', NULL, 'application/x-shockwave-flash', 7);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (6, 'mpeg audio', 'mpeg', NULL, 'audio/mpeg', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (7, 'mpeg audio link', 'mpeg', NULL, 'audio/x-mpegurl', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (8, 'wma', 'wma', NULL, 'audio/x-ms-wma', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (9, 'ogg', 'ogg', NULL, 'audio/x-ogg', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (10, 'Real Audio', 'rm', NULL, 'audio/x-pn-realaudio', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (11, 'Real Audio Link', 'ram', NULL, 'audio/x-pn-realaudio-meta', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (12, 'scpls', 'scpls', NULL, 'audio/x-scpls', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (13, 'wav', 'wav', NULL, 'audio/x-wav', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (14, 'gif', 'gif', NULL, 'image/gif', 2);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (15, 'jpeg', 'jpeg', NULL, 'image/jpeg', 2);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (16, 'png', 'png', NULL, 'image/png', 2);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (19, 'mpeg video', 'mpeg', NULL, 'video/mpeg', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (20, 'quicktime', 'mov', NULL, 'video/quicktime', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (21, 'wmv', 'wmv', NULL, 'video/x-ms-wmv', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (22, 'msvideo', 'msvideo', NULL, 'video/x-msvideo', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (23, 'Real Video', 'rm', NULL, 'video/x-pn-realvideo', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (24, 'Real Video Link', 'ram', NULL, 'video/x-pn-realvideo-meta', 4);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (25, 'jpeg', 'jpg', NULL, 'image/jpeg', 2);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (27, 'mp3', 'mp3', NULL, 'audio/mpeg', 3);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (28, 'html', 'html', NULL, 'text/html', 0);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (29, 'text', 'txt', NULL, 'text/plain', 0);
INSERT INTO `media_type` (`media_type_id`, `name`, `extension`, `description`, `mime_type`, `media_type_grouping_id`) VALUES (30, 'mpeg video', 'mp4', NULL, 'video/mp4', 4);
             
-- 
-- Dumping data for table `media_type_grouping`
-- 


INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (2, 'image');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (3, 'audio');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (4, 'video');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (5, 'pdf & other document types');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (7, 'flash & other applications');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (0, 'none');
INSERT INTO `media_type_grouping` (`media_type_grouping_id`, `name`) VALUES (1, 'text');
        

-- 
-- Dumping data for table `news_item_status`
-- 

INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (2, 'New', 1, 0x31, 0x30, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (3, 'Highlighted-Local', 3, 0x31, 0x30, 0x31);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (5, 'Highlighted-NonLocal', 4, 0x31, 0x30, 0x31);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (7, 'Corp Repost-Local', 5, 0x30, 0x30, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (11, 'Corp Report-NonLocal', 6, 0x30, 0x30, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (13, 'Other', 7, 0x30, 0x30, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (17, 'Hidden', 2, 0x30, 0x31, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (19, 'Questionable/Hide', 8, 0x30, 0x31, 0x30);
INSERT INTO `news_item_status` (`news_item_status_id`, `name`, `sort_order`, `show_in_user_search`, `is_hidden`, `is_highlighted`) VALUES (23, 'Questionable/Don''t Hide', 9, 0x30, 0x30, 0x30);

-- 
-- Dumping data for table `news_item_type`
-- 

INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (1, 'article');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (2, 'attachment');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (3, 'comment');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (4, 'event');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (5, 'event_location');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (6, 'breaklng_news');
INSERT INTO `news_item_type` (`news_item_type_id`, `name`) VALUES (7, 'blurb');

-- 
-- Dumping data for table `newswire_type`
-- 

INSERT INTO `newswire_type` (`newswire_type_id`, `name`) VALUES (1, 'within categories: local, global, and other/breaking');
INSERT INTO `newswire_type` (`newswire_type_id`, `name`) VALUES (2, 'within categories: all classified posts');
INSERT INTO `newswire_type` (`newswire_type_id`, `name`) VALUES (3, 'within categories: all posts');
INSERT INTO `newswire_type` (`newswire_type_id`, `name`) VALUES (4, 'all categories: local, global and other/breaking');

-- 
-- Dumping data for table `page`
-- 


INSERT INTO `page` VALUES (16, 'Poverty & Housing', 'poverty', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'poverty', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3137, '2006-05-11 18:32:17');
INSERT INTO `page` VALUES (15, 'Race', 'race', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'race', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3230, '2006-05-11 18:32:20');
INSERT INTO `page` VALUES (14, 'Environment & Forest Defense', 'environment', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'environment', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3539, '2006-05-11 18:31:59');
INSERT INTO `page` VALUES (12, 'Front Page', 'frontpage', 0, 0, '', 5, 4, 3, 6, 14, 1, 1, 0, 0, 0, 0, 15, '', 0x30, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3132, '2006-05-11 18:31:12');
INSERT INTO `page` VALUES (13, 'Police State', 'police', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'police', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3134, '2006-05-11 18:32:14');
INSERT INTO `page` VALUES (18, 'Anti-War', 'antiwar', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'antiwar', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3530, '2006-05-11 18:31:50');
INSERT INTO `page` VALUES (19, 'Labor & Workers', 'labor', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'labor', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3130, '2006-05-11 18:32:10');
INSERT INTO `page` VALUES (48, 'Iraq', 'iraq', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75, 'international/iraq', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3331, '2006-05-11 18:32:31');
INSERT INTO `page` VALUES (49, 'Palestine', 'palestine', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 'international/palestine', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3334, '2006-05-11 18:32:34');
INSERT INTO `page` VALUES (50, 'Haiti', 'haiti', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 'international/haiti', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3239, '2006-05-11 18:32:29');
INSERT INTO `page` VALUES (51, 'Afghanistan', 'afghanistan', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 'international/afghanistan', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3234, '2006-05-11 18:32:24');
INSERT INTO `page` VALUES (22, 'Globalization & Capitalism', 'globalization', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'globalization', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3032, '2006-05-11 18:32:02');
INSERT INTO `page` VALUES (27, 'Drug War', 'drugwar', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'drugwar', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3534, '2006-05-11 18:31:54');
INSERT INTO `page` VALUES (28, 'legal support', 'legal', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'legal', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3130, '2006-05-11 18:32:10');
INSERT INTO `page` VALUES (29, 'LGBTI / Queer', 'lgbtqi', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'lgbtqi', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3133, '2006-05-11 18:32:13');
INSERT INTO `page` VALUES (30, 'Education & Student Activism', 'education', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'education', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3537, '2006-05-11 18:31:57');
INSERT INTO `page` VALUES (31, 'Womyn', 'womyn', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'womyn', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3232, '2006-05-11 18:32:22');
INSERT INTO `page` VALUES (32, 'Indymedia', 'indymedia', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'indymedia', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3038, '2006-05-11 18:32:08');
INSERT INTO `page` VALUES (33, 'En Espa&ntilde;ol', 'espanol', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'espanol', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3038, '2006-05-11 18:31:08');
INSERT INTO `page` VALUES (34, 'Arts + Action', 'arts', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'arts', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3533, '2006-05-11 18:31:53');
INSERT INTO `page` VALUES (35, 'Central Valley', 'centralvalley', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 156, 'centralvalley', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3235, '2006-05-11 18:31:25');
INSERT INTO `page` VALUES (36, 'City of San Francisco', 'sf', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'sf', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3238, '2006-05-11 18:31:28');
INSERT INTO `page` VALUES (37, 'South Bay Area', 'southbay', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'southbay', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3433, '2006-05-11 18:31:43');
INSERT INTO `page` VALUES (38, 'East Bay Area', 'eastbay', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'eastbay', 0x30, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3331, '2006-05-11 18:31:31');
INSERT INTO `page` VALUES (39, 'Peninsula', 'peninsula', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'peninsula', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3338, '2006-05-11 18:31:38');
INSERT INTO `page` VALUES (40, 'North Bay / Marin', 'northbay', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'northbay', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3335, '2006-05-11 18:31:35');
INSERT INTO `page` VALUES (41, 'North Coast', 'northcoast', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'northcoast', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3336, '2006-05-11 18:31:36');
INSERT INTO `page` VALUES (42, 'California', 'california', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'california', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3133, '2006-05-11 18:31:13');
INSERT INTO `page` VALUES (43, 'U.S.', 'us', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'us', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3436, '2006-05-11 18:31:46');
INSERT INTO `page` VALUES (44, 'International', 'international', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 40, 'international', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3334, '2006-05-11 18:31:34');
INSERT INTO `page` VALUES (45, 'Government & Elections', 'government', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'government', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3034, '2006-05-11 18:32:04');
INSERT INTO `page` VALUES (53, 'Americas', 'americas', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50, 'americas', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3237, '2006-05-11 18:32:27');
INSERT INTO `page` VALUES (54, 'Fault Lines', 'faultlines', 0, 0, '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'faultlines', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3038, '2006-05-11 18:31:08');
INSERT INTO `page` VALUES (56, 'Immigrant Rights', 'immigrant', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'immigrant', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33323a3036, '2006-05-11 18:32:06');
INSERT INTO `page` VALUES (58, 'Animal Liberation', 'animalliberation', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 'animalliberation', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3438, '2006-05-11 18:31:48');
INSERT INTO `page` VALUES (60, 'Santa Cruz', 'santacruz', 0, 0, '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 'santacruz', 0x31, 0, '0000-00-00 00:00:00', 0, 0x323030362d30352d31312031383a33313a3430, '2006-05-11 18:31:40');
        
-- 
-- Dumping data for table `page_category`
-- 

INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (12, 12);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (13, 13);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (14, 14);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (15, 15);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (16, 16);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (18, 18);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (19, 19);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (22, 22);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (27, 27);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (28, 28);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (29, 29);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (30, 30);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (31, 31);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (32, 32);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (33, 33);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (34, 34);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (35, 35);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (36, 36);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (37, 37);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (38, 38);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (39, 39);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (40, 40);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (41, 41);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (42, 42);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (43, 43);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (44, 44);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (45, 45);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (48, 48);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (49, 49);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (50, 50);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (51, 51);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (52, 52);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (53, 53);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (54, 54);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (56, 56);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (58, 58);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (59, 59);
INSERT INTO `page_category` (`page_id`, `category_id`) VALUES (60, 60);

-- 
-- Dumping data for table `upload_type`
-- 
INSERT INTO `upload_type` VALUES (1, 'Post');
INSERT INTO `upload_type` VALUES (7, 'medium thumbnail');
INSERT INTO `upload_type` VALUES (4, 'Admin Upload');
INSERT INTO `upload_type` VALUES (5, 'original before resize');
INSERT INTO `upload_type` VALUES (6, 'small thumbnail');


-- 
-- Dumping data for table `event_list_type`
-- 

INSERT INTO `event_list_type` VALUES (0, 'no highlighted events');
INSERT INTO `event_list_type` VALUES (1, 'local/global for this page');
INSERT INTO `event_list_type` VALUES (2, 'local for this page');
INSERT INTO `event_list_type` VALUES (3, 'local/global for any page');
        
        