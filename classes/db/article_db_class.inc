<?php
//--------------------------------------------
//Indybay ArticleDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"ArticleCache");

require_once (CLASS_PATH."/db/news_item_version_db_class.inc");
require_once (CLASS_PATH."/db/media_attachment_db_class.inc");


//class for article specific DB functionality
//most of the actual SQL is in the NewsItemDB and NewsItemVersionDB classes
class ArticleDB extends NewsItemVersionDB
{


	//returns a combination of news_item and news_item_version info for an article
	function get_article_info_from_news_item_id($news_item_id) {
	
		$this->track_method_entry("ArticleDB","get_article_info_from_news_item_id", "news_item_id", $news_item_id);
		
		$version_id=$this->get_current_version_id($news_item_id);
		
		$article_info=$this->get_article_info_from_version_id($version_id);
		
		$this->track_method_exit("ArticleDB","get_article_info_from_news_item_id");
		
		return $article_info;
	}



	//returns a combination of news_item and news_item_version info for an article
    function get_article_info_from_version_id($version_id) {
    
    	$this->track_method_entry("ArticleDB","get_article_info_from_version_id", "version_id", $version_id);
    	
    	$media_attachment_db_class= new MediaAttachmentDB;
    	$version_info=$this->get_news_item_version_info_from_version_id($version_id);
    	$item_info=$this->get_news_item_info($version_info["news_item_id"]);
    	$article_info = (is_array($version_info) && is_array($item_info)) ? array_merge($version_info, $item_info) : NULL;
    	
    	$this->track_method_exit("ArticleDB","get_article_info_from_version_id");
    	
    	return $article_info;
    }
    
    
    
    //adds a new version to the DB from posted fields
    function create_new_version($news_item_id, $post_array){
    
    	$this->track_method_entry("ArticleDB","create_new_version", "news_item_id", $news_item_id);
    	
    	$post_array['version_created_by_id']= $_SESSION['session_user_id'];
    	$post_array['media_type_grouping_id']=0;
    	$post_array['thumbnail_media_attachment_id']=0;
    	if (!isset($post_array['is_summary_html']))
    		$post_array['is_summary_html']=0;
    	if (!isset($post_array['is_text_html']))
    		$post_array['is_text_html']=0;
    	if (!isset($post_array['display_contact_info']))
    		$post_array['display_contact_info']=0;
    	if (trim($post_array['media_attachment_id'])==""){
    		$post_array['media_attachment_id']=0;
    	}
    	if (!isset($post_array['event_duration']) || trim($post_array['event_duration'])==""){
    		$post_array['event_duration']=0;
    	}
    	$news_item_version_id=$this->add_news_item_version_if_changed($news_item_id, $post_array);
    	
    	$this->track_method_exit("ArticleDB","create_new_version", "news_item_version_id", $news_item_version_id);
    	
    	return $news_item_version_id;
    }
    
    
    
    
    //creates a new article meaning the newsitem as well as the version from posted fields
    function create_new_article($item_type_id, $post_array){
    
    	$this->track_method_entry("ArticleDB","create_new_article", "item_type_id", $item_type_id);
    	
    	$post_array['version_created_by_id']=0;

    	if (!array_key_exists('media_type_grouping_id',$post_array))
    		$post_array['media_type_grouping_id']=0;
    	if (!array_key_exists('media_type_grouping_id',$post_array))	
    		$post_array['thumbnail_media_attachment_id']=0;
    	if (!array_key_exists("is_summary_html", $post_array))
    		$post_array['is_summary_html']=0;
    	if (!array_key_exists("display_contact_info", $post_array))
    		$post_array['display_contact_info']=0;
    	if (!array_key_exists("is_text_html", $post_array))
    		$post_array['is_text_html']=0;
    	if (!array_key_exists("display_contact_info", $post_array))
    		$post_array['display_contact_info']=0;
    	if (!array_key_exists("title2", $post_array))
    		$post_array['title2']=$post_array['title1'];
    	if (!array_key_exists("title1", $post_array))
    		$post_array['title1']=$post_array['title2'];

    	if (!array_key_exists("event_duration", $post_array))
    		$post_array['event_duration']=0;
    	else
    		$post_array['event_duration']=$post_array['event_duration']+0;
    		
    	if (!array_key_exists("media_attachment_id", $post_array) || trim($post_array['media_attachment_id'])==""){
    		$post_array['media_attachment_id']=0;
    	}
    	$parent_id=0;
    	if ( array_key_exists("parent_item_id", $post_array)){
    		$parent_id=$post_array["parent_item_id"]+0;
    	}
    	$news_item_id=$this->add_news_item(NEWS_ITEM_STATUS_ID_NEW, $item_type_id, $parent_id);
    	$news_item_version_id=$this->add_news_item_version($news_item_id, $post_array);
    	$this->track_method_exit("ArticleDB","create_new_article", "news_item_id", $news_item_id);
    	
    	return $news_item_id;
    }
    
    
    
    //returns a list of ids for all comments on an article
    function get_comment_ids($news_item_id){
	    
	    $this->track_method_entry("ArticleDB","get_comment_ids", "news_item_id", $news_item_id);
	    
	    $news_item_id = (int) $news_item_id;
	    $query = "select news_item_id from news_item where parent_item_id=$news_item_id
	    	and news_item_type_id=".NEWS_ITEM_TYPE_ID_COMMENT." and current_version_id>0 and current_version_id is not null order by news_item_id desc";
	    $result=$this->single_column_query($query);
	    $result=array_reverse($result);
	    $this->track_method_exit("ArticleDB","get_comment_ids");
	    
	    return $result;
    }
    
    
    
    
    //returns a list of ids for all attachments on an article
    function get_attachment_ids($news_item_id){
    	
    	$this->track_method_entry("ArticleDB","get_attachment_ids", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;	
    	$query = "select news_item_id from news_item where parent_item_id=$news_item_id
	    	and news_item_type_id=".NEWS_ITEM_TYPE_ID_ATTACHMENT." and current_version_id>0 and current_version_id is not null order by news_item_id desc";
	    $result=$this->single_column_query($query);
	    $result=array_reverse($result);
	    $this->track_method_exit("ArticleDB","get_attachment_ids");
	    
	    return $result;
    }
    
    //given a title and an id finds possible duplicates that were posted nearby timewise to the post
    function find_recent_duplicate_nonhidden_versions_by_title($title, $old_id){
   		$this->track_method_entry("ArticleDB","find_recent_duplicate_nonhidden_versions_by_title");

		$title = $this->prepare_string($title);
		$old_id = (int) $old_id;
        $query = "select ni.news_item_id from news_item ni, news_item_version niv ".
        	" where ni.news_item_id=niv.news_item_id and niv.title1 like '%".$title."%' ".
        	" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN." and  ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN." ".
        	" and ni.news_item_id>".($old_id-10000)." and ni.news_item_id<".($old_id+10000)." and ni.news_item_id!=".$old_id." order by news_item_id desc";

        $result=$this->single_column_query($query);
   		$dup_info="";
   		if (is_array($result) && sizeof($result)>0){
  			$dup_id=$result[0];
  			$dup_info=$this->get_article_info_from_news_item_id($dup_id);
  		}
 

   		$this->track_method_exit("ArticleDB","find_recent_duplicate_nonhidden_versions_by_title");
   		
   		return $dup_info;
   	}
   	
   	//finds all recent nonhidden duplicates by title
   	function find_all_recent_duplicate_nonhidden_versions_by_title($title){
   		$this->track_method_entry("ArticleDB","find_all_recent_duplicate_nonhidden_versions_by_title");


    	if (!array_key_exists('old_max_news_item_id',$GLOBALS) || $GLOBALS['old_max_news_item_id']+0==0){
	    	$query="select max(news_item_id) from news_item";
	    	$rss_item_ids=$this->single_column_query($query);
	    	$max_id=array_pop($rss_item_ids);
	    	$GLOBALS['old_max_news_item_id']=$max_id;
	    }else{
	    	$max_id=$GLOBALS['old_max_news_item_id']+0;
	    }
		    
		$title = $this->prepare_string($title);
        $query = "select niv.*, ni.parent_item_id, unix_timestamp(niv.displayed_date) as displayed_timestamp from news_item ni, news_item_version niv ".
        	" where ni.news_item_id=niv.news_item_id and niv.title1='".$title."' ".
        	" and ni.news_item_id>".($max_id-5000)."  order by news_item_id desc";

        $result=$this->query($query);

   		$this->track_method_exit("ArticleDB","find_all_recent_duplicate_nonhidden_versions_by_title");
   		
   		return $result;
   	}
   	
   	//function to change all ids for children of a post
   	//this is mainly useful if you are making one post and its attachments an
   	//attachment on another post
   	function change_all_parent_ids_for_children($news_item_id,$parent_item_id){
		$this->track_method_entry("ArticleDB","change_all_parent_ids_for_children");
		
		$news_item_id = (int) $news_item_id;
		$parent_item_id = (int) $parent_item_id;
		$query = "update news_item set parent_item_id=$parent_item_id ".
        	" where parent_item_id=$news_item_id";
        $this->execute_statement($query);
		
		$this->track_method_exit("ArticleDB","change_all_parent_ids_for_children");
  	}
    
}
