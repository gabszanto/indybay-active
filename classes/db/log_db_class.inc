<?php
//--------------------------------------------
//Indybay NewsItemDB Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewsItemDB");

require_once(CLASS_PATH."/db/db_class.inc");

include_once (CLASS_PATH.'/cache/log_cache_class.inc');

//manages logging of ips to the DB if they match the current settings
class LogDB extends DB
{
    
    
   	//adds log entry to the DB if the HTTP headers match the 
   	//current settings for what should be logged
    function add_log_entry ()
    {
    
    	$this->track_method_entry("LogDB","add_log_entry");
    	
    	$referring_url = getenv("HTTP_REFERER"); 
    	
    	$request_method=$_SERVER['REQUEST_METHOD'];
    	
    	$log_cache = new LogCache();
    	if (!$log_cache->should_record($request_method))
    		return;
    		
    	if ($log_cache->should_record_ips($request_method)){
    		if(getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
    		else if (getenv("HTTP_CLIENT_IP")) $ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $ip = getenv("HTTP_X_FORWARDED_FOR");
			else $ip = "UNKNOWN"; 
			if ($ip!="UNKNOWN"){
	    		$ip_pieces = explode(".", $ip);
	    		$ip_part1=$ip_pieces[0]+0;
	    		$ip_part2=$ip_pieces[1]+0;
	    		$ip_part3=$ip_pieces[2]+0;
	    		$ip_part4=$ip_pieces[3]+0;
    		}
    	}else{
    		$ip=0;
    		$ip_part1=0;
    		$ip_part2=0;
    		$ip_part3=0;
    		$ip_part4=0;
    	}
    	
    	$query_string="";
    	if ($_SERVER['QUERY_STRING']!="")
    		$query_string="?".$_SERVER['QUERY_STRING'];
    	$url=$_SERVER['SCRIPT_NAME'].$query_string;
	
    	$news_item_id=0;
    	$parent_news_item_id=0;
    	
    	if (isset($_GET["news_item_id"])){
    		$news_item_id=$_GET["news_item_id"]+0;
    	}
    	if (strrpos($url, "newsitems")>0){
    		$i=strrpos($url,"/");
    		
    		$j=strrpos($url,".");
    		$parent_news_item_id=substr($url, $i+1,$j-$i-1)+0;
    	}else if (isset($_GET['top_id'])){
    			$parent_news_item_id=$_GET['top_id']+0; 
    	}else if (isset($_POST['parent_item_id'])){
    		$parent_news_item_id=$_POST['parent_item_id']+0; 
    	}
    	
    	$full_headers="";

    	if (isset($_SERVER['REDIRECT_URL']))
    		$full_headers.="REDIRECT_URL: ".$_SERVER['REDIRECT_URL']."\r\n";
    	$full_headers.="HTTP_USER_AGENT: ".$_SERVER['HTTP_USER_AGENT']."\r\n";
    	
    	$ip = $this->prepare_string($ip);
    	$ip_part1 = (int) $ip_part1;
    	$ip_part2 = (int) $ip_part2;
    	$ip_part3 = (int) $ip_part3;
    	$ip_part4 = (int) $ip_part4;
    	$request_method = $this->prepare_string($request_method);
    	$url = $this->prepare_string($url);
    	$referring_url = $this->prepare_string($referring_url);
    	$parent_news_item_id = (int) $parent_news_item_id;
    	$full_headers = $this->prepare_string($full_headers);

        $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',$parent_news_item_id,
			'$full_headers',null)";
        
        $log_id=$this->execute_statement_return_autokey($query);
        $GLOBALS['log_id']=$log_id;
        
        $this->track_method_exit("LogDB","add_log_entry");
        
        return $news_item_id;
    }
    
    
    //this update happens at the end of when pages run so it can be determined how long searches and uploads are taking
    function update_log_id_end_time(){
    	$this->track_method_entry("LogDB","update_log_id_end_time");
    	
    	if (isset($GLOBALS["log_id"])){
    		$sql="update logs set end_time=Now() where log_id=".$GLOBALS["log_id"];
 		   	$this->execute_statement($sql);
    	}
    	$this->track_method_exit("LogDB","update_log_id_end_time");
    }
    
    
    //logging to the DB at publish time is slightly different than at other times since more
    //data is available
    function add_log_entry_before_publish ()
    {
    	$this->track_method_entry("LogDB","add_log_entry_before_publish");
    	
    	$log_cache = new LogCache();
    	if (!$log_cache->should_record("DB"))
    		return;
    		
    	$referring_url = getenv("HTTP_REFERER"); 
    	
    	if ($log_cache->should_record_ips("DB")){
    		if (getenv("HTTP_CLIENT_IP")) $ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $ip = getenv("HTTP_X_FORWARDED_FOR");
			else if(getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
			else $ip = "UNKNOWN"; 
    		$ip_pieces = explode(".", $ip);
    		$ip_part1=$ip_pieces[0]+0;
    		$ip_part2=$ip_pieces[1]+0;
    		$ip_part3=$ip_pieces[2]+0;
    		$ip_part4=$ip_pieces[3]+0;
    	}else{
    		$ip=0;
    		$ip_part1=0;
    		$ip_part2=0;
    		$ip_part3=0;
    		$ip_part4=0;
    	}
    	
    	$query_string="";
    	if ($_SERVER['QUERY_STRING']!="")
    		$query_string="?".$_SERVER['QUERY_STRING'];
    	$url=$_SERVER['SCRIPT_NAME'].$query_string;
    	
    	$parent_news_item_id=0;
    	if (isset($_POST['parent_item_id'])){
    		$parent_news_item_id=$_POST['parent_item_id'];
    	}

    	
    	$request_method="DB";
    	
    	$full_headers="";
    	if (isset($_SERVER['REDIRECT_URL']))
    		$full_headers.="REDIRECT_URL: ".$_SERVER['REDIRECT_URL']."\r\n";
    	$full_headers.="HTTP_USER_AGENT: ".$_SERVER['HTTP_USER_AGENT']."\r\n";
    	$full_headers.="POST_TITLE1: ".$_POST['title1']."\r\n";
    	$full_headers.="POST_AUTHOR: ".$_POST['displayed_author_name']."\r\n";
    	$full_headers.="POST_EMAIL: ".$_POST['email']."\r\n";
    	
    	$ip = $this->prepare_string($ip);
    	$ip_part1 = (int) $ip_part1;
    	$ip_part2 = (int) $ip_part2;
    	$ip_part3 = (int) $ip_part3;
    	$ip_part4 = (int) $ip_part4;
    	$request_method = $this->prepare_string($request_method);
    	$url = $this->prepare_string($url);
    	$referring_url = $this->prepare_string($referring_url);
    	$parent_news_item_id = (int) $parent_news_item_id;
    	$full_headers = $this->prepare_string($full_headers);
    	
        $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id, news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',$parent_news_item_id,0,
			'$full_headers', null)";
 
        
        $log_id=$this->execute_statement_return_autokey($query);
        $GLOBALS["db_log_id"]=$log_id;
        $this->track_method_exit("LogDB","add_log_entry_before_publish");

    }
    
    //updates end time so it can be determined how long publishing took (as compared to file uploading)
    function update_log_entry_after_publish(){
    	$this->track_method_entry("LogDB","update_log_entry_after_publish");
    	if (isset($GLOBALS["just_added_id"]) && array_key_exists("db_log_id",$GLOBALS)){
    		$sql="update logs set end_time=Now(), news_item_id=".$GLOBALS["just_added_id"]." where log_id=".$GLOBALS["db_log_id"];
    		$this->execute_statement($sql);
    	}
    	
        $this->track_method_exit("LogDB","update_log_entry_after_publish");
    }
    
    
    //code to add log entry before a search is run
     function add_log_entry_before_search()
    {
    	$this->track_method_entry("LogDB","add_log_entry_before_search");
    	
    	$log_cache = new LogCache();
    	if (!$log_cache->should_record("SEARCH"))
    		return;
    		
    	$referring_url = getenv("HTTP_REFERER"); 
    	
    	if ($log_cache->should_record_ips("SEARCH")){
    		if (getenv("HTTP_CLIENT_IP")) $ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $ip = getenv("HTTP_X_FORWARDED_FOR");
			else if(getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
			else $ip = "UNKNOWN"; 
    		$ip_pieces = explode(".", $ip);
    		$ip_part1=$ip_pieces[0]+0;
    		$ip_part2=$ip_pieces[1]+0;
    		$ip_part3=$ip_pieces[2]+0;
    		$ip_part4=$ip_pieces[3]+0;
    	}else{
    		$ip=0;
    		$ip_part1=0;
    		$ip_part2=0;
    		$ip_part3=0;
    		$ip_part4=0;
    	}
    	$query_string="";
    	if ($_SERVER['QUERY_STRING']!="")
    		$query_string="?".$_SERVER['QUERY_STRING'];
    	$url=$_SERVER['SCRIPT_NAME'].$query_string;
    	
    	$request_method="SEARCH";
    	
    	$full_headers="";
    	if (isset($_SERVER['REDIRECT_URL']))
    		$full_headers.="REDIRECT_URL: ".$_SERVER['REDIRECT_URL']."\r\n";
    	$full_headers.="HTTP_USER_AGENT: ".$_SERVER['HTTP_USER_AGENT']."\r\n";
    	
    	$ip = $this->prepare_string($ip);
    	$ip_part1 = (int) $ip_part1;
    	$ip_part2 = (int) $ip_part2;
    	$ip_part3 = (int) $ip_part3;
    	$ip_part4 = (int) $ip_part4;
    	$request_method = $this->prepare_string($request_method);
    	$url = $this->prepare_string($url);
    	$referring_url = $this->prepare_string($referring_url);
    	$full_headers = $this->prepare_string($full_headers);

        $query = "insert into logs (
        	start_time, ip, ip_part1, ip_part2,ip_part3,ip_part4,http_method,url,
			referring_url,parent_news_item_id, news_item_id,
			full_headers, end_time) values 
        	(Now(), '$ip', $ip_part1, $ip_part2,$ip_part3,$ip_part4,'$request_method', '$url',
			'$referring_url',0,0,
			'$full_headers', null)";
 
        
        $log_id=$this->execute_statement_return_autokey($query);
        $GLOBALS["search_log_id"]=$log_id;
        $this->track_method_exit("LogDB","add_log_entry_before_search");
        
        return $log_id;
    }
    
    //code to update log entry at end of searching
    function update_log_id_after_search(){
    	$this->track_method_entry("LogDB","update_log_id_after_search");
    	
    	if (array_key_exists("search_log_id",$GLOBALS)){
    		$sql="update logs set end_time=Now() where log_id=".$GLOBALS["search_log_id"];
    		$this->execute_statement($sql);
    	}
    	$this->track_method_exit("LogDB","update_log_id_after_search");
    }


	//returns the log list for the admin log list page    
    function get_log_list($page_number, $page_size, $ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword, $sort_by){
    	
    	$this->track_method_entry("LogDB","get_log_list");
    	
    	$page_number = (int) $page_number;
    	$page_size = (int) $page_size;
    	$logs_query="select *,UNIX_TIMESTAMP(end_time)-UNIX_TIMESTAMP(start_time) as time_diff ";
    	$logs_query.=$this->get_log_list_query($ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword);
        if ($sort_by=="DURATION")
        	$logs_query.=" order by UNIX_TIMESTAMP(end_time)-UNIX_TIMESTAMP(start_time) desc";
		else
			$logs_query.=" order by log_id desc";
		$logs_query.=" limit ".(0+$page_size*($page_number-1)).", ".$page_size;
		
        $result = $this->query($logs_query);
        
        $this->track_method_exit("LogDB","get_log_list");
        
        return $result;
    }
    
    
    //helper function used for getting log list for admin log list page
	function get_log_list_query($ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword){
        	
    	$needs_and=false;
    	$needs_where=true;
        $logs_query=" from logs ";
        
        if ($method_type!="" and $method_type!="0"){
        	$logs_query.="where http_method='".$method_type."' ";
        	$needs_and=true;
        	$needs_where=false;
  
        }

        
        if ($ip!=""){
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" ip='" . $this->prepare_string($ip) . "' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($ip_part1!=""){
        	$ip_part1 = (int) $ip_part1;
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" ip_part1='".$ip_part1."' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($ip_part2!=""){
        	$ip_part2 = (int) $ip_part2;
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" ip_part2='".$ip_part2."' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($ip_part3!=""){
        	$ip_part3 = (int) $ip_part3;
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" ip_part3='".$ip_part3."' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($url!=""){
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" url like '%" . $this->prepare_string($url) . "%' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($referring_url!=""){
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" referring_url like '%" . $this->prepare_string($referring_url) . "%' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($news_item_id!=""){
        	$news_item_id = (int) $news_item_id;
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" news_item_id='".$news_item_id."' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($parent_news_item_id!=""){
        	$parent_news_item_id = (int) $parent_news_item_id;
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" parent_news_item_id='".$parent_news_item_id."' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        if ($keyword!=""){
        	if ($needs_where)
        		$logs_query.=" where ";
        	if ($needs_and)
        		$logs_query.= " and ";
        	$logs_query.=" full_headers like '%" . $this->prepare_string($keyword) . "%' ";
            $needs_and=true;
        	$needs_where=false;
        }
        
        return $logs_query;

    
    
    }
	
	//needed for paging link to last page on log list admin page
	function get_log_list_max_page($page_size, $ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword){
		    	
    	$this->track_method_entry("LogDB","get_log_list_max_page");
    	
    	$logs_query="select count(*) ";
    	$logs_query.=$this->get_log_list_query($ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $referring_url, $news_item_id, $parent_news_item_id, $keyword);
		
        $result = $this->query($logs_query);

        $length=array_pop(array_pop($result));
      
        $page_max=ceil($length/$page_size);

        $this->track_method_exit("LogDB","get_log_list_max_page");
        
        return $page_max;
	}
	
	//in admin there are several ways logs can be cleared. This clears GET logs
	function clear_get_logs(){    	
    	$this->track_method_entry("LogDB","clear_get_logs");
    	$query="delete from logs where http_method='GET'";
    	$this->execute_statement($query);
    	$this->track_method_exit("LogDB","clear_get_logs");
    }
    
    //in admin there are several ways logs can be cleared. This clears all logs
    function clear_all_logs(){
		    	
    	$this->track_method_entry("LogDB","clear_all_logs");
    	$query="truncate logs";
    	$this->execute_statement($query);
    	$this->track_method_exit("LogDB","clear_all_logs");
    }
    
    //clears all ips from logs
    function clear_ips(){
		    	
    	$this->track_method_entry("LogDB","clear_ips");
    	$query="update logs set ip_part3=0,ip_part2=0, ip_part1=0, ip=''";
    	$this->execute_statement($query);
    	$this->track_method_exit("LogDB","clear_ips");
    }
}
