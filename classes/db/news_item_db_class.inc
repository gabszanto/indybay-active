<?php
//--------------------------------------------
//Indybay NewsItemDB Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewsItemDB");
include_once (CLASS_PATH.'/cache/spam_cache_class.inc');
include_once (CLASS_PATH.'/cache/newswire_cache_class.inc');
require_once(CLASS_PATH."/db/db_class.inc");
require_once(CLASS_PATH."/db/log_db_class.inc");


//loads information that is used by all newsitem classes
class NewsItemDB extends DB
{
    
    
    //adds an entry to the news_item table with 0 for the user id that added it
    function add_news_item ($news_item_status_id, $news_item_type_id, $parent_item_id)
    {
    	$this->track_method_entry("NewsItemDB","add_news_item", "news_item_type_id", $news_item_type_id);

		$news_item_status_id = (int) $news_item_status_id;
		$news_item_type_id = (int) $news_item_type_id;
		$parent_item_id = (int) $parent_item_id;
		$spam_cache= new SpamCache();
		if ($spam_cache->should_block_db_add()){
			$news_item_status_id=NEWS_ITEM_STATUS_ID_HIDDEN;
		}
		
        $query = "insert into news_item (news_item_status_id, news_item_type_id, parent_item_id, creation_date) values ".
        	"($news_item_status_id, $news_item_type_id, $parent_item_id, Now())";
        $news_item_id=$this->execute_statement_return_autokey($query);
        $GLOBALS["just_added_id"]=$news_item_id;
        if ($parent_item_id==0 && $news_item_id>0 ){
        	$this->update_parent_id($news_item_id, $news_item_id);
        }else if ($news_item_type_id==NEWS_ITEM_TYPE_ID_COMMENT){
        	$this->update_numcomments($parent_item_id);
        	if ($news_item_id-$parent_item_id<1000){
        		
        		$newswire_cache= new NewswireCache();
        		//trying to reuse method meant for something else hence weird args
        		$parent_info=$this->get_news_item_info($parent_item_id);
        		$newswire_cache->regenerate_newswires_for_newsitem_helper($parent_item_id,
        		  array(), NEWS_ITEM_STATUS_ID_HIDDEN, $parent_info["news_item_status_id"]);
        	}
        }
        
        $this->track_method_exit("NewsItemDB","add_news_item", "news_item_id", $news_item_id);
        
        return $news_item_id;
    }
    
    
    
    //adds news item with a user id
    function add_news_item_for_registered_user ($news_item_status_id, $news_item_type_id, $parent_item_id)
    {
    
    	$this->track_method_entry("NewsItemDB","add_news_item_for_registered_user", "news_item_type_id", $news_item_type_id);
    	
        $news_item_status_id = (int) $news_item_status_id;
        $news_item_type_id = (int) $news_item_type_id;
        $parent_item_id = (int) $parent_item_id;
        $db_obj = new DB; 
        $query = "insert into news_item (news_item_status_id, news_item_type_id, parent_item_id, created_by_id, creation_Date) values ".
        	"($news_item_status_id, $news_item_type_id, $parent_item_id, ".$_SESSION['session_user_id'].", Now())";
        $news_item_id=$this->execute_statement_return_autokey($query);
        if ($parent_item_id==0 && $news_item_id>0 ){
        	$this->update_parent_id($news_item_id, $news_item_id);
        }
        
        
        $this->track_method_exit("NewsItemDB","add_news_item_for_registered_user", "news_item_id", $news_item_id);
        
        return $news_item_id;
    }
    
    
    
    //removes the association with a news item and a category
    function remove_news_item_category ($news_item_id,$category_id)
    {
    	$this->track_method_entry("NewsItemDB","remove_news_item_category", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $category_id = (int) $category_id;
        $query = "delete from news_item_category where news_item_id=$news_item_id and category_id=$category_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("NewsItemDB","remove_news_item_category");
        
        return 1;
    }
    
    
    
    //adds an association between a newsitem and a category
    function add_news_item_category ($news_item_id, $category_id)
    {
     
     	$this->track_method_entry("NewsItemDB","add_news_item_category", "news_item_id", $news_item_id);
        
        $news_item_id = (int) $news_item_id;
        $category_id = (int) $category_id;
        $query = "insert into news_item_category (news_item_id, category_id) values ($news_item_id, $category_id)";
        $this->execute_statement($query);
        
        $this->track_method_exit("NewsItemDB","add_news_item_category");
        
        return 1;
    }
    
    
    
    //updates the current version if on a newsitem
   	function is_news_item_in_category($news_item_id, $category_id)
    {
    	$this->track_method_entry("NewsItemDB","is_news_item_in_category", "news_item_id", $news_item_id, "category_id", $category_id);
    	
        $news_item_id = (int) $news_item_id;
        $category_id = (int) $category_id;
        $query = "select * from news_item_category where news_item_id=".$news_item_id." and category_id=".$category_id;
        $result=$this->query($query);
        $ret=0;
        if (is_array($result) && sizeof($result)>0){
        	$ret=1;
        }
        $this->track_method_exit("NewsItemDB","is_news_item_in_category", "ret", $ret);
        
        return $ret;
    }
    
    
        
    //updates the latest version on a newsitem
   	function update_current_version_id_and_media_grouping_id($news_item_id, $current_version_id, $media_type_grouping_id)
    {
    	$this->track_method_entry("NewsItemDB","update_current_version_id_and_media_grouping_id", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $current_version_id = (int) $current_version_id;
        $media_type_grouping_id = (int) $media_type_grouping_id;
        $query = "update news_item set current_version_id=$current_version_id, media_type_grouping_id=$media_type_grouping_id
          where news_item_id=$news_item_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("NewsItemDB","update_current_version_id_and_media_grouping_id");
        
        return 1;
    }
    
    //updates the latest version on a newsitem
   	function update_latest_version_id($news_item_id, $latest_version_id)
    {
    	$this->track_method_entry("NewsItemDB","update_latest_version_id", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $latest_version_id = (int) $latest_version_id;
        $query = "update news_item set latest_version_id=$latest_version_id where news_item_id=$news_item_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("NewsItemDB","update_latest_version_id");
        
        return 1;
    }
    
    
    
    //updates the parent id of a news item
    function update_parent_id($news_item_id, $new_parent_id)
    {
        $this->track_method_entry("NewsItemDB","update_parent_id", "news_item_id", $news_item_id);
        
        $news_item_id = (int) $news_item_id;
        $new_parent_id = (int) $new_parent_id;
        $query = "update news_item set parent_item_id=$new_parent_id where news_item_id=$news_item_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("NewsItemDB","update_parent_id");
        
        return 1;
    }
    
    
    
    //updates the status id on a news item
    function update_news_item_status_id($news_item_id, $new_status_id)
    {
    	$this->track_method_entry("NewsItemDB","update_news_item_status_id", "news_item_id", $news_item_id, "new_status_id", $new_status_id);
    	
        $news_item_id = (int) $news_item_id;
        $new_status_id = (int) $new_status_id;
    	$this->update_status_helper($news_item_id, $new_status_id);
    	
        $query = "update news_item set news_item_status_id=$new_status_id where news_item_id=$news_item_id";
        $this->execute_statement($query);
        
        
        $this->track_method_exit("NewsItemDB","update_news_item_status_id");
        return 1;
    }
    
    
    
    //updates both status and type on a newsitem
    function update_news_item_status_and_type($news_item_id, $news_item_status_id, $news_item_type_id)
    {
    
    	$this->track_method_entry("NewsItemDB","update_news_item_status_and_type", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $news_item_status_id = (int) $news_item_status_id;
        $news_item_type_id = (int) $news_item_type_id;
    	$this->update_status_helper($news_item_id, $news_item_status_id);
    	
        $query = "update news_item set news_item_type_id=$news_item_type_id, news_item_status_id=$news_item_status_id ".
        	" where news_item_id=$news_item_id";
        $this->execute_statement($query);
        
       	$this->track_method_exit("NewsItemDB","update_news_item_status_and_type");
        
        return 1;
    }
    
    function update_status_helper($news_item_id, $new_status_id){
    	$this->track_method_entry("NewsItemDB","update_status_helper", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;
    	$new_status_id = (int) $new_status_id;
    	$news_item_info=$this->get_news_item_info($news_item_id);
        $parent_item_id=$news_item_info["parent_item_id"];
        $news_item_type_id=$news_item_info["news_item_type_id"];
        if ($news_item_type_id==NEWS_ITEM_TYPE_ID_COMMENT){
        	$this->update_numcomments($parent_item_id);
        	if ($news_item_id-$parent_item_id<1000){
        		
        		$newswire_cache= new NewswireCache();
        		//trying to reuse method meant for something else hence weird args
        		$parent_info=$this->get_news_item_info($parent_item_id);
        		$newswire_cache->regenerate_newswires_for_newsitem_helper($parent_item_id,
        		  array(), NEWS_ITEM_STATUS_ID_HIDDEN, $parent_info["news_item_status_id"]);
        	}
        }

        if ($news_item_type_id==NEWS_ITEM_TYPE_ID_POST && $news_item_info["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_HIDDEN){
        	$this->update_numcomments($news_item_id);
        	$query="update news_item set news_item_status_id=".$new_status_id." 
        	where parent_item_id=".$news_item_id." and news_item_type_id=".NEWS_ITEM_TYPE_ID_ATTACHMENT." and
        	news_item_status_id<>".NEWS_ITEM_STATUS_ID_HIDDEN." and news_item_id>".$news_item_id;
        	$this->execute_statement($query);
        }else if ($news_item_type_id==NEWS_ITEM_TYPE_ID_POST && $news_item_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN){
        	$query="update news_item set news_item_status_id=".$new_status_id." 
        	where parent_item_id=".$news_item_id." and news_item_type_id=".NEWS_ITEM_TYPE_ID_ATTACHMENT." and news_item_id>".$news_item_id;;
        	$this->execute_statement($query);
        }

    	$this->track_method_exit("NewsItemDB","update_status_helper", "news_item_id", $news_item_id);
    
    }
    
    
    
    //gets category information for a newsitem
    function get_news_item_category_info($news_item_id, $category_type_id)
    {
    
    	$this->track_method_entry("NewsItemDB","get_news_item_category_info", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $category_type_id = (int) $category_type_id;
        $query = "select c.category_id, c.name, c.category_type_id from news_item_category nic, ".
        	"category c where nic.news_item_id=$news_item_id and ".
        	"c.category_id=nic.category_id";
       if ($category_type_id!=0){
       		$query.=" and c.category_type_id=$category_type_id";
       }
       $query.=" order by c.name";
       $result=$this->query($query);
       
       $this->track_method_exit("NewsItemDB","get_news_item_category_info");
       
       return $result;
    }
    
    
    
    //gets lists of ids for categories associated with a newsitem
    function get_news_item_category_ids($news_item_id)
    {
        $this->track_method_entry("NewsItemDB","get_news_item_category_ids", "news_item_id", $news_item_id);
        
        $news_item_id = (int) $news_item_id;
        $query = "select category_id from news_item_category where news_item_id=$news_item_id";
        $result=$this->single_column_query($query);
        
        $this->track_method_exit("NewsItemDB","get_news_item_category_ids");
          
        return $result;
    }
    
    
    
    //updates categories on a newsitem and returns list of old categories 
    //(so newswires and the link can get updated)
    function update_news_item_categories_return_old_categories ($news_item_id, $new_category_array)
    {
    
    	$this->track_method_entry("NewsItemDB","update_news_item_categories_return_old_categories", "news_item_id", $news_item_id);
    	
        $old_category_array=$this->get_news_item_category_ids($news_item_id);

        $temp_array=array();
        if (!isset($new_category_array))
        	$new_category_array= array();
        $temp_array=array_merge($new_category_array,$temp_array);
		
		if (is_array($old_category_array)){
	        foreach($old_category_array as $next_cat_id){
	        	if (!isset($temp_array[$next_cat_id])){
	        		$this->remove_news_item_category($news_item_id, $next_cat_id);
	        	}else{
	        		unset($temp_array,$next_cat_id);
	        	}
	        }
        }
        if (is_array($temp_array)){;
	        foreach  ($temp_array as $next_cat_id){
	        	$this->add_news_item_category($news_item_id, $next_cat_id);
	        }
        }
        
        $this->track_method_exit("NewsItemDB","update_news_item_categories_return_old_categories");
         
        return $old_category_array;
    }
    
    
    
    //gets news item info from a news item id
    function get_news_item_info($news_item_id)
    {
    	$this->track_method_entry("NewsItemDB","get_news_item_info", "news_item_id", $news_item_id);
    	
        $news_item_id = (int) $news_item_id;
        $query = "select news_item_id, current_version_id, news_item_type_id, news_item_status_id,
        	 parent_item_id, unix_timestamp(creation_date) as creation_timestamp, 
        	 	date_format(creation_date,'%W %b %D, %Y %l:%i %p ') as created,
        	 	created_by_id
        	 from news_item where news_item_id=".$news_item_id;
       	$result_list=$this->query($query);
       	if (!is_array($result_list) || sizeof($result_list)==0){
       		echo "get_news_item_info:unable to find news item # ".$news_item_id."<br />";
       		$result="";
       	}else{
        	$result=array_pop($result_list);
        }
        
        $this->track_method_exit("NewsItemDB","get_news_item_info");
        
        return $result;
    }
    
    
    
    //returns the current version id for a news item
    function get_current_version_id($news_item_id)
    {
    
    	$this->track_method_entry("NewsItemDB","get_current_version_id", "news_item_id", $news_item_id);
        
        $news_item_id = (int) $news_item_id;
        $query = "select current_version_id from news_item where news_item_id=$news_item_id";
        $result_list=$this->single_column_query($query);
        $version_id=0;
        if (!is_array($result_list) || sizeof($result_list)==0){
        	echo "get_current_version_id:unable to find news item # ".$news_item_id."<br />";
       		return "";
        }else{
        	$version_id=array_pop($result_list);
        }
        
        $this->track_method_exit("NewsItemDB","get_current_version_id", "version_id", $version_id); 
         
        return $version_id;
    }
    
    
    
    //returns the latest version id for a news item
    function get_latest_version_id($news_item_id)
    {
    
    	$this->track_method_entry("NewsItemDB","get_latest_version_id", "news_item_id", $news_item_id);
        
        $news_item_id = (int) $news_item_id;
        $query = "select current_version_id from news_item where news_item_id=$news_item_id";
        $result=$this->query($query);
        $version_id=0;
        if (sizeof($result)==0){
        	echo "get_latest_version_id:unable to find news item # ".$news_item_id."<br />";
       		return "";
        }else{
        	$list=$this->query($query);
        	if (sizeof($list)>0){
        		$row=array_pop($list);
        		$version_id=array_pop($row);
        	}
        }
        
        $this->track_method_exit("NewsItemDB","get_latest_version_id", "latest_version_id", $version_id); 
         
        return $version_id;
    }
    
    
    
    //returns a list of type names and ids
	function get_news_item_types_select_list(){
	
		$this->track_method_entry("NewsItemDB","get_news_item_types_select_list");
		
		$query="select news_item_type_id, name from news_item_type where news_item_type_id!=5 and news_item_type_id!=6";
		$types=$this->query($query);
		$type_select_list=array();
		foreach($types as $row){
			$type_select_list[$row["news_item_type_id"]]=$row["name"];
		}
        
        $this->track_method_exit("NewsItemDB","get_news_item_types_select_list");
        
        return $type_select_list;
	}


	
	//returns the status type names and ids for newsitems
	function get_news_item_status_select_list(){
		$this->track_method_entry("NewsItemDB","get_news_item_status_select_list");
		
		$query="select news_item_status_id, name from news_item_status order by sort_order desc";
		$types=$this->query($query);
		if (is_array($types)){
			$type_select_list=array();
			foreach($types as $row){
				$type_select_list[$row["news_item_status_id"]]=$row["name"];
			}
		}
		
		$this->track_method_exit("NewsItemDB","get_news_item_status_select_list");
		
        return $type_select_list;
	}
	
	//updates the number of nonhidden comments
	function update_numcomments($news_item_id){
		$this->track_method_entry("NewsItemDB","update_numcomments");
		$news_item_id = (int) $news_item_id;
		$query="select count(*) from news_item where parent_item_id=".$news_item_id."
			and news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN." and news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN." and
			news_item_type_id=".NEWS_ITEM_TYPE_ID_COMMENT." and current_version_id>0 and current_version_id is not null";
		$result=$this->single_column_query($query);
		$numcomments=array_pop($result);
		$query= "update news_item set num_comments=".$numcomments." where news_item_id=".$news_item_id;
		$this->execute_statement($query);
		$this->track_method_exit("NewsItemDB","update_numcomments");
	}  

	function update_syndicated($news_item_id){
		$news_item_id = (int) $news_item_id;
		$query= 'update news_item set syndicated = 1 where news_item_id = ' . $news_item_id;
		$this->execute_statement($query);
	}
}
