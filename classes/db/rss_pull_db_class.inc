<?php
//--------------------------------------------
//Indybay RSSPullDB Class
//Written July 2007 
//

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"RSSPullDB");

require_once (CLASS_PATH."/db/db_class.inc");



//the rss feed DB class is responsible for DB access
//related to pulling rss feed onto the site
class RSSPullDB extends DB
{

	//get list of feeds
    function get_feed_list()
    {
    
    	$this->track_method_entry("RSSPullDB","get_feed_list");
    	
    	$query = "select * from inbound_rss_feeds order by name";
	    $result=$this->query($query);
        $this->track_method_exit("RSSPullDB","get_feed_list");
        
        return $result;
    }
    
    function get_feed_list_for_default_status($status_id)
    {
    
    	$this->track_method_entry("RSSPullDB","get_feed_list_for_default_status");
        $status_id = (int) $status_id;
    	if ($status_id!=NEWS_ITEM_STATUS_ID_OTHER && $status_id!=NEWS_ITEM_STATUS_ID_NEW && $status_id!=0)
    		$query = "select * from inbound_rss_feeds where default_status_id=". $status_id ." order by name";
	    else
	    	$query = "select * from inbound_rss_feeds where default_status_id=0 or default_status_id=".NEWS_ITEM_STATUS_ID_OTHER." or default_status_id=".NEWS_ITEM_STATUS_ID_NEW." order by name";
	    $result=$this->query($query);
        $this->track_method_exit("RSSPullDB","get_feed_list_for_default_status");
        
        return $result;
    }
    
	//get list of feed items from temp table
    function get_item_list()
    {
    	$this->track_method_entry("RSSPullDB","get_item_list");
    	
    	$query = "select * from inbound_rss_feed_items";
	    $result=$this->query($sql);
	    
        $this->track_method_exit("RSSPullDB","get_item_list");
        
        return $result;
    }
    
    
    //get list of feed items from temp table
    function get_item_list_for_feed($feed_id)
    {
    	$this->track_method_entry("RSSPullDB","get_item_list_for_feed");
    	
    	$feed_id = (int) $feed_id;
    	$query = "select *, unix_timestamp(creation_date) as creation_date_timestamp  from inbound_rss_feed_items ";
    	if ($feed_id != 0)
    		$query .="where rss_feed_id=".$feed_id;
    	$query.=" order by creation_date desc , rss_item_id desc";
	    $result=$this->query($query);
	    
        $this->track_method_exit("RSSPullDB","get_item_list_for_feed");
        
        return $result;
    }
    
    
        //get list of feed items from temp table
    function get_item_list_by_feed_status($status_id)
    {
    	$this->track_method_entry("RSSPullDB","get_item_list_by_feed_status");
    	
    	$status_id = (int) $status_id;
    	$query = "select fi.*, unix_timestamp(fi.creation_date) as creation_date_timestamp  from inbound_rss_feed_items fi, ";
    	$query .= " inbound_rss_feeds f where f.rss_feed_id=fi.rss_feed_id and f.default_status_id=".$status_id;
    	$query.=" order by creation_date desc , rss_item_id desc";
	    $result=$this->query($query);
	    
        $this->track_method_exit("RSSPullDB","get_item_list_by_feed_status");
        
        return $result;
    }
    
    
    
    

	//get full info for a feed item
    function get_item_info($feed_item_id)
    {
    
    	$this->track_method_entry("RSSPullDB","get_item_info","rss_item_id",$feed_item_id);
    	
    	$feed_item_id = (int) $feed_item_id;
    	$query = "select *, unix_timestamp(creation_date) as creation_date_timestamp from inbound_rss_feed_items where rss_item_id=".$feed_item_id;
       	$result_list=$this->query($query);
       	if (!is_array($result_list) || sizeof($result_list)==0){
       		echo "get_item_info:unable to find feed item # ".$feed_item_id."<br />";
       		$result="";
       	}else{
        	$result=array_pop($result_list);
        }
        
        $this->track_method_exit("RSSPullDB","get_item_info");
        
        return $result;
    }
    
    
    //get full info for a feed item
    function get_feed_info($feed_id)
    {
    
    	$this->track_method_entry("RSSPullDB","get_feed_info","rss_feed_id",$feed_id);
    	
    	$feed_id = (int) $feed_id;
    	$query = "select * from inbound_rss_feeds where rss_feed_id=".$feed_id;
       	$result_list=$this->query($query);
       	if (!is_array($result_list) || sizeof($result_list)==0){
       		echo "get_feed_info:unable to find feed # ".$feed_id."<br />";
       		$result="";
       	}else{
        	$result=array_pop($result_list);
        }
        
        $this->track_method_exit("RSSPullDB","get_feed_info");
        
        return $result;
    }
   
	
	function get_rss_item_id_from_related_url_and_feed_id($related_url,$feed_id){
		$feed_id = (int) $feed_id;
		$query="select rss_item_id from inbound_rss_feed_items where related_url='".
			$this->prepare_string($related_url);
		$query.="' and rss_feed_id=".$feed_id;
		
		$rss_item_ids=$this->single_column_query($query);
		if (is_array($rss_item_ids)){
			return array_pop($rss_item_ids);
		}
		return 0;
	}


    //add item
    function add_item($item_info)
    {
    	if (!isset($item_info['external_unique_id']))
    		$item_info['external_unique_id']="";
    	$this->track_method_entry("RSSPullDB","add_item","title",$item_info["title"]);

    	$query = "insert into inbound_rss_feed_items (is_published,rss_feed_id, external_unique_id, title, summary, author, text,categories,news_item_status_id,related_url,creation_date, pull_date) values ".
        	"(0,". (int) $item_info['rss_feed_id'] .", '"
        	.$this->prepare_string($item_info['external_unique_id'])."', '"
        	.$this->prepare_string($item_info['title'])."', '"
        	.$this->prepare_string($item_info['summary'])."', '"
        	.$this->prepare_string($item_info['author'])."', '"
        	.$this->prepare_string($item_info['text'])."', '"
        	.$this->prepare_string($item_info['categories'])."', "
        	. (int) $item_info['news_item_status_id'] .", '"
        	. (int) $item_info['related_url'] ."', '"
        	. (int) $item_info['creation_date'] ."', Now()) ";
        $rss_item_id=$this->execute_statement_return_autokey($query);

        $this->track_method_exit("RSSPullDB","add_item");
        
        return $rss_item_id;
    }
    
        
    //add feed
    function add_feed($feed_info)
    {
    
    	$this->track_method_entry("RSSPullDB","add_feed","url",$feed_info[url]);
    	
    	$query = "insert into inbound_rss_feeds (name, url, feed_format_id, author_template,
    	 	summary_template, text_template,  text_scrape_start, text_scrape_end,summary_scrape_start,
    	  	summary_scrape_end,author_scrape_start, author_scrape_end,date_scrape_start,
    	   	date_scrape_end,title_scrape_start, title_scrape_end,  replace_url,
    	    replace_url_with,  restrict_urls, default_region_id, default_topic_id, default_status_id) values ".
        	"('".$this->prepare_string($feed_info['name'])."', '"
		.$this->prepare_string($feed_info['url'])."', "
        	. (int) $feed_info['feed_format_id'] .", '"
        	.$this->prepare_string($feed_info['author_template'])."', '"
        	.$this->prepare_string($feed_info['summary_template'])."', '"
        	.$this->prepare_string($feed_info['text_template'])."', '"
        	.$this->prepare_string($feed_info['text_scrape_start'])."', '"
        	.$this->prepare_string($feed_info['text_scrape_end'])."', '"
        	.$this->prepare_string($feed_info['summary_scrape_start'])."', '"
        	.$this->prepare_string($feed_info['summary_scrape_end'])."', '"
        	.$this->prepare_string($feed_info['author_scrape_start'])."', '"
        	.$this->prepare_string($feed_info['author_scrape_end'])."', '"
        	.$this->prepare_string($feed_info['date_scrape_start'])."', '"
        	.$this->prepare_string($feed_info['date_scrape_end'])."', '"
        	.$this->prepare_string($feed_info['title_scrape_start'])."', '"
        	.$this->prepare_string($feed_info['title_scrape_end'])."', '"
        	.$this->prepare_string($feed_info['replace_url'])."', '"
        	.$this->prepare_string($feed_info['replace_url_with'])."', '"
        	.$this->prepare_string($feed_info['restrict_urls'])."', "
        	. (int) $feed_info['default_region_id'] .", "
        	. (int) $feed_info['default_topic_id'] .", "
        	. (int) $feed_info['default_status_id'] .") ";
        $rss_feed_id=$this->execute_statement_return_autokey($query);
        $this->track_method_exit("RSSPullDB","add_feed");
        
        return $rss_feed_id;
    }
    
    
    
    //add feed
    function update_feed($feed_info)
    {
    
    	$this->track_method_entry("RSSPullDB","update_feed","url",$feed_info["url"]);
    	
    	
    	$query = "update inbound_rss_feeds ".
    		"set name='".prepare_string($feed_info["name"])."', ".
    		"url='".prepare_string($feed_info["url"])."', ".
    		"author_template='".prepare_string($feed_info['author_template'])."', ".
    		"feed_format_id=".prepare_string($feed_info['feed_format_id']).", ".
    		"summary_template='".prepare_string($feed_info['summary_template'])."', ".
    		"text_template='".prepare_string($feed_info['text_template'])."', ".
    		"text_scrape_start='".prepare_string($feed_info['text_scrape_start'])."', ".
    		"text_scrape_end='".prepare_string($feed_info['text_scrape_end'])."', ".
    		"summary_scrape_start='".prepare_string($feed_info['summary_scrape_start'])."', ".
    		"summary_scrape_end='".prepare_string($feed_info['summary_scrape_end'])."', ".
    		"author_scrape_start='".prepare_string($feed_info['author_scrape_start'])."', ".
    		"author_scrape_end='".prepare_string($feed_info['author_scrape_end'])."', ".
    		"date_scrape_start='".prepare_string($feed_info['date_scrape_start'])."', ".
    		"date_scrape_end='".prepare_string($feed_info['date_scrape_end'])."', ".
    		"title_scrape_start='".prepare_string($feed_info['title_scrape_start'])."', ".
    		"title_scrape_end='".prepare_string($feed_info['title_scrape_end'])."', ".
    		"replace_url='".prepare_string($feed_info['replace_url'])."', ".
    		"replace_url_with='".prepare_string($feed_info['replace_url_with'])."', ".
    		"restrict_urls='".prepare_string($feed_info['restrict_urls'])."', ".
    		"default_region_id=". (int) $feed_info['default_region_id'] .", ".
    		"default_topic_id=". (int) $feed_info['default_topic_id'] .", ".
    		"default_status_id=". (int) $feed_info['default_status_id'] .
    		" where rss_feed_id=". (int) $feed_info['rss_feed_id'];
        $this->execute_statement($query);
            
        $this->track_method_exit("RSSPullDB","update_feed");
    }
    
    //add item
    function update_item($item_info, $is_not_from_post=false)
    {
    	$this->track_method_entry("RSSPullDB","update_item","title",$item_info["title"]);

    	if ($is_not_from_post){
    		$item_info["title"]=$this->prepare_string($item_info["title"]);
    		$item_info["summary"]=$this->prepare_string($item_info["summary"]);
    		$item_info["author"]=$this->prepare_string($item_info["author"]);
    		$item_info["text"]=$this->prepare_string($item_info["text"]);
    		$item_info["related_url"]=$this->prepare_string( $item_info["related_url"]);
    	}else{
    	   	$item_info["title"]=$this->prepare_string($item_info["title"]);
    		$item_info["summary"]=$this->prepare_string($item_info["summary"]);
    		$item_info["author"]=$this->prepare_string($item_info["author"]);
    		$item_info["text"]=$this->prepare_string($item_info["text"]);
    		$item_info["related_url"]=$this->prepare_string( $item_info["related_url"]);	
    	}

    	$query = "update inbound_rss_feed_items ".
    		"set title='".$item_info["title"]."', ".
    		"summary='". $item_info["summary"]."', ".
    		"author='". $item_info["author"]."', ";
    	if (isset($item_info["creation_date"])){
	    	$creation_date_timestamp=strtotime($item_info["creation_date"]);
	    	if ($creation_date_timestamp>time()-60*60*24*1000){
	    		$query .=	"creation_date='". date("Y-m-d H:i",$creation_date_timestamp)."', ";
	    	}
    	}
    	$query .=	"text='". $item_info["text"]."', ".
    		"categories='". $this->prepare_string($item_info["categories"]) ."', ".
    		"news_item_status_id=". (int) $item_info["news_item_status_id"] .", ".
    		"related_url='". $this->prepare_string($item_info["related_url"]) ."' ".
    		"where rss_item_id=". (int) $item_info["rss_item_id"];
        $this->execute_statement($query);

        
        $this->track_method_exit("RSSPullDB","update_item");
    }
    

    function set_item_to_published($rss_item_id, $published_news_item_id)
    {
	    $this->track_method_entry("RSSPullDB","set_item_to_published");
	    
	    $sql = "update inbound_rss_feed_items set is_published=1, published_news_item_id=". (int) $published_news_item_id
			." where rss_item_id=".  (int) $rss_item_id;
        $this->execute_statement($sql);
	    
	    $this->track_method_exit("RSSPullDB","set_item_to_published");
    }
    
     //update feed pull time feed
    function update_feed_pull_time_and_duration($feed_id, $last_pull_duration_secs)
    {
    
    	$this->track_method_entry("RSSPullDB","update_feed_pull_time_and_duration");
    	
    	
    	$query = "update inbound_rss_feeds ".
    		"set last_pull_date=Now(), ".
    		"last_pull_duration_usecs=". (int) $last_pull_duration_secs.
    		" where rss_feed_id=". (int) $feed_id;
        $this->execute_statement($query);
            
        $this->track_method_exit("RSSPullDB","update_feed_pull_time_and_duration");
    }
    
    //update item
    function delete_feed($feed_id)
    {
    
    	$this->track_method_entry("RSSPullDB","delete_feed","feed_id",$feed_id);
    	
    	$query = "delete from inbound_rss_feeds where rss_feed_id=". (int) $feed_id;
    	$this->execute_statement($query);
        $this->track_method_exit("RSSPullDB","delete_feed");

    }
    
    //add feed
    function delete_item($item_id)
    {
    
    	$this->track_method_entry("RSSPullDB","delete_item","rss_item_id",$item_id);
    	
    	
    	$query = "delete from inbound_rss_feed_items where rss_item_id=". (int) $item_id;
    	$this->execute_statement($query);
    	
        $this->track_method_exit("RSSPullDB","delete_item");
        
        return $ret;
    }
    
        
    //add feed
    function delete_all_items_for_feed($feed_id)
    {
    
    	$this->track_method_entry("RSSPullDB","delete_item");
    	
    	$query = "delete from inbound_rss_feed_items where rss_feed_id=". (int) $feed_id;
    	$this->execute_statement($query);
    	
        $this->track_method_exit("RSSPullDB","delete_item");
    }
    
    //add feed
    function delete_old_items($feed_id)
    {
    
    	$this->track_method_entry("RSSPullDB","delete_item","rss_item_id",$feed_id);
    	
    	$query = "delete from inbound_rss_feed_items where rss_feed_id=". (int) $feed_id;
    	$query .=" and DATEDIFF(Now(),creation_date)>5 and DATEDIFF(Now(),creation_date)<4000"; 
    	$this->execute_statement($query);
    	
        $this->track_method_exit("RSSPullDB","delete_item");
        
    }
    
    
    function update_if_duplicate($rss_item_info){
    
    	$this->track_method_entry("RSSPullDB","update_if_duplicate");

    	if ($rss_item_info["related_url"]!="" && strlen($rss_item_info["title"])>7){
	    	if (!isset($GLOBALS['old_max_news_item_id']) || $GLOBALS['old_max_news_item_id']+0==0){
		    	$query="select max(news_item_id) from news_item";
		    	$rss_item_ids=$this->single_column_query($query);
		    	$max_id=array_pop($rss_item_ids);
		    	$GLOBALS['old_max_news_item_id']=$max_id;
		    }else{
		    	$max_id=$GLOBALS['old_max_news_item_id']+0;
		    }
		    
	    	$query="select news_item_id from news_item_version where (related_url='".
	    			$this->prepare_string($rss_item_info["related_url"])."' or title1='".$this->prepare_string($rss_item_info["title"])."' or ".
	    			" summary like '%".$this->prepare_string($rss_item_info["related_url"])."%' or text like '%".$this->prepare_string($rss_item_info["related_url"])."%') and news_item_id>".($max_id-6000);

	    	$result_list=$this->single_column_query($query);
	       	if (!is_array($result_list) || sizeof($result_list)==0){
		       	return;
	       	}else{
	        	$news_item_id=array_pop($result_list);
	        	$this->set_item_to_published($rss_item_info["rss_item_id"],$news_item_id);
	        }
        }
    			
    	$this->track_method_exit("RSSPullDB","update_if_duplicate");
    }

}

