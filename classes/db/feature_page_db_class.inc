<?php
//--------------------------------------------
//Indybay FeaturePageDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"FeaturePageDB");

require_once (CLASS_PATH."/db/db_class.inc");



//the feature page DB class is responsible for DB access
//related to feature pages and lists of blurbs
//the blurb DB class deals with the contents of the actual blurbs
class FeaturePageDB extends DB
{

	//gets info about a specific page
    function get_feature_page_info($page_id)
    {
    
    	$this->track_method_entry("FeaturePageDB","get_feature_page_info", "page_id", $page_id);
    	
        $page_id = (int) $page_id;
        if (strlen($page_id) != 0)
        {
            $page_detail_query = "SELECT * FROM page WHERE page_id=" . $page_id;
            $resultset = $this->query($page_detail_query);
            $page = array_pop($resultset);
            $ret=$page;
        } else
        {
            $ret=0;
        }
        
        $this->track_method_exit("FeaturePageDB","get_feature_page_info");
        
        return $ret;
    }



	//gets a list of pages given a parent category id and a category type
	//this is used when getting a list of pages associated with a post (for the links at the top for examnple) since the
	//relationship is that poists are associted with categories and categories
	//are associated with pages
    function get_page_list($category_type_id, $parent_cat_id)
    {
    	$this->track_method_entry("FeaturePageDB","get_page_list", "category_type_id", $category_type_id, "parent_cat_id", $parent_cat_id);
    	
        $category_type_id = (int) $category_type_id;
        $parent_cat_id = (int) $parent_cat_id;
        //returns a list of all features in the system.
        $featurepage_list_query="SELECT p.* FROM page p  ".
		" where p.page_id in (select pc.page_id from page_category pc , ".
		"category c where pc.category_id=c.category_id and c.category_type_id=".$category_type_id;
		if ($parent_cat_id==0){
			$featurepage_list_query.=" and c.parent_category_id is null";
		}else{
			$featurepage_list_query.=" and c.parent_category_id=".$parent_cat_id;
		}
		$featurepage_list_query.=" ) ".
		"order by p.long_display_name";
        $result = $this->query($featurepage_list_query);
        
        $this->track_method_exit("FeaturePageDB","get_page_list");
        
        return $result;
    }
    
    
    
    //returns a list of pages for a select list (mainly used within admin)
    function get_all_page_select_list()
    {

		$this->track_method_entry("FeaturePageDB","get_all_page_select_list");

        $featurepage_list_query="SELECT page_id, long_display_name FROM page p ".
		"order by p.long_display_name";
        $result = $this->query($featurepage_list_query);
        $ret=array();
        foreach($result as $row){
       		 $ret[$row["page_id"]]=$row["long_display_name"];
        }
        
        $this->track_method_exit("FeaturePageDB","get_all_page_select_list");
        return $ret;
     }
       
       
	//add page needs to be written at some point but for now
	//all pages are manually added to the DB and included in the
	//seed data for the site
    function add_page($categoryfields)
    {
    	$this->track_method_entry("FeaturePageDB","add_page");
    	$this->track_method_exit("FeaturePageDB","add_page");
       
	}
	
	//While you cant add pages you can update them
    //this method updates some of the things associated with pages
    //that could change over time (like what type of center column
    //and write column should be displayed etc...) 
    function update_feature_page_info($posted_fields)
    {
    
    	$this->track_method_entry("FeaturePageDB","update_feature_page_info");
    	
    	//allows code to work with magic quotes on or off
    	$long_display_name=$this->prepare_string($posted_fields['long_display_name']);
    	$short_display_name=$this->prepare_string($posted_fields['short_display_name']);
    	$relative_path=$this->prepare_string($posted_fields['relative_path']);
    	
    	
     	$include_in_readmore_links=$posted_fields['include_in_readmore_links'];
     	if ($include_in_readmore_links!=1)
     		$include_in_readmore_links=0;
  		$update="update page set long_display_name='".$long_display_name."', ".
  			"short_display_name='".$short_display_name."', ".
  			"center_column_type_id=".($posted_fields['center_column_type_id']+0).",".
  			"newswire_type_id=".($posted_fields['newswire_type_id']+0).", ".
  			"items_per_newswire_section=".($posted_fields['items_per_newswire_section']+0).", ".
  			"relative_path='".$relative_path."', ".
  			"include_in_readmore_links=".($include_in_readmore_links+0).", ".
  			"event_list_type_id=".($posted_fields['event_list_type_id']+0).", ".
  			"max_events_in_highlighted_list=".($posted_fields['max_events_in_highlighted_list']+0).", ".
  			"max_timespan_in_days_for_highlighted_event_list=".($posted_fields['max_timespan_in_days_for_highlighted_event_list']+0).", ".
  			"max_events_in_full_event_list=".($posted_fields['max_events_in_full_event_list']+0).", ".
  			"max_timespan_in_days_for_full_event_list=".($posted_fields['max_timespan_in_days_for_full_event_list']+0).
  			" where page_id=".($posted_fields['page_id']+0);
		$ret=$this->execute_statement($update);  
						
  		$this->track_method_exit("FeaturePageDB","update_feature_page_info");
  		
  		return  $ret;
    }



	//gets the current blurbs that are on a given page
	//used for previewing pages and when pages are pushed live
    function get_current_blurb_list($page_id){
    
    	$this->track_method_entry("FeaturePageDB","get_current_blurb_list", "page_id", $page_id);
    
    	$page_id = (int) $page_id;
    	$query="select nipo.order_num, nipo.display_option_id, niv.*, ";
    	$query.=" date_format(niv.version_creation_date,'%Y-%m-%d %h:%i %p') as modified, ";
    	$query.=" date_format(ni.creation_date,'%Y-%m-%d %h:%i %p') as created, ";
    	$query.=" unix_timestamp(ni.creation_date) as creation_timestamp,
               unix_timestamp(niv.version_creation_date) as modified_timestamp, syndicated ";
    	$query.=" from news_item_page_order nipo, news_item_version niv, news_item ni
    		 where nipo.page_id=".$page_id." and nipo.news_item_id=niv.news_item_id and 
    		 ni.news_item_type_id=".NEWS_ITEM_TYPE_ID_BLURB." and ni.news_item_status_id!=".
    		 NEWS_ITEM_TYPE_ID_BLURB." and
    		  ni.current_version_id=niv.news_item_version_id 
    		 order by nipo.order_num desc";
    	$resultset = $this->query($query);

        foreach ($resultset as $key => $blurb) {
          // Get attached breaking news items. An index should make this query
          // pretty fast. For speed, do not sort and only get "Other" items.
          $query = 'SELECT news_item_id FROM news_item WHERE parent_item_id = '
            . $blurb['news_item_id'] . ' AND news_item_type_id = '
            . NEWS_ITEM_TYPE_ID_BREAKING . ' AND news_item_status_id = '
            . NEWS_ITEM_STATUS_ID_OTHER;
          $resultset[$key]['breaking_items'] = $this->query($query);
        }

    	$this->track_method_exit("FeaturePageDB","get_current_blurb_list");
    	
        return $resultset;
    
    }
        
    //gets the list of blurbs used within admin when not previewing but reordering
    //or trying to find blurbs to edit
    function get_current_blurb_list_limited_info($page_id){
    	$this->track_method_entry("FeaturePageDB","get_current_blurb_list_limited_info", "page_id", $page_id);
    	
    	$page_id = (int) $page_id;
    	$query="select nipo.*, 
    		niv.news_item_id, niv.title2,niv.version_created_by_id, niv.version_creation_date, ";
    	$query.=" date_format(niv.version_creation_date,'%m/%d/%Y %h:%i %p') as modified, ";
    	$query.=" date_format(ni.creation_date,'%m/%d/%Y %h:%i %p') as created ";
    	$query.=" from news_item_page_order nipo, news_item_version niv, news_item ni
    		 where nipo.page_id=".$page_id." and nipo.news_item_id=niv.news_item_id and
    		  ni.current_version_id=niv.news_item_version_id 
    		 order by nipo.order_num desc";
    	$resultset = $this->query($query);
    	
    	$this->track_method_exit("FeaturePageDB","get_current_blurb_list_limited_info");
    	
        return $resultset;
    
    }
    
    //gets the list of blurbs used within admin when viewing the archived blurbs for a page
    function get_archived_blurb_list_limited_info($page_id, $limit_start, $num_items_per_page){
    
    	$this->track_method_entry("FeaturePageDB","get_archived_blurb_list_limited_info", "page_id", $page_id);
    	
    	$page_id = (int) $page_id;
    	$limit_start = (int) $limit_start;
    	$num_items_per_page = (int) $num_items_per_page;
    	$query="select ni.news_item_id, max(niv.title2) as title2, max(niv.version_created_by_id) as version_created_by_id, ";
    	$query.=" max(date_format(niv.version_creation_date,'%Y-%m-%d %h:%i %p')) as modified, ";
    	$query.=" max(date_format(ni.creation_date,'%Y-%m-%d %h:%i %p')) as created ";
    	$query.="
    		 from news_item_version niv, news_item ni, news_item_category nic
    		 where ni.news_item_type_id=".NEWS_ITEM_TYPE_ID_BLURB." and nic.news_item_id=ni.news_item_id 
    		 and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN."
    		 and ni.current_version_id=niv.news_item_version_id
    		 and nic.category_id in (select category_id from page_category where page_id=".$page_id.")
    		 and ni.news_item_id not in (select news_item_id from news_item_page_order where page_id=$page_id)
    		 group by ni.news_item_id
    		 order by ni.news_item_id desc
    		 limit $limit_start, $num_items_per_page";
    	$resultset = $this->query($query);
    	
    	$this->track_method_exit("FeaturePageDB","get_archived_blurb_list_limited_info");
    	
        return $resultset;
    }
    
     //gets the list of blurbs used within admin when viewing the hidden blurbs for a page
    function get_hidden_blurb_list_limited_info($page_id, $limit_start, $num_items_per_page){
    
    	$this->track_method_entry("FeaturePageDB","get_hidden_blurb_list_limited_info");
    	
    	$page_id = (int) $page_id;
    	$limit_start = (int) $limit_start;
    	$num_items_per_page = (int) $num_items_per_page;
    	$query="select ni.news_item_id, max(niv.title2) as title2, max(niv.version_created_by_id) as version_created_by_id, ";
    	$query.=" max(date_format(niv.version_creation_date,'%Y-%m-%d %h:%i %p')) as modified, ";
    	$query.=" max(date_format(ni.creation_date,'%Y-%m-%d %h:%i %p')) as created ";
    	$query.="
    		 from news_item_version niv, news_item ni, news_item_category nic
    		 where ni.news_item_type_id=".NEWS_ITEM_TYPE_ID_BLURB." and nic.news_item_id=ni.news_item_id 
    		 and ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_HIDDEN."
    		 and ni.current_version_id=niv.news_item_version_id
    		 and nic.category_id in (select category_id from page_category where page_id=".$page_id.")
    		 and ni.news_item_id not in (select news_item_id from news_item_page_order where page_id=$page_id)
    		 group by ni.news_item_id
    		 order by ni.news_item_id desc
    		 limit $limit_start,".$num_items_per_page;

    	$resultset = $this->query($query);
    	$this->track_method_exit("FeaturePageDB","get_hidden_blurb_list_limited_info");
    	
        return $resultset;
    }
    
    //gets the dropdown list used on the page editing page
    function get_newswire_type_list(){
    
    	$this->track_method_entry("FeaturePageDB","get_newswire_type_list");
    	
    	$query="select name, newswire_type_id from newswire_type";
    	$resultset = $this->query($query);
        $res=array();
        foreach($resultset as $row){
        	$res[$row['newswire_type_id']]=$row['name'];
        }
        
        $this->track_method_exit("FeaturePageDB","get_newswire_type_list");
        
        return $res;
    }
    
     //gets the dropdown list used on the page editing page
    function get_center_column_type_list(){
    
    	$this->track_method_entry("FeaturePageDB","get_center_column_type_list");
    	
    	$query="select * from center_column_type";
    	$resultset = $this->query($query);
    	
        $res=array();
        foreach($resultset as $row){
        	$res[$row['center_column_type_id']]=$row['name'];
        }
        
    	$this->track_method_exit("FeaturePageDB","get_center_column_type_list");
    	
        return $res;
    }
    
     //gets the dropdown list used on the page editing page
    function get_event_list_type_list(){
    
    	$this->track_method_entry("FeaturePageDB","get_event_list_type_list");
    	
    	$query="select * from event_list_type";
    	$resultset = $this->query($query);
    	
        $res=array();
        foreach($resultset as $row){
        	$res[$row['event_list_type_id']]=$row['name'];
        }
        
    	$this->track_method_exit("FeaturePageDB","get_event_list_type_list");
    	
        return $res;
    }
    
    //from a category returns the list of ids of all associated pages
    function get_associated_page_ids($category_list){
    
    	$this->track_method_entry("FeaturePageDB","get_associated_page_ids");
    	
    	$page_list= array();
    	$db_obj = new DB();
    	foreach($category_list as $next_cat_id){
    		$next_cat_id = (int) $next_cat_id;
    		$query="select page_id from page_category where category_id=".$next_cat_id;
    		$next_list=$db_obj->single_column_query($query);
    		$page_list=array_merge($page_list, $next_list);
    	}
    	
    	$this->track_method_exit("FeaturePageDB","get_associated_page_ids");
    	
    	return $page_list;
    }
    
    
    
    //gets the list of all pages that should show all categories 
    //on most Indymedia sites this is just the front page
    function get_pages_with_all_category_newswires(){
    
    	$this->track_method_entry("FeaturePageDB","get_pages_with_all_category_newswires");
    
    	$query="select page_id from page where newswire_type_id=4";
    	$ret=$this->single_column_query($query);
    
    	$this->track_method_exit("FeaturePageDB","get_pages_with_all_category_newswires");
    
    	return $ret;
    }
    
    //given a page id returns a list of all associated category ids
    function get_category_ids_for_page($page_id){
    	
    	$this->track_method_entry("FeaturePageDB","get_category_ids_for_page", "page_id", $page_id);
    	
    	$page_id = (int) $page_id;
    	$query="select category_id from page_category where page_id=".$page_id;
    	
    	$ret=$this->single_column_query($query);
    	
    	$this->track_method_exit("FeaturePageDB","get_category_ids_for_page");
    	
    	return $ret;
    }
    
    
    //gets the first topic type category associated with a page for use when preselecting
    //category dropdown lists when a user posts from the page
    function get_first_topic_for_page($page_id){
    
	    $this->track_method_entry("FeaturePageDB","get_first_topic_for_page", "page_id", $page_id);
    
    	$page_id = (int) $page_id;
    	$topic_id=0;
    	$query="select c.category_id from page_category pc, category c
    		 where pc.page_id=".$page_id." and c.category_id=pc.category_id
    		 and c.category_type_id=".CATEGORY_TYPE_TOPIC;
    	$list=$this->single_column_query($query);
    	if (sizeof($list)>0){
    		$topic_id=array_pop($list);
    	}
    	
    	$this->track_method_exit("FeaturePageDB","get_first_topic_for_page", "topic_id",  $topic_id);
    	
    	return $topic_id;
    }
    
     //gets the first region type category associated with a page for use when preselecting
    //category dropdown lists when a user posts from the page
    function get_first_region_for_page($page_id){
    
    	$this->track_method_entry("FeaturePageDB","get_first_region_for_page", "page_id", $page_id);
    	
    	$page_id = (int) $page_id;
    	$region_id=0;
    	$query="select c.category_id from page_category pc, category c
    		 where pc.page_id=".$page_id." and c.category_id=pc.category_id
    		 and c.category_type_id=".CATEGORY_TYPE_REGION;
    	$list=$this->single_column_query($query);
    	if (sizeof($list)>0){
    		$region_id=array_pop($list);
    	}
    	
    	$this->track_method_exit("FeaturePageDB","get_first_region_for_page");
    	
    	return $region_id;
    }
    
    
    
    //returns the numbers of blurbs on a page
    function get_blurb_count($page_id){
    
    	$this->track_method_entry("FeaturePageDB","get_blurb_count", "page_id", $page_id);
    
    	$page_id = (int) $page_id;
    	$query="select count(*) from news_item_page_order where page_id=".$page_id;
    	
        $result = $this->single_column_query($query);
        $ret = array_pop($result);
    	
    	$this->track_method_exit("FeaturePageDB","get_blurb_count", "blurb_count", $ret);
    	
    	return $ret;
    }
    
    
    //returns the numbers of nonpushed blurbs on a page
    //*****
    //THIS CODE IS BROKEN SINCE IT DOESNT DETECT BLURBS ADDED AFTER THE PAGE GOES LIVE
    //*****
    function get_nonpushed_blurb_count($page_id){
    
    	$this->track_method_entry("FeaturePageDB","get_nonpushed_blurb_count", "page_id", $page_id);
    
    	$page_id = (int) $page_id;
    	$query="select count(*) from news_item_page_order nipo, news_item_version niv, news_item ni, page p
    	  where	ni.news_item_id=nipo.news_item_id and niv.news_item_version_id=ni.current_version_id
    	  and niv.version_creation_date>p.last_pushed_date and
    	  nipo.page_id=p.page_id and p.page_id=".$page_id;
    	  
        $result = $this->single_column_query($query);
        $ret = array_pop($result);
    	
    	$this->track_method_exit("FeaturePageDB","get_nonpushed_blurb_count");
    	
    	return $ret;
    }
    
    
    
    //returns pages associated with a given news item
    function get_associated_pages_info($news_item_id){
    
    	$this->track_method_entry("FeaturePageDB","get_associated_pages_info", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;
    	$query="select p.* from page p, page_category pc, category c, news_item_category nic
    		where nic.news_item_id=".$news_item_id." and nic.category_id=pc.category_id
    		and pc.page_id=p.page_id and c.category_id=pc.category_id order by c.parent_category_id desc, c.category_type_id, p.long_display_name";

    	$ret=$this->query($query);
    	
    	$this->track_method_exit("FeaturePageDB","get_associated_pages_info");
    		
    	return $ret;
    }
    
    
    
    //updates the last pushed live date for a page
    function update_page_pushed_live_date($feature_page_id){
    	$this->track_method_entry("FeaturePageDB","update_page_pushed_live_date", "feature_page_id", $feature_page_id);
    	
    	$feature_page_id = (int) $feature_page_id;
    	$query="update page set last_pushed_date=Now() where page_id=".$feature_page_id;
    		
    	$ret=$this->execute_statement($query);
    	
    	$this->track_method_exit("FeaturePageDB","update_page_pushed_live_date");
    }
    
    //Returns a list of pahes with pushed long versions of a blurb
    //This method is mainly used for generating the "Read More On" Links 
    //At the end of short versions of blurbs
    function get_pages_with_pushed_long_versions_of_blurb($news_item_id){
    	$this->track_method_entry("FeaturePageDB","get_pages_with_pushed_long_versions_of_blurb", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;
    	$query="select max(p.short_display_name) as short_display_name , max(p.long_display_name) as long_display_name , max(p.page_id) as page_id,
    		 max(p.relative_path) as relative_path
    		 from page p, news_item_page_order nipo, news_item_version niv
    		where niv.news_item_id=".$news_item_id." and nipo.news_item_id=".$news_item_id."
    		and nipo.page_id=p.page_id and niv.version_creation_date<=
    		p.last_pushed_date and (
    			nipo.display_option_id=".BLURB_DISPLAY_OPTION_AUTO_LONG."
		    	or nipo.display_option_id=".BLURB_DISPLAY_OPTION_NOIMAGE_LONG."
		    	or nipo.display_option_id=".BLURB_DISPLAY_OPTION_LEFT_LONG."
		    	or nipo.display_option_id=".BLURB_DISPLAY_OPTION_RIGHT_LONG."
    		or
	    		(p.center_column_type_id<4 and (nipo.display_option_id=".BLURB_DISPLAY_OPTION_AUTO."))
	    	)
	    	group by p.page_id 
	    	";
    		
    	$ret=$this->query($query);
    	
    	$this->track_method_exit("FeaturePageDB","get_pages_with_pushed_long_versions_of_blurb");
    	return $ret;
    
    }
    
    //returns a list of recent blurbs to list at the bottom of a single item view or the front page
     function get_recent_blurb_version_ids($page_id = 0){
    	$this->track_method_entry("FeaturePageDB","get_recent_blurb_version_ids");
    	
    	$page_id = (int) $page_id;
    	$query="select max(news_item_version_id) as news_item_version_id
    		from news_item_page_order nipo, news_item_version niv, news_item ni, page p
    		where  nipo.news_item_id=niv.news_item_id and nipo.page_id=p.page_id
    		and p.last_pushed_date>niv.version_creation_date
    		and nipo.order_num<1000
    		and ni.news_item_id=niv.news_item_id";
    	if ($page_id!=0){
    		$query.=" and p.page_id=".$page_id;
    	}
    	$query.=" group by niv.news_item_id
    		 order by  max(ni.creation_date)  desc limit 200";
    	$resultset = $this->single_column_query($query);
    	$this->track_method_exit("FeaturePageDB","get_recent_blurb_version_ids");
    	
        return $resultset;
    
    }
    
    
    
    
}
