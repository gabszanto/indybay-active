<?php
//--------------------------------------------
//Indybay UserDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"UserDB");

require_once(CLASS_PATH."/db/db_class.inc");

//used for getting user information to show edit histories as 
//well as the user admin pages
class UserDB extends DB
{

    
    function get_user_info($user_id)
    {
    		$this->track_method_entry("UserDB","get_user_info" , "user_id", $user_id);
    		
            $user_detail_query = "SELECT u.user_id, u.username, u.has_login_rights, date_format(u.creation_date, '%b %D %l:%i %p %Y') as created, date_format(u.last_login_time, '%b %D %l:%i %p %Y') as last_login,  c.* FROM user u, contact_info c WHERE 
            	u.user_id=". (int) $user_id." and c.contact_info_id=u.contact_info_id";
            $resultset = $this->query($user_detail_query);
            $user = array_pop($resultset);
            
            $this->track_method_exit("UserDB","get_user_info");
            
            return $user;
    }




    function get_list($is_enabled)
    {
    
    	$this->track_method_entry("UserDB","get_list" , "is_enabled", $is_enabled);
    	
         $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%b %D %l:%i %p %y') as last_activity, u.username, date_format(u.creation_date, '%b %D %l:%i %p %y') as created, date_format(u.last_login_time, '%b %D %l:%i %p %y') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=". (int) $is_enabled." order by u.username";
        $result = $this->query($user_list_query);
        
        $this->track_method_exit("UserDB","get_list");
        
        return $result;
    }


    function get_recent_list($num_days)
    {
    
    	$this->track_method_entry("UserDB","get_list");
    	
         $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%W %b %D %l:%i %p') as last_activity, u.username, date_format(u.creation_date, '%W %b %D %l:%i %p') as created, date_format(u.last_login_time, '%W %b %D %l:%i %p') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=1
            	and
            		(UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_admin_activity) <=". (int) $num_days."*60*60*24
            	or
            	UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_login_time) <=". (int) $num_days."*60*60*24
            	)
            	 order by u.last_admin_activity desc";
        $result = $this->query($user_list_query);
        
        $this->track_method_exit("UserDB","get_list");
        
        return $result;
    }




    function get_nonrecent_list($num_days)
    {
    
    	$this->track_method_entry("UserDB","get_list" );
    	
         $user_list_query = "SELECT u.user_id, date_format(u.last_admin_activity, '%b %D %l:%i %p %Y') as last_activity, u.username, date_format(u.creation_date, '%b %D %l:%i %p %Y') as created, date_format(u.last_login_time, '%b %D %l:%i %p %Y') as last_login, 
          c.* FROM user u left join contact_info c on 
            	c.contact_info_id=u.contact_info_id where u.has_login_rights=1 and
            	(	(UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_admin_activity) > ". (int) $num_days."*60*60*24
            	and
            	UNIX_TIMESTAMP( ) - UNIX_TIMESTAMP( last_login_time) >". (int) $num_days."*60*60*24
            	)
            	or (last_login_time is null and last_admin_activity is null)
            	)
            	
            	 order by u.username";
        $result = $this->query($user_list_query);
        $this->track_method_exit("UserDB","get_list");
        
        return $result;
    }







    function add($userfields)
    {
    
    	$this->track_method_entry("UserDB","add");
    	
    	$user_id=-1;
    	 
        if (strlen($userfields['username']) != 0 && strlen($userfields['password']) != 0)
        {
        
        	$email=$this->prepare_string($userfields['email']);
        	$phone=$this->prepare_string($userfields['phone']);
        	$last_name=$this->prepare_string($userfields['last_name']);
        	$first_name=$this->prepare_string($userfields['first_name']);
        	
        	$user_add_query  = "INSERT INTO contact_info (";
            $user_add_query .= "email, phone, last_name, first_name";
            $user_add_query .= ") VALUES ('";
            $user_add_query .= $email."', '";
            $user_add_query .= $phone."', '";
            $user_add_query .= $last_name."', '";
            $user_add_query .= $first_name."') ";
            $contact_info_id = $this->execute_statement_return_autokey($user_add_query);
            
            if ($contact_info_id>0){
	            $user_add_query  = "INSERT INTO user (";
	            $user_add_query .= "username, has_login_rights, contact_info_id";
	            $user_add_query .= ") VALUES ('";
	            $user_add_query .= $this->prepare_string($userfields['username'])."',
	            ". (int) $userfields['has_login_rights'] .", ". $contact_info_id ."
	            )";
				$user_id=$this->execute_statement_return_autokey($user_add_query);
            	$error=0;
            	if ($user_id+0>0){
            		$sql="insert into user_password (user_id, password) values (". $user_id.", MD5('".$this->prepare_string($userfields['password']);
   					$sql.="'))";
            		$error=$this->execute_statement($sql);
            	}else{
            		echo "couldnt set password";
            		$user_id=-1;
            	}
            	if ($error+0>0){
            		echo "couldnt set password";
            		$user_id=-1;
            	}
            }
        }

		$this->track_method_exit("UserDB","add", "user_id", $user_id);
        
        return $user_id;
    }




    function update($user_id, $userfields)
    {
    		$this->track_method_entry("UserDB","update", "user_id", $user_id);
    		
    		        
        	$email=$this->prepare_string($userfields['email']);
        	$phone=$this->prepare_string($userfields['phone']);
        	$last_name=$this->prepare_string($userfields['last_name']);
        	$first_name=$this->prepare_string($userfields['first_name']);
        	
        	
            $user_update_query  = "UPDATE contact_info ci, user u SET ";
            $user_update_query .= "has_login_rights=";
            $user_update_query .= (int) $userfields['has_login_rights'];
            $user_update_query .= ", email='";
            $user_update_query .= $email;
            $user_update_query .= "', phone='";
            $user_update_query .= $phone;
            $user_update_query .= "', first_name='";
            $user_update_query .= $first_name;
            $user_update_query .= "', last_name='";
            $user_update_query .= $last_name;
            $user_update_query .= "' WHERE ci.contact_info_id=u.contact_info_id and u.user_id=" . (int) $user_id;
            $this->execute_statement($user_update_query);
            
            $this->track_method_exit("UserDB","update");
    }
   
   
   
   
   function change_user_password($user_id, $new_password){
   
   		$this->track_method_entry("UserDB","change_user_password", "user_id", $user_id);
   	
   		$sql="update user_password set password=MD5('".$this->prepare_string($new_password);
   		$sql.="') where user_id=". (int) $user_id;
   		$this->execute_statement($sql);
   		$this->track_method_exit("UserDB","change_user_password");
   	
   }
   

 	function update_last_activity_date()
    {
    	$user_id=$_SESSION['session_user_id'];
    	if ($user_id+0>0){
    		$update_last_activity_date="update user set last_admin_activity=Now() where user_id=". (int) $user_id;
    		$this->execute_statement($update_last_activity_date);
    	}
	}
	
	
    function authenticate($username, $password)
    {
    
    	$this->track_method_entry("UserDB","authenticate", "username", $username);
    	
    	$username=$this->prepare_string($username);
    	$password=$this->prepare_string($password);
    	
    	
        $user_authentication_query  = "SELECT u.username, u.user_id FROM user u, user_password up "
        ." WHERE u.has_login_rights=1 and up.user_id=u.user_id and u.username='";
        $user_authentication_query .= $username . "' AND up.password=MD5('" . $password . "')";

        $result = $this->query($user_authentication_query);

        if (count($result) > 0)
        {

            $user_row = array_pop($result);
            $user_id = $user_row['user_id'];
            $update_last_login_query="update user set last_login_time=Now() where user_id=".$user_id;
            $this->execute_statement($update_last_login_query);
            $ret=$user_id;
        } else
        {
            $ret=0;
        }
        
        $this->track_method_exit("UserDB","authenticate", "can_login", $ret);
        
        return $ret;
    }
    
    
    
    function validate_old_password($user_id, $password){
    	
    	$this->track_method_entry("UserDB","validate_old_password", "user_id", $user_id);
    	
    	$sql="SELECT user_id FROM user_password where user_id=". (int) $user_id." and password=MD5('".$this->prepare_string($password)."')";
    	$result = $this->query($sql);
    	if (is_array($result) && sizeof($result)>0){
    		$ret=1;
    	}else{
    		$ret=0;
    	}
    	$this->track_method_exit("UserDB","validate_old_password");
    	
    	return $ret;
    }
    
    function update_update_user_temp_password($user_id, $temp_password){
        	
        	$this->track_method_entry("UserDB","update_update_user_temp_password" , "user_id", $user_id);
    		
            $user_detail_query = "update ...";
            $user = array_pop($resultset);
            
            $this->track_method_exit("UserDB","update_update_user_temp_password");
            
            return $user;
    }
}
