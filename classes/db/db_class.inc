<?php
//--------------------------------------------
//Indybay DB Class
//Written December 2005 - January 2006
//modified from an sf-active class
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"DB");

require_once(CLASS_PATH."/common_class.inc");
require_once(CLASS_PATH."/renderer/renderer_class.inc");


//common DB classes used as a wrapper around all database access
class DB extends Common
{
	
	//get conection
    function get_connection()
    {
    
    	ini_set("mssql.timeout", "120");
    	
    	$this->track_method_entry("DB","get_connection");
        

        
        if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
	        @$GLOBALS['db_conn'] = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
	        
	        $i=0;
	        while ($i<7 && $GLOBALS['db_conn']==""){
	        	sleep(5*i);
	        	@$GLOBALS['db_conn'] = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
	        	$i=$i+1;
	        }
	        
	        if ($GLOBALS['db_conn']==""){
	        	$GLOBALS['db_down']="1";
	        }else{
	        	@$GLOBALS['db_conn']->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
		        @mysqli_select_db($GLOBALS['db_conn'], DB_DATABASE);
		        @mysqli_set_charset($GLOBALS['db_conn'], 'utf8');
	        }
        }
        $this->track_method_exit("DB","get_connection");
    }


	//runs a sql query
    function query($sql)
    {
    	$this->track_method_entry("DB","query");
    	
    	if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
	        $this->get_connection();
	
	        @$result_prt = mysqli_query($GLOBALS['db_conn'], $sql);
	
	        $error_num=$this->error_num($GLOBALS['db_conn']);
	
	        if ($error_num < 0)
	        {
	            $result=$error_num;
	        } else{
	            $result_array = array();
	            $i=0;
	            while (@$next_row = mysqli_fetch_array($result_prt))
	            {
	                $result_array[$i]=$next_row;
	                $i=$i+1;
	            }
	            $result=$result_array;
	        }
	        
	        if ($error_num<0)
	        	$this->track_method_exit("DB","query",$sql,"ERROR", $error_num);
	        else
	        	$this->track_method_exit("DB","query");
        }
        return $result;
    }
    
    
    function single_column_query($sql)
    {
	    $this->track_method_entry("DB","single_column_query");
        
        if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
	        if (!array_key_exists('db_conn',$GLOBALS) || !$GLOBALS['db_conn']){
	        	$this->get_connection();
	        }
	        @$result_prt = mysqli_query($GLOBALS['db_conn'], $sql);
	        $error_num=$this->error_num($GLOBALS['db_conn']);
	
	        if ($error_num < 0)
	        {
	             $result= $error_num;
	        } else{
	            $result_array = array();
	            while (@$next_row = mysqli_fetch_array($result_prt))
	            {
	                array_push($result_array, array_pop($next_row));
	            }
	            $result= $result_array;
	        }
	        
	                
	        if ($error_num<0)
	        	$this->track_method_exit("DB","single_column_query","sql",$sql,"ERROR", $error_num);
	        else
	        	$this->track_method_exit("DB","single_column_query");
        }
        return $result;	
    }
    
    

    function execute_statement($sql)
    {
    	$error_num=0;
    	if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
	    	$this->track_method_entry("DB","execute_statement");
	        if (!array_key_exists("db_conn",$GLOBALS)) $this->get_connection();
	        @mysqli_query($GLOBALS['db_conn'], $sql);
	        $error_num = $this->error_num($GLOBALS['db_conn']);
	       	if ($error_num<0)
	        	$this->track_method_exit("DB","execute_statement","sql",$sql,"ERROR", $error_num);
	        else
	        	$this->track_method_exit("DB","execute_statement");
        }
        return $error_num;
    }

    function execute_statement_return_autokey($sql)
    {
    	if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
	    	$this->track_method_entry("DB","execute_statement_return_autokey");
	        if (!isset($GLOBALS['db_conn'])) $this->get_connection();
	        @mysqli_query($GLOBALS['db_conn'], $sql);
	        $error_num=$this->error_num($GLOBALS['db_conn']);
	        if ($error_num==0)
	        {
	            $result_num=mysqli_insert_id($GLOBALS['db_conn']);
	        } else{
	            $result_num=$error_num;
	        }
	       	if ($error_num<0)
	        	$this->track_method_exit("DB","execute_statement_return_autokey","sql",$sql,"ERROR", $error_num);
	        else
	        	$this->track_method_exit("DB","execute_statement_return_autokey");
	    }
        return $result_num;
    }


	function prepare_string($str) {
		if (empty($GLOBALS['db_conn'])) {
			$this->get_connection();
		}
		$str = $this->convertcharsets($str);
		$str = mysqli_real_escape_string($GLOBALS['db_conn'], $str);
		return $str;
	}
	
	function prepare_reloaded_string($str){
		return $this->prepare_string($str);
	}
	
	function prepare_int($str) {
		return (int) $str;
	}
	
    
    function error_num()
    {
        //returns an error num as a negative number to distinguish it
        //from an id (in the case of inserts)
        $ret=-1;
        @$ret=-1*mysqli_errno($GLOBALS['db_conn']);
        return $ret; 
    }

    function close()
    {
    	    	
    	if ($GLOBALS['db_down']!="1"){
        	mysqli_close($GLOBALS['db_conn']);
        }
    }
    
    
    
	function convertcharsets($html_string){

		$renderer= new Renderer();
		
		if (!$renderer->is_utf8($html_string)){
			$html_string2=$html_string;
			@$html_string2=iconv("Windows-1252", 'UTF-8', $html_string2);
			if (mb_strlen($html_string2)<mb_strlen($html_string)*0.75  || mb_strpos(' '.$html_string2,'Â')>0   ){
				$html_string2=mb_convert_encoding($html_string,'UTF-8');
			}
			if (mb_strlen($html_string2)<mb_strlen($html_string)*0.75 || mb_strpos($html_string2,'Â')){
				$html_string2=$html_string;
			}
			$html_string=$html_string2;
		}

		return $html_string;
	}
}
