<?php
//--------------------------------------------
//Indybay BlurbDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"BlurbDB");

require_once (CLASS_PATH."/db/news_item_version_db_class.inc");
require_once (CLASS_PATH."/db/feature_page_db_class.inc");


//class for saving and loading info for center column blurbs
class BlurbDB extends NewsItemVersionDB
{


	//gets info for the current blurbs on a page
    function get_current_page_info_for_blurb($news_item_id){
    	
    	$this->track_method_entry("BlurbDB","get_current_page_info_for_blurb", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;
    	$page_list= array();
    	$query="select p.* from page p,
    		news_item_page_order nipo where 
    		nipo.page_id=p.page_id and nipo.news_item_id=$news_item_id";
    	$page_info_list=$this->query($query);
    	
    	$this->track_method_exit("BlurbDB","get_current_page_info_for_blurb");
    	
    	return $page_info_list;
    }
    
    
    
    //gets info for the archived blurbs on a page
    function get_archived_page_info_for_blurb($news_item_id){
    	
    	$this->track_method_entry("BlurbDB","get_archived_page_info_for_blurb", "news_item_id", $news_item_id);
    	    	
    	$news_item_id = (int) $news_item_id;
    	$page_list= array();
    	$query="select p.* from page p,
    		page_category pc, news_item_category nic, news_item ni  where 
    		nic.news_item_id=$news_item_id and nic.category_id=pc.category_id and
    		ni.news_item_id=nic.news_item_id and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN." and
    		pc.page_id=p.page_id and p.page_id not in ( select page_id from 
    		news_item_page_order where news_item_id=$news_item_id)
    		";
    	//get rid of duplicates
    	$page_info_list=$this->query($query);
    	$new_list=array();
    	foreach ($page_info_list as $key=>$value){
    		$new_list[$key]=$value;
    	}
    	
    	$this->track_method_exit("BlurbDB","get_archived_page_info_for_blurb");
    	
    	return $new_list;
    }
    
    
    
    //gets info for the hidden blurbs on a page
    function get_hidden_page_info_for_blurb($news_item_id){
    	
    	$this->track_method_entry("BlurbDB","get_hidden_page_info_for_blurb", "news_item_id", $news_item_id);
    	
    	$news_item_id = (int) $news_item_id;
    	$page_list= array();
    	$query="select p.* from page p,
    		page_category pc, news_item_category nic, news_item ni  where 
    		nic.news_item_id=$news_item_id and nic.category_id=pc.category_id and
    		ni.news_item_id=nic.news_item_id and ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_HIDDEN." and
    		pc.page_id=p.page_id and p.page_id not in ( select page_id from 
    		news_item_page_order where news_item_id=$news_item_id)
    		";
    	//get rid of duplicates
    	$page_info_list=$this->query($query);
    	$new_list=array();
    	foreach ($page_info_list as $key=>$value){
    		$new_list[$key]=$value;
    	}
    	
    	$this->track_method_exit("BlurbDB","get_hidden_page_info_for_blurb");
    	
    	return $new_list;
    }
    

	
	//gets blurb info given a blurbs news_item_id
	function get_blurb_info_from_news_item_id($news_item_id) {
		
		$this->track_method_entry("BlurbDB","get_blurb_info_from_news_item_id", "news_item_id", $news_item_id);
		
		$news_item_db_class= new NewsItemDB;
		$version_id=$news_item_db_class->get_current_version_id($news_item_id);
		
		$version_info=$this->get_blurb_info_from_version_id($version_id);
		
		$this->track_method_exit("BlurbDB","get_blurb_info_from_news_item_id");
		
		return $version_info;
	}
	
	
	
	//gets blurb info given a blurbs version_id
	function get_blurb_info_from_version_id($version_id) {
		
		$this->track_method_entry("BlurbDB","get_blurb_info_from_version_id", "version_id", $version_id);
    	
    	$news_item_version_db_class= new NewsItemVersionDB;
    	$news_item_db_class= new NewsItemDB;
    	$version_info=$news_item_version_db_class->get_news_item_version_info_from_version_id($version_id);
    	$item_info=$news_item_db_class->get_news_item_info($version_info["news_item_id"]);
    	$blurb_info=array_merge($version_info, $item_info);

        // Get attached breaking news items. An index should make this query
        // pretty fast. For speed, do not sort and only get "Other" items.
        $query = 'SELECT news_item_id FROM news_item WHERE parent_item_id = '
          . $version_info['news_item_id'] . ' AND news_item_type_id = '
          . NEWS_ITEM_TYPE_ID_BREAKING . ' AND news_item_status_id = '
          . NEWS_ITEM_STATUS_ID_OTHER;
        $blurb_info['breaking_items'] = $this->query($query);
    	
    	$this->track_method_exit("BlurbDB","get_blurb_info_from_version_id");
    	
    	return $blurb_info;
    }


	//adds a blurb to a page and its associated categories
	function add_blurb_to_categories_and_page($news_item_id, $page_id){
	
		$this->track_method_entry("BlurbDB","add_blurb_to_categories_and_page", "news_item_id", $news_item_id, "page_id", $page_id);
		
		$feature_page_db_class= new FeaturePageDB();
		$this->add_blurb_to_page($news_item_id, $page_id);
		$category_list=$feature_page_db_class->get_category_ids_for_page($page_id);
		foreach ($category_list as $next_category_id){
			if (!$this->is_news_item_in_category($news_item_id,$next_category_id)){
				$this->add_news_item_category($news_item_id,$next_category_id);
			}
		}
		
		$this->track_method_exit("BlurbDB","get_blurb_info_from_version_id");
	}
	
	
	//adds blubs to pages associated with a category (as well as adding the 
	//category if it isnt there)
	function add_blurb_to_pages_for_given_category($news_item_id, $category_id){
		
		$this->track_method_entry("BlurbDB","add_blurb_to_pages_for_given_category", "news_item_id", $news_item_id, "category_id", $category_id);
		
		$feature_page_db_class= new FeaturePageDB;
		if (!$this->is_news_item_in_category($news_item_id,$category_id)){
				$this->add_news_item_category($news_item_id,$category_id);
		}
		$page_ids=$feature_page_db_class->get_associated_page_ids(array( $category_id));
		if (sizeof($page_ids)>0){
			foreach($page_ids as $page_id){
				$this->add_blurb_to_page($news_item_id, $page_id);
			}
		}
		
		$this->track_method_exit("BlurbDB","add_blurb_to_pages_for_given_category");
	}
	
	
	//adds blurb to a page
	function add_blurb_to_page($news_item_id, $page_id){
	
		$this->track_method_entry("BlurbDB","add_blurb_to_page", "news_item_id", $news_item_id, "page_id", $page_id);
		
		//make sure blurb isnt already added
		
		//make sure this really is a blurb

		//get max_ordernum on page < 10000
		
		$news_item_id = (int) $news_item_id;
		$page_id = (int) $page_id;
		$order_num=10;
		$query = 'SELECT MAX(order_num) FROM news_item_page_order WHERE page_id = ' . $page_id . ' AND order_num < 10000';
		$numlist=$this->single_column_query($query);
		if (sizeof($numlist>0)){
			$order_num=array_pop($numlist)+10;
		}
		
        $query = "insert into news_item_page_order (news_item_id, page_id, order_num, display_option_id)";
        $query .= " values ($news_item_id, $page_id, $order_num,0)";
        $this->execute_statement($query);
        
        $this->track_method_exit("BlurbDB","add_blurb_to_page");
        
        return 1;
    }
    
    
    //removes a blurb from the current page (archiving it)
   	function remove_blurb_from_page($news_item_id, $page_id){
   	
   		$this->track_method_entry("BlurbDB","remove_blurb_from_page", "news_item_id", $news_item_id, "page_id", $page_id);
   		
        $news_item_id = (int) $news_item_id;
        $page_id = (int) $page_id;
        $query = "delete from  news_item_page_order  where news_item_id=$news_item_id
        	and page_id=$page_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("BlurbDB","remove_blurb_from_page");
        
        return 1;
    }
    
    
    
    //adds a new blurb  
    function create_new_blurb($post_array){
    
    	$this->track_method_entry("BlurbDB","create_new_blurb");
    	
    	$post_array['version_created_by_id']=$_SESSION["session_user_id"];
    	$post_array['media_type_grouping_id']=0;
    	$post_array['thumbnail_media_attachment_id']=$post_array['thumbnail_media_attachment_id']+0;
    	if (!isset($post_array['is_summary_html']))
    		$post_array['is_summary_html']=0;
    	if (!isset($post_array['is_text_html']))
    		$post_array['is_text_html']=0;


    	$post_array['display_contact_info']=0;
    	$post_array['event_duration']=0;

    		
    	if (trim($post_array['media_attachment_id'])==""){
    		$post_array['media_attachment_id']=0;
    	}
    	$news_item_db_class= new NewsItemDB;
    	$news_item_id=$news_item_db_class->add_news_item_for_registered_user(NEWS_ITEM_STATUS_ID_NEW, NEWS_ITEM_TYPE_ID_BLURB, 0);
    	$news_item_version_db_class= new NewsItemVersionDB;
    	$news_item_version_id=$news_item_version_db_class->add_news_item_version($news_item_id, $post_array);

		$this->track_method_exit("BlurbDB","remove_blurb_from_page", "news_item_id", $news_item_id);
    	
    	return $news_item_id;
    }
    
    
    
    //updates an order entry (reordering and changing template)
    function update_order_entry($page_id, $news_item_id, $order_num, $display_option_id){

		$this->track_method_entry("BlurbDB","update_order_entry", "news_item_id", $news_item_id, "page_id", $page_id);
    	
        $page_id = (int) $page_id;
        $news_item_id = (int) $news_item_id;
        $order_num = (int) $order_num;
        $display_option_id = (int) $display_option_id;
    	if ($order_num==0)
    		return -1;
        $query = "update news_item_page_order set order_num=$order_num,
        	display_option_id=$display_option_id where page_id=$page_id and
        	news_item_id=$news_item_id";
        $this->execute_statement($query);
        
        $this->track_method_exit("BlurbDB","update_order_entry");
        
        return 1;
    }
    
    
    
    //adds a new version entry for a blurb
    function create_new_version($news_item_id, $post_array){

		$this->track_method_entry("BlurbDB","create_new_version", "news_item_id", $news_item_id);
		
    	$post_array['version_created_by_id']=$_SESSION['session_user_id'];
    	$post_array['media_type_grouping_id']=0;
    	
    	$post_array['thumbnail_media_attachment_id']=$post_array['thumbnail_media_attachment_id']+0;
    	$post_array['event_duration']=0;
    	if (!isset($post_array['is_summary_html']))
    		$post_array['is_summary_html']=0;
    	if (!isset($post_array['is_text_html']))
    		$post_array['is_text_html']=0;
    	$post_array['display_contact_info']=0;

    	if (trim($post_array['media_attachment_id'])==""){
    		$post_array['media_attachment_id']=0;
    	}
    	$news_item_version_db_class= new NewsItemVersionDB;
    	$news_item_version_id=$news_item_version_db_class->add_news_item_version_if_changed($news_item_id, $post_array);

		$this->track_method_exit("BlurbDB","update_order_entry", "news_item_version_id", $news_item_version_id);
		
    	return $news_item_version_id;
    }
    
    
    
    //removes a blurb from all categories associated with a page
    function remove_blurb_from_category($news_item_id, $page_id){
    
    	$this->track_method_entry("BlurbDB","remove_blurb_from_category", "news_item_id", $news_item_id, "page_id", $page_id);
    	
    	$this->remove_blurb_from_page($news_item_id, $page_id);
    	$feature_page_db_class= new FeaturePageDB;
    	$category_list=$feature_page_db_class->get_category_ids_for_page($page_id);
    	foreach($category_list as $category_id){
    		$this->remove_news_item_category($news_item_id,$category_id);
    	}
    	
    	$this->track_method_exit("BlurbDB","remove_blurb_from_category");
    	
    }
    
}//end class
