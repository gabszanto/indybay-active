<?php
//--------------------------------------------
//Indybay MediaAttachmentDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"MediaAttachmentDB");

require_once (CLASS_PATH."/db/calendar_db_class.inc");



//class which loads the seperate sections for the newswires on the various feature pages
//the "newswire" that has the search options at the top is dealt with in the 
//SearchDB class
class EventListDB extends CalendarDB
{

	//returns all events related to a given page for the list at the bottom of feature pages
 	function get_list_for_page($page_id){
 	
 		$this->track_method_entry("EventListDB","get_list_for_page");
 		
 		$feature_page_db_class= new FeaturePageDB();
 		$page_info=$feature_page_db_class->get_feature_page_info($page_id);
 		$max_num_days=0;
 		if (isset($page_info[ "max_timespan_in_days_for_full_event_list"  ]))
 			$max_num_days=$page_info[ "max_timespan_in_days_for_full_event_list"  ];
 		if ($max_num_days==0)
 			$max_num_days=100;
 		$max_date_timestamp= time()+(60*60*24)*$max_num_days;
 		$max_num_items=$page_info["max_events_in_full_event_list"];
 		if ($max_num_items==0)
 			$max_num_items=30;
 		$category_list=$feature_page_db_class->get_category_ids_for_page($page_id);
 		$ret=$this->get_list_for_categories_helper($category_list,$max_num_items, $max_date_timestamp);
 	
 		$this->track_method_exit("EventListDB","get_list_for_page");
 		
 		return $ret;
 	}
 	
 	
	//returns all events related to a given page that are marked as highlighted
	//for the list at the top of feature pages (redlinks)
 	function get_highlighted_event_list_for_page($page_id){
 		
 		$this->track_method_entry("EventListDB","get_highlighted_event_list_for_page");
 		
 		$feature_page_db_class= new FeaturePageDB();
 		$page_info=$feature_page_db_class->get_feature_page_info($page_id);
 		
 		$max_num_days=$page_info["max_timespan_in_days_for_highlighted_event_list" ];
 		if ($max_num_days==0)
 			$max_num_days=30;
 		$max_date_timestamp= time()+(60*60*24)*$max_num_days;
 		$max_num_items=$page_info[ "max_events_in_highlighted_list"  ];
 		if ($max_num_items==0)
 			$max_num_items=12;
 		if ($page_info["event_list_type_id"]==3){
 			$ret=$this->get_highlighted_list_helper($max_num_items, $max_date_timestamp);
 		}else{
 			$category_list=$feature_page_db_class->get_category_ids_for_page($page_id);
 			$ret=$this->get_highighted_list_for_categories($category_list,$max_num_items, $max_date_timestamp);
 		}
 		
 		
 		$this->track_method_exit("EventListDB","get_highlighted_event_list_for_page");
 		
 		return $ret;
 	}
 
 
 //function used by others in this class to create the where clause for
 //lists of categories) This assumes that the relationship is "or" which no pages
 //yet are (see the comment at the top of this file for the issues with this)
  function get_category_or_list($category_list){
  	
  	$this->track_method_entry("NewswireDB","get_category_or_list");
  	
  	if (is_array($category_list)){
	  	if (sizeof($category_list>1))
	  		$or_list=" (";
	  	$i=0;
	  	
	  	foreach($category_list as $cat_id){
	  		$cat_id = (int) $cat_id;
	  		if ($i>0)
	  			$or_list.=" or ";
	  		$or_list.=" nic.category_id=".$cat_id." ";
	  		$i=$i+1;
	  	}
	  	if (sizeof($category_list>1))
	  		$or_list.=")";
	  	
	  	$this->track_method_exit("NewswireDB","get_category_or_list");
  	}
  	return $or_list;
  }
  
  
  //given a category list returns a list of highlighted events for those categories
   function get_highighted_list_for_categories($category_list,$max_num_items, $max_date_timestamp){
   		
   		$this->track_method_entry("NewswireDB","get_highighted_list_for_categories");
   		
   		$ret=$this->get_highlighted_list_for_categories_helper($category_list,$max_num_items,$max_date_timestamp);
   		
   		$this->track_method_exit("NewswireDB","get_highighted_list_for_categories");
   		
   		return $ret;
   		
   }
   

  //helper class used for getting lists of highlighted lists (redlinks)
   function get_highlighted_list_helper($max_num_items,$max_date_timestamp){
  	$this->track_method_entry("NewswireDB","get_highlighted_list_helper");
  	
  		$max_num_items = (int) $max_num_items;
  		$query="select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ";
  		$query.=" unix_timestamp(niv.displayed_date ) as displayed_timestamp ";
  		$query.=" from news_item_version niv, news_item ni ";
  		$query.=" where ni.current_version_id=niv.news_item_version_id and news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT; 
  		$query.=" and (ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED." or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED.") ";
  		$query.=" and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60 and unix_timestamp(niv.displayed_date) < '" . $this->prepare_string($max_date_timestamp) . "'";
  		$query.=" order by niv.displayed_date limit 0 , ".$max_num_items;
  	$db_obj = new DB();

  	$result=$db_obj->query($query);
  	$this->track_method_exit("NewswireDB","get_highlighted_list_helper");
  	
  	return $result;
  	
  }
  
  //helper class used for getting lists of highlighted lists (redlinks)
  function get_highlighted_list_for_categories_helper($category_list,$max_num_items,$max_date_timestamp){
  	$this->track_method_entry("NewswireDB","get_highlighted_list_for_categories_helper");
  	
  	if (!array_key_exists('db_down',$GLOBALS) || $GLOBALS['db_down']!="1"){
  	
	  		$max_num_items = (int) $max_num_items;
	  		$query="select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ";
	  		$query.=" unix_timestamp(niv.displayed_date ) as displayed_timestamp ";
	  		$query.=" from news_item_version niv, news_item ni, news_item_category nic ";
	  		$query.=" where ni.current_version_id=niv.news_item_version_id and news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT; 
	  		$query.=" and  nic.news_item_id=ni.news_item_id and ".$this->get_category_or_list($category_list);
	  		$query.=" and (ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED." or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED." or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED.") ";
	  		$query.=" and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*24 ";
	  		$query.=" and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*niv.event_duration and unix_timestamp(niv.displayed_date) < '" . $this->prepare_string($max_date_timestamp) . "'";
	  		$query.=" order by niv.displayed_date limit 0 , ".$max_num_items;
	
	  	$db_obj = new DB();
	
	  	$result=$db_obj->query($query);
	}
  	$this->track_method_exit("NewswireDB","get_highlighted_list_for_categories_helper");
  	
  	return $result;
  	
  }
  
  //helper class used for getting lists of highlighted lists (redlinks)
    function get_list_for_categories_helper($category_list,$max_num_items,$max_date_timestamp){
  	$this->track_method_entry("NewswireDB","get_list_for_categories_helper");
  	
  		$max_num_items = (int) $max_num_items;
  		$query="select niv.title1, niv.news_item_id, unix_timestamp(ni.creation_date) as creation_timestamp, ";
  		$query.=" unix_timestamp(niv.displayed_date ) as displayed_timestamp ";
  		$query.=" from news_item_version niv, news_item ni, news_item_category nic ";
  		$query.=" where ni.current_version_id=niv.news_item_version_id and news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT; 
  		$query.=" and  nic.news_item_id=ni.news_item_id and ".$this->get_category_or_list($category_list);
  		$query.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN;
  		$query.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN;
  		$query.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_NEW;
  		$query.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN;
  		$query.=" and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*24 ";
  		$query.=" and unix_timestamp(niv.displayed_date)>unix_timestamp(Now())-60*60*niv.event_duration and unix_timestamp(niv.displayed_date) < '" . $this->prepare_string($max_date_timestamp) . "'";
  		$query.=" order by niv.displayed_date limit 0 , ".$max_num_items;

  	$db_obj = new DB();

  	$result=$db_obj->query($query);

  	$this->track_method_exit("NewswireDB","get_list_for_categories_helper");
  	
  	return $result;
  	
  }
  

} //end class
