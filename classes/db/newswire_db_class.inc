<?php
//--------------------------------------------
//Indybay MediaAttachmentDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"MediaAttachmentDB");

require_once (CLASS_PATH."/db/db_class.inc");



//class which loads the separate sections for the newswires on the various feature pages
//the "newswire" that has the search options at the top is dealt with in the 
//SearchDB class
class NewswireDB extends DB
{


  
   //returns the local newswire data given a list of categories
   function get_local_list_for_categories($category_list,$items_per_newswire_section){
   		
   		$this->track_method_entry("NewswireDB","get_local_list_for_categories");
   		
   		$additional_and=" and ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED." ";
  		
   		$ret=$this->get_list_for_categories_helper($category_list,$items_per_newswire_section,$additional_and,1);
   		
   		$this->track_method_exit("NewswireDB","get_local_list_for_categories");
   		
   		return $ret;
   		
   }
   
   
   
   
   //returns the nolocal/global newswire data given a list of categories
   function get_nonlocal_list_for_categories($category_list,$items_per_newswire_section){
   		
   		$this->track_method_entry("NewswireDB","get_nonlocal_list_for_categories");
   		
   		$additional_and=" and ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED." ";
   		$ret=$this->get_list_for_categories_helper($category_list,$items_per_newswire_section,$additional_and,0);
   		
   		$this->track_method_exit("NewswireDB","get_nonlocal_list_for_categories");
   		
   		return $ret;
   }
   
   
   
   
   
   //returns the other/breaking newswire data given a list of categories
   function get_other_list_for_categories($category_list,$items_per_newswire_section){
   		
   		$this->track_method_entry("NewswireDB","get_other_list_for_categories");
   		
   		$additional_and=" and (ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED."
   		 or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED."
   		  or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_OTHER."
   		  or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN."
   		  or ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_NEW." )
   		  ";
   		$ret=$this->get_list_for_categories_helper($category_list,$items_per_newswire_section,$additional_and,0);
   
   		$this->track_method_exit("NewswireDB","get_other_list_for_categories");
   
   		return $ret;
   }
   
   
   
   
   //returns  open newswire data given a list of categories
   function get_all_list_for_categories($category_list,$items_per_newswire_section){
   
   		$this->track_method_entry("NewswireDB","get_all_list_for_categories");
   	
   		$additional_and=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN."  ";
   		$additional_and.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN."  ";
   		$ret=$this->get_list_for_categories_helper($category_list,$items_per_newswire_section,$additional_and,1);
   
   		$this->track_method_exit("NewswireDB","get_all_list_for_categories");
   
   		return $ret;
   }
  
  
  
  
   //returns  classified newswire data given a list of categories
   function get_classified_list_for_categories($category_list,$items_per_newswire_section){
   		
   		$this->track_method_entry("NewswireDB","get_classified_list_for_categories");
   		
   		$additional_and=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN." ";
   		$additional_and.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN." ";
   		$additional_and.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_OTHER." ";
   		$additional_and.=" and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_NEW." ";
   		$ret=$this->get_list_for_categories_helper($category_list,$items_per_newswire_section,$additional_and,1);
   
   		$this->track_method_exit("NewswireDB","get_classified_list_for_categories");
   		
   		return $ret;
   }
  
  
  
  
  //helper function that returns newswires given portions of where clause
  function get_list_for_categories_helper($category_list,$items_per_newswire_section, $additional_and, $include_events){
  	
  	$this->track_method_entry("NewswireDB","get_list_for_categories_helper");
  	
  	$items_per_newswire_section = (int) $items_per_newswire_section;
  	$num_cats=sizeof($category_list);
  	$temp_num_items=$items_per_newswire_section;
  	if ($num_cats>1)
  		$temp_num_items=$num_cats*items_per_newswire_section;
  	if (sizeof($category_list)==0){
  		$query="select niv.title1, niv.news_item_id, niv.displayed_author_name, ni.creation_date, ";
  		$query.=" unix_timestamp(ni.creation_date) as creation_timestamp, ni.news_item_type_id, ni.num_comments, ";
  		$query.=" date_format(ni.creation_date,'%W %b %D %l:%i %p') as created, ";
  		$query.=" ni.media_type_grouping_id from news_item_version niv, news_item ni ";
  		$query.=" where ni.current_version_id=niv.news_item_version_id and ( news_item_type_id=".NEWS_ITEM_TYPE_ID_POST;
  		if($include_events==1){
  			$query.=" or news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT ;
  		}
  		$query.=" ) and (ni.parent_item_id=ni.news_item_id)";
  		$query.=$additional_and;
  		$query.=" order by ni.news_item_id desc limit 0,$items_per_newswire_section";
  	}else{
  	
  		$query="select niv.title1, niv.news_item_id, niv.displayed_author_name, ni.creation_date, ni.news_item_type_id, ni.num_comments,  ";
  		$query.=" unix_timestamp(ni.creation_date) as creation_timestamp, ";
  		$query.=" date_format(ni.creation_date,'%W %b %D %l:%i %p') as created, ";
  		$query.=" ni.media_type_grouping_id from news_item_version niv, news_item ni, news_item_category nic ";
  		$query.=" where ni.current_version_id=niv.news_item_version_id and ( news_item_type_id=".NEWS_ITEM_TYPE_ID_POST;
  		
  		if($include_events==1){
  			$query.=" or news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT ;
  		}
  		
  		$query.=" ) and (ni.parent_item_id=ni.news_item_id) ";
  		$query.=" and  nic.news_item_id=ni.news_item_id and ".$this->get_category_or_list($category_list);
  		$query.=$additional_and;
  		$query.=" order by nic.news_item_id desc limit 0 , ".$temp_num_items;
  	}
  	$db_obj = new DB();
  	$result=$db_obj->query($query);

  	if ($num_cats>1){
  		$result=$this->remove_duplicates_and_restrict_size($result,$items_per_newswire_section);
  	}
  	$result=$result;
  	
  	$this->track_method_exit("NewswireDB","get_list_for_categories_helper");
  	
  	return $result;
  	
  }
  
  //removes duplicates that could be caused by how where clause is setup
  //(these are duplicates in the results with same id not duplicates in terms of contents)
  function remove_duplicates_and_restrict_size($result,$items_per_newswire_section ){
  
  	$this->track_method_entry("NewswireDB","remove_duplicates_and_restrict_size");
  	
  	$new_results=array();
  	$prev_news_item_id=0;
  	$i=1;
  	foreach ($result as $row){
  		$next_news_item_id=$row[news_item_id];
  		if ($next_news_item_id!=$prev_news_item_id && $i<$items_per_newswire_section){
  			array_push($new_results,$row);
  		}
  		$prev_news_item_id=$next_news_item_id;
  		$i=$i+1;
  	}
  	array_reverse($new_results);
  	
  	$this->track_method_exit("NewswireDB","remove_duplicates_and_restrict_size");
  	
  	return $new_results;
  }
  
   
  //helper function that sets up a list of ors in the where clauses for newswire queries
  function get_category_or_list($category_list){
  	
  	$this->track_method_entry("NewswireDB","get_category_or_list");
  	
  	if (sizeof($category_list>1))
  		$or_list=" (";
  	$i=0;
  	foreach($category_list as $cat_id){
  		if ($i>0)
  			$or_list.=" or ";
  		$or_list.=" nic.category_id=". (int) $cat_id." ";
  		$i=$i+1;
  	}
  	if (sizeof($category_list>1))
  		$or_list.=")";
  	
  	$this->track_method_exit("NewswireDB","get_category_or_list");
  		
  	return $or_list;
  }
  
  

} //end class
