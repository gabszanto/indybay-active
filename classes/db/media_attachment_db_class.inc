<?php
//--------------------------------------------
//Indybay MediaAttachmentDB Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"MediaAttachmentDB");

require_once (CLASS_PATH."/db/db_class.inc");

class MediaAttachmentDB  extends DB
{
   
   //adds a media attachment to the DB table (used for posts and admin uploads)
    function add_media_attachment($upload_name, $file_name, $relative_path, $alt_tag, $original_file_name, 
    	  $media_type_id, $created_by_id,
     		$upload_type_id, $upload_status_id, $parent_item_id=0,
		$file_size=0, $image_width=0, $image_height=0){
     		
     		$this->track_method_entry("MediaAttachmentDB","add_media_attachment");
     		
     		$media_type_id = (int) $media_type_id;
     		$created_by_id = (int) $created_by_id;
     		$upload_type_id = (int) $upload_type_id;
     		$upload_status_id = (int) $upload_status_id;
     		$parent_item_id = (int) $parent_item_id;
     		$file_size = (int) $file_size;
     		$image_width = (int) $image_width;
     		$image_height = (int) $image_height;
     		$query="insert into media_attachment (upload_name, file_name, relative_path, alt_tag, original_file_name,   media_type_id, created_by_id,
     			upload_type_id, upload_status_id, creation_date,
			parent_attachment_id, file_size, image_width, image_height) values (
     			'".$this->prepare_string($upload_name)."',
     			 '".$this->prepare_string($file_name)."',
     			 '" . $this->prepare_string($relative_path) . "',
     			 '".$this->prepare_string(mb_substr($alt_tag, 0, 200, 'UTF-8'))."',
     			  '".$this->prepare_string($original_file_name)."',
     			$media_type_id, $created_by_id,
     			$upload_type_id, $upload_status_id, Now(),
			$parent_item_id, $file_size, $image_width, $image_height )";
	
     		$media_attachment_id=$this->execute_statement_return_autokey($query);
     		$this->track_method_exit("MediaAttachmentDB","add_media_attachment", "media_attachment_id", $media_attachment_id);

     		return $media_attachment_id;
     }
   
   
     
	//returns info about a media attachment for use on admin pages or when rendering
     function get_media_attachment_info($media_attachment_id){
     	 
     	 $this->track_method_entry("MediaAttachmentDB","get_media_attachment_info", "media_attachment_id", $media_attachment_id);
     	 
     	 $media_attachment_id = (int) $media_attachment_id;
     	 $return=""; 
     	 
     	 $query="select ma.*,
     	 		unix_timestamp(creation_date) as creation_timestamp, mat.mime_type, mat.media_type_grouping_id 
     	 		from media_attachment ma,
     	 		 ".
     			"media_type mat where ma.media_type_id=mat.media_type_id and ".
     	 		"ma.media_attachment_id=$media_attachment_id";
		 $ret=$this->query($query);
		 if (is_array($ret) && sizeof($ret)>0){
		 	$return=array_pop($ret);
		 	$query="select name from upload_type where upload_type_id=".$return["upload_type_id"];
		 	$ret=$this->single_column_query($query);
		 	if (is_array($ret) && sizeof($ret)>0){
		 		$upload_type_name=array_pop($ret);
		 		$return["upload_type_name"]=$upload_type_name;
		 	}
		 }

     	 $this->track_method_exit("MediaAttachmentDB","get_media_attachment_info");
     	 
     	 return $return;
     }
     
     
     //returns a list of media attachments for section of admin that shows recent uploads
     function get_media_attachment_list($upload_type_id, $media_type_grouping_id, $creator_id,$page_size,$limit_start, $keyword=""){
     
     	$this->track_method_entry("MediaAttachmentDB","get_media_attachment_list");
     	
     	$query="select ma.*,  unix_timestamp(creation_date) as creation_timestamp, mt.* from media_attachment ma, media_type mt ".
     	"where ma.media_type_id=mt.media_type_id ";
     	
     	if (trim($keyword)!=""){
     		$query.=" and (file_name like '%".$this->prepare_string($keyword)."%' or original_file_name like '%".$this->prepare_string($keyword)."%') ";
     	}
     	
     	if ($upload_type_id!=0){
     		$upload_type_id = (int) $upload_type_id;
     		$query.=" and upload_type_id=".$upload_type_id;
     	}
     	
     	if ($media_type_grouping_id!=0){
     		$media_type_grouping_id = (int) $media_type_grouping_id;
     		$query.=" and media_type_grouping_id=".$media_type_grouping_id;
     	}
     	
     	if ($creator_id!=0){
     		$creator_id = (int) $creator_id;
     		$query.=" and creator_id=".$creator_id;
     	}
     	$limit_start = (int) $limit_start;
        $page_size = (int) $page_size;
     	$query.=" order by ma.creation_date desc limit $limit_start, $page_size";
   
     	$ret= $this->query($query);
     	 
     	$this->track_method_exit("MediaAttachmentDB","get_media_attachment_list");
     	
     	return $ret;
     }
     
     
     //gets list of recent admin uploads for use on blurb pages when someone is trying
     //to chose a photo for a blurb
     function get_recent_admin_upload_select_list(){
     	$upload_type_id=UPLOAD_TYPE_ADMIN_UPLOAD;
     	$media_type_grouping_id=MEDIA_TYPE_GROUPING_IMAGE;
     	$page_size=100;
     	$limit_start=0;
     	$creator_id=0;
     	$list=$this->get_media_attachment_list($upload_type_id, $media_type_grouping_id, $creator_id,$page_size,$limit_start);
     	$return_options=array();
     	if (is_array($list)){
	     	foreach ($list as $list_item){
	     		$media_attachment_id=$list_item["media_attachment_id"];
	     		$name=$media_attachment_id.") ".$list_item["file_name"];
	     		$name.=" (".$list_item["image_width"]." x ".$list_item["image_height"].")";
	     		$return_options[$media_attachment_id]=$name;
	     	}
     	}
     	return $return_options;
     }
     
         
    //returns list of media type groupings for search screens
    function get_medium_options(){
    
	    $this->track_method_entry("MediaAttachmentDB","get_medium_options");
    
    	$query="select media_type_grouping_id, name from media_type_grouping where media_type_grouping_id>0";
    	$res=$this->query($query);
    	$options=array();
    	if (is_array($res)){
	    	foreach ($res as $row){
	    		$options[$row["media_type_grouping_id"]]=$row["name"];
	    	}
    	}
    	
    	$this->track_method_exit("MediaAttachmentDB","get_medium_options");
    	
    	return $options;
    }
    
	
	//gets a list of upload types for use in searching options in media attachment section of admin
    function get_upload_types(){
    
	    $this->track_method_entry("MediaAttachmentDB","get_upload_types");
    
    	$query="select upload_type_id, name from upload_type";
    	$res=$this->query($query);
    	$options=array();
    	if (is_array($res)){
	    	foreach ($res as $row){
	    		$options[$row["upload_type_id"]]=$row["name"];
	    	}
    	}
    	
    	$this->track_method_exit("MediaAttachmentDB","get_upload_types");
    	
    	return $options;
    }
     
     
     //when an item is posted this determines its type in the DB based off the name of the file 
     //and any available MIME info 
     function get_media_type_id_from_file_extension_and_mime($file_ext, $mime){
     
     		$this->track_method_entry("MediaAttachmentDB","get_media_type_id_from_file_extension", "file_ext", $file_ext);
     		
     		$media_type_id=0;
     		$query = "select media_type_id from media_type where ".
     			" extension = '" . $this->prepare_string(strtolower(trim($file_ext))) . "' ".
     			" and mime_type = '" . $this->prepare_string($mime) . "'";
     		$result=$this->single_column_query($query);
     		if (sizeof($result)==0){
     			$query = "select media_type_id from media_type where extension = '" . $this->prepare_string(strtolower(trim($file_ext))) . "'";
     			$result=$this->single_column_query($query);
     		}
     		if (!is_array($result) || sizeof($result)==0){
     			$media_type_id=0;
     		}else{
     			$media_type_id=array_pop($result);
     		}
     		
     		$this->track_method_exit("MediaAttachmentDB","get_media_type_id_from_file_extension", "media_type_id", $media_type_id);
     		
     		return $media_type_id;
     }
     
     
     //given a type (such as a jpeg of gif or mov file) returns a grouping type (image, movie etc...)
     function get_media_type_grouping_id_from_type_id($media_type_id){
     
     		$media_type_grouping_id=0;
     		
     		$this->track_method_entry("MediaAttachmentDB","get_media_type_grouping_id_from_type_id", "media_type_id", $media_type_id);
      		$media_type_id = (int) $media_type_id;     		
     		$query="select media_type_grouping_id from media_type where  media_type_id=".$media_type_id;
     		$result=$this->single_column_query($query);
     		if (!is_array($result) || sizeof($result)==0)
     			return 0;
     		
     		if (is_array($result) && sizeof($result)>0)
     			$media_type_grouping_id=array_pop($result);
     		
     		$this->track_method_exit("MediaAttachmentDB","get_media_type_id_from_file_extension", "media_type_grouping_id", $media_type_grouping_id);
     		
     		return $media_type_grouping_id;
     }
     
     
     
     
     //updates the upload status for an image so the DB can record if the item really was uploaded sucessfully
     //this is mainly useful when the site is being setup as a mirror or dev site
     //or when an Indymedia is upgraded from a different code based (as when Indybay
     //got converted from sf-active)
     function update_upload_status($media_attachment_id, $new_status_id){
     
     	$this->track_method_entry("MediaAttachmentDB","update_upload_status", "media_attachment_id", $media_attachment_id);
     
     	$new_status_id = (int) $new_status_id;
        $media_attachment_id = (int) $media_attachment_id;
     	$sql="update media_attachment set upload_status_id=".$new_status_id."
     		where media_attachment_id=".$media_attachment_id;
     	$this->execute_statement($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","update_upload_status");
     }
     
     
     
     //updates the upload status and file location for an image so the DB can record if the item really was uploaded sucessfully
     //this is mainly useful when the site is being setup as a mirror or dev site
     //or when an Indymedia is upgraded from a different code based (as when Indybay
     //got converted from sf-active)
     function update_file_location_and_status($media_attachment_id, $new_file_rel_path,$new_file_name, $new_status){
     
     	$this->track_method_entry("MediaAttachmentDB","update_file_location_and_status", "media_attachment_id", $media_attachment_id);
     
     	$new_status = (int) $new_status;
     	$media_attachment_id = (int) $media_attachment_id;
     	$sql="update media_attachment set upload_status_id=".$new_status.", relative_path='" . $this->prepare_string($new_file_rel_path) . "',
     		file_name='" . $this->prepare_string($new_file_name) . "'
     		where media_attachment_id=".$media_attachment_id;
     	$this->execute_statement($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","update_file_location_and_status");
     }
     
     
     
     //updates the file size stored for an upload (mainly useful for legacy uploads that dont yet have this stored)
     function update_file_size($media_attachment_id, $file_size){
       	
       	$this->track_method_entry("MediaAttachmentDB","update_file_size", "media_attachment_id", $media_attachment_id);
       
        $file_size = (int) $file_size;
        $media_attachment_id = (int) $media_attachment_id;
        $sql="update media_attachment set file_size=".$file_size."
     		where media_attachment_id=".$media_attachment_id;
     		
     	$this->execute_statement($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","update_file_size");
     }
     
     
     
     //when images are rendered, more info becomes available like width and height, this method is needed
     //so that can be stored in the DB so lists of images can show size (as when you are tying to choose one
     //for a blurb)
     function update_image_and_file_size($media_attachment_id, $file_size, $image_width, $image_height){
        
        $this->track_method_entry("MediaAttachmentDB","update_image_and_file_size", "media_attachment_id", $media_attachment_id);
        
        $file_size = (int) $file_size;
        $media_attachment_id = (int) $media_attachment_id;
        $image_width = (int) $image_width;
        $image_height = (int) $image_height;
        $sql="update media_attachment set file_size=".$file_size.", image_width=".$image_width.",
        	image_height=".$image_height."
     		where media_attachment_id=".$media_attachment_id;
     		
     	$this->execute_statement($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","update_image_and_file_size");
     }
     
     
     //returns the news item or blurbs that use this image
     //this is currently mainly used to navigate back and forth between the media upload detail pages 
     //and news items that use them but it could also be used to delete old unused items from the DB
     //or delete images for hidden posts...
     function get_current_related_news_item_ids($media_attachment_id){
     	$this->track_method_entry("MediaAttachmentDB","get_current_related_news_item_ids", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id)";
     		
     	$result=$this->single_column_query($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","get_current_related_news_item_ids");
     	
     	return $result;
     }
     
     //returns the news item or blurbs that use this image
     //this is currently mainly used to navigate back and forth between the media upload detail pages 
     //and news items that use them but it could also be used to delete old unused items from the DB
     //or delete images for hidden posts...
     function get_current_nonhidden_related_news_item_ids($media_attachment_id){
     	$this->track_method_entry("MediaAttachmentDB","get_current_nonhidden_related_news_item_ids", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id) and ni.news_item_status_id<>".NEWS_ITEM_STATUS_ID_HIDDEN;
     		
     	$result=$this->single_column_query($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","get_current_nonhidden_related_news_item_ids");
     	
     	return $result;
     }
     
          //returns the news item or blurbs that use this image
     //this is currently mainly used to navigate back and forth between the media upload detail pages 
     //and news items that use them but it could also be used to delete old unused items from the DB
     //or delete images for hidden posts...
     function get_current_hidden_related_news_item_ids($media_attachment_id){
     	$this->track_method_entry("MediaAttachmentDB","get_current_hidden_related_news_item_ids", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="select ni.news_item_id from news_item ni, news_item_version niv
        	where ni.current_version_id=niv.news_item_version_id and 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id) and ni.news_item_status_id=".NEWS_ITEM_STATUS_ID_HIDDEN;
     		
     	$result=$this->single_column_query($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","get_current_hidden_related_news_item_ids");
     	
     	return $result;
     }
     
     //same as previous function but also lists news items that used to use the upload
     function get_all_related_news_item_ids($media_attachment_id){
     	$this->track_method_entry("MediaAttachmentDB","get_all_related_news_item_ids", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="select distinct(niv.news_item_id) from news_item_version niv
        	where 
        	(niv.media_attachment_id=$media_attachment_id or
        		niv.thumbnail_media_attachment_id=$media_attachment_id)";
     		
     	$result=$this->single_column_query($sql);
     	
     	$this->track_method_exit("MediaAttachmentDB","get_all_related_news_item_ids");
     	
     	return $result;
     }
     
     //updates the parent id and type of an image
     //this is mainly useful when the original image uploaded is not the one associated with the
     //post because it was too large
     function update_type_and_parent($media_attachment_id, $upload_type_id, $parent_attachment_id){
     	$this->track_method_entry("MediaAttachmentDB","update_type_and_parent", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $upload_type_id = (int) $upload_type_id;
        $parent_attachment_id = (int) $parent_attachment_id;
        $sql="update media_attachment set parent_attachment_id=".$parent_attachment_id.", upload_type_id=".$upload_type_id."
     		where media_attachment_id=".$media_attachment_id;
     		
     	$this->execute_statement($sql);
 
     	$this->track_method_exit("MediaAttachmentDB","update_type_and_parent");
     }
     
     
     //returns the parent attachment info for a media attachment
     //this is the media attachment that shows up with posts as opposed to
     //thumbnails whose ids may be handed into this
     function get_parent_attachment_info($media_attachment_id){
          	$this->track_method_entry("MediaAttachmentDB","get_parent_attachment_info", "media_attachment_id", $media_attachment_id);
        
        $info=$this->get_media_attachment_info($media_attachment_id);
        $sql="select * from media_attachment where media_attachment_id=".$info["parent_attachment_id"];

     	$result=$this->query($sql);

     	if (is_array($result) && sizeof($result)>0)
     		$res=array_pop($result);
     	else 
     		$res="";
     		
     	$this->track_method_exit("MediaAttachmentDB","get_parent_attachment_info");
     
     	return $res;
     }
     
     
     //returns lists of child attachments (normally thumbnails)
     function get_child_attachment_info_list($media_attachment_id){
          	$this->track_method_entry("MediaAttachmentDB","get_child_attachment_info_list", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="select ma.*, ut.name as type_name from media_attachment ma, upload_type ut
         where parent_attachment_id=".$media_attachment_id." and ut.upload_type_id=ma.upload_type_id";
   
     	$result=$this->query($sql);
     		
     	$this->track_method_exit("MediaAttachmentDB","get_child_attachment_info_list");
     	
     	return $result;
     }
     
     //deletes DB row for media attachment
     function delete_media_attachment($media_attachment_id){
        
        
        $this->track_method_entry("MediaAttachmentDB","delete_media_attachment", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $sql="delete from media_attachment where media_attachment_id=".$media_attachment_id;
     	$this->execute_statement($sql);

     	$sql="update news_item_version set media_attachment_id=0 where media_attachment_id=".$media_attachment_id;
     	$this->execute_statement($sql);

     	$sql="update  news_item_version set thumbnail_media_attachment_id=0 where thumbnail_media_attachment_id=".$media_attachment_id;
     	$this->execute_statement($sql);
  
     	$this->track_method_exit("MediaAttachmentDB","delete_media_attachment");
     }
     
     
    //given a media attachment id and an upload type returns children matching the type
    //an example would be looking for an image tat is a child of a pdf
    function get_thumbnail_given_parent_id_and_type($media_attachment_id, $upload_type_id){
    	$this->track_method_entry("MediaAttachmentDB","get_thumbnail_given_parent_id_and_type", "media_attachment_id", $media_attachment_id);
        
        $media_attachment_id = (int) $media_attachment_id;
        $upload_type_id = (int) $upload_type_id;
        $sql="select media_attachment_id from media_attachment
         where parent_attachment_id=".$media_attachment_id." and upload_type_id=".$upload_type_id;
   
   		$id=0;
     	$result=$this->single_column_query($sql);
     	if (is_array($result) && sizeof($result)>0)
     		$id=array_pop($result);
     	$this->track_method_exit("MediaAttachmentDB","get_thumbnail_given_parent_id_and_type");
     	
     	return $id;
	}
	
	//returns info on the original upload if the current item isnt the original upload
	//this will be the movie or large image associated with a displayed image or thumbnail
	function get_original_attachment_info_from_attachment_id($media_attachment_id){
	    	$this->track_method_entry("MediaAttachmentDB","get_original_attachment_info_from_attachment_id", "media_attachment_id", $media_attachment_id);
        $media_attachment_id = (int) $media_attachment_id;
        $attachment_info="";
        
        //first get_parent if this isnt the parent attachment
        $parent_info=$this->get_parent_attachment_info($media_attachment_id);
        if (is_array($parent_info) && sizeof($parent_info)>0){
        if ($parent_info["media_attachment_id"]!=$media_attachment_id)
        	$media_attachment_id=$parent_info["media_attachment_id"];
        }
        
        $sql="select media_attachment_id from media_attachment
         where parent_attachment_id=".$media_attachment_id." and upload_type_id=".UPLOAD_TYPE_POST_ORIGINAL_BEFORE_RESIZE;
   
   		$id=0;
     	$result=$this->single_column_query($sql);
     	if (is_array($result) && sizeof($result)>0){
     		$id=array_pop($result);
     		$attachment_info=$this->get_media_attachment_info($id);
     	}else{
     		$attachment_info=$this->get_media_attachment_info($media_attachment_id);
     	}
     	$this->track_method_exit("MediaAttachmentDB","get_original_attachment_info_from_attachment_id");
     	
     	return $attachment_info;
	}
	
	
	//returns torrents associated with an upload
	function get_associated_torrent_info($media_attachment_id){
	    	$this->track_method_entry("MediaAttachmentDB","get_associated_torrent_info", "media_attachment_id", $media_attachment_id);
	    	$media_attachment_id = (int) $media_attachment_id;
        	$attachment_info="";
        
	        //first get_parent if this isnt the parent attachment
//              I fixed a typo here but wasn't really sure what this does so commented out for now --mark
//	        $parent_info=$this->get_parent_attachment_info($media_attachment_id);
//	        if (is_array($parent_info) && sizeof($parent_info)>0){
//		        if ($parent_info["media_attachment_id"]!=$media_attachment_id)
//		        	$media_attachment_id=$parent_info["media_attachment_id"];
//	        }
	        
	        $sql="select media_attachment_id from media_attachment
	         where parent_attachment_id=".$media_attachment_id." and upload_type_id=".UPLOAD_TYPE_TORRENT;
	   
	   		$id=0;
	     	$result=$this->single_column_query($sql);
	     	if (is_array($result) && sizeof($result)>0){
	     		$id=array_pop($result);	
	     		$attachment_info=$this->get_media_attachment_info($id);
	     	}
	     	$this->track_method_exit("MediaAttachmentDB","get_associated_torrent_info");
	     	
	     	return $attachment_info;
	}
	
	//gets images associated with a pdf
	function get_renderered_pdf_info($media_attachment_id){
	
	    	$this->track_method_entry("MediaAttachmentDB","get_renderered_pdf_info", "media_attachment_id", $media_attachment_id);
	    	$media_attachment_id = (int) $media_attachment_id;
        	$attachment_info="";
        
	        //first get_parent if this isnt the parent attachment
	        $parent_info=$this->get_parent_attachment_info($media_attachment_id);
	        if (is_array($parent_info) && sizeof($parent_info)>0){
		        if ($parent_info["media_attachment_id"]!=$media_attachment_id)
		        	$media_attachment_id=$parent_info["media_attachment_id"];
	        }
	        
	        $sql="select media_attachment_id from media_attachment
	         where parent_attachment_id=".$media_attachment_id." and upload_type_id=".UPLOAD_TYPE_THUMBNAIL_MEDIUM;
	   
	   		$id=0;
	     	$result=$this->single_column_query($sql);
	     	
	     	if (is_array($result) && sizeof($result)>0){
	     		$id=array_pop($result);	
	     		$attachment_info=$this->get_media_attachment_info($id);
	     	}
	     	$this->track_method_exit("MediaAttachmentDB","get_renderered_pdf_info");
	     	
	     	return $attachment_info;
	}

	//gets images associated with a video
	function get_renderered_video_info($media_attachment_id, $type = UPLOAD_TYPE_THUMBNAIL_SMALL) {
	    	$this->track_method_entry("MediaAttachmentDB","get_renderered_video_info", "media_attachment_id", $media_attachment_id);
	    	$media_attachment_id = (int) $media_attachment_id;
	    	$type = (int) $type;
        	$attachment_info="";
        
	        //first get_parent if this isnt the parent attachment
//              I fixed a typo here but wasn't really sure what this does so commented out for now --mark
//	        $parent_info=$this->get_parent_attachment_info($media_attachment_id);
//	        if (is_array($parent_info) && sizeof($parent_info)>0){
//		        if ($parent_info["media_attachment_id"]!=$media_attachment_id)
//		        	$media_attachment_id=$parent_info["media_attachment_id"];
//	        }
	        
	        $sql="select media_attachment_id from media_attachment
	         where parent_attachment_id=".$media_attachment_id." and upload_type_id=". $type;
	   
	   		$id=0;
	     	$result=$this->single_column_query($sql);
	     	if (is_array($result) && sizeof($result)>0){
	     		$id=array_pop($result);	
	     		$attachment_info=$this->get_media_attachment_info($id);
	     	}
	     	$this->track_method_exit("MediaAttachmentDB","get_renderered_video_info");
	     	
	     	return $attachment_info;
	}

	//gets images associated with a flv
	function get_renderered_flv($media_attachment_id){
	    	$this->track_method_entry("MediaAttachmentDB","get_renderered_flv", "media_attachment_id", $media_attachment_id);
		$media_attachment_id = (int) $media_attachment_id;
        	$attachment_info="";
        
	        //first get_parent if this isnt the parent attachment
//              I fixed a typo here but wasn't really sure what this does so commented out for now --mark
//	        $parent_info=$this->get_parent_attachment_info($media_attachment_id);
//	        if (is_array($parent_info) && sizeof($parent_info)>0){
//		        if ($parent_info["media_attachment_id"]!=$media_attachment_id)
//		        	$media_attachment_id=$parent_info["media_attachment_id"];
//	        }
	        
	        $sql="select media_attachment_id from media_attachment
	         where parent_attachment_id=".$media_attachment_id." and upload_type_id=".UPLOAD_TYPE_FLV;
	   
	   		$id=0;
	     	$result=$this->single_column_query($sql);
	     	if (is_array($result) && sizeof($result)>0){
	     		$id=array_pop($result);	
	     		$attachment_info=$this->get_media_attachment_info($id);
	     	}
	     	$this->track_method_exit("MediaAttachmentDB","get_renderered_flv");
	     	
	     	return $attachment_info;
	}

} //end class
