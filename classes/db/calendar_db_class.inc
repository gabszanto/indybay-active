<?php
//--------------------------------------------
//Indybay CalendarDB Class
//Written December 2005 - January 2006
//somewhat extended from something in sf-active
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"CalendarDB");

require_once (CLASS_PATH."/db/article_db_class.inc");



//The event DB class is responsible for all Calendar related DB access
//for individual events most of the sql is inherited from ArticleDB
//the main code in this class relates to finding events between dates and
//saving and loading event type associations
class CalendarDB extends ArticleDB{

	function get_events_between_dates($date1, $date2){
		$news_item_db_class= new NewsItemDB;
		$this->track_method_entry("CalendarDB","get_events_between_dates");
		$sql="select ni.news_item_id, niv.title1, news_item_status_id, 
			 unix_timestamp(creation_date) as creation_timestamp, event_duration,
	        	 	date_format(creation_date,'%W %b %D %l:%i %p') as created,
	        	 	unix_timestamp(displayed_date) as event_date_timestamp
	        	 from news_item ni, news_item_version niv 
	        	 where ni.current_version_id=niv.news_item_version_id
	        	 and displayed_date >= '" . $this->prepare_string($date1) . "'
	        	 and displayed_date <= '" . $this->prepare_string($date2) . "'
	        	 and news_item_type_id=".NEWS_ITEM_TYPE_ID_EVENT."
	        	 and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_HIDDEN."
	        	 and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN."
	        	 and ni.news_item_status_id!=".NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN."
	        	 order by displayed_date";
       	$result=$this->query($sql);
       	
       	$this->track_method_exit("CalendarDB","get_events_between_dates");
       	
       	return $result;
	}
	
	
	//gets the event type for an event
	function get_event_type_info_for_newsitem($news_item_id){
	
		$event_type_info="";
		
		$this->track_method_entry("CalendarDB","get_event_type");
		
		$news_item_id = (int) $news_item_id;
		$sql="select k.keyword_id, k.keyword from keyword k, news_item_keyword nik
		where nik.keyword_id=k.keyword_id and nik.news_item_id=".$news_item_id.
		" and k.keyword_type_id=1";
       	$result=$this->query($sql);
       	if (is_array($result) &&sizeof($result)>0){
       		$event_type_info=array_pop($result);
       	}
       	
       	$this->track_method_exit("CalendarDB","get_event_type");
		
		return $event_type_info;
	}
	
	
	
	//gets list of event types for a dropdown list
	function get_event_type_for_select_list(){
	
		$event_type_select_list=array();
		
		$this->track_method_entry("CalendarDB","get_event_type_for_select_list");
		
		$sql="select k.keyword, k.keyword_id from keyword k where k.keyword_type_id=1";
       	$result=$this->query($sql);

       	if (is_array($result) &&sizeof($result)>0){
       		foreach($result as $row){
       			$event_type_select_list[$row["keyword_id"]]=$row["keyword"];
       		}
       	}
       	
       	$this->track_method_exit("CalendarDB","get_event_type_for_select_list");
		
		return $event_type_select_list;
	}
	
	
	
	//adds or changes the event type of an event
	function set_or_change_event_type($news_item_id, $keyword_id){
		
		$this->track_method_entry("CalendarDB","set_or_change_event_type");
		
		$news_item_id = (int) $news_item_id;
		$keyword_id = (int) $keyword_id;
		$sql="delete from news_item_keyword where news_item_id=".$news_item_id."
		and keyword_id in (select keyword_id from keyword where keyword_type_id=1)";
		
		$this->execute_statement($sql);
		$sql="insert into news_item_keyword (news_item_id, keyword_id) values (";
		$sql.=$news_item_id.", ".$keyword_id." )";
		
		$this->execute_statement($sql);
		
		$this->track_method_exit("CalendarDB","set_or_change_event_type");
	}
	
}
