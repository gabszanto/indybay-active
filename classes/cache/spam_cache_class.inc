<?php
//--------------------------------------------
//Indybay SpamCache Class
//Written June 2007
//
//Modification Log:
//5-6/2007  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"SpamCache");

require_once(CLASS_PATH."/cache/cache_class.inc");
require_once(CLASS_PATH."/cache/legacy_spam_cache_class.inc");

//New Blocking System (as opposed to legacy one from a year earlier)
//----
//Consists of lists of ips that will get redirected to a spam page for all but the front page, lists
//of keywords, title words and ips that will result in posting as hidden and finally a list of keywords
//that will cause validation failure on post with messages telling those
//posting not to use the keywords (examples being things like "[url=" that seem to only be in spam as
//well as phrases like "download free porn" etc...)
class SpamCache extends Cache
{
 
 
   // looks for specific redirection urls based off HTTP params
   //the last part of this method is the main hook-in to the 
   //continuing functionality from the legacy spam system
   function get_alternate_spam_block_page(){
   	
		$redirect="";
	
		//set local vars from http headers
		
		$current_url = $_SERVER['SCRIPT_NAME']; 
		if ($current_url=="/index.php" || strpos($current_url, "admin")==1)
			return "";
		
		if(getenv("REMOTE_ADDR")) $current_ip = getenv("REMOTE_ADDR");
    		else if (getenv("HTTP_CLIENT_IP")) $current_ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $current_ip = getenv("HTTP_X_FORWARDED_FOR");
			else $current_ip = "UNKNOWN"; 
		
		$ip_pieces = explode(".", $current_ip);
		$current_ip1=trim($ip_pieces[0]);
		$current_ip2=trim($ip_pieces[1]);
		$current_ip3=trim($ip_pieces[2]);
		$threepartip=$current_ip1.".".$current_ip2.".".$current_ip1.".*";
		$twopartip=$current_ip1.".".$current_ip2.".*";
		$onepartip=$current_ip1.".*";
		
		$iplist=$this->load_ip_block_file();
		
		$test=getenv("HTTP_CLIENT_IP");
		if (strlen($test)>5 && strpos(' '.$iplist,$test)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		$test=getenv("HTTP_X_FORWARDED_FOR");
		if (strlen($test)>5 && strpos(' '.$iplist,$test)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		$test=getenv("REMOTE_ADDR");
		if (strlen($test)>5 && strpos(' '.$iplist,$test)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		
		if (strpos(' '.$iplist,$threepartip)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		if (strpos(' '.$iplist,$twopartip)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		if (strpos(' '.$iplist,$onepartip)>0){
			return FULL_ROOT_URL."spam2.php";
		}
		
		//now check in older system
		$legacy_spam_class= new LegacySpamCache();
		return $legacy_spam_class->get_alternate_spam_block_page();
   }
 
   //this code is run after a post is added to the DB
   //this is part of the newer spam blocking system that
   //consists of lists of ips, keywords etc.. that should result in items getting
   //posted as hidden
   function should_block_db_add(){
   
		if(getenv("REMOTE_ADDR")) $current_ip = getenv("REMOTE_ADDR");
    		else if (getenv("HTTP_CLIENT_IP")) $current_ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $current_ip = getenv("HTTP_X_FORWARDED_FOR");
			else $current_ip = "UNKNOWN"; 
			
		$ip_pieces = explode(".", $current_ip);
		$current_ip1=trim($ip_pieces[0]);
		$current_ip2=trim($ip_pieces[1]);
		$current_ip3=trim($ip_pieces[2]);
		$threepartip=$current_ip1.".".$current_ip2.".".$current_ip1.".*";
		$twopartip=$current_ip1.".".$current_ip2.".*";
		$onepartip=$current_ip1.".*";
		
		$iplist=$this->load_ip_spam_file();
		if (strpos(' '.$iplist,$current_ip)>0){
			return true;	
		}
		if (strpos(' '.$iplist,$threepartip)>0){
			return true;	
		}
		if (strpos(' '.$iplist,$twopartip)>0){
			return true;	
		}
		if (strpos(' '.$iplist,$onepartip)>0){
			return true;	
		}
		
		$titlelist=$this->load_title_spam_file();
		$titlelist=str_replace("\r","",$titlelist);
		if (strpos(" \n".$titlelist."\n","\n".trim($_POST['title1'])."\n")>0){
			return true;	
		}
		
		$textlist=$this->load_text_spam_file();
		$textlist=str_replace("\r","",$textlist);
		$text_array=explode("\n", $textlist);
		foreach($text_array as $spamstr){
			$spamstr=trim($spamstr);
			if ($spamstr=="")
				continue;
			if (isset($_POST['summary']) && strpos(' '. $_POST['summary'], $spamstr) > 0) {
				return true;	
			}
			if (isset($_POST['text']) && strpos(' '. $_POST['text'], $spamstr) > 0) {
				return true;	
			}
		}
		
   }

   function load_ip_block_file(){
      	$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/ip_block_list.txt");
   }
   
      		
  
   function save_ip_block_file($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/ip_block_list.txt", $str);
   }
   
   function load_ip_spam_file(){
      	$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/ip_spam_list.txt");
   }
   
      		
   function save_ip_spam_file($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/ip_spam_list.txt", $str);
   }
   
   function load_title_spam_file(){
      	$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/title_spam_list.txt");
   }
   
   function save_title_spam_file($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/title_spam_list.txt", $str);
   }
   
   function load_text_spam_file(){
      	$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/text_spam_list.txt");
   }
   

   function save_text_spam_file($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/text_spam_list.txt", $str);
   }
   
   function load_validation_string_file(){
      	$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/validation_string_list.txt");
   }
   

   function save_validation_string_file($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/validation_string_list.txt", $str);
   }
   

} //end class

class Captcha {

  function make_form() {
    if ($this->validate_captcha()) {
      return 'You appear to be human. <input name="captcha_verbal" type="hidden" value="'
             . htmlspecialchars($this->captchas['verbal']['answer'])
             . '" /><input name="captcha_math" type="hidden" value="'
             . htmlspecialchars($this->captchas['math']['answer'])
             . '" /><input type="hidden" name="token" value="'
             . htmlspecialchars($this->token) . '" />';
    }
    else {
      $this->verbal_captcha();
      $this->math_captcha();
      $this->make_token();
      return $this->captchas['verbal']['form']
             . ' <input name="captcha_verbal" type="text" size="6" maxlength="1" /><br /><br />'
             . $this->captchas['math']['form']
             . ' <input name="captcha_math" type="text" size="6" maxlength="3" /><input type="hidden" name="token" value="'
             . htmlspecialchars($this->token) . '" />';
    }
  }

  function verbal_captcha() {
    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $missing = mt_rand(3, 22);
    $captcha['answer'] = substr($alphabet, $missing, 1);
    $prefix = substr($alphabet, $missing - 3, 3);
    $suffix = substr($alphabet, $missing + 1, 3);
    $captcha['form'] = "What letter is missing from the alphabet: {$prefix}_$suffix?";
    $this->captchas['verbal'] = $captcha;
  }

  function math_captcha() {
    $high = mt_rand(6, 10);
    $low = mt_rand(0, 5);
    $operator = mt_rand(0, 1);
    $operators = array('plus', 'minus');
    $captcha['answer'] = $operator ? $high - $low : $high + $low;
    $captcha['form'] = "What is $high {$operators[$operator]} $low?";
    $this->captchas['math'] = $captcha;
  }
  
  function make_token() {
    $this->token = hash_hmac('sha256', $this->captchas['verbal']['answer'] . mktime(date('H'), 0, 0) . $this->captchas['math']['answer'], INDYBAY_PRIVATE_KEY);
  }

  function validate_captcha() {
    $verbal = isset($_POST['captcha_verbal']) ? strtoupper($_POST['captcha_verbal']) : '';
    $math = isset($_POST['captcha_math']) ? $_POST['captcha_math'] : '';
    $token = isset($_POST['token']) ? $_POST['token'] : '';
    $time = mktime(date('H'), 0, 0);
    $tokens = array_fill(0, 5, '');
    foreach ($tokens as $key => $value) {
      if ($token === hash_hmac('sha256', $verbal . ($time - (3600 * $key)) . $math, INDYBAY_PRIVATE_KEY)) {
        $this->token = $token;
        $this->captchas = array(
          'verbal' => array('answer' => $verbal),
          'math' => array('answer' => $math),
        );
        return $this->valid = TRUE;
      }
    }
    return $this->valid = FALSE;
  }
}
