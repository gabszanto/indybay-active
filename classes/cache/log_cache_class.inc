<?php
//--------------------------------------------
//Indybay LogCache Class
//Written May 2006
//
//Modification Log:
//5/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"LogCache");

require_once(CLASS_PATH."/cache/cache_class.inc");

//Caches and loads lists rules about what to log to the DB
class LogCache extends Cache
{

	//should logging record anything aside from DB adds
	function log_any_non_db(){
		$log_setting_array=$this->load_cached_logs_into_array();
		if ($log_setting_array[0]=="1" || $log_setting_array[2]==1)
			return true;
		else
			return false;
	}

	//determines if logging should take place for the given http method (or search since that overrides GET)
	function should_record($http_method){
		$log_setting_array=$this->load_cached_logs_into_array();
		if ($http_method=="GET"){
			if ($log_setting_array[0]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="SEARCH"){
			if ($log_setting_array[6]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="POST"){
			if ($log_setting_array[2]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="DB"){
			if ($log_setting_array[4]==1)
				return true;
			else
				return false;
		}
   }
 
  //determines if ip logging is on for the given HTTP method (or search since that overrides GET)
   function should_record_ips($http_method){
   		$log_setting_array=$this->load_cached_logs_into_array();
		if ($http_method=="GET"){
			if ($log_setting_array[1]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="SEARCH"){
			if ($log_setting_array[7]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="POST"){
			if ($log_setting_array[3]==1)
				return true;
			else
				return false;
		}
		if ($http_method=="DB"){
			if ($log_setting_array[5]==1)
				return true;
			else
				return false;
		}
   }
 
   //changes log settings and then calls save
   function change_log_settings(
        		$log_get, $log_ip_get,$log_search, $log_ip_search, $log_post, $log_ip_post,
        		$log_db, $log_ip_db
        		){
        		$log_seting_array = array();
        		if ($log_get!="")
        			$log_seting_array[0]=1;
       			else
       				$log_seting_array[0]=0;
        		if ($log_ip_get!="")
        			$log_seting_array[1]=1;
        		else
        			$log_seting_array[1]=0;
        		if ($log_post!="")
        			$log_seting_array[2]=1;
        		else
 		       		$log_seting_array[2]=0;
        		if ($log_ip_post!="")
        			$log_seting_array[3]=1;
        		else
        			$log_seting_array[3]=0;
        		if ($log_db!="")
        			$log_seting_array[4]=1;
        		else
        			$log_seting_array[4]=0;
        		if ($log_ip_db!="")
        			$log_seting_array[5]=1;
        		else
        			$log_seting_array[5]=0;
        		if ($log_search!="")
        			$log_seting_array[6]=1;
        		else
        			$log_seting_array[6]=0;
        		if ($log_ip_search!="")
        			$log_seting_array[7]=1;
        		else
        			$log_seting_array[7]=0;
        		$str=implode("|", $log_seting_array);
        		$this->save_log_settings_from_string($str);
   }
   
   //"private" functions for use in this class
   function load_cached_logs_into_array(){
   		if (!file_exists(CACHE_PATH."/log_settings.txt"))
   			return array(0,0,0,0,0,0,0);
   		$file_util= new FileUtil();
   		$str=$file_util->load_file_as_string(CACHE_PATH."/log_settings.txt");
   		$ret_array=explode("|", $str);
   		if (!is_array($ret_array) || sizeof($ret_array)<7){
   			$ret_array=  array(0,0,0,0,0,0,0,0);
		}
   		return $ret_array;
   }
   
   //saves log settings
    function save_log_settings_from_string($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/log_settings.txt", $str);
   }
   
   //should just append to file
   function append_spam_block_row($row_str){
   		$str=$this->load_cached_spam_blocks_into_string();
   		$rows=explode("\r\n",$str);
   		
   		array_push($rows, $row_str);
   		$str2=implode("\r\n",$rows);
   		$this->save_spam_blocks_from_string($str2);
   }
   
   

} //end class
?>