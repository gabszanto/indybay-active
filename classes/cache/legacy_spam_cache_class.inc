<?php
//--------------------------------------------
//Indybay SpamCache Class
//Written May 2006
//
//Modification Log:
//5/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"LegacySpamCache");

require_once(CLASS_PATH."/cache/cache_class.inc");

//Legacy blocking system (largely replaces a year after it was written but
//still used for some rare types of blocks not supported by newer code)
//This system consists of lists of rules that get cached to a file that define
//what should get blocked and what the result of blocking should be
//the system was largely replaces since it was hard to use and wasnt easy to
//integrate with external lists of spam ips
class LegacySpamCache extends Cache
{
 
   //adds an entry from the older version of the spam blocking system
   //this code should eventually ger moved to a redirection section of the code
   //since it remains useful for redirecting based off url, browser type or referring url
   function add_entry_to_spam_cache($ip, $ip_part1, $ip_part2, $ip_part3,
   			$url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note){
   		$str_row=$this->create_savable_string_row($ip, $ip_part1, $ip_part2, $ip_part3,
   			$url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note);
   		$this->append_spam_block_row($str_row);
   }
 
   //removes an entry from the older version of the spam blocking system
   //this code should eventually ger moved to a redirection section of the code
   //since it remains useful for redirecting based off url, browser type or referring url
   function remove_entry_from_spam_cache($id){
   		$str=$this->load_cached_spam_blocks_into_string();
   		$rows=$string_array=explode("\r\n",$str);
   		$array2=array();
   		foreach($rows as $next_row){
   			$parsed_row=$this->parse_cache_block_row($next_row);
   			if ($parsed_row["id"]!=$id){
   				array_push($array2, $next_row);
   			}
   		}
   		$str2=implode("\r\n",$array2);
   		$this->save_spam_blocks_from_string($str2);
   }
 
   //removes all entries from the older version of the spam blocking system
   //this code should eventually get moved to a redirection section of the code
   //since it remains useful for redirecting based off url, browser type or referring url
   function clear_spam_cache(){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/spam_block.txt", "");
   }
 
   //this is the core of the older spam blockng system
   //that looks for specific redirection urls based off HTTP params
   function get_alternate_spam_block_page(){
   	
		$redirect="";
		
		$block_conditions=$this->list_blocks();
		foreach ($block_conditions as $block_condition){
			if (!$this->compare_current_request_to_block_helper($block_condition, false)){
				$redirect=$block_condition['destination'];
				break;
			}
		}
		return $redirect;	
   }
 
   //compares a block conditions against the current state of an HTTP request
   //returns true if request should be blocked (and redirected) and false if it shouldnt
   //the calling method looks for the redirect url in the block conditions
   function compare_current_request_to_block_helper($block_condition, $isdbadd){
 
		if(getenv("REMOTE_ADDR")) $current_ip = getenv("REMOTE_ADDR");
    		else if (getenv("HTTP_CLIENT_IP")) $current_ip = getenv("HTTP_CLIENT_IP");
			else if(getenv("HTTP_X_FORWARDED_FOR")) $current_ip = getenv("HTTP_X_FORWARDED_FOR");
			else $current_ip = "UNKNOWN"; 
		
		///legacy code to block
		
		//set local vars
		//get vars from block_condition_dict
		
		$block_url=$block_condition['url'];
		$current_url = $_SERVER['SCRIPT_NAME'] . (isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : ''); 
		
		//so you cant block yourself from getting rid of a block
		if (strpos(" ".$current_url, "/admin/")>0)
			return true;
		
		if (!$isdbadd && $block_condition['http_method']!="" && $block_condition['http_method']!="0"){
			$request_method=$_SERVER['REQUEST_METHOD'];
			if ($block_condition['http_method']!=$request_method){
				return true;
			}
		}	
		
		$block_parent_id=$block_condition["parent_news_item_id"];
		if (isset($url) && strrpos($url, "newsitems")>0){
		   	$i=strrpos($current_url,"/");
			$j=strrpos($current_url,".");
			$current_parent_id=substr($current_url, $i+1,$j-$i-1)+0;
		}else if (!empty($_GET['top_id'])){
			$current_parent_id=$_GET['top_id']+0; 
		}else if (!empty($_POST['parent_item_id'])){
			$current_parent_id=$_POST['parent_item_id']+0; 
		}else{
			$current_parent_id = 0;
		}
		    	
		$block_ip=$block_condition['ip'];
		
		if ($current_ip != 'UNKNOWN') {
                  $ip_pieces = explode('.', $current_ip);
                  $block_ip1 = trim($block_condition['ip_part1']);
                  $current_ip1 = trim($ip_pieces[0]);
                  $block_ip2 = trim($block_condition['ip_part2']);
                  $current_ip2 = isset($ip_pieces[1]) ? trim($ip_pieces[1]) : '';
                  $block_ip3 = trim($block_condition['ip_part3']);
                  $current_ip3 = isset($ip_pieces[2]) ? trim($ip_pieces[2]) : '';
		}
		
		$block_ref_url=$block_condition['referring_url'];
		$current_ref_url = getenv('HTTP_REFERER') . (isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '');

		$block_keyword=$block_condition['keyword'];
		$current_keywords = (isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '') .' '. $_SERVER['QUERY_STRING'] .' '. (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '');
                $current_keywords .= 'POST_TITLE1: '. (isset($_POST['title1']) ? $_POST['title1'] : '');
    	        $current_keywords .= 'POST_AUTHOR: '. (isset($_POST['displayed_author_name']) ? $_POST['displayed_author_name'] : '');
    	        $current_keywords .= 'POST_EMAIL: '. (isset($_POST['email']) ? $_POST['email'] : '');
    	        $current_keywords .= 'POST_SUMMARY: '. (isset($_POST['summary']) ? $_POST['summary'] : '');
    	
		$ret=false;
		$at_least_one_set=false;
	
		if (!empty($block_method)){
			$at_least_one_set=true;
		}
		
		$block_ip = isset($block_condition['ip']) ? $block_condition['ip'] : '';
		if ($ret==false && $block_ip!=""){
			if ($current_ip==$block_ip){
				$at_least_one_set=true;
			}else{
				$ret=true;
			}
		}	
	
		if ($ret==false && $block_ip1!=""){
			if ($current_ip1==$block_ip1){
				$at_least_one_set=true;
				if ($block_ip2!="" && $block_ip2!=$current_ip2){
					$ret=true;
				}else if ($block_ip3!="" && $block_ip3!=$current_ip3){
					$ret=true;
				}
			}else{
				$ret=true;
			}
		}
	

		if ($ret==false && $block_url!=""){
			$at_least_one_set=true;
			if (!strpos(" ".$current_url, $block_url)>0){
				$ret=true;
			}
		}
		if ($ret==false && $block_ref_url!=""){
			$at_least_one_set=true;
			if (!strpos(" ".$current_ref_url, $block_ref_url)>0){
				$ret=true;
			}	
		}
	
		if ($ret==false && $block_parent_id!="" && $block_parent_id!=0){
			$at_least_one_set=true;
			if ($block_parent_id!=$current_parent_id){
				$ret=true;
			}
		}

		if ($ret==false && $block_keyword!=""){
			$at_least_one_set=true;
			if (!strpos(" ".$current_keywords, $block_keyword)>0){
				$ret=true;
			}
		}
	
		if ($at_least_one_set){
			return $ret;
		}else{
			return true;
		}

	
   }
 
   //lists all blocks stored in the cache file for blocking
   function list_blocks(){
   
   		$this->track_method_entry("SpamCache","list_blocks");
   		$cache_file_str=$this->load_cached_spam_blocks_into_string();
   		$string_array=explode("\r\n",$cache_file_str);
   		$return_array= array();
   		foreach ($string_array as $row){
   		
   			$parsed_row=$this->parse_cache_block_row($row);
   			
   			if (sizeof($parsed_row)==13){
   				array_push($return_array,$parsed_row);
   			}
   		}
   		$return_array=array_reverse($return_array);
   		$this->track_method_entry("SpamCache","list_blocks");
   		
   		return $return_array;
   }
   
   
   
   //loads cached spam blocks into a string for use by other methods
   function load_cached_spam_blocks_into_string(){
   		$file_util= new FileUtil();
   		return $file_util->load_file_as_string(CACHE_PATH."/spam/spam_block.txt");
   }
   
   //takes a row from the cached spam block file and parses it into its various components
   function parse_cache_block_row($row_text){
   		$row_dict=array();
   		$row_param_array=explode("|\t|",$row_text);
   		if (sizeof($row_param_array)==13){
   			$row_dict["id"]=$row_param_array[0];
   			$row_dict["ip"]=$row_param_array[1];
   			$row_dict["ip_part1"]=$row_param_array[2];
   			$row_dict["ip_part2"]=$row_param_array[3];
   			$row_dict["ip_part3"]=$row_param_array[4];
   			$row_dict["url"]=$row_param_array[5];
   			$row_dict["referring_url"]=$row_param_array[6];
   			$row_dict["http_method"]=$row_param_array[7];
   			$row_dict["parent_news_item_id"]=$row_param_array[8];
   			$row_dict["keyword"]=$row_param_array[9];
   			$row_dict["destination"]=$row_param_array[10];
   			$row_dict["added_info"]=$row_param_array[11];
   			$row_dict["note"]=$row_param_array[12];
   		}
   		return $row_dict;
   }
   
   
   //takes block conditions and returns a string that can be saved to cache file for blocking
   //requests that match the given condition
   function create_savable_string_row($ip, $ip_part1, $ip_part2, $ip_part3,
   			$url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note){
   		
   		$user_info_str="Added By ".$_SESSION['session_username'];
   		$user_info_str.= " on ".date(DATE_RFC822);
   		
   		$id=$_SESSION['session_user_id']."_".time();
   		$row_param_array=array();
   		$row_param_array[0]=$id;
   		$row_param_array[1]=$ip;
   		$row_param_array[2]=$ip_part1;
   		$row_param_array[3]=$ip_part2;
   		$row_param_array[4]=$ip_part3;
   		$row_param_array[5]=$url;
   		$row_param_array[6]=$referring_url;
   		$row_param_array[7]=$http_method;
   		$row_param_array[8]=$parent_news_item_id;
   		$row_param_array[9]=$keyword;
   		$row_param_array[10]=$destination;
   		$row_param_array[11]=$user_info_str;
   		$row_param_array[12]=$note;
   		$str_row=implode ("|\t|",$row_param_array);
   		return $str_row;
   }
   
   //saves the full cache blocing file
   function save_spam_blocks_from_string($str){
   		$file_util= new FileUtil();
   		$file_util->save_string_as_file(CACHE_PATH."/spam/spam_block.txt", $str);
   }
   
   
   //should just append to file
   function append_spam_block_row($row_str){
   		$str=$this->load_cached_spam_blocks_into_string();
   		$rows=explode("\r\n",$str);
   		
   		array_push($rows, $row_str);
   		$str2=implode("\r\n",$rows);
   		$this->save_spam_blocks_from_string($str2);
   }

} //end class
