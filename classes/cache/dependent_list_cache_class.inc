<?php
//--------------------------------------------
//Indybay DependentListCache Class
//Written January 2006
//
//Modification Log:
//1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"DependentListCache");

require_once(CLASS_PATH."/cache/cache_class.inc");
require_once(CLASS_PATH."/cache/newswire_cache_class.inc");
require_once(CLASS_PATH."/cache/event_list_cache_class.inc");
require_once(CLASS_PATH."/db/news_item_version_db_class.inc");
require_once(CLASS_PATH."/db/feature_page_db_class.inc");

//this class is responsible for generating the cache files that may need to 
//be updated if a news_item is changed or added
class DependentListCache extends Cache
{

	//method to upate all lists on the site when an article (event or post) is added
	//for posts this means newswires and for events it could mean redlinks and calendar week caches
	function update_all_dependencies_on_add($article_info){
		$newswire_cache_class= new NewswireCache;
       	$newswire_cache_class->regenerate_newswires_for_new_newsitem($article_info["news_item_id"]);
       	if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
	       	$event_cache_class= new EventCache();
			$year=$article_info["displayed_date_year"];
			$month=$article_info["displayed_date_month"];
			$day=$article_info["displayed_date_day"];
			$date= new Date();
			$date->set_time(0,0,$day,$month,$year);
			$event_cache_class->recache_week_given_date($date);	
		}
	}
	
	
	//method to upate all lists on the site when an article (event or post) is updated
	//for posts this means newswires and for events it could mean redlinks & calendar week caches
	function update_all_dependencies_on_update($old_article_info,$new_article_info, $old_cat_list){
		   						
		
		$news_item_id=$new_article_info["news_item_id"];
		$old_news_item_version_id=$old_article_info["news_item_version_id"];
		$new_news_item_version_id=$new_article_info["news_item_version_id"];
		$old_news_item_type_id=$old_article_info["news_item_type_id"];
		$new_news_item_type_id=$new_article_info["news_item_type_id"];
		
		//update newswires if needed
		if ($new_news_item_type_id==NEWS_ITEM_TYPE_ID_EVENT ||$old_news_item_type_id==NEWS_ITEM_TYPE_ID_EVENT
		|| $new_news_item_type_id==NEWS_ITEM_TYPE_ID_POST || $old_news_item_type_id==NEWS_ITEM_TYPE_ID_POST){
			$newswire_cache_class= new NewswireCache();
			$newswire_cache_class->regenerate_newswires_for_newsitem($new_article_info["news_item_id"],$old_cat_list, 
			$old_article_info["news_item_status_id"],$new_article_info["news_item_status_id"]);
		}
		
		//update calendar weeks, highlighted lists and upcomming events
		if ($new_news_item_type_id==NEWS_ITEM_TYPE_ID_EVENT ||
			$old_news_item_type_id==NEWS_ITEM_TYPE_ID_EVENT){

			$event_list_cache_class= new EventListCache();
			if ($old_news_item_type_id!=NEWS_ITEM_TYPE_ID_EVENT){
				// @fixme: Commenting out because this method does not exist!
				// $event_list_cache_class->update_caches_on_add($new_article_info);
			}else{
				$event_list_cache_class->update_caches_on_change($old_article_info,$new_article_info );
			}
			
		}
		
	}
}
