<?php
//--------------------------------------------
//Indybay EventListCache Class
//Written January 2006
//
//Modification Log:
//1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"EventListCache");

require_once(CLASS_PATH."/cache/dependent_list_cache_class.inc");
require_once(CLASS_PATH."/cache/event_cache_class.inc");
require_once(CLASS_PATH."/renderer/event_list_renderer_class.inc");
require_once(CLASS_PATH."/db/event_list_db_class.inc");

//this class is responsible for generating the redlinks and upcomming events lists included on the 
//feature pages. 
class EventListCache extends DependentListCache
{

	//code to figure out what to update cache-wise when an event is updated
	function update_caches_on_change($old_article_info, $new_article_info){

		$this->track_method_entry("EventListCache","update_caches_on_change");

		$old_timestamp=$old_article_info["displayed_timestamp"];
		$new_timestamp=$new_article_info["displayed_timestamp"];
		$old_date= new Date();
		$new_date= new Date();
		$old_date->set_time_from_unix_time($old_timestamp);
		$new_date->set_time_from_unix_time($new_timestamp);

		$old_date->find_start_of_week();
		$new_date->find_start_of_week();

		$event_cache_class= new EventCache();
		$event_cache_class->recache_week_given_date($old_date);
		if ($old_date->get_formatted_date()!=
			$new_date->get_formatted_date()){
			$event_cache_class->recache_week_given_date($new_date);	
		}

		$feature_page_db_class= new FeaturePageDB();
		$page_list=$feature_page_db_class->get_associated_pages_info($old_article_info["news_item_id"]);
		foreach ($page_list as $page_info){
			$this->cache_highlighted_events_cache_for_page($page_info["page_id"]);
			$this->cache_events_for_page($page_info["page_id"]);
		}

		$this->cache_highlighted_events_cache_for_page(FRONT_PAGE_CATEGORY_ID);
		
		$this->track_method_exit("EventListCache","update_caches_on_change");
		
	}
	
	//code to load highlighted events (redlinks) for a given page
	function load_cached_highlighted_events_cache_for_page($page_id){
	
		$this->track_method_entry("EventListCache","load_cached_highlighted_events_cache_for_page");
		
		$file_path=CACHE_PATH."/calendar/highlighted_cache/highlighted_list_for_page_".$page_id;
		
		$ret=$this->cache_or_call_based_off_age(new EventListRenderer, "render_highlighted_event_list",$page_id,$file_path, 600);
				
		$this->track_method_exit("EventListCache","load_cached_highlighted_events_cache_for_page");
		
		return $ret;
	
	}

	//code to recache highlighted events (redlinks) for a given page
	function cache_highlighted_events_cache_for_page($page_id){
	
		$this->track_method_entry("EventListCache","load_cached_highlighted_events_cache_for_page");
		
		$file_path=CACHE_PATH."/calendar/highlighted_cache/highlighted_list_for_page_".$page_id;
		
		$this->cache_or_call_based_off_age(new EventListRenderer, "render_highlighted_event_list",$page_id,$file_path, 0);
				
		$this->track_method_exit("EventListCache","load_cached_highlighted_events_cache_for_page");
	
	}
	
	//code to load full events for a given page (this is the list at the bottom of the feature page)
	function load_cached_events_for_page($page_id){
	
		$this->track_method_entry("EventListCache","load_cached_events_for_page");
		
		$file_path=CACHE_PATH."/calendar/upcoming_cache/event_list_cache_for_page_".$page_id;
		$ret=$this->cache_or_call_based_off_age(new EventListRenderer, "render_event_list", $page_id, $file_path, 600);
		
		$this->track_method_exit("EventListCache","load_cached_events_for_page");
		
		return $ret;
	}
	
	//code to recache full events list for a given page (this is the list at the bottom of the feature page)
	function cache_events_for_page($page_id){
		$this->track_method_entry("EventListCache","cache_events_for_page");
		
		$file_path=CACHE_PATH."/calendar/upcoming_cache/event_list_cache_for_page_".$page_id;
		$this->cache_or_call_based_off_age(new EventListRenderer, "render_event_list", $page_id, $file_path, 0);
		
		$this->track_method_exit("EventListCache","cache_events_for_page");
	}

	
} //end class
?>