<?php
//--------------------------------------------
//Indybay Cache Class
//Written through refactoring of an sfactive class (with little original code left)
//December 2005
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"Cache");

require_once(CLASS_PATH."/common_class.inc");
require_once(CLASS_PATH."/media_and_file_util/file_util_class.inc");
require_once(CLASS_PATH."/renderer/renderer_class.inc");


//main cache class
//this file is uses for common caching functionality
//this mainly includes getting directory names and making
//sure safe_includes dont allow malicious content

class Cache extends Common
{

	//this just calls the FileUtils save for now but it could also
	//do some security checks since only certain folders can be used for caching
    function cache_file($file_path, $contents)
    {
    	$this->track_method_entry("Cache","cache_file", "file_name", $file_path);

		$file_util_class= new FileUtil();
		$success=$file_util_class->save_string_as_file($file_path, $contents);

		$this->track_method_exit("Cache","cache_file");
		
		return $success;
    }
    
    

	//returns a date portion of a directory structure as used when saving
	//any cache file
	function get_rel_dir_from_date($date){
    
    	$this->track_method_entry("Cache","get_rel_dir_from_date", "date", $date);
	
		$year=date("Y", $date);
		$month=date("m", $date);
		$day=date("d", $date);
		$date_partial_path=$year."/".$month."/".$day."/";
	
		$this->track_method_exit("Cache","get_rel_dir_from_date", "date", $date);
	
		return $date_partial_path;
	}


	//created a relative directory structure if it doesnt yet exist
	//and returns the same result as get_rel_dir_from_date
	function create_date_based_relative_directory($partial_path, $date){
	
		$this->track_method_entry("Cache","create_date_based_relative_directory", "partial_path", $partial_path, "date", $date);
		
		$rel_dir=$this->get_rel_dir_from_date($date);
		$year=date("Y", $date);
		$month=date("m", $date);
		$day=date("d", $date);
		$date_partial_path=$year."/";
		$mkdir_return_code=1;
		if (!file_exists($partial_path.$date_partial_path)){
			$mkdir_return_code=mkdir($partial_path.$date_partial_path, 0777);
		}
		$date_partial_path.=$month."/";
		if ($mkdir_return_code==1 &&  !file_exists($partial_path.$date_partial_path))
			$mkdir_return_code=mkdir($partial_path.$date_partial_path, 0777);
		$date_partial_path.=$day."/";
		if ($mkdir_return_code==1 && !file_exists($partial_path.$date_partial_path))
			$mkdir_return_code=mkdir($partial_path.$date_partial_path, 0777);
		if ($mkdir_return_code==1)
			$ret=$date_partial_path;
		else
			$ret=0;
	
		$this->track_method_exit("Cache","create_date_based_relative_directory");
		
		return $ret;

	}



	//this is the include that shuld be around any user content that is cached
	function safe_include($file_path) {
		$this->track_method_entry("Cache","safe_include", "file_path", $file_path);
		readfile($file_path);
		$this->track_method_exit("Cache", "safe_include");
	}
	
	
	
	//this is the include that should be around any admin user content that is cached
	function safe_admin_include($file_path) {
		$this->track_method_entry("Cache","safe_admin_include", "file_path", $file_path);
		readfile($file_path);
		$this->track_method_exit("Cache", "safe_admin_include");
	}
	
	//the function allows you to have a method only called if the cached file for the results is too old
	//if the cached file isnt too old its contents are returned. The method itself doesnt
	//define the name of the cached file based off the paramters; that must be done by the calling
	//method and be part of the cache_file name
	function cache_or_call_based_off_age($class, $method, $param, $cache_file, $max_age_in_seconds){
		$html="";
		$file_util = new FileUtil();
		
		if ($max_age_in_seconds==0 || !file_exists($cache_file) || $file_util->get_file_age_in_seconds($cache_file)>$max_age_in_seconds){
			touch($cache_file);
			$html=$class->$method($param);
			if ($html!=""){
				$file_util->save_string_as_file($cache_file, $html);
			}
		}
		if ($html==""){
			$html=$file_util->load_file_as_string($cache_file);
		}
		return $html;
	}	

} //end class
