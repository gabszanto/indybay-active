<?php
//--------------------------------------------
//Indybay NewswireCache Class
//Written January 2006
//
//Modification Log:
//12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewswireCache");

require_once(CLASS_PATH."/cache/cache_class.inc");
require_once(CLASS_PATH."/renderer/newswire_renderer_class.inc");
require_once(CLASS_PATH."/db/newswire_db_class.inc");
require_once(CLASS_PATH."/db/feature_page_db_class.inc");

//this class is responsible for generating the newswires included on the 
//feature pages. Most of the code relates to only updating the needed newswires
//when events are classified or change category
class NewswireCache extends Cache
{

	//regenerate newswires for a newsitem that just came in
	function regenerate_newswires_for_new_newsitem($news_item_id){
		
		$this->track_method_entry("NewswireCache","regenerate_newswires_for_new_newsitem", "news_item_id", $news_item_id);
		
		$old_cats= array();
		$this->regenerate_newswires_for_newsitem_helper($news_item_id, $old_cats, NEWS_ITEM_STATUS_ID_NEW,NEWS_ITEM_STATUS_ID_NEW);
		
		$this->track_method_exit("NewswireCache","regenerate_newswires_for_new_newsitem");
	}
	
	
	
	//regenerate newswires for a changed newsitem
	function regenerate_newswires_for_newsitem($news_item_id, $old_cats, $old_display_status, $new_display_status){
		
		$this->track_method_entry("NewswireCache","regenerate_newswires_for_new_newsitem", "news_item_id", $news_item_id);
		
		$this->regenerate_newswires_for_newsitem_helper($news_item_id, $old_cats, $old_display_status, $new_display_status);
		
		$this->track_method_exit("NewswireCache","regenerate_newswires_for_new_newsitem");
	}
	
	
	
	//regenerate newswires for a changed newsitem or new item
	function regenerate_newswires_for_newsitem_helper($news_item_id, $old_cats, $old_display_status, $new_display_status){
	
		$this->track_method_entry("NewswireCache","regenerate_newswires_for_newsitem_helper", "news_item_id", $news_item_id);
		
		$news_item_db_class= new NewsItemDB();
		$page_db_class= new FeaturePageDB();
		$new_categories=$news_item_db_class->get_news_item_category_ids($news_item_id);
		$page_id_list=$page_db_class->get_associated_page_ids(array_merge($new_categories,$old_cats));
		$page_id_list=array_merge($page_id_list,$page_db_class->get_pages_with_all_category_newswires());
		foreach ($page_id_list as $page_id){
			$this->regenerate_newswire_for_page_helper($page_id, $old_display_status, $new_display_status);
		}

		$this->track_method_exit("NewswireCache","regenerate_newswires_for_newsitem_helper");	
	}



	//force full regeneration of all the newswire sections on a specific page
	function force_regenerate_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","force_regenerate_newswire_for_page", "page_id", $page_id);	
	
		$this->regenerate_newswire_for_page_helper($page_id, 0,0);
		
		$this->track_method_exit("NewswireCache","force_regenerate_newswire_for_page");	
	}
	
	

	//helper method used in regeneration of newswire sections on a page if they need to be updated
	function regenerate_newswire_for_page_helper($page_id, $old_display_status, $new_display_status){
		
		$this->track_method_entry("NewswireCache","regenerate_newswire_for_page_helper", "page_id", $page_id);	
		
		$page_db_class= new FeaturePageDB;
		$page_info=$page_db_class->get_feature_page_info($page_id);
		$page_name="page".$page_info['page_id'];
		$newswire_type_id=$page_info['newswire_type_id'];
		$items_per_newswire_section=$page_info['items_per_newswire_section'];
		$associated_category_array=array();
	
		if ($old_display_status==0 && $new_display_status==0){
			$force_regenerate=1;
		}
		if ($newswire_type_id!=NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_NOCAT){
			$associated_category_array=$page_db_class->get_category_ids_for_page($page_id);
		}

		if ($newswire_type_id==NEWSWIRE_TYPE_ALL_CAT){
			$this->regenerate_all_newswire_for_categories($page_name, $associated_category_array,$items_per_newswire_section);
		}else if ($newswire_type_id==NEWSWIRE_TYPE_CLASSIFIED_CAT){
			if (!empty($force_regenerate) || !($old_display_status==NEWS_ITEM_STATUS_ID_HIDDEN || $old_display_status==NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN
			   || $new_display_status==NEWS_ITEM_STATUS_ID_HIDDEN || $new_display_status==NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN))
					$this->regenerate_classified_newswire($page_name, $associated_category_array,$items_per_newswire_section);
		}else{
			if (isset($force_regenerate) || $old_display_status==NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED || 
					$new_display_status==NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED){
				$this->regenerate_local_newswire($page_name, $associated_category_array,$items_per_newswire_section);
				
			}
			if (isset($force_regenerate) ||$old_display_status==NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED || 
					$new_display_status==NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED){	
					$this->regenerate_nonlocal_newswire($page_name, $associated_category_array,$items_per_newswire_section);
			}
			$this->regenerate_other_newswire($page_name, $associated_category_array,$items_per_newswire_section);
		}
		
		$this->track_method_exit("NewswireCache","regenerate_newswire_for_page_helper");

	}


	
	//recache the local newswire on a page
	function regenerate_local_newswire($page_name, $category_list, $items_per_newswire_section){
		
		$this->track_method_entry("NewswireCache","regenerate_local_newswire", "page_name", $page_name);	
		$newswire_db_class=new NewswireDB;
		$newswire_cache_file_name="newswire"."_".$page_name."_local";
		if (isset($GLOBALS["part_of_bulk_operation"]) && $GLOBALS["part_of_bulk_operation"]==1 && sizeof($category_list)!=0){
			$this->mark_newswire_file_for_regenerate_on_view($newswire_cache_file_name);
		}else{
			$newswire_list=$newswire_db_class->get_local_list_for_categories($category_list,$items_per_newswire_section);
			$this->regenerate_newswire_given_result_data($newswire_cache_file_name,$newswire_list);
		}
		$this->track_method_exit("NewswireCache","regenerate_local_newswire");
	}


	
	//recache the nonlocal newswire on a page
	function regenerate_nonlocal_newswire($page_name,$category_list, $items_per_newswire_section){
	
		$this->track_method_entry("NewswireCache","regenerate_nonlocal_newswire", "page_name", $page_name);	
		
		$newswire_db_class=new NewswireDB;
		$newswire_cache_file_name="newswire"."_".$page_name."_nonlocal";
		if (array_key_exists("part_of_bulk_operation",$GLOBALS) && $GLOBALS["part_of_bulk_operation"]==1 && sizeof($category_list)!=0){
			$this->mark_newswire_file_for_regenerate_on_view($newswire_cache_file_name);
		}else{
			$newswire_list=$newswire_db_class->get_nonlocal_list_for_categories($category_list,$items_per_newswire_section);
			$this->regenerate_newswire_given_result_data($newswire_cache_file_name,$newswire_list);
		}
		$this->track_method_exit("NewswireCache","regenerate_nonlocal_newswire");
	}
	
	
	
	//recache the other newswire on a page
	function regenerate_other_newswire($page_name,$category_list, $items_per_newswire_section){
	
		$this->track_method_entry("NewswireCache","regenerate_other_newswire", "page_name", $page_name);	
		
		$newswire_db_class=new NewswireDB;
		$newswire_cache_file_name="newswire"."_".$page_name."_other";
		
		if (array_key_exists("part_of_bulk_operation", $GLOBALS) && $GLOBALS["part_of_bulk_operation"]==1 && sizeof($category_list)!=0){
			$this->mark_newswire_file_for_regenerate_on_view($newswire_cache_file_name);
		}else{
			$newswire_list=$newswire_db_class->get_other_list_for_categories($category_list,$items_per_newswire_section);
			$this->regenerate_newswire_given_result_data($newswire_cache_file_name,$newswire_list);
		}
		
		$this->track_method_exit("NewswireCache","regenerate_other_newswire");
	}
	
	
	
	//recache the classified newswire on a page
	function regenerate_classified_newswire($page_name,$category_list, $items_per_newswire_section){
	
		$this->track_method_entry("NewswireCache","regenerate_classified_newswire", "page_name", $page_name);	
		
		$newswire_db_class=new NewswireDB;
		$newswire_cache_file_name="newswire"."_".$page_name."_classified";
		if (isset($GLOBALS['part_of_bulk_operation']) && $GLOBALS['part_of_bulk_operation'] == 1 && sizeof($category_list) != 0) {
			$this->mark_newswire_file_for_regenerate_on_view($newswire_cache_file_name);
		}else{
			$newswire_list=$newswire_db_class->get_classified_list_for_categories($category_list,$items_per_newswire_section);
			$this->regenerate_newswire_given_result_data($newswire_cache_file_name,$newswire_list);
		}
		$this->track_method_exit("NewswireCache","regenerate_classified_newswire");
	}
	
	
	
	//recache the all newswire on a page
	function regenerate_all_newswire_for_categories($page_name,$category_list, $items_per_newswire_section){
		
		$this->track_method_entry("NewswireCache","regenerate_all_newswire_for_categories", "page_name", $page_name);	
		
		$newswire_db_class=new NewswireDB;
		$newswire_cache_file_name="newswire"."_".$page_name."_all";
		if ($GLOBALS['part_of_bulk_operation']==1 && sizeof($category_list)==0){
			$this->mark_newswire_file_for_regenerate_on_view($newswire_cache_file_name);
		}else{
			$newswire_list=$newswire_db_class->get_all_list_for_categories($category_list,$items_per_newswire_section);
			$this->regenerate_newswire_given_result_data($newswire_cache_file_name,$newswire_list);
		}
		$this->track_method_exit("NewswireCache","regenerate_all_newswire_for_categories");
	}
	
	
	
	//helper method used in saving of any newswire
	function regenerate_newswire_given_result_data($newswire_cache_file_name, $result_data){
		
		$this->track_method_entry("NewswireCache","regenerate_newswire_given_result_data", "newswire_cache_file_name", $newswire_cache_file_name);	
		
		$file_path=CACHE_PATH."/newswires/".$newswire_cache_file_name;
		$newswire_renderer_class= new NewswireRenderer();
		$html=$newswire_renderer_class->get_newswire_rows_html($result_data);
		$cache_class= new Cache;
		if ($html!="")
			$cache_class->cache_file($file_path,$html);
		
		$this->track_method_exit("NewswireCache","regenerate_newswire_given_result_data");
	
	}
	
	
	//puts a hidden field in the newswire section forcing it to be regenerated the next time you look at it
	//this is mainly needed so updates to posts that are on a lot of pages
	//dont take forever to save. Instead of regenerating the newswires when an item in them changes
	//this just sets the newswire itself to have a tag in it telling the cache code that loads
	//it to rerun the newswire section on next viewing
	function mark_newswire_file_for_regenerate_on_view($cachefile_name){
		$this->track_method_entry("NewswireCache","mark_newswire_file_for_regenerate_on_view");	
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string(CACHE_PATH."/newswires/".$cachefile_name);
		$str=" <!--regen-->".$str;
		$file_util->save_string_as_file(CACHE_PATH."/newswires/".$cachefile_name,$str);
		$this->track_method_exit("NewswireCache","mark_newswire_file_for_regenerate_on_view");
	}
	
	
	
	
	//loads the highlighted local section of the newswire for a given page
	//The code first tries to load from cache but if it is marked dirty it will rerun the newswire
	function load_local_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","load_local_newswire_for_page", "page_id", $page_id);	
		
		$cachefile_name="newswire"."_page".$page_id."_local";
		$file_path=CACHE_PATH."/newswires/".$cachefile_name;
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string($file_path);
		if (strpos($str, "<!--regen-->")>0){
			$this->regenerate_newswire_for_page_helper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED,NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
			$str=$file_util->load_file_as_string($file_path);
		}
		$this->track_method_exit("NewswireCache","load_local_newswire_for_page");
		
		return $str;
	}
	
	
	
	//loads the highlighted non-local (ie global) section of the newswire for a given page
	//The code first tries to load from cache but if it is marked dirty it will rerun the newswire
	function load_nonlocal_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","load_nonlocal_newswire_for_page", "page_id", $page_id);	
		
		$cachefile_name="newswire"."_page".$page_id."_nonlocal";
		$file_path=CACHE_PATH."/newswires/".$cachefile_name;
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string($file_path);
		if (strpos($str, "<!--regen-->")>0){
			$this->regenerate_newswire_for_page_helper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED,NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
			$str=$file_util->load_file_as_string($file_path);
		}
		$this->track_method_exit("NewswireCache","load_nonlocal_newswire_for_page");
		
		return $str;
	}
	
	
	
	//loads the "other/breaking" section of the newswire for a given page
	//The code first tries to load from cache but if it is marked dirty it will rerun the newswire
	function load_other_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","load_other_newswire_for_page", "page_id", $page_id);	
		
		$cachefile_name="newswire"."_page".$page_id."_other";
		$file_path=CACHE_PATH."/newswires/".$cachefile_name;
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string($file_path);
		if (strpos($str, "<!--regen-->")>0){
			$this->regenerate_newswire_for_page_helper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED,NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
			$str=$file_util->load_file_as_string($file_path);
		}
		$this->track_method_exit("NewswireCache","load_other_newswire_for_page");
		
		return $str;
	}
	


	//loads the classified newswire for a given page
	//This newswire is mainly used by international pages on Indybay
	//it has no sections and displays anything that has been classified as editors
	//to new anything but OTHER, NEW, HIDDEN or QUESTIONABLE (hidden or unhidden)
	function load_classified_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","load_classified_newswire_for_page", "page_id", $page_id);	
		
		$cachefile_name="newswire"."_page".$page_id."_classified";
		$file_path=CACHE_PATH."/newswires/".$cachefile_name;
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string($file_path);
		if (strpos($str, "<!--regen-->")>0){
			$this->regenerate_newswire_for_page_helper($page_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED,NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
			$str=$file_util->load_file_as_string($file_path);
		}
		$this->track_method_exit("NewswireCache","load_classified_newswire_for_page");
		
		return $str;
	}
	
	
	
	
	//loads the full newswire for a given page
	//This newswire is not a part of Indybay
	//it has no sections and displays anything that has not been classified as editors
	//as HIDDEN or QUESTIONABLE HIDDEN
	function load_all_newswire_for_page($page_id){
		
		$this->track_method_entry("NewswireCache","load_all_newswire_for_page", "page_id", $page_id);	
		
		$cachefile_name="newswire"."_page".$page_id."_all";
		$file_path=CACHE_PATH."/newswires/".$cachefile_name;
		$file_util= new FileUtil();
		$str=$file_util->load_file_as_string($file_path);
		if (strpos($str, "<!--regen-->")>0){
			$this->regenerate_newswire_for_page_helper($page_id, NEWS_ITEM_STATUS_ID_OTHER,NEWS_ITEM_STATUS_ID_OTHER);
			$str=$file_util->load_file_as_string($file_path);
		}
		$this->track_method_exit("NewswireCache","load_all_newswire_for_page");
		
		return $str;
	}
	

	

} //end class
?>
