<?php
//--------------------------------------------
//Indybay NewsItemCache Class
//Written January 2006
//
//Modification Log:
//12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewsItemCache");

require_once ("cache/cache_class.inc");



// the newsitem cache class included common caching methods used
// by articles, events, blurbs and breaking news items
class NewsItemCache extends Cache
{


	//returns the file name of the main files saved for a news item
	function get_file_name_for_news_item($news_item_id){
	
		$this->track_method_entry("NewsItemCache","get_file_name_for_news_item","news_item_id",$news_item_id);
		
		$file_name=$news_item_id.".php";
		
		$this->track_method_exit("NewsItemCache","get_file_name_for_news_item");
		
		return $file_name;
	}


	//returns the relative web path to a newsitem given its id and creation timstamp
	function get_web_cache_path_for_news_item_given_date($news_item_id, $creation_date){
	
		$this->track_method_entry("NewsItemCache","get_web_cache_path_for_news_item_given_date","news_item_id",$news_item_id);
		
		$cache_class= new Cache;
		$additional_date_path=$cache_class->get_rel_dir_from_date($creation_date);
		$rel_link=NEWS_REL_PATH."/".$additional_date_path. $this->get_file_name_for_news_item($news_item_id);
		
		$this->track_method_exit("NewsItemCache","get_web_cache_path_for_news_item_given_date");
		
		return $rel_link;
	}	

}
?>