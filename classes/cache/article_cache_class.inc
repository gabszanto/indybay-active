<?php
//--------------------------------------------
//Indybay ArticleCache Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"ArticleCache");

require_once(CLASS_PATH."/cache/news_item_cache_class.inc");
require_once(CLASS_PATH."/renderer/article_renderer_class.inc");
require_once(CLASS_PATH."/renderer/blurb_renderer_class.inc");
require_once(CLASS_PATH."/cache/newswire_cache_class.inc");
require_once(CLASS_PATH."/db/article_db_class.inc");
require_once(CLASS_PATH."/db/feature_page_db_class.inc");

// class dealing with caching posted newsitems (events & articles)
class ArticleCache extends NewsItemCache
{



	//get a rendered version of central section of a posted article or event
	//this is in the cache class since the rendered template is just a series of
	//includes
    function get_cached_include_article_html($directory, $article_info){

    	$this->track_method_entry("ArticleCache","get_cached_include_article_html");

    	$template_obj = new FastTemplate(TEMPLATE_PATH);
    	if ($article_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN){
    		if (isset($article_info["not_duplicate"]))
    			$template_name = "article/hidden_post_layout_template.tpl";
    		else
    			$template_name = "article/precheck_layout_template.tpl";
    	}else if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
    		$template_name = "calendar/calendar_layout_template.tpl";
    	}else if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB){
       		$template_name = "feature_page/single_blurb_layout_template.tpl";
    	}else if ($article_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED ||
    				 $article_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED){
       		$template_name = "article/highlighted_post_layout_template.tpl";
       	}else{
       		$template_name = "article/post_layout_template.tpl";
       	}
        $template_obj->clear_all();
        $template_obj->define(array("article_page" => $template_name));
        $temp_array=array();
        $temp_array["TPL_LOCAL_NEWS_ITEM_ID"]=$article_info["news_item_id"];
        $temp_array["TPL_LOCAL_TITLE1"]=str_replace("\"","\\\"",$article_info["title1"]);
        $temp_array["TPL_LOCAL_PATH"]=$directory;
    	$template_obj->assign($temp_array);
        $template_obj->parse("CONTENT","article_page");
        $result_html = $template_obj->fetch("CONTENT");

        $this->track_method_exit("ArticleCache","get_cached_include_article_html");

        return $result_html;

    }



    //returns the full php to store for an article
    function get_cached_full_article_file_html($directory, $article_info){

        $this->track_method_entry("ArticleCache","get_cached_full_article_file_html");

    	$feature_page_db_class = new FeaturePageDB();
    	$html  = "<?php\ninclude_once(\"".CLASS_PATH."/config/indybay.cfg\");\n$"."cache_class=new Cache();\r\n";
        $html .= "header('Last-Modified: " . gmdate(DATE_RFC1123) . "');\n\n";

        if (isset($article_info["not_duplicate"]) ||
        	$article_info["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_HIDDEN ){
        $page_links="";
        $html .= "$"."GLO"."BALS[\"news_item_id\"]=".$article_info["news_item_id"].";\n\n";
        if ($article_info["title1"]!="")
        	$html .= "$"."GLO"."BALS[\"page_title\"]='".str_replace (array('\\', "'"), array('', "\'"), $article_info["title1"])."';\n\n";
        else
        	$html .= "$"."GLO"."BALS[\"page_title\"]='".str_replace (array('\\', "'"), array('', "\'"), $article_info["title2"])."';\n\n";
        $html .= '$GLOBALS["page_summary"] = ' . "'" . str_replace(array('\\', "'"), array('', "\'"), strip_tags($article_info['summary']))."';\n\n";

        $image_url = '';
        $image_width = 0;
        $image_height = 0;
        $mime_type = '';
        if ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
          $body_class = 'event';
        }
        elseif ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_BLURB) {
          $body_class = 'feature-blurb';
        }
        else {
          $body_class = 'article-page';
        }
        if (!empty($article_info['media_attachment_id'])) {
          $media_attachment_db_class = new MediaAttachmentDB();
          $media_attachment_info = $media_attachment_db_class->get_media_attachment_info($article_info['media_attachment_id']);
          $media_grouping_id = $media_attachment_info['media_type_grouping_id'];
          if ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_IMAGE) {
            $body_class .= ' content-image';
          }
          elseif ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_AUDIO) {
            $body_class .= ' content-audio';
          }
          elseif ($media_grouping_id + 0 == MEDIA_TYPE_GROUPING_VIDEO) {
            $body_class .= ' content-video';
          }
          if ($image = $media_attachment_db_class->get_renderered_video_info($article_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM)) {
            $image_width = $image['image_width'];
            $image_height = $image['image_height'];
            $poster_url = SERVER_URL . UPLOAD_URL . $image['relative_path'] . str_replace(array('\\', "'"), array('', "\'"), $image['file_name']);
          }
          $image_url = SERVER_URL . UPLOAD_URL . $media_attachment_info['relative_path'] . str_replace(array('\\', "'"), array('', "\'"), $media_attachment_info['file_name']);
          $mime_type = $media_attachment_info['mime_type'];
        }
        $html .= '$GLOBALS["body_class"] = ' . "'$body_class';\n";
        $html .= '$GLOBALS["image_url"] = ' . "'" . $image_url . "';\n";
        $html .= '$GLOBALS["poster_url"] = ' . "'" . $poster_url . "';\n";
        $html .= '$GLOBALS["image_width"] = ' . "'" . $image_width . "';\n";
        $html .= '$GLOBALS["image_height"] = ' . "'" . $image_height . "';\n";
        $html .= '$GLOBALS["mime_type"] = ' . "'" . $mime_type . "';\n";

        if ($article_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN ||
        	$article_info["news_item_status_id"]==NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN){
        	$html .= "$"."GLO"."BALS[\"page_display\"]=\"f\";\n\n";
        }

        $html .= "$"."GLO"."BALS[\"page_ids\"]=array(";
        $i=0;

        $associated_pages=$feature_page_db_class->get_associated_pages_info($article_info["news_item_id"]);
        foreach ($associated_pages as $page_info){
        	if ($i>0){
        		$html .=", ";
        		$page_links .=" | ";
        	}
        	$html .=$page_info["page_id"];
        	$page_links.="<a href=\"/".$page_info["relative_path"]."\">";
        	$page_links .= Renderer::check_plain($page_info['long_display_name']);
        	$page_links.="</a>";
        	$i=$i+1;

        }
        $html .= ");\n\n";
        $html .= "include(INCLUDE_"."PATH.\"/article/article_header.inc\");\n";
        if (sizeof($page_links)>0){
        	$html .="?><h3>";
        	$html .= $page_links;
        	$html .="</h3><?php\n";
        }
        }
		$html .= $this->get_cached_include_article_html($directory,$article_info);
		if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB){
			$html .= "\n
			if (sizeof($"."_GET[\"printable\"])==0){\n
				include(INCLUDE_"."PATH.\"/common/center_right.inc\");\n
			}\n
			";
        }
        $html .= "include(INCLUDE_"."PATH.\"/article/article_footer.inc\"); ?" . ">";

        $this->track_method_exit("ArticleCache","get_cached_full_article_file_html");

        return $html;
    }

    //gets the php for the included comments include
    //this file is intended to be solely a list of safe includes so
    //for optimization adding comments can juts append to this file and
    //not require additional regeneration
    function get_cached_include_comment_html($news_item_id_array, $directory){

    	$this->track_method_entry("ArticleCache","get_cached_include_comment_html");

        if (sizeof($news_item_id_array)==0)
    		return "";
    	$html="";
    	foreach($news_item_id_array as $news_item_id){
    		$html .= "$"."cache_class->safe_include(\"".$directory.$news_item_id."_content.html\");\n";
    	}

    	$ret="\n<"."?php\n".$html."\n?".">\n";
    	$this->track_method_exit("ArticleCache","get_cached_include_comment_html");

    	return $ret;
    }


    //gets the php for the included attachments include
    //this works the same as the comments be is mainly to keep the main
    //layout template simple so it only has one include even if it has 20 attachments
    function get_cached_include_attachment_html($news_item_id_array,$directory){

        $this->track_method_entry("ArticleCache","get_cached_include_attachment_html");

    	if (sizeof($news_item_id_array)==0)
    		return "";
    	$html="";
    	foreach($news_item_id_array as $news_item_id){
    		$html .= "$"."cache_class->safe_include(\"".$directory.$news_item_id."_content.html\");\n";
    	}
    	$ret="\n<"."?php\n".$html."\n?".">\n";

    	$this->track_method_exit("ArticleCache","get_cached_include_attachment_html");

    	return $ret;
    }



    //gets the include php for comments and writes it to the correct file
    //see function that renders the php for more info on the purpose behind this
    function cache_comments_include_for_article($directory, $news_item_id,  $news_item_id_array){

        $this->track_method_entry("ArticleCache","cache_comments_include_for_article");

       	$file_path=$directory.$news_item_id."_comments.inc";
    	$html=$this->get_cached_include_comment_html($news_item_id_array,$directory);
    	$this->cache_file($file_path, $html);

    	$this->track_method_exit("ArticleCache","cache_comments_include_for_article");
    }


    //saves the latest comment box at that is included at the bottom of posts
    function cache_comment_box_for_article_if_needed($directory, $news_item_id){

        $this->track_method_entry("ArticleCache","cache_comment_box_for_article");

       	$article_renderer_class= new ArticleRenderer();
       	$file_path=$directory.$news_item_id."_commentbox.html";
    	$html=$article_renderer_class->get_rendered_comment_box_html($news_item_id,$directory);
    	$this->cache_file($file_path, $html);

    	$this->track_method_exit("ArticleCache","cache_comment_box_for_article");
    }

    //gets the include php for attachments and writes it to the correct file
    //see function that renders the php for more info on the purpose behind this
    function cache_attachment_include_for_article($directory, $news_item_id, $news_item_id_array){

    	$this->track_method_entry("ArticleCache","cache_attachment_include_for_article");

    	$file_path=$directory.$news_item_id."_attachments.inc";
    	$html=$this->get_cached_include_attachment_html($news_item_id_array,$directory);
    	$this->cache_file($file_path,$html);

    	$this->track_method_exit("ArticleCache","cache_attachment_include_for_article");
    }



    //gets the php for the main include for an article
    //and caches it
    function cache_main_include_for_article($directory, $article_info){

	    $this->track_method_entry("ArticleCache","cache_main_include_for_article");

    	$file_path=$directory.$article_info["news_item_id"].".php";
    	$html=$this->get_cached_full_article_file_html($directory,$article_info);
    	$this->cache_file($file_path,$html);

    	$this->cache_comment_box_for_article_if_needed($directory,$article_info["news_item_id"]);

    	$this->track_method_exit("ArticleCache","cache_main_include_for_article");
    }



    //saves the content of an article or event to a file
    function cache_content_for_article($directory, $article_info){

    	$this->track_method_entry("ArticleCache","cache_content_for_article");

    	$article_renderer_class= new ArticleRenderer;
    	$file_path=$directory.$article_info["news_item_id"]."_content.html";

    	if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB){
    		$blurb_renderer_class= new BlurbRenderer;
    		$html=$blurb_renderer_class->get_rendered_blurb_html_from_news_item_id($article_info["news_item_id"]);
    	}else
    		$html=$article_renderer_class->get_rendered_article_html($article_info);
    	$this->cache_file($file_path,$html);
    	if ($article_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
    		$html=$article_renderer_class->get_rendered_article_html($article_info);
    	}
    	$this->track_method_exit("ArticleCache","cache_content_for_article");

    	return $html;
    }



    //called when an article changes
    //the reason the main include is also refreshed is that we want to
    //make sure users get the new timestamp and dont still see the old file
    //because of browser caching
    function recache_just_article($news_item_id){

    	$this->track_method_entry("ArticleCache","recache_just_article", "news_item_id", $news_item_id);

    	$article_db_class= new ArticleDB;
    	$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
    	if ($article_info["parent_item_id"]+0!=0 && $article_info["parent_item_id"]!=$news_item_id){
    		$parent_info=$article_db_class->get_article_info_from_news_item_id($article_info["parent_item_id"]);
    	}else{
    		$parent_info=$article_info;
    	}
    	$directory=NEWS_PATH."/".$this->create_date_based_relative_directory(NEWS_PATH."/",$parent_info["creation_timestamp"]);

    	$this->cache_content_for_article($directory, $article_info);
    	$this->cache_main_include_for_article($directory, $parent_info);

    	$this->track_method_exit("ArticleCache","recache_just_article");
    }



    //recaches or initially caches everything for an artcile and returns a preview string
    //that includes the central section of the page but not the headers and footers
    function cache_everything_for_article($news_item_id, $syndicate = FALSE){

		$this->track_method_entry("ArticleCache","cache_everything_for_article", "news_item_id", $news_item_id);

    	$article_db_class= new ArticleDB;
    	$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
		if ($article_info["parent_item_id"]!=0 && $article_info["parent_item_id"]!=$news_item_id){
			$news_item_id=$article_info["parent_item_id"];
			$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
		}
    	$directory=NEWS_PATH."/".$this->create_date_based_relative_directory(NEWS_PATH."/",$article_info["creation_timestamp"]);
    	$preview_html=$this->cache_content_for_article($directory, $article_info);

	if ($syndicate) {
		$link = 'http://www.indybay.org/newsitems/' . $this->create_date_based_relative_directory(NEWS_PATH."/", $article_info["creation_timestamp"]);
	}

    	$attachment_ids_for_post=$article_db_class->get_attachment_ids($news_item_id);
    	foreach($attachment_ids_for_post as $child_id){
    		$child_info=$article_db_class->get_article_info_from_news_item_id($child_id);
    		$preview_html.=$this->cache_content_for_article($directory, $child_info);
    	}
    	$this->cache_attachment_include_for_article($directory, $news_item_id,  $attachment_ids_for_post);
    	if ($article_info["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_HIDDEN){
	    	$comment_ids_for_post=$article_db_class->get_comment_ids($news_item_id);
	    	foreach($comment_ids_for_post as $child_id){
	    		$child_info=$article_db_class->get_article_info_from_news_item_id($child_id);
	    		$this->cache_content_for_article($directory, $child_info);
	    	}
    	}
    	$this->cache_comments_include_for_article($directory, $news_item_id,  $comment_ids_for_post);

    	$this->cache_main_include_for_article($directory, $article_info);

    	$this->track_method_exit("ArticleCache","cache_everything_for_article");

    	return $preview_html;
    }



    //if an attachment changes and an editor chooses a full recache this method is called
    //it will likely always call the comment regenerate but is kept seperate in
    //case there are ever differences
    function cache_everything_for_attachment($news_item_id, $parent_item_info){

    	$this->track_method_entry("ArticleCache","cache_everything_for_attachment", "news_item_id", $news_item_id);

    	$ret= $this->cache_everything_for_comment($news_item_id, $parent_item_info);

    	$this->track_method_exit("ArticleCache","cache_everything_for_attachment");

    	return $ret;
    }



    //If a comment changes and an editor chooses a full recache this method is called
    //usually only the comment include itself need change since the parent safe_include will
    //just include  an empty string if the comment is hidden but if things ever get
    //messed up with a post this lets an editor force the recache
    function cache_everything_for_comment($news_item_id, $parent_item_info){

    		$this->track_method_entry("ArticleCache","cache_everything_for_comment", "news_item_id", $news_item_id);

    		$article_db_class= new ArticleDB;
    		$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
    		$parent_info=$article_db_class->get_article_info_from_news_item_id($parent_item_info);
    		$directory=NEWS_PATH."/".$this->create_date_based_relative_directory(NEWS_PATH."/",$parent_info["creation_timestamp"]);
    		$preview_html=$this->cache_content_for_article($directory, $article_info);

    		$comment_ids_for_post=$article_db_class->get_comment_ids($parent_item_info);
    		$this->cache_comments_include_for_article($directory, $parent_item_info,  $comment_ids_for_post);

    		$this->cache_main_include_for_article($directory, $parent_info);

    		$this->track_method_exit("ArticleCache","cache_everything_for_comment");

    		return $preview_html;
    }



    //returns the relative url for an artcile or event (this can be used either
    //to access the file from the root file directory or to create a url based off the
    //root web path)
    function get_relative_url_for_article($news_item_id){

        $this->track_method_entry("ArticleCache","get_relative_url_for_article", "news_item_id", $news_item_id);


        $article_db_class= new ArticleDB;
    	$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);

    	$directory="/newsitems"."/".$this->create_date_based_relative_directory(NEWS_PATH."/",$article_info["creation_timestamp"]);
    	$rel_url=$directory.$news_item_id.".php";

    	$this->track_method_exit("ArticleCache","get_relative_url_for_article");

    	return $rel_url;
    }

    //if you want to make the normal cache file for a news item actually redirect to another item
    //you can use this. It is mainly useful for hidden posts and posts turned into attachments
    function cache_redirect_for_article($news_item_id, $news_item_id2){
            $this->track_method_entry("ArticleCache","cache_redirect_for_article", "news_item_id", $news_item_id);

        	$article_db_class= new ArticleDB;
    		$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
    		if (is_array($article_info)){
        		$new_url=$this->get_relative_url_for_article($news_item_id2);
        		$directory=NEWS_PATH."/".$this->create_date_based_relative_directory(NEWS_PATH."/",$article_info["creation_timestamp"]);
        		$file_path=$directory.$article_info["news_item_id"].".php";
        		$html="<?php\nheader(\"HTTP/1.0 301 Moved Permanently\");\n";
				$html.="header(\"Location: ".SERVER_URL.$new_url."\");\n";
				$html.="?>";
        		$this->cache_file($file_path,$html);
        	}

            $this->track_method_exit("ArticleCache","cache_redirect_for_article");
    }



} //end class
?>
