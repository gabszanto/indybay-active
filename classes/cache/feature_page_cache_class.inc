<?php
//--------------------------------------------
//Indybay FeaturePageCache Class
//Written January 2006
//
//Modification Log:
//12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"FeaturePageCache");

require_once(CLASS_PATH."/cache/cache_class.inc");
require_once(CLASS_PATH."/cache/article_cache_class.inc");
require_once(CLASS_PATH."/cache/event_list_cache_class.inc");
require_once(CLASS_PATH."/cache/newswire_cache_class.inc");
require_once(CLASS_PATH."/renderer/feature_page_renderer_class.inc");

//Caches center columns for pages 
//as well as data used in rendering of left column
//and headers for pages
class FeaturePageCache extends Cache
{
    
    
   //caches the newswire template for a page an regenerates the newswire caches
   //included by the template
   function cache_newswire_template_for_page($page_info){
       	
       	$this->track_method_entry("FeaturePageCache","cache_newswire_template_for_page", "page_id", $page_info["page_id"]);
       	
    	$newswire_type_id=$page_info["newswire_type_id"];
    	if ($newswire_type_id==NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_CAT ||
    		 $newswire_type_id==NEWSWIRE_TYPE_LOCAL_NONLOCAL_OTHER_NOCAT){
        	$template_name = "newswire/newswire_lgo.tpl";
    	} else if ($newswire_type_id==NEWSWIRE_TYPE_ALL_CAT ){
        	$template_name = "newswire/newswire_all.tpl";
        }else if ($newswire_type_id==NEWSWIRE_TYPE_CLASSIFIED_CAT){
        	$template_name = "newswire/newswire_classified.tpl";
        }
        if (sizeof($template_name)>0){
        	$template_obj = new FastTemplate(TEMPLATE_PATH);
        	$template_obj->clear_all();
        	$template_obj->define(array(newswire => $template_name));
        	$temp_array=array("TPL_LOCAL_PAGE_ID"=>$page_info["page_id"]);
    		$template_obj->assign($temp_array);
        	$template_obj->parse(CONTENT,"newswire");
        	$result_html = $template_obj->fetch("CONTENT");
        	$file_path=CACHE_PATH."/newswires/newswire_page".$page_info["page_id"];
        	$cache_class= new Cache;
        	$cache_class->cache_file($file_path, $result_html);
        }	
    	$newswire_cache_class= new NewswireCache();	
    	$newswire_cache_class->force_regenerate_newswire_for_page($page_info['page_id']);
    	
    	$this->track_method_exit("FeaturePageCache","cache_newswire_template_for_page");
    }
    
    
    
    //given html for a center column caches it for use in the includes by the 
    //index.php files
    function cache_center_column($page_info, $html_for_center_column){
    	
    	$this->track_method_entry("FeaturePageCache","cache_center_column", "page_id", $page_info["page_id"]);
    	
    	$file_path=CACHE_PATH."/feature_page/center_columns/center_column_page".$page_info["page_id"].".html";
        $cache_class= new Cache;
        $cache_class->cache_file($file_path, $html_for_center_column);
        $this->track_method_exit("FeaturePageCache","cache_center_column");
    }
    
    
    //cache or recache all blurbs on a page
    function cache_all_blurbs_on_page($page_id){
    
    	$this->track_method_entry("FeaturePageCache","cache_all_blurbs_on_page", "page_id", $page_id);
    	
    	$feature_page_db_class= new FeaturePageDB();
    	$article_cache_class= new ArticleCache();
    	$current_blurbs=$feature_page_db_class->get_current_blurb_list($page_id);
    	$news_item_db=new NewsItemDB();
    	foreach($current_blurbs as $blurb_info){
		$syndicate = FALSE;
                if ($page_id == FRONT_PAGE_CATEGORY_ID) {
                  if (!$blurb_info['syndicated']) {

//    [news_item_version_id] => 18791015
//    [news_item_id] => 18701483
//    [title1] => Vacant Bank Occupied in Santa Cruz
//    [title2] => 75 River Street Occupied as Community Center for 75+ Hours
//    [displayed_author_name] => 
//    [email] => 
//    [phone] => 
//    [address] => 
//    [displayed_date] => 2011-12-4 9:59:00
//    [event_duration] => 0
//    [summary] => On November 30th, more than a hundred activists in Santa Cruz demonstrated in front of a Chase bank, before marching 
//    [text] => On November 30th, more than a hundred activists in Santa Cruz demonstrated in front of a Chase bank, before marching to 
//    [is_summary_html] => 1
//    [is_text_html] => 1
//    [related_url] => /newsitems/2011/12/01/18701406.php
//    [display_contact_info] => 0
//    [media_attachment_id] => 18639615
//    [thumbnail_media_attachment_id] => 18639614
//    [version_creation_date] => 2011-12-04 09:59:29
//    [version_created_by_id] => 69
//    [modified] => 2011-12-04 09:59 AM
//    [displayed_date_year] => 2011
//    [displayed_date_month] => 12
//    [displayed_date_day] => 04
//    [displayed_date_hour] => 09
//    [displayed_date_minute] => 59
//    [displayed_timestamp] => 1323021540
//    [modified_timestamp] => 1323021569
//    [current_version_id] => 18791015
//    [news_item_type_id] => 6
//    [news_item_status_id] => 3
//    [parent_item_id] => 18701483
//    [creation_timestamp] => 1322740830
//    [created] => Thursday Dec 1st, 2011 4:00 AM 
//    [created_by_id] => 69


//  require('HTTP/OAuth/Consumer.php');
//  $request = new HTTP_Request2('https://graph.facebook.com/' . FACEBOOK_PAGE_ID . '/feed', HTTP_Request2::METHOD_POST, array('ssl_verify_peer' => FALSE));
//  $request->addPostParameter('access_token', FACEBOOK_PAGE_ACCESS_TOKEN);
//  $request->addPostParameter('uid', FACEBOOK_PAGE_ID);
//  $request->addPostParameter('message', 'Blurb text');
//  $request->addPostParameter('picture', 'http://icanhascheezburger.files.wordpress.com/2009/03/funny-pictures-kitten-finished-his-milk-and-wants-a-cookie.jpg');
//  $request->addPostParameter('link', 'http://www.myvideosite/videopage.html');
		    $syndicate = TRUE;
                    $news_item_db->update_syndicated($blurb_info['news_item_id']);
                  }
                }
    		$article_cache_class->cache_everything_for_article($blurb_info["news_item_id"], $syndicate);
    		$news_item_db->update_news_item_status_id($blurb_info["news_item_id"],NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
    	}
    	
    	$this->track_method_exit("FeaturePageCache","cache_all_blurbs_on_page");
    
    }
    
    
    //recaches a feature page given its id
   function cache_feature_page($page_id){ 
   
   		$this->track_method_entry("FeaturePageCache","cache_feature_page", "page_id", $page_id);
   		
   		$feature_page_renderer_class= new FeaturePageRenderer();
   		$feature_page_db_class= new FeaturePageDB();
   		$page_info= $feature_page_db_class->get_feature_page_info($page_id);
   		$blurb_list = $feature_page_db_class->get_current_blurb_list($page_id);
		$html_for_page=$feature_page_renderer_class->render_page($blurb_list,$page_info );
		$this->cache_center_column($page_info, $html_for_page);
		$this->cache_all_blurbs_on_page($page_id);
		$feature_page_db_class->update_page_pushed_live_date($page_id);
		
		$this->track_method_exit("FeaturePageCache","cache_all_blurbs_on_page");
    }
    
    
    
    //loads the cache for a feature page given its id
    function load_cache_for_feature_page($page_id){
    	
    	$this->track_method_entry("FeaturePageCache","load_cache_for_feature_page", "page_id", $page_id);
   		
   		$file_util= new FileUtil();
		$center_column_file=CACHE_PATH. "/feature_page/center_columns/center_column_page".$page_id.".html";
		if (!file_exists($center_column_file)){
			if (isset($page_id) &&  $page_id>0){
				$this->cache_feature_page($page_id);
			}
		}
		if (file_exists($center_column_file)){
			
			$ret=$file_util->load_file_as_string($center_column_file);
			if (strpos($ret,"INDYBAY_HIGHLIGHTED_EVENTS")>0){
				$event_list_cache_class= new EventListCache();
				$cached_events=$event_list_cache_class->load_cached_highlighted_events_cache_for_page($page_id);
				$ret=str_replace("INDYBAY_HIGHLIGHTED_EVENTS",$cached_events, $ret);
			}
		}

		$this->track_method_exit("FeaturePageCache","load_cache_for_feature_page");
		
		return $ret;
    
    }
    
    //loads cached list of recent features for a given page (intended for bottom of single blurb view and other such places)
    function load_cached_recent_features($page_id){
	
		$this->track_method_entry("EventListCache","load_cached_recent_features");
		
		$file_path=CACHE_PATH."/feature_page/recent_blurbs/recent_blurbs_for_page_".$page_id;
		$ret=$this->cache_or_call_based_off_age(new FeaturePageRenderer, "render_recent_feature_list", $page_id, $file_path, 600);
		
		$this->track_method_exit("EventListCache","load_cached_recent_features");
		
		return $ret;
	}

} //end class
