<?php
//--------------------------------------------
//Indybay EventCache Class
//Written January 2006
//
//Modification Log:
//1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"EventCache");

require_once(CLASS_PATH."/cache/article_cache_class.inc");
require_once(CLASS_PATH."/renderer/event_renderer_class.inc");



//caches events, days and event weeks
//since events act a lot like articles most of the 
//single event caching is inherited from ArticleCache and this
//class mainly deals with caching days and weeks
class EventCache extends ArticleCache{
	
	
	
	//caches the html for a week given a dat in it
	//loops through and recaches the days and 
	//also cache the week wrapper around the days
	function recache_week_given_date($date) {
	
		$this->track_method_entry("EventCache","recache_week_given_date");
		
		$date->find_start_of_week();
		$event_renderer=new EventRenderer();	
		$html=$event_renderer->render_week_html($date->get_unix_time(),0,0);
		
		$year=$date->get_year();
		$month=$date->get_month();
		$day=$date->get_day();
		$cache_file_name="calendar_week_".$year."_".$month."_".$day;
		
		$week_file_path=CACHE_PATH."/calendar/week_cache/".$cache_file_name;
		
	$this->cache_file($week_file_path,$html);
		$this->track_method_exit("EventCache","recache_week_given_date");
	}

	//returns the cache file for a week filtred by topic/region and status
	function get_cached_week($date, $topic_id, $region_id, $news_item_status_restriction){
	
		$this->track_method_entry("EventCache","get_cached_week");
	
		$event_renderer= new EventRenderer();

		$year=$date->get_year();
		$month=$date->get_month();
		$day=$date->get_day();

		$cache_file_name="calendar_week_".$year."_".$month."_".$day;
		$week_file_path=CACHE_PATH."/calendar/week_cache/".$cache_file_name;
		if (!file_exists($week_file_path)){
			$this->recache_week_given_date($date);
		}

		if (file_exists($week_file_path)){
			$file_util= new FileUtil();
			$html=$file_util->load_file_as_string($week_file_path);
			$html=$event_renderer->filter_cached_html(
			$html, $topic_id, $region_id, $news_item_status_restriction);
		}
	
		$this->track_method_exit("EventCache","get_cached_week");
		
		return $html;
	}
	
	

}
?>