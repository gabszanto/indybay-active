<?php
require_once("syndication/rss_util_class.inc");
require_once("renderer/renderer_class.inc");
		
class rss_generator  
{

    function generate($search_fields)
    {
    
    	$logdb= new LogDB();

        
		$rss_util_class= new RSSUtil();
		
		$num_results = 30;
		$news_item_status_restriction = 0;
		$include_posts = 1;
		$include_events=0;
		$include_blurbs=0;
		$media_type_grouping_id=0;
		$topic_id=0;
		$region_id=0;
		
		
		if (!isset($search_fields['include_posts']) && !isset($search_fields['submitted_search'])
		&& !isset($search_fields['include_blurbs']))
    		$include_posts =1;
    	else if (isset($search_fields['include_posts']))
    		$include_posts = $search_fields['include_posts']+0;
    	else
    		$include_posts =0;
    	
    	if (isset($search_fields['media_type_grouping_id']))
    		$media_type_grouping_id = $search_fields['media_type_grouping_id']+0;
    	else
    		$media_type_grouping_id =0;
    	
    	if (isset($search_fields['include_comments']))
    		$include_comments = $search_fields['include_comments']+0; 
    	else
    		$include_comments =0;
    		
    	if (!isset($search_fields['include_attachments']) && $media_type_grouping_id!=0 && !isset($search_fields['submitted_search']))
    		$include_attachments=1;
    	else if (isset($search_fields['include_attachments']))
    		$include_attachments = $search_fields['include_attachments']+0;   
    	else
    		$include_attachments =0;
    		
    	if (!isset($search_fields['include_events']) && !isset($search_fields['submitted_search']) && !isset($search_fields['include_blurbs']))
    		$include_events =1;
    	else if (isset($search_fields['include_events']))
    		$include_events = $search_fields['include_events']+0; 
    	else
    		$include_events = 0;
    		
    	if (isset($search_fields['include_blurbs']))	
    		$include_blurbs = $search_fields['include_blurbs']+0; 
    	else
    		$include_blurbs =0;
    		
    	if (isset($search_fields['topic_id']))
    		$topic_id = $search_fields['topic_id']+0;
    	else
    		$topic_id = 0;
    	if (isset($search_fields['region_id']))
    		$region_id = $search_fields['region_id']+0;
    	else
    		$region_id = 0;

		if (isset($search_fields['page_id']))
    		$page_id=$search_fields['page_id']+0;
    	else
    		$page_id=0;
    		
    	///assumes topic and region ids line up with page ids
    	if ($region_id==0 && $page_id+0>0)
    		$region_id=$page_id;
    	if ($topic_id==0 && $page_id+0>0)
    		$topic_id=$page_id;
    		
    		

    	if (!isset($news_item_status_restriction) || $news_item_status_restriction==0 ){
    		if (isset($search_fields['news_item_status_restriction']) && $search_fields['news_item_status_restriction']+0!=0)	
				$news_item_status_restriction =$search_fields['news_item_status_restriction']+0;
			else
    			$news_item_status_restriction=690690;
    	}
    	
    	if (isset($search_fields["include_full_text"]))
			$include_full_text=$search_fields["include_full_text"]+0;
		else
			$include_full_text=0;
			
		if (isset($search_fields["rss_version"]))
			$rss_version=$search_fields["rss_version"]+0;
		else
			$rss_version=0;
			
		if (isset($search_fields["search"]))
			$search=strip_tags($search_fields['search']);
		else
			$search="";
			
		$result=$this->cache_helper($num_results, $news_item_status_restriction ,
		        	$include_posts, $include_events, $include_blurbs,
		        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version, $search);
		$result=$this->remove_exclusions($result, $_GET);	

        
		return $result;

    }
    
    function remove_exclusions($rss_string, $fields){

    	if (isset($_GET["exclude_duplicates_from_cat_1"]) && $_GET["exclude_duplicates_from_cat_1"]!="" && $_GET["rss_version"]==1){

    		$objXml= new DOMDocument();
    		$objXml->loadXML($rss_string );
    		$root_node=$objXml->getElementsByTagName("RDF")->item(0);
    		$nodes=$root_node->getElementsByTagName("item");
    		foreach ($nodes as $node){
				$subjects=$node->getElementsByTagName("subject");
				foreach($subjects as $subject) { 
					$add=1;
					if ($subject->nodeValue=="Santa Cruz"){
						$root_node->removeChild($node);
					}
				}
			}
			return $objXml->saveXML();
		}else if (isset($_GET["exclude_duplicates_from_cat_1"]) && $_GET["exclude_duplicates_from_cat_1"]!=""){
			
    		$objXml= new DOMDocument();
    		$objXml->loadXML($rss_string );
    		$root_node=$objXml->getElementsByTagName("channel")->item(0);
    		$nodes=$root_node->getElementsByTagName("item");
    		foreach ($nodes as $node){
				$subjects=$node->getElementsByTagName("subject");
				foreach($subjects as $subject) { 
					$add=1;
					if ($subject->nodeValue=="Santa Cruz"){
						$root_node->removeChild($node);
					}
				}
			}
			return $objXml->saveXML();
    	}else{
    		return $rss_string;
    	}
    
    }
    
    function cache_helper($num_results, $news_item_status_restriction ,
		        	$include_posts, $include_events, $include_blurbs,
		        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version, $search){
		    
		    $rss_util_class= new RSSUtil();    	
		    $cache_util= new Cache();
		    
		    $file_path=$this->make_rss_name($num_results, $news_item_status_restriction ,
		        	$include_posts, $include_events, $include_blurbs,
		        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version,$search);
		    $var_array= array();
		    $var_array[0]=$num_results+0;
		    $var_array[1]=$news_item_status_restriction+0;
		    $var_array[2]=$include_posts+0;
		    $var_array[3]=$include_events+0;
		    $var_array[4]=$include_blurbs+0;
		    $var_array[5]=$media_type_grouping_id+0;
		    $var_array[6]=$topic_id+0;
		    $var_array[7]=$region_id+0;
		    $var_array[8]=$include_full_text+0;
		    $var_array[9]=$rss_version+0;
		    $vars=implode("_",$var_array);
		    if ($search!=""){
		    	$args=array();
		    	$args[0]= $vars;
		    	$args[1]=$search;
		    	$result=$cache_util->cache_or_call_based_off_age($this, "rss_cache_helper", $args, $file_path, 30*60);
		    }else{
		    	$result=$cache_util->cache_or_call_based_off_age($this, "rss_cache_helper", $vars, $file_path, 30*60);
		    }
		    return $result; 
	}
	
	function rss_cache_helper($args){
	
		if (is_array($args)){
			$vars=$args[0];
			$search=$args[1];
		}else{
			$vars=$args;
			$search="";
		}
	
		$logdb= new LogDB();
		$logdb->add_log_entry_before_search();
		
		$rss_util_class= new RSSUtil();
		$var_array=explode("_",$vars);
		$var_array['num_results']= isset($num_results) ? $num_results : 0;
			$num_results=$var_array[0];
		    $news_item_status_restriction=$var_array[1];
		    $include_posts=$var_array[2];
		    $include_events=$var_array[3];
		    $include_blurbs=$var_array[4];
		    $media_type_grouping_id=$var_array[5];
		    $topic_id=$var_array[6];
		    $region_id=$var_array[7];
		    $include_full_text=$var_array[8];
		    $rss_version=$var_array[9];
		    
			$result=$rss_util_class->make_news_item_rss_string($num_results, $news_item_status_restriction ,
		        	$include_posts, $include_events, $include_blurbs,
		        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version, $search);    	

        	$logdb->update_log_id_after_search();
        
			return $result;
	}
	
    function make_rss_name($num_results, $news_item_status_restriction ,
		        	$include_posts, $include_events, $include_blurbs,
		        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version, $search=""){
		$filename=CACHE_PATH."/rss/cache_file_".$num_results."_".$news_item_status_restriction;
		$filename.="_".$include_posts."_".$include_events."_".$include_blurbs;
		$filename.="_".$media_type_grouping_id."_".$topic_id."_".$region_id."_".$include_full_text."_";
		
		if ($search!=""){
			$filename.=md5($search)."_";
		}
		$filename.=$rss_version.".rss";

		return $filename;      		
	}

}
