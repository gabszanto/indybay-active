<?php

// class used for /syn/index.php (builds the page & is used to cache local/cache/syndication_index.inc)
require_once("db/feature_page_db_class.inc");

class Syndication_index extends Page
{
    function syndication_index()
    {
        return 1;
    }



    function execute()
    {
                $feature_page_db_class= new FeaturePageDB();
               
   				$page_list=$feature_page_db_class->get_page_list(1,0);
				$this->tkeys['LOCAL_REGION_SYN_LIST'] = $this->render_syndication_rows($page_list);
				
				$page_list=$feature_page_db_class->get_page_list(2,0);
				$this->tkeys['LOCAL_TOPIC_SYN_LIST'] = $this->render_syndication_rows($page_list);
				
				$page_list=$feature_page_db_class->get_page_list(2,44);
      			$this->tkeys['LOCAL_INT_SYN_LIST'] = $this->render_syndication_rows($page_list);
        }
        
        function render_syndication_rows($page_list){
    		$syndication_index ="";
        	foreach ($page_list as $row)
                {
        		  $syndication_index .= "\r\n<tr><td><b>".$row["short_display_name"]."</b></td></tr>" ;
				  $syndication_index .= '<tr><td class="bgsearchgrey">'.$row["long_display_name"].'<br />' ;
 
                  $syndication_index .= "<a href=\"".ROOT_URL."syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_blurbs=1&amp;rss_version=1&amp;page_id=".$row["page_id"]."\">TPL_FEATURES</a> (simple ) -  ";
                  $syndication_index .= "\r\n<a href=\"".ROOT_URL."syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_blurbs=1&amp;rss_version=1&amp;include_full_text=1&amp;page_id=".$row["page_id"]."\">Features</a> (with content) -  ";
                   
                  $syndication_index .= "<a href=\"".ROOT_URL."syn/generate_rss.php?include_posts=1&amp;include_events=0&amp;news_item_status_restriction=3&amp;rss_version=1&amp;page_id=".$row["page_id"]."\">Newswire (rdf)</a> -";    
                  $syndication_index .= "<a href=\"".ROOT_URL."syn/generate_rss.php?include_posts=1&amp;include_events=0&amp;news_item_status_restriction=3&amp;page_id=".$row["page_id"]."\">Newswire (xml)</a> -";

				  $syndication_index .= "<a href=\"".ROOT_URL."syn/generate_rss.php?include_posts=0&amp;include_attachments=0&amp;include_events=0&amp;include_events=1&amp;page_id=".$row["page_id"]."\">" ;
				  $syndication_index .= "TPL_CALENDAR (rss)</A> </td></tr><tr><td>&nbsp;</td></tr>";
			}
			return $syndication_index;
				 
        }
}
?>
