<?php
// Class for admin_email page
require_once(CLASS_PATH."/db/feature_page_db_class.inc");
require_once(CLASS_PATH."/db/blurb_db_class.inc");
require_once(CLASS_PATH."/cache/article_cache_class.inc");
require_once (CLASS_PATH."/db/event_list_db_class.inc");
require_once (CLASS_PATH."/db/calendar_db_class.inc");
class admin_email extends Page
{
    function admin_email ()
    {
        // Class constructor, does nothing
        return 1;
    }
    function execute()
    {
        // mostly we are just assigning template variables here, publishing is done in publish class
        $tr = new Translate;
        $this->tkeys['local_success'] = '';
        $this->tkeys['local_sendto_address'] = $GLOBALS['mailing_list_news'];
        $this->tkeys['local_nicedate'] = date("l, F j, Y");
        $this->tkeys['local_textarea'] = "";
        $this->tkeys['local_msubject'] ="";
        if (isset($_POST['generate']))
        {
        	$suf=$this->getDaySuffix(strftime("%e"));
        	$this->tkeys['local_msubject'] = 'Indybay News Update for ' . strftime('%A %B %e' . $suf . ' %Y');
        	$this->tkeys['local_textarea'] = $this->tkeys['local_msubject'];
        	$this->tkeys['local_textarea'].="\nhttp://indybay.org\n\n";
        	$this->tkeys['local_textarea'].="Use this link to Publish an Article on Indybay!\n";
			$this->tkeys['local_textarea'].="http://indybay.org/publish.php\n\n";
			$this->tkeys['local_textarea'].="Publish Upcoming Events on the Indybay's Calendar!\n";
			$this->tkeys['local_textarea'].="http://indybay.org/calendar/event_add.php\n\n";
        	$this->tkeys['local_textarea'] .=$this->generate_recent_blurb_text(0);
        	$this->tkeys['local_textarea'].="\n\n";
        	$_POST["to"]="indybay-news@lists.riseup.net";
        	$_POST["from"]="indybay@lists.riseup.net";
        }else if (isset($_POST['generate_sc'])){https://lists.riseup.net/www/info/indybay-news
        	$suf=$this->getDaySuffix(strftime("%e"));
        	$this->tkeys['local_msubject'] ="Santa Cruz Indymedia Update For ".strftime("%A %B %e".$suf." %Y");
        	$this->tkeys['local_textarea']="Santa Cruz Indymedia Update For ".strftime("%A %B %e".$suf." %Y");;
        	$this->tkeys['local_textarea'].="\nhttp://indybay.org/santacruz \n\n";
        	$this->tkeys['local_textarea'].="check http://Indybay.org for more featured news!\n\n";

			$this->tkeys['local_textarea'].="Use this link to Publish an Article on SC-IMC!\n";
			$this->tkeys['local_textarea'].="http://indybay.org/publish.php?page_id=60\n\n";

			$this->tkeys['local_textarea'].="Publish Upcoming Events on the SC-IMC Calendar!\n";
			$this->tkeys['local_textarea'].="http://indybay.org/calendar/event_add.php?page_id=60\n\n";
			
			
        	$this->tkeys['local_textarea'] .=$this->generate_recent_blurb_text(60);
        	$this->tkeys['local_textarea'].="\n\n";
        	$_POST["to"]="scimc-news@lists.riseup.net";
        	//$_POST["from"]="scimc@indymedia.org";
        	$_POST["from"]="scimc-news@lists.riseup.net";
        }
        
        
        $to_options=array();
        $to_options["indybay-news@lists.riseup.net"]="indybay-news@lists.riseup.net";
   		$to_options["indybay@lists.riseup.net"]="indybay@lists.riseup.net";
   		//$to_options["sfbay-announce@lists.indymedia.org"]="sfbay-announce@lists.indymedia.org";
   		$to_options["scimc-news@lists.riseup.net"]="scimc-news@lists.riseup.net";
   		$to_options["bradley@riseup.net"]="Santa Cruz Developer (Bradley)";
   		$to_options["zogren@yahoo.com"]="Indybay Developer";
   		$renderer= new Renderer;
   		if (!isset($_POST["to"]))
   			$_POST["to"]="";
   		$this->tkeys['local_to_dropdown']=$renderer->make_select_form("to", $to_options, $_POST["to"]);
   		$from_options=array();
        $from_options["indybay@lists.riseup.net"]="indybay@lists.riseup.net";
   		//$from_options["scimc@indymedia.org"]="scimc@indymedia.org";
   		$from_options["scimc-news@lists.riseup.net"]="scimc-news@lists.riseup.net";
   		if (!isset($_POST["from"]))
   			$_POST["from"]="";
   		$this->tkeys['local_from_dropdown']=$renderer->make_select_form("from", $from_options, $_POST["from"]);

        if (isset($_POST['Submit']))
        {
            if (isset($_POST['failsafe']) && $_POST['failsafe']!="")
            {
                $msubject = $_POST['msubject'];
                $mbody = $_POST['mbody'];
                
                $this->tkeys['local_msubject'] = $_POST['msubject'];
            	$this->tkeys['local_textarea'] = $_POST['mbody'];
                $this->tkeys['local_success'] = $tr->trans('your_mail_not_sent');
                
                mail($_POST[to],$msubject,$mbody,"From: ".$_POST[from]);
                $this->tkeys['local_success'] = $tr->trans('your_mail_sent')." ".$_POST[to];
            } else
            {
            	$this->tkeys['local_msubject'] = $_POST['msubject'];
            	$this->tkeys['local_textarea'] = $_POST['mbody'];
                $this->tkeys['local_success'] = $tr->trans('your_mail_not_sent');
            }
        }
        
    }
    
    function generate_recent_blurb_text($page_id){
			$feature_page_db_class= new FeaturePageDB();
			
		$summary="* Upcoming Events\n";
		$upcomevent="UPCOMING EVENTS\n\n";
		$blurb_db_class= new BlurbDB();
		$article_cache_class= new ArticleCache();
		$results=$feature_page_db_class->get_recent_blurb_version_ids($page_id);
		$i=0;
		$previous_titles=array();
		$ret="";
		foreach ($results as $version_id){
			$blurb_info=$blurb_db_class->get_blurb_info_from_version_id($version_id);
			
			$title2=$blurb_info["title2"];
			$title1=$blurb_info["title1"];
			if (trim($title1)=="" || trim($title2)=="")
				continue;
				
			if (!isset($previous_titles[$title2])){
				$pages=$feature_page_db_class->get_pages_with_pushed_long_versions_of_blurb($blurb_info["news_item_id"]);
				if (isset($pages) && is_array($pages) && sizeof($pages)>0 && !(sizeof($pages)==1 && isset($pages[1]["page_id"]) && $pages[1]["page_id"]==FRONT_PAGE_CATEGORY_ID)){
				
					$create_time_formatted=strftime("%D",$blurb_info["creation_timestamp"]);
					
					$page_link="";
					$pi=0;
					$page_link="";
					
					foreach ($pages as $page_info){
							$pi=$pi+1;
							if ($pi>1)
								$page_link.=" | ";
							$page_link.=$page_info["short_display_name"];
					}	
					
					$previous_titles[$title2]=1;
					$searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($blurb_info["news_item_id"], $blurb_info["creation_timestamp"]);
					$row="";
					$row.=$blurb_info["title1"];
					$row.="\n";
					if ($blurb_info["title1"]!=$blurb_info["title2"]){
						$row.=$blurb_info["title2"]."\n";
					}
					if ($page_id==60){
						$row.=$create_time_formatted.": ";
					}else{
						$row.=$create_time_formatted." - ".$page_link;
						$row.="\n";
					}
					if (strlen($row)<700){
						$row.=str_replace("\r"," ",str_replace("\n"," ",strip_tags($blurb_info["summary"])));
					}else{
						$blurb_info["summary"]=subsubstr(strip_tags($blurb_info["summary"]),0,700);
						$j=strrpos(" ",$blurb_info["summary"]);
						if ($j>0){
							$blurb_info["summary"]=subsubstr($blurb_info["summary"],0,$j);
						}
						$row.=str_replace("\r"," ",str_replace("\n"," ",$blurb_info["summary"]))."...";
					}
					$row.="\n";
					$row.="http://www.indybay.org".$searchlink;
					$row.="\n\n\n";
					$summary.="* ".$blurb_info["title1"]."\n";

					$ret.=$row;
					$i=$i+1;
					if ($i>10)
					break;
				}
			}
		}
		$event_list_db= new EventListDB();
		if ($page_id==0){
			$events=$event_list_db->get_highlighted_list_helper(200,time()+(60*60*24)*8);
		}else{
			$catlist=array();
			$catlist[0]=$page_id;
			$events=$event_list_db->get_highlighted_list_for_categories_helper($catlist,200,time()+(60*60*24)*30);
		}
		$news_item_cache_class= new NewsItemCache;
		$news_item_db=new NewsItemDB();
		$calendar_db_class= new CalendarDB();
		$upcomeven="";
		foreach ($events as $event){
			$cats=$news_item_db->get_news_item_category_ids($event["news_item_id"]);
			$typel=$calendar_db_class->get_event_type_info_for_newsitem($event["news_item_id"]);
			if (is_array($typel)){
				$type=$typel["keyword"];
			}
			$st=$this->getDaySuffix(strftime("%e",$event["displayed_timestamp"]));
			$upcomevent.=strftime("%B %e".$st." %l:%M %p",$event["displayed_timestamp"]).": ".$event["title1"]." (".$type.")\n";
			$upcomevent.="http://www.indybay.org".$news_item_cache_class->get_web_cache_path_for_news_item_given_date($event["news_item_id"], $event["creation_timestamp"]);
			$upcomevent.="\n\n";
		}
		$upcomeven.="\n\n";
		
		if ($page_id==60){
			$ret.="----------------------------------------------------------------
Santa Cruz Indymedia is a member of the Independent Media Center network with a focus on local issues and the direct impact of larger  issues on our community. Santa Cruz Indymedia is not a membership  organization; it is a tactic, a concept, and a movement that can be  effectively utilized in many different ways.

Santa Cruz Independent Media Center
http://Indybay.org/SantaCruz
			";
		}else{
			$ret.="----------------------------------------------------------------
The San Francisco Bay Area Independent Media Center is a non-commercial, democratic collective of bay area independent media makers and media outlets, and serves as the local organizing unit of the global Indymedia network.";
		}
		
		$ret2=$summary."\n\n\n".$upcomevent;
		if ($page_id==0){
			$ret2.="More Events On Indybay's Calendar:\nhttp://www.indybay.org/calendar/";
		}else if ($page_id==60){
			$ret2.="More Events On Santa Cruz Indymedia's Calendar:\nhttp://www.indybay.org/calendar/?page_id=60";
		}else{
			$ret2.="More Events: http://www.indybay.org/calendar/?page_id="+$page_id;
		}
		$ret2.="\n\n\n".$ret;

   		return $ret2;
    }
    
function getDaySuffix($dayOfTheMonth) {
               switch ($dayOfTheMonth) {
                       case  1: return "st";
                       case  2: return "nd";
                       case  3: return "rd";
                       case 21: return "st";
                       case 22: return "nd";
                       case 23: return "rd";
                       case 31: return "st";
                       default: return "th";
               }
}
    
    
    
} // end class
