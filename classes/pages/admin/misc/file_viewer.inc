<?php

// Class for file_viewer page

class file_viewer extends Page {

    function file_viewer() {
        return 1;
    }

    function execute() {


	$fullfilename=$_GET["filename"];
	if (strlen($fullfilename)<1){
		$fullfilename=$_POST["filename"];
	}
	
	if (!file_exists($fullfilename)){
		echo "Error: No File Exists With Name ".$fullfilename;
		exit;
	}
	if (strpos(' '.$fullfilename,INCLUDE_PATH)==""
		&& strpos(' '.$fullfilename,WEB_PATH."/test")==""
		&& strpos(' '.$fullfilename,WEB_PATH."/themes")==""
		&& strpos(' '.$fullfilename,TEMPLATE_PATH)==""
		){
			echo "You do not have permissions to view or edit: ".$fullfilename;
			exit;
	}	
		

	if (isset($_POST["save"])>0){
		$lf=fopen($fullfilename,"w");
		$write=str_replace("\r","",$_POST["contents"]);
		fwrite($lf, $write);
		fclose($lf);
	}	

	$lf=fopen($fullfilename,"r");
	if ($lf){
		$file_util= new FileUtil;
		$contents=$file_util->load_file_as_string($fullfilename);
		$this->tkeys['contents'] = $contents;
        	$this->tkeys['filename'] = $fullfilename;
		return 1;
		fopen($lf);
	}
    }

}

?>


