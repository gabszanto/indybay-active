<?php

class stats extends Page {

    function stats() {
        return 1;
    }

    function execute() {
	$stats = file_get_contents(CACHE_PATH . '/pages/stats.html');
	$stats = str_replace(array('<html', '<body', '<head'), '<div', $stats);
	$stats = str_replace(array('</html', '</body', '</head'), '</div', $stats);
        print $stats;
	return 1;
    }

}
