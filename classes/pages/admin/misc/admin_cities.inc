<?php
// Refreshes the local cities list based on global XML master.
// Based on some dadaIMC code
//     license: GNU LGPL
//     copyright 2001-2002: dada typo and contributors
class admin_cities extends Page
{
    function admin_cities ()
    {
        return 1;
    }
    function execute()
    {
        $pathtofile = INDYBAY_BASE_PATH.'/cities.inc';
        $file = 'https://contact.indymedia.org/cities.xml';
        $GLOBALS['depth'] = 0;
        $GLOBALS['currenttag'] = '';
        $GLOBALS['items'] = array();
        $GLOBALS['listing'] = array();
    function startElement($parser, $name, $attrs)
    {
	$GLOBALS['currenttag'] = $name;
	if ($GLOBALS['currenttag'] == 'DICT')
        {
            $GLOBALS['depth']++;
        }
    }
    function endElement($parser, $name)
    {
        if ($GLOBALS['depth'] == 3 && count($GLOBALS['items']) >= 2)
        {
$GLOBALS['listing'][$GLOBALS['currentcat']][$GLOBALS['currentregion']][]=$GLOBALS['items'];
	    $GLOBALS['items'] = array();
	}
	if ($name == 'DICT')
        {
            $GLOBALS['depth']--;
        }
	$GLOBALS['currenttag'] = '';
    }
    function characterData($parser, $data)
    {
    	if ($GLOBALS['depth'] == 1 && $GLOBALS['currenttag'] == 'KEY')
        {
     	    $GLOBALS['currentcat'] = $data;
            $GLOBALS['listing'][$GLOBALS['currentcat']] = array();
    	}
        elseif ($GLOBALS['depth']==2 && $GLOBALS['currenttag']=='KEY')
        {
            $GLOBALS['currentregion'] = $data;
            $GLOBALS['listing'][$GLOBALS['currentcat']][$GLOBALS['currentregion']] = array();
    	}
        elseif ($GLOBALS['depth'] == 3)
        {
     	    if ($GLOBALS['currenttag']=='KEY' || $GLOBALS['currenttag']=='STRING')
            {
      	        if (isset($GLOBALS['items'][$GLOBALS['currenttag']]))
                {
       	            $GLOBALS['items'][$GLOBALS['currenttag']] .= $data;
        	}
                else
                {
     		    $GLOBALS['items'][$GLOBALS['currenttag']] = $data;
        	}
            }
    	}
    }
        $xml_parser = xml_parser_create('UTF-8');
// use case-folding so we are sure to find the tag in $map_array
        xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, true);
        xml_set_element_handler($xml_parser,"startElement","endElement");
        xml_set_character_data_handler($xml_parser,"characterData");
        if (!($fp = file_get_contents($file)))
        {
	    $tr = new Translate ;
	    $trans_die = $tr->trans('die');
            die($trans_die);
        }
        if (!xml_parse($xml_parser, $fp, true))
        {
            die(sprintf("XML error: %s at line %d.",
                        xml_error_string(xml_get_error_code($xml_parser)),
                        xml_get_current_line_number($xml_parser)));
        }
        xml_parser_free($xml_parser);
        if (!empty($GLOBALS['listing']))
        {
            $started = false;
	    $str = '';
	    foreach($GLOBALS['listing'] as $cat=>$arr)
            {
		if ($started) $str .= "\n";
	       	if ($cat != 'NULL')
                {
		    $str .= "\n<div><small><br /><b>".$cat."</b></small></div>\n";
        	}
	        $started2 = false;
	        foreach($arr as $reg=>$imcarr)
                {
	            if ($started2) $str .= "\n";
		    if ($cat == 'NULL' && $reg != 'NULL')
                    {
	                $str .= "\n<div><small><br /><b>".$reg."</b></small></div>\n";
        	    }
	            foreach($imcarr as $i=>$data)
                    {
		        $str .= '<div><small><a class="left" href="'.$data['STRING'].'">'.htmlspecialchars(mb_strtolower($data['KEY'], 'UTF-8'))."</a></small></div>";
		    }
        	    $started2 = true;
	        }
//                $str = str_replace('&', '&amp;', $str);
//                $str = str_replace('&amp;amp;', '&amp;', $str);
	    $started = true;
            }
	    $str .= "\n";
        }
        else
        {
	$tr = new Translate ;
        $str = $tr->trans('cities_unavailable') ; 
        }
//        $str=str_replace('>www.indymedia.org<','><b>imc network</b><',$str);
        $str=str_replace('>www.indymedia.org<','>indymedia.org<',$str);
        file_put_contents($pathtofile, $str);
        $this->tkeys['local_cities'] = file_get_contents($pathtofile);
    }
}
?>
