<?php

// Class for logedit_index page

class logedit_index extends Page {

    function logedit_viewer() {
        return 1;
    }
    
    function execute() {
    
    $testlist="";
    $htmllisttpl="";
    $htmllist="";
    $htmllistcss="";
    	$dir = opendir(INCLUDE_PATH."/test/");
	while($item = readdir($dir)){
          if (preg_match('/(.)+\\.(inc$|php$)/i', $item)) {
            $testlist .= "<a href=\"/admin/design/file_viewer.php?filename=".INCLUDE_PATH."/test/".$item."\">".$item."</a><br />";
          }
	}
	$dir = opendir(WEB_PATH."/test/");
	while($item = readdir($dir)){
	  if (preg_match('/(.)+\\.(inc$|php$|css$)/i', $item)) {
            $testlist .= "<a href=\"/admin/design/file_viewer.php?filename=".WEB_PATH."/test/".$item."\">".$item."</a><br />";
          }
	}
	$this->tkeys["test_list"]=$testlist;
	
	
	// first i get all css
	$dir = opendir(WEB_PATH."/themes/");
	while($item = readdir($dir)){
	  if (preg_match('/(.)+\\.css$/i', $item)) {
            $htmllistcss .= "<a href=\"file_viewer.php?filename=".WEB_PATH."/themes/".$item."\">".$item."</a><br />";
          }
	}
	$this->tkeys["css_list"]=$htmllistcss;
	$dir = opendir(TEMPLATE_PATH);
	while($item = readdir($dir)){
		if ($item!=".." && $item!="." && $item!="temp" && $item!="pages" && is_dir(TEMPLATE_PATH."/".$item)){
			$dir1=opendir(TEMPLATE_PATH."/".$item."/");
			while($item2 = readdir($dir1)){
				if (preg_match('/(.)+\\.tpl$/i', $item2)) {
                                  $htmllisttpl .= "<a href=\"file_viewer.php?filename=".TEMPLATE_PATH."/".$item."/".$item2."\">".$item."/".$item2."</a><br />";
                                }
			}
		}else{
			if (preg_match('/(.)+\\.tpl$/i', $item)) {
                          $htmllisttpl .= "<a href=\"file_viewer.php?filename=".TEMPLATE_PATH."/".$item."\">".$item."</a><br />";
                        }
		}
	}	
	$this->tkeys["template_list"]=$htmllisttpl;
	
	$dir = opendir(INCLUDE_PATH."/common/");
	while($item = readdir($dir)){
	  if (preg_match('/(.)+\\.(inc$|php$)/i', $item)) {
            $htmllist .= "<a href=\"file_viewer.php?filename=".$item."\">".$item."</a><br />";
          }
	}
	$this->tkeys["include_list"]=$htmllist;


    }
}
