<?php

// Class for admin_index page

require_once(CLASS_PATH."/cache/spam_cache_class.inc");

class spam_identification extends Page {

	function execute()
    {
        $spam_cache= new SpamCache();
        
        if (isset($_POST["save_ip_block_list"])){
        	$spam_cache->save_ip_block_file($_POST["ip_block_list"]);
        }
        $ip_block_list="";
        $ip_block_list=$spam_cache->load_ip_block_file();
        $this->tkeys['local_block_ips'] = $ip_block_list;
        
        
        if (isset($_POST["save_ip_spam_list"])){
        	$spam_cache->save_ip_spam_file($_POST["ip_spam_list"]);
        }
        $ip_spam_list="";
        $ip_spam_list=$spam_cache->load_ip_spam_file();
        $this->tkeys['local_spam_ips'] = $ip_spam_list;
        
        
        if (isset($_POST["save_title_spam_list"])){
        	$spam_cache->save_title_spam_file($_POST["title_spam_list"]);
        }
        $spam_title_list="";
        $spam_title_list=$spam_cache->load_title_spam_file();
        $this->tkeys['local_spam_title'] = $spam_title_list;
        
        
        if (isset($_POST["save_text_spam_list"])){
        	$spam_cache->save_text_spam_file($_POST["text_spam_list"]);
        }
        $spam_text_list="";
        $spam_text_list=$spam_cache->load_text_spam_file();
        $this->tkeys['local_spam_text'] = $spam_text_list;
        
        
        if (isset($_POST["save_validation_string_list"])){
        	$spam_cache->save_validation_string_file($_POST["validation_string_list"]);
        }
        $val_string_list="";
        $val_string_list=$spam_cache->load_validation_string_file();
        $this->tkeys['local_validation_strings'] = $val_string_list;

        return 1;

    }


}
?>
