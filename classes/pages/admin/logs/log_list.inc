<?php

// Class for admin_index page
include_once (CLASS_PATH.'/db/log_db_class.inc');
	
class log_list extends Page {

	function execute()
    {
        
        if (isset($_POST["clear_get_logs"])){
        	$log_db = new LogDB();
        	$log_db->clear_get_logs();
        }
        
        if (isset($_POST["clear_all_logs"])){
        	$log_db = new LogDB();
        	$log_db->clear_all_logs();
        }
        
        if (isset($_POST["clear_ips"])){
        	$log_db = new LogDB();
        	$log_db->clear_ips();
        }
        
        if (!isset($_POST["log_get"]))
        	$_POST["log_get"]=0;
        if (!isset($_POST["log_ip_get"]))
        	$_POST["log_ip_get"]=0;
        if (!isset($_POST["log_search"]))
        	$_POST["log_search"]=0;
        if (!isset($_POST["log_ip_search"]))
        	$_POST["log_ip_search"]=0;
        if (!isset($_POST["log_post"]))
        	$_POST["log_post"]=0;
        if (!isset($_POST["log_ip_post"]))
        	$_POST["log_ip_post"]=0;
        if (!isset($_POST["log_db"]))
        	$_POST["log_db"]=0;
        if (!isset($_POST["log_ip_db"]))
        	$_POST["log_ip_db"]=0;

        	
        $log_cache = new LogCache();
        if (isset($_POST["save_log_status_changes"])){
        	$log_cache->change_log_settings(
        		$_POST["log_get"], $_POST["log_ip_get"],
        		$_POST["log_search"], $_POST["log_ip_search"],
        		$_POST["log_post"], $_POST["log_ip_post"],
        		$_POST["log_db"], $_POST["log_ip_db"]);
        }
        
        if (isset($_GET['page_number']))
    		$page_number = $_GET['page_number']+0;
    	else
    		$page_number=0;
        
        $log_list=$this->renderLogs($page_number,10);

        $this->tkeys['local_log_list'] = $log_list;
        $this->tkeys['local_nav'] = $this->format_paging($this->tkeys['local_page_number']);
       
		$log_cache_array=$log_cache->load_cached_logs_into_array();
		
		if ($log_cache_array[0]==1)
        	$this->tkeys['local_get_checked'] ="checked";
        else
        	$this->tkeys['local_get_checked'] ="";
        	
        if ($log_cache_array[1]==1)
        	$this->tkeys['local_getip_checked'] ="checked";
        else
        	$this->tkeys['local_getip_checked'] ="";
        
        if ($log_cache_array[6]==1)
        	$this->tkeys['local_search_checked'] ="checked";
        else
        	$this->tkeys['local_search_checked'] ="";
        	
        if ($log_cache_array[7]==1)
        	$this->tkeys['local_searchip_checked'] ="checked";
        else
        	$this->tkeys['local_searchip_checked'] ="";
        	
        if ($log_cache_array[2]==1)
       		$this->tkeys['local_post_checked'] ="checked";
        else
        	$this->tkeys['local_post_checked'] ="";
        
        if ($log_cache_array[3]==1)
        	$this->tkeys['local_postip_checked'] ="checked";
        else
        	$this->tkeys['local_postip_checked'] ="";
        
        if ($log_cache_array[4]==1)
        	$this->tkeys['local_db_checked'] ="checked";
        else
        	$this->tkeys['local_db_checked'] ="";
        
        if ($log_cache_array[5]==1)
        	$this->tkeys['local_dbip_checked'] ="checked";
        else
        	$this->tkeys['local_dbip_checked'] ="";
        
        return 1;

    }


	function renderLogs($page_number, $page_size){
		$tr = new Translate("");
        $log_db = new LogDB();
        
        if (isset($_GET['sort_by']))
        	$sort_by=$_GET['sort_by'];
        else
        	$sort_by="";
        	
        if (isset($_GET['ip']))
        	$ip=$_GET['ip'];
        else
        	$ip="";
        	
        if (isset($_GET['ip1']))
        	$ip_part1=$_GET['ip1'];
        else
        	$ip_part1="";
        
        if (isset($_GET['ip1']))
        	$ip_part2=$_GET['ip2'];
        else
        	$ip_part2="";
        	
 		if (isset($_GET['ip3']))
        	$ip_part3=$_GET['ip3'];
        else
        	$ip_part3="";
        
        if (isset($_GET['method']))
        	$method_type=$_GET['method'];
        else
        	$method_type="";
        
        if (isset($_GET['url']))
        	$url=$_GET['url'];
        else
        	$url="";
        
        if (isset($_GET['ref_url']))
        	$ref_url=$_GET['ref_url'];
        else
        	$ref_url="";
        	
		if (isset($_GET['news_item_id']))
        	$news_item_id=$_GET['news_item_id'];
        else
        	$news_item_id="";
        	
        if (isset($_GET['parent_news_item_id']))
        	$parent_news_item_id=$_GET['parent_news_item_id'];
        else
        	$parent_news_item_id="";
        	
		if (isset($_GET['keyword']))
        	$keyword=$_GET['keyword'];
        else
        	$keyword="";

        $this->tkeys['last_page']=$log_db->get_log_list_max_page($page_size, $ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $ref_url, $news_item_id, $parent_news_item_id, $keyword);

		$this->tkeys['local_page_number']=$page_number;
		
        $log_list = $log_db->get_log_list($page_number+1, $page_size, $ip, $ip_part1, $ip_part2, $ip_part3,
        $method_type, $url, $ref_url, $news_item_id, $parent_news_item_id, $keyword, $sort_by);


		
		$i=0;
		$tblhtml="";
        foreach ($log_list as $nextlogentry)
        {
        	
        	$i=$i+1;
        	$tblhtml .= "<tr ";
        	if (!is_int($i/2)){
        		$tblhtml.="class=\"bgsearchgrey\"";
        	}
            $tblhtml .= " ><td valign=\"top\">";
            $nextlogentry['ip']=trim($nextlogentry['ip']);
            $lastipchar = $nextlogentry['ip'][strlen($nextlogentry['ip'])-1]; 
            if ($lastipchar=="."){
            	$nextlogentry['ip']=$nextlogentry['ip']."0";
            }
            $tblhtml .= $nextlogentry['ip'];
            
            if ($nextlogentry['ip']!=""){
            	$tblhtml .="<br />(".@gethostbyaddr($nextlogentry['ip']).")";
            	$tblhtml .="<br /><small><a href=\"/admin/logs/?ip=".$nextlogentry['ip']."\">see all recent access</a></small>";
            	$tblhtml .="<br /><small><a href=\"http://www.dnsstuff.com/tools/city.ch?ip=".$nextlogentry['ip']."\">Lookup Location</a></small>";
            }
          	$tblhtml .= "</td>";
          	if ($nextlogentry['time_diff']=="")
          		$nextlogentry['time_diff']="running";
			$tblhtml .= "<td valign=\"top\">". $nextlogentry['start_time']." (". $nextlogentry['time_diff'].")</td>";
			$tblhtml .= "<td valign=\"top\">". $nextlogentry['http_method']."</td>";
			$tblhtml .= "<td valign=\"top\"><small><a href=\"".$nextlogentry['url']."\">".  wordwrap ($nextlogentry['url'],40,"<br />", true)."</a></small></td>";
			$tblhtml .= "<td valign=\"top\"><small><a href=\"".$nextlogentry['referring_url']."\">".  wordwrap ($nextlogentry['referring_url'],40,"<br />", true)."</a></small></td>";
			$tblhtml .= "<td valign=\"top\">". $nextlogentry['parent_news_item_id']."</td>";
			if ($nextlogentry['news_item_id']+0!=0)
				$tblhtml .= "<td valign=\"top\"><a href=\"/admin/article/article_edit.php?id=".$nextlogentry['news_item_id']."\">". $nextlogentry['news_item_id']."</a></td>";
			else
			$tblhtml .= "<td valign=\"top\">&nbsp;</td>";
			$tblhtml .= "<td valign=\"top\"><small>".  wordwrap ($nextlogentry['full_headers'],40,"<br />", true)."</small></td>";
            $tblhtml .= "</Tr>";
        }
        
        
        $this->tkeys['local_ip'] = $ip;
        $this->tkeys['local_ip1'] = $ip_part1;
        $this->tkeys['local_ip2'] = $ip_part2;
        $this->tkeys['local_ip3'] = $ip_part3;
        $this->tkeys['local_url'] = $url;

        $this->tkeys['local_ref_url'] = $ref_url;
        $this->tkeys['local_news_item_id'] = $news_item_id;
        $this->tkeys['local_parent_news_item_id'] = $parent_news_item_id;
        $this->tkeys['local_keyword'] = $keyword;

		$options=array();
		$options["GET"]="GET";
		$options["POST"]="POST (nonDB save)";
		$options["SEARCH"]="SEARCH";
		$options["DB"]="POST (DB save)";
		$renderer= new Renderer();
        $this->tkeys['local_method_select'] = $renderer->make_select_form ("method", $options, $method_type, $default="ALL");
        
        $sort_by_options=array();
        $sort_by_options["DURATION"]="DURATION";
        $this->tkeys['local_sort_by_select'] = $renderer->make_select_form ("sort_by", $sort_by_options, $sort_by, $default="DATE");
        
        return $tblhtml;
	}
	
	
function format_paging($page_number){
		$this->tkeys['nav']="<p>";
    	
    	if ($page_number>0) $this->tkeys['nav'].="<a href=\"".$this->generate_paging_url(0)."\" rel=\"nofollow\"><img src=\"/im/first_arrow.jpg\" /></a>&nbsp;&nbsp;";
	if ($page_number>0) $this->tkeys['nav'].="<a href=\"".$this->generate_paging_url($page_number-1)."\" rel=\"nofollow\"><img src=\"/im/prev_arrow.gif\" /></a>&nbsp;&nbsp;";
    	$displaypage=$page_number+1;
    	$this->tkeys['nav'].="$displaypage of ".$this->tkeys['last_page']." ";
    	if ($this->tkeys['last_page'] > $page_number+1){
         	$this->tkeys['nav'].="&nbsp;&nbsp;<a href=\"".$this->generate_paging_url($page_number+1)."\" rel=\"nofollow\"><img src=\"/im/next_arrow.gif\" /></a>";	
         	$this->tkeys['nav'].="&nbsp;&nbsp;<a href=\"".$this->generate_paging_url($this->tkeys['last_page']-1)."\" rel=\"nofollow\"><img src=\"/im/last_arrow.jpg\" /></a>";
	}
    	$this->tkeys['nav'].="</p>";

    	if (isset($GLOBALS['db_down'])){
    		$this->tkeys['nav']="";
    	}
}

function generate_paging_url($page_number){
	$url = $_SERVER['SCRIPT_NAME'] . '?page_number=' . $page_number;
	foreach ($_GET as $key=>$value){
		if ($key!="page_number")
			$url=$url."&".$key."=".$value;
	}
	return $url;
}
}
?>
