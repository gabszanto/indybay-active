<?php

// Class for admin_index page

require_once(CLASS_PATH."/cache/spam_cache_class.inc");

class spam_blocker extends Page {

	function execute()
    {
        
        if (isset($_POST["ip"]))
			$ip=$_POST["ip"];
		else
			$ip="";
		if (isset($_POST["ip_part1"]))
			$ip_part1=$_POST["ip_part1"];
		else
			$ip_part1="";
		if (isset($_POST["ip_part1"]))
			$ip_part2=$_POST["ip_part2"];
		else
			$ip_part2="";
		if (isset($_POST["ip_part1"]))
			$ip_part3=$_POST["ip_part3"];
		else
			$ip_part3="";
		if (isset($_POST["http_method"]))
			$http_method=$_POST["http_method"];
		else
			$http_method="";
		if (isset($_POST["url"]))
			$url=$_POST["url"];
		else
			$url="";
		if (isset($_POST["referring_url"]))
			$referring_url=$_POST["referring_url"];
		else
			$referring_url="";
		if (isset($_POST["keyword"]))
			$keyword=$_POST["keyword"];
		else
			$keyword="";
		if (isset($_POST["note"]))
			$note=$_POST["note"];
		else
			$note="";
		if (isset($_POST["parent_news_item_id"]))
			$parent_news_item_id=$_POST["parent_news_item_id"];
		else
			$parent_news_item_id="";
		if (isset($_POST["redirect_url"]))
			$destination=$_POST["redirect_url"];
		else
			$destination="";
		
		$legacy_spam_cache = new LegacySpamCache();

        if (isset($_POST["add_block"])){
        	$legacy_spam_cache->add_entry_to_spam_cache($ip, $ip_part1, $ip_part2, $ip_part3,
   			$url, $referring_url, $http_method, $parent_news_item_id, $keyword, $destination, $note);
        }
        if (isset($_POST["remove_block_id"])){
        	$legacy_spam_cache->remove_entry_from_spam_cache($_POST["remove_block_id"]);
        }
        
		$this->tkeys['local_ip'] = $ip;
        $this->tkeys['local_ip1'] = $ip_part1;
        $this->tkeys['local_ip2'] = $ip_part2;
        $this->tkeys['local_ip3'] = $ip_part3;
        $this->tkeys['local_url'] = $url;

        $this->tkeys['local_referring_url'] = $referring_url;
        //$this->tkeys['local_news_item_id'] = $news_item_id;
        $this->tkeys['local_parent_news_item_id'] = $parent_news_item_id;
        $this->tkeys['local_keyword'] = $keyword;
        $this->tkeys['local_redirect_url'] = $destination;
        $this->tkeys['local_note'] = $note;

		$options=array();
		$options["GET"]="GET";
		$options["POST"]="POST (nonDB save)";
		$options["DB"]="POST (DB save)";
		$renderer= new Renderer();
        $this->tkeys['local_method_select'] = $renderer->make_select_form ("http_method", $options, $http_method, $default="ALL");
        
        
        $block_list=$this->renderBlocks();

        $this->tkeys['local_block_list'] = $block_list;

        return 1;

    }


	function renderBlocks(){
		$tr = new Translate("");
        $legacy_spam_cache = new LegacySpamCache();
        $block_list = $legacy_spam_cache->list_blocks();

		
		$i=0;
		$tblhtml="";
        foreach ($block_list as $nextblockentry)
        {
        	
        	$i=$i+1;
        	$tblhtml .= "<tr ";
        	if (!is_int($i/2)){
        		$tblhtml.="class=\"bgsearchgrey\"";
        	}
            $tblhtml .= " ><td valign=\"top\">";
            $tblhtml .="<form method=\"POST\"><input type=\"Submit\" value=\"remove block\"><input type=\"hidden\" name=\"remove_block_id\" value=\"".$nextblockentry["id"];
            
            $tblhtml .= "\"></form></td><td valign=\"top\">";
            $tblhtml .= $nextblockentry["ip"];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['ip_part1'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['ip_part2'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['ip_part3'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
          	if ($nextblockentry['http_method']=="" || $nextblockentry['http_method']=="0")
          		$tblhtml .= "ALL";
          	else
            	$tblhtml .= $nextblockentry['http_method'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['url'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['referring_url'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['parent_news_item_id'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['keyword'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['destination'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['note'];
          	$tblhtml .= "</td>";
          	$tblhtml .= "<td valign=\"top\">";
            $tblhtml .= $nextblockentry['added_info'];
          	$tblhtml .= "</td>";
            $tblhtml .= "</Tr>";
        }
        
        return $tblhtml;
	}

	
}
?>
