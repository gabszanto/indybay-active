<?php
require_once(CLASS_PATH."/pages/admin/article/article_edit.inc");
require_once(CLASS_PATH."/renderer/renderer_class.inc");
require_once(CLASS_PATH."/cache/event_cache_class.inc");
    
//for now just inherits but also needs to deal with display date issues
class  event_edit extends article_edit {

	function process_additional_db_dependencies($old_article_info, $new_article_info){

	}
    
    
    
    function validate(){
		$ret=1;
		if (trim($_POST["title1"])==""){
			$this->add_validation_message("Title Is Required.");
			$ret= 0;
		}
		return $ret;
	}
	
}

?>
