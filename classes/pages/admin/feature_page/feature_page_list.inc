<?php

require_once("db/feature_page_db_class.inc");
require_once("renderer/feature_page_renderer_class.inc");
require_once("cache/feature_page_cache_class.inc");
// Class for category_display_list page

class feature_page_list extends Page
{



    function execute()
    {

        $other_tblhtml=$this->renderFeaturePageList(3,0);
        $region_tblhtml=$this->renderFeaturePageList(1,0);
        $topic_tblhtml=$this->renderFeaturePageList(2,0);
        $int_tblhtml=$this->renderFeaturePageList(2,44);

        $this->tkeys['other_featurepage_tablerows'] = $other_tblhtml;
        $this->tkeys['region_featurepage_tablerows'] = $region_tblhtml;
        $this->tkeys['topic_featurepage_tablerows'] = $topic_tblhtml;
        $this->tkeys['international_featurepage_tablerows'] = $int_tblhtml;
        
        if (isset($GLOBALS['db_down']) && $GLOBALS['db_down']==1){
        	$this->add_status_message("The database running this site is busy due to a large number of people looking at the site. Try searching again in a few minutes");
        	$this->add_status_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
        }
        
        return 1;

    }

	function renderFeaturePageList($category_type_id, $parent_category_id){
			$tr = new Translate("");
	        $feature_page_db_class = new FeaturePageDB();
			$feature_page_cache = new FeaturePageCache();
			$feature_page_renderer_class = new FeaturePageRenderer();
	        $featurepage_list = $feature_page_db_class->get_page_list($category_type_id,$parent_category_id);
	
			$i=0;
			$tblhtml="";
			if (is_array($featurepage_list)){
	        foreach ($featurepage_list as $nextfeaturepage)
	        {
	        	 if (isset($_GET["force_full_newswire_regeneration"]) ){
	        		$feature_page_cache->cache_newswire_template_for_page($nextfeaturepage);
	        	}
	            if (isset($_GET["force_all_pages_live"]) || (isset($_GET["force_all_nonfp_pages_live"]) && $nextfeaturepage["page_id"]!=FRONT_PAGE_CATEGORY_ID)){
	        		$blurb_list = $feature_page_db_class->get_current_blurb_list($nextfeaturepage['page_id']);
					$html_for_page=$feature_page_renderer_class->render_page($blurb_list,$nextfeaturepage );
					$feature_page_cache_class= new FeaturePageCache;
					$feature_page_cache_class->cache_center_column($nextfeaturepage, $html_for_page);
					$feature_page_cache_class->cache_all_blurbs_on_page($nextfeaturepage['page_id']);
					$feature_page_db_class->update_page_pushed_live_date($nextfeaturepage['page_id']);
	        	}
	        	$blurb_count=$feature_page_db_class->get_blurb_count($nextfeaturepage['page_id']);
	        	$not_pushed_blurb_count=$feature_page_db_class->get_nonpushed_blurb_count($nextfeaturepage['page_id']);
	        	$i=$i+1;
	        	$tblhtml .= "<tr ";
	        	if (!is_int($i/2)){
	        		$tblhtml.="class=\"bgsearchgrey\"";
	        	}
	            $tblhtml .= " ><td>";
	            $tblhtml .= $nextfeaturepage['long_display_name'];
	          	$tblhtml .= "</td><td>";
	          	$tblhtml .= "<a ";
	    		if ($not_pushed_blurb_count>0){
	          		$tblhtml .= " style=\"color: #cc0000; font-weight: bold\" ";
	          	}
	          	$tblhtml .= " href=\"/admin/feature_page/feature_page_blurb_list.php?page_id=";
	         	$tblhtml .= $nextfeaturepage['page_id'] . "\">" . $tr->trans('view_blurb_list') . "</a>";
				$tblhtml .= "&nbsp;&nbsp;&nbsp;</td>";
				$tblhtml .= "<td>".$blurb_count."</td>";
				$tblhtml .= "<td>".$not_pushed_blurb_count."</td>";
				$tblhtml .= "<td><a href=\"/admin/feature_page/feature_page_edit.php?page_id=";
	          	$tblhtml .= $nextfeaturepage['page_id'] . "\">";
		    	$tblhtml .= $tr->trans('edit_page_info')."</a></td>";
	            $tblhtml .= "</Tr>";
	        }
	        }
	        return $tblhtml;
	}
}
?>
