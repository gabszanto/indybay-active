<?php
require_once('blurb_edit.inc');

// Class for article_edit page

class blurb_recategorize extends blurb_edit {

    function execute() {
    
 	   $news_item_db_class= new NewsItemDB;
 	   $news_item_version_db_class= new NewsItemVersionDB;
 	   $blurb_renderer_class= new BlurbRenderer;
       $tr = new Translate();
       
       $tr->create_translate_table('article_edit');

       $this->tkeys['publish_result'] = "";
  
  		$blurb_db_class= new BlurbDB;
  		$blurb_renderer_class = new BlurbRenderer;
       if (isset($_GET['version_id'])) {
       		$news_item_version_id = $_GET['version_id'];
       		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
       		$news_item_id=$version_info[news_item_id];
       }
       if (isset($_GET['id'])) {
       		$news_item_id=$_GET['id'];

       		$news_item_version_id = $news_item_db_class->get_current_version_id($news_item_id);
       		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
       }

       if (isset($_GET['add_current'])) {
       		$blurb_db_class->add_blurb_to_categories_and_page($news_item_id, $_GET['add_page_id']);
       }
       		
       if (isset($_GET['add_archived'])) {
       			$news_item_db_class->add_news_item_category($news_item_id, $_GET['add_page_id']);
       }
       if (isset($_GET['remove_association'])) {
       			$blurb_db_class->remove_blurb_from_category($news_item_id, $_GET['remove_page_id']);
       }
        
        $this->tkeys['local_news_item_id'] =       $version_info['news_item_id'];
        
        $num_cur_pages= $this->setup_admin_page_lists_ret_current_num($news_item_id);
        
        
        //the common function above gets page info for display but
        //we need to get it again for the dropdowns
        //this could be in the common function but its something
        //likely only needed by this page
        $page_info_list=$blurb_db_class->get_current_page_info_for_blurb($news_item_id);
		$already_on_pages=array();
        foreach($page_info_list as $page_info){
        	$already_on_pages[$page_info['page_id']]=$page_info['long_display_name'];
        }
        $page_info_list=$blurb_db_class->get_archived_page_info_for_blurb($news_item_id);
        foreach($page_info_list as $page_info){
        	$already_on_pages[$page_info['page_id']]=$page_info['long_display_name'];
        }
        $page_info_list=$blurb_db_class->get_hidden_page_info_for_blurb($news_item_id);
        foreach($page_info_list as $page_info){
        	$already_on_pages[$page_info['page_id']]=$page_info['long_display_name'];
        }
        $feature_page_db_class=new FeaturePageDB;
        $page_options=$feature_page_db_class->get_all_page_select_list();
        $page_options_not_already_selected=array();
        foreach($page_options as $page_id=>$page_name){
        	if (!isset($already_on_pages[$page_id])){
        		$page_options_not_already_selected[$page_id]=$page_name;
        	}
        	
        }
        $this->tkeys['local_select_nonassociated_pages'] =$blurb_renderer_class->make_select_form("add_page_id", $page_options_not_already_selected, 0);        
        
        $this->tkeys['local_select_associated_pages'] =$blurb_renderer_class->make_select_form("remove_page_id", $already_on_pages, 0);        
                
         
        return 1;
    }

	function validate(){
		return 1;
	}
	
	function add_new_version($post_array){
		$blurb_db_class= new BlurbDB;
		$news_item_id=$post_array['news_item_id'];
		$news_item_version_id=$blurb_db_class->create_new_version($news_item_id, $post_array);
		$this->cache_blurb($news_item_id,$oldcats);
		$version_info = $blurb_db_class->get_blurb_info($news_item_version_id);
	
		return $version_info ;
	
	}

}

?>
