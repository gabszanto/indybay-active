<?php

// Class for feature_display_list page

require_once("db/blurb_db_class.inc");
require_once("db/feature_page_db_class.inc");

class feature_page_hidden_blurb_list extends Page
{

    function execute()
    {

		if (isset($_GET['page_id']))
			$page_id=$_GET['page_id'];
		else
			$page_id=0;
			
		if (isset($_GET['page_number']))	
			$page_number=$_GET['page_number']+0;
        else
        	$page_number=0;
        	
        $tblhtml = "";
        $tr = new Translate();
        if (isset($_GET['make_blurb_hidden'])){
			$news_item_id=$_GET['make_blurb_hidden'];
			if ($news_item_id>0){
				$blurb_db_class= new BlurbDB;
				$blurb_db_class->update_news_item_status_id($news_item_id,NEWS_ITEM_STATUS_ID_HIDDEN);
				$blurb_db_class->remove_blurb_from_page($news_item_id, $page_id);
			}
		}

        $this->tkeys['local_subtitle'] = $tr->trans('features_edit');

		$feature_page_db_class = new FeaturePageDB;
        $feature_page_info = $feature_page_db_class->get_feature_page_info($page_id);
        $feature_page_long_name = $feature_page_info['long_display_name'];
        $this->tkeys['local_feature_page_name']=$feature_page_long_name;

		$page_size=20;
		$start_limit=($page_number)*$page_size;
        $blurb_list = $feature_page_db_class->get_hidden_blurb_list_limited_info($page_id,$start_limit,$page_size+1);

        $this->tkeys['local_current_link'] = "<a href=\"feature_page_blurb_list.php?page_id=$page_id\">";
        $this->tkeys['local_current_link'] .= $tr->trans('current');
        $this->tkeys['local_current_link'] .= "</a></td>";

        $this->tkeys['local_archived_link'] = "<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id\">";
        $this->tkeys['local_archived_link'] .= $tr->trans('archived');
        $this->tkeys['local_archived_link'] .= "</a></td>";
        $this->tkeys['local_hidden_link'] = $tr->trans('hidden');


        if (!is_array($blurb_list) || count($blurb_list) < 1)
        {
            $tblhtml = "<tr><td colspan=\"6\"><center>";
            $tblhtml .= $tr->trans('no_features_to_edit');
            $tblhtml .= "</center></td></tr>";
            $this->tkeys['nav']="";

        } else{
        	$i=0;
        	
        $this->tkeys['nav']="<p>";
    	if ($page_number>0){
    		 $this->tkeys['nav'].="<a href=\"/admin/feature_page/feature_page_archived_blurb_list.php?page_id=".
        		$page_id."&amp;page_number=".($page_number-1).
        		"\"><img src=\"/im/prev_arrow.gif\" border=0/></a>&nbsp;&nbsp;";
        	
    	}
    	$displaypage=$page_number+1;
    	$this->tkeys['nav'].=$page_number+1;
    	if (sizeof($blurb_list)>$page_size){
	        $this->tkeys['nav'].="&nbsp;&nbsp;
	        	<a href=\"/admin/feature_page/feature_page_archived_blurb_list.php?page_id=".
	        		$page_id."&amp;page_number=".($page_number+1).
	        		"\"><img src=\"/im/next_arrow.gif\" border=0/></a>";
        }
    	$this->tkeys['nav'].="</p>";
    	
    	$blurb_list=array_reverse($blurb_list);
          while ($next_blurb = array_pop($blurb_list ))
            {
                $tblhtml .= "<tr ";
                $i=$i+1;
                if (!is_int($i/2))
        			$tblhtml.="class=\"bgsearchgrey\"";
                
                $tblhtml .="><td>";
                $tblhtml .= "<a href=\"blurb_edit.php?id=";
                $tblhtml .= $next_blurb['news_item_id'];
                $tblhtml .= "\"><small>";
                $tblhtml .= $next_blurb['news_item_id']."</small></td><td>";            
                $tblhtml .= "<a href=\"blurb_edit.php?id=";
                $tblhtml .= $next_blurb['news_item_id'];
                $tblhtml .= "\">";
                $tblhtml .= $next_blurb['title2'];
                $tblhtml .= "</a></td><td>";
                $tblhtml .= $next_blurb['created'];
                $tblhtml .= "</td><td>";
                $tblhtml .= $next_blurb['modified'];
                $tblhtml .= "</td>";
                $tblhtml .= "<td>&nbsp;<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id&amp;make_blurb_unhidden=";
                $tblhtml .= $next_blurb['news_item_id'];
                $tblhtml .= "\">" . $tr->trans('action_archive') . "</a></td>";
                $tblhtml .= "<td>&nbsp;<a href=\"feature_page_blurb_list.php?page_id=$page_id&amp;make_blurb_current=";
                $tblhtml .= $next_blurb['news_item_id'];
                $tblhtml .= "&amp;make_blurb_unhidden=";
                $tblhtml .= $next_blurb['news_item_id'];
                $tblhtml .= "\">" . $tr->trans('action_display') . "</a></td></tr>";
                if  ($i==$page_size) break;
            }

        }

        $this->tkeys['local_table_rows'] = $tblhtml;
	
        return 1;

    }

}

?>
