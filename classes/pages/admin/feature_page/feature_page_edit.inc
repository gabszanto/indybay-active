<?php

require_once("renderer/feature_page_renderer_class.inc");
require_once("cache/feature_page_cache_class.inc");
require_once("db/feature_page_db_class.inc");

class feature_page_edit extends Page
{

    function execute()
    {

        $tr = new Translate;

        if (strlen($_GET['page_id']) > 0 || strlen($_POST['page_id']) > 0)
        {
            $is_edit = true;
            $this->tkeys['subtitle'] = $tr->trans('feature_page_edit');
            $this->tkeys['local_form'] = "feature_page_update.php";
            if (strlen($_POST['page_id']) > 0) $_GET['page_id'] = $_POST['page_id'];
        } else
        {
            $is_edit  = false;
            $this->tkeys['local_form'] = "feature_page_add.php";
            $this->tkeys['subtitle'] = $tr->trans('feature_page_add');
        }
        
		$feature_page_db_class = new FeaturePageDB;
		$feature_page_renderer_class = new FeaturePageRenderer;
		
		if (isset($_POST['save'])){
			$feature_page_db_class->update_feature_page_info($_POST);
			$page_info=$feature_page_db_class->get_feature_page_info($_POST['page_id']);
			$feature_page_cache_class= new FeaturePageCache;
			$feature_page_cache_class->cache_newswire_template_for_page($page_info);
		}
        if ($is_edit == true)
        {

            
            $feature_page = $feature_page_db_class->get_feature_page_info($_GET['page_id']);
  
            $this->tkeys['local_short_display_name'] = $feature_page['short_display_name'];
            $this->tkeys['local_page_id'] = $feature_page['page_id'];
            $this->tkeys['local_long_display_name'] = $feature_page['long_display_name'];
            $this->tkeys['local_items_per_newswire_section'] = $feature_page['items_per_newswire_section'];
            $this->tkeys['local_newswire'] = $feature_page['newswire'];
            $this->tkeys['local_center'] = $feature_page['center'];
            $this->tkeys['local_relative_path'] = $feature_page['relative_path'];
            $this->tkeys['local_include_in_readmore_links'] = "";
            if ($feature_page['include_in_readmore_links']==1){
            	 $this->tkeys['local_include_in_readmore_links']=" checked ";
            }
            $this->tkeys['LOCAL_MAX_EVENTS_IN_HIGHLIGHTED_LIST'] = $feature_page['max_events_in_highlighted_list'];
            $this->tkeys['LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_HIGHLIGHTED_EVENT_LIST'] = $feature_page['max_timespan_in_days_for_highlighted_event_list'];
            $this->tkeys['LOCAL_MAX_EVENTS_IN_FULL_EVENT_LIST'] = $feature_page['max_events_in_full_event_list'];
            $this->tkeys['LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_FULL_EVENT_LIST'] = $feature_page['max_timespan_in_days_for_full_event_list'];
            
        }

		$cache_class= new Cache;
				
		$center_column_select_list = $feature_page_renderer_class->make_select_form(
				"center_column_type_id",
				$feature_page_db_class->get_center_column_type_list(),
				$feature_page['center_column_type_id']);
				
		$news_select_list = $feature_page_renderer_class->make_select_form("newswire_type_id",
				$feature_page_db_class->get_newswire_type_list(),
				$feature_page['newswire_type_id']);
				
		$eventlist_select_list = $feature_page_renderer_class->make_select_form("event_list_type_id",
				$feature_page_db_class->get_event_list_type_list(),
				$feature_page['event_list_type_id']);
        
        $this->tkeys['local_select_newswire'] =$news_select_list ;
        $this->tkeys['local_select_centercolumn'] = $center_column_select_list;
        $this->tkeys['local_select_eventlist'] = $eventlist_select_list;
        
        return 1;
    }
}

?>
