<?php

require_once("renderer/feature_page_renderer_class.inc");
require_once("db/feature_page_db_class.inc");
require_once("cache/feature_page_cache_class.inc");

class feature_page_preview extends Page
{

    function execute()
    {
		$feature_page_db_class= new FeaturePageDB;
		$feature_page_renderer_class= new FeaturePageRenderer;
		$page_id=$_GET['page_id'];
        $tblhtml = "";
        $tr = new Translate();
        
        $page_info=$feature_page_db_class->get_feature_page_info($page_id);
        $this->tkeys['local_feature_page_name']=$page_info['long_display_name'];
        $this->tkeys['local_page_id']=$page_id;
        $blurb_list = $feature_page_db_class->get_current_blurb_list($page_id);
		$html_for_page=$feature_page_renderer_class->render_page($blurb_list,$page_info );
		if (isset($_GET["push_page_live"])){
			$feature_page_cache_class= new FeaturePageCache;
			$feature_page_cache_class->cache_center_column($page_info, $html_for_page);
			$feature_page_cache_class->cache_all_blurbs_on_page($page_id);
			$feature_page_db_class->update_page_pushed_live_date($page_id);
			$tr = new Translate;
			$this->add_status_message($tr->trans('page_push_success')."<br /><br />");
		}
		if (strpos($html_for_page,"INDYBAY_HIGHLIGHTED_EVENTS")>0){
				$event_list_cache_class= new EventListCache();
				$cached_events=$event_list_cache_class->load_cached_highlighted_events_cache_for_page($page_id);
				$html_for_page=str_replace("INDYBAY_HIGHLIGHTED_EVENTS",$cached_events, $html_for_page);
		}
		$this->tkeys['local_preview_feature_page'] = $html_for_page;
	
        return 1;

    }

}

?>
