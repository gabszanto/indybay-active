<?php

// Class for publish page
require_once('renderer/blurb_renderer_class.inc');
require_once('db/blurb_db_class.inc');
//require_once("/pages/article/publish.inc");
include_once("db/newswire_db_class.inc");
include_once("db/category_db_class.inc");
include_once("db/media_attachment_db_class.inc");

class blurb_add extends Page
{

    function execute()
    {
    
    	$blurb_renderer_class = new BlurbRenderer;
    	$category_db_class = new CategoryDB;
    	$media_attachment_db_class= new MediaAttachmentDB;
        $comment = false;
        $counter_msg = "";

        $db_obj = new DB();
        $this->tr = new Translate();
        $this->tr->create_translate_table('publish');

		$category0=0;
		$category1=0;
		$category2=0;
		if (isset($_POST['category'])){
        	$category0=$_POST['category'][0]+0;
        	$category1=$_POST['category'][1]+0;
        	$category2=$_POST['category'][2]+0;
     	}
     	
     	if ($category0+0==0 && isset($_GET["preselect_page_id"])){
     		$category0=$_GET["preselect_page_id"]+0;
     	}

     	if ($category1+0==0 && isset($_GET["preselect_page_id"]) ){
     		$category1=$_GET[preselect_page_id]+0;
     	}

     	if ($category2+0==0 && isset($_GET["preselect_page_id"]) ){
     		$category2=$_GET[preselect_page_id]+0;
     	}

     	
        
       
		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_topic_options,$cat_international_options);
     	$cat_topic_options[0]="Please Select";
     	if (sizeof($cat_topic_options[$category0])==0)
     		$category0=0;
     	$this->tkeys['cat_topic_select'] = $blurb_renderer_class->make_select_form("category[]", $cat_topic_options, $category0);        
		$cat_region_options=$category_db_class->get_category_info_list_by_type(1,0);
		$cat_region_options[0]="Please Select";
		if (sizeof($cat_region_options[$category1])==0)
     		$category1=0;
     	$this->tkeys['cat_region_select'] = $blurb_renderer_class->make_select_form("category[]", $cat_region_options, $category1);        
		$cat_other_options=$category_db_class->get_category_info_list_by_type(3,0);
     	$cat_other_options[0]="None Selected";
     	if (sizeof($cat_other_options[$category2])==0)
     		$category2=0;
     	$this->tkeys['cat_other_select'] = $blurb_renderer_class->make_select_form("category[]", $cat_other_options, $category2);        
        
        
        
        
        
        $form_vars = array("title1","title2", "summary","related_url","text");

        // Initialize template variables
        foreach ($form_vars as $fv)
        {
            if (isset($_POST[$fv]))
            {
                $this->tkeys['local_'.$fv] = $_POST[$fv];
            } else
            {
                $this->tkeys['local_'.$fv] = '';
            }
        }
        
        if (isset($_POST["publish"])){
	        $this->tkeys['local_checkbox_is_text_html'] = $blurb_renderer_class->make_boolean_checkbox_form("is_text_html", $_POST["is_text_html"]);
	        $this->tkeys['local_checkbox_is_summary_html'] = $blurb_renderer_class->make_boolean_checkbox_form("is_summary_html", $_POST["is_summary_html"]);
        }else{
        	$this->tkeys['local_checkbox_is_text_html'] = $blurb_renderer_class->make_boolean_checkbox_form("is_text_html", true);
	        $this->tkeys['local_checkbox_is_summary_html'] = $blurb_renderer_class->make_boolean_checkbox_form("is_summary_html", true);
        	if ($this->tkeys['local_summary']==""){
        		$this->tkeys['local_summary']="On <strong>[put date here]</strong>, ....\n";;
        	}
        	if ($this->tkeys['local_text']==""){
        		$this->tkeys['local_text']="<!-- This is just a template for a new long version of a blurb. You likely wont want or need all the stuff in this template, so delete what you dont need -->\n\n".
        		"On <strong>[put date here]</strong>, ....\n<br /><br />\n[more text]\n<br /><br />\n".
        		"<br /><br />\n<strong><a href=\"[related url 1]\">Read more</a>".
        		" | ".
        		' <img alt="photo" src="/im/imc_photo.gif" /> Photos: <a href="[photo link]">1</a> '.
        		' | <img alt="video" src="/im/imc_video.gif" /> Videos:  <a href="[video link]">1</a> | '.
        		'<img alt="audio" src="/im/imc_audio.gif" /> Audio: <a href="[audio link]">1</a></strong>'.
        		"\n<br /><br />\n<strong>\n<a href=\"[related url 2]\">related story 2 title</a> | ".
        		"<a href=\"[related url 3]\">related story 3 title</a> | ".
        		"<a href=\"[external website related to story]\">related website name</a>\n".
        		"</strong>";
        	}
        }
		if (isset($_POST["media_attachment_id"]))
			$media_attachment_id=$_POST["media_attachment_id"]+0;
        else
        	$media_attachment_id="";
        
        if (isset($_POST["media_attachment_select_id"]) && $_POST["media_attachment_select_id"]+0!=0){
	        $media_attachment_id= $media_attachment_select_id;
        }
        
        if (isset($_POST["thumbnail_media_attachment_id"])){
	        $thumbnail_media_attachment_id=$_POST["thumbnail_media_attachment_id"]+0;
	        $thumbnail_media_attachment_select_id=$_POST["thumbnail_media_attachment_select_id"];
	        if ($thumbnail_media_attachment_select_id+0!=0){
	        	$thumbnail_media_attachment_id=$thumbnail_media_attachment_select_id;
	        }
	    }else{
	    	$thumbnail_media_attachment_id="";
	    }
        
        
        if (isset($_POST["publish"]))
        {
            $posted_fields=$_POST;
            $changed_thumbnail_attachment_id=$this->process_thumbnail_upload();
            if ($changed_thumbnail_attachment_id!=0){
            	$posted_fields["thumbnail_media_attachment_id"]=$changed_thumbnail_attachment_id;
            }else{
            	$posted_fields["thumbnail_media_attachment_id"]=$thumbnail_media_attachment_id;
            }
            $changed_attachment_id=$this->process_main_upload();
            if ($changed_attachment_id!=0){
            	$posted_fields["media_attachment_id"]=$changed_attachment_id;
            }else{
            	$posted_fields["media_attachment_id"]=$media_attachment_id;
            }
            if ($this->validate_post($posted_fields))
            {
                
                $news_item_id=$this->add_blurb_to_db($posted_fields);
                $this->process_categories($news_item_id, $posted_fields);
				$this->redirect("blurb_edit.php?id=".$news_item_id);
            } else
            {
               // $this->tkeys['display_preview'] = $article->display_error_status();
            }

            $Instructions = 0;

        } 

        $recent_upload_options=$media_attachment_db_class->get_recent_admin_upload_select_list();
        $this->tkeys['local_recent_admin_uploads_select']=$blurb_renderer_class->make_select_form("media_attachment_select_id", $recent_upload_options, $media_attachment_id, "Select An Image");        
        $this->tkeys['local_thumbnail_recent_admin_uploads_select']=$blurb_renderer_class->make_select_form("thumbnail_media_attachment_select_id", $recent_upload_options, $thumbnail_media_attachment_id, "Select An Image");        

		$this->tkeys['local_media_attachment_id']=$media_attachment_id;
		$this->tkeys['local_thumbnail_media_attachment_id']=$thumbnail_media_attachment_id;
    } // end function

    
      function add_blurb_to_db($posted_fields){
    	 $blurb_db_class= new BlurbDB;
    	 $news_item_id=$blurb_db_class->create_new_blurb($posted_fields);
		 return $news_item_id;
    }
    
    
    function validate_post($posted_fields){
    	$ret=1;
    	if (!isset($posted_fields["title2"]) || strlen(trim($posted_fields["title2"]))<3){
    		$this->add_validation_message("SubTitle Was Required And It Must At Least Be 3 Characters Long");
    		$ret=0;
    	}
    	if (!isset($posted_fields["summary"]) || strlen(trim($posted_fields["summary"]))<3){
    		$this->add_validation_message("Short Version Is Required");
    		$ret=0;
    	}
    	if (trim($posted_fields["media_attachment_id"])+0!=0){
    		$media_attachment_db_class= new MediaAttachmentDB();
    		$media_atachment_info=$media_attachment_db_class->get_media_attachment_info($posted_fields["media_attachment_id"]+0);
    		if (!is_array($media_atachment_info) || sizeof($media_atachment_info)==0){
    			$this->add_validation_message("\"".$posted_fields["media_attachment_id"]."\" Is Not A Valid Image ID");
    			$ret=0;
    		}
    	}
    	if (trim($posted_fields["thumbnail_media_attachment_id"])+0!=0){
    		$media_attachment_db_class= new MediaAttachmentDB();
    		$media_atachment_info=$media_attachment_db_class->get_media_attachment_info($posted_fields["thumbnail_media_attachment_id"]+0);
    		if (!is_array($media_atachment_info) || sizeof($media_atachment_info)==0){
    			$this->add_validation_message("\"".$posted_fields["thumbnail_media_attachment_id"]."\" Is Not A Valid Image ID");
    			$ret=0;
    		}
    	}
    
    	return $ret;
    }

    
        function process_categories($news_item_id, $posted_fields){
    		$news_item_db_class= new NewsItemDB;
    		$category_array=$posted_fields['category'];
    		$posted_topic_category=$category_array[0];
    		$posted_region_category=$category_array[1];
    		$posted_other_category=$category_array[2];
			$blurb_db_class= new BlurbDB;
			
    		if ($posted_topic_category>0){

    			$news_item_db_class->add_news_item_category($news_item_id,$posted_topic_category);
    			$blurb_db_class->add_blurb_to_pages_for_given_category($news_item_id,$posted_topic_category);
    			
    		}
    		if ($posted_region_category>0){

    			$news_item_db_class->add_news_item_category($news_item_id,$posted_region_category);
    			$blurb_db_class->add_blurb_to_pages_for_given_category($news_item_id,$posted_region_category);
    			
    		}
    		if ($posted_other_category>0){
 
    			$news_item_db_class->add_news_item_category($news_item_id,$posted_other_category);
    			$blurb_db_class->add_blurb_to_pages_for_given_category($news_item_id,$posted_other_category);
    		}
    		
    }

	function process_thumbnail_upload(){
		if (isset($_FILES["thumbnail_upload"]) && $_FILES["thumbnail_upload"]["name"]!=""){
		
			$upload_util_class= new UploadUtil;
			$temp_file_name=$_FILES['thumbnail_upload']['tmp_name'];
        	$original_file_name=$_FILES['thumbnail_upload']['name'];
        	if (isset($_FILES['thumbnail_upload']['mime_type']))
        		$mime_type=$_FILES['thumbnail_upload']['mime_type'];
        	else
        		$mime_type="";
        	$restrict_to_width=THUMBNAIL_WIDTH_SMALL;
        	$restrict_to_height=THUMBNAIL_HEIGHT_SMALL;
        	if (isset($_POST["title1"]))
				$title=$_POST["title1"];
			else
				$title="";
			$new_thumbnail_media_attachment_id=$upload_util_class->process_admin_media_upload(
					$temp_file_name,
					$original_file_name,
					$mime_type,
					$restrict_to_width,$restrict_to_height,"", $title
				);
		}else{
			$new_thumbnail_media_attachment_id=0;
		}
		return $new_thumbnail_media_attachment_id;
	}
	
	function process_main_upload(){
		if (isset($_FILES["main_upload"]) && $_FILES["main_upload"]["name"]!=""){
			$upload_util_class= new UploadUtil;
			$temp_file_name=$_FILES['main_upload']['tmp_name'];
        	$original_file_name=$_FILES['main_upload']['name'];
        	$mime_type="";
        	if (isset($_FILES['main_upload']['mime_type']))
        		$mime_type=$_FILES['main_upload']['mime_type'];
        	$restrict_to_width=THUMBNAIL_WIDTH_MEDIUM;
        	$restrict_to_height=0;
        	if (isset($_POST["title1"]))
				$title=$_POST["title1"];
			else
				$title="";
			$new_media_attachment_id=$upload_util_class->process_admin_media_upload(
					$temp_file_name,
					$original_file_name,
					$mime_type,
					$restrict_to_width,$restrict_to_height, "", $title
				);
		}else{
			$new_media_attachment_id=0;
		}
		return $new_media_attachment_id;
	}
      


} // end class
?>
