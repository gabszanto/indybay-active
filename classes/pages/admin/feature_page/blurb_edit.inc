<?php
require_once("db/blurb_db_class.inc");

require_once("renderer/blurb_renderer_class.inc");
require_once("db/feature_page_db_class.inc");
require_once("db/user_db_class.inc");
require_once("renderer/user_renderer_class.inc");
require_once("renderer/feature_page_renderer_class.inc");
require_once("cache/article_cache_class.inc");
require_once("pages/admin/feature_page/blurb_add.inc");

class blurb_edit extends blurb_add {


    function execute() {
   
       $tr = new Translate();
       $feature_page_renderer_class= new FeaturePageRenderer;
       $media_attachment_db_class= new MediaAttachmentDB;

       $this->tkeys['publish_result'] = "";
  
  		$blurb_db_class= new BlurbDB;
  		$blurb_cache_class= new ArticleCache;
  		$blurb_renderer_class = new BlurbRenderer;
  		
  	   if (isset($_POST["news_item_id"]) && $_POST["news_item_id"]+0>0){
  	   		$news_item_id=$_POST["news_item_id"];
  	   		$news_item_version_id = $blurb_db_class->get_current_version_id($news_item_id);
       		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
  	   }
       if (isset($_GET['version_id']) && $_GET['version_id'] > 0) {
       		$news_item_version_id = $_GET['version_id'];
       		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
       		$news_item_id=$version_info["news_item_id"];
       }
       if (isset($_GET['id']) && $_GET['id'] > 0) {
       		$news_item_id=$_GET['id'];

       		$news_item_version_id = $blurb_db_class->get_current_version_id($news_item_id);
       		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
       }

       
        if (isset($_POST["media_attachment_id"]))
       		$media_attachment_id=$_POST["media_attachment_id"]+0;
        else
        	$media_attachment_id=0;
        if (isset($_POST["media_attachment_select_id"]))
        	$media_attachment_select_id=$_POST["media_attachment_select_id"]+0;
        else
        	$media_attachment_select_id=0;
        if ($media_attachment_select_id!=0){
        	$media_attachment_id=$media_attachment_select_id;
        }
        
        if (isset($_POST["thumbnail_media_attachment_id"]))
        	$thumbnail_media_attachment_id=$_POST["thumbnail_media_attachment_id"]+0;
        else
        	$thumbnail_media_attachment_id=0;
        	
        if (isset($_POST["thumbnail_media_attachment_select_id"]))
        	$thumbnail_media_attachment_select_id=$_POST["thumbnail_media_attachment_select_id"];
        else
        	$thumbnail_media_attachment_select_id=0;
        	
        if ($thumbnail_media_attachment_select_id+0!=0){
        	$thumbnail_media_attachment_id=$thumbnail_media_attachment_select_id;
        }
        
       $num_cur_pages= $this->setup_admin_page_lists_ret_current_num($news_item_id);
        
       if (isset($_POST['editswitch']) > 0) {
       		if ($this->validate($_POST)==1){
       			$posted_fields=$_POST;
       			$changed_thumbnail_attachment_id=$this->process_thumbnail_upload();
	            if ($changed_thumbnail_attachment_id!=0){
	            	$posted_fields["thumbnail_media_attachment_id"]=$changed_thumbnail_attachment_id;
	            	$thumbnail_media_attachment_id=$changed_thumbnail_attachment_id;
	            }else{
	            	$posted_fields["thumbnail_media_attachment_id"]=$thumbnail_media_attachment_id;
	            }
	            $changed_attachment_id=$this->process_main_upload();
	            if ($changed_attachment_id!=0){
	            	$posted_fields["media_attachment_id"]=$changed_attachment_id;
	            	$media_attachment_id=$changed_attachment_id;
	            }else{
	            	$posted_fields["media_attachment_id"]=$media_attachment_id;
	            }
            
       			$version_info=$this->add_new_version($posted_fields,$num_cur_pages);
       			$news_item_id=$version_info["news_item_id"];
       			
       			
       			$tr = new Translate;
				$this->add_status_message($tr->trans('blurb_edit_success')."<br /><br />");
			
       		}else{
       			$version_info["title1"]=$_POST["title1"];
       			$version_info["title2"]=$_POST["title2"];
       			$version_info["summary"]=$_POST["summary"];
       			$version_info["text"]=$_POST["text"];
       		}

        }
         
        $recent_upload_options=$media_attachment_db_class->get_recent_admin_upload_select_list();
        $this->tkeys['local_recent_admin_uploads_select']=$blurb_renderer_class->make_select_form("media_attachment_select_id", $recent_upload_options, $media_attachment_id, "Select An Image");        
        $this->tkeys['local_thumbnail_recent_admin_uploads_select']=$blurb_renderer_class->make_select_form("thumbnail_media_attachment_select_id", $recent_upload_options, $thumbnail_media_attachment_id, "Select An Image");        

		$this->tkeys['local_media_attachment_id']=$media_attachment_id;
		$this->tkeys['local_thumbnail_media_attachment_id']=$thumbnail_media_attachment_id;
		
		
       if (isset($_GET['add_current']) > 0) {
       		$blurb_db_class->add_blurb_to_categories_and_page($news_item_id, $_GET['add_page_id']);
       }
       		
       if (isset($_GET['add_archived']) > 0) {
       			$news_item_db_class->add_news_item_category($news_item_id, $_GET['add_page_id']);
       }
        
        $is_text_html_options = array(
                            "0" => "text/plain",
                            "1" => "text/html"
        );                    
        $s=array('<','>'); $r=array('&lt;','&gt;');
        
        
        $user_db_class= new UserDB;
       	$user_renderer_class= new UserRenderer;
        $last_updated_by_user_id=$version_info["version_created_by_id"];
        $created_by_user_id=$version_info["created_by_id"];
        if (!isset($last_updated_by_user_id))
        	$last_updated_by_user_id=0;
       	if (!isset($created_by_user_id))
        	$created_by_user_id=0;
        $user_info=$user_db_class->get_user_info($last_updated_by_user_id);
        $rendered_modified_user_info=$user_renderer_class->render_user_detail_info($user_info);
        $user_info=$user_db_class->get_user_info($created_by_user_id);
        $rendered_created_user_info=$user_renderer_class->render_user_detail_info($user_info);
        
        $this->tkeys['local_last_updated_by_user_info'] =  $rendered_modified_user_info;   
        $this->tkeys['local_created_by_user_info'] =  $rendered_created_user_info;    
        $this->tkeys['local_news_item_id'] =  htmlspecialchars($version_info["news_item_id"]);    
        $this->tkeys['local_news_item_version_id'] =  htmlspecialchars($version_info["news_item_version_id"]);       
        $this->tkeys['local_summary'] =  htmlspecialchars($version_info["summary"]);                    
        $this->tkeys['local_text'] =  htmlspecialchars($version_info['text']);
        $this->tkeys['local_media_attachment_id'] =     $version_info['media_attachment_id'];
        if ($version_info['media_attachment_id']+0!=0){
        	$media_attachment_id=$version_info['media_attachment_id'];
        	$thumbnail_edit_link="<small>(Last Saved ID Was ".$media_attachment_id." -- <a href=\"/admin/media/upload_edit.php?media_attachment_id=".
        		$media_attachment_id."\">Edit Image</a>)</small>";
        	$this->tkeys['local_attachment_edit_link']=	$thumbnail_edit_link;
        }else{
        	$this->tkeys['local_attachment_edit_link']="";
        }
        $this->tkeys['local_thumbnail_media_attachment_id'] =$version_info['thumbnail_media_attachment_id'];
        if ($version_info['thumbnail_media_attachment_id']+0!=0){
        	$thumbnail_media_attachment_id=$version_info['thumbnail_media_attachment_id'];
        	$thumbnail_edit_link="<small>(Last Saved ID Was ".$thumbnail_media_attachment_id." -- <a href=\"/admin/media/upload_edit.php?media_attachment_id=".
        		$thumbnail_media_attachment_id."\">Edit Image</a>)</small>";
        	$this->tkeys['local_thumbnail_edit_link']=	$thumbnail_edit_link;
        }else{
        	$this->tkeys['local_thumbnail_edit_link']="";
        }
        $this->tkeys['local_related_url'] = $version_info['related_url'];
        $this->tkeys['local_title1'] =    htmlspecialchars($version_info['title1']);
        $this->tkeys['local_title2'] =    htmlspecialchars($version_info['title2']);
        $this->tkeys['local_creation_date'] =  $version_info['created'];
        $this->tkeys['local_version_creation_date'] = $version_info['modified'];
        $this->tkeys['local_id'] =       $version_info['news_item_id'];
        
        
        
		$this->tkeys['local_checkbox_is_summary_html'] = $feature_page_renderer_class->make_boolean_checkbox_form("is_summary_html", $version_info['is_summary_html']);
		$this->tkeys['local_checkbox_is_text_html'] = $feature_page_renderer_class->make_boolean_checkbox_form("is_text_html", $version_info['is_text_html']);
		
		$this->tkeys['local_select_date_displayed_date'] = $feature_page_renderer_class->make_date_select_form(
		"displayed_date",10,1, $version_info['displayed_date_day'], $version_info['displayed_date_month'],$version_info['displayed_date_year']);

        
        $this->tkeys['local_previous_version_list'] = 
        	$blurb_renderer_class->render_blurb_version_list(
        		$blurb_db_class->get_version_list_info($news_item_id,5));
       	if ($news_item_version_id+0>0 && !isset($_POST['editswitch'])){
			$blurb_info=$blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);
		}else{
       		$blurb_info=$blurb_db_class->get_blurb_info_from_news_item_id($news_item_id);
       	}

	  $template=$feature_page_renderer_class->determine_single_item_template($blurb_info);
      $long_version_rendered_html=$blurb_renderer_class->get_rendered_blurb_html($blurb_info, $template);
      $rel_link=$blurb_cache_class->get_web_cache_path_for_news_item_given_date($news_item_id,$version_info["creation_timestamp"]);
      $this->tkeys['local_display_long_preview']=$long_version_rendered_html;
      $this->tkeys['local_cached_link']=ROOT_URL.$rel_link;

     $template=$feature_page_renderer_class->determine_sample_short_template($blurb_info);
     $short_version_rendered_html=$blurb_renderer_class->get_rendered_blurb_html($blurb_info, $template);
     $this->tkeys['local_display_short_preview']=$short_version_rendered_html;

    return 1;
    }

	function validate(){
		$ret=1;
		if (trim($_POST["summary"])==""){
			$this->add_validation_message("You must have a summary");
			$ret= 0;
		}
		if (trim($_POST["title2"])==""){
			$this->add_validation_message("SubTitle Is Required.");
			$ret= 0;
		}
		return $ret;
	}
	
	function add_new_version($post_array,$num_cur_pages){
		$blurb_db_class= new BlurbDB;
		$news_item_id=$post_array['news_item_id'];
		
		$news_item_version_id=$blurb_db_class->create_new_version($news_item_id, $post_array);
		$this->cache_blurb($news_item_id,$num_cur_pages);
		$version_info = $blurb_db_class->get_blurb_info_from_version_id($news_item_version_id);

		return $version_info ;
	
	}
	
	function cache_blurb($news_item_id, $num_cur_pages){
			  $blurb_cache_class=new ArticleCache;
              //recache only if not live on any page (since we then want to recache only if the page is pushed live so we dont push live blurbs being worked on
              if ($num_cur_pages==0){
              	$rel_link=$blurb_cache_class->cache_everything_for_article($news_item_id);
              	$this->tkeys['local_cached_link']=ROOT_URL.$rel_link;
              }else{
              	$this->tkeys['local_cached_link']="";
              }  
      }
      
      
      function setup_admin_page_lists_ret_current_num($news_item_id){
      	$curnum=0;
      	$blurb_db_class= new BlurbDB;
      	$tr = new Translate();
      	
      	$page_info_list=$blurb_db_class->get_current_page_info_for_blurb($news_item_id);
        $rendered_page_list="";
        $ii=1;
        $page_association_text="";
        foreach($page_info_list as $page_info){
        	if ($ii>1){
        		$rendered_page_list.=" | ";
        	}
        	$ii=$ii+1;
        	$rendered_page_list.="<a href=\"/admin/feature_page/feature_page_blurb_list.php?page_id=".$page_info['page_id'];
        	$rendered_page_list.="\">".$page_info["long_display_name"]."</a>";
        }
        $curnum=$ii-1;
        if (sizeof($page_info_list)>0){
        	$page_association_text.=$tr->trans('is_current_on_pages').":<br />"; 
        	$page_association_text.=$rendered_page_list."<br />";
        }
        $page_info_list=$blurb_db_class->get_archived_page_info_for_blurb($news_item_id);
        $rendered_page_list="";
        $ii=1;
        foreach($page_info_list as $page_info){
        	if ($ii>1){
        		$rendered_page_list.=" | ";
        	}
        	$ii=$ii+1;
        	$rendered_page_list.="<a href=\"/admin/feature_page/feature_page_archived_blurb_list.php?page_id=".$page_info['page_id'];
        	$rendered_page_list.="\">".$page_info["long_display_name"]."</a>";
        }
        if (sizeof($page_info_list)>0){
        	$page_association_text.=$tr->trans('is_archived_on_pages').":<br />"; 
        	$page_association_text.=$rendered_page_list."<br />";
        }
        $page_info_list=$blurb_db_class->get_hidden_page_info_for_blurb($news_item_id);
        $rendered_page_list="";
        $ii=1;
        foreach($page_info_list as $page_info){
        	if ($ii>1){
        		$rendered_page_list.=" | ";
        	}
        	$ii=$ii+1;
        	$rendered_page_list.="<a href=\"/admin/feature_page/feature_page_hidden_blurb_list.php?page_id=".$page_info['page_id'];
        	$rendered_page_list.="\">".$page_info["long_display_name"]."</a>";
        }
        if (sizeof($page_info_list)>0){
        	$page_association_text.=$tr->trans('is_hidden_on_pages').":<br />"; 
        	$page_association_text.=$rendered_page_list."<br />";
        }
        $this->tkeys['local_page_associations'] = $page_association_text;
        return $curnum;
      }
}

?>
