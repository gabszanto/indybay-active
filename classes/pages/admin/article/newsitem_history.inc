<?php
    require_once("db/article_db_class.inc");
    require_once("db/category_db_class.inc");
    require_once("cache/article_cache_class.inc");
    require_once("cache/newswire_cache_class.inc");
    require_once("cache/dependent_list_cache_class.inc");
    require_once("db/newswire_db_class.inc");
    require_once("db/user_db_class.inc");
    require_once("renderer/user_renderer_class.inc");
    require_once("renderer/article_renderer_class.inc");
// Class for article_edit page

class newsitem_history extends Page {

    function execute() {
    
 
 	   $article_db_class= new ArticleDB;
 	   $article_renderer_class= new ArticleRenderer;
 	   $article_cache_class= new ArticleCache;
 	   $category_db_class = new CategoryDB;
       $tr = new Translate();

       $this->tkeys['publish_result'] = "";
  
       if ($_GET['version_id'] > 0) {
       	$news_item_version_id = $_GET['version_id'];
       	$version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
       	$news_item_id=$version_info[news_item_id];
       }
       if ($_GET['id'] > 0) {
       	$news_item_id=$_GET['id'];
       	$news_item_version_id = $article_db_class->get_current_version_id($news_item_id);
       	$version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
       	
       }


        $this->tkeys['local_title1'] =    htmlspecialchars($version_info['title1']);
        $this->tkeys['local_title2'] =    htmlspecialchars($version_info['title2']);
        $this->tkeys['local_news_item_id'] =$news_item_id;
         
        $this->tkeys['local_previous_version_list'] = 
        	$article_renderer_class->render_version_list(
        		$article_db_class->get_version_list_info($news_item_id, 100), "/admin/article/article_edit.php");

 	
    }

	


      
}

?>
