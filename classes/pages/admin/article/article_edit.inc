<?php
    require_once("db/article_db_class.inc");
    require_once("db/category_db_class.inc");
    require_once("cache/article_cache_class.inc");
    require_once("cache/newswire_cache_class.inc");
    require_once("cache/dependent_list_cache_class.inc");
    require_once("db/newswire_db_class.inc");
    require_once("db/user_db_class.inc");
    require_once("pages/admin/article/article_list.inc");
    require_once("pages/admin/feature_page/blurb_add.inc");
    require_once("renderer/user_renderer_class.inc");
    require_once("renderer/article_renderer_class.inc");
    include_once(CLASS_PATH."/db/media_attachment_db_class.inc");
// Class for article_edit page

class article_edit extends Page {

    function execute() {
		$blurb_add= new blurb_add();

 	   $article_db_class= new ArticleDB;
 	   $article_renderer_class= new ArticleRenderer;
 	   $article_cache_class= new ArticleCache;
 	   $category_db_class = new CategoryDB;
       $tr = new Translate();
       
       $tr->create_translate_table('article_edit');

       $this->tkeys['publish_result'] = "";
  
    	if (array_key_exists("news_item_id",$_POST) && $_POST["news_item_id"]+0>0){
  	   		$news_item_id=$_POST["news_item_id"];
  	   		$news_item_version_id = $article_db_class->get_current_version_id($news_item_id);
       		$version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
  	   }
       if (array_key_exists("version_id",$_GET)&& $_GET['version_id']+0>0) {
       	$news_item_version_id = $_GET['version_id']+0;
       	$version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
       	$news_item_id=$version_info["news_item_id"];
       }
       if (array_key_exists("id",$_GET)&& $_GET['id'] > 0) {
       	$news_item_id=$_GET['id'];
       	$news_item_version_id = $article_db_class->get_current_version_id($news_item_id);
       	$version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
       	
       }
		
		
		$this->tkeys['LOCAL_IMAGE_OPTIONS']="";
		if ($version_info["media_attachment_id"]+0!=0){
			$media_atachment_db_class=new MediaAttachmentDB;
		    $image_info=$media_atachment_db_class->get_media_attachment_info($version_info["media_attachment_id"]);   
		 	if ($image_info["media_type_id"]==14 || $image_info["media_type_id"]==15 || $image_info["media_type_id"]==25 || $image_info["media_type_id"]==16) {
		 		$image_options="&nbsp;<strong>or Rotate By: ";
		 		$image_options.=" <input type=\"submit\" name=\"rotate\" value=\"90\"> degrees</strong>";

		 		$this->tkeys['LOCAL_IMAGE_OPTIONS']=$image_options;
		 	
		 	}
		}
		

		/// need to add generic redirect for this type of post
		if (array_key_exists("blurbify",$_POST) && $_POST["blurbify"]!=""){
			$this->blurbify_post_fields($news_item_id);
			
			$page = new Page('blurb_add', "admin/feature_page");
				$page->add_status_message("<h3>Add Blurb From Post</h3><small>".
				"Since you dont have a preview on this page, you will have to add the blurb (but dont need to push live) to review the images that were preselected for you. ".
				"The short version is the summary from the post and the long version should be the first paragraph from the text.<br />It's likely you will have to make a lot of changes to make the blurb look reasonable; ".
				" the \"create blurb from post\" functionality is only intended to speed the process of writing blurbs and not create them for you.<small>");
    			$page->build_page();	
    			echo $page->get_html();
    			include(INCLUDE_PATH."/admin/admin-footer.inc");
    			exit;
		}
		
       if (array_key_exists("editswitch",$_POST)) {
       
       		if ($this->validate($_POST)==1){
       			$version_info=$this->add_new_version($_POST);
       			$news_item_id=$version_info["news_item_id"];
       			$this->media_atachment_dependency($version_info);
       		}else{
       		 
       			$version_info["title1"]=$_POST["title1"];
       			$version_info["title2"]=$_POST["title2"];
       			$version_info["displayed_author_name"]=$_POST["displayed_author_name"];
       			$version_info["summary"]=$_POST["summary"];
       			$version_info["text"]=$_POST["text"];
       			$version_info["email"]=$_POST["email"];
       			$version_info['news_item_status_id']=$_POST['news_item_status_id'];
       			$version_info['display_contact_info']=$_POST['display_contact_info'];
       			$version_info["news_item_type_id"]=$_POST["news_item_type_id"];
       			$version_info['is_summary_html']=$_POST['is_summary_html'];
       			$version_info['is_text_html']=$_POST['is_text_html'];
       			$version_info['related_url']=$_POST['related_url'];
       			
        	}
       			
        }else{	
        	if ($version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB){
        		$preview="";
        		if (isset($_GET["p"]))
        			$preview="#preview";
        		if (isset($_GET["version_id"]))
        			$this->redirect("/admin/feature_page/blurb_edit.php?version_id=".$_GET["version_id"].$preview);
        		else
        			$this->redirect("/admin/feature_page/blurb_edit.php?id=".$news_item_id.$preview);
        		return;
        	}
        }
        if ($version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
        		$this->forced_template_file="admin/calendar/event_edit.tpl";
        }else{
        	$this->forced_template_file="admin/article/article_edit.tpl";
        } 
        
        $is_text_html_options = array(
                            "0" => "text/plain",
                            "1" => "text/html"
        );                    
        
       
       	  if ($version_info["news_item_type_id"]+0==NEWS_ITEM_TYPE_ID_POST || $version_info["news_item_type_id"]+0==NEWS_ITEM_TYPE_ID_EVENT)            
        	$type_options = array(
                            NEWS_ITEM_TYPE_ID_POST => "Post",
                            NEWS_ITEM_TYPE_ID_EVENT => "Event"
        	);
        else if ($version_info["news_item_type_id"]+0==NEWS_ITEM_TYPE_ID_ATTACHMENT ||
        		$version_info["news_item_type_id"]+0==NEWS_ITEM_TYPE_ID_COMMENT
        	){
            $type_options = array(
                            NEWS_ITEM_TYPE_ID_ATTACHMENT => "Attachment",
                            NEWS_ITEM_TYPE_ID_COMMENT => "Comment",
                            "convert_to_post" => "Post"
        	);
        }else{
        	$type_options = $article_db_class->get_news_item_types_select_list();
        }   
       	
       	if ($version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
       		$article_list= new article_list();
       		$disp_options = $article_list->get_news_item_event_display_options();
       	}else if ($version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_POST || $version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_ATTACHMENT){
       		$disp_options = $article_db_class->get_news_item_status_select_list();
       	}else{
       		$article_list= new article_list();
       		$disp_options = $article_list->get_news_item_comment_display_options();
       	}
        
        $user_db_class= new UserDB;
       	$user_renderer_class= new UserRenderer;
        $last_updated_by_user_id=$version_info["version_created_by_id"];
        $created_by_user_id=$version_info["created_by_id"];
        if (!isset($last_updated_by_user_id))
        	$last_updated_by_user_id=0;
       	if (!isset($created_by_user_id))
        	$created_by_user_id=0;
        	
        $user_info=$user_db_class->get_user_info($last_updated_by_user_id);
        $rendered_modified_user_info=$user_renderer_class->render_user_detail_info($user_info);
        $user_info=$user_db_class->get_user_info($created_by_user_id);
        $rendered_created_user_info=$user_renderer_class->render_user_detail_info($user_info);
        
        $this->tkeys['local_last_updated_by_user_info'] =  $rendered_modified_user_info;   
        $this->tkeys['local_created_by_user_info'] =  $rendered_created_user_info;    
        $this->tkeys['local_news_item_id'] =  Renderer::check_plain($version_info["news_item_id"]);    
        $this->tkeys['local_news_item_version_id'] =  Renderer::check_plain($version_info["news_item_version_id"]);       
        $this->tkeys['local_summary'] = $version_info["summary"];       
        $this->tkeys['local_email'] =  Renderer::check_plain($version_info["email"]);         
        $this->tkeys['local_phone'] =  Renderer::check_plain($version_info["phone"]);      
        $this->tkeys['local_address'] =  Renderer::check_plain($version_info["address"]);             
        $this->tkeys['local_text'] =  $version_info['text'];
        
        $media_attachment_id=$version_info['media_attachment_id'];
        $this->tkeys['local_media_attachment_id'] =$media_attachment_id;
        if ($media_attachment_id!=0){
        	$this->tkeys['local_attachment_edit_link']=
        	"<a href=\"/admin/media/upload_edit.php?media_attachment_id="
        	.$media_attachment_id
        	."\">(edit attachment id # ". $media_attachment_id.")</a></small>";
        }else{
        	$this->tkeys['local_attachment_edit_link']="";
        }
        $thumbnail_attachment_id=$version_info['thumbnail_media_attachment_id'];
        $this->tkeys['local_thumbnail_media_attachment_id'] = $thumbnail_attachment_id;
        if ($thumbnail_attachment_id!=0){
        	$this->tkeys['local_thumbnail_edit_link']=
        	"<a href=\"/admin/media/upload_edit.php?media_attachment_id="
        	.$thumbnail_attachment_id
        	."\">(edit attachment id # ". $thumbnail_attachment_id.")</a></small>";
        }else{
        	$this->tkeys['local_thumbnail_edit_link']="";
        }
        
        $this->tkeys['local_displayed_author_name'] =   Renderer::check_plain($version_info['displayed_author_name']);
        $this->tkeys['local_parent_id'] = $version_info['parent_item_id'];
        $this->tkeys['local_related_url'] =     Renderer::check_plain($version_info['related_url']);
        if (array_key_exists('contact_info_id',$version_info))
        	$this->tkeys['local_contact_info_id'] =  Renderer::check_plain($version_info['contact_info_id']);
        else
        	$this->tkeys['local_contact_info_id'] =0;
        $this->tkeys['local_title1'] =    Renderer::check_plain($version_info['title1']);
        $this->tkeys['local_title2'] =    Renderer::check_plain($version_info['title2']);
        $this->tkeys['local_creation_date'] =  $version_info['created'];
        $this->tkeys['local_version_creation_date'] = $version_info['modified'];
        $this->tkeys['local_id'] =       $version_info['news_item_id'];
        $this->tkeys['local_event_duration'] =       $version_info['event_duration'];
		$this->tkeys['local_old_display'] = $version_info['news_item_status_id'];
        $this->tkeys['select_display'] = $article_renderer_class->make_select_form("news_item_status_id",$disp_options,$version_info['news_item_status_id']);
        
        $this->tkeys['select_news_item_type_id'] = $article_renderer_class->make_select_form("news_item_type_id",$type_options,$version_info['news_item_type_id']);
       	if ($version_info["news_item_type_id"]+0==NEWS_ITEM_TYPE_ID_POST){
       		$this->tkeys['select_news_item_type_id'].="<a href=\"/admin/article/article_make_attachment.php?item_id=".$version_info["news_item_id"]."\">Convert This Into An Attachment</a>";
       	}

		$this->tkeys['local_checkbox_is_summary_html'] = $article_renderer_class->make_boolean_checkbox_form("is_summary_html", $version_info['is_summary_html']);
		$this->tkeys['local_checkbox_is_text_html'] = $article_renderer_class->make_boolean_checkbox_form("is_text_html", $version_info['is_text_html']);
		$this->tkeys['local_checkbox_display_contact_info'] = $article_renderer_class->make_boolean_checkbox_form("display_contact_info", $version_info['display_contact_info']);
		
		$displayed_year=$version_info['displayed_date_year'];
		$displayed_month=$version_info['displayed_date_month'];
		$displayed_day=$version_info['displayed_date_day'];
		$displayed_hour=$version_info['displayed_date_hour'];
		$displayed_minute=$version_info['displayed_date_minute'];
		$displayed_ampm="AM";
		if ($displayed_hour>=12){
			$displayed_ampm="PM";
		}
		if ($displayed_hour>12){
			$displayed_hour=$displayed_hour-12;
		}
		if ($displayed_hour==0){
			$displayed_hour=12;
		}

		if ($version_info["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
			$calendar_db_class= new CalendarDB();
			$event_type_info=$calendar_db_class->get_event_type_info_for_newsitem($version_info["news_item_id"]);
			$event_type_id=$event_type_info["keyword_id"];
			$keyword_list=$calendar_db_class->get_event_type_for_select_list();
			$this->tkeys['cat_type_select'] = $article_renderer_class->make_select_form(
			"event_type_id",$keyword_list,
			$event_type_id);
		}
		
		
		$this->tkeys['local_select_date_displayed_date'] = $article_renderer_class->make_datetime_select_form(
		"displayed_date",0,1, $displayed_day, $displayed_month ,$displayed_year,
		$displayed_hour,$displayed_minute,$displayed_ampm);
		

		$this->tkeys['local_select_date_displayed_date'] = $article_renderer_class->make_datetime_select_form(
		"displayed_date",10,1, $version_info['displayed_date_day'], $version_info['displayed_date_month'],$version_info['displayed_date_year'],
		$displayed_hour,$version_info['displayed_date_minute'],$displayed_ampm);
				
		$saved_categories_for_article=$article_db_class->get_news_item_category_ids($news_item_id,0);

        $available_categories=$category_db_class->get_category_info_list_by_type(1,0);
        $this->tkeys['local_checkbox_region'] = $article_renderer_class->make_checkbox_form("catartarray[]",$available_categories,$saved_categories_for_article, 3);
        $available_categories=$category_db_class->get_category_info_list_by_type(2,0);
        $this->tkeys['local_checkbox_topic'] = $article_renderer_class->make_checkbox_form("catartarray[]",$available_categories,$saved_categories_for_article, 3);
        $available_categories=$category_db_class->get_category_info_list_by_type(2,44);
        $this->tkeys['local_checkbox_int'] = $article_renderer_class->make_checkbox_form("catartarray[]",$available_categories,$saved_categories_for_article, 3);
        $available_categories=$category_db_class->get_category_info_list_by_type(3,0);
        $this->tkeys['local_checkbox_other'] = $article_renderer_class->make_checkbox_form("catartarray[]",$available_categories,$saved_categories_for_article, 3);
  
        $this->tkeys['local_previous_version_list'] = 
        	$article_renderer_class->render_version_list(
        		$article_db_class->get_version_list_info($news_item_id, 5), "/admin/article/article_edit.php");

 	   
       
       $article_info["is_preview"]=1;
       
       
       if (array_key_exists('version_id',$_GET) && $_GET['version_id']+0>0){
       		$article_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
       }else{
       		$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
       }
       $rendered_html=$article_renderer_class->get_rendered_article_html($article_info);
       $rel_link=$article_renderer_class->get_relative_web_path_from_item_info($article_info);

       $this->tkeys['LOCAL_DISPLAY_PREVIEW']=$rendered_html;
       $this->tkeys['LOCAL_PUBLISH_LINK']=FULL_ROOT_URL.substr($rel_link,1);
       if ($article_info["news_item_type_id"]+0!=NEWS_ITEM_TYPE_ID_ATTACHMENT &&
           $article_info["news_item_type_id"]+0!=NEWS_ITEM_TYPE_ID_COMMENT){
	       $this->tkeys['LOCAL_PARENT_SECTION']="<input type=\"hidden\" name=\"parent_item_id\" ";
	       $this->tkeys['LOCAL_PARENT_SECTION'].="size=\"10\" value=\"".$article_info["parent_item_id"]."\" />";
	       $this->tkeys['LOCAL_PARENT_SECTION'].=$article_info["parent_item_id"];
 	   }  else{
 	   	   $this->tkeys['LOCAL_PARENT_SECTION']="<input type=\"input\" name=\"parent_item_id\" ";
	       $this->tkeys['LOCAL_PARENT_SECTION'].="size=\"10\" value=\"".$article_info["parent_item_id"]."\" />";
 	   }    
        return 1;
    }

	function validate(){
		$ret=1;
		if (trim($_POST["title1"])==""){
			$this->add_validation_message("Title Is Required.");
			$ret= 0;
		}
		if (trim($_POST["displayed_author_name"])==""){
			$this->add_validation_message("Author Name Is Required.");
			$ret= 0;
		}
		return $ret;
	}
	
	function add_new_version($post_array){
		$article_db_class= new ArticleDB;
		$newswire_cache_class= new NewswireCache;
		$news_item_db_class= new NewsItemDB;
		
		$post_array=$this->replace_upload_if_changed($post_array);
		if (isset($_POST["rotate"])){
			$post_array=$this->rotate_image($post_array);
		}
		
	   	$news_item_id=$post_array["news_item_id"];
		$old_article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
		$old_parent_id=$old_article_info["parent_item_id"];
		$old_display_status=$old_article_info["news_item_status_id"];
		$old_type_id=$old_article_info["news_item_type_id"];
		
		if (!isset($post_array['catartarray']))
	   		$post_array['catartarray']=array();
	   	$old_cat_list=$news_item_db_class->update_news_item_categories_return_old_categories($news_item_id,$post_array['catartarray']);
        
        $news_item_version_id=$article_db_class->create_new_version($news_item_id, $post_array);
        
        $news_item_db_class= new NewsItemDB;
    	$news_item_status_id=$post_array['news_item_status_id'];
    	$news_item_type_id=$post_array['news_item_type_id'];
    	$parent_item_id=$post_array['parent_item_id'];
    	
    	if ($post_array["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
	 		$event_type_id=$post_array["event_type_id"];
	 		$calendar_db_class= new CalendarDB;
	 		$calendar_db_class->set_or_change_event_type($news_item_id,$event_type_id);
    	}
    	
    
    	$article_cache_class = new ArticleCache;
    	
    	
    	if ($news_item_type_id=="convert_to_post"){
    		$news_item_type_id=NEWS_ITEM_TYPE_ID_POST;
    		$parent_item_id=$news_item_id;    		
    	}
    	
    	if ($old_type_id!=$news_item_type_id || $old_display_status!=$news_item_status_id)
    		$news_item_db_class->update_news_item_status_and_type($news_item_id,$news_item_status_id,$news_item_type_id);
		if ($old_parent_id!=$parent_item_id)
			$news_item_db_class->update_parent_id($news_item_id,$parent_item_id);

		if ($parent_item_id==$news_item_id || $parent_item_id==0){
				$article_cache_class->cache_everything_for_article($news_item_id);
		}
		if ($old_parent_id!=$news_item_id && $old_parent_id!=0){
			$article_cache_class->cache_everything_for_article($old_parent_id);
		}

		$article_info=$article_db_class->get_article_info_from_version_id($news_item_version_id);
		if ($parent_item_id==$news_item_id || $parent_item_id==0){
			$this->process_additional_db_dependencies($old_article_info,$article_info);
		
			$dependent_list_cache_class= new DependentListCache();
			$dependent_list_cache_class->update_all_dependencies_on_update(
				$old_article_info,$article_info, $old_cat_list);
		}
		$tr = new Translate;
		$this->add_status_message($tr->trans('article_update_success')."<br /><br />");
		
		return $article_info ;
	
	}
	

	function process_additional_db_dependencies($old_article_info,
				$new_article_info){
	}
	
	function media_atachment_dependency($version_info){
		$upload_util= new UploadUtil();
		$upload_util->generate_secondary_files_on_change($version_info);
	}

	function blurbify_post_fields($news_item_id){
			$article_renderer_class= new ArticleRenderer;
			$category_db_class = new CategoryDB;
	 	   	$article_db_class= new ArticleDB;
			$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
       		$rel_link=$article_renderer_class->get_relative_web_path_from_item_info($article_info);
			$_POST["news_item_id"]="";
			$_POST["news_item_version_id"]="";
			$_POST["text"]=substr($_POST["text"],0,2000);
			$i=strrpos($_POST["text"],"\n");
			if ($i>200){
				$_POST["text"]=substr($_POST["text"],0,$i);
			}
			$_POST["text"]=trim($_POST["text"]);
			if ($_POST[is_text_html]==""){
				$_POST["text"]=nl2br(htmlentities($_POST["text"]));
			}
			$_POST["title2"]=$_POST["title1"];
			$read_more_set=0;
			if ($_POST["media_attachment_id"]+0!=0){
				$media_db_class=new MediaAttachmentDB();
				$media_info=$media_db_class->get_media_attachment_info($_POST["media_attachment_id"]);
				$media_grouping_id=$media_info["media_type_grouping_id"];
				if ($media_grouping_id+0!=MEDIA_TYPE_GROUPING_IMAGE){
					$_POST["media_attachment_id"]=0;
					$_POST["thumbnail_media_attachment)id"]=0;
					$media_type_id=$media_info["media_type_id"];
					
					if ($media_grouping_id+0==MEDIA_TYPE_GROUPING_AUDIO){
						$read_more_set=1;
						$_POST["text"]=$_POST["text"]."\n\n<br /><br />\n<strong>\n".
							"<img src=\"/im/imc_audio.gif\" alt=\"audio\" /> <a href=\"".$rel_link."\">Audio</a>\n".
							"</strong>";
					}else if ($media_grouping_id+0==MEDIA_TYPE_GROUPING_VIDEO){
						$read_more_set=1;
						$_POST["text"]=$_POST["text"]."\n\n<br /><br />\n<strong>\n".
							"<img src=\"/im/imc_video.gif\" alt=\"video\" /> <a href=\"".$rel_link."\">Video</a>\n".
							"</strong>";
					//pdf
					}else if ($media_type_id==2){
						$read_more_set=1;
						$_POST["text"]=$_POST["text"]."\n\n<br /><br />\n<strong>\n".
							"<img src=\"/im/imc_pdf.gif\" alt=\"pdf\" /> <a href=\"".$rel_link."\">PDF</a>\n".
							"</strong>";
					}
				}else{
					$read_more_set=1;
					$_POST["text"]=$_POST["text"]."\n\n<br /><br />\n<strong>\n".
					"<img src=\"/im/imc_photo.gif\" alt=\"photo\" /> <a href=\"".$rel_link."\">Photos</a>\n".
					"</strong>";
					$media_attachment_db_class=  new MediaAttachmentDB;
					$parent_id=$_POST["media_attachment_id"];
					$med_thumb_id=$media_attachment_db_class->get_thumbnail_given_parent_id_and_type($parent_id,UPLOAD_TYPE_THUMBNAIL_MEDIUM);
					if ($med_thumb_id+0!=0){
						$_POST["media_attachment_id"]=$med_thumb_id;
					}
					$small_thumb_id=$media_attachment_db_class->get_thumbnail_given_parent_id_and_type($parent_id,UPLOAD_TYPE_THUMBNAIL_SMALL);
					if ($small_thumb_id+0!=0){
						$_POST["thumbnail_media_attachment_id"]=$small_thumb_id;
				}
			}	
			}
			if ($read_more_set==0){
				$_POST["text"]=$_POST["text"]."\n\n<br /><br />\n<strong>\n".
				"<a href=\"".$rel_link."\">Read More</a>\n".
				"</strong>";
			}

			$_POST["related_url"]=$rel_link;
			
			$saved_categories_for_article=$article_db_class->get_news_item_category_ids($news_item_id,0);
			$_POST['category']=array();
			if (is_array($saved_categories_for_article)){
	        	$available_categories=$category_db_class->get_category_info_list_by_type(1,0);
	        	foreach ($saved_categories_for_article as $cat){
	        		if ($available_categories[$cat]!=""){
	        			$_POST['category'][1]=$cat;
	        		}
	        	}
	        	$available_categories=$category_db_class->get_category_info_list_by_type(2,44);
	        	foreach ($saved_categories_for_article as $cat){
	        		if ($available_categories[$cat]!=""){
	        			$_POST['category'][0]=$cat;
	        		}
	        	}
	        	$available_categories=$category_db_class->get_category_info_list_by_type(2,0);
	        	foreach ($saved_categories_for_article as $cat){
	        		if ($available_categories[$cat]!=""){
	        			$_POST['category'][0]=$cat;
	        		}
	        	}
	        }
	        
		
	}
	
	
	
	function replace_upload_if_changed($post_array){
	
		if (isset($_FILES["replace_upload"]) && $_FILES["replace_upload"]["name"]!=""){
				
			$upload_util_class= new UploadUtil;
            $media_attachement_db_class=new MediaAttachmentDB;
            $tmp_file_path=$_FILES["replace_upload"]["tmp_name"];
            $mime_type_from_post=$_FILES["replace_upload"]["type"];
            $original_file_name=$_FILES["replace_upload"]["name"];
            
            
            $file_info=$upload_util_class->process_anonymous_upload($tmp_file_path,$original_file_name,$mime_type_from_post);
            
            $image_name=$original_file_name;
          	
            $alt_tag=$post_array["summary"];
            if (sizeof($alt_tag)>20)
               $alt_tag=substr($alt_tag,20)."...";
               
            $media_attachment_id=$media_attachement_db_class->add_media_attachment($image_name,
                		 $file_info["file_name"], $file_info["relative_path"], $alt_tag,  $file_info["original_file_name"]
                		 ,   $file_info["media_type_id"], 0,  UPLOAD_TYPE_POST,UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
            $associated_attachment_ids=$upload_util_class->make_secondary_files_and_associate($media_attachment_id, $file_info);        

            $post_array["media_attachment_id"]=$associated_attachment_ids["media_attachment_id"];
            if (isset($associated_attachment_ids["thumbnail_media_attachment_id"]))
            	$post_array["thumbnail_media_attachment_id"]=$associated_attachment_ids["thumbnail_media_attachment_id"];
			else
				$post_array["thumbnail_media_attachment_id"]=0;
		}
		return $post_array;
	}
	
	function rotate_image($post_array){
			$upload_util_class= new UploadUtil;
			$media_attachement_db_class=new MediaAttachmentDB;
		    $original_info=$media_attachement_db_class->get_original_attachment_info_from_attachment_id($post_array["media_attachment_id"]);
		    if (!is_array($original_info)){
		    	$original_info=$media_attachement_db_class->get_media_attachment_info($post_array["media_attachment_id"]);  
		    } 	
		    $upload_path=UPLOAD_PATH."/";
	    	$date_rel_path=$original_info[relative_path];
	    	$upload_dir=$upload_path.$date_rel_path;
	    	$existing_full_file_path=$upload_dir.$original_info[file_name];
    		$image_util_class= new ImageUtil();
    		$rotated_file_path=$existing_full_file_path."_rotated.jpg";

    		@unlink($rotated_file_path);
			list($width_orig, $height_orig, $type, $attr) = getimagesize($existing_full_file_path);
    		$new_file_info=$image_util_class->scale_image_by_width_if_too_large($original_info[file_name], $existing_full_file_path, $rotated_file_path,   $height_orig,  $width_orig,  0, $post_array["rotate"]);

    		if (is_array($new_file_info)){

    			$file_info=$upload_util_class->process_anonymous_upload($rotated_file_path,$original_info["file_name"],"",1);

	            $image_name=$original_file_name;
	          
	            $alt_tag=$posted_fields["summary"];
	            if (sizeof($alt_tag)>20)
	               $alt_tag=substr($alt_tag,20)."...";
	               
	            $media_attachment_id=$media_attachement_db_class->add_media_attachment($image_name,
	                		 $file_info[file_name], $file_info[relative_path], $alt_tag,  $file_info[original_file_name]
	                		 ,   $file_info[media_type_id], 0,  UPLOAD_TYPE_POST,UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
	            $associated_attachment_ids=$upload_util_class->make_secondary_files_and_associate($media_attachment_id, $file_info);        
	
	            $post_array["media_attachment_id"]=$associated_attachment_ids["media_attachment_id"];
	            $post_array["thumbnail_media_attachment_id"]=$associated_attachment_ids["thumbnail_media_attachment_id"];
    		}
    		return $post_array;
    	}
}
