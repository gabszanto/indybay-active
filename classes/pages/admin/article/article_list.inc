<?php

    require_once("db/article_db_class.inc");
    require_once("db/search_db_class.inc");
    require_once("db/category_db_class.inc");
    require_once("db/feature_page_db_class.inc");
    require_once("cache/article_cache_class.inc");
    require_once("renderer/article_renderer_class.inc");
    require_once("cache/event_list_cache_class.inc");
    
//admin article list class is used for both the admin list page as 
//well as being the parent for all nonadmin lists
//this means you must be careful when making changes that you check that
//all dependent pages work and you are not opening up security holes
//by letting variables through that allow classifying
class article_list extends Page
{
    
    function execute()
    {

        $logdb= new LogDB();
        $logdb->add_log_entry_before_search();
        		
        if (isset($_POST['classify'])){
        	if ($_SESSION['session_is_editor'] == true)
				{
        		$this->bulk_classify($_POST);
        	}
        }
        
        $search_fields=$this->setup_fields();
        
        $search = $search_fields["search"];
    	
    	$page_number = $search_fields['page_number'];
        
        if ($page_size=$search_fields["page_size"]+0){
        	$page_size=$search_fields["page_size"]+0;
        }
        
        $min_item_id=$search_fields['min_item_id']; 
        $max_item_id=$search_fields['max_item_id'];
        	
        if ($page_number>2 && $min_item_id+$max_item_id==0){
				$page_number=0;
		}

        
        $tr = new Translate();
        $tr->create_translate_table('article');
        $article_renderer= new ArticleRenderer();
        $article_db_class=new ArticleDB();
        $article_cache_class=new ArticleCache();
        $tblhtml='';

  		$search_db_class= new SearchDB;
  		$media_type_grouping_id = $search_fields['media_type_grouping_id']+0;
  		
  		//setup local vars (already cleaned and set to defaults if needed via setup_fields)
    	$include_attachments=$search_fields['include_attachments'];
    	$include_posts=$search_fields['include_posts'];
    	$include_events=$search_fields['include_events'];
    	$include_blurbs=$search_fields['include_blurbs'];
    	$include_comments=$search_fields['include_comments'];
    	$topic_id = $search_fields['topic_id'];
    	$region_id = $search_fields['region_id'];
    	$page_id=$search_fields['page_id'];
    	$news_item_status_restriction = $search_fields['news_item_status_restriction'];
    	$search_date_type=$search_fields['search_date_type'];
    	$date_range_start_day=$search_fields['date_range_start_day'];
    	$date_range_start_month=$search_fields['date_range_start_month'];
    	$date_range_start_year=$search_fields['date_range_start_year'];
    	$date_range_start=$search_fields['date_range_start'];
		$date_range_end_day=$search_fields['date_range_end_day'];
    	$date_range_end_month=$search_fields['date_range_end_month'];
    	$date_range_end_year=$search_fields['date_range_end_year'];
    	$date_range_end=$search_fields['date_range_end'];
    	
    	$parent_item_id= $search_fields['parent_item_id'];
        $display_options =  $article_db_class->get_news_item_status_select_list(); 
    	
    	$search_date_type=$search_fields["search_date_type"];
    	$date_range_start_day=$search_fields["date_range_start_day"];

        $total_rows_returned=$search_db_class->standard_search($page_size, $page_number, $search,$news_item_status_restriction ,
        	$include_posts, $include_comments,$include_attachments,$include_events,$include_blurbs,
        	$media_type_grouping_id,$topic_id,$region_id,$parent_item_id ,$search_date_type, $date_range_start,$date_range_end,1, 0,0);
    	
	if (isset($search_fields["last_page"]) || isset($search_fields["last_page.x"]))
		$page_number=ceil(($total_rows_returned+0)/$page_size)-1;
	if (isset($search_fields["first_page"]) || isset($search_fields["first_page.x"]))
		$page_number=0;
	$search_result=$search_db_class->standard_search($page_size, $page_number, $search,$news_item_status_restriction ,
        	$include_posts, $include_comments,$include_attachments,$include_events,$include_blurbs,
        	$media_type_grouping_id,$topic_id,$region_id,$parent_item_id ,$search_date_type, $date_range_start,$date_range_end,0, $min_item_id,$max_item_id);
  
        // sanitize search for output!
        $search = $article_renderer->check_plain($search);

  	$this->setup_search_form($page_size, $page_number, $search, $news_item_status_restriction ,
        	$include_posts, $include_comments,$include_attachments, $include_events,
        	$include_blurbs, $media_type_grouping_id, $topic_id, $region_id, $parent_item_id,$search_date_type,
         $date_range_start_day,$date_range_start_month,$date_range_start_year,
         $date_range_end_day,$date_range_end_month,$date_range_end_year);
         
        $this->tkeys['total_rows_returned']=$total_rows_returned+0;

        $this->tkeys['last_page']=ceil( $this->tkeys['total_rows_returned']/$page_size);
        $numrows=0;
    	if (isset($search_result)) $numrows=sizeof($page_size);

   		if ($numrows == 0){
            $tblhtml.="<h2>$lang_no_results";
            if (preg_match('/ /', $keyword)) {
              $result = $result . "<h4>$lang_search_suggestion</h4>";
            }
            $tblhtml.="</h2>";
    	}
		$min_row_in_results=0;
		$max_row_in_results=0;
		if (is_array($search_result)){
	    	if ($numrows > 0){
	        	$cell_counter=0;
	            foreach ($search_result as $row) {
	                $cell_counter++;
	                $parent_row="";
	                if ($min_row_in_results==0){
	                	$min_row_in_results=$row["news_item_id"];
	                }
	                
	                //sql returns an extra line so we can do paging correctly
	                if (!($cell_counter>$page_size)){
	                    $max_row_in_results=$row["news_item_id"];
	                	if ($row["parent_item_id"]!=$row["news_item_id"] && $row["parent_item_id"]!=0)
	                		$parent_row=$article_db_class->get_article_info_from_news_item_id($row["parent_item_id"]);
	                	$tblhtml.=$this->render_row($cell_counter, $row, $parent_row, $tr, $article_renderer, $article_cache_class, $display_options,$article_db_class);     
	               		}
	               	}
	    	}
    	}

    	$this->tkeys['table_middle'] = $tblhtml;
 
    	$this->format_paging($page_number, $search_fields,$numrows, $min_row_in_results,$max_row_in_results);
    	
        $search_param_array = array();
        while(list($key, $value) = each($GLOBALS['dict'])){
            $keyid = "TPL_" . strtoupper($key);
            $search_param_arrays[$keyid] = $value;
        }
    	reset($GLOBALS['dict']);
        if (array_key_exists( 'db_down',$GLOBALS) && $GLOBALS['db_down']=="1"){
        	$this->add_status_message("The database running this site is busy due to a large number of people looking at the site. Try searching again in a few minutes");
        	$this->add_status_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
        }
        
        $logdb->update_log_id_after_search();
        
            
    	if ($GLOBALS['browser_type']=='pda'){
 	   		$this->tkeys['pda_hide_begin'] = "<!--";
 	   		$this->tkeys['pda_hide_end'] = "-->";
 	   	}else{
 	   		$this->tkeys['pda_hide_begin'] = "";
 	   		$this->tkeys['pda_hide_end'] = "";
 	   	}
 	   	
    	return 1;

    }
    
    
    function setup_search_form($page_size, $page_number, $search, $news_item_status_restriction,
        $include_posts, $include_comments,$include_attachments,$include_events,$include_blurbs,
         $media_type_grouping_id, $topic_id, $region_id, $parent_item_id,$search_date_type,
         $date_range_start_day,$date_range_start_month,$date_range_start_year,
         $date_range_end_day,$date_range_end_month,$date_range_end_year){
        
        $tr = new Translate();
        $tr->create_translate_table('article');
        
		$article_db_class= new ArticleDB;
		$media_attachment_db_class= new MediaAttachmentDB();
        $article_renderer = new ArticleRenderer();
        $article_cache_class= new ArticleCache;

        $category_db_class = new CategoryDB;
		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_international_options, $cat_topic_options);
     	 $this->tkeys["CAT_TOPIC_SELECT"] = $article_renderer->make_select_form("topic_id", $cat_topic_options, $topic_id,"All Topics");        
		$this->tkeys["LOCAL_TOPIC_ID"]=$topic_id;
		$cat_region_options=$category_db_class->get_category_info_list_by_type(1,0);
     	 $this->tkeys["CAT_REGION_SELECT"] = $article_renderer->make_select_form("region_id", $cat_region_options, $region_id,"All Regions");        
         $this->tkeys["LOCAL_REGION_ID"]=$region_id;
        
        $display_options =  $this->get_news_item_status_select_list(); 
        $medium_options = $media_attachment_db_class->get_medium_options();
        $this->tkeys["local_news_item_status_restriction"]=$news_item_status_restriction;
        $this->tkeys["SEARCH_MEDIUM"]= $article_renderer->make_select_form("media_type_grouping_id", $medium_options, $media_type_grouping_id, "All Media");
        $this->tkeys["LOCAL_MEDIA_TYPE_GROUPING_ID"]=$media_type_grouping_id;
        $this->tkeys["LOCAL_CHECKBOX_COMMENTS"]= $article_renderer->make_boolean_checkbox_form("include_comments", $include_comments);
        $this->tkeys["local_include_comments"]=$include_comments;
        $this->tkeys["LOCAL_CHECKBOX_ATTACHMENTS"]= $article_renderer->make_boolean_checkbox_form("include_attachments", $include_attachments);
        $this->tkeys["local_include_attachments"]=$include_attachments;
        $this->tkeys["LOCAL_CHECKBOX_EVENTS"]= $article_renderer->make_boolean_checkbox_form("include_events", $include_events);
        $this->tkeys["local_include_events"]=$include_events;
 
        $this->tkeys["LOCAL_CHECKBOX_POSTS"]= $article_renderer->make_boolean_checkbox_form("include_posts", $include_posts);
        $this->tkeys["local_include_posts"]=$include_posts;
        $this->tkeys["LOCAL_CHECKBOX_BLURBS"]= $article_renderer->make_boolean_checkbox_form("include_blurbs", $include_blurbs);
        $this->tkeys["local_include_blurbs"]=$include_blurbs;
        
        $this->tkeys["SEARCH_DISPLAY"]= $article_renderer->make_select_form("news_item_status_restriction", $display_options, $news_item_status_restriction);
        $this->tkeys["LOCAL_SEARCH"]=$search;
        
        $this->tkeys["LOCAL_DATE_TYPE"]=$search_date_type;
        
        $this->tkeys["LOCAL_DATE_RANGE_START_DAY"]=$date_range_start_day;
        $this->tkeys["LOCAL_DATE_RANGE_START_MONTH"]=$date_range_start_month;
        $this->tkeys["LOCAL_DATE_RANGE_START_YEAR"]=$date_range_start_year;
        
        $this->tkeys["LOCAL_DATE_RANGE_END_DAY"]=$date_range_end_day;
        $this->tkeys["LOCAL_DATE_RANGE_END_MONTH"]=$date_range_end_month;
        $this->tkeys["LOCAL_DATE_RANGE_END_YEAR"]=$date_range_end_year;
        
        $this->tkeys["GO"]=$tr->trans('go');
		$this->tkeys["FORM_NAME"]='top_form';
		
		foreach ($this->tkeys as $key=>$value){
			$search_param_array["TPL_".strtoupper($key)]=$value;
		}
		$search_param_array=$this->set_additional_values($search_param_array);
        $search_template_class = new FastTemplate(TEMPLATE_PATH . "/pages");
        $search_template_class->clear_all();
        $search_template_class->define(array("page" =>$this->get_search_template() ));
        
        $search_template_class->assign($search_param_array);
        $search_template_class->parse("CONTENT","page");
        $top_search_form = $search_template_class->fetch("CONTENT");

        $this->tkeys['top_search_form'] = $top_search_form;
        $this->tkeys["FORM_NAME"]='bottom_form';
       
        $search_template_class->assign($search_param_array);
        $search_template_class->parse("CONTENT","page");
        $bottom_search_form= $search_template_class->fetch("CONTENT");
        $this->tkeys['bottom_search_form'] = $bottom_search_form;
        $this->tkeys['local_parent_item_id'] = $parent_item_id;
        $this->tkeys['local_page_number'] = $page_number;
        
    }
    
    function set_additional_values($search_param_array){
    	return $search_param_array;
    }
    
 	function render_row($cell_counter, $row, $parent_row, $tr, $article_renderer ,$article_cache_class, $display_options, $article_db_class){
        
                $parent_item_id=$row["parent_item_id"];
                $news_item_id=$row["news_item_id"];
                $news_item_type_id=$row["news_item_type_id"];
               
                $edit_block="";
                
	                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT){
	                	$edit_block="<a  href=\"/admin/calendar/event_edit.php?id=";
	                }else if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB){
	                	$edit_block="<a  href=\"/admin/feature_page/blurb_edit.php?id=";
	                }else{
	    				$edit_block="<a  href=\"article_edit.php?id=";
	    			}
	                $edit_block.=$row["news_item_id"];
	                $edit_block .= '" target="_blank">';
	                $edit_block.=$tr->trans('edit');
	                $edit_block.="</a>&nbsp;&nbsp;";
	            
                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT)
                	$edit_block.=$article_renderer->make_select_form("value_".$cell_counter, $this->get_news_item_event_display_options(), $row["news_item_status_id"]);
                else if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_POST || $row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_ATTACHMENT)
                	$edit_block.=$article_renderer->make_select_form("value_".$cell_counter, $display_options, $row["news_item_status_id"]);
                else
                	$edit_block.=$article_renderer->make_select_form("value_".$cell_counter, $this->get_news_item_comment_display_options(), $row["news_item_status_id"]);
                $edit_block.="<input type=\"hidden\" name=\"id_".$cell_counter."\" value=\"".$row["news_item_id"]."\">";
                $edit_block.="<input type=\"hidden\" name=\"old_display_".$cell_counter."\" value=\"".$row["news_item_status_id"]."\">";
                $edit_block.="<input type=\"hidden\" name=\"type_".$cell_counter."\" value=\"".$row["news_item_type_id"]."\">";

                $edit_block.="<A href=\"javascript:changeStatusSelect(".$cell_counter.",".NEWS_ITEM_STATUS_ID_HIDDEN.");\">hide</A>";
                
                if ($row["parent_item_id"] != $row["news_item_id"] && $row["parent_item_id"] != 0)
                {
                	if ($parent_row["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN 
                		&& $parent_row["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_HIDDEN
                		&& $row["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_HIDDEN
                		&& $row["news_item_status_id"]!=NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN
                		){
		                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($parent_row["news_item_id"], $parent_row["creation_timestamp"]);
		                    $searchlink.="?show_comments=1#".$row["news_item_id"];
                    }else{
                    	$searchlink="article_edit.php?id=".$row["news_item_id"]."#preview";
                    }
                } else
                {
                   
                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
                }

                $parent_item_id=$row["parent_item_id"];
                $title = $article_renderer->check_plain($row["title1"]);
                $author = $article_renderer->check_plain($row["displayed_author_name"]);
                $summary = $article_renderer->html_purify(substr($row["summary"],0,800));
				$title2="";
                if ($parent_item_id!=$row["news_item_id"])
                {
                	$parent_title = $article_renderer->check_plain($parent_row["title1"]);
                	if ($news_item_type_id==3)
                		$title2="<br />(Comment On: ".$parent_title.")";
                	if ($news_item_type_id==NEWS_ITEM_TYPE_ID_ATTACHMENT)
                		$title2="<br />(Attachment On: ".$parent_title.")";
                    $summary = $article_renderer->html_purify(substr($row["text"],0,800));
                }
				$news_item_type_id=$row["news_item_type_id"];
                $media_type_grouping_id = $row["media_type_grouping_id"];
                
                if ($media_type_grouping_id ==0) $mime='image" src="'.MEDIA_TYPE_GROUPING_TEXT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_TEXT) $mime='image" src="'.MEDIA_TYPE_GROUPING_TEXT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_IMAGE) $mime='image" src="'.MEDIA_TYPE_GROUPING_IMAGE_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_AUDIO) $mime='image" src="'.MEDIA_TYPE_GROUPING_AUDIO_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_VIDEO) $mime='image" src="'.MEDIA_TYPE_GROUPING_VIDEO_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_DOCUMENT) $mime='image" src="'.MEDIA_TYPE_GROUPING_DOCUMENT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_OTHER) $mime='image" src="'.MEDIA_TYPE_GROUPING_OTHER_ICON.'" ';
                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT) $mime='calendar" src="'.MEDIA_TYPE_GROUPING_CALENDAR_ICON.'" ';
                if (($news_item_type_id==NEWS_ITEM_TYPE_ID_ATTACHMENT || $news_item_type_id==NEWS_ITEM_TYPE_ID_COMMENT)
                		&& $media_type_grouping_id <MEDIA_TYPE_GROUPING_IMAGE)
                	 $mime='image" src="'.COMMENT_ICON.'" ';
                	 
                $tblhtml="<div ";
                if (!is_int($cell_counter/2))

                $tblhtml.="class=\"bgsearchgrey\"";
                $tblhtml.="><b>".$row["news_item_id"]."</b>";
                if ($row["parent_item_id"]!=$news_item_id)
                	$tblhtml.="(".$row["parent_item_id"].")";

                if ($row["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN) $tblhtml.="<strike>";

                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB)
                	    $author=SHORT_SITE_NAME;
                	    
                $tblhtml .= '<a target="_blank" name="' . $cell_counter . '" href="' . $searchlink . '"><img alt="' . $mime . '" align="middle" border="0" /></a>
                    <b><a target="_blank" href="' . $searchlink . '">' . $title . '</a></b> 
                 ' . $title2 . '
                    <br />by ' . $author;
                    $tblhtml.="(<i>".strftime("%a %b %e, %Y %l:%M%p",$row["creation_timestamp"])."</i>)";
                    if ($row["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN) $tblhtml.="</strike>";
                    
                    $tblhtml.="<br /><small>".strip_tags($summary)."</small>";
                    if (isset($row["date_entered"]))	
                      $tblhtml.=" <br /><i>".$row["date_entered"]."</i>";

                
                	$tblhtml.="<br />".$edit_block;

        		$category_list=$article_db_class->get_news_item_category_info($row["news_item_id"],0);
				if (sizeof($category_list)>0){
					$catlisting=" | <small>Categories:&nbsp;&nbsp;";
				}
				foreach($category_list as $catrow){
					$catlisting .= "&nbsp;&nbsp;\"".$catrow["name"]."\"";
				}
				if (sizeof($category_list)>0){
					 $catlisting.="</small>";
				}
				if (isset($catlisting))
				$tblhtml.= $catlisting;
				if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT)
              		$tblhtml.="<div style=\"clear:left; font-style: italic\" >Event Date:".strftime("%a %b %e, %Y",$row["displayed_timestamp"])."</div><br.>";
                $tblhtml.="</div><br />";
        		return $tblhtml;
    }
    function get_search_template(){
    	return "admin/article/newswire_search.tpl";
    }
   
   function get_news_item_status_select_list(){
   		$display_options=array();
   		$display_options[NEWS_ITEM_STATUS_ID_NEW*NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]=
   			"Needs Attention";
   		$display_options[NEWS_ITEM_STATUS_ID_NEW]="New";
   		$display_options[0]="All Statuses";
   		$display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN]="Not Hidden";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlighted-Local";
   		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlighted-NonLocal";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED]
   		="Reposts";
   		$display_options[NEWS_ITEM_STATUS_ID_HIDDEN*NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]
   		="Hidden";
   		$display_options[NEWS_ITEM_STATUS_ID_OTHER]
   		="Other";
		return $display_options;
   }
   
     function get_news_item_event_display_options(){
     
     	$display_options=array();
		
		$display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]="Questionable/Don't Hide";
		$display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]="Questionable/Hide";
		$display_options[NEWS_ITEM_STATUS_ID_OTHER]="Calendar Only";
		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Not Newswire | Feature Redlinks";
		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED]="Not Newswire | Front Page Redlinks";
		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Newswire | Front Page Redlinks";
		$display_options[NEWS_ITEM_STATUS_ID_HIDDEN]="Hidden";
		$display_options[NEWS_ITEM_STATUS_ID_NEW]="New";
		return $display_options;
   }
   
   
   function get_news_item_comment_display_options(){
     
     	$display_options=array();
		
		$display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]="Questionable/Don't Hide";
		$display_options[NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]="Questionable/Hide";
		$display_options[NEWS_ITEM_STATUS_ID_OTHER]="Other";
		$display_options[NEWS_ITEM_STATUS_ID_HIDDEN]="Hidden";
		$display_options[NEWS_ITEM_STATUS_ID_NEW]="New";
		return $display_options;
   }
  
   
   function get_page_size(){
   	return 20;
   }


	//this should probably get broken up a bit but the logic to limit what has to get
	//updated is complicated enough its likely to remain like this 
	function bulk_classify($posted_fields){
		$num_rows=$this->get_page_size()+1;
		if ($num_rows+1>0){
			$GLOBALS["part_of_bulk_operation"]=1;
			$i=1;
			$pages=array();
			$status_changes_for_pages=array();
			$news_item_db_class = new NewsItemDB();
			$page_db_class = new FeaturePageDB();
			$article_cache_class = new ArticleCache();
			while ($i<$num_rows){
				
				$next_id_key="id_".$i;
				$next_old_display_key="old_display_".$i;
				$next_new_display_key="value_".$i;
				$next_type_key="type_".$i;
				if (array_key_exists($next_type_key,$posted_fields)){
				$next_type_id=$posted_fields[$next_type_key];
				if ($posted_fields[$next_id_key]!="" && $posted_fields[$next_id_key]!=0 && $posted_fields[$next_id_key]+0==$posted_fields[$next_id_key]){
				
					$next_id=$posted_fields[$next_id_key]+0;
					$next_old_display=$posted_fields[$next_old_display_key];
					$next_new_display=$posted_fields[$next_new_display_key];
						
					if ($next_old_display!=$next_new_display){
                                                $start = microtime(TRUE); // TEMPORARY DEBUG CODE
						$news_item_db_class->update_news_item_status_id($next_id, $next_new_display);

                                           //     $this->add_status_message('<div>updating status: ' . (microtime(TRUE) - $start) . ' seconds</div>'); // TEMPORARY DEBUG CODE
                                                $start = microtime(TRUE); // TEMPORARY DEBUG CODE
							$article_cache_class->recache_just_article($next_id);
                                          //      $this->add_status_message('<div>caching article: ' . (microtime(TRUE) - $start) . ' seconds</div>'); // TEMPORARY DEBUG CODE
						if ($next_type_id==NEWS_ITEM_TYPE_ID_EVENT){
							$article_db_class= new ArticleDB;
							$old_event_info=$article_db_class->get_article_info_from_news_item_id($next_id);
							$changed_event_info=$old_event_info;
							$changed_event_info["news_item_status_id"]=$next_new_display;
							$event_list_cache_class= new EventListCache();
							$event_list_cache_class->update_caches_on_change(
							$old_event_info,$changed_event_info);
						}
						$next_categories=$news_item_db_class->get_news_item_category_ids($next_id);
						$page_id_array=$page_db_class->get_associated_page_ids($next_categories);
						$more_pages=$page_db_class->get_pages_with_all_category_newswires();
						$merged_pages=array_merge($more_pages,$page_id_array);
						
						foreach($merged_pages as $page_id){
							if (array_key_exists($page_id,$pages))
								$next_status_change=$pages[$page_id];
							else
								$next_status_change=array();
								
							//limit updates for things that correspond to the same newswire
							if ($next_old_display==NEWS_ITEM_STATUS_ID_NEW ||
								$next_old_display==NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN)
								$next_old_display=NEWS_ITEM_STATUS_ID_OTHER;
							if 	($next_old_display==NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED )
								$next_old_display=NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED;
							$next_status_change[$next_old_display]=1;
							$next_status_change[$next_new_display]=1;
							$pages[$page_id]=$next_status_change;
						}
					}
				}
				}
				
				$i=$i+1;
			}
			$GLOBALS["part_of_bulk_operation"]=0;
			if (sizeof($pages)>0){
				$newswire_cache_class= new NewswireCache;
				foreach($pages as $page_id=>$page_changes){
					foreach($page_changes as $old_display_status=>$value){
						$newswire_cache_class->regenerate_newswire_for_page_helper($page_id, $old_display_status, $old_display_status);
					}
				}
			}
			
		}
		$tr = new Translate;
		$this->add_status_message($tr->trans('article_classify_success')."<br /><br />");
		
		
	}

function format_paging($page_number, $search_fields,$num_rows_on_this_page, $min_row_in_results,$max_row_in_results){

		if ($GLOBALS['browser_type']!='pda'){
			$firstarrow="<img src=\"/im/first_arrow.jpg\" alt=\"first\" />";
			$prevarrow="<img src=\"/im/prev_arrow.gif\" alt=\"previous\" />";
			$nextarrow="<img src=\"/im/next_arrow.gif\" alt=\"next\" />";
			$lastarrow="<img src=\"/im/last_arrow.jpg\" alt=\"last\" />";
		}else{
			$firstarrow="&lt;&lt;";
			$prevarrow="&lt;";
			$nextarrow="&gt;";
			$lastarrow="&gt;&gt;";
		}
		
		$this->tkeys['nav']="<p>";
    	
    	if ($page_number>0) $this->tkeys['nav'].="<a href=\"".$this->generate_paging_url(0,$search_fields, $min_row_in_results, 0)."\" rel=\"nofollow\">".$firstarrow."</a>&nbsp;&nbsp;";
	if ($page_number>0) $this->tkeys['nav'].="<a href=\"".$this->generate_paging_url($page_number-1,$search_fields,$min_row_in_results,0)."\" rel=\"nofollow\">".$prevarrow."</a>&nbsp;&nbsp;";
    	$displaypage=$page_number+1;
    	$this->tkeys['nav'].="$displaypage of ".$this->tkeys['last_page']." ";
    if ($this->tkeys['last_page'] > $page_number+1){
         	$this->tkeys['nav'].="&nbsp;&nbsp;<a href=\"".$this->generate_paging_url($page_number+1,$search_fields, 0, $max_row_in_results)."\" rel=\"nofollow\">".$nextarrow."</a>";	
         	$this->tkeys['nav'].="&nbsp;&nbsp;<a href=\"".$this->generate_paging_url($this->tkeys['last_page']-1,$search_fields, 0, $max_row_in_results)."\" rel=\"nofollow\">".$lastarrow."</a>";
	}
    	$this->tkeys['nav'].="</p>";
    	$this->tkeys['num_rows']=$num_rows_on_this_page+1;
    	if ($num_rows_on_this_page+1>$search_fields["page_size"]){
    		$this->tkeys['num_rows']=$search_fields["page_size"];
    	}
    	
    	if (array_key_exists('db_down',$GLOBALS) && $GLOBALS['db_down']=="1"){
    		$this->tkeys['nav']="";
    	}
}

/**
 * Generate a HTML-encoded paging URL.
 */
function generate_paging_url($page_number, array $search_fields, $min_item_id, $max_item_id) {
        // These parameters should already be integers but let's make sure.
	$url = $_SERVER['SCRIPT_NAME'] . '?page_number=' . intval($page_number) . '&amp;min_item_id=' . intval($min_item_id) . '&amp;max_item_id=' . intval($max_item_id);
	foreach ($search_fields as $key => $value) {
	  if (!is_object($value) && $key != 'page_number' && $key != 'min_item_id' && $key != 'max_item_id') {
	    $url = $url . '&amp;' . rawurlencode($key) . '=' . rawurlencode($value);
	  }
	}
	return $url;
}

function setup_fields(){
                $renderer_class = new Renderer;
		$search_fields=$_GET;
		if (!array_key_exists('submitted_search',$search_fields)){
        	if (!array_key_exists('include_events',$search_fields) &&
	    		 !array_key_exists('include_posts',$search_fields) 
	    		 && !array_key_exists('include_comments',$search_fields) 
	    			&& !array_key_exists('include_attachments',$search_fields) 
	    			&& !array_key_exists('include_blurbs',$search_fields) 
	    			){
	    		$search_fields["include_events"] =1;
	    		
	    		$search_fields["include_posts"]=1;

	    	}
	    	
	    	if (!array_key_exists('include_attachments',$search_fields)
	    	 && array_key_exists('media_type_grouping_id',$search_fields)
	    	 && $search_fields["media_type_grouping_id"]+0!=0 )
	    		$search_fields["include_attachments"]=1;  
    	}
    	
		if (array_key_exists("page_number",$search_fields)){
			$search_fields["page_number"]=$search_fields["page_number"]+0;
		}else{
			$search_fields["page_number"]=0;
		}
		if (array_key_exists("min_item_id",$search_fields)){
			$search_fields["min_item_id"]=$search_fields["min_item_id"]+0;
		}else{
			$search_fields["min_item_id"]=0;
		}
		if (array_key_exists("max_item_id",$search_fields)){
			$search_fields["max_item_id"]=$search_fields["max_item_id"]+0;
		}else{
			$search_fields["max_item_id"]=0;
		}
		$search_fields['search'] = (array_key_exists('search', $search_fields)) ? trim($search_fields['search']) : '';
		if (array_key_exists("include_attachments",$search_fields))
	    	$search_fields['include_attachments']=$search_fields['include_attachments']+0;
    	else
    		$search_fields['include_attachments']=0;
    	if (array_key_exists("include_posts",$search_fields))	
    		$search_fields['include_posts']=$search_fields['include_posts']+0;
    	else
    		$search_fields['include_posts']=0;
    	if (array_key_exists("include_events",$search_fields))	
    		$search_fields['include_events']=$search_fields['include_events']+0;
    	else
    		$search_fields['include_events']=0;
    	if (array_key_exists("include_blurbs",$search_fields))	
    		$search_fields['include_blurbs']=$search_fields['include_blurbs']+0;
    	else
    		$search_fields['include_blurbs']=0;
    	if (array_key_exists("include_comments",$search_fields))	
    		$search_fields['include_comments']=$search_fields['include_comments']+0;
        else	
        	$search_fields['include_comments']=0;
        if (array_key_exists("media_type_grouping_id",$search_fields))	
    		$search_fields['media_type_grouping_id']=$search_fields['media_type_grouping_id']+0;
        else	
        	$search_fields['media_type_grouping_id']=0;
		if (array_key_exists("topic_id",$search_fields))	
    		$search_fields['topic_id']=$search_fields['topic_id']+0;
        else	
        	$search_fields['topic_id']=0;
        if (array_key_exists("region_id",$search_fields))	
    		$search_fields['region_id']=$search_fields['region_id']+0;
        else	
        	$search_fields['region_id']=0;
        if (array_key_exists("page_id",$search_fields))	
    		$search_fields['page_id']=$search_fields['page_id']+0;
        else	
        	$search_fields['page_id']=0;
        if (array_key_exists("search_date_type",$search_fields))	
    		$search_fields['search_date_type'] = $renderer_class->check_plain($search_fields['search_date_type']);
        else	
        	$search_fields['search_date_type']="";
        	
        	
       if (array_key_exists("date_range_start_day",$search_fields))	
    		$search_fields['date_range_start_day']=$search_fields['date_range_start_day']+0;
       else	
        	$search_fields['date_range_start_day']="";	
       if (array_key_exists("date_range_start_month",$search_fields))	
    		$search_fields['date_range_start_month']=$search_fields['date_range_start_month']+0;
       else	
        	$search_fields['date_range_start_month']="";
       if (array_key_exists("date_range_start_year",$search_fields))	
    		$search_fields['date_range_start_year']=$search_fields['date_range_start_year']+0;
       else	
        	$search_fields['date_range_start_year']="";
        	
       if (array_key_exists("date_range_end_day",$search_fields))	
    		$search_fields['date_range_end_day']=$search_fields['date_range_end_day']+0;
       else	
        	$search_fields['date_range_end_day']="";	
       if (array_key_exists("date_range_end_month",$search_fields))	
    		$search_fields['date_range_end_month']=$search_fields['date_range_end_month']+0;
       else	
        	$search_fields['date_range_end_month']="";
       if (array_key_exists("date_range_end_year",$search_fields))	
    		$search_fields['date_range_end_year']=$search_fields['date_range_end_year']+0;
       else	
        	$search_fields['date_range_end_year']="";
        	
       $search_fields["date_range_start"]= new Date();
    	if ($search_fields["search_date_type"]!="" && $search_fields["date_range_start_day"]+0==0){
    		$search_fields["date_range_start"]->set_date_to_now();
			$search_fields['date_range_start_day']=$search_fields["date_range_start"]->get_day();
			$search_fields['date_range_start_month']=$search_fields["date_range_start"]->get_month();
			$search_fields['date_range_start_year']=$search_fields["date_range_start"]->get_year();
    	}else{
    		$search_fields["date_range_start"]->set_time(0,0,$search_fields['date_range_start_day'],$search_fields['date_range_start_month'],
    			$search_fields['date_range_start_year']);
    	}
    	
    	$search_fields["date_range_end"]= new Date();
    	if ($search_fields["search_date_type"]!="" && $search_fields["date_range_end_day"]+0==0){
    		$search_fields["date_range_end"]->set_date_to_now();
    		$search_fields["date_range_end"]->move_forward_n_days(365);
			$search_fields['date_range_end_day']=$search_fields["date_range_end"]->get_day();
			$search_fields['date_range_end_month']=$search_fields["date_range_end"]->get_month();
			$search_fields['date_range_end_year']=$search_fields["date_range_end"]->get_year();
    	}else{
    		$search_fields["date_range_end"]->set_time(0,0,$search_fields['date_range_end_day'],$search_fields['date_range_end_month'],
    			$search_fields['date_range_end_year']);
    		$search_fields["date_range_end"]->move_forward_n_days(1);
    	}
    	
    	if (!array_key_exists('parent_item_id',$search_fields)){
    		$search_fields['parent_item_id']=0;
    	}else{
    		$search_fields['parent_item_id']=$search_fields['parent_item_id']+0;
	    	if (array_key_exists('force_parent_id_search',$search_fields)){
	    		$search_fields['include_posts']=1;
	    		$search_fields['include_comments']=1;
	    		$search_fields['include_attachments']=1;
	    		$search_fields['include_events']=1;
	    		$search_fields['include_blurbs']=1;
	    		$search_fields['topic_id']=0;
	    		$search_fields['region_id']=0;
	    		$search_fields['page_id']=0;
	    		$search_fields['news_item_status_restriction']=0;
	    	}
    	}
    	
    	if (array_key_exists('page_id',$search_fields) && $search_fields['page_id']+0>0){
    		$feature_page_db= new FeaturePageDB();
    		$search_fields["topic_id"]=$feature_page_db->get_first_topic_for_page($search_fields['page_id']);
    		$search_fields["region_id"]=$feature_page_db->get_first_region_for_page($search_fields['page_id']);
    	}
    	
    	if (!isset($search_fields['news_item_status_restriction']) )
    		$search_fields['news_item_status_restriction']=NEWS_ITEM_STATUS_ALL_NONHIDDEN;
    	
       
        	
        //legacy support PLUS THIS IS WHAT GOOGLE NEWS USES (so we can render it more to their liking)
        if (array_key_exists("display",$search_fields) && $search_fields["display"]=="lg"){
        	$search_fields['news_item_status_restriction']=NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
        	$search_fields['include_events']=0;
        	$search_fields['include_posts']=1;
        	$search_fields['include_blurbs']=1;
        	$search_fields['include_comments']=0;
        	$search_fields['page_size']=45;
        }else{
        	$search_fields['page_size']=$this->get_page_size();
        }
        return $search_fields;
}

}
