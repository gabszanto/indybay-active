<?php
include_once(CLASS_PATH."/db/media_attachment_db_class.inc");
include_once(CLASS_PATH."/renderer/media_attachment_renderer_class.inc");
include_once(CLASS_PATH."/media_and_file_util/image_util_class.inc");
// Class for upload_display_add page

class upload_list extends Page {

    function execute() 
    {
    
    	$media_attachment_db_class=  new MediaAttachmentDB;
		$media_attachment_renderer_class=  new MediaAttachmentRenderer;
		
		$image_list="<br />";
		
		$page_size=15;
		if (isset($_GET["page_number"]))
			$page_number=$_GET["page_number"]+0;
		else
			$page_number=0;
		$upload_type_id=0;
		if (!isset($_GET["media_type_grouping_id"]))
			$media_type_grouping_id=MEDIA_TYPE_GROUPING_IMAGE;
		else 
			$media_type_grouping_id=$_GET["media_type_grouping_id"];
		if (!isset($_GET["upload_type_id"]))
			$upload_type_id=UPLOAD_TYPE_ADMIN_UPLOAD;
		else 
			$upload_type_id=$_GET["upload_type_id"];
			
		if (isset($_GET["keyword"]))
			$keyword=$_GET["keyword"];
		else 
			$keyword="";

		$limit_start=0;
		$creator_id=0;
		
		
		
		$start_limit=($page_number)*$page_size;
		
		$image_list_info=$media_attachment_db_class->
			get_media_attachment_list($upload_type_id, $media_type_grouping_id, 
				$creator_id,$page_size+1,$start_limit, $keyword);
				
		$additional_links="&keyword=".urlencode($keyword)."&media_type_grouping_id=$media_type_grouping_id&upload_type_id=$upload_type_id";
		$this->tkeys['nav']="";
    	if ($page_number>0){
    		 $this->tkeys['nav'].="<a href=\"/admin/media/upload_list.php?page_number=".($page_number-1).$additional_links.
        		"\"><img src=\"/im/prev_arrow.gif\" border=0/></a>&nbsp;&nbsp;";
    	}
    	$displaypage=$page_number+1;
    	$this->tkeys['nav'].=$page_number+1;
    	if (sizeof($image_list_info)>$page_size){
	        $this->tkeys['nav'].="&nbsp;&nbsp;
	        	<a href=\"/admin/media/upload_list.php?page_number=".($page_number+1).$additional_links.
	        		"\"><img src=\"/im/next_arrow.gif\" border=0/></a>";
        }
    	$this->tkeys['nav'].="";
    	
		if (is_array($image_list_info)){
			$i=0;
			foreach ($image_list_info as $next_image_info){
					$image_list.=$this->render_media_attachment_edit_row($next_image_info);
				$i=$i+1;
				if ($i==$page_size)
					break;
			}
		}
		$this->tkeys['upload_rows']=$image_list;
		$medium_options = $media_attachment_db_class->get_medium_options();
        $medium_options[0]="All Media";
		$this->tkeys['LOCAL_SELECT_GROUPING_IDS']=$media_attachment_renderer_class->make_select_form("media_type_grouping_id", $medium_options, $media_type_grouping_id);
		$upload_types = $media_attachment_db_class->get_upload_types();
        $upload_types[0]="All Types";
		$this->tkeys['LOCAL_SELECT_UPLOAD_TYPES']=$media_attachment_renderer_class->make_select_form("upload_type_id", $upload_types, $upload_type_id);
		$this->tkeys['LOCAL_KEYWORD']=$keyword;
		
	}
	
	
	function render_media_attachment_edit_row($row){
		
		$media_attachment_renderer= new MediaAttachmentRenderer();
		
		$local_upload_size="";
		
		$this->track_method_entry("upload_list","render_media_attachment_edit_row");
		if ($row["file_size"]+0!=0){
		$local_upload_size=round($row["file_size"]/1000)."kb";
		if ($row["image_width"]+0!=0){
			$local_upload_size.="(".$row["image_width"]."x".$row["image_height"].")";
		}
		}
		$ret="<tr class=\"bgsearchgrey\" >";
		$ret.="<td><a href=\"upload_edit.php?media_attachment_id=".$row["media_attachment_id"]."\">";
		$ret.=$row["media_attachment_id"]."</a></td>";
		$ret.="<td>".$row["file_name"]."<br />".$local_upload_size."</td>";
		$ret.="<td>";
		$ret.=$media_attachment_renderer->render_attachment($row,1,200,200)."</td>";
		$ret.="</tr>";
		
		$this->track_method_exit("upload_list","render_media_attachment_edit_row");
		
		return $ret;
	}
}

?>
