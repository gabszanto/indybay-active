<?php
include_once(CLASS_PATH."/db/media_attachment_db_class.inc");
include_once(CLASS_PATH."/renderer/media_attachment_renderer_class.inc");
include_once(CLASS_PATH."/media_and_file_util/image_util_class.inc");
// Class for upload_display_add page

class upload_add extends Page {

    function execute() 
    {
		$cache_class= new Cache;
		$media_attachment_renderer_class=new MediaAttachmentRenderer;
		$upload_util_class= new UploadUtil;
		$this->tkeys['local_media_attachment_info'] ="";
        $this->tkeys['local_update'] = '';
        $this->tkeys['local_uplimg'] = '';

		$had_error=0;
		$error_message="";
		
		if (isset($_FILES['upload_file']['name'])) 
        {
        	$temp_file_name=$_FILES['upload_file']['tmp_name'];
        	$original_file_name=$_FILES['upload_file']['name'];
        	$mime_type=$_FILES['upload_file']['mime_type'];
        	$restrict_to_width=$_POST["max_width"]+0;
        	if ($_POST["restrict_dimensions"]==1){
        		$restrict_to_height=(90*$_POST["max_width"]/120);
        	}else{
        		$restrict_to_height=0;
        	}
		$media_attachment_id=$upload_util_class->process_admin_media_upload(
					$temp_file_name,
					$original_file_name,
					$mime_type,
					$restrict_to_width,$restrict_to_height,$image_name, $alt_text
				);
		if ($media_attachment_id>0){
			$this->redirect("upload_edit.php?media_attachment_id=".$media_attachment_id);
		}else{
        	echo "An error occured";
        }

	}

	$this->tkeys['local_select_max_size']=
		$media_attachment_renderer_class->get_restrict_image_size_select("max_width");
	 

        return 1;
    }
}

?>
