<?php
include_once(CLASS_PATH."/db/media_attachment_db_class.inc");
include_once(CLASS_PATH."/renderer/media_attachment_renderer_class.inc");
include_once(CLASS_PATH."/media_and_file_util/image_util_class.inc");
// Class for upload_display_add page

class upload_display_add extends Page {

    function execute() 
    {
		$cache_class= new Cache;
		$media_attachment_db_class=  new MediaAttachmentDB;
		$media_attachment_renderer_class=  new MediaAttachmentRenderer;
		$this->tkeys['local_media_attachment_info'] ="";
        $this->tkeys['local_update'] = '';
        $this->tkeys['local_uplimg'] = '';

        $this->translate = new Translate();
        $this->translate->create_translate_table($pageid);
		$had_error=0;
		$error_message="";
        // Check if we are coming here from a form
        if (isset($_FILES['upload_file']['name'])) 
        {
            $copy_from = $_FILES['upload_file']['tmp_name'];
            $original_filename = $_FILES['upload_file']['name'];
            $image_name=$_POST['image_name'];
            $alt_text=$_POST['alt_text'];
            $upload_partial_rel_path="/admin_uploads/";
            
            preg_match('#([a-z0-9/\._-]+)\.([a-z0-9]+)#i', $original_filename, $regs);
        	$upload_ext = $regs[2];
        	
        	$media_attachment_db_class= new MediaAttachmentDB();
        	$media_type_id=$media_attachment_db_class->get_media_type_id_from_file_extension($upload_ext);

			$created_by_id=$_SESSION['session_user_id'];
            $file_already_exists = false;
			$scale_to_width=$_POST['max_width'];
			$new_file_name=$original_filename;
			
			$date_rel_path=$cache_class->create_date_based_relative_directory(UPLOAD_PATH ."/". $upload_partial_rel_path, time());
			if ($date_rel_path==0){
				$had_error=1;
				$error_message_string_code="image_cannot_create_cache_dir";
			}else{
				$upload_rel_path=$upload_partial_rel_path.$date_rel_path;
				$upload_web_path="/uploads/".$upload_rel_path;
				$copy_to = UPLOAD_PATH . $upload_rel_path.$new_file_name;
				$image_util_class= new IMageUtil();
            	if ($image_util_class->scale_image_by_width_if_too_large($copy_from, $copy_to,$scale_to_width)==1) 
            	{
					$media_attachment_id=$media_attachment_db_class->add_media_attachment(
					
					$image_name, $new_file_name,
                	 $upload_rel_path, $alt_text, $original_filename,   $media_type_id, $created_by_id,
     					0, UPLOAD_UPLOAD_TYPE_ADMIN_UPLOAD, UPLOAD_STATUS_NOT_VALIDATED_ADMIN);
                    $this->tkeys['local_uplimg'] = '<IMG SRC="' . $upload_web_path . rawurlencode($new_file_name) . '">';
                    $this->tkeys['local_media_attachment_info'] = "<br />Media Attachment ID Is: ".$media_attachment_id;
                    $update_status=$this->translate->trans('image_uploaded')." ".ROOT_URL.$upload_web_path .
                    	 rawurlencode($image_name). "<br />Media Attachment Id Was "+$media_attachment_id;
                  	echo $update_status;
            	} else{
            	    $error_message_string_code='image_cannot_move';
            		$had_error=1;
            	}
                	
            }
            if ($had_error==1)
            	$update_error_status($this->translate->trans($error_message_string_code));
	}

	
	$image_list="<br />";
	if ($media_attachment_id!=0)
		$limit_start=1;
	else
		$limit_start=0;
	$image_list_info=$media_attachment_db_class->get_media_attachment_list(0+$limit_start,6+$limit_start,1,$_SESSION['session_user_id']);
	if (is_array($image_list_info)){
		foreach ($image_list_info as $next_image_info){
			$file_path=UPLOAD_PATH."/".$next_image_info['relative_path'].$next_image_info['file_name'];
			if (file_exists($file_path)){
				$image_list.=$next_image_info[media_attachment_id].") <img ";
				list($width_orig, $height_orig) = getimagesize($file_path);
				if ( $width_orig>200)
					$image_list.=" width=200 ";
				$image_list.=" src=\"/uploads/".$next_image_info['relative_path'].$next_image_info['file_name'];
				$image_list.="\">($width_orig x $height_orig)<br />";
			}
		}
	}
	$this->tkeys['local_your_recent_images']=$image_list;
	$this->tkeys['local_select_max_size']=
		$media_attachment_renderer_class->get_restrict_image_size_select("max_width");
	 

        return 1;
    }
}

?>
