<?php

require_once("db/rss_pull_db_class.inc");
require_once("db/article_db_class.inc");
require_once("db/category_db_class.inc");
require_once("renderer/article_renderer_class.inc");


// Class for inbound_feed_edit page

class inbound_feed_edit extends Page
{



    function execute()
    {
    	
    	$rss_pull_db = new RSSPullDB();
    	if (isset($_POST["delete"])){
    		$feed_id=$_POST["feed_id"];
    		if (sizeof($rss_pull_db->get_item_list_for_feed($feed_id))>0){
    			echo "This Feed Has Pulled Items, You Must Delete Them To Delete The Feed";
    		}else{
	    		$rss_pull_db->delete_feed($feed_id);
	    		$this->redirect("inbound_feed_list.php");
    		}
    	}
    	if (isset($_POST["delete_items"])){
    		$feed_id=$_POST["feed_id"];
    		$rss_pull_db->delete_all_items_for_feed($feed_id);
    		echo "Items Deleted";
    	}
    	if (isset($_POST["update"])){
    		$feed_id=$_POST["feed_id"];
    		$feed_info=$_POST;
    		$feed_info['rss_feed_id']=$feed_info['feed_id'];
    		$feed_info['rss_version']=2;
    		$rss_pull_db->update_feed($feed_info);
    		foreach($_POST as $key=>$value){
    			$feed_info[$key]= htmlentities($value);	
    		}
    	}else{
    	    $feed_id=$_GET["feed_id"];
    		$feed_info = $rss_pull_db->get_feed_info($feed_id);
    		foreach($feed_info as $key=>$value){
    			$feed_info[$key]= htmlentities($value);	
    		}
    	}
    	if (isset($_POST["clone"])){
    		$_POST["feed_id"]="";
    		$url="inbound_feed_add.php?cloned=1";
    		foreach($_POST as $key=>$value){
    			$url.="&".$key."=".urlencode($value);
    		}
    		$this->redirect($url);
    	}
    	

    	
    	$category_db_class = new CategoryDB;
        $article_db_class = new ArticleDB;
        $article_renderer_class= new ArticleRenderer();
    	$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_international_options, $cat_topic_options);
     	$this->tkeys['cat_topic_select'] = $article_renderer_class->make_select_form("default_topic_id", $cat_topic_options, $feed_info['default_topic_id'], "Select A Default");        
		$cat_region_options=$category_db_class->get_category_info_list_by_type(1,0);
     	$this->tkeys['cat_region_select'] = $article_renderer_class->make_select_form("default_region_id", $cat_region_options, $feed_info['default_region_id'], "Select A Default");        
		$status_options=array();
		$status_options[NEWS_ITEM_STATUS_ID_NEW]="New";
		$status_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlight Local";
		$status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlight NonLocal";
		$status_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED]="Corporate Repost Local";
		$status_options[NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED]="Corporate Repost NonLocal";
		$status_options[NEWS_ITEM_STATUS_ID_OTHER]="Other";
		
		$this->tkeys['status_select'] ="<SELECT name=\"default_status_id\">".$article_renderer_class->create_dropdown($status_options,$feed_info['default_status_id'])."</SELECT>";
		
		$feed_types=array();
		$feed_types[0]="RSS";
		$feed_types[1]="HTML";
		$feed_types[2]="iCal";
		$this->tkeys['local_feed_type_select'] ="<SELECT name=\"feed_format_id\">".$article_renderer_class->create_dropdown($feed_types,$feed_info['feed_format_id'])."</SELECT>";
		

    	$this->tkeys['local_name'] =  	$feed_info["name"];
    	$this->tkeys['local_url'] =  	$feed_info["url"];
    	$this->tkeys['local_author_template'] =  	$feed_info["author_template"];
    	$this->tkeys['local_summary_template'] =  	$feed_info["summary_template"];
    	$this->tkeys['local_text_template'] =  	$feed_info["text_template"];
    	$this->tkeys['local_rss_feed_id'] =  	$feed_info["rss_feed_id"];
    	$this->tkeys['local_scrape_summary_start'] =  	$feed_info["summary_scrape_start"];
    	$this->tkeys['local_scrape_summary_end'] =  	$feed_info["summary_scrape_end"];
    	$this->tkeys['local_scrape_text_start'] =  	$feed_info["text_scrape_start"];
    	$this->tkeys['local_scrape_text_end'] =  	$feed_info["text_scrape_end"];
    	$this->tkeys['local_scrape_date_start'] =  	$feed_info["date_scrape_start"];
    	$this->tkeys['local_scrape_date_end'] =  	$feed_info["date_scrape_end"];
    	$this->tkeys['local_scrape_title_start'] =  	$feed_info["title_scrape_start"];
    	$this->tkeys['local_scrape_title_end'] =  	$feed_info["title_scrape_end"];
    	$this->tkeys['local_scrape_author_start'] =  	$feed_info["author_scrape_start"];
    	$this->tkeys['local_scrape_author_end'] =  	$feed_info["author_scrape_end"];
    	$this->tkeys['local_replace_url_from'] =  	$feed_info["replace_url"];
    	$this->tkeys['local_replace_url_to'] =  	$feed_info["replace_url_with"];
    	$this->tkeys['local_restrict_urls'] =  	$feed_info["restrict_urls"];
    }


}
?>
