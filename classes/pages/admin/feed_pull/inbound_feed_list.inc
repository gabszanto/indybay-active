<?php

require_once("db/rss_pull_db_class.inc");
require_once("syndication/feed_puller.inc");

// Class for inbound_feed_list page

class inbound_feed_list extends Page
{


    function execute()
    {
    	if (isset($_POST["delete_all"])){
    		$this->deleteAll();
    	}
    	if (isset($_POST["pull_all"]) || isset($_POST["pull_local"]) || isset($_POST["pull_nonlocal"])
    	 || isset($_POST["pull_corplocal"]) || isset($_POST["pull_corpnonlocal"])
    	  || isset($_POST["pull_other"])
    	){
    		$this->pullFeeds();
    	}
    	if (isset($_GET["feeds_to_pull"]))
    		$feeds_to_pull=$_GET["feeds_to_pull"];
    	else
    		$feeds_to_pull="";
    	if ($feeds_to_pull!=""){
    		$rss_pull_db = new RSSPullDB();
    		$feed_pull_array=explode(",",$feeds_to_pull);
    		if (isset($_GET["initial_pull_size"])){
    			$initial_pull_size=$_GET["initial_pull_size"]+0;
    		}else{
    			$initial_pull_size=sizeof($feed_pull_array);
    		}
    		$next_feed_id=array_pop($feed_pull_array);
    		if ($next_feed_id+0!=0){
	    		$feed_puller= new FeedPuller();
	    		$feed_puller->pull_feed_into_staging_table($next_feed_id);
	    		$feed_info=$rss_pull_db->get_feed_info($next_feed_id+0);
	    			echo "<h3>Pulled Feed \"".$feed_info["name"]."\" In ".($feed_info["last_pull_duration_usecs"]/1000000.00)." Seconds</h3>";
	    		if (sizeof($feed_pull_array)>0){
		    		$feed_info=$rss_pull_db->get_feed_info($feed_pull_array[sizeof($feed_pull_array)-1]+0);
	    			echo "<h3>Pulling Feed \"".$feed_info["name"]."\"</h3>";
	    			echo "<h4>".($initial_pull_size-sizeof($feed_pull_array))." Feeds Pulled, ".sizeof($feed_pull_array)." Left To Pull</h4>";
		    		$new_feed_url="/admin/feed_pull/inbound_feed_list.php?initial_pull_size=".$initial_pull_size."&feeds_to_pull=".implode(",",$feed_pull_array);
	    		}else{
	    			if ($_GET["return_to_item_list"]==""){
	    				$new_feed_url="/admin/feed_pull/inbound_feed_list.php";
	    			}else{
	    				if ($_GET["return_to_item_list"]==0)
	    					$new_feed_url="/admin/feed_pull/feed_item_list.php?feed_id=0";
	    				else
	    					$new_feed_url="/admin/feed_pull/feed_item_list.php?feed_id=0&status_id=".$_GET["return_to_item_list"];
	    			}
	    		}
	    		if ($_GET["return_to_item_list"]!=""){
		 			$new_feed_url.="&return_to_item_list=".$_GET["return_to_item_list"];
		 		}
	    		$this->redirect($new_feed_url,3);
	    	}
    	}
    	$tblhtml=$this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
        $this->tkeys['local_highlightedlocal_feed_list'] = $tblhtml;
        $tblhtml=$this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED);
        $this->tkeys['local_highlightednonlocal_feed_list'] = $tblhtml;
        $tblhtml=$this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED);
        $this->tkeys['local_corplocal_feed_list'] = $tblhtml;
        $tblhtml=$this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED);
        $this->tkeys['local_corpnonlocal_feed_list'] = $tblhtml;
        $tblhtml=$this->renderFeedListForStatus(NEWS_ITEM_STATUS_ID_NEW);
        $this->tkeys['local_other_feed_list'] = $tblhtml;

    }

	function deleteAll(){
		$rss_pull_db = new RSSPullDB();
		$rss_feed_list = $rss_pull_db->get_feed_list();
		foreach ($rss_feed_list as $rss_feed){
		 	$rss_pull_db->delete_all_items_for_feed($rss_feed["rss_feed_id"]);
		}
	}
	
	function pullFeeds(){
		$rss_pull_db = new RSSPullDB();
	    $feed_puller= new FeedPuller();
	    $status_id=-1;

	    if ($_POST["pull_local"])
	    	$status_id=NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED;
	    if ($_POST["pull_nonlocal"])
	    	$status_id=NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
	   	if ($_POST["pull_corplocal"])
	    	$status_id=NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED;
	    if ($_POST["pull_corpnonlocal"])
	    	$status_id=NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED;
	    if ($_POST["pull_other"])
	    	$status_id=0;
	    		
	    if ($status_id==-1)
	    	$rss_feed_list = $rss_pull_db->get_feed_list();
	    else
	    	$rss_feed_list = $rss_pull_db->get_feed_list_for_default_status($status_id);
	    $new_feed_url="/admin/feed_pull/inbound_feed_list.php?feeds_to_pull=";
	    $feed_ids=array();
	    if (is_array($rss_feed_list)){
		 foreach ($rss_feed_list as $rss_feed)
		 {
		 	array_push($feed_ids,$rss_feed["rss_feed_id"]);
		 }
		 $new_feed_url=$new_feed_url.implode(",",$feed_ids);
		 $this->redirect($new_feed_url);
		}
	}
	
	function renderFeedListForStatus($status_id){
	        $rss_pull_db = new RSSPullDB();
	        $feed_puller= new FeedPuller();
	        $rss_feed_list = $rss_pull_db->get_feed_list_for_default_status($status_id);

			$i=0;
			$totalproblemcount=0;
			$totalitemcount=0;
			$latestpulldate=0;
			$totalduration=0;
			
			
			if (!is_array($rss_feed_list) || sizeof($rss_feed_list)==0){
				return "<tr class=\"bgsearchgrey\" ><td colspan=\"5\">No Feeds Yet Exist Of This Type</td></tr>";
			}
			$tblhtml ="";
	        foreach ($rss_feed_list as $rss_feed)
	        {
	        	$i=$i+1;
	            $itemcount=0;
	            $problemcount=0;
	            $rss_item_list = $rss_pull_db->get_item_list_for_feed($rss_feed["rss_feed_id"]);
	            if (is_array($rss_item_list)){
	            	foreach ($rss_item_list as $rss_item){
	            		  $itemcount= $itemcount+1;
	            		 if ($feed_puller->item_has_problems($rss_item)){
	            		 	$problemcount=$problemcount+1;
	            		 }
	            	}
	            }
	            $totalproblemcount+=$problemcount;
	           	$totalitemcount+=$itemcount;
	           	$totalduration+=$rss_feed['last_pull_duration_usecs'];
	           	if ($rss_feed['last_pull_date']+0!=0){
	           		if ($rss_feed['last_pull_date']>$latestpulldate)
	           			$latestpulldate=$rss_feed['last_pull_date'];
	           	}
	        	$tblhtml .= "<tr ";
	        	if (!is_int(($i)/2)){
	        		$tblhtml.="class=\"bgsearchgrey\"";
	        	}
	            $tblhtml .= " ><td>";
	           

	            $tblhtml .= $rss_feed['rss_feed_id'];
	          	$tblhtml .= "</td><td>";
	          	$tblhtml .= $rss_feed['name'];
	          	$tblhtml .= "</td><td>";

	          	$tblhtml .= "<a href=\"/admin/feed_pull/feed_item_list.php?feed_id=";
	         	$tblhtml .= $rss_feed['rss_feed_id'] . "\">View Pulled Items</a>";
				$tblhtml .= "</td><td>";
	          	$tblhtml .= "<a ";
	          	$tblhtml .= " href=\"/admin/feed_pull/inbound_feed_edit.php?feed_id=";
	         	$tblhtml .= $rss_feed['rss_feed_id'] . "\">Edit/Delete Feed</a>";
				$tblhtml .= "</td><td>";
				$tblhtml .= $itemcount;
				$tblhtml .= "</td><td>";
				if ($problemcount==$itemcount && $itemcount!=0)
					$tblhtml .= "<font color=\"red\">";
				else if ($problemcount!=0)
					$tblhtml .= "<font color=\"orange\">";
				$tblhtml .= $problemcount;
				if ($problemcount!=0)
					$tblhtml .= "</font>";
				$tblhtml .= "</td><td>url: <a href=\"";
				$tblhtml .= $rss_feed['url'];
				$urlshortened=$rss_feed['url'];
				if (strlen($urlshortened)>40){
					$urlshortened=substr($urlshortened,0,40)."...";
				}
	          	$tblhtml .= "\">".$urlshortened."</a><br />Last Pull Date:";
	          	$tblhtml .= $rss_feed['last_pull_date'];
	          	$tblhtml .= "<br />Last Pull Duration:";
	          	$tblhtml .= (($rss_feed['last_pull_duration_usecs']+0)/1000000.0000);
	          	$tblhtml .= " seconds</td>";
	            $tblhtml .= "</Tr>";
	        }

	        return $tblhtml;
	}
}
?>
