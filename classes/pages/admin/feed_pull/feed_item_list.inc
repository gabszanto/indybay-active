<?php

require_once("db/rss_pull_db_class.inc");
require_once("syndication/feed_puller.inc");

// Class for feed_item_list page

class feed_item_list extends Page
{

    function execute()
    {
        $rss_pull_db = new RSSPullDB();
        if (isset($_GET["feed_id"]))
        	$feed_id=$_GET["feed_id"]+0;
        else
        	$feed_id=0;
        if (isset($_GET["status_id"]))	
        	$status_id=$_GET["status_id"]+0;
        else
        	$status_id=0;
    	if ($status_id+0==0 && isset($_POST["status_id"])){
    		$status_id=$_POST["status_id"]+0;
    	}
        if (isset($_POST["force_feed_pull"])){
    		$feed_id=$_POST["feed_id"];
    		$this->pull_feed($feed_id, $status_id);
    	}else if (isset($_POST["publish"])){
    		$i=1;
    		$idlist="";
    		while ($_POST["id_".$i]!=""){
    			if ($_POST["idset_".$i]!=""){
    				$idlist.=$_POST["id_".$i].",";
    			}
    			$i=$i+1;
    		}

    		if ($idlist!=""){
    			$idlist=substr($idlist,0,strlen($idlist)-1);
    			$this->redirect("publish_preview.php?feed_ids=".$idlist);
    		}
    	}
    	$feed_id=$feed_id+0;

    	if ($feed_id==0){
    		$feed_info = array();
    		if ($status_id==0){
	    		$this->tkeys['local_feed_name'] =  	"All Feeds";
	    	}else if ($status_id==2){
	    		$this->tkeys['local_feed_name'] =  	"Other Feeds";
	    	}else if ($status_id==3){
	    		$this->tkeys['local_feed_name'] =  	"Highlighted Local Feeds";
	    	}else if ($status_id==5){
	    		$this->tkeys['local_feed_name'] =  	"Highlighted NonLocal Feeds";
	    	}else if ($status_id==7){
	    		$this->tkeys['local_feed_name'] =  	"Corporate Local Feeds";
	    	}
	    	else if ($status_id==11){
	    		$this->tkeys['local_feed_name'] =  	"Corporate NonLocal Feeds";
	    	}
	    	$this->tkeys['local_url'] =  	"";
	    	$this->tkeys['local_feed_id'] =  	0;
	    	$this->tkeys['local_status_id'] =  	$status_id;
    	}else{
	    	$feed_info = $rss_pull_db->get_feed_info($feed_id);
	    	$this->tkeys['local_feed_name'] =  	$feed_info["name"];
	    	$this->tkeys['local_url'] =  	$feed_info["url"];
	    	$this->tkeys['local_feed_id'] =  	$feed_info["rss_feed_id"];
	    	$this->tkeys['local_status_id'] =  	"0";
		}
    	$tblhtml=$this->renderItemList($feed_id, $status_id);
        $this->tkeys['local_item_list'] = $tblhtml;
        if ($feed_id+0==0)
        	$this->tkeys['local_additional_links'] = "";
        else{
        	$this->tkeys['local_additional_links'] = "| <a href=\"/admin/feed_pull/inbound_feed_edit.php?feed_id=".$feed_id."\">Edit Feed Definition</a>";
    		$this->tkeys['local_additional_links'] .="</strong><small><br />Last Pull Date:".$feed_info["last_pull_date"];
    		$this->tkeys['local_additional_links'] .="<br />Last Pull Duration:".($feed_info["last_pull_duration_usecs"]+0)/1000000.000. " seconds</small><strong>";
    	}
    }
    
    function pull_feed($feed_id, $status_id){
    	$feed_puller= new FeedPuller();
    	$rss_pull_db = new RSSPullDB();
    	if ($feed_id+0!=0){
    		$feed_puller->pull_feed_into_staging_table($feed_id);
    	}else{ 
    		if ( $status_id!=0){
    			$rss_feed_list = $rss_pull_db->get_feed_list_for_default_status($status_id);
    		}else{
    			$rss_feed_list = $rss_pull_db->get_feed_list();
    		}
    		$new_feed_url="/admin/feed_pull/inbound_feed_list.php?feeds_to_pull=";
	    	$feed_ids=array();
	    	if (is_array($rss_feed_list)){
			 	foreach ($rss_feed_list as $rss_feed)
			 	{
			 		array_push($feed_ids,$rss_feed["rss_feed_id"]);
				 }
				 if ($status_id==0)
				 	$list=$rss_pull_db->get_feed_list();
				 else
				 	$list=$rss_pull_db->get_feed_list_for_default_status($status_id);
				 
				$num_feeds=sizeof($list);
			 	$new_feed_url=$new_feed_url.implode(",",$feed_ids)."&return_to_item_list=".($status_id+0)."&initial_pull_size=".$num_feeds;
			 	$this->redirect($new_feed_url,3);	
		 	}
    	}
    }

	function renderItemList($feed_id, $status_id){
			$tr = new Translate("");
	        $rss_pull_db = new RSSPullDB();
	        $feed_puller= new FeedPuller();
	        if ($status_id+0==0)
	        	$rss_item_list = $rss_pull_db->get_item_list_for_feed($feed_id);
			else
				$rss_item_list = $rss_pull_db->get_item_list_by_feed_status($status_id+0);
			$i=0;
			if (is_array($rss_item_list)){
				
				if ($feed_id==0){
					$feednames=array();
					$feedlist= $rss_pull_db->get_feed_list();
				   foreach ($feedlist as $feed_item){
				   		$feednames[$feed_item["rss_feed_id"]]=$feed_item["name"];
				   }
				}
				$tblhtml ="";
		        foreach ($rss_item_list as $rss_item)
		        {
		        	if ($feed_id==0 && $status_id==0){
		        		if ($feed_puller->item_has_problems($rss_item) || $rss_item["creation_date_timestamp"]<time()-60*60*24*2)
		        			continue;
		        	}else if ($feed_id==0){
		        		if ($feed_puller->item_has_problems($rss_item) && $rss_item["creation_date_timestamp"]<time()-60*60*24*2)
		        			continue;
		        		if ($rss_item["creation_date_timestamp"]<time()-60*60*24*7)
		        			continue;
		        	}
		        	
		        	$i=$i+1;
		        	if ($i>70)
		        		break;
		        	if ($i>40 && $rss_item["creation_date_timestamp"]<time()-24*60*60)
		        		break;
		        	if ($i>20 && $rss_item["creation_date_timestamp"]<time()-24*3*60*60)
		        		break;
		        	$tblhtml .= "<tr ";
		        	if (!is_int($i/2)){
		        		$tblhtml.="class=\"bgsearchgrey\"";
		        	}
		        	if ($rss_item["creation_date_timestamp"]>strtotime("1/1/2000")){
		        		$cdate=date("m/d/Y",$rss_item["creation_date_timestamp"]);
		        	}else{
		        		$cdate="No Date Found";
		        	}
					if ($rss_item['is_published']=="1"){
						$tblhtml .= "><td colspan=\"2\"><font color=\"green\">Already Published</font>";
					}else{
			            $tblhtml .= " ><td><input type=\"checkbox\" name=\"idset_".$i."\" value=\"".$rss_item['rss_item_id']."\"></td>";			        
			           	$tblhtml .= "<td>";
						$tblhtml .= "Not Published";
					}
		           
		            $tblhtml .= "</td><td>".$cdate."</td><td>";
		            if ($feed_id==0){
		            	 $tblhtml .= $feednames[$rss_item['rss_feed_id']]."</td><td>";
		            }
		            $tblhtml .= "<font ";
		            if ($feed_puller->item_has_problems($rss_item)){
		            	  $tblhtml .= " color=\"red\" ";
		            }
		            $tblhtml .= "><input type=\"hidden\" name=\"id_".$i."\" value=\"".$rss_item['rss_item_id']."\">";
		            $tblhtml .= $rss_item['rss_item_id'];
		            $tblhtml .= "</font></td><td><font ";
	
		            $tblhtml .= "<a href=\"feed_item_edit.php?rss_item_id=".$rss_item['rss_item_id']."\">Edit and Publish</>";
		          	$tblhtml .= "</td><td>";
		          	 $tblhtml .= $rss_item['title'];
		          	$tblhtml .= "</td>";
		            $tblhtml .= "</Tr>";
		        }
	        }
	        return $tblhtml;
	}
	

}
?>
