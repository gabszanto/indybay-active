<?php

require_once("db/db_class.inc");

class breaking_list extends Page {
    function breaking_list() {
        return 1;
    }
    function execute() {
        // Execution method, does nothing
       $db_obj= new DB;
       $tr = new Translate();
       $tr->create_translate_table('breaking');
       $this->tkeys['create_result'] = '';
       if ($_GET['publish']) {
       $query="update legacy_sfactive_breaking set display='t' where display='f' and current='t' and headline='$_GET[publish]'";
       $result = $db_obj->execute_statement($query); }
       $query="select * from legacy_sfactive_breaking where current='t' or send='t' order by headline asc,version asc";
       $result = $db_obj->query($query);
       foreach ($result as $row) {
          if ($row['headline'] == $lastrow) { $listing .= '<blockquote>'; }
          $listing .= '<div style="margin: 1em">';
          if ($row['display'] == 'f' and $row['current'] == 't') { $listing .= '<div style="color: grey; background: yellow">'; } 
          if ($row['current'] == 't') $listing .= '<span><a href="update.php?newsitem=' . $row['headline'] . '">Update</a></span>';      
          if ($row['current'] == 't' and $row['display'] == 'f') $listing .= ' | <span><a href="list.php?publish=' . $row['headline'] . '">Publish</a></span>';      
          if ($row['send'] == 't') { $listing .= ' <small style="background: orange"><b>Txt msg sent ' . date('M. jS g:ia',strtotime($row['updated']))  . '</b></small>'; }
          $listing .= ' <span class="newsitemdate"><b>' . date('M. jS g:ia',strtotime($row['created'])) . ':</b></span>';      
          $listing .= ' <span class="txt">' . htmlspecialchars($row['txt']) . '</span>';
          $listing .= ' <span class="htm">' . $row['htm'] . '</span>';      
          if ($row['display'] == 'f' and $row['current'] == 't') { $listing .= '</div>'; } 
          $listing .= '</div>';
          if ($row['headline'] == $lastrow) { $listing .= '</blockquote>'; }
          $lastrow = $row['headline'];
       }
       $this->tkeys['listing'] = $listing;
    }
}
?>
