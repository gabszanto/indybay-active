<?php

require('db/news_item_version_db_class.inc');

class breaking_create extends Page {

  function execute() {
    if (!empty($_POST['AccountSid']) && !empty($_POST['CallSid']) && $_POST['AccountSid'] == TWILIO_SID) {
      require "twilio/Services/Twilio.php";
      $db_obj = new DB();
      $response = new Services_Twilio_Twiml();
      $titles = $db_obj->single_column_query("SELECT title1 FROM news_item_version INNER JOIN news_item ON current_version_id = news_item_version_id INNER JOIN dispatch ON news_item.news_item_id = dispatch.news_item_id WHERE dispatch.sid = '" . $db_obj->prepare_string($_POST['CallSid']) . "'");
      foreach ($titles as $title) {
        $response->say($title);
      }
      $response->hangup();
      print $response;
    }
  }

}
