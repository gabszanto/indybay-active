<?php

require_once("db/db_class.inc");

class breaking_update extends Page {
    function breaking_update() {
        return 1;
    }
    function execute() {
       $db_obj =new DB;
        $query = "select username, user_id from user";
        $result = $db_obj->query($query);
        while ($next_user=array_pop($result))
        {
            $user_id=$next_user[user_id];
            $username=$next_user[username];
            $user_dictionary[$user_id]=$username;
        }
       $tr = new Translate();
       $tr->create_translate_table('breaking');
       $this->tkeys['create_result'] = '';
       if ($_GET['newsitem']) $newsitem = $_GET['newsitem']; 
       if ($_POST['newsitem']) $newsitem = $_POST['newsitem']; 
       if ($_POST['txt']) { 
           $query="update legacy_sfactive_breaking set current='f', updated=updated where headline='$newsitem'";
           $result = $db_obj->execute_statement($query);
           $query="insert into legacy_sfactive_breaking (user,current,txt,htm,created,send,display,headline) 
           values('$_SESSION[user_id]','t','$_POST[txt]',
           '$_POST[htm]','$_POST[created]','$_POST[send]','$_POST[display]','$_POST[newsitem]')";
           $result = $db_obj->execute_statement_return_autokey($query);
           $this->tkeys['create_result'] = 'BREAKING NEWS #' . $_POST[newsitem] . ' UPDATED';
           if ($_POST['send']=='t') {
               exec('/usr/local/bin/curl --cookie ' . CACHE_PATH . '/cookies --cookie-jar ' . CACHE_PATH . 
               '/cookies --insecure https://www.txtmob.com/login.php');
               exec('/usr/local/bin/curl --cookie ' . CACHE_PATH . '/cookies --cookie-jar ' . CACHE_PATH . 
               '/cookies --data login=indybay --data pw=sfbayindym --insecure https://www.txtmob.com/login.php');
               exec('/usr/local/bin/curl --cookie ' . CACHE_PATH . '/cookies --cookie-jar ' . CACHE_PATH . 
               '/cookies --data listID=1223 --data message=' . 
               rawurlencode($_POST['txt']) . 
               ' --insecure https://www.txtmob.com/do_send.php');
               $this->tkeys['create_result'] .= '<br />TXT MSG SENT';
           }
       }
       if ($newsitem) {
           $this->tkeys['checkedpublish']='';
           $this->tkeys['checkedhidden']='';
           $query = "select * from legacy_sfactive_breaking where headline='$newsitem'";
           $result = $db_obj->query($query);
           foreach ($result as $row) {
               $rowcount = $rowcount + 1;
               if ($rowcount==1) {
                   $this->tkeys['txt'] = htmlspecialchars($row['txt']);
                   $this->tkeys['htm'] = htmlspecialchars($row['htm']);
                   $this->tkeys['created'] = htmlspecialchars($row['created']);
                   if ($row['display']=='t') $this->tkeys['checkedpublish'] = 'checked="checked"';
                   if ($row['display']=='f') $this->tkeys['checkedhidden'] = 'checked="checked"';
               }
               $rowlist .= '<div style="margin: 0.5em; padding: 0.25em; border: 3px solid gray" class="rowlist">';
               $rowlist .= '<div class="date">Updated: ' . $row['updated'] . '</div>';
               $rowlist .= '<div class="date">Created: ' . $row['created'] . '</div>';
               $rowlist .= '<div class="txt">TXT: ' . htmlspecialchars($row['txt']) . '</div>';
               $rowlist .= '<div class="htm">HTM: ' . htmlspecialchars($row['htm']) . '</div>';
               $rowlist .= '<div class="parameters"><b>';
               if ($row['send']=='t') $rowlist .= 'sent';
               if ($row['send']=='f') $rowlist .= 'not sent';
               $rowlist .= ' | ';
               if ($row['display']=='t') $rowlist .= 'published';
               if ($row['display']=='f') $rowlist .= 'not published';
               $rowlist .= '</b></div>';
               $rowlist .= '<div class="user">Submitted by ' . $user_dictionary[$row['user']] . '</div>';
               $rowlist .= '</div>';
           }
           $this->tkeys['rowlist'] = $rowlist;
           $this->tkeys['newsitem'] = $newsitem;
       }
    }
}
?>
