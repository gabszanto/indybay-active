<?php

// Class for user_add page
require_once("db/user_db_class.inc");
require_once("renderer/user_renderer_class.inc");

class user_change_password extends Page {


    function execute() {
        $user_renderer_class= new UserRenderer();
        $userid = '';
        $is_edit = false;
        $this->tkeys['local_form_action'] = "user_add.php";

        if (isset($_POST['user_id'])) $user_id = $_POST['user_id'];
        if (isset($_GET['user_id'])) $user_id = $_GET['user_id'];

        $user_obj=new UserDB();

        if (isset($_POST["change_password"])){
        	if ($_POST["new_password1"]!=$_POST["new_password2"]){
        		$this->add_validation_message("new passwords must match");
        	}else if (strlen(trim($_POST["new_password1"]))<5){
        		$this->add_validation_message("password must be at least 5 characters long");
        	}else if ($user_obj->validate_old_password($user_id, $_POST["old_password"])!=1){
        		$this->add_validation_message("old password entered was not correct");
        	}else{
        		$user_obj->change_user_password($user_id, trim($_POST["new_password1"]));
        		$this->redirect("user_list.php");
        	}
        }
        
        $user_detail=$user_obj->get_user_info($user_id);
        
        if (isset($_POST["email_password"]) && strlen($_POST["email_password"])>0){
        	$email_address=$user_detail['email'];
        	if (strlen(trim($email_address))<5 or strpos($email_address,"@")<1
        		or strpos($email_address, " ")>0
        		or strpos($email_address, "(")>0
        		or strrpos($email_address, ".")<3
        	){
        		$this->add_validation_message($email_address." is not a valid email address");
        	}else{
        		$subject="indybay admin message";
        		
        		$body="This email may eventually contain a temporrary password that will expire within a few hours of when its sent"
					."but there are enough security risks in emailing passwords I need to review the functionality with the group" 
					."before implementing it. If you locked yourself out of the dev machine email sfbay-web and I can change the password via the DB";
        		if (mail($email_address,$subject, $body)){
		        	$this->add_status_message("A password has been emailed to ".$email_address);
		        }else{
		        	echo "Unable to send email at this time";
		        }
	        }
        }
        
        $this->tkeys['local_username'] =   $user_detail['username'];
       
        $this->tkeys['local_email'] =      $user_detail['email'];
        $this->tkeys['local_user_id'] = $user_id;
        $this->tkeys['local_user_id'] = $user_id;
        return 1;
    }

}

?>
