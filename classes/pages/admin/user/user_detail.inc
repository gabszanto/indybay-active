<?php

// Class for user_display_edit page
require_once("db/user_db_class.inc");
require_once("renderer/user_renderer_class.inc");
class user_detail extends Page {

    function execute() {
  		$user_renderer_class= new UserRenderer();
        $userid = '';
        $is_edit = false;

        if (isset($_POST['user_id']))
        	 $user_id = $_POST['user_id'];
        if (isset($_GET['user_id']))
          $user_id = $_GET['user_id'];

        $user_obj=new UserDB();
        if (isset($_POST["user_id"])){
        	$user_obj->update($user_id, $_POST);
        	$this->redirect("user_list.php");
        }
        $user_detail=$user_obj->get_user_info($user_id);
        $this->tkeys['local_username'] =   $user_detail['username'];
       
       	$this->tkeys['local_user_id'] = $user_id;
        $this->tkeys['local_email'] =      $user_detail['email'];
        $this->tkeys['local_phone'] =      $user_detail['phone'];
        $this->tkeys['local_first_name'] = $user_detail['first_name'];
        $this->tkeys['local_last_name'] =  $user_detail['last_name'];
        $this->tkeys['local_last_login'] =  $user_detail['last_login'];
        $this->tkeys['local_form_action'] = "user_update.php";
        $this->tkeys['local_form_hidden'] = "<INPUT TYPE=\"HIDDEN\" NAME=\"user_id\" VALUE=\"" . $user_id . "\">"; 

		if ($user_detail['has_login_rights']==1)
			$this->tkeys['local_has_login_rights'] ="Yes";
		else
			$this->tkeys['local_has_login_rights'] ="No";
		if (isset($user_detail['created']))
			$creation_date=$user_detail['created'];
		else
			$creation_date="";
		if (isset($user_detail['created_by_id']))
			$created_by_id=$user_detail['created_by_id'];
		else
			$created_by_id=0;
		$user_detail2="";
		if ($created_by_id!=0)
			$user_detail2=$user_obj->get_user_info($created_by_id);
		if (is_array($user_detail2)==0)
			$this->tkeys['local_creation_info'] =   "created by database migration on ".$creation_date;
		else
			$this->tkeys['local_creation_info'] =   "created by ".$user_detail2['username']." on ".$creation_date;
		 
        return 1;
    }

}

?>
