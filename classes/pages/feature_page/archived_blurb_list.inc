<?php

require_once (CLASS_PATH."/db/feature_page_db_class.inc");
require_once (CLASS_PATH."/renderer/blurb_renderer_class.inc");
// Class for display_by_date
class archived_blurb_list extends Page
{

    function execute()
    {

        
        
		//old google news link
		if (!empty($_GET['current'])) {
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/news/search_results.php?news_item_status_restriction=690690&include_blurbs=1&include_posts=0&include_events=0");
					exit;
		}
		
		$logdb= new LogDB();
        $logdb->add_log_entry_before_search();
        
		$page_id=(int)$_GET["page_id"];
		$page_number = isset($_GET['page_number']) ? (int)$_GET['page_number'] : 0;
		
		if ($page_id==0)
			$page_id=FRONT_PAGE_CATEGORY_ID;
		
		$feature_page_db_class= new FeaturePageDB();
		
		$page_info=$feature_page_db_class->get_feature_page_info($page_id);
		if (empty($GLOBALS['db_down'])) {
			if (!is_array($page_info)){
				$this->redirect("/");
			}
			$feature_page_name = $page_info['long_display_name'];
			$feature_page_url = '/' . $page_info['relative_path'];
			$this->tkeys['local_feature_page_name'] = $feature_page_name;
			$this->tkeys['local_feature_page_url'] = $feature_page_url;
			
			$start_limit=0;
			
			
			$page_size=7;
			$start_limit=($page_number)*$page_size;
			$blurb_list=$feature_page_db_class->get_archived_blurb_list_limited_info($page_id, $start_limit, $page_size+1);
			if (is_array($blurb_list)){
				$this->tkeys['nav']="";
		    	if ($page_number>0){
		    		 $this->tkeys['nav'].="<a href=\"/archives/archived_blurb_list.php?page_id=".
		        		$page_id."&page_number=".($page_number-1).
		        		"\"><img src=\"/im/prev_arrow.gif\" border=0/></a>&nbsp;&nbsp;";
		    	}
		    	$displaypage=$page_number+1;
		    	$this->tkeys['nav'].=$page_number+1;
		    	if (sizeof($blurb_list)>$page_size){
			        $this->tkeys['nav'].="&nbsp;&nbsp;
			        	<a href=\"/archives/archived_blurb_list.php?page_id=".
			        		$page_id."&page_number=".($page_number+1).
			        		"\"><img src=\"/im/next_arrow.gif\" border=0/></a>";
		        }
		    	$this->tkeys['nav'].="";
		    	
		    	$blurb_renderer_class= new BlurbRenderer();
		    	$i=0;
			$tblhtml = '';
		        foreach ($blurb_list as $blurb){
		        	$news_item_id= $blurb['news_item_id'];
		        	$next_html=$blurb_renderer_class->get_rendered_blurb_html_for_list_from_news_item_id($news_item_id);
		        	$tblhtml.=$next_html;
		        	$i=$i+1;
		        	if ($i==$page_size)
		        		break;
		        }
	        }
	   		$this->tkeys['local_html'] = $tblhtml;
			$this->tkeys['local_page_id'] = $page_id;
		}else{
			$this->tkeys['local_html'] ="";
			$this->tkeys['local_page_id'] = 0;
			$this->tkeys['nav']="";
			$this->tkeys['local_feature_page_name'] = "Feature Page";
			$this->tkeys['local_feature_page_url'] = "/";
        	$this->add_status_message("The database running this site is busy due to a large number of people looking at the site. Try searching again in a few minutes");
        	$this->add_status_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
		}
		        
        $logdb->update_log_id_after_search();
		
    }
}
