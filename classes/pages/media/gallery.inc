<?php

    require_once("pages/admin/article/article_list.inc");
        
class gallery extends article_list
{
    
    var $width=220;
    
    function execute(){
        // Quick fix for idiotic XSS vulns.
        foreach ($_GET as $key => $value) {
          $_GET[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        }
    	$_GET["media_type_grouping_id"]=2;
        if (isset($_GET["location_id"]) && !isset($_GET["region_id"])) {
          $_GET['region_id'] = $_GET['location_id'];
        }
    	if (!isset($_GET["include_attachments"]) || $_GET["include_attachments"]=="")
    		$_GET["include_attachments"]=1;
    	if (array_key_exists("gallery_display_option_id",$_GET) && $_GET["gallery_display_option_id"]+0>1)
    		$this->width=130;
    	if (!isset($_GET["news_item_status_restriction"]) && empty($_GET["region_id"]) && empty($_GET["topic_id"]))
    		$_GET['news_item_status_restriction']=1155;
    	parent::execute();
    }

 	function render_row($cell_counter, $row, $parent_row, $tr, $article_renderer ,$article_cache_class, $display_options, $article_db_class){

                $parent_item_id=$row["parent_item_id"];
                $news_item_id=$row["news_item_id"];
                $news_item_type_id=$row["news_item_type_id"];
                $media_attachment_renderer= new MediaAttachmentRenderer();
                
                if ($parent_item_id!=$news_item_id && $parent_item_id!=0)
                {
                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($parent_row["news_item_id"], $parent_row["creation_timestamp"]);
                    $searchlink.="#".$news_item_id;
                } else
                {
                   
                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
                    $idlink=$row["news_item_id"];
                }

                
                $search=array("'\''","'<'","'>'");
                $replace=array("&#039;","&lt;","&gt;");
               
                
                $tblhtml="\r\n<a href=\"".$searchlink."\">";
                
                if ($_GET["gallery_display_option_id"]+0>1)
                	$tblhtml.=$media_attachment_renderer->render_small_thumbnail_from_news_item_info($row, "  border=\"0\" ");
                else{
                	$t=substr($row["title1"],0,35);
                	if (strlen($row["title1"])>35)
                		$t.="...";
                	$tblhtml.="<strong>".$t."</strong><br />";
                	$tblhtml.=$media_attachment_renderer->render_medium_thumbnail_from_news_item_info($row, "  border=\"0\" ");
                }
                $tblhtml.="</A>\r\n";
				
				
                if (is_int(($cell_counter)/$this->get_row_size()))
                	$tblhtml.="</td></tr><tr><td valign=\"top\" width=\"".$this->width."\">";
                else
                	$tblhtml.="</td><td valign=\"top\" width=\"".$this->width."\">";
        		return $tblhtml;
    }
   
    function get_search_template(){
    	return "article/gallery_search.tpl";
    }
   
   function get_news_item_status_select_list(){
   		$display_options=array();
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlighted-Local";
   		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlighted-NonLocal";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]
   			="Highlighted";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]
   			="All";
		return $display_options;
   }
   
   	//this is needed to override the ability for poeple to hack the site by
   	//posting to this page and having the inherited method get run
	function bulk_classify() {
		echo "Classifictions Requires Being Logged Into The Admin System!!";
	}
   
   function set_additional_values($search_param_array){
   		$article_renderer= new ArticleRenderer();
   		if (!array_key_exists("gallery_display_option_id",$_GET))
   			$_GET["gallery_display_option_id"]=0;
   		$gallery_display_options=array();
   		$gallery_display_options[0]="3x6 large images";
   		$gallery_display_options[1]="4x8 large images";
   		$gallery_display_options[2]="5x8 small images";
   		$gallery_display_options[3]="7x10 small images";
   		$search_param_array['TPL_GALLERY_DISPLAY_OPTIONS']=$article_renderer->make_select_form("gallery_display_option_id", $gallery_display_options, $_GET["gallery_display_option_id"]+0);
    	return $search_param_array;
    }
    
    function get_row_size(){
    	$ret=3;
    	if (!array_key_exists("gallery_display_option_id",$_GET))
    		$_GET["gallery_display_option_id"]=0;
    	if ($_GET["gallery_display_option_id"]+0==0){
    		$ret=3;
    	}else if ($_GET["gallery_display_option_id"]==1){
    		$ret=4;
    	}else if ($_GET["gallery_display_option_id"]==2){
    		$ret=5;
    	}else if ($_GET["gallery_display_option_id"]==3){
    		$ret=7;
    	}
    	return $ret;
    }
    
    function get_page_size(){
    	$ret=18;
    	if (!array_key_exists("gallery_display_option_id",$_GET) || $_GET["gallery_display_option_id"]+0==0){
    		$ret=18;
    	}else if ($_GET["gallery_display_option_id"]==1){
    		$ret=32;
    	}else if ($_GET["gallery_display_option_id"]==2){
    		$ret=40;
    	}else if ($_GET["gallery_display_option_id"]==3){
    		$ret=70;
    	}
    	return $ret;
    }
    
    
}

?>
