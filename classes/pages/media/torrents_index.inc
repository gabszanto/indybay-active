<?php

    require_once("pages/article/newswire.inc");
        
class torrents_index extends newswire
{
    
    var $width=220;
    
    function execute(){
    	$_GET['media_type_grouping_id'] = 11;
    	$_GET['news_item_status_restriction'] = 1155;
    	$_GET['include_attachments'] = 1;
    	if (!empty($_GET['gallery_display_option_id']))
    		$this->width = 130;
    	parent::execute();
    }
    
   function get_page_size(){
   	return 40;
   }

   
   
   	//this is needed to override the ability for poeple to hack the site by
   	//posting to this page and having the inherited method get run
	function bulk_classify($_POST){
		echo "Classifictions Requires Being Logged Into The Admin System!!";
	}
   
   
   
  	function render_row($cell_counter, $row, $parent_row, $tr, $article_renderer ,$article_cache_class, $display_options, $article_db_class){

                $parent_item_id = $row['parent_item_id'];
                $news_item_id = $row['news_item_id'];
                $news_item_type_id = $row['news_item_type_id'];
                $media_attachment_renderer= new MediaAttachmentRenderer();
                
                if ($parent_item_id!=$news_item_id && $parent_item_id!=0)
                {
                    $searchlink = $article_cache_class->get_web_cache_path_for_news_item_given_date($parent_row['news_item_id'], $parent_row['creation_timestamp']);
                    $searchlink .= "#" . $news_item_id;
                } else
                {
                   
                    $searchlink = $article_cache_class->get_web_cache_path_for_news_item_given_date($row['news_item_id'], $row['creation_timestamp']);
                    $idlink = $row['news_item_id'];
                }

                
                $search = array("'\''","'<'","'>'");
                $replace = array("&#039;","&lt;","&gt;");
               
            $media_attachment_db = new MediaAttachmentDB();
			$media_attachment_info = $media_attachment_db->get_media_attachment_info($row['media_attachment_id']);
	        
	        $file_name = $media_attachment_info['file_name'];
	    	$upload_name = $media_attachment_info['upload_name'];
	    	$relative_path = $media_attachment_info['relative_path'];
	    	$file_dir = UPLOAD_PATH . '/' . $media_attachment_info['relative_path'];
	    	$full_file_path = $file_dir . $file_name;
	    	$mime_type = $media_attachment_info['mime_type'];
	    	$relative_url = '/uploads/' . $relative_path.$file_name;
	    	$relative_torrent_url = '';
	    	if (file_exists($full_file_path)){
	    		$file_size = $media_attachment_renderer->render_file_size(filesize($full_file_path));
	    		$torrent_info = $media_attachment_db->get_associated_torrent_info($row['media_attachment_id']);
	             if (is_array($torrent_info) && sizeof($torrent_info)>0){
        			$torrent_file_name = $torrent_info['file_name'];
			    	$torrent_upload_name = $torrent_info['upload_name'];
			    	$relative_torrent_path = $torrent_info['relative_path'];
			    	$torrent_file_dir = UPLOAD_PATH . '/' . $torrent_info['relative_path'];
			    	$full_torrent_file_path = $torrent_file_dir . $torrent_file_name;
			    	$relative_torrent_url = '/uploads/' . $relative_torrent_path . $torrent_file_name;
			    	//echo "full_torrent_file_path=".$full_torrent_file_path;
			    	if (!file_exists($full_torrent_file_path)){
			    		$relative_torrent_url = '';
			    	}
	             }
	    	}else
	    		$file_size = '';
               if (!is_int($cell_counter/2))
                	$tblhtml = '<tr class="torrentodd"><td>';
                else
                	$tblhtml = '<tr class="torrenteven"><td>';
      	
                //$tblhtml.=$media_attachment_renderer->render_small_thumbnail_from_news_item_info($row, "  border=\"0\" ");

	    	
              $media_type_grouping_id = $row['media_type_grouping_id'];
              $tblhtml .= "<image ";
              	if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_AUDIO) $tblhtml.=' src="'.MEDIA_TYPE_GROUPING_AUDIO_ICON.'" ';
                if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_VIDEO) $tblhtml.=' src="'.MEDIA_TYPE_GROUPING_VIDEO_ICON.'" ';
                if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_DOCUMENT) $tblhtml.=' src="'.MEDIA_TYPE_GROUPING_DOCUMENT_ICON.'" ';
                if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_OTHER) $tblhtml.=' src="'.MEDIA_TYPE_GROUPING_OTHER_ICON.'" ';
              $tblhtml.="/> ";
                $tblhtml.=$mime_type."<br />(".$file_size.")</td><td><strong>" . Renderer::check_plain($file_name) . "</strong><br /><small>";
               $tblhtml .= Renderer::check_plain($row['title2']);
               $tblhtml.="</small></td><td>";

               	if($relative_torrent_url!=""){
               		$tblhtml.="<a href=\"";
              		$tblhtml.=$relative_torrent_url;
              		$tblhtml.="\">";
                 	$tblhtml.="download torrent</a>";
                 }else{
              		$tblhtml.="<small>torrent needs to be regenerated</small>";
              	   }
                $tblhtml.="</td><td>";
                $tblhtml.="<a href=\"".$searchlink."\">view related post</a>";
              $tblhtml.="</td><td>";
              if ($file_size!=""){
               	$tblhtml.="<a href=\"";
              	$tblhtml.=$relative_url;
              	$tblhtml.="\">";
              	$tblhtml.="download file</a>";
              }else{
              	$tblhtml.="<small>file needs to be regenerated</small>";
              }
              
              $tblhtml.="</td></tr>";


        		return $tblhtml;
    }
    
     function get_search_template(){
    	return "article/torrent_search.tpl";
    }
    
}
