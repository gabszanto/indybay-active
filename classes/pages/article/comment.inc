<?php

require_once("publish.inc");

class comment extends Publish
{

	function execute(){
		
		$article_db_class= new ArticleDB();
		$news_item_id=0;
		if (array_key_exists("top_id",$_GET))
			$news_item_id = (int) $_GET["top_id"];
		if ($news_item_id==0 && array_key_exists("parent_item_id",$_POST))
			$news_item_id = (int) $_POST["parent_item_id"];
		if ($news_item_id+0 !=0){
			$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
			if(is_array($article_info)){
				$this->tkeys['local_parent_title1'] = Renderer::check_plain($article_info["title1"]);
				$this->tkeys['local_parent_displayed_author_name'] = Renderer::check_plain($article_info["displayed_author_name"]);
				$this->tkeys['local_parent_summary'] = Renderer::html_purify($article_info["summary"]);
			}
		}
		
		if ($news_item_id+0==0 || !is_array($article_info) || sizeof($article_info)==0){
			echo "<h2>You have chosen to add a comment to a nonexistent article</h2>
				if you got here by clicking on 'add comment' click back and try again, otherwise you were probably given a bad link";
			exit;
		}
		parent::execute();
	}
	
	function get_child_news_item_type_id(){
		return NEWS_ITEM_TYPE_ID_COMMENT;
	}
	
    function update_latest_comments_pages(){
	        include(INDYBAY_BASE_PATH . '/classes/pages/comment_latest.inc');
                $lc = new comment_latest();

		    $comment_link  = $article_obj->article['article_url_path'];
		    $comment_link .= $article_obj->article['id'] . "_comment.php";
		    // set the latest comment page under a global varibale which defines which article display go into the latest comment page -- blicero
                  if (preg_match('/' . $article_obj->article['display'] . '/', $GLOBALS['latest'])) {
			    $lc->add_latest_comment(
                            $article_obj->article['numcomment'],
                            $article_obj->render_entities($article_obj->article['heading']),
                            $comment_link,
                            $article_obj->article['id'],
                            $article_obj->render_entities($article->article['author']),
                            $article->article['id']
                            );
		    }
		    }

} 
