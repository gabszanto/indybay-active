<?php

// Class for publish page
require_once("renderer/article_renderer_class.inc");
require_once("db/article_db_class.inc");
require_once("cache/article_cache_class.inc");
require_once("cache/dependent_list_cache_class.inc");
require_once("db/newswire_db_class.inc");
require_once("db/category_db_class.inc");
require_once("media_and_file_util/upload_util_class.inc");

class publish extends Page
{

	function execute()
	{

		$is_published = false;
		$preview=false;
		if (is_array($_POST))
		$posted_fields=$this->clean_posted_data($_POST);
			
		$comment = false;
		$counter_msg = "";

		//test connection
		$db_class = new DB;
		$db_class->get_connection();
		 
		$category_db_class = new CategoryDB;
		$article_db_class = new ArticleDB;
		$article_renderer_class= new ArticleRenderer();
		$this->tr = new Translate();
		$this->tr->create_translate_table('publish');

		// Set up the template variables for this page
		$this->tkeys['upload_max_filesize'] = round(min(ini_get('upload_max_filesize') + 0, ini_get('post_max_size') + 0)) . 'MB';
		$this->tkeys['post_max_size'] = round((ini_get('post_max_size')+0)) . 'MB';
		$this->tkeys['max_execution_time'] = ini_get('max_execution_time') / 60 / 60;

		$this->captcha = new Captcha();
		$this->tkeys['captcha_form'] = $this->captcha->make_form();

		$parent_item_id=0;
		$parent_formatted_info="";
		if (array_key_exists("top_id",$_GET) && $_GET["top_id"]+0>0){
			$parent_item_id = intval($_GET['top_id']);
		}
		if (array_key_exists("parent_item_id",$posted_fields) && $posted_fields["parent_item_id"]+0>0){
			$parent_item_id=$posted_fields["parent_item_id"]+0;
		}
		if ($parent_item_id!=0){
			$article_db_class=new ArticleDB;
			$article_info=$article_db_class->get_article_info_from_news_item_id($parent_item_id);
			$parent_formatted_info="<strong>Add A Comment On \"".$article_info["title1"]."\" by ".$article_info["displayed_author_name"]."<br/>";
			$parent_formatted_info.="<small>".substr($article_info["summary"],0,150)."</small><br/>";
		}
		$this->tkeys['local_parent_item_id'] = $parent_item_id;


		$this->tkeys['local_parent_info'] = $parent_formatted_info;

		$topic_id=0;
		if (array_key_exists("topic_id",$posted_fields)){
			$topic_id=$posted_fields["topic_id"];
		}else if (array_key_exists("page_id",$_GET) && $_GET["page_id"]!=""){
			$topic_id=$_GET["page_id"]+0;
		}

		$region_id=0;
		if (array_key_exists("region_id",$posted_fields)){
			$region_id=$posted_fields["region_id"];
		}else if (array_key_exists("page_id",$_GET) && $_GET["page_id"]!=""){
			$region_id=$_GET["page_id"]+0;
		}

		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0,1);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44,1);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_international_options, $cat_topic_options);
		$this->tkeys['cat_topic_select'] = $article_renderer_class->make_select_form("topic_id", $cat_topic_options, $topic_id, "Please Select");
		$cat_region_options=$category_db_class->get_category_info_list_by_type(1,0,1);
		$this->tkeys['cat_region_select'] = $article_renderer_class->make_select_form("region_id", $cat_region_options, $region_id, "Please Select");

                $this->tkeys['local_preview'] = '';
                if ($posted_fields['file_count'] + 0 == 0) {
                  if (empty($this->captcha->valid)) {
                    $this->tkeys['local_preview'] = 'You must preview your post before publishing.<br /><br />';
                  }
                  $this->tkeys['local_preview'] .= '<input type="submit" name="preview" value="Preview" />';
                }
			
		$form_vars = array("title1","displayed_author_name", "summary","related_url","text", "email", "address","phone",
        "event_duration");

		// Initialize template variables
		foreach ($form_vars as $fv)
		{
			if (array_key_exists($fv, $posted_fields) && strlen($posted_fields[$fv]) > 0)
			{
				$this->tkeys['local_'.$fv] = $posted_fields[$fv];
			} else
			{
				$this->tkeys['local_'.$fv] = '';
			}
		}

		$this->tkeys['local_checkbox_is_text_html'] = $article_renderer_class->make_boolean_checkbox_form("is_text_html", $posted_fields["is_text_html"]+0);
		$this->tkeys['local_checkbox_display_contact_info'] =
		$article_renderer_class->make_boolean_checkbox_form("display_contact_info",
		$posted_fields["display_contact_info"]);

		$this->generate_filebox_html($posted_fields);
		$this->tkeys['file_boxes'] = $this->filebox;

		// Set up the file number select list
		$max_num_uploads = array();

		if ($parent_item_id+0!=0)
		{
			$max_num_uploads = array(0 => 0, 1 => 1);
		} else if ($this->get_parent_news_item_type_id()==NEWS_ITEM_TYPE_ID_EVENT){
			$max_num_uploads = array(0 => 0, 1 => 1, 2 => 2, 3 => 3);
		}else{
			for ($x = 0; $x <= 20; $x++)
			{
				$max_num_uploads[$x] = $x;
			}
		}

		// Check if we are actually dealing with a comment
		if ($parent_item_id > 0)
		{
			$comment = true;
			$ctr = new Translate();
			$this->forced_template_file = 'comment.tpl';
			$this->tkeys['pub_stepone'] = $ctr->trans('comment_pub_one');
			$this->tkeys['pub_steptwo'] = $ctr->trans('comment_pub_two');
			$this->tkeys['pub_stepthree'] = $ctr->trans('comment_pub_three');
			$this->tkeys['pub_optional'] = $ctr->trans('optional');
			$this->tkeys['local_site_nick'] = $GLOBALS['site_nick'];
			$this->tkeys['pub_author'] = $ctr->trans('author');
			$this->tkeys['pub_url'] = $ctr->trans('link');
		}



		if ($posted_fields["publish"] != "")
		{
			 
			if ($posted_fields["is_text_html"])
			$posted_fields["is_summary_html"]=true;
			if ($this->validate_post($posted_fields))
			{
				$article_cache_class= new ArticleCache;
				$logdb= new LogDB();
				$logdb->add_log_entry_before_publish();


				$associated_attachment_ids_array=$this->process_uploads($posted_fields);

				//first check for duplicate and only save if not duplicate
				$is_duplicate =false;
				$news_item_id=$this->get_duplicate($posted_fields);
				if ($news_item_id+0==0){
					$news_item_id=$this->add_post_to_db($posted_fields, $associated_attachment_ids_array);
				}else{
					$is_duplicate =true;
				}
				 
				//set parent id for duplicate and nonduplicate cases
				$parent_item_id=$parent_item_id+0;
				if ($parent_item_id==0){
					$parent_item_id=$news_item_id;
				}
				 
				//proces categories, regen newswires and feeds and regen cache only if it is not a duplicate
				if ($news_item_id>0 && !$is_duplicate){
					 
					if ($parent_item_id==$news_item_id){
						$this->process_categories($news_item_id, $posted_fields);
						$this->update_syndication_wires($news_item_id, $posted_fields);
					}

					$article_renderer_class= new ArticleRenderer;
					$article_db_class= new ArticleDB;
						
					if ($parent_item_id==$news_item_id){
						$rendered_html=$article_cache_class->cache_everything_for_article($news_item_id);
						$dependent_list_cache_class= new DependentListCache();
						$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
						$dependent_list_cache_class->update_all_dependencies_on_add($article_info);

					}else{
						$rendered_html=$article_cache_class->cache_everything_for_comment($news_item_id, $parent_item_id);
					}
				}

				$is_published = true;

				//setup ids for duplicates
				$article_info="";
				if ($news_item_id>0 && $is_duplicate){
					$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
					$parent_item_id=$article_info["parent_item_id"];
					$article_renderer_class=  new ArticleRenderer();
					$rendered_html=$article_renderer_class->get_rendered_article_html($article_info);
				}
				 
				//post or calendar item
				if ($parent_item_id==$news_item_id)
				{
					$this->tkeys['local_publish_result'] = $this->tr->trans('publish_successful');
					if (!is_array($article_info))
					$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($news_item_id,$article_info["creation_timestamp"]);
				
				//comment
				} else if ($news_item_id>0){
					$this->tkeys['local_publish_result'] = $this->tr->trans('comment_successful');
					$article_info=$article_db_class->get_article_info_from_news_item_id($parent_item_id);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($parent_item_id,$article_info["creation_timestamp"])."?show_comments=1#".$news_item_id;

				//duplicate comment on a different post from original
				}else if ($news_item_id==-1){
					$this->tkeys['local_publish_result'] = "<font color=\"red\">Indybay's spam filter prevents posting of the exact same comment to more than one post.<br/> If you really need the exact same comment on this post, click back and make some minor change.</font>";
					$article_info=$article_db_class->get_article_info_from_news_item_id($_GET["top_id"]);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($_POST["parent_item_id"],$article_info["creation_timestamp"]);
				}

				$this->tkeys['LOCAL_DISPLAY_PREVIEW'] = isset($rendered_html) ? $rendered_html : '';
				$this->tkeys['LOCAL_PUBLISH_LINK']=SERVER_URL.$rel_link;

				$logdb->update_log_entry_after_publish();

				$this->forced_template_file = 'article/publish_success.tpl';

			}

			$Instructions = 0;

		} else{
			 
	  if ($posted_fields["preview"] != "") {
	  	$preview=1;
	  } else {
	  	if ($parent_item_id==0) $Instructions = 1;
	  }
		}

		if (!$is_published)
		{
			foreach ($form_vars as $fv)
			{
				$this->tkeys["local_$fv"] = htmlspecialchars($this->tkeys['local_'.$fv], ENT_QUOTES, 'UTF-8');
			}

			if (isset($Instructions))
			{
				$this->print_publish_instructions();
				 
			}

			if ($preview) {

				$this->validate_post($posted_fields);
				$article_renderer_class= new ArticleRenderer;
				$posted_fields["creation_date"]=strftime('%c');
				$posted_fields["created"]=strftime('%c');
				if ($parent_item_id+0!=0)
				$posted_fields['news_item_type_id']=$this->get_child_news_item_type_id();
				else
				$posted_fields["news_item_type_id"]=$this->get_parent_news_item_type_id();
				$posted_fields["is_preview"]=1;
				$this->tkeys['LOCAL_DISPLAY_PREVIEW']="<h3>Preview:</h3>".$article_renderer_class->get_rendered_article_html($posted_fields, true)."<br />";
			}
			else
			{
				if(!array_key_exists("LOCAL_DISPLAY_PREVIEW",$this->tkeys))
				$this->tkeys['LOCAL_DISPLAY_PREVIEW'] = "";
			}

		} else
		{
			//echo $article->cachestatus;
			//echo $article_obj->cachestatus;
		}

		$this->tkeys['select_filecount'] = $article_renderer_class->make_select_form("file_count", $max_num_uploads, $posted_fields['file_count']);


		$this->tkeys['hide1']="";
		$this->tkeys['hide2']="";
		if (array_key_exists('db_down',$GLOBALS) && $GLOBALS['db_down']=="1" && ($posted_fields["publish"] != "" || $posted_fields["preview"] != "")){
			$this->clear_validation_messages();
			$this->add_validation_message("The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.");
			$this->add_validation_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
		}else if (array_key_exists('db_down',$GLOBALS) && $GLOBALS['db_down']=="1"){
			$this->tkeys['hide1']="<!--";
			$this->add_status_message("The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.");
			$this->add_status_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
			$this->tkeys['hide2']="-->";
		}


	} // end function



	function generate_filebox_html($posted_fields)
	{

		$this->trr = new Translate();
		$this->trr->create_translate_table('publish');

		if ($posted_fields["publish"] == "" && $posted_fields['file_count'] > 0){

			if (!$this->validate_post($posted_fields)){
				$this->add_validation_message("Validation Must Be Successful To Choose Uploads");
				$posted_fields['file_count']=0;
			}
		}

		$fbox = "";
		$fbox = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" class=\"bgpenult\">\n";
		$fbox .= "<tr><td>";

		if ($posted_fields['file_count'] > 0)
		{
			$file_count = $posted_fields['file_count'];
		} else
		{
			$file_count = 0;
		}

		for ($counter = 0; $file_count >= $counter; $counter++)
		{

			if ($counter > 0)
			{
				if ($counter > 1) $fbox .= "<HR />\n";
				$fbox .= "<table width=\"100%\" border=\"0\" cellspacing=\"2\" cellpadding=\"4\"><tr>";
				$fbox .= "<td width=\"25%\" valign=\"top\"><b>";
				$fbox .= $this->trr->trans('select_file') . " #" . $counter . "</b></td>\n";
				$maxsize = round(min(ini_get('upload_max_filesize') + 0, ini_get('post_max_size') + 0)) * 1024 * 1024;
				$fbox .= "<td width=\"75%\">
                	  <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"".$maxsize."\" />
                	  <input type=\"file\" name=\"linked_file_" . $counter . "\"  /><br />";
				$fbox .= $this->trr->trans('upload_limit') . "</td></tr>\n";

				if ($counter > 1)
				{
					if (!array_key_exists('linked_file_title_'.$counter,$posted_fields)){
						$posted_fields['linked_file_title_'.$counter]="";
					}
					if (!array_key_exists('linked_file_comment_'.$counter,$posted_fields)){
						$posted_fields['linked_file_comment_'.$counter]="";
					}
					$fbox .= "<tr><td valign=\"top\" width=\"25%\"><b>" . $this->trr->trans('title') . " #" . $counter . "</b> <small>(";
					$fbox .= $this->trr->trans('required') . ")</small></td><td>";
					$fbox .= "<input size=\"25\" maxlength=\"90\" type=\"text\" name=\"linked_file_title_" . $counter;
					$fbox .= "\" value=\"" . $posted_fields['linked_file_title_'.$counter] . "\" />";
					$fbox .= "</td></tr><tr><td valign=\"top\" width=\"25%\">";
					$fbox .= $this->trr->trans('comment') . " #" . $counter . "</td>\n<td>";
					$fbox .= "<textarea name=\"linked_file_comment_" . $counter . "\" rows=\"3\" cols=\"50\" wrap=\"virtual\">";
					$fbox .= $posted_fields['linked_file_comment_'.$counter] . "</textarea></td></tr>\n";
				}

				$fbox .= "</table>\n";
			}
		}

		$fbox .= "</td></tr></table>\n";
		$this->filebox = $fbox;

	}

	function process_uploads($posted_fields){
		$associated_ids_array=array();
		if (is_array($_FILES)){
			for ($counter = 1; $posted_fields["file_count"] >= $counter; $counter++)
			{
				if ($counter>25){
					echo "Error!! file counter>25";
					exit;
				}
				$associated_attachment_ids=array();
				$has_file=isset($_FILES["linked_file_".$counter]);
				if ($has_file && is_array($_FILES["linked_file_".$counter])){
					if ($_FILES["linked_file_".$counter]["name"]!=""){

						$upload_util_class= new UploadUtil;
						$media_attachement_db_class=new MediaAttachmentDB;
						$tmp_file_path=$_FILES["linked_file_".$counter]["tmp_name"];
						$mime_type_from_post=$_FILES["linked_file_".$counter]["type"];
						$original_file_name=$_FILES["linked_file_".$counter]["name"];
						$file_info=$upload_util_class->process_anonymous_upload($tmp_file_path,$original_file_name,$mime_type_from_post);
						$image_name=$original_file_name;
						if ($counter!=1 && array_key_exists("linked_file_ext_".$counter,$posted_fields)){
							$alt_tag=$posted_fields["linked_file_ext_".$counter];
							if (sizeof($alt_tag)>20)
							$alt_tag=substr($alt_tag,20)."...";
						}
						if ($counter==1 && array_key_exists("summary",$posted_fields)){
							$alt_tag=$posted_fields["summary"];
							if (sizeof($alt_tag)>20)
							$alt_tag=substr($alt_tag,20)."...";
						}
						$media_attachment_id=$media_attachement_db_class->add_media_attachment($image_name,
						$file_info["file_name"], $file_info["relative_path"], $alt_tag,  $file_info["original_file_name"]
						,   $file_info["media_type_id"], 0,  UPLOAD_TYPE_POST,UPLOAD_STATUS_NOT_VALIDATED_ANONYMOUS);
						$associated_attachment_ids=$upload_util_class->make_secondary_files_and_associate($media_attachment_id, $file_info);
					}
				}

				$associated_ids_array[$counter]=$associated_attachment_ids;
			}
		}
		return $associated_ids_array;
	}

	function validate_post($posted_fields){
		$ret=1;
		$parent_item_id=0;

		if (empty($this->captcha->valid)) {
			$this->add_validation_message("Please answer the CAPTCHA (anti-spam questions) correctly.");
			$ret=0;
		}

		if (array_key_exists("parent_item_id",$posted_fields))
		$parent_item_id=$posted_fields["parent_item_id"]+0;
		 
		if (!isset($posted_fields["title1"]) || mb_strlen(trim($posted_fields["title1"]), 'UTF-8')<5){
			$this->add_validation_message("Title Was Required And It Must At Least Be 5 Characters Long");
			$ret=0;
		}
		if (!isset($posted_fields["displayed_author_name"]) || mb_strlen(trim($posted_fields["displayed_author_name"]), 'UTF-8')<1){
			$this->add_validation_message("Author Name Is Required");
			$ret=0;
		}
		if ($parent_item_id==0){
			if (!isset($posted_fields["summary"]) || mb_strlen(trim($posted_fields["summary"]), 'UTF-8')<3){
				$this->add_validation_message("Summary Is Required");
				$ret=0;
			}
		}
		 
		if ($parent_item_id!=0 && $posted_fields["file_count"]==0){
			if (!isset($posted_fields["text"]) || mb_strlen(trim($posted_fields["text"]), 'UTF-8')<10){
				$this->add_validation_message("Text Either Wasn't Entered Or Was Too Short");
				$ret=0;
			}
		}
		$spam_cache_class= new SpamCache();
		$spamvalidatelist=$spam_cache_class->load_validation_string_file();
		$spamvalidatelist=str_replace("\r","",$spamvalidatelist);
		$spamvalidate_array=explode("\n", $spamvalidatelist);
		foreach($spamvalidate_array as $spamstr){
			$spamstr=trim($spamstr);
			if ($spamstr=="")
			continue;
			if (strpos(' '.$posted_fields["summary"],$spamstr)>0){
				$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
				$ret=0;
			}
			if (strpos(' '.$posted_fields["text"],$spamstr)>0){
				$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
				$ret=0;
			}
		}
		 
		return $ret;
	}

	function clean_posted_data($posted_fields){
		$renderer= new Renderer();
		if (array_key_exists("title1",$posted_fields)){
			$posted_fields["title1"]=trim($posted_fields["title1"]);
			// Not sure why we want to do this and /e modifier is no more.
			// $posted_fields["title1"]=preg_replace('/[^\x09\x0A\x0D\x20-\xff]/e', '"&#".ord($0).";"', $posted_fields["title1"]);
		}else{
			$posted_fields["title1"]="";
		}
		 
		$repl=Array("\n", "\r", "     ", "   ", "  ", "  ");
		$replw=Array(" "," ", " ", " ", " ", " ");
		 
		$posted_fields["title1"]=str_replace($repl,$replw,$posted_fields["title1"]);
		 
		if (!array_key_exists("publish",$posted_fields))
		$posted_fields["publish"]=0;

		if (!array_key_exists("preview",$posted_fields))
		$posted_fields["preview"]=0;

		if (!array_key_exists("parent_item_id",$posted_fields))
		$posted_fields["parent_item_id"]=0;

		if (array_key_exists("displayed_author_name",$posted_fields))
		$posted_fields["displayed_author_name"]=strip_tags($posted_fields["displayed_author_name"]);
		else
		$posted_fields["displayed_author_name"]="";
		if (array_key_exists("related_url",$posted_fields))
		$posted_fields["related_url"]=strip_tags($posted_fields["related_url"]);
		else
		$posted_fields["related_url"]="";
		if (array_key_exists("email",$posted_fields))
		$posted_fields["email"]=strip_tags($posted_fields["email"]);
		else
		$posted_fields["email"]="";
		if (array_key_exists("phone",$posted_fields))
		$posted_fields["phone"]=strip_tags($posted_fields["phone"]);
		else
		$posted_fields["phone"]="";
		if (array_key_exists("address",$posted_fields))
		$posted_fields["address"]=strip_tags($posted_fields["address"]);
		else
		$posted_fields["address"]="";
		if (array_key_exists("summary",$posted_fields))
		$posted_fields["summary"]=$renderer->html_purify($posted_fields["summary"]);
		else
		$posted_fields["summary"]="";
		if (array_key_exists("text",$posted_fields))
		$posted_fields["text"]=$renderer->html_purify($posted_fields["text"]);
		else
		$posted_fields["text"]="";

		if (array_key_exists("is_text_html",$posted_fields))
		$posted_fields["is_text_html"]=$posted_fields["is_text_html"]+0;
		else
		$posted_fields["is_text_html"]=0;

		if (array_key_exists("display_contact_info",$posted_fields))
		$posted_fields["display_contact_info"]=$posted_fields["display_contact_info"]+0;
		else
		$posted_fields["display_contact_info"]=0;


		if (!array_key_exists("file_count",$posted_fields)){
			$posted_fields["file_count"]=0;


		}else{
			for ($counter = 2; $posted_fields["file_count"]+0 >= $counter; $counter++)
			{
				if ($counter>25){
					echo "Error!! file counter>25";
					exit;
				}
				if (array_key_exists("linked_file_title_".$counter,$posted_fields)){
					$posted_fields["linked_file_title_".$counter] =     strip_tags($posted_fields["linked_file_title_".$counter]);
					$posted_fields["linked_file_comment_".$counter] = isset($posted_fields["linked_file_comment_".$counter]) ? strip_tags($posted_fields["linked_file_comment_".$counter]) : '';
				}
			}
		}
		 
		return $posted_fields;

	}





	function add_post_to_db($posted_fields,$associated_ids_array){

		$article_db_class= new ArticleDB;
		if (isset($associated_ids_array[1]) && isset($associated_ids_array[1]["media_attachment_id"])){
			$posted_fields["media_attachment_id"]=$associated_ids_array[1]["media_attachment_id"];
			$posted_fields["media_type_grouping_id"]=$associated_ids_array[1]["media_type_grouping_id"];
			if (isset($associated_ids_array[1]["thumbnail_media_attachment_id"]))
			$posted_fields["thumbnail_media_attachment_id"]=$associated_ids_array[1]["thumbnail_media_attachment_id"];
			else
			$posted_fields["thumbnail_media_attachment_id"]=0;
		}
		 
		if (!array_key_exists("parent_item_id",$posted_fields) || $posted_fields["parent_item_id"]==0){
			$news_item_id=$article_db_class->create_new_article($this->get_parent_news_item_type_id(), $posted_fields);
		}else{
			$news_item_id=$article_db_class->create_new_article($this->get_child_news_item_type_id(), $posted_fields);
		}
		 
		for ($counter = 2; $posted_fields["file_count"] >= $counter; $counter++)
		{
			if ($counter>25){
				echo "Error!! file counter>25";
				exit;
			}
			$attachment_fields=array();
			if (array_key_exists("linked_file_title_".$counter,$posted_fields))
			$attachment_fields["title1"] =$posted_fields["linked_file_title_".$counter];
			else
			$attachment_fields["title1"] =     $posted_fields["title1"];

			if (array_key_exists("linked_file_comment_".$counter,$posted_fields))
			$attachment_fields["text"] =     $posted_fields["linked_file_comment_".$counter];
			else
			$attachment_fields["text"] ="";

			if (array_key_exists("displayed_author_name",$posted_fields))
			$attachment_fields["displayed_author_name"] =$posted_fields["displayed_author_name"];
			else
			$attachment_fields["displayed_author_name"]="";

			if (array_key_exists("related_url",$posted_fields))
			$attachment_fields["related_url"]=$posted_fields["related_url"];
			else
			$attachment_fields["related_url"]="";
			 
			$attachment_fields["parent_item_id"] =   $news_item_id;
			if (sizeof($associated_ids_array)>=$counter && is_array($associated_ids_array[$counter])){
				if (array_key_exists("media_attachment_id",$associated_ids_array[$counter]))
				$attachment_fields["media_attachment_id"]= $associated_ids_array[$counter]["media_attachment_id"];
				else
				$attachment_fields["media_attachment_id"]=0;
				if (array_key_exists("media_type_grouping_id",$associated_ids_array[$counter]))
				$attachment_fields["media_type_grouping_id"]=$associated_ids_array[$counter]["media_type_grouping_id"];
				else
				$attachment_fields["media_type_grouping_id"]=0;
				if (array_key_exists("thumbnail_media_attachment_id",$associated_ids_array[$counter]))
				$attachment_fields["thumbnail_media_attachment_id"]= $associated_ids_array[$counter]["thumbnail_media_attachment_id"];
				else
				$attachment_fields["thumbnail_media_attachment_id"]= 0;
			}
			if (trim($attachment_fields["title1"])!="" ||
			trim($attachment_fields["text"])!="" ||
			$attachment_fields["media_attachment_id"]+0>0){
				$next_child_id=$article_db_class->create_new_article( NEWS_ITEM_TYPE_ID_ATTACHMENT, $attachment_fields);
			}
		}
		$this->add_additional_info_for_publish_subtypes($news_item_id, $posted_fields);

		 
		return $news_item_id;
	}




	function process_categories($news_item_id, $posted_fields){
		$news_item_db_class= new NewsItemDB;
		if (array_key_exists("topic_id",$posted_fields)){
			$posted_topic_category=$posted_fields["topic_id"]+0;
			if ($posted_topic_category>0)
			$news_item_db_class->add_news_item_category($news_item_id,$posted_topic_category);
		}
		if (array_key_exists("region_id",$posted_fields)){
			$posted_region_category=$posted_fields["region_id"]+0;
			if ($posted_region_category>0)
			$news_item_db_class->add_news_item_category($news_item_id,$posted_region_category);
		}
	}

	 
	function get_child_news_item_type_id(){
		return NEWS_ITEM_TYPE_ID_COMMENT;
	}

	function get_parent_news_item_type_id(){
		return NEWS_ITEM_TYPE_ID_POST;
	}

	function update_syndication_wires($news_item_id, $posted_fields){
	}
	 
	function print_publish_instructions(){
		if (!array_key_exists("db_down",$GLOBALS) || $GLOBALS["db_down"]!="1"){
			$util=new FileUtil();
			$str=$util->load_file_as_string(INCLUDE_PATH . "/article/publish-instructions.inc");
			$str = str_replace('TPL_LOCAL_PAGE_ID', isset($_GET['page_id']) ? (int)$_GET['page_id'] : '', $str);
			echo $str;
		}
	}
	 
	function add_additional_info_for_publish_subtypes($news_item_id, $posted_fields){
	}

	function get_duplicate($posted_fields){
		if ($posted_fields["file_count"]>0)
		return 0;

		$article_db_class= new ArticleDB();
		$possible_dups=$article_db_class->find_all_recent_duplicate_nonhidden_versions_by_title($posted_fields["title1"]);
		 
		if (is_array($possible_dups) && sizeof($possible_dups)>0){
			foreach($possible_dups as $possible_dup){
				if ($possible_dup["summary"]==$posted_fields["summary"]
				&& $possible_dup["text"]==$posted_fields["text"]
				&& $possible_dup["displayed_author_name"]==$posted_fields["displayed_author_name"]
				&& $possible_dup["related_url"]==$posted_fields["related_url"]
				&& $possible_dup["email"]==$posted_fields["email"]
				&& $possible_dup["is_text_html"]+0==$posted_fields["is_text_html"]+0
				){
					if ($posted_fields["parent_item_id"]+0 !=0 && $possible_dup["parent_item_id"]+0!=$posted_fields["parent_item_id"]){
						return -1;
					}
					return $possible_dup["news_item_id"];
				}
			}
			return 0;
		}else{
			return 0;
		}
		 
	}

	 
} // end class
