<?php

require_once(CLASS_PATH.'/db/media_attachment_db_class.inc');
require_once('db/article_db_class.inc');
require_once('cache/article_cache_class.inc');
require_once('renderer/article_renderer_class.inc');
require_once(CLASS_PATH."/media_and_file_util/upload_util_class.inc");

class audio {

  function execute() {
    $article_db_class = new ArticleDB;
    $article_renderer_class = new ArticleRenderer;
    $article_cache_class = new ArticleCache;
    $media_attachment = new MediaAttachmentDB;
    $news_item_id = (int) basename($_SERVER['PHP_SELF']);
    $upload_util_class= new UploadUtil();
    $image_util= new ImageUtil();
    $news_item_version_id = $article_db_class->get_current_version_id($news_item_id);
    $version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
    $media_attachment_info = $media_attachment->get_media_attachment_info($version_info['media_attachment_id']);
    $file_name = $media_attachment_info["file_name"];
    $relative_path = $media_attachment_info["relative_path"];
    $relative_url = "/uploads/" . $relative_path.$file_name;
    $media .= '<audio width="300" height="28" preload="none" controls><source src="' . SERVER_URL . $relative_url . '"type="video/mp4" /><a class="audio" href="' . SERVER_URL . $relative_url . '">Download audio' . $thumbnail . $file_name . ' (' . $file_size . ')</a></audio>';
    print $media;
  }

}
