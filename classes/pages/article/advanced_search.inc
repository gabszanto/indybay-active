<?php

    require_once("pages/admin/article/article_list.inc");
        
class advanced_search extends Page
{
    function execute (){
    $tr = new Translate();
        $tr->create_translate_table('article');

        if (!isset($_GET['include_posts']) && !isset($_GET['submitted_search']))
    		$include_posts =1;
    	else
    		$include_posts = $_GET['include_posts'] + 0;
    		
    	if (isset($_GET['media_type_grouping_id']))	
    		$media_type_grouping_id = $_GET['media_type_grouping_id']+0;
    	else
    		$media_type_grouping_id=0;
    	
    	if (isset($_GET['include_comments']))	
    		$include_comments = $_GET['include_comments']+0; 
    	else
    		$include_comments=0;
    		
    	if (!isset($_GET['include_attachments']) && $media_type_grouping_id!=0 && !isset($_GET['submitted_search']))
    		$include_attachments=1;
    	else if (isset($_GET['include_attachments']))
    		$include_attachments = $_GET['include_attachments']+0;   
    	else
    		$include_attachments=0;
    		
    	if (!isset($_GET['include_events']) && !isset($_GET['submitted_search']))
    		$include_events =1;
    	else if (isset($_GET['include_events']))
    		$include_events = $_GET['include_events']+0; 
    	else
    		$include_events=0;
    		
    	if (isset($_GET['include_blurbs']))
    		$include_blurbs = $_GET['include_blurbs']+0; 
    	else
    		$include_blurbs = 0; 
    	
    	if (isset($_GET['topic_id']))
    		$topic_id = $_GET['topic_id']+0;
    	else
    		$topic_id=0;
    	
    	if (isset($_GET['region_id']))	
    		$region_id = $_GET['region_id']+0;
    	else
    		$region_id=0;
    		
    	if (isset($_GET['page_id']))	
    		$page_id=$_GET['page_id']+0;
        else
        	$page_id=0;
        
        if ($page_id>0){
    		$feature_page_db= new FeaturePageDB();
    		$topic_id=$feature_page_db->get_first_topic_for_page($page_id);
    		$region_id=$feature_page_db->get_first_region_for_page($page_id);
    	}
    	if (isset($_GET['news_item_status_restriction']) && $news_item_status_restriction!=0)
    		$news_item_status_restriction = $_GET['news_item_status_restriction'] + 0;
    	else
    		$news_item_status_restriction=690690;
    	
		$article_db_class= new ArticleDB;
		$media_attachment_db_class= new MediaAttachmentDB();
        $article_renderer = new ArticleRenderer();
        $article_cache_class= new ArticleCache;

        $category_db_class = new CategoryDB;
		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_topic_options,$cat_international_options);
     	$cat_topic_options[0]="All Topics";
     	$this->tkeys["CAT_TOPIC_SELECT"] = $article_renderer->make_select_form("topic_id", $cat_topic_options, $topic_id);        
		$cat_region_options[0]="All Regions";
		$cat_region_options=$this->array_merge_without_renumbering($cat_region_options,$category_db_class->get_category_info_list_by_type(1,0));
     	$this->tkeys["CAT_REGION_SELECT"] = $article_renderer->make_select_form("region_id", $cat_region_options, $region_id);        
        
        $display_options =  $this->get_news_item_status_select_list(); 
        $display_options[0] = "All Statuses";
        $medium_options = $media_attachment_db_class->get_medium_options();
        $medium_options[0]="All Media";
        
        if (isset($_GET["search"]))
        	$this->tkeys['LOCAL_SEARCH'] = $article_renderer->check_plain($_GET['search']);
        else
        	$this->tkeys["LOCAL_SEARCH"]= "";
        
        $this->tkeys["SEARCH_MEDIUM"]= $article_renderer->make_select_form("media_type_grouping_id", $medium_options, $media_type_grouping_id);
        
        $this->tkeys["LOCAL_CHECKBOX_COMMENTS"]= $article_renderer->make_boolean_checkbox_form("include_comments", $include_comments);
        $this->tkeys["LOCAL_CHECKBOX_ATTACHMENTS"]= $article_renderer->make_boolean_checkbox_form("include_attachments", $include_attachments);
        $this->tkeys["LOCAL_CHECKBOX_EVENTS"]= $article_renderer->make_boolean_checkbox_form("include_events", $include_events);
        $this->tkeys["LOCAL_CHECKBOX_POSTS"]= $article_renderer->make_boolean_checkbox_form("include_posts", $include_posts);
        $this->tkeys["LOCAL_CHECKBOX_BLURBS"]= $article_renderer->make_boolean_checkbox_form("include_blurbs", $include_blurbs);
        
        $this->tkeys["SEARCH_DISPLAY"]= $article_renderer->make_select_form("news_item_status_restriction", $display_options, $news_item_status_restriction);
        
        $date_options= array();
        $date_options[0]="Dont Restrict Dates";
        $date_options["creation_date"]="Restrict By Date Posted";
        $date_options["displayed_date"]="Restrict By Event Date";
        if (isset($_GET["search_date_type"]))
        	$search_type = $article_renderer->check_plain($_GET['search_date_type']);
        else
        	$search_type="";
        $this->tkeys["SEARCH_DATE_TYPE_SELECT"]= $article_renderer->make_select_form("search_date_type", $date_options, $search_type);
        
        $date_range_start_day=0;
        if (isset($_GET["date_range_start_day"]))
        	$date_range_start_day=$_GET["date_range_start_day"]+0;
        if ($date_range_start_day==0)
        	$date_range_start_day=strftime("%e");
        $date_range_start_month=0;
        if (isset($_GET["date_range_start_month"]))
        	$date_range_start_month=$_GET["date_range_start_month"]+0;
        if ($date_range_start_month==0)
        	$date_range_start_month=date("n");
        $date_range_start_year=0;
        if (isset($_GET["date_range_start_year"]))
        	$date_range_start_year=$_GET["date_range_start_year"]+0;
        if ($date_range_start_year==0)
        	$date_range_start_year=strftime("%Y");
        
        $date_range_end_day=0;
        if (isset($_GET["date_range_end_day"]))
        	$date_range_end_day=$_GET["date_range_end_day"]+0;
        if ($date_range_end_day==0)
        	$date_range_end_day=strftime("%e");
        $date_range_end_month=0;
        if (isset($_GET["date_range_end_month"]))
        	$date_range_end_month=$_GET["date_range_end_month"]+0;
        if ($date_range_end_month==0)
        	$date_range_end_month=date("n");
        $date_range_end_year=0;
        if (isset($_GET["date_range_end_year"]))
        	$date_range_end_year=$_GET["date_range_end_year"]+0;
        if ($date_range_end_year==0)
        	$date_range_end_year=strftime("%Y");
           
        $this->tkeys["date_between_start"]= $article_renderer->make_date_select_form("date_range_start",10,2, $date_range_start_day, $date_range_start_month, $date_range_start_year);
        
        $this->tkeys["date_between_end"]= $article_renderer->make_date_select_form("date_range_end",10,2, $date_range_end_day, $date_range_end_month, $date_range_end_year);
    
    }

   function get_news_item_status_select_list(){
   		$display_options=array();
   		$display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN]="All";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlighted-Local";
   		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlighted-NonLocal";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]
   			="Highlighted";
   		$display_options[NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]
   			="Other";
   		$display_options[NEWS_ITEM_STATUS_ID_HIDDEN*NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]
   			="Hidden";
		return $display_options;
   }
   
   	//this is needed to override the ability for poeple to hack the site by
   	//posting to this page and having the inherited method get run
	function bulk_classify() {
		echo "Classifictions Requires Being Logged Into The Admin System!!";
	}
   
   
}

?>
