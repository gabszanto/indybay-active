<?php

require_once(CLASS_PATH."/pages/article/newswire.inc");
require_once(CLASS_PATH."/db/media_attachment_db_class.inc");
     
class pda_search extends newswire
{
    
	function execute(){
		parent::execute();
		$types=array();
		
		$feature_page_db_class= new FeaturePageDB();
		$search_results="";
		
				
		if ($_GET['news_item_status_restriction']==3){
			$search_results.=" Local ";
		}
		
		if ($_GET['news_item_status_restriction']==5){
			$search_results.=" US/World ";
		}
		
		if ($this->tkeys['LOCAL_REGION_ID']+0!=0){
			$page_info=$feature_page_db_class->get_feature_page_info($this->tkeys['LOCAL_REGION_ID']);
			$search_results.=$page_info[long_display_name]." ";
		}
		if ($this->tkeys['LOCAL_TOPIC_ID']+0!=0){
			$page_info=$feature_page_db_class->get_feature_page_info($this->tkeys['LOCAL_TOPIC_ID']);
			$search_results.=$page_info[long_display_name]." ";
		}
		
		if ($this->tkeys['LOCAL_MEDIA_TYPE_GROUPING_ID']+0!=0){
			$media_attachment_db_class= new MediaAttachmentDB();
			$medium_options=$media_attachment_db_class->get_medium_options();
			$str=$medium_options[$this->tkeys['LOCAL_MEDIA_TYPE_GROUPING_ID']+0];
			$search_results.=$str." ";
		}
		
		if ($this->tkeys['local_include_events']==1){
			array_push($types, "Events");
		}
		if ($this->tkeys['local_include_posts']==1){
			array_push($types, "Posts");
		}
		if ($this->tkeys['local_include_attachments']==1){
			array_push($types, "Attachments");
		}
		if ($this->tkeys['local_include_blurbs']==1){
			array_push($types, "Center Column");
		}
		$ii=sizeof($types);
		$i=1;
		foreach ($types as $next_type){
			if ($ii==$i && $i>1){
				$search_results.=" and ";
			}
			if ($i<$ii && $ii>2 & $i>1){
				$search_results.=", ";
			}
			$search_results.=$next_type;
			$i=$i+1;
		}


		
		$this->tkeys['local_advanced_search_results_info']=$search_results;
	}
	
	
	
 	
    function get_search_template(){
    	return "article/hidden_search.tpl";
    }
   
   function get_news_item_status_select_list(){
   		$display_options=array();
   		$display_options[NEWS_ITEM_STATUS_ALL_NONHIDDEN]="All";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlighted-Local";
   		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlighted-NonLocal";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]
   			="Highlighted";
   		$display_options[NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]
   			="Other";
   		$display_options[NEWS_ITEM_STATUS_ID_HIDDEN*NEWS_ITEM_STATUS_ID_QUESTIONABLE_HIDDEN]
   			="Hidden";
		return $display_options;
   }
   
   	//this is needed to override the ability for poeple to hack the site by
   	//posting to this page and having the inherited method get run
	function bulk_classify() {
		echo "Classifictions Requires Being Logged Into The Admin System!!";
	}
   
   
}
