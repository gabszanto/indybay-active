<?php
// Class for publish page
require_once("pages/article/publish.inc");

class mpublish extends publish
{

	function execute()
	{

		$is_published = false;
	
		if (is_array($_POST)){
			//print_r($_POST);
			if ($_POST["title"]<>""){
				$_POST["title1"]=$_POST["title"];
			}
		if ($_POST["author"]<>""){
				$_POST["displayed_author_name"]=$_POST["author"];
			}
			$posted_fields=$this->clean_posted_data($_POST);
			//print_r($_POST);
			
		}
		$comment = false;
		//test connection
		$db_class = new DB;
		$db_class->get_connection();

		$this->tkeys['site_status'] = "UKNOWN STATUS";

		$topic_id=0;
		if (array_key_exists("topic_id",$posted_fields)){
			$topic_id=$posted_fields["topic_id"];
		}else if (array_key_exists("page_id",$_GET) && $_GET["page_id"]!=""){
			$topic_id=$_GET["page_id"]+0;
		}

		$region_id=0;
		if (array_key_exists("region_id",$posted_fields)){
			$region_id=$posted_fields["region_id"];
		}else if (array_key_exists("page_id",$_GET) && $_GET["page_id"]!=""){
			$region_id=$_GET["page_id"]+0;
		}


		if ($posted_fields["publish"] != "")
		{
			 
			if ($posted_fields["is_text_html"])
			$posted_fields["is_summary_html"]=true;
			if ($this->validate_post($posted_fields))
			{
				$article_cache_class= new ArticleCache;
				$logdb= new LogDB();
				$logdb->add_log_entry_before_publish();

				$associated_attachment_ids_array=$this->process_uploads($posted_fields);

				
				
    			$posted_fields=$this->clean_posted_data($posted_fields);
    			
    			//only process categories and update newswires if it is not a duplicate
    			$news_item_id=$this->get_duplicate($posted_fields);
	    		if ($news_item_id+0==0){
	    			$news_item_id=$this->add_post_to_db($posted_fields, $associated_attachment_ids_array);
	    			if ($news_item_id>0){
						$this->process_categories($news_item_id, $posted_fields);
						$this->update_syndication_wires($news_item_id, $posted_fields);
	    			}
	    		}
				
				$article_db_class= new ArticleDB;
				
				if ($news_item_id>0){
					$parent_item_id=$news_item_id;
					$article_renderer_class= new ArticleRenderer;
					$rendered_html=$article_cache_class->cache_everything_for_article($news_item_id);
					$dependent_list_cache_class= new DependentListCache();
					$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id);
					$dependent_list_cache_class->update_all_dependencies_on_add($article_info);

				}

				$is_published = true;

				//setup ids for duplicates
				$article_info="";
				$news_item_id2=$news_item_id;
				if ($news_item_id2<0 && $news_item_id2!=-1){
					$news_item_id2=$news_item_id2*-1;
					$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id2);
					$parent_item_id=$article_info["parent_item_id"];
					$article_renderer_class=  new ArticleRenderer();
					$rendered_html=$article_renderer_class->get_rendered_article_html($article_info);
				}
				 
				if ($parent_item_id==$news_item_id2)
				{
					$this->tkeys['local_publish_result'] = "SUCCESS";//$this->tr->trans('publish_successful');
					if (!is_array($article_info))
						$article_info=$article_db_class->get_article_info_from_news_item_id($news_item_id2);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($news_item_id2,$article_info["creation_timestamp"]);
					$this->forced_template_file = 'article/mpublish_success.tpl';
				} else if ($news_item_id2>0){
					$this->tkeys['local_publish_result'] = "SUCCESS";
					$article_info=$article_db_class->get_article_info_from_news_item_id($parent_item_id);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($parent_item_id,$article_info["creation_timestamp"])."?show_comments=1#".$news_item_id2;
					$this->forced_template_file = 'article/mpublish_success.tpl';
				}else {
					$this->tkeys['local_publish_result'] = "FAILURE";
					$article_info=$article_db_class->get_article_info_from_news_item_id($_GET["top_id"]);
					$rel_link=$article_cache_class->get_web_cache_path_for_news_item_given_date($_POST["parent_item_id"],$article_info["creation_timestamp"]);
				}
				 $this->tkeys['LOCAL_PUBLISH_LINK']=SERVER_URL.$rel_link;

				$logdb->update_log_entry_after_publish();
			}

			$Instructions = 0;
		}else{
			$this->tkeys['site_status'] = "VERIFIED";
		}
	if (array_key_exists('db_down',$GLOBALS) && $GLOBALS['db_down']=="1" && ($posted_fields["publish"] != "" || $posted_fields["preview"] != "")){
		$this->clear_validation_messages();
		$this->add_validation_message("The database running this site is busy due to a large number of people posting to the site at the same time. Please wait a few minutes and then try publishing again.");
		$this->add_validation_message("If you keep getting this message for more than an hour email indybay@lists.riseup.net so Indybay's technical support team can look into the problem");
	}

} // end function




function validate_post($posted_fields){
	$ret=1;
	$parent_item_id=0;


	//this is placeholder security during testing
	//first real version will probably have external file with list of valid values for this field
	if (!isset($posted_fields["application_key"]) ||$posted_fields["application_key"]!="at#Hqe_t342qtzz"){
		$this->add_validation_message("Invalid Application Key");
		$ret=0;
	}

	//there shuld be more validation of this field but for first releases it will
	//either be used to block or to act almost as a password if we have spam issues
	if (!isset($posted_fields["user_key"]) || mb_strlen(trim($posted_fields["user_key"]), 'UTF-8')<5){
		$this->add_validation_message("Invalid User Key");
		$ret=0;
	}
	 
	if (!isset($posted_fields["title1"]) || mb_strlen(trim($posted_fields["title1"]), 'UTF-8')<5){
		$this->add_validation_message("Title Was Required And It Must At Least Be 5 Characters Long");
		$ret=0;
	}
	if (!isset($posted_fields["displayed_author_name"]) || mb_strlen(trim($posted_fields["displayed_author_name"]), 'UTF-8')<1){
		$this->add_validation_message("Author Name Is Required");
		$ret=0;
	}
	if (!isset($posted_fields["summary"]) || mb_strlen(trim($posted_fields["summary"]), 'UTF-8')<3){
		$this->add_validation_message("Summary Is Required");
		$ret=0;
	}
	 
	if ($posted_fields["file_count"]==0){
		if (!isset($posted_fields["text"]) || mb_strlen(trim($posted_fields["text"]), 'UTF-8')<10){
			$this->add_validation_message("Text Either Wasn't Entered Or Was Too Short");
			$ret=0;
		}
	}
	$spam_cache_class= new SpamCache();
	$spamvalidatelist=$spam_cache_class->load_validation_string_file();
	$spamvalidatelist=str_replace("\r","",$spamvalidatelist);
	$spamvalidate_array=explode("\n", $spamvalidatelist);
	foreach($spamvalidate_array as $spamstr){
		$spamstr=trim($spamstr);
		if ($spamstr=="")
		continue;
		if (strpos(' '.$posted_fields["summary"],$spamstr)>0){
			$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
			$ret=0;
		}
		if (strpos(' '.$posted_fields["text"],$spamstr)>0){
			$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
			$ret=0;
		}
	}
	 
	return $ret;
    }
     


     
} // end class
