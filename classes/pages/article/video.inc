<?php

require_once(CLASS_PATH.'/db/media_attachment_db_class.inc');
require_once('db/article_db_class.inc');
require_once('cache/article_cache_class.inc');
require_once('renderer/article_renderer_class.inc');
require_once(CLASS_PATH."/media_and_file_util/upload_util_class.inc");

class video {

  function execute() {
    $article_db_class = new ArticleDB;
    $article_renderer_class = new ArticleRenderer;
    $article_cache_class = new ArticleCache;
    $media_attachment = new MediaAttachmentDB;
    $news_item_id = (int) basename($_SERVER['PHP_SELF']);
    $upload_util_class= new UploadUtil();
    $image_util= new ImageUtil();
    $news_item_version_id = $article_db_class->get_current_version_id($news_item_id);
    $version_info = $article_db_class->get_article_info_from_version_id($news_item_version_id);
    $media_attachment_info = $media_attachment->get_media_attachment_info($version_info['media_attachment_id']);
    $rendered_video_info = $media_attachment->get_renderered_video_info($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
    if (is_array($rendered_video_info)) {
      $rendered_video_file_name = $rendered_video_info['file_name'];
      $rendered_video_relative_path = $rendered_video_info['relative_path'];
      $rendered_video_file_dir = UPLOAD_PATH . '/' . $rendered_video_info['relative_path'];
      $rendered_video_url = SERVER_URL .'/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
      $file_name = $media_attachment_info["file_name"];
      $relative_path = $media_attachment_info["relative_path"];
      $relative_url = "/uploads/" . $relative_path.$file_name;
      $thumbnail = '<br /><span class="video-thumbnail"><img src="' . SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '" border="0" /></span><br />';
      $poster = SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
    }
    $media .= '<video width="100%" preload="none" poster="' . $poster . '" controls><source src="' . SERVER_URL . $relative_url . '"type="video/mp4" /><a class="video" href="' . SERVER_URL . $relative_url . '">Download video' . $thumbnail . $file_name . ' (' . $file_size . ')</a></video>';
    print $media;
  }

}
