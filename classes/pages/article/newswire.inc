<?php

    require_once("pages/admin/article/article_list.inc");
        
class newswire extends article_list
{
    

 	function render_row($cell_counter, $row, $parent_row, $tr, $article_renderer ,$article_cache_class, $display_options, $article_db_class){

                
                $parent_item_id=$row["parent_item_id"];
                $news_item_id=$row["news_item_id"];
                $news_item_type_id=$row["news_item_type_id"];
                $media_attachment_renderer= new MediaAttachmentRenderer();
                
                if ($parent_item_id!=$news_item_id && $parent_item_id!=0)
                {
                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($parent_row["news_item_id"], $parent_row["creation_timestamp"]);
                    $searchlink.="#".$news_item_id;
                } else
                {
                   
                    $searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
                    $idlink=$row["news_item_id"];
                }

                
                $search=array("'\''","'<'","'>'");
                $replace=array("&#039;","&lt;","&gt;");
                $title=preg_replace($search,$replace,$row["title2"]);
                if ($title=="")
                	$title=preg_replace($search,$replace,$row["title1"]);
                	
                if ($parent_item_id!=$news_item_id && $parent_item_id!=0){
                	$parent_title=preg_replace($search,$replace,$parent_row["title1"]);
                	if ($news_item_type_id==3)
                		$title.="<br />(Comment On: ".$parent_title.")";
                	if ($news_item_type_id==NEWS_ITEM_TYPE_ID_ATTACHMENT)
                		$title.="<br />(Attachment On: ".$parent_title.")";
                }
                $author=preg_replace($search,$replace,$row["displayed_author_name"]);
                $summary=trim(strip_tags($row["summary"]));
				if (strlen($summary)>500){
					$summary=substr($summary,0,497)."...";
				}
                if ($parent_item_id!=$news_item_id){
                    $summary=strip_tags(trim($row["text"]));
                }
				
                $media_type_grouping_id = $row["media_type_grouping_id"];
                
                if ($media_type_grouping_id ==0) $mime='text" src="'.MEDIA_TYPE_GROUPING_TEXT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_TEXT) $mime='text" src="'.MEDIA_TYPE_GROUPING_TEXT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_IMAGE) $mime='image" src="'.MEDIA_TYPE_GROUPING_IMAGE_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_AUDIO) $mime='audio" src="'.MEDIA_TYPE_GROUPING_AUDIO_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_VIDEO) $mime='video" src="'.MEDIA_TYPE_GROUPING_VIDEO_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_DOCUMENT) $mime='document" src="'.MEDIA_TYPE_GROUPING_DOCUMENT_ICON.'" ';
                if ($media_type_grouping_id ==MEDIA_TYPE_GROUPING_OTHER) $mime='other" src="'.MEDIA_TYPE_GROUPING_OTHER_ICON.'" ';
                
                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT) $mime='calendar" src="'.MEDIA_TYPE_GROUPING_CALENDAR_ICON.'" ';
                
        
                if (($news_item_type_id==NEWS_ITEM_TYPE_ID_ATTACHMENT || $news_item_type_id==NEWS_ITEM_TYPE_ID_COMMENT)
                		&& $media_type_grouping_id <MEDIA_TYPE_GROUPING_IMAGE)
                	 $mime='image" src="'.COMMENT_ICON.'" ';
                 
                $tblhtml="<div ";
                if (!is_int($cell_counter/2))

                $tblhtml.="class=\"bgsearchgrey\"";
                $tblhtml.=">";

                if ($row["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN) $tblhtml.="<strike>";

                $tblhtml.="<a  href=\"".$searchlink."\"><img alt=\"".$mime."\" align=\"middle\" border=\"0\" /></a> 
                    <b><a href=\"".$searchlink."\">".$title."</a></b> ";
                    
                if ($media_type_grouping_id==MEDIA_TYPE_GROUPING_IMAGE || $media_type_grouping_id==MEDIA_TYPE_GROUPING_DOCUMENT){
                	$tblhtml.="<a href=\"".$searchlink."\">";
                	$tblhtml.=$media_attachment_renderer->render_small_thumbnail_from_news_item_info($row, " style=\"float: left \"  border=\"0\" ");
                	$tblhtml.="</A>";
                }else if ($media_type_grouping_id!=0 && $media_type_grouping_id!=1){
                	$media_attachment_db=new MediaAttachmentDB();
					$media_attachment_info=$media_attachment_db->get_media_attachment_info($row["media_attachment_id"]);

			        $file_name=$media_attachment_info["file_name"];
			    	$upload_name=$media_attachment_info["upload_name"];
			    	$relative_path=$media_attachment_info["relative_path"];
			    	$file_dir=UPLOAD_PATH."/".$media_attachment_info["relative_path"];
			    	$full_file_path=$file_dir.$file_name;
			    	$mime_type=$media_attachment_info["mime_type"];
			    	$relative_url="/uploads/" . $relative_path.$file_name;
			    	$file_size=0;
			    	if (file_exists($full_file_path)){
	    				$file_size=$media_attachment_renderer->render_file_size(filesize($full_file_path));
                	}
                	$tblhtml.="(".$mime_type." ".$file_size.")";
                	
                }
                    
                if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_BLURB)
                	    $author=SHORT_SITE_NAME;
                $tblhtml.=" - by ".$author;
                    
                    if ($row["news_item_status_id"]==NEWS_ITEM_STATUS_ID_HIDDEN) $tblhtml.="</strike>";
                    
                    $tblhtml.="<div >".$summary;
                    		
               $tblhtml.="</div>";
              // if ($media_type_grouping_id==MEDIA_TYPE_GROUPING_IMAGE)
              // 	 $tblhtml.="<br clear=\"all\"/>";
               if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT)
              		$tblhtml.="<div style=\"clear:left; font-style: italic\" >Event Date:".strftime("%a %b %e, %Y %l:%M%p",$row["displayed_timestamp"])."</div>";
               if ($row["news_item_type_id"]!=NEWS_ITEM_TYPE_ID_EVENT || $GLOBALS['browser_type']!='pda')
               		$tblhtml.="<div style=\"clear:left; font-style: italic\" >Posted: ".strftime("%a %b %e, %Y %l:%M%p",$row["creation_timestamp"])."</div>";

        		
                $tblhtml.="</div>";

                
        		return $tblhtml;
    }
   
    function get_search_template(){
    	return "article/newswire_search.tpl";
    }
   
   function get_news_item_status_select_list(){
  
                
   		$display_options=array();
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED]="Highlighted-Local";
   		$display_options[NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]="Highlighted-NonLocal";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED]
   			="Highlighted (Local & NonLocal)";
   		$display_options[NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NEW*
			NEWS_ITEM_STATUS_ID_LOCAL_CORPORATE_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_CORPORATE_HIGHLIGHTED*
			NEWS_ITEM_STATUS_ID_OTHER*NEWS_ITEM_STATUS_ID_QUESTIONABLE_NOTHIDDEN]
   			="All";
		return $display_options;
   }
   
   	//this is needed to override the ability for poeple to hack the site by
   	//posting to this page and having the inherited method get run
	function bulk_classify() {
		echo "Classifictions Requires Being Logged Into The Admin System!!";
	}
   
   
}
