<?php

// Class for network page
require_once("cache/article_cache_class.inc");
class missing extends Page
{
	function execute(){


		$article_cache_class= new ArticleCache;
		$article_renderer_class= new ArticleRenderer;
		$article_db_class= new ArticleDB;
		$relative_url = parse_url($_SERVER['REDIRECT_URL'], PHP_URL_PATH);
		$old_query_string="";
		if (array_key_exists("REDIRECT_QUERY_STRING",$_SERVER))
			$old_query_string=$_SERVER["REDIRECT_QUERY_STRING"];
		
		$i=strpos($relative_url, "newsitems/");
		$i2=strpos($relative_url, "news/");
		$i3=strpos($relative_url, "archives/");
		$i4=strpos($relative_url, "calendar/");
		$iaudio = strpos($relative_url, 'audio.rss');
		$ivideo = strpos($relative_url, 'video.rss');
		$i5=strpos($relative_url, ".rss");
		$i6=strpos($relative_url, ".rdf");
		$i7=strpos(" ".$relative_url, "/uploads/");
		$i8=strpos(" ".$relative_url, "/im/");
		$i9=strpos(" ".$relative_url, "/images/");
		$i10=strpos(" ".$relative_url, "/imcenter/");
                $i11 = strpos($relative_url, "/display.php");
                $i12 = strpos($relative_url, "/print.php");
		if ($i>0){
			$j=strripos($relative_url,"/");
			$k=strripos($relative_url,".");
			if ($k>$j){
				
				$news_item_id=substr($relative_url,$j+1,($k-$j)-1);
				
				$news_item_info=$article_db_class->get_news_item_info($news_item_id);
				if (!is_array($news_item_info) || sizeof($news_item_info)==0)
					return;
				$relative_path=$article_renderer_class->get_relative_web_path_from_item_info($news_item_info);
				
				if ($relative_path!=$relative_url){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL.$relative_path);
					exit;
				}
				$article_cache_class->cache_everything_for_article($news_item_id);
				header("HTTP/1.0 200 OK");				
				include(WEB_PATH.$relative_url);
				exit;
			}
		}else if ($i2>0){
			$j=strripos($relative_url,"/");
			$k=strripos($relative_url,".");
			if ($k>$j){
				$old_id=substr($relative_url,$j+1,($k-$j)-1);
				if ($old_id+0>0){
					$news_item_id=($old_id*10)+1;
					$new_relative_url=$article_cache_class->get_relative_url_for_article($news_item_id);
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL.$new_relative_url);
					exit;
				}
			}
		
		
		}else if ($i3>0){
			$j=strpos($old_query_string,"?id=");
			$k=strpos($old_query_string,"&");
			if ($k>$j){
				$old_id=substr($old_query_string,$j+3,$k-$j-3);
			}else{
				$old_id=substr($old_query_string,$j+4);
			}
			if ($old_id+0>0){
				$article_cache_class= new ArticleCache;
				$news_item_id=($old_id*10)+2;
				$new_relative_url=$article_cache_class->get_relative_url_for_article($news_item_id);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$new_relative_url);
				exit;
			}
		}else if ($i4>0){
			$j=strpos($old_query_string,"?event_id=");
			$k=strpos($old_query_string,"&");
			if ($k>$j){
				$old_id=substr($old_query_string,$j+9,$k-$j-9);
			}else{
				$old_id=substr($old_query_string,$j+9);
			}
			if ($old_id+0>0){
				$article_cache_class= new ArticleCache;
				$news_item_id=($old_id*10)+3;
					//		echo $old_id;
				//echo "/".$news_item_id;
				$new_relative_url=$article_cache_class->get_relative_url_for_article($news_item_id);

				//exit;
				//include(WEB_PATH.$new_relative_url);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$new_relative_url);
				exit;
			}
                }
                else if ($iaudio > 0) {
                  header('HTTP/1.0 301 Moved Permanently');
                  header('Location: ' . SERVER_URL . '/syn/generate_rss.php?media_type_grouping_id=3&news_item_status_restriction=1155');
                  exit;
                }
                else if ($ivideo > 0) {
                  header('HTTP/1.0 301 Moved Permanently');
                  header('Location: ' . SERVER_URL . '/syn/generate_rss.php?media_type_grouping_id=4&news_item_status_restriction=1155');
                  exit;
		}else if ($i6>0 ){
				if (strpos(" ".$relative_url, "santacruz")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?page_id=60&include_posts=0&include_events=0&include_blurbs=1&use_long=1");
					exit;
				}else if (strpos(" ".$relative_url, "newswire")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=1&include_posts=1&include_blurbs=0&include_posts=0");
					exit;
				}else if (strpos(" ".$relative_url, "audio")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=1&include_posts=1&include_attachments=1&media_type_grouping_id=3");
					exit;
				}else{
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=1&include_posts=0&include_blurbs=1&include_posts=0");
					exit;
				}
		}else if ($i5>0){
				if (strpos(" ".$relative_url, "santacruz")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?page_id=60&include_posts=0&include_events=0&include_blurbs=1&rss_version=1&use_long=1");
					exit;
				}else if (strpos(" ".$relative_url, "newswire")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=2&include_posts=1&include_blurbs=0");
					exit;
				}else if (strpos(" ".$relative_url, "audio")>0){
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=2&include_posts=1&include_attachments=1&media_type_grouping_id=3");
					exit;
				}else{
					header("HTTP/1.0 301 Moved Permanently");
					header("Location: ".SERVER_URL."/syn/generate_rss.php?rss_version=2&include_posts=0&include_blurbs=1&include_posts=0");
					exit;
				}
		}else if ($i7>0){
				$relative_url=str_replace("uploads","olduploads",$relative_url);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$relative_url);
				exit;
		}else if ($i8>0){
				$relative_url=str_replace("/im/","/oldim/",$relative_url);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$relative_url);
				exit;
		}else if ($i9 == 1){
				$relative_url=str_replace("/images/","/oldimages/",$relative_url);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$relative_url);
				exit;
		}else if ($i10>0){
				$relative_url=str_replace("/imcenter/","/oldimcenter/",$relative_url);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$relative_url);
				exit;
		}else if ($i11 === 0 || $i12 === 0){
			if (substr($old_query_string, 3)) {
				$article_cache_class = new ArticleCache;
				$news_item_id = (substr($old_query_string, 3) * 10) + 1;
				$new_relative_url = $article_cache_class->get_relative_url_for_article($news_item_id);
				header("HTTP/1.0 301 Moved Permanently");
				header("Location: ".SERVER_URL.$new_relative_url);
				exit;
			}
		}
		
	}

}
