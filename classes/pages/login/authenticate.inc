<?php

include(CLASS_PATH . '/db/user_db_class.inc');
        
// Class for authenticate page

class authenticate extends Page
{

    function authenticate()
    {
        // In this kind of script, we can do everything in the constructor to avoid the whole mess



        $user_obj = new UserDB;
        $user_id=$user_obj->authenticate($_POST['username1'], $_POST['password']);
        if ($user_id+0>0)
        {
			ini_set('session.save_path', SESSIONS_PATH);
            $username = $_POST['username1'];
			if (!isset($_SESSION))
            	session_start();
            $_SESSION['session_username']  = $_POST['username1'];
            $_SESSION['session_user_id']   = $user_id;
            $_SESSION['session_is_editor'] = true;
			$_SESSION['session_last_activity_time'] = time();
			
            session_write_close();

			//if($_SESSION['secure'] !== "yes") { $goto = "/admin/user/user_display_edit.php?user_id1=".$_SESSION['user_id'] ; }
            
            if ($_POST['goto']!="" 
            && strpos($_POST['goto'], "admin")+0>0
            	&& ($_POST['goto']!="/admin/")
            	&& ($_POST['goto']!="//admin/")
             && $_POST['goto'] && strpos( $_POST['goto'], "authentication")+0==0
            ) { 
            	$goto=$_POST['goto']; 
				if (strpos(" ".$goto, "/")==1){
					$goto=substr($goto, 1);
				}
            }else { 
            	if ($GLOBALS['browser_type']!='pda'){
            		$goto="admin/feature_page/feature_page_list.php";
            	}else{
            		$goto="/admin/article/?include_posts=1&include_events=1&include_comments=1&news_item_status_restriction=0&news_item_status_restriction=874&pda=true";
            	}
		    }
            header("Location: ".ADMIN_ROOT_URL.$goto);
            exit;
        }else
        {
            $goto=$_POST['goto'];
            $goto=urlencode($goto);
            header("Location: ".ADMIN_ROOT_URL."/admin/authentication/authenticate_display_logon.php?logon_failed=true&goto=$goto");
            exit;
        }
    }

    function execute() {
        // Execution method, does nothing
        return 1;
    }

}

?>
