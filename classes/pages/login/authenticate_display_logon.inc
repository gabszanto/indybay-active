<?php
require_once(INDYBAY_BASE_PATH . '/classes/db/db_class.inc');
// Class for authenticate_display_logon page

class authenticate_display_logon extends Page {

    function authenticate_display_logon() {
        // Class constructor, does nothing
        return 1;
    }

    function execute() {
		 
        $tr = new Translate();

		//test connection
		$db_class = new DB;
        $db_class->get_connection();
       

        if (array_key_exists('db_down',$GLOBALS) && $GLOBALS['db_down']=="1"){
        	$this->tkeys['local_error'] = "<p class=\"error\"><strong>The DB Seems to be overloaded (possibly due to a spam attack or other such problem), please try to connect again in a few minutes</strong></p>";
        }else if (array_key_exists('logon_failed',$_GET)) {
            $this->tkeys['local_error'] = "<p class=\"error\"><strong>" . $tr->trans('logon_failed') . "</strong></p>";
        } else {
            $this->tkeys['local_error'] = "";
        }
		if (array_key_exists('username1',$_POST)){
			$username = $_POST['username1'];
		}else if (array_key_exists('session_username',$_SESSION)){
			$username = $_SESSION['session_username'];
		}else{
			$username="";
		}
        $this->tkeys['sitenick'] = $GLOBALS['site_nick'];
        $renderer_class = new Renderer;
	$this->tkeys['local_username'] = $renderer_class->check_plain($username);
        $this->tkeys['goto'] = $renderer_class->check_plain($_GET['goto']);

        return 1;
    }

}

?>
