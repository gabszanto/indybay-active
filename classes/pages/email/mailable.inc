<?php
/**
 *  page to let you email an article to someone.
 *  @package	sf-active
 *  @subpackage	articles
 */
class mailable extends Page {

    /**
     *	Class constructor, does nothing
     */
    function mailable () {
        return 1;
    }

    /**
     *	let you email an article to someone
     */
    function execute () {
	
		
		// Translate all TPL_ in the Template
		$tr = new Translate();
		$tr->create_translate_table('mailable');
		$this->translation($tr);

// set defauilt values for variables 
		if (isset($_POST['fromaddress'])) { $this->tkeys['fromvalue']=htmlspecialchars($_POST['fromaddress']); } else { $this->tkeys['fromvalue']=""; }
		if (isset($_POST['toaddress'])) { $this->tkeys['tovalue']=htmlspecialchars($_POST['toaddress']); } else { $this->tkeys['tovalue']=""; }
		if (isset($_POST['subject'])) { $this->tkeys['subjectvalue']=htmlspecialchars($_POST['subject']); } else { $this->tkeys['subjectvalue']=""; }
		if (isset($_POST['commentmail'])) { $this->tkeys['commentmail_value']=htmlspecialchars($_POST['commentmail']); } else { $this->tkeys['commentmail_value']=""; }
		if (isset($_POST['id'])) { $this->tkeys['idvalue']=htmlspecialchars($_POST['id']); } elseif (isset($_GET['id'])) { $this->tkeys['idvalue']=htmlspecialchars($_GET['id']); } else { $this->tkeys['idvalue']=""; }
		if ($_GET['comments']=="yes") { $this->tkeys['commentsvalue']="yes"; } else { $this->tkeys['commentsvalue']=""; }

// check for mistakes
		if (isset($_POST['email_it'])) {
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@([0-9a-z](-?[0-9a-z])*\.)+[a-z]{2}([zmuvtg]|fo|me)?$/i", $_POST[fromaddress])) {
                  $error_msg .= $tr->trans('from_is_not_mail') . "<br>";
                }
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@([0-9a-z](-?[0-9a-z])*\.)+[a-z]{2}([zmuvtg]|fo|me)?$/i", $_POST[toaddress])) {
                  $error_msg .= $tr->trans('to_is_not_mail') . "<br>";
                }
		if (strlen($_POST[subject]) > 60) {  $error_msg.=$tr->trans('subject_is_too_long')."<br>"; $this->tkeys[subjectvalue]=substr($_POST[subject],0,30);}
		if (strlen($_POST[subject]) < 1 ) {  $error_msg.=$tr->trans('subject_is_null')."<br>"; }
		if (strlen($_POST[commentmail]) > 300) {  $error_msg.=$tr->trans('comment_is_too_long')."<br>"; $this->tkeys[commentmailvalue]=substr($_POST[commentmail],0,300);}
		}
// set error_msg value
		if (isset($error_msg)) { $this->tkeys[error_msg]=$error_msg; } else { $this->tkeys[error_msg]=""; }		

// if there is no error then send the mail 	
		if ( (isset($_POST['email_it'])) && (!isset($error_msg)) ) {
			$from="From: ".$_POST['fromaddress'];
			$to=$_POST['toaddress'];
			$subject=$_POST['subject'];
                        
// create the message 
			$article = new Article ($_GET['id']);
                        $article->render();
                        $article->set_paths();
	
			$body.=$_POST['commentmail']."\n\n";
                        if ($article->article['display'] == 'f') { print 'Sorry, error mailing hidden article.'; exit; }
			$body.=$tr->trans('prn_original_article').$article->article['article_url']."\n";
			if (strlen($article->article['linked_file'])>1)
				{
					$body.=$tr->trans('original_upload').$article->article['fileurl'];
					$body.=" (".$article->article['filesize'].")\n\n";
				} else {
					$body.="\n\n";
				}
			$body.=$article->article['heading']; // ."\n";
			if (substr($article->article['heading'],-1,1)!="\n") $body.="\n";
			$body.=$article->article['author'];
			if (strlen($article->article['contact'])>1)
				{
					$body.=" - ".$article->article['contact']."\n";
				}
			if (strlen($article->article['address'])>1)
				{
					$body.=$article->article['address']." ";
				}
			if (strlen($article->article['phone'])>1)
				{
					$body.=$article->article['phone'];
				}
			if (strlen($article->article['address'])>1 || strlen($article->article['phone']!="")>1)
				{
					$body.="\n";
				}
			$body.= ' ' . $article->article['format_created']."\n\n";
			$body .= str_ireplace('<br />', "\n", $article->article['summary']) . "\n\n";
			$body .= str_ireplace('<br />', "\n", $article->article['article']) . "\n\n";
			$body.=$article->article['link']."\n\n\n";

			if ( ( $_GET['comments'] == "yes" ) || ($_POST['comments'] == "yes" ) ) {
		
			if ( $article->article['numcomment'] > 0 ) 
				{
					$body.=$tr->trans('plural_comment').":\n\n";
					$article->set_comments_data("asc",0);
					if (is_array($article->comments))
					{
					    while($article_fields = array_pop($article->comments))
					    {
						$comment = new Article($article_fields["id"]);
						$body.=$comment->article['heading'];
						if (substr($comment->article['heading'],-1,1)!="\n") $body.="\n";
						$body.=$comment->article['author'];
						if (strlen($comment->article['contact'])>1)
						    {
							$body.=" - ".$comment->article['contact']."\n";
						    }
						if (strlen($comment->article['address'])>1)
						    {
							$body.=$comment->article['address']." ";
						    }
							if (strlen($comment->article['phone'])>1)
						    {
							$body.=$comment->article['phone'];
						    }
							if (strlen($comment->article['address'])>1 || strlen($comment->article['phone'])>1)
						    {
							$body.="\n";
						    }
						$body.= ' ' . $comment->article['format_created']."\n";
						if (strlen($comment->article['linked_file'])>1)
						    {
							$body.=$tr->trans('original_upload').$comment->article['fileurl'];
							$body.=" (".$comment->article['filesize'].")\n\n";
						    } else {
							$body.="\n\n";
						    }
						$body .= str_ireplace('<br />', "\n", $comment->article['article']) . "\n\n";
						$body.=$article->article['link']."\n\n\n";
					    }
					}

				} else {
				
					$body .= $tr->trans('prn_no_comments');
					
				}
			}

			$body=strip_tags($body,"<a><i><b><u><br><p><h1><h2><h3><h4><h5><h6><hr>");
//			$body=eregi_replace("<a href=\"",$tr->trans("link").": ",$body);
//			$body=eregi_replace("\">"," >",$body);
//			$body=eregi_replace("</a>","< ",$body);

//			$body=eregi_replace('<A .*HREF=("|\')?([^ "\']*)("|\')?.*>([^<]*)</A>', '[\\4] ('.$tr->trans("link").': \\2)',$body);

//			$body=eregi_replace('(<a [^<]*href=["|\']?([^"\']*)["|\']?[^>].*>([^<]*)</a>)','[\\3] ('.$tr->trans("link").': \\2)', $body);

			$body = str_ireplace('<i>', '/', $body);
			$body = str_ireplace('</i>', '/', $body); 
			$body = str_ireplace('<b>', '*', $body);
			$body = str_ireplace('</b>', '*', $body);
			$body = str_ireplace('<u>',' _', $body);
			$body = str_ireplace('</u>', '_', $body);
			$body = str_ireplace('<br>', "\n", $body);
			$body = str_ireplace('<p>', "\n\n\t", $body);
			$body = str_ireplace('</p>', "\n\n", $body);
			$body = str_ireplace('<hr>', "\n----\n", $body);
			$body = preg_replace('/<h?>/i', "\n\n", $body);
			$body = preg_replace('#</h?>#i', "\n\n", $body);
                        $body=str_replace('&quot;','"',$body);
			$body = preg_replace('#(<a [^<]*href=["|\']?([^"\']*)["|\']?[^>].*>([^<]*)</a>)#i', '[\\3] ('.$tr->trans("link").': \\2)', $body);


			$body.="\n\n".$tr->trans('notice').ROOT_URL."/process/disclaimer.php - ".ROOT_URL."/process/privacy.php";

		// send the mail
			mail($to,$subject,$body,$from);	

		header("Location: ".$article->article['article_url']);
		}	

		
		if ( $_GET['id'] > 0 ) 
		{

			$article = new Article ($_GET['id']);
			$article->render();
			$article->set_paths();

			$this->tkeys['PRN_ARTICLE_HTML'] = $article->html_article;
			$this->tkeys['PRN_ARTICLE_URL'] = $article->article['article_url'];
			$this->tkeys['PRN_TITLE'] = $article->article['heading']." : ".$GLOBALS['site_nick'];

			//$PHP_SELF."?id=".$_GET['id'];	
			//$PHP_SELF."?id=".$_GET['id']."&comments=yes";

			if ( $_GET['comments'] == "yes" ) 
			{
				
				$this->tkeys ['PRN_COMMENT_LINK'] = $PHPSELF."?id=".urlencode($_GET['id']);
				$this->tkeys ['PRN_COMMENT_TEXT'] = $tr->trans('prn_hide_comments');
				
				if ( $article->article['numcomment'] > 0 ) 
				{
					$article->render_all_comments();
					$this->tkeys ['PRN_COMMENTS_HTML'] = $article->html_comments;
					
				} else {
				
					$this->tkeys ['PRN_COMMENTS_HTML'] = $tr->trans('prn_no_comments');
					
				}
	
			} else {
	
				$this->tkeys ['PRN_COMMENT_LINK'] = $PHPSELF."?id=".urlencode($_GET['id'])."&comments=yes";
				$this->tkeys ['PRN_COMMENT_TEXT'] = $tr->trans('prn_print_comments');
				$this->tkeys ['PRN_COMMENTS_HTML'] = "";
				
	
			}

			
		} else {
			$this->tkeys ['PRN_COMMENTS_HTML'] = "";
			$this->tkeys['PRN_TITLE'] = "";
			$this->tkeys['PRN_ARTICLE_URL'] = "";
			$this->tkeys ['PRN_COMMENT_LINK'] = "";
			$this->tkeys ['PRN_COMMENT_TEXT'] = "";
			$this->tkeys ['PRN_ARTICLE_HTML'] = $tr->trans('prn_no_id');
		}
		
		return 1;		
	}

	/**
	 *  assigns static strings to the template variables.
	 *  @param  object  $tr
	 */
	function translation ($tr) {
		$this->tkeys['LANG_PRN_ORGINAL_ARTICLE'] = $tr->trans('prn_orginal_article');
		$this->tkeys['LANG_PRN_SITE_NAME'] = $GLOBALS['site_name'];
	}

}
