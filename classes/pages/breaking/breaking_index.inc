<?php

require_once(CLASS_PATH."/db/breaking_db_class.inc");


class breaking_index extends Page {
    function breaking_index() {
        return 1;
    }
    function execute() {
        $tr = new Translate();
        $tr->create_translate_table('breaking');
        $breaking_obj = new BreakingDB;

$this->tkeys['breaking_nav'] = '';

    if ($_GET['year']) $this->tkeys['breaking_nav'] .= ' : <a href="/breaking/?year=' . 
htmlspecialchars($_GET[year]) . '">' .
                           htmlspecialchars($_GET[year]) . '</a>';
    if ($_GET['month']) $this->tkeys['breaking_nav'] .= '-<a href="/breaking/?year=' . htmlspecialchars($_GET[year]) . 
 '&amp;month=' . htmlspecialchars($_GET[month]) . '">' . htmlspecialchars($_GET[month]) . '</a>';
     if ($_GET['day']) $this->tkeys['breaking_nav'] .= '-<a href="/breaking/?year=' . htmlspecialchars($_GET[year]) . 
 '&amp;month=' . htmlspecialchars($_GET[month]) . '&amp;day=' . htmlspecialchars($_GET[day]) . '">' . 
 htmlspecialchars($_GET[day]) . '</a>';

    $this->tkeys['display_breaking'] = 
$breaking_obj->display_breaking($_GET['year'],$_GET['month'],$_GET['day'],$_GET['id'],$_GET['date'],$_GET['range']);

    if ($_GET['year']) $date = htmlspecialchars($_GET['year']);
    if ($_GET['month']) $date .= '-' . htmlspecialchars($_GET['month']);
    if ($_GET['day']) $date .= '-' . htmlspecialchars($_GET['day']);
    if (!$GLOBALS['page_title']) $GLOBALS['page_title']='Breaking News ' . $date;

        return 1;
    }
}
?>
