<?php
require_once (CLASS_PATH."/renderer/event_renderer_class.inc");
require_once (CLASS_PATH."/db/category_db_class.inc");
require_once (CLASS_PATH."/db/feature_page_db_class.inc");
require_once (CLASS_PATH."/cache/event_cache_class.inc");
// Class for the Calender Main Page...

class event_week extends Page {

    function execute () {
        // Quick fix for idiotic XSS vulns.
        foreach ($_GET as $key => $value) {
          $_GET[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        }
    	if (array_key_exists("page_id",$_GET))
			$page_id=$_GET["page_id"]+0;
		else
			$page_id=0;
		$topic_id=0;
		$region_id=0;
		if ($page_id>0){
			$feature_page_db= new FeaturePageDB();
			$topic_id=$feature_page_db->get_first_topic_for_page($page_id);
			$region_id=$feature_page_db->get_first_region_for_page($page_id);
		}else{
			if (isset($_GET["topic_id"]))
				$topic_id=$_GET["topic_id"];
			if (isset($_GET["region_id"]))
				$region_id=$_GET["region_id"];
		}

		$GLOBALS['page_title']=$GLOBALS['dict']['calendar'];
		$event_renderer= new EventRenderer();
		
		$date= new Date();
		$date->set_date_to_now();
		if (!array_key_exists('day',$_GET))
			$_GET['day']="";
		if (!array_key_exists('month',$_GET))
			$_GET['month']="";
		if (!array_key_exists('year',$_GET))
			$_GET['year']="";
		if ( strlen($_GET['day'] ) > 0 ) $date->set_time( 0, 0, $_GET['day'], $_GET['month'], $_GET['year'] );
		$datenow = $date->clonedate();
		$datenow->find_start_of_week();
		$date->find_start_of_week();

		$this->tkeys['local_day']=$date->get_day();
		$this->tkeys['local_month']=$date->get_month();
		$this->tkeys['local_year']=$date->get_year();
		$nextweek = $date->clonedate();
		if ($nextweek->get_year() < (date("Y")+3)) $nextweek->move_n_weeks_forward(1);
		$nextweek_day = $nextweek->get_day();
		$nextweek_month = $nextweek->get_month();
		$nextweek_year = $nextweek->get_year();

		$this->tkeys['local_next_week_day'] = $nextweek_day;
		$this->tkeys['local_next_week_month'] = $nextweek_month;
		$this->tkeys['local_next_week_year'] = $nextweek_year;		
		
		$lastweek = $date->clonedate();
		if (!array_key_exists('site_start_year',$GLOBALS))
			$GLOBALS['site_start_year']=date("Y")-3;
		if ($lastweek->get_year() > $GLOBALS['site_start_year']-1) 
			$lastweek->move_n_weeks_forward(-1);
		$lastweek_day = $lastweek->get_day();
		$lastweek_month = $lastweek->get_month();
		$lastweek_year = $lastweek->get_year();

		$this->tkeys['local_last_week_day'] = $lastweek_day;
		$this->tkeys['local_last_week_month'] = $lastweek_month;
		$this->tkeys['local_last_week_year'] = $lastweek_year;

		$this->tkeys['local_topic_id']=$topic_id;

		$this->tkeys['local_region_id']=$region_id;
		
		if (isset($_GET['news_item_status_restriction']))
			$news_item_status_restriction= intval($_GET['news_item_status_restriction']);
		else
			$news_item_status_restriction=0;
		// Short Month View
                $this->tkeys['local_news_item_status_restriction'] = $news_item_status_restriction;

		$this->tkeys['CAL_EVENT_MONTH_VIEW_PREV'] = $event_renderer->render_short_month_view_prev($date, $datenow, $topic_id, $region_id, $news_item_status_restriction);
		
		$this->tkeys['CAL_EVENT_MONTH_VIEW_NEXT'] = $event_renderer->render_short_month_view_next($date, $datenow, $topic_id, $region_id, $news_item_status_restriction);

		// Topic Drop Down
        $category_db_class = new CategoryDB;
		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_international_options, $cat_topic_options);

     	$this->tkeys['CAL_EVENT_TOPIC_DROPDOWN'] = $event_renderer->make_select_form("topic_id", $cat_topic_options, $topic_id, "All");        
		$cat_region_options=$category_db_class->get_category_info_list_by_type(1,0);
     	$this->tkeys['CAL_EVENT_LOCATION_DROPDOWN'] = $event_renderer->make_select_form("region_id", $cat_region_options, $region_id, "All");        

        $category_db_class = new CategoryDB;
		$cat_topic_options=$category_db_class->get_category_info_list_by_type(2,0);
		$cat_international_options=$category_db_class->get_category_info_list_by_type(2,44);
		$cat_topic_options=$this->array_merge_without_renumbering($cat_topic_options,$cat_international_options);
		$this->tkeys['CAL_CUR_DATE'] = date("m/d/Y",$date->get_unix_time());

		$this->tkeys['CAL_DAY'] = $_GET['day'];
		$this->tkeys['CAL_MONTH'] = $_GET['month'];
		$this->tkeys['CAL_YEAR'] = $_GET['year'];		

		// Daytitles
		$tmpday = 0;
		$daytitles ="";
		while ( $tmpday < 7 )
		{
			$daytitles .='<td nowrap="nowrap" class="weekTitles">';
			$daytitles .= strftime ("%a %b %d", $date->get_unix_time());
			$daytitles .='</td>';
			$date->move_forward_n_days(1);
			$tmpday++;
		}

		$this->tkeys['CAL_EVENT_MONTH_DAYTITLE'] = $daytitles;

		// Event Week Render
		$date->move_forward_n_days(-7);
			
		$newdate = $date->clonedate();
		$newdate->find_start_of_week();

		$event_cache_class= new EventCache();
		$html_for_week=$event_cache_class->get_cached_week(
			$date, $topic_id, $region_id, $news_item_status_restriction);

		$this->tkeys['CAL_EVENT_MONTH_VIEW_FULL']=$html_for_week;

		return 1;		
	}
}
