<?php
require_once(CLASS_PATH."/pages/article/publish.inc");
require_once(CLASS_PATH."/db/calendar_db_class.inc");
require_once(CLASS_PATH."/cache/event_cache_class.inc");
require_once(CLASS_PATH."/cache/news_item_cache_class.inc");

class ical_single_item extends Page {
	
	function execute(){
	}
	
	function ical_single_item(){
		$news_item_id = intval($_GET['news_item_id']);
		$article_db_class = new ArticleDB();
                $news_item_cache_class = new NewsItemCache();
		if ($article_info = $article_db_class->get_article_info_from_news_item_id($news_item_id)) {
                  $article_info["text"] .= "\n ". SERVER_URL . $news_item_cache_class->get_web_cache_path_for_news_item_given_date($article_info['news_item_id'], $article_info['creation_timestamp']);
                  $this->tkeys['local_title']=$article_info["title1"];
		  $this->tkeys['local_summary'] = wordwrap(str_replace(array("\r\n", "\n"), "\\n", strip_tags($article_info["summary"])), 75, " \r\n ");
		  $this->tkeys['local_description'] = wordwrap(str_replace(array("\r\n", "\n"), "\\n", strip_tags($article_info["text"])), 75, " \r\n ");
		  $this->tkeys['local_news_item_id']=$news_item_id;
		  $this->tkeys['local_news_item_version_id']=$article_info['news_item_version_id'];
		  $created_timestamp=$article_info["creation_timestamp"];
		  $start_timestamp=$article_info["displayed_timestamp"];
		  $duration=$article_info["event_duration"];
		  //echo "test st was \"".$start_time_stamp."\"";
		  $end_timestamp=$start_timestamp+($duration*60*60);
		  $this->tkeys['local_email']='';
		  $this->tkeys['local_ical_created']=gmdate('Ymd',$created_timestamp)."T".gmdate('Hi00',$created_timestamp)."Z";
		  $this->tkeys['local_ical_start']=gmdate('Ymd',$start_timestamp)."T".gmdate('Hi00',$start_timestamp)."Z";
		  $this->tkeys['local_ical_end']=gmdate('Ymd',$end_timestamp)."T".gmdate('Hi00',$end_timestamp)."Z";
                  $this->tkeys['local_url'] = SERVER_URL . $news_item_cache_class->get_web_cache_path_for_news_item_given_date($article_info['news_item_id'], $article_info['creation_timestamp']);
                }
                else {
                  header('HTTP/1.1 404 Not Found');
                }
	}

}
