<?php
// $Id$

/**
 * @file
 * Generate an icalendar event feed.
 */

require_once(CLASS_PATH .'/pages/article/publish.inc');
require_once(CLASS_PATH .'/db/calendar_db_class.inc');
require_once(CLASS_PATH .'/db/search_db_class.inc');
require_once(CLASS_PATH .'/db/feature_page_db_class.inc');
require_once(CLASS_PATH .'/cache/event_cache_class.inc');
require_once(CLASS_PATH .'/cache/news_item_cache_class.inc');

class ical_feed extends Page {
	
  function execute(){
  }

  function ical_feed() {
    $feature_page_db_class = new FeaturePageDB;
    $search_db_class = new SearchDB;
    $news_item_cache_class = new NewsItemCache;
    $topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : '0';
    $region_id = isset($_GET['region_id']) ? intval($_GET['region_id']) : '0';
    $news_item_status_restriction = isset($_GET['news_item_status_restriction']) ? intval($_GET['news_item_status_restriction']) : 1365;
    $calendar_name = SHORT_SITE_NAME;
    $pages = array();
    if ($topic_id) {
      $pages[] = $feature_page_db_class->get_feature_page_info($topic_id);
    } 
    if ($region_id) {
      $pages[] = $feature_page_db_class->get_feature_page_info($region_id);
    } 
    foreach ($pages as $page) {
      $calendar_name .= ' - '. $page['long_display_name'];
    }
    $this->tkeys['local_calendar_name'] = $calendar_name;
    $date_range_start = new Date();
    $date_range_start->set_date_to_now();
    $date_range_end = new Date();
    $date_range_end->set_date_to_now();
    $date_range_end->move_forward_n_days(365);
    $search_result = $search_db_class->standard_search(180, 0, NULL, $news_item_status_restriction, 0, 0, 0, 1, 0, NULL, $topic_id, $region_id, 0 , 'displayed_date', $date_range_start, $date_range_end);
    $this->tkeys['local_events'] = '';
    foreach ($search_result as $article_info) {
      $event = array();
      $event['url'] = SERVER_URL . $news_item_cache_class->get_web_cache_path_for_news_item_given_date($article_info['news_item_id'], $article_info['creation_timestamp']);
      $article_info["text"] .= "\n". $event['url'];
      $event['summary'] = wordwrap(str_replace(array("\r\n", "\n"), "\\n", strip_tags($article_info["summary"])), 75, " \r\n ");
      $event['description'] = wordwrap(str_replace(array("\r\n", "\n"), "\\n", strip_tags($article_info["text"])), 75, " \r\n ");
      $event['title'] = wordwrap($article_info["title1"], 65, " \r\n ");
      $event['news_item_id'] = $article_info['news_item_id'];
      $event['news_item_version_id'] = $article_info['news_item_version_id'];
      $created_timestamp = $article_info["creation_timestamp"];
      $start_timestamp = $article_info["displayed_timestamp"];
      $duration = $article_info["event_duration"];
      $end_timestamp = $start_timestamp+($duration*60*60);
      $event['email'] = '';
      $event['ical_created'] = gmdate('Ymd',$created_timestamp)."T".gmdate('Hi00',$created_timestamp)."Z";
      $event['ical_start'] = gmdate('Ymd',$start_timestamp)."T".gmdate('Hi00',$start_timestamp)."Z";
      $event['ical_end'] = gmdate('Ymd',$end_timestamp)."T".gmdate('Hi00',$end_timestamp)."Z";
      $template_obj = new FastTemplate(TEMPLATE_PATH);
      $template_name = "pages/calendar/ical_feed_event.tpl";
      $template_obj->clear_all();
      $template_obj->define(array("ical_feed_event" => $template_name));
      $temp_array = array();
      foreach ($event as $key => $value){
        $temp_array["TPL_LOCAL_".strtoupper($key)] = $value;
      }
      $template_obj->assign($temp_array);
      $template_obj->parse("CONTENT", "ical_feed_event");
      $this->tkeys['local_events'] .= $template_obj->fetch("CONTENT");
    }
  }
}
