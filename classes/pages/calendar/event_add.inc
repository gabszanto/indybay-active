<?php
require_once(CLASS_PATH."/pages/article/publish.inc");
require_once(CLASS_PATH."/db/calendar_db_class.inc");
require_once(CLASS_PATH."/cache/event_cache_class.inc");

class event_add extends publish {
	
	
	function event_add(){
		
		if (isset($_POST["displayed_date_day"]) && $_POST["displayed_date_day"]+0>0){
			$day=$_POST["displayed_date_day"];
		}else{
			$day=date("d");
		}
		
		if (isset($_POST["displayed_date_month"]) && $_POST["displayed_date_month"]+0>0){
			$month=$_POST["displayed_date_month"];
		}else{
			$month = date('m');
		}
		
		if (isset($_POST["displayed_date_year"]) && $_POST["displayed_date_year"]+0>0){
			$year=$_POST["displayed_date_year"];
		}else{
			$year=date("Y");
		}
		
		if (isset($_POST["displayed_date_hour"]) && $_POST["displayed_date_hour"]+0>0){
			$hour=$_POST["displayed_date_hour"];
		}else{
			$hour=12;
		}
		
		if (isset($_POST["displayed_date_minute"]) && $_POST["displayed_date_minute"]+0>0){
			$minute=$_POST["displayed_date_minute"];
		}else{
			$minute=0;
		}
		$ampm="24Hour";
		
		 $renderer= new Renderer;
		$this->tkeys['cal_select_date_displayed_date'] = $renderer->make_datetime_select_form_24hour(
		"displayed_date",0,1, $day, $month ,$year,
		$hour,$minute);
		
		if (isset($_POST["event_type_id"])  && $_POST["event_type_id"]+0>0){
			$event_type_id=$_POST["event_type_id"];
		}else{
			$event_type_id=0;
		}
		
		if (isset($_POST["event_duration"])  && $_POST["event_duration"]+0>0){
			$event_duration=$_POST["event_duration"];
		}else{
			$event_duration=0;
		}
		
		$calendar_db_class= new CalendarDB();
		$keyword_list=$calendar_db_class->get_event_type_for_select_list();
		$this->tkeys['cat_type_select'] = $renderer->make_select_form(
		"event_type_id",$keyword_list,
		$event_type_id);
		$duration_list=array();
		$duration_list["0"]="0:00";
		$duration_list["0.25"]="0:15";
		$duration_list["0.5"]="0:30";
		$duration_list["0.75"]="0:45";
		$duration_list["1"]="1:00";
		$duration_list["1.5"]="1:30";
		$duration_list["2"]="2:00";
		$duration_list["2.5"]="2:30";
		$duration_list["3"]="3:00";
		$duration_list["4"]="4:00";
		$duration_list["5"]="5:00";
		$duration_list["6"]="6:00";
		$duration_list["7"]="7:00";
		$duration_list["8"]="8:00";
		$duration_list["9"]="9:00";
		$duration_list["10"]="10:00";
		$duration_list["11"]="11:00";
		$duration_list["12"]="12:00";
		$duration_list['24'] = '24:00';
		$this->tkeys['local_select_duration'] = $renderer->make_select_form(
		"event_duration",$duration_list,
		$event_duration);
	}
	
	
	
	
	function get_parent_news_item_type_id(){
		return NEWS_ITEM_TYPE_ID_EVENT;
	}
	
	
	
	
	function get_child_news_item_type_id(){
		return NEWS_ITEM_TYPE_ID_COMMENT;
	}





     function print_publish_instructions(){
     	if (!isset($GLOBALS['db_down']) || $GLOBALS['db_down']!="1"){
	  		include(INCLUDE_PATH . "/article/event-publish-instructions.inc");
	  	}
	  }
	  
	  
	  
	  
	  
	 function validate_post($posted_fields){
    	$ret=1;
        if (empty($this->captcha->valid)) {
          $this->add_validation_message("Please answer the CAPTCHA (anti-spam questions) correctly.");
          $ret=0;
        }
        if (mktime($posted_fields['displayed_date_hour'], $posted_fields['displayed_date_minute'], 0, $posted_fields['displayed_date_month'], $posted_fields['displayed_date_day'], $posted_fields['displayed_date_year']) < $_SERVER['REQUEST_TIME']) {
          $this->add_validation_message('A future event date is required.');
          $ret = 0;
        }
    	if (!isset($posted_fields["title1"]) || mb_strlen(trim($posted_fields["title1"]), 'UTF-8')<5){
    		$this->add_validation_message("Title Was Required And It Must At Least Be 5 Characters Long");
    		$ret=0;
    	}
    	if (!isset($posted_fields["summary"]) || mb_strlen($posted_fields["summary"], 'UTF-8')<1){
    			$this->add_validation_message("Location Details Is Required");
    			$ret=0;
    	}
    	$posted_fields["event_duration"]=trim($posted_fields["event_duration"]);
    	if (strrpos($posted_fields["event_duration"],".")>0){
    		$posted_fields["event_duration"]=substr($posted_fields["event_duration"],0,strrpos($posted_fields["event_duration"],"."));
    	}
    	if (strlen($posted_fields["event_duration"])<1 || ($posted_fields["event_duration"]+0)." "!=$posted_fields["event_duration"]." "){
    			$this->add_validation_message("Event Duration Is Required And Must Be Entered As A Number in Decimal Format (i.e. 1.5 is an hour and a half)");
    			$ret=0;
    	}

    	$spam_cache_class= new SpamCache();
    	$spamvalidatelist=$spam_cache_class->load_validation_string_file();
		$spamvalidatelist=str_replace("\r","",$spamvalidatelist);
		$spamvalidate_array=explode("\n", $spamvalidatelist);

		foreach($spamvalidate_array as $spamstr){
			$spamstr=trim($spamstr);
			if ($spamstr=="")
				continue;
			if (strpos(' '.$posted_fields["summary"],$spamstr)>0){
				$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
				$ret=0;
			}
			if (strpos(' '.$posted_fields["text"],$spamstr)>0){
				$this->add_validation_message("Indybay's Spam Filter Doesnt Allow The String \"".$spamstr."\" To Be In The Summary or Text Of Posts");
				$ret=0;
			}
		}
    	return $ret;
    }
    
    
    
    
     function add_additional_info_for_publish_subtypes($news_item_id, $posted_fields){
	 	$event_type_id=$_POST["event_type_id"];
	 	$calendar_db_class= new CalendarDB;
	 	$calendar_db_class->set_or_change_event_type($news_item_id,$event_type_id); 
	}
	
	
	
	 function get_duplicate($posted_fields){
	 	$article_db_class= new ArticleDB();
	 	$possible_dups=$article_db_class->find_all_recent_duplicate_nonhidden_versions_by_title($posted_fields["title1"]);
	 	
	 	if (is_array($possible_dups) && sizeof($possible_dups)>0){

	 		foreach($possible_dups as $possible_dup){
	 		
	 			if (!isset($posted_fields["is_summary_html"]))
	 				$posted_fields["is_summary_html"]=0;
	 			if ($possible_dup["summary"]==$posted_fields["summary"]
		 			&& $possible_dup["text"]==$posted_fields["text"]
		 			&& $possible_dup["displayed_author_name"]==$posted_fields["displayed_author_name"]
		 			&& $possible_dup["related_url"]==$posted_fields["related_url"]
		 			&& $possible_dup["email"]==$posted_fields["email"]
					&& $possible_dup["is_text_html"]+0==$posted_fields["is_text_html"]+0
					&& $possible_dup["is_summary_html"]+0==$posted_fields["is_summary_html"]+0
					&& $possible_dup["event_duration"]+0==$posted_fields["event_duration"]+0
					&& date("Y",$possible_dup["displayed_timestamp"])+0==$posted_fields["displayed_date_year"]+0
					&& date("d",$possible_dup["displayed_timestamp"])+0==$posted_fields["displayed_date_day"]+0
					&& date("m",$possible_dup["displayed_timestamp"])+0==$posted_fields["displayed_date_month"]+0
					&& date("H",$possible_dup["displayed_timestamp"])+0==$posted_fields["displayed_date_hour"]+0
					&& date("i",$possible_dup["displayed_timestamp"])+0==$posted_fields["displayed_date_minute"]+0
	 			){
	 				return $possible_dup["news_item_id"];
	 			}
	 		}
	 		return 0;
	 	}else{
	 		return 0;
	 	}
	 	
	 }
    
	  
}
