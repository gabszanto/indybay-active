<?php
//--------------------------------------------
//Indybay NewsItemRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewsItemRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");
require_once(CLASS_PATH."/db/news_item_db_class.inc");

//used for rendering related tasks that relate to newsitems
//parent class for article and blurb renderers
class NewsItemRenderer extends Renderer
{
	function get_relative_web_path_from_item_info($news_item_info){
		$this->track_method_entry("NewsItemRenderer","get_relative_web_path_from_id");
		
		$cache_class= new Cache();
		$append_anchor="";
		if (array_key_exists("parent_item_id",$news_item_info) && $news_item_info["parent_item_id"]!=0 && $news_item_info["parent_item_id"]!=$news_item_info["news_item_id"]){
			$append_anchor="#".$news_item_info["news_item_id"];
			$news_item_db_class= new NewsItemDB();
			$news_item_info=$news_item_db_class->get_news_item_info($news_item_info["parent_item_id"]);
		}
		$created_date=$news_item_info["creation_timestamp"];
		$rel_dir=$cache_class->get_rel_dir_from_date($created_date);
		$rel_url=NEWS_REL_PATH."/".$rel_dir.$news_item_info["news_item_id"].".php".$append_anchor;
		
		$this->track_method_exit("NewsItemRenderer","get_relative_web_path_from_id");
		
		return $rel_url;
	}

} //end class
?>