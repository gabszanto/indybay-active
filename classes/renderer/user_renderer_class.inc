<?php
//--------------------------------------------
//Indybay UserRenderer Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"UserRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");

require_once ("renderer/renderer_class.inc");



//render information related to users
class UserRenderer extends Renderer
{

	//text block on edit pages for information
	//on who has been changing things
 	function render_user_detail_info($user_info){
 	
 		$this->track_method_entry("UserRenderer","render_user_detail_info");
 		
 		$result=$user_info['username']."<br/>";
 		$result.=$user_info['first_name']." ".$user_info["last_name"]."<br/>";
 		$result.=$user_info['email']."<br/>";
 		$result.=$user_info['phone']."<br/>";;
 		
 		$this->track_method_entry("UserRenderer","render_user_detail_info");
 		
 		return $result;
 	}
 
} //end class
?>