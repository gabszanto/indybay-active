<?php
//--------------------------------------------
//Indybay IndyCalendar Class
//Continued over from sf-active codebase
//

require_once(CLASS_PATH."/renderer/calendar/calendar.inc");
class IndyCalendar extends Calendar{

	 var $topic_id=0;
	 var $region_id=0;
	 var $news_item_status_restriction=0;
	
	//Specialized Calendar for the Indymedia site that inherits from the Calendar class

	function IndyCalendar($topic_id, $region_id, $news_item_status_restriction){
		$this->topic_id=$topic_id;
		$this->region_id=$region_id;
		$this->news_item_status_restriction=$news_item_status_restriction;
	}
	
	function getDateLink($day, $month, $year){
		//returns a link for a given day
		$date= new Date;
		$date->set_time(0,0, $day, $month,$year);
		$day=$date->get_day();
		$month=$date->get_month();
		$year=$date->get_year();
		if (!isset($news_item_status_restriction))
			$news_item_status_restriction=0;
		return "event_week.php?day=".$day."&month=".$month."&year=".$year."&".
				"topic_id=".$this->topic_id."&region_id=".$this->region_id."&".
				"news_item_status_restriction=".$news_item_status_restriction;
	}
	function getCalendarLink($month, $year){
		//returns link to allow navigation by month
		global $unusedmonthvalue;
		global $isleft;
		//for now disable link
		//$unusedmonthvalue=$month;
		if ($unusedmonthvalue==$month){
			return "";
		}else{
			$date= new Date;
			if ($isleft){
				//if ($month==12){
				//	$date->set_time(0,0,1,1,$year+1);
				//}else{
					$date->set_time(0,0,1,$month+1,$year);
				//}
				$date->move_forward_n_days(-1);
			}else{
				//if ($month==1){
				//	$date->set_time(0,0,1,1,$year+1);
				//}else{
					$date->set_time(0,0,1,$month,$year);
				//}
				$date->find_start_of_week();
				if ($month!=$date->get_month()){
					$date->move_n_weeks_forward(1);
				}
			}
			$day1=$date->get_day();
			$month1=$date->get_month();
			$year1=$date->get_year();
			//return "or=".$ormonth."un=".$unusedmonthvalue;
			return "event_week.php?day=".$day1."&amp;month=".$month1."&amp;year=".$year1."&amp;isl=".$isleft."&topic_id=".$this->topic_id."&region_id=".$this->region_id."&news_item_status_restriction=".$this->news_item_status_restriction;
		}
	}
}
?>
