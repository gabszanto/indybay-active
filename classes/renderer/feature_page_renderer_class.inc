<?php
//--------------------------------------------
//Indybay FeaturePageRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"FeaturePageRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");
require_once(CLASS_PATH."/renderer/blurb_renderer_class.inc");

//used to render who feature pages and previes of those pages
//also used to render options that appear on the page edit page
class FeaturePageRenderer extends Renderer
{

	
	//renders the blurbs on a current feature page
   function render_page($blurb_list, $page_info){
   
   		$this->track_method_entry("FeaturePageRenderer","render_page");
   		
   		$blurb_renderer_class= new BlurbRenderer;
   		$html="";
   		$template_name="";
   		$previous_template="";
   		$center_column_type_id= $page_info["center_column_type_id"];
   		foreach ($blurb_list as $next_blurb){
   				$template_name=$this->determine_template($next_blurb, $next_blurb["display_option_id"], $previous_template, $center_column_type_id);
				if (strpos($template_name,"left")>0 || strpos($template_name,"right")>0)
   					$previous_template=$template_name;
   				$html.=$blurb_renderer_class->get_rendered_blurb_html($next_blurb, $template_name);
   		}
   		
   		$this->track_method_exit("FeaturePageRenderer","render_page");
   		
   		return $html;
   }
   
   
   
   
   //determines the template to use when rendering a blurb
   function determine_template($next_blurb, $display_option_id, $previous_template, $center_column_type_id){
  
  		$this->track_method_entry("FeaturePageRenderer","determine_template", "news_item_id", $next_blurb["news_item_id"]);
  		
   		$has_short_image=0;
   		$has_long_image=0;
   		$default_left=0;
   		$default_right=0;   		
   		$long_version_exists=0;
   		

   		if ($center_column_type_id==CENTER_COLUMN_TYPE_SHORT_ALTERNATING ||
   			$center_column_type_id==CENTER_COLUMN_TYPE_SHORT_LEFT ||
   			$center_column_type_id==CENTER_COLUMN_TYPE_SHORT_RIGHT
   			){
   			$page_default_size=2;
   		}else{
   			$page_default_size=1;
   		}
   		if ($center_column_type_id==CENTER_COLUMN_TYPE_LONG_LEFT ||
   			$center_column_type_id==CENTER_COLUMN_TYPE_SHORT_LEFT 
   			){
   			$page_default=2;
   		}else if ($center_column_type_id==CENTER_COLUMN_TYPE_LONG_RIGHT ||
   			$center_column_type_id==CENTER_COLUMN_TYPE_SHORT_RIGHT 
   			){
   			$page_default=3;
   		}else{
   			$page_default=1;
   		}

   		$prev_left=0;
   		
   		if ($next_blurb["media_attachment_id"]+0!=0){
   			$has_long_image=1;
   		}
   		if ($next_blurb["thumbnail_media_attachment_id"]+0!=0){
   			$has_short_image=1;
   		}
   		
   		if (strpos($previous_template,"left"))
   			$prev_left=1;
   			
   		
   		if ($display_option_id==BLURB_DISPLAY_OPTION_AUTO){
   			if ($page_default_size==1){
   				$display_option_id=1;
   			}else if ($page_default_size==2){
   				$display_option_id=2;
   			}
   		}
   		
   		if ($display_option_id==BLURB_DISPLAY_OPTION_AUTO_LONG){
   			if ($has_long_image==0 || $page_default==4)
   				$template_name="feature_page/long_version_no_image.tpl";
   			else if ($page_default==3 ||	($page_default==1 && $prev_left==1))
   				$template_name="feature_page/long_version_image_on_right.tpl";
   			else if ($page_default==2 || $page_default==1)
   				$template_name="feature_page/long_version_image_on_left.tpl";
   		}		
   		
   		if ($display_option_id==BLURB_DISPLAY_OPTION_AUTO_SHORT){
   			if ($has_short_image==0 || $page_default==4)
   				$template_name="feature_page/short_version_no_image.tpl";
   			else if ($page_default==3 ||	($page_default==1 && $prev_left==1))
   				$template_name="feature_page/short_version_image_on_right.tpl";
   			else if ($page_default==2 || $page_default==1)
   				$template_name="feature_page/short_version_image_on_left.tpl";
   		}
  
   		if ($display_option_id==BLURB_DISPLAY_OPTION_NOIMAGE_LONG){
   			$template_name="feature_page/long_version_no_image.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_LEFT_LONG){
   			$template_name="feature_page/long_version_image_on_left.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_RIGHT_LONG){
   			$template_name="feature_page/long_version_image_on_right.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_NOIMAGE_SHORT){
   			$template_name="feature_page/short_version_no_image.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_LEFT_SHORT){
   			$template_name="feature_page/short_version_image_on_left.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_RIGHT_SHORT){
   			$template_name="feature_page/short_version_image_on_right.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_HTML_LONG){
   			$template_name="feature_page/long_version_html_only.tpl";
   		}else if ($display_option_id==BLURB_DISPLAY_OPTION_HTML_SHORT){
   			$template_name="feature_page/short_version_html_only.tpl";
   		}
   
	   $this->track_method_exit("FeaturePageRenderer","determine_template");

   	   return $template_name;
   }
   
   
   
   
   //returns the template to use for a blurb in the preview of the short version
   //on the blurb edit and add pages
   function determine_sample_short_template($blurb_info){
   		
   		$this->track_method_entry("FeaturePageRenderer","determine_sample_short_template");
   	    
   	    if ($blurb_info['thumbnail_media_attachment_id']==0)
   	    	$ret= "feature_page/short_version_no_image.tpl";
   	    else
   	    	$ret= "feature_page/short_version_image_on_left.tpl";
   	    
   	    $this->track_method_exit("FeaturePageRenderer","determine_sample_short_template");
      	
      	return $ret;
   }
   
   
   
   
   //returns the template to use for a blurb in the single item view
   function determine_single_item_template($blurb_info){
   		
   		$this->track_method_entry("FeaturePageRenderer","determine_single_item_template");
   	    
   	    if ($blurb_info['media_attachment_id']==0)
   	    	$ret= "feature_page/single_item_version_no_image.tpl";
   	    else
   	    	$ret= "feature_page/single_item_version_image_on_left.tpl";
   	    	
   	    $this->track_method_exit("FeaturePageRenderer","determine_single_item_template");
   	    
   	    return $ret;
   }
   
      //returns the template to use for a blurb in the single item view
   function determine_single_item_template_for_list($blurb_info){
   		
   		$this->track_method_entry("FeaturePageRenderer","determine_single_item_template_for_list");
   	    
   	    if ($blurb_info['media_attachment_id']==0)
   	    	$ret= "feature_page/short_version_no_image.tpl";
   	    else
   	    	$ret= "feature_page/short_version_image_on_left.tpl";
   	    	
   	    $this->track_method_exit("FeaturePageRenderer","determine_single_item_template_for_list");
   	    
   	    return $ret;
   }
   
   
   
   
   
   function render_recent_feature_list($page_id){
   
   		require_once(CLASS_PATH."/db/feature_page_db_class.inc");
		require_once(CLASS_PATH."/db/blurb_db_class.inc");
		require_once(CLASS_PATH."/cache/article_cache_class.inc");
		$feature_page_db_class= new FeaturePageDB();
		$blurb_db_class= new BlurbDB();
		$article_cache_class= new ArticleCache();
		$results=$feature_page_db_class->get_recent_blurb_version_ids();
		$i=0;
		$previous_titles=array();
		$ret="";
		foreach ($results as $version_id){
			$blurb_info=$blurb_db_class->get_blurb_info_from_version_id($version_id);
			
			$title2=$blurb_info["title2"];
			$title1=$blurb_info["title1"];
			if (trim($title1)=="" || trim($title2)=="")
				continue;
			if (!isset($previous_titles[$title2])){
				$pages=$feature_page_db_class->get_pages_with_pushed_long_versions_of_blurb($blurb_info["news_item_id"]);
				if (sizeof($pages)!=0 && !(sizeof($pages)==1 && $pages[0]["page_id"]==FRONT_PAGE_CATEGORY_ID)){
				
					$create_time_formatted=strftime("%D",$blurb_info["creation_timestamp"]);
					
					$page_link="";
					$pi=0;
					$page_link="";
					foreach ($pages as $page_info){
						$pi=$pi+1;
						if ($pi>1)
							$page_link.=" | ";
						$page_link.="<a href=\"/".$page_info["relative_path"]."\">";
						$page_link.=$page_info["short_display_name"];
						$page_link.="</a>";
					}
				
					$previous_titles[$title2]=1;
					$searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($blurb_info["news_item_id"], $blurb_info["creation_timestamp"]);
					$row=$create_time_formatted;
					$row.=" <a href=\"".$searchlink;
					$row.="\">";
					$row .= $this->check_plain($blurb_info['title2']);
					$row.="</a> &nbsp; &nbsp; <strong>";
					$row.=$page_link;
					$row.="</strong><br />";
					$ret.=$row;
					$i=$i+1;
					if ($i>10)
						break;
				}
	
		}
			}
   		return $ret;
   }
   
} //end class
?>
