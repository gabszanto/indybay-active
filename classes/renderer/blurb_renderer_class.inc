<?php
//--------------------------------------------
//Indybay BlurbRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"BlurbRenderer");

require_once(CLASS_PATH."/renderer/news_item_version_renderer_class.inc");
require_once(CLASS_PATH."/renderer/media_attachment_renderer_class.inc");
require_once(CLASS_PATH."/renderer/feature_page_renderer_class.inc");
require_once(CLASS_PATH."/db/media_attachment_db_class.inc");
require_once(CLASS_PATH."/db/blurb_db_class.inc");
require_once(CLASS_PATH."/media_and_file_util/upload_util_class.inc");



//renders the html for a blurb as it appears in the center column,
//a single item view, the archive list or the preview page
class BlurbRenderer extends NewsItemVersionRenderer
{

	//used when trying to get a single blurb view for single item view 
	function get_rendered_blurb_html_from_news_item_id($news_item_id){
	
		$this->track_method_entry("BlurbRenderer","get_rendered_blurb_html_from_news_item_id", "news_item_id", $news_item_id);
		
		$blurb_db_class=new BlurbDB;
		$feature_page_renderer_class= new FeaturePageRenderer;
		
		$blurb_info=$blurb_db_class->get_blurb_info_from_news_item_id($news_item_id);
		$template_name=$feature_page_renderer_class->determine_single_item_template($blurb_info);
		$html=$this->get_rendered_blurb_html($blurb_info, $template_name);
		
		$this->track_method_exit("BlurbRenderer","get_rendered_blurb_html_from_news_item_id");
	
		return $html;
	}
	
		//used when trying to get a single blurb view for archives
	function get_rendered_blurb_html_for_list_from_news_item_id($news_item_id){
	
		$this->track_method_entry("BlurbRenderer","get_rendered_blurb_html_for_list_from_news_item_id", "news_item_id", $news_item_id);
		
		$blurb_db_class=new BlurbDB;
		$feature_page_renderer_class= new FeaturePageRenderer;
		
		$blurb_info=$blurb_db_class->get_blurb_info_from_news_item_id($news_item_id);
		$template_name=$feature_page_renderer_class->determine_single_item_template_for_list($blurb_info);
		$html=$this->get_rendered_blurb_html($blurb_info, $template_name);
		
		$this->track_method_exit("BlurbRenderer","get_rendered_blurb_html_for_list_from_news_item_id");
	
		return $html;
	}
	
	
	
	//renders the html for a blurb as it appears in the center column,
	//a single item view, the archive list or the preview page
    function get_rendered_blurb_html($article_data, $template_name){
    
    	$this->track_method_entry("BlurbRenderer","get_rendered_blurb_html", "news_item_id", $article_data["news_item_id"]);
    	
    	$new_array=array();
    	$new_array=array_merge($new_array, $article_data);
    	
    	$icons="";
    	$icon_names="";
    	if (strpos($new_array["text"],"imc_photo")>0){
    		$icons .= '<img alt="photo" src="/im/imc_photo.gif" /> ';
    		$icon_names.=" Photos";
    	}
    	if (strpos($new_array["text"],"imc_video")>0){
    		$icons .= '<img alt="video" src="/im/imc_video.gif" /> ';
    		if (strlen($icon_names)>0){
    			$icon_names.=" & ";
    		}
    		$icon_names.=" Video";
    	}
    	if (strpos($new_array["text"],"imc_audio")>0){
    		$icons .= '<img alt="audio" src="/im/imc_audio.gif" /> ';
    		if (strlen($icon_names)>0){
    			$icon_names.=" & ";
    		}
    		$icon_names.=" Audio";
    	}
    	if (strpos($new_array["text"],"imc_pdf")>0){
    		$icons .= '<img alt="PDF" src="/im/imc_pdf.gif" /> ';
    		if (strlen($icon_names)>0){
    			$icon_names.=" & ";
    		}
    		$icon_names.=" PDF";
    	}

        	
    	if ($new_array["is_text_html"])
    		$new_array["text"]=$new_array["text"];
    	else
    		$new_array["text"]=$this->cleanup_text($new_array["text"], TRUE);
    		
    	if ($new_array["is_summary_html"])
    		$new_array["summary"]=$new_array["summary"];
    	else
    		$new_array["summary"]=$this->cleanup_text($new_array["summary"], TRUE);
        
        $created_timestamp=$new_array["creation_timestamp"];
        $modified_timestamp=$new_array["modified_timestamp"];
        $fcreated=strftime("%a %b %e %Y",$created_timestamp);
        $fmodified=strftime("%D",$modified_timestamp);
        if (strftime("%D",$modified_timestamp)==strftime("%D",$created_timestamp)){
        	$formatted_date=$fcreated;
        }else{
        	$formatted_date=$fcreated." <small>(Updated ";
        	$formatted_date.=$fmodified.")</small>";
        }
        $new_array["formatted_date"]=$formatted_date;
		if (isset($new_array["related_url"]))
        	$new_array["related_link"]=$new_array["related_url"];
    	else
    		$new_array["related_link"]="";
    	$new_array["shortened_related_link"]=$this->shorten_link_for_display($new_array["related_link"]);
        if (strpos($template_name,"short")>0){
        	$media_attachment_id=$new_array["thumbnail_media_attachment_id"];
        }else{
        	$media_attachment_id=$new_array["media_attachment_id"];
        }
        
        $media_attachment_db= new MediaAttachmentDB();
        $media_attachment_renderer= new MediaAttachmentRenderer();
        if ($media_attachment_id!=0){
        	$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
        	if ($media_attachment_info["upload_status_id"]+0==0){
        		$upload_util= new UploadUtil();
        		$media_attachment_info=$upload_util->verify_attachment($media_attachment_info);
        	}
        	if ($media_attachment_info["upload_status_id"]+0!=0 && $media_attachment_info["upload_status_id"]+0!=3){
        		$new_array["image_url"]=UPLOAD_URL.$media_attachment_info["relative_path"].$media_attachment_info["file_name"];
        		$new_array["alt_tag"] = $this->check_plain($media_attachment_info['alt_tag']);
        	}else{
        		$new_array["media"]="DEBUG:MISSING Attachment";
	        	$new_array["alt_tag"]="DEBUG:MISSING Attachment";
        	}
        }else{
	        $new_array["media"]="";
	        $new_array["alt_tag"]="";
        }
        
        $new_array["SINGLE_ITEM_VIEW_LINK"]=$this->get_relative_web_path_from_item_info($article_data);
        
         if (strpos($template_name,"short")>=0){
         	$feature_page_db_class= new FeaturePageDB;
        	$read_more_link="<strong><a href=\"".$new_array["SINGLE_ITEM_VIEW_LINK"]."\">Read more</a>";
        	$pages_with_long_versions_of_blurb=$feature_page_db_class->get_pages_with_pushed_long_versions_of_blurb($article_data["news_item_id"]);
        	$i=1;
        	$j=sizeof($pages_with_long_versions_of_blurb);
        	foreach($pages_with_long_versions_of_blurb as $page_info){
        		if ($i==1)
        			$read_more_link .= " on Indybay's ";
        			
        			$read_more_link.="<a href=\"/".$page_info["relative_path"]."\">";
        		$read_more_link.=$page_info["long_display_name"]."</a>";
        		
        		if ($i<$j-1)
        			$read_more_link.=", ";
        		if ($i==$j-1)
        			$read_more_link.=" or ";
        		if ($i==$j && $j>1)
        			$read_more_link.=" pages";
        		if ($j==1 && $i==$j)
        			$read_more_link.=" page";
        		$i=$i+1;
        	}
        	$read_more_link.="</strong>";
        	if (strlen($icon_names)>0){
    			$read_more_link="<div class=\"readmorelinks\">".$icons." ".$read_more_link."</div>";
    		}else{
    			$read_more_link="<div class=\"readmorelinks\">".$read_more_link."</div>";
    		}
    		
        	
        	$new_array["READ_MORE_LINKS"]=$read_more_link;
        }
        
        $new_array['breaking_news'] = '';
        rsort($new_array['breaking_items']);
        foreach ($new_array['breaking_items'] as $news_item) {
          require_once('db/article_db_class.inc');
          require_once('renderer/article_renderer_class.inc');
          $article_db_class = new ArticleDB;
          $article_info = $article_db_class->get_article_info_from_news_item_id($news_item['news_item_id']);
          $article_renderer_class = new ArticleRenderer;
          $new_array['breaking_news'] .= $article_renderer_class->get_rendered_article_html($article_info);
        }
        unset($new_array['breaking_items']);
        
        $template_obj = new FastTemplate(TEMPLATE_PATH);
        $template_obj->clear_all();
        $template_obj->define(array("article_page" => $template_name));
        $temp_array=array();
        foreach ($new_array as $key=>$value){
        	$temp_array["TPL_LOCAL_".strtoupper($key)]=$value;
        }
        
    	$template_obj->assign($temp_array);
        $template_obj->parse("CONTENT","article_page");
        $result_html = $template_obj->fetch("CONTENT");
    	return $this->html_purify($result_html, NULL, TRUE);
    	
    	$this->track_method_exit("BlurbRenderer","get_rendered_blurb_html");
    
    }
    
    
    //renders the blurb version list as it appears on the blurb edit page
    function render_blurb_version_list($article_list_info){
    
	    $this->track_method_entry("BlurbRenderer","render_blurb_version_list");
    
    	return $this->render_version_list($article_list_info, "blurb_edit.php");
    	
    	$this->track_method_exit("BlurbRenderer","render_blurb_version_list");
    }

} //end class
