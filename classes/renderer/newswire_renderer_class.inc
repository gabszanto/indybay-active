<?php
//--------------------------------------------
//Indybay NewswireRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewswireRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");
require_once(CLASS_PATH."/cache/news_item_cache_class.inc");


//used to render the newswires on the feature pages
class NewswireRenderer extends Renderer
{

	//renders the hrml for a newswire section
    function get_newswire_rows_html($result_data){
    
    	$this->track_method_entry("NewswireRenderer","get_newswire_rows_html");
    	
    	$html="";
    	foreach($result_data as $row){
    		$html.=$this->render_newswire_row($row);
    	}
    	
    	$this->track_method_exit("NewswireRenderer","get_newswire_rows_html");
    	
    	return $html;
    }
    
    /**
     * Format a string containing a count of items.
     * 
     * @param $count
     *   The item count to display.
     * @param $singular
     *   The string for the singular case. Please make sure it is clear this is
     *   singular, to ease translation (e.g. use "1 new comment" instead of "1 new").
     *   Do not use @count in the singular string.
     * @param $plural
     *   The string for the plural case. Please make sure it is clear this is plural,
     *   to ease translation. Use @count in place of the item count, as in "@count
     *   new comments".
     * @return
     *   A translated string.
     */
    function format_plural($count, $singular, $plural) {
      if ($count == 1) {
        return $singular;
      }
      else {
        return strtr($plural, array('@count' => 0 + $count));
      }
    }    

    //renders a row in the cached newswire file
    function render_newswire_row($row){
    	
    	$this->track_method_entry("NewswireRenderer","render_newswire_row");
    	
    	if ($row["news_item_type_id"]==NEWS_ITEM_TYPE_ID_EVENT)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_CALENDAR_ICON;
        else if ($row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_NONE ||
        	 $row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_TEXT)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_TEXT_ICON;
    	else if ($row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_IMAGE)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_IMAGE_ICON;
    	else if ($row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_AUDIO)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_AUDIO_ICON;
    	else if ($row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_VIDEO)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_VIDEO_ICON;
    	else if ($row["media_type_grouping_id"]==MEDIA_TYPE_GROUPING_DOCUMENT)
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_DOCUMENT_ICON;
    	else
    		$row["image_icon_link"]=MEDIA_TYPE_GROUPING_OTHER_ICON;
    	
    	if (0 + $row['num_comments'] > 0) {
          $row['numcomments_section'] = '('. $this->format_plural($row['num_comments'], '1 comment', '@count comments') .')';
        }
    	else {
          $row['numcomments_section'] = '';
        }

    	$news_item_cache_class= new NewsItemCache;
    	$row["news_item_link"]=$news_item_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
    	$template_obj = new FastTemplate(TEMPLATE_PATH);
        $template_name = "newswire/newswire_row.tpl";
        $template_obj->clear_all();
        $template_obj->define(array("newswire_row" => $template_name));
        $temp_array=array();
        foreach ($row as $key=>$value){
        	$value=htmlspecialchars($value);
        	$temp_array["TPL_LOCAL_".strtoupper($key)]=$value;
        }
    	$template_obj->assign($temp_array);
        $template_obj->parse("CONTENT","newswire_row");
        $result_html = $template_obj->fetch("CONTENT");
        
        $this->track_method_exit("NewswireRenderer","render_newswire_row");
        
        return $result_html;
    
    }

}
