<?php
//--------------------------------------------
//Indybay MediaAttachmentRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"MediaAttachmentRenderer");

require_once(CLASS_PATH."/media_and_file_util/upload_util_class.inc");
require_once(CLASS_PATH."/renderer/renderer_class.inc");

//renders media attachments on posts and center column items
//also makes calls off to the legacy migration code to move
//uploads into date specific folders when they are 
//not found during rendering
class MediaAttachmentRenderer extends Renderer
{


	function render_small_thumbnail_from_news_item_info($news_item_info,$additional_image_tag_info){
		
		$this->track_method_entry("MediaAttachmentRenderer","render_small_thumbnail_from_news_item_info");
		
		if (is_array($news_item_info)){
			$upload_util_class= new UploadUtil();
			$news_item_version_db_class = new NewsItemVersionDB();
			$thumbnail_media_attachment_id=$news_item_info["thumbnail_media_attachment_id"];
			$media_attachment_id=$news_item_info["media_attachment_id"];
			$news_item_version_id=$news_item_info["news_item_version_id"];
			
			if ($thumbnail_media_attachment_id+0!=0){
				$media_attachment_db=new MediaAttachmentDB();
				$thumbnail_info=$media_attachment_db->get_media_attachment_info($thumbnail_media_attachment_id);
				if (!is_array($thumbnail_info) || $thumbnail_info["media_attachment_id"]!=$thumbnail_media_attachment_id)
					$thumbnail_media_attachment_id=0;
			}
			if ($thumbnail_media_attachment_id+0!=0){
				$ret=$this->render_small_thumbnail_for_attachment_from_media_attachment_id($thumbnail_media_attachment_id,$additional_image_tag_info);
			}else if ($media_attachment_id+0!=0){
				$thumbnail_info=$upload_util_class->generate_thumbnails_for_media_attachment_from_id($media_attachment_id);
				$thumbnail_media_attachment_id=$thumbnail_info["small_thumbnail_media_attachment_id"];
				if ($thumbnail_media_attachment_id+0>0){
					if ($news_item_info["news_item_type_id"]!=NEWS_ITEM_TYPE_ID_BLURB){
						$news_item_version_db_class->update_thumbnail_attachment_id_for_version($news_item_version_id, $thumbnail_media_attachment_id);
					}
					$ret=$this->render_small_thumbnail_for_attachment_from_media_attachment_id($thumbnail_media_attachment_id,$additional_image_tag_info);
				}else{
					$ret=$this->render_small_thumbnail_for_attachment_from_media_attachment_id($media_attachment_id,$additional_image_tag_info);
				}
			}else{
				$ret="";
			}
		}else{
			echo "error: render_small_thumbnail_from_news_item_info called with null info<br />";
			$ret="";
		}
		
		$this->track_method_exit("MediaAttachmentRenderer","render_small_thumbnail_from_news_item_info");
	
		return $ret;
	}
	
	function render_medium_thumbnail_from_news_item_info($news_item_info,$additional_image_tag_info){
		
		$this->track_method_entry("MediaAttachmentRenderer","render_medium_thumbnail_from_news_item_info");
		
		$ret="";
		if (is_array($news_item_info)){
			$upload_util_class= new UploadUtil();
			$news_item_version_db_class = new NewsItemVersionDB();
			$media_attachment_id=$news_item_info["media_attachment_id"];
			$news_item_version_id=$news_item_info["news_item_version_id"];
			if ($media_attachment_id+0!=0){
				$thumbnail_info=$upload_util_class->generate_thumbnails_for_media_attachment_from_id($media_attachment_id);
				if (is_array($thumbnail_info) && sizeof($thumbnail_info)>0){
					$thumbnail_media_attachment_id=$thumbnail_info["medium_thumbnail_media_attachment_id"];
					if ($thumbnail_media_attachment_id+0>0){
						$ret=$this->render_medium_thumbnail_for_attachment_from_media_attachment_id($thumbnail_media_attachment_id,$additional_image_tag_info);
					}else{
						$ret=$this->render_medium_thumbnail_for_attachment_from_media_attachment_id($media_attachment_id,$additional_image_tag_info);
					}
				}
			}else{
				$ret="";
			}
		}else{
			echo "error: render_medium_thumbnail_from_news_item_info called with null info<br />";	
		}
		
		$this->track_method_exit("MediaAttachmentRenderer","render_medium_thumbnail_from_news_item_info");
	
		return $ret;
	}
	
	

	
	
	function render_small_thumbnail_for_attachment_from_media_attachment_id($media_attachment_id, $additional_image_tag_info){
		
		$this->track_method_entry("MediaAttachmentRenderer","render_small_thumbnail_for_attachment_from_media_attachment_id");
			
		$media_attachment_db=new MediaAttachmentDB();
		$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
		if (!is_array($media_attachment_info) || $media_attachment_info["media_attachment_id"]!=$media_attachment_id){
			echo "render_small_thumbnail_for_attachment_from_media_attachment_id called with bad media_attachment_id ".$media_attachment_id."<br />";
			$ret="";
		}else{
			$media_attachment_info["is_thumbnail"]=1;
			$ret=$this->render_attachment($media_attachment_info,0,THUMBNAIL_WIDTH_SMALL,THUMBNAIL_HEIGHT_SMALL,$additional_image_tag_info);
		}
		$this->track_method_exit("MediaAttachmentRenderer","render_small_thumbnail_for_attachment_from_media_attachment_id");
	
		return $ret;
	}
	
	function render_medium_thumbnail_for_attachment_from_media_attachment_id($media_attachment_id, $additional_image_tag_info){
		
		$this->track_method_entry("MediaAttachmentRenderer","render_medium_thumbnail_for_attachment_from_media_attachment_id");
			
		$media_attachment_db=new MediaAttachmentDB();
		$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
		if (!is_array($media_attachment_info) || $media_attachment_info["media_attachment_id"]!=$media_attachment_id){
			echo "render_medium_thumbnail_for_attachment_from_media_attachment_id called with bad media_attachment_id ".$media_attachment_id."<br />";
			$ret="";
		}else{
			$media_attachment_info["is_thumbnail"]=1;
			$ret=$this->render_attachment($media_attachment_info,1,THUMBNAIL_WIDTH_MEDIUM,THUMBNAIL_HEIGHT_MEDIUM,$additional_image_tag_info);
		}
		$this->track_method_exit("MediaAttachmentRenderer","render_medium_thumbnail_for_attachment_from_media_attachment_id");
	
		return $ret;
	}
	
	
	
	
    function render_small_thumbnail_for_attachment($media_attachment_info){
    	
    	$this->track_method_entry("MediaAttachmentRenderer","render_small_thumbnail_for_attachment","media_attachment_id",$media_attachment_info["media_attachment_id"]);
    	
    	$ret=$this->render_attachment($media_attachment_info);
    	
    	$this->track_method_exit("MediaAttachmentRenderer","render_small_thumbnail_for_attachment");
    	
    	return $ret;
    }
    
    
	function render_attachment($media_attachment_info, $include_name_info=1, $restrict_width_in_tag=0,$restrict_height_in_tag=0, $additional_tag_params = ''){
		
		$this->track_method_entry("MediaAttachmentRenderer","render_attachment","media_attachment_id",$media_attachment_info["media_attachment_id"]);
			
		if (!is_array($media_attachment_info) || sizeof($media_attachment_info)==0){
			echo "render_attachment called with empty media attachment info<br/>";
		}else{
			
	    	$media="";
	    	$upload_util_class= new UploadUtil();
	    	$image_util_class= new ImageUtil();
	    	
	    	if ($media_attachment_info["upload_status_id"]!=UPLOAD_STATUS_VALIDATED){
	    		$media_attachment_info=$upload_util_class->verify_attachment($media_attachment_info);
	        }
	        
	        $file_name=$media_attachment_info["file_name"];
	    	$upload_name=$media_attachment_info["upload_name"];
	    	$relative_path=$media_attachment_info["relative_path"];
	    	$file_dir=UPLOAD_PATH."/".$media_attachment_info["relative_path"];
	    	$full_file_path=$file_dir.$file_name;
	    	$mime_type=$media_attachment_info["mime_type"];
	    	$relative_url="/uploads/" . $relative_path.$file_name;
	        
	        if ($media_attachment_info["upload_status_id"]!=UPLOAD_STATUS_FAILED){
	        
	       
	        					if (file_exists($full_file_path))
	        						$file_size=$this->render_file_size(filesize($full_file_path));
						        $tr = new Translate;
						
						       // $this->article['torrent_link'] = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <small>(<a href="' .
						        //    TORRENT_URL . '/' . $this->article['basename'] . '.torrent">download torrent</a>)</small>';
						 
						        switch ($media_attachment_info["mime_type"])
						        {
						            case "image/jpeg":
						            case "image/gif":
						            case "image/png":
						           
						                if (file_exists($full_file_path)){
						               
	
											$description = $tr->trans('image_file');
							                $image_info = getimagesize($full_file_path);
							                $image_size = $image_info[0] . "x" . $image_info[1];
				
						                	if ($restrict_width_in_tag!=0 && $restrict_width_in_tag<$image_info[0]){
						                		$new_width=$restrict_width_in_tag;
						                		$new_height=$image_info[1]*($restrict_width_in_tag/$image_info[0]);
						                	}else{
						                		$new_width=$image_info[0];
						            			$new_height=$image_info[1];
						                	}
						                	if ($restrict_height_in_tag!=0 && $restrict_height_in_tag<$new_height){
						                		$new_width=$new_width*($restrict_height_in_tag/$new_height);
						                		$new_height=$restrict_height_in_tag;
						                	}
						                	
						                	$additional_tag_params.= ' width="' . $new_width . '" height="' . $new_height . '" ';

						                	if ($image_info['mime']) { 
						                		$image_type = $image_info['mime']; 
						                	}
						                	
						                	if ($image_info[2] == 4 || $image_info[2] == 13)
						                	{
						                    	$media= '<embed allowScriptAccess="never" allowNetworking="none" allowFullScreen="false" $additional_tag_params ';
						                	} else
						                	{
		                    	$media= "<img ".$additional_tag_params." ";
		                	}
		                	$media.= "src=\"" . $relative_url . "\" ";
		                	
		                	

		                	$alt_tag = $file_name . ' ';
		                	if ($image_info[0] >= POSTED_IMAGE_MAX_WIDTH - 210 || $image_info[1] >= POSTED_IMAGE_MAX_HEIGHT - 160 || ($restrict_width_in_tag!=0)){
		                			$media_atachment_db_class= new MediaAttachmentDB();
		                			$original_info=$media_atachment_db_class->get_original_attachment_info_from_attachment_id($media_attachment_info["media_attachment_id"]);
		                			if (is_array($original_info) && $original_info["media_attachment_id"]!=$media_attachment_info["media_attachment_id"]){
		                				$original_file_name=$original_info["file_name"];
								    	$original_relative_path=$original_info["relative_path"];
								    	$original_file_dir=UPLOAD_PATH."/".$original_info["relative_path"];
								    	$original_full_file_path=$original_file_dir.$original_file_name;
								    	$original_relative_url="/uploads/" . $original_relative_path.$original_file_name;
								    	$original_image_info = getimagesize($original_full_file_path);
								    	
								    	//if this is not a thumbnail the link should go to the original large upload
								    	//if it is a thumbnail the image will usually already be in a link to the news article
								    	if (!array_key_exists("is_thumbnail",$media_attachment_info)){
									    		$media="<a href=\"".$original_relative_url."\">".$media;
		                				}
		                				$alt_tag.="original image ( ".$original_image_info[0]."x".$original_image_info[1].")";
		                			}
		                		}
		                	$media .= ' alt="' . $alt_tag . '" />';
		                	if ((!isset($original_info) || $original_info["media_attachment_id"]==$media_attachment_info["media_attachment_id"]) && 
		                	(($restrict_width_in_tag>0  && $image_info[0]>$restrict_width_in_tag) || ($restrict_height_in_tag>0 && $image_info[1]>$restrict_height_in_tag))
		                	)
		                	{
		                		$media = '<a href="' . $relative_url . '">' . $media . '</a>';                                                
		                	}
		                	if ($include_name_info==1){
								if (strlen($file_name)>27){
									$file_name=substr($file_name,0,25)."...";
								}
		                		$media.= "<br /><strong>" . $file_name . "</strong><br /><small>";
		                		if (isset($original_info) && $original_info["media_attachment_id"]!=$media_attachment_info["media_attachment_id"]){
									if (array_key_exists("is_thumbnail",$media_attachment_info)){
										$media.="</a>";
									}
									$media.= "<a href=\"".$original_relative_url."\">original image ( ";
		                			$media.= $original_image_info[0]."x".$original_image_info[1].")</a>";
		                		}
		                		$media.="</small>";
		                	}
		                }
						break;
		
		            case "application/pdf":
		            	$media = "<a href=\"".$relative_url;
		                $media.= "\"><strong>";
		            	$media_atachment_db_class= new MediaAttachmentDB();
		            	$rendered_pdf_info=$media_atachment_db_class->get_renderered_pdf_info($media_attachment_info["media_attachment_id"]);
            			$upload_util= new UploadUtil();
            			if(!is_array($rendered_pdf_info)){
            				$image_util_class->make_image_for_pdf($media_attachment_info["media_attachment_id"], $media_attachment_info);
            				$rendered_pdf_info=$media_atachment_db_class->get_renderered_pdf_info($media_attachment_info["media_attachment_id"]);
            			}
            			if (is_array($rendered_pdf_info)){
            				$rendered_pdf_file_name=$rendered_pdf_info["file_name"];
					    	$rendered_pdf_relative_path=$rendered_pdf_info["relative_path"];
					    	$rendered_pdf_file_dir=UPLOAD_PATH."/".$rendered_pdf_info["relative_path"];
					    	$rendered_pdf_relative_url="/uploads/" . $rendered_pdf_relative_path.$rendered_pdf_file_name;
							 $media.="<img src=\"".$rendered_pdf_relative_url."\" /><br />";
						}
						
		                $description = $tr->trans('pdf_file');
		                $media.= $file_name."<br />download PDF (" . $file_size . ")</strong></a>";
		                
		                
		                
		                
		                break;
		
		            case "application/smil":
		                $description = $tr->trans('smil_file');
		                $media.= "multimedia: <a href=\"" . $relative_url . "\">SMIL at ";
		                $media.= $file_size . "</a>";
		                break;
		
                            case "audio/amr":
                            case "audio/x-m4a":
		            case "audio/mpeg":
		            case "audio/ogg":
		            case "audio/x-mpegurl":
		            case "audio/mpegurl":
		            case "audio/x-scpls":
		            case "audio/x-pn-realaudio":
		            case "audio/x-pn-realaudio-meta":
		            case "audio/x-ms-wma":
		            case "audio/x-wav":
                              $md5 = 'i' . md5($relative_url);
                              $media .= '<center><table width="100%"><tr valign="top">';
                              if ($media_attachment_info['mime_type'] != 'audio/x-mpegurl') {
                                $audio = '<audio preload="none" src="' . SERVER_URL . $relative_url . '" controls="controls"></audio>';
                                $media .= '<th>Listen now:<br />' . $audio . '<br /><small style="font-weight: normal;">Embed code: </small><textarea onclick="this.focus();this.select();" style="overflow: hidden; width: 200px; height: 10px; font-family: monospace; font-size: xx-small;" rows="1" cols="1">' . $this->check_plain($audio) . '</textarea></th>';
                              }
                              $media .= '<th width="50%">Download audio:<br /><a href="' . $relative_url . '" title="Download audio" class="audio"><img src="/im/audio-icon_160x24.png" alt="Download audio" width="160" height="24" /><br />' . $file_name . ' ' . $file_size . '</a></th>';
                              $media .= '</tr></table></center>';
                              break;

			case "video/mp4":
                                $media_atachment_db_class = new MediaAttachmentDB();
                                $rendered_video_info = $media_atachment_db_class->get_renderered_video_info($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
                                $upload_util = new UploadUtil();
                                $image_util= new ImageUtil();
                                if(!is_array($rendered_video_info)) {
                                        $image_util->make_image_for_video($media_attachment_info['media_attachment_id'], $media_attachment_info);
                                        $rendered_video_info = $media_atachment_db_class->get_renderered_video_info($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
                                }
                                $thumbnail = ' ';
                                if (is_array($rendered_video_info)) {
                                        $rendered_video_file_name = $rendered_video_info['file_name'];
                                        $rendered_video_relative_path = $rendered_video_info['relative_path'];
                                        $rendered_video_file_dir = UPLOAD_PATH . '/' . $rendered_video_info['relative_path'];
                                        $rendered_video_url = SERVER_URL .'/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
					$thumbnail = '<br /><span class="video-thumbnail"><img src="' . SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '" border="0" /></span><br />';
                                        $poster = SERVER_URL . '/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
				}
                                $media .= '<video width="100%" preload="none" poster="' . $poster . '" controls><source src="' . SERVER_URL . $relative_url . '" type="video/mp4" /><a class="video" href="' . SERVER_URL . $relative_url . '">Download video' . $thumbnail . $file_name . ' (' . $file_size . ')</a></video>';
                                $media .= '<div style="width: 100%; text-align: left; margin: auto; max-width: 480px; font-size: xx-small">Copy the following to embed the movie into another web page:</div><textarea style="overflow: hidden; width: 100%; max-width: 480px; font-family: monospace; font-size: xx-small" onclick="this.focus();this.select();">' . htmlspecialchars($media) . '</textarea>';
		                break;

                        case 'video/ogg':
			case "video/x-ms-wmv":
			case "video/x-pn-realvideo":
			case "video/quicktime":
			case "video/x-msvideo":
			case "video/avi":
			case "video/mp2":
		        case "video/mpeg":
			case 'video/x-flv':
                        case 'video/3gpp':
                                $media_atachment_db_class = new MediaAttachmentDB();
                                $rendered_video_info = $media_atachment_db_class->get_renderered_video_info($media_attachment_info['media_attachment_id']);
                                $upload_util = new UploadUtil();
                                $image_util= new ImageUtil();
                                if(!is_array($rendered_video_info)) {
                                        $image_util->make_image_for_video($media_attachment_info['media_attachment_id'], $media_attachment_info);
                                        $rendered_video_info = $media_atachment_db_class->get_renderered_video_info($media_attachment_info['media_attachment_id']);
                                }
                                $thumbnail = ' ';
                                if (is_array($rendered_video_info)) {
                                        $rendered_video_file_name = $rendered_video_info['file_name'];
                                        $rendered_video_relative_path = $rendered_video_info['relative_path'];
                                        $rendered_video_file_dir = UPLOAD_PATH . '/' . $rendered_video_info['relative_path'];
                                        $rendered_video_url = SERVER_URL .'/uploads/' . $rendered_video_relative_path . $rendered_video_file_name;
					$thumbnail = '<br /><span class="video-thumbnail"><img src="/uploads/' . $rendered_video_relative_path . $rendered_video_file_name . '" border="0" /></span><br />';
				}
		                $media .= '<table width="100%"><tr>';
                                $flv = $media_atachment_db_class->get_renderered_flv($media_attachment_info['media_attachment_id']);
                                if (!empty($flv) || $media_attachment_info['mime_type'] == 'video/x-flv') {
                                  $large_thumbnail_url = SERVER_URL .'/im/playButton-640x360.png';
                                  if ($media_attachment_info['mime_type'] == 'video/x-flv') {
                                    $rendered_video_url = SERVER_URL . $relative_url;
                                  }
                                  else {
                                    $rendered_video_url = SERVER_URL .'/uploads/' . $flv['relative_path'] . $flv['file_name'];
                                  }
                                  $md5 = 'i' . md5($rendered_video_url);
                                  $media .= '<td align="center"><div class="flowplayer-video" id="'. $md5 .'"><a href="' . $relative_url . '"><img src="/im/playButton-640x360.png" /></a></div>';
                                  $media .= '<script type="text/javascript">
                        		// <![CDATA[
              $(function() {
                flowplayer("' . $md5 . '", "/js/flowplayer/flowplayer.swf", { clip: { url: "' . $rendered_video_url . '", scaling: "fit", useHWScaling: true, accelerated: true } });
              });
		// ]]>
                </script>';
                                  $flash = '<object type="application/x-shockwave-flash" width="480" height="360" data="https://www.indybay.org/js/flowplayer/FlowPlayer.swf"> ';
                                  $flash .= '<param name="movie" value="https://www.indybay.org/js/flowplayer/FlowPlayer.swf" /> ' .
                                    '<param name="quality" value="high" /><param name="bgcolor" value="#000000" /><param name="allowFullScreen" value="true" /> ' .
                                    '<param name="flashvars" value="config={videoFile:\''. $rendered_video_url .'\',splashImageFile:\''. $large_thumbnail_url .'\',loop:false,autoPlay:false,autoBuffering:false,bufferLength:5,initialScale:\'fit\'}" /> <p>Your browser is not able to display this multimedia content.</p></object>';
                                  $media .= '<div style="width: 640px; font-size: xx-small">Copy the following to embed the movie into another web page:</div>' .
                                    '<textarea style="overflow: hidden; width: 480px; font-family: monospace; font-size: xx-small" onclick="this.focus();this.select();">' . htmlspecialchars($flash) . '</textarea>' . '</td>';
                                }
                                $media .= '<td align="center"><a class="video" href="' . $relative_url . '">download video:' . $thumbnail . $file_name . ' (' . $file_size . ')</a></td>';
                                $media .= '</tr></table>';
		                break;

		            case "video/x-pn-realvideo-meta":
		                $description = $tr->trans('realvideo_metafile');
		                $media.= "video: <a href=\"" . $relative_url . "\">
		                 ".$file_name."<br />
		                RealVideo ";
		                $media.= "metafile</a>";
		                break;
		
		            case "application/x-bittorrent":
		                $description = 'BitTorrent';
		                $media.= "download: <a href=\"" . $relative_url . "\">
		                ".$file_name."<br />
		                BitTorrent ";
		                $media.= "at " . $file_size . "</a>";
		                break;
		        }
		    }
	        //if (filesize($full_file_path) > $GLOBALS['torrent_minimumfilesize'] and 
	        //    $this->article['display'] != 'f' and $GLOBALS['torrent_generate'])
	        //{
	        //    $this->cache_torrent();
		    //$media.= $this->article['torrent_link'];
	        //}
		}
		$this->track_method_exit("MediaAttachmentRenderer","render_attachment");
        
        return $media;
    }
    


	

	
	function get_restrict_image_size_select($select_name){
		
		$this->track_method_entry("MediaAttachmentRenderer","get_restrict_image_size_select");
		
		$dropdown_array=$this->custom_range_by_n(120,320,40);
		$dropdown_array["0"]="don't resize";
		$select_list=$this->make_select_form($select_name,$dropdown_array,120);
		
		$this->track_method_exit("MediaAttachmentRenderer","get_restrict_image_size_select");
		
		return $select_list;
	}
	
	
	
	function render_file_size ($size)
    {
    	$this->track_method_entry("MediaAttachmentRenderer","render_file_size");
        // Returns HTML for a filesize

        if ($size < 1048576)
        {
            $size = $size / 1024;
            $size = sprintf("%.1f",$size);
            $size = $size . " KB";
        } else
        {
            $size = $size / 1048576;
            $size = sprintf("%.1f",$size);
            $size = $size . 'MB';
        }
		
		$this->track_method_exit("MediaAttachmentRenderer","render_file_size");
		
        return $size;
    }

}
