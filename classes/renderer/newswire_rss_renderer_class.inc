<?php
//--------------------------------------------
//Indybay NewswireRSSRenderer Class
//for now this is just old code that will need to
//be refactored
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"NewswireRSSRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");

class NewswireRSSRenderer extends Renderer{

    /**
     *    returns a list of articles to be syndicated given a category.
     *    @param    int $cat
     *    @return   array
     */
    function get_syndicated_articles($cat = 0)
    {
        // function to get articles for syndication. $newsfeed could be "l" or "lgtf"
    if($GLOBALS['config_defcategory'] == $cat ) { $cat = 0 ; }

    if(!$this->max_id) $this->get_max_id();


        $newsfeed = $GLOBALS['newsfeed'];
        $news_length = $GLOBALS['news_length'];
        while ($char < strlen($newsfeed))
        {
            $display_query .= "display='$newsfeed[$char]' or "; $char++;
        }

    // we add an id limiter. this forces mysql to use the primary_key & to select
    // in a much smaller range. result: much faster queries.
    // if they're not defined, we set some quite sane defaults (should be easier
    // to upgrade running sites with it.
    if($cat)
    {
        if(!$GLOBALS['nwf_c_range']) $GLOBALS['nwf_c_range'] = 1000 ;
        $id_limiter = $this->max_id - $GLOBALS['nwf_c_range'] ;
        $limit_query = " and w.id > ".$id_limiter." ";
    }
    else
    {
        if(!$GLOBALS['nwf_range']) $GLOBALS['nwf_range'] = 250 ;
        $id_limiter = $this->max_id - $GLOBALS['nwf_range'] ;
        $limit_query = " and id > ".$id_limiter." ";
    }

        $display_query = preg_replace('/or $/', '', $display_query);
        $query = "select display,numcomment,id,linked_file,author,heading,mime_type,created, summary, article, language_id from webcast where ($display_query) and parent_id=0 $limit_query ORDER BY id DESC limit $news_length";

// corrected the following query to accept correctly the display_query :)))) -- blicero
        if ($cat > 0 && $GLOBALS['config_defcategory'] !== $cat)
        {
            $query = "SELECT w.display as display,
                             w.numcomment as numcomment,
                             w.id as id,
                             w.linked_file as linked_file,
                             w.author as author,
                             w.heading as heading,
                             w.mime_type as mime_type,
                             w.created as created,
                             w.summary as summary,
                             w.article as article,
                 w.language_id as language_id
                      FROM webcast w, catlink c
                      WHERE (".$display_query.")
                        AND parent_id = 0
                        AND w.id = c.id
                        AND c.catid = $cat
            $limit_query
                      ORDER BY id
                      DESC limit $news_length";
            
        }

        $resultset = $this->db->query($query);
        return $resultset;
    }

    /**
     *    caches an rss-feed obtained from {@link get_syndicated_articles()} given a category and a name.
     *    @param    int    $cat
     *    @param    string    $name
     */
    function make_newswire_rss($cat = 0, $name = '')
    {
        $GLOBALS['print_rss'] = "" ;
        $GLOBALS['print_rss2'] = "";
        include_once(CLASS_PATH.'/rss10.inc');
	include_once(CLASS_PATH.'/rss20.inc');

    $lang_obj = new language ;
        $resultset = $this->get_syndicated_articles($cat);
        $rows = sizeof($resultset);
        $xml_logo = $GLOBALS['xml_logo'];

        if (strlen($name) > 0)
        {
            $xml_file = WEB_PATH . '/syn/' . $name . '.xml';
            $rdf_file = WEB_PATH . '/syn/' . $name . '.rdf';
	    $rdf2_file = WEB_PATH . '/syn/' . $name . '.rss';
            $rdf_url = ROOT_URL . '/syn/' . $name . '.rdf';
        } else
        {
            $xml_file = WEB_PATH . '/syn/' . $GLOBALS['xml_file'];
            $rdf_file = WEB_PATH . '/syn/' . $GLOBALS['rdf_file'];
	    $rdf2_file = WEB_PATH . '/syn/' . $GLOBALS['rdf2_file'];
            $rdf_url = ROOT_URL . '/syn/' . $GLOBALS['rdf_file'];
        }

        $site_nick = $GLOBALS['site_nick'];
        $site_name = $GLOBALS['site_name'];

        $root_url = ROOT_URL.'/';
        $news_url = NEWS_URL.'/';
        $uploads_url = UPLOAD_URL.'/';
        $rss=new RSSWriter($root_url, $site_nick, $site_name, $rdf_url, array("dc:publisher" => $site_nick, "dc:creator" => $site_nick));
        $rss->useModule("content", "http://purl.org/rss/1.0/modules/content/");
        $rss->useModule("dcterms", "http://purl.org/dc/terms/");
        $rss->setImage($xml_logo, $site_nick);

	$rss2 = new RSS2Writer($root_url, $site_nick, $site_name, $rdf_url);

        foreach(array_reverse($resultset) as $row) 
        {

            $id = $row['id'];
            $imagelink = "";
            $topic = "";

            if (preg_match('/image/', $row[mime_type])) {
                $imagebase = basename(trim($row['linked_file']));
                $imagelink = $uploads_url . $imagebase;
            }

            $created = $row['created'];
//            $date_array  = substr($created,0,4) . "-" . substr($created,4,2) . "-" . substr($created,6,2) . " ";
//            $date_array .= substr($created,8,2) . ":" . substr($created,10,2) . ":" . substr($created,12,2);
            $rdf_date_array = gmdate('Y-m-d\TH:i:s\Z',strtotime($created));
            $pathtofile = MakeCacheDir($row['created']);

            $articlelink =  $news_url . $pathtofile . $row['id'] . ".php";
            $catquery = "select c.name as name from catlink l,category c where l.id=$id and c.category_id=l.catid";
            $catset = $this->db->query($catquery);

            /* cycles through all the category of a article and displays them in the newswire rss feed -- blicero 30.07.2002 */

            while ($catrow = array_pop($catset))
            {
                $topic .= utf8_encode(htmlspecialchars($catrow[name])) . "\t";
            }

            $topic=trim($topic);

            switch ($row['display'])
            {
                case 'f':
                    $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_hidden']));
                    break;
                case 'l':    
                    $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_local']));
                    break;
                case 't':
                    $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_display']));
                    break;
                case 'g':
                    $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_global']));
                    break;
            }
            $heading = trim(utf8_encode(htmlspecialchars($row[heading])));
            $author= trim(utf8_encode(htmlspecialchars($row[author])));
            $fields = str_replace(array("&amp;#","&amp;amp;","&amp;gt;","&amp;lt;","&amp;quot;"),array("&#","&amp;","&gt;","&lt;","&quot;"),array($heading,$author,$topic,$section));
            $xml_list .= "
        <story>
            <title>$fields[0]</title>
            <url>$articlelink</url>
            <time>$date_array</time>
            <author>$fields[1]</author>
            <department>$row[mime_type]</department>
            <topic>$fields[2]</topic>
            <comments>$row[numcomment]</comments>
            <section>$fields[3]</section>
            <image>$imagelink</image>
        </story>";
            if (preg_match('#' . UPLOAD_PATH . '#', $row['linked_file'])) {
        // note: this will only work if you never moved your images across the filesystem.
		$file_size = filesize($row['linked_file']);
                $file = str_replace(UPLOAD_PATH, UPLOAD_URL, $row['linked_file']) ;
            }else {
                $file = '';
            }
          $a=str_replace(array('&','<','>',chr(19),chr(20),chr(24),chr(25),chr(28),chr(29),chr(128),chr(145),chr(146),chr(147),chr(148),chr(149),chr(150),chr(151),chr(133)),array('&amp;','&lt;','&gt;','&#8211;','&#8212;','"',"'",'"','"','&#8364;','&#8216;','&#8217;','&#8220;','&#8221;','&#8226;','&#8211;','&#8212;','&#8230;'),array($row['heading'],$row['summary'],$row['author'],$row['article']));
           	$title = utf8_encode(trim($a[0]));
            $summary = utf8_encode(trim($a[1]));
            $date = $rdf_date_array ;
            $mime = utf8_encode(trim($row['mime_type']));
            $author = utf8_encode(trim($a[2]));
            $article = utf8_encode(trim($a[3]));
            $language = trim(utf8_encode(htmlspecialchars($lang_obj->get_language_code($row['language_id']))));
            $rss->addItem($articlelink, $title, array("description" => $summary, "dc:date" => $date, "dc:subject" => $name, "dc:creator" => $author, "dc:format" => $mime, "dc:language" => $language, "content:encoded" => $article, "dcterms:hasPart" => $file));
            $rss2->addItem($articlelink, $title, array("description" => $summary, "pubDate" => $date, "enclosure" => "$file,$mime,$size" ));

        }

        $xml = "";
        $xml = "<?xml version=\"1.0\"?>
        <!DOCTYPE backslash [ <!ELEMENT backslash (story*)> <!ELEMENT story (title, url, time, author, department, topic, comments, section, image)> <!ELEMENT title (#PCDATA)> <!ELEMENT url (#PCDATA)> <!ELEMENT time (#PCDATA)> <!ELEMENT author (#PCDATA)> <!ELEMENT department (#PCDATA)> <!ELEMENT topic (#PCDATA)> <!ELEMENT comments (#PCDATA)> <!ELEMENT section (#PCDATA)> <!ELEMENT image (#PCDATA)> ]>
        <backslash>";
        $xml .= $xml_list;
        $xml .= "
        </backslash>";
        $rdf=$rss->serialize();
        $rdf=str_replace(array("&amp;#","&amp;amp;","&amp;gt;","&amp;lt;","&amp;quot;"),array("&#","&amp;","&gt;","&lt;","&quot;"),$rdf);

        $rdf2=$rss2->serialize();
        $rdf2=str_replace(array("&amp;#","&amp;amp;","&amp;gt;","&amp;lt;","&amp;quot;"),array("&#","&amp;","&gt;","&lt;","&quot;"),$rdf2);

        $this->cache_file($xml, $xml_file);
        $this->cache_file($rdf, $rdf_file);
        $this->cache_file($rdf2, $rdf2_file);
    }
}//end class

?>
