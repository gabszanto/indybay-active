<?php
//--------------------------------------------
//Indybay EventListRenderer Class
//Written January 2006
//
//Modification Log:
//1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"EventListRenderer");

require_once(CLASS_PATH."/renderer/renderer_class.inc");
require_once(CLASS_PATH."/cache/news_item_cache_class.inc");


//used to render the newswires on the feature pages
class EventListRenderer extends Renderer
{

	//renders the hrml for a newswire section
    function render_highlighted_event_list($page_id){
    	$this->track_method_entry("NewswireRenderer","render_highlighted_event_list");
    	$event_list_db= new EventListDB();
		$result_data=$event_list_db->get_highlighted_event_list_for_page($page_id);	
		if (!is_array($result_data))
			return "";
    	$html="";
    	
    	if (sizeof($result_data)==0){
	    		$html="";
	    	}else{
	    	
	    	$num_rows=0;
	    	$last_formatted_day="";
	    	foreach($result_data as $row){
	    		$event_start_date=$row["displayed_timestamp"];
	    		$formatted_day=strftime("%A %b %e",$event_start_date);
	    		if ($formatted_day!=$last_formatted_day){
	    			$num_rows=$num_rows+1;
	    		}
	    		$last_formatted_day=$formatted_day;
	    		$num_rows=$num_rows+1;
	    	}
	    	$current_row=0;
	    	$last_formatted_day="";
	    	$added_column=0;
	    	$html.="<table width=\"100%\"><tr><td valign=\"top\" width=\"50%\"> <span class=\"breakbug-event\">";
	    	foreach($result_data as $row){
	    		$event_start_date=$row["displayed_timestamp"];
	    		$formatted_day=strftime("%A %b %e",$event_start_date);
	    		if ($formatted_day!=$last_formatted_day){
	    			$html.="<strong>".$formatted_day."</strong><br/>";
	    			$current_row=$current_row+1;
	    		}
	    		$last_formatted_day=$formatted_day;
	    		$html.=$this->render_highlighted_event_link_row($row);
	    		$current_row=$current_row+1;
	    		if ($current_row>=round($num_rows/2) && $added_column==0){
	    			$html.="</span></td><td valign=\"top\" width=\"50%\"><span class=\"breakbug-event\">";
	    			$added_column=1;
	    		}
	    	}

	    	$html.="
	    	</span></td></tr></table><strong><a href=\"/calendar/?page_id=".$page_id."\">More Events...</a></strong>
	    	<br /><br />
	    	";
    	}
    	$this->track_method_exit("NewswireRenderer","render_highlighted_event_list");
    	
    	return $html;
    }
    
    //renders the hrml for a newswire section
    function render_event_list($page_id){

    	$this->track_method_entry("NewswireRenderer","render_highlighted_event_list");
    
	$event_list_db= new EventListDB();
	$result_data=$event_list_db->get_list_for_page($page_id);	
    	$html="<small>";
    	
    	if (sizeof($result_data)!=0){
	    	$num_rows=0;
	    	$current_row=0;
	    	$html.="";
	    	foreach($result_data as $row){
	    		$event_start_date=$row["displayed_timestamp"];
	    		$formatted_day=strftime("%A %b %e",$event_start_date);
	    		$last_formatted_day=$formatted_day;
	    		$row["formatted_day"]=$formatted_day;
	    		$html.=$this->render_event_link_row($row);
	    		$current_row=$current_row+1;
	    	}
	    	$html.="";
    	}
    	$this->track_method_exit("NewswireRenderer","render_highlighted_event_list");
    	
    	return $html;
    }
    
    
    
    
    //renders a row in the cached newswire file
    function render_highlighted_event_link_row($row){
    	
    	$this->track_method_entry("EventListRenderer","render_event_link_row");

    	$news_item_cache_class= new NewsItemCache;
    	$news_item_link=$news_item_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
    	
    	if ($row["title1"]==strtoupper($row["title1"]))
    		$row["title1"]=ucwords(strtolower($row["title1"]));
    		
        $title = $row['title1'];
    	$test=wordwrap($row["title1"],49);
    	$i=strpos($test,"\n");
    	
    	if ($i>0)
    		$row["title1"]=substr($test,0,$i)."...";
    		
    	$event_start_date=$row["displayed_timestamp"];
    	if (strftime("%M",$event_start_date)=="00")
    		$row["formatted_time"]=strftime("%l%p",$event_start_date);
    	else
    		$row["formatted_time"]=strftime("%l:%M%p",$event_start_date);

    	$result_html ="
			".$row["formatted_time"].'
			<a title="' . $this->check_plain($title) . '" href="'. $news_item_link .'">
			'. $this->check_plain($row['title1']) .'</a>
			<br />
    	';
    	
    	$this->track_method_exit("EventListRenderer","render_event_link_row");
    	
        return $result_html;
    
    }
    
    
     //renders a row in the cached newswire file
    function render_event_link_row($row){
    	
    	$this->track_method_entry("EventListRenderer","render_event_link_row");

    	$news_item_cache_class= new NewsItemCache;
    	$news_item_link=$news_item_cache_class->get_web_cache_path_for_news_item_given_date($row["news_item_id"], $row["creation_timestamp"]);
    	
    	if ($row["title1"]==strtoupper($row["title1"]))
    		$row["title1"]=ucwords(strtolower($row["title1"]));
    		
    	$test=wordwrap($row["title1"],55);
    	$i=strpos($test,"\n");
    	
    	if ($i>0)
    		$row["title1"]=substr($test,0,$i)."...";
    		
    	$event_start_date=$row["displayed_timestamp"];
    	if (strftime("%M",$event_start_date)=="00")
    		$row["formatted_time"]=strftime("%l%p",$event_start_date);
    	else
    		$row["formatted_time"]=strftime("%l:%M%p",$event_start_date);

    	$result_html ="<div class=\"eventText\">
			".$row["formatted_time"]." ".$row["formatted_day"]."
			<a href=\"".$news_item_link."\">
			".$row["title1"]."</a>
			</div>
    	";
    	
    	$this->track_method_exit("EventListRenderer","render_event_link_row");
    	
        return $result_html;
    
    }

}
