<?php
//--------------------------------------------
//Indybay Renderer Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"Renderer");

require_once(CLASS_PATH."/common_class.inc");



//common methods used in rendering things like
//dropdown lists, safe text from users etc...
class Renderer extends Common
{

  /**
   * Encode special characters in a plain-text string for display as HTML.
   */
  public static function check_plain($text) {
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
  }

  /**
   * Prevent cross site scripting attacks on Internet Explorer 6.
   */
  public static function validate_utf8($text) {
    if (strlen($text) == 0) {
      return TRUE;
    }
    return (preg_match('/^./us', $text) == 1);
  }

  /**
   * Functional wrapper for the HTML Purifier library.
   * @param $allowed Optional comma-delimited list of allowed tags.
   * @return Purified HTML.
   */
  public static function html_purify($html, $allowed = NULL, $trusted = FALSE) {
    static $purifier = FALSE;
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Cache.SerializerPath', CACHE_PATH .'/HTMLPurifier');
    if (!$purifier) {
      $purifier = new HTMLPurifier($config);
    }
    $config->set('HTML.Allowed', $allowed);
    if ($trusted) {
      $config->set('HTML.DefinitionID', 'trusted');
      $config->set('HTML.DefinitionRev', 1);
      $config->set('Attr.EnableID', TRUE);
      if ($def = $config->maybeGetRawHTMLDefinition()) {
        $def->addAttribute('img', 'usemap', 'URI');
        $def->addElement('map', 'Block', 'Flow', 'Common', array(
          'name' => 'CDATA',
        ));
        $def->addElement('area', 'Inline', 'Empty', 'Common', array(
          'shape' => 'Enum#rect,circle,poly,default',
          'coords' => 'CDATA',
          'href' => 'URI',
          'nohref' => 'Bool',
          'alt' => 'CDATA',
        ));
      }
    }
    return $purifier->purify($html, $config);
  }

  /**
   * Return an HTML-escaped page title.
   */
  public static function page_title() {
    $title = isset($GLOBALS['page_title']) ? $GLOBALS['page_title'] .' : '. $GLOBALS['site_nick'] : $GLOBALS['site_name'];
    return self::check_plain($title);
  }

  /**
   * Returns an HTML-escaped page URL.
   */
  public static function htmlPageUrl() {
    return SERVER_URL . self::check_plain(str_replace('/index.php', '/', $_SERVER['REQUEST_URI']));
  }

    function cleanup_text($tmpvar, $trusted = FALSE)
    {
    	$this->track_method_entry("Renderer","cleanup_text");
    	
        // encode
        $tmpvar = str_replace(array('<','>','"'),array('&lt;','&gt;','&quot;'),$tmpvar);
        
        // re-encode to avoid confusing the link generator
        $tmpvar = str_replace(array('&quot;','&lt;','&gt;'),array(' &quot; ',' &lt; ',' &gt; '),$tmpvar);

        // replace newlines with html
        $tmpvar = preg_replace('/(\n\n|\r\r|\r\n\r\n|\r\n\t|\n\t|\r\t)/', ' <br /><br /> ', $tmpvar);
        $tmpvar = preg_replace('/(\r\n|\n|\r)/', ' <br /> ', $tmpvar);

        // turn e-mail addresses into mailto links
        $tmpvar = preg_replace('/([A-Za-z0-9_]([-._]?[A-Za-z0-9])*){1}@([A-Za-z0-9]([-.]?[A-Za-z0-9])*\.[A-Za-z]+)/', '<a href="mailto:\\0">\\1 [at] \\3</a>', $tmpvar);

        $rel = $trusted ? '' : 'rel="nofollow" ';

        // turn URLs into links; 
        $tmpvar = preg_replace("^((www\.)([a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/]))^i", "http://\\1", $tmpvar);
        $tmpvar = preg_replace("^((ftp://|http://|https://){2})([a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/])^i", "http://\\3", $tmpvar);
        $tmpvar = preg_replace("^(((ftp://|http://|https://){1})[a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/])^i", "<a " . $rel . "href=\"\\1\">\\1</a>", $tmpvar);

        // turn <a href="<a href=""></a>"> and <a href=<a href=""></a>> back into links
        $tmpvar = preg_replace("^(( &lt; a href= &quot; <a " . $rel . "href=\")(((ftp://|http://|https://){1})[a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/]\">))((((ftp://|http://|https://)([a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/]))))(</a> &quot;  &gt; )^i", "<a " . $rel . "href=\"\\6\">", $tmpvar);
        $tmpvar = preg_replace("^(( &lt; a href=<a " . $rel . "href=\")(((ftp://|http://|https://){1})[a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/]\">))((((ftp://|http://|https://)([a-zA-Z0-9@:%_.~#-\?&!]+[a-zA-Z0-9@:%_~#\?&/]))))(</a> &gt; )^i", "<a " . $rel . "href=\"\\6\">", $tmpvar);
        $tmpvar = preg_replace("^ &lt; /a &gt; ^i", "</a>", $tmpvar);

        // de-reencode
        $tmpvar = str_replace(array(' &lt; ',' &gt; ',' &quot; '),
            array('&lt;','&gt;','&quot;'),$tmpvar); 
        
        $this->track_method_exit("Renderer","cleanup_text");
        
        return $tmpvar;
    }
    
    
    
    
    function shorten_link_for_display($url){
    	
    	$this->track_method_entry("Renderer","shorten_link_for_display");
    	
    	if (strlen($url)>45){
    		$url=substr($url,0,42)."...";
    	}
    	
    	$this->track_method_exit("Renderer","shorten_link_for_display");
    	
    	return $url;
    }
    
    
    
    
    
    function format_link($urllink){
    	
    	$this->track_method_entry("Renderer","format_link");
    	
    	if (trim($urllink)=="")
    		return trim($urllink);
    	$urllink=trim($urllink);
    	if (!(preg_match("#^http://|^ftp://|^https://#i", $urllink, $reg))) {
            $urllink = "http://" . $urllink;
        }
        
        $this->track_method_exit("Renderer","format_link");
        
        return $this->check_plain($urllink);
    
    }
    

    
      function make_datetime_select_form_24hour($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year, $selected_hour, $selected_minute){
     	
     	$this->track_method_entry("Renderer","make_datetime_select_form");
     	$return_html="";
     	$return_html.=$this->make_select_form($base_name."_month",$this->custom_range(1,12), $selected_month);
     	$return_html.="/";
     	$return_html.=$this->make_select_form($base_name."_day",$this->custom_range(1,31), $selected_day);
     	$return_html.="/";
     	$year_start=strftime("%Y")-$num_years_in_past;
     	$year_end=strftime("%Y")+$num_years_in_future;
     	$return_html.=$this->make_select_form($base_name."_year",$this->custom_range($year_start,$year_end), $selected_year);
     	$return_html.='<input type="hidden" size="10" id="linkedDates" disabled="disabled" /><br />';
     	$hours=array(0=>"Midnight", 1=>"1 AM", 2=>"2 AM", 2=>"3 AM", 4=>"4 AM", 5=>"5 AM", 6=>"6 AM", 7=>"7 AM", 8=>"8 AM", 9=>"9 AM", 10=>"10 AM", 11=>"11 AM",
     	 12=>"Noon", 13=>"1 PM", 14=>"2 PM", 15=>"3 PM", 16=>"4 PM", 17=>"5 PM", 18=>"6 PM", 19=>"7 PM", 20=>"8 PM", 21=>"9 PM", 22=>"10 PM", 23=>"11 PM");
     	$return_html.=$this->make_select_form($base_name."_hour",$hours, $selected_hour);
     	$return_html.=":";
     	$minute_options=array();
     	$minute_options[0]="on the hour";
     	$minute_options[15]="15 minutes after the hour";
     	$minute_options[30]="30 minutes after the hour";
     	$minute_options[45]="45 minutes after the hour";
     	$return_html.=$this->make_select_form($base_name."_minute",$minute_options, $selected_minute);
     	
     	$this->track_method_exit("Renderer","make_datetime_select_form");
     	
     	return $return_html; 
     }
     
    
      function make_datetime_select_form($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year, $selected_hour, $selected_minute, $ampm){
     	
     	$this->track_method_entry("Renderer","make_datetime_select_form");
     	$return_html="";
     	$return_html.=$this->make_select_form($base_name."_month",$this->custom_range(1,12), $selected_month);
     	$return_html.="/";
     	$return_html.=$this->make_select_form($base_name."_day",$this->custom_range(1,31), $selected_day);
     	$return_html.="/";
     	$year_start=strftime("%Y")-$num_years_in_past;
     	$year_end=strftime("%Y")+$num_years_in_future;
     	$return_html.=$this->make_select_form($base_name."_year",$this->custom_range($year_start,$year_end), $selected_year);
     	$return_html.=" ";
     	$return_html.=$this->make_select_form($base_name."_hour",$this->custom_range(1,12), $selected_hour);
     	$return_html.=":";
     	$minute_options=array();
     	$minute_options[0]="00";
     	$minute_options[15]="15";
     	$minute_options[30]="30";
     	$minute_options[45]="45";
     	$return_html.=$this->make_select_form($base_name."_minute",$minute_options, $selected_minute);
     	$return_html.="<select name=\"".$base_name."_ampm\">";
     	$return_html.="<option value=\"AM\">AM</option>";
     	$return_html.="<option value=\"PM\" ";
     	if ($ampm=="PM"){
     		$return_html.=" selected ";
     	}
     	$return_html.=">PM</option>";
     	$return_html.="</select>";
     	
     	$this->track_method_exit("Renderer","make_datetime_select_form");
     	
     	return $return_html; 
     }
     
     
     
     
     
     function make_date_select_form($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year)
     {
     	
     	$this->track_method_entry("Renderer","make_date_select_form");
     	$return_html="";
     	$return_html.=$this->make_select_form($base_name."_month",$this->custom_range(1,12), $selected_month);
     	$return_html.="/";
     	$return_html.=$this->make_select_form($base_name."_day",$this->custom_range(1,31), $selected_day);
     	$return_html.="/";
     	$year_start=strftime("%Y")-$num_years_in_past;
     	$year_end=strftime("%Y")+$num_years_in_future;
     	$return_html.=$this->make_select_form($base_name."_year",$this->custom_range($year_start,$year_end), $selected_year);
     
     	$this->track_method_exit("Renderer","make_date_select_form");
     	
     	return $return_html; 
     }

 
 
 
 
 function make_boolean_checkbox_form($checkbox_name, $checked){
     		$this->track_method_entry("Renderer","make_boolean_checkbox_form");
     		$return_html="";
            $return_html .= "<input type=\"checkbox\" name=\"" . $checkbox_name . "\"";
            $return_html .= " value='1' ";
            if ($checked==1){
                        $return_html .= " checked=\"checked\"";
            }
            $return_html .= " />";
         	
         	$this->track_method_exit("Renderer","make_boolean_checkbox_form");
         	
         	return $return_html;

    }
    
function is_utf8($str)
{
   // values of -1 represent disalloweded values for the first bytes in current UTF-8
   static $trailing_bytes = array (
       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
       -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
       -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
       -1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
       2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, 3,3,3,3,3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
   );

   $ups = unpack('C*', $str);
   if (!($aCnt = count($ups))) return true; // Empty string *is* valid UTF-8
   for ($i = 1; $i <= $aCnt;)
   {
       if (!($tbytes = $trailing_bytes[($b1 = $ups[$i++])])) continue;
       if ($tbytes == -1) return false;
      
       $first = true;
       while ($tbytes > 0 && $i <= $aCnt)
       {
           $cbyte = $ups[$i++];
           if (($cbyte & 0xC0) != 0x80) return false;
          
           if ($first)
           {
               switch ($b1)
               {
                   case 0xE0:
                       if ($cbyte < 0xA0) return false;
                       break;
                   case 0xED:
                       if ($cbyte > 0x9F) return false;
                       break;
                   case 0xF0:
                       if ($cbyte < 0x90) return false;
                       break;
                   case 0xF4:
                       if ($cbyte > 0x8F) return false;
                       break;
                   default:
                       break;
               }
               $first = false;
           }
           $tbytes--;
       }
       if ($tbytes) return false; // incomplete sequence at EOS
   }       
   return true;
}
  
    
    
    
    
     function make_checkbox_form($checkbox_name, $options, $checked, $numberline){
     	  
     	  $this->track_method_entry("Renderer","make_checkbox_form");
     	  
     	  $i=1;
     	  $return_html ="<table><tr>";
          while (list($key, $value) = each ($options))
          {
               $return_html .= "<td><input type=\"checkbox\" name=\"" . $checkbox_name . "\"";
               $return_html .= " value=\"" . $key . "\"";
               if (is_array($checked))
               {
                    if (in_array($key,$checked))
                    {
                         $return_html .= " checked=\"checked\"";
                    }
               }
               $return_html .= " />" . $value . "</td>\n";
							if ($i==$numberline){
								$return_html .="</tr><tr>";
								$i=1;
							}else {
								$i=$i+1;
							}
          }
     			$return_html .="</tr></table>";
     			
     	  $this->track_method_exit("Renderer","make_checkbox_form");
     	 
          return $return_html;

    }
    
    
    
     function make_select_form ($select_name, $options, $match, $default=""){
          
          $this->track_method_entry("Renderer","make_select_form");
     		  
          $return_html = "<select name=\"" . $select_name . "\" id=\"". $select_name ."\">\n";
          if ($default!=""){
          	$return_html .="<option value=\"0\">".$default."</option>";
          }
          while (list($key, $value) = each ($options))
          {
               $return_html .= "<option value=\"$key\"";
               if ($match == $key)
               {
                $return_html .= " selected=\"selected\"";
               }
               $return_html .= '>' . $this->check_plain($value) . "</option>\n";
          }
          $return_html .= "</select>\n";
          
          $this->track_method_exit("Renderer","make_select_form");
          
          return $return_html;
     }
     
     
     
     function custom_range($val1, $val2){
     	
     	$this->track_method_entry("Renderer","custom_range");
     	
     	$new_array= array();
     	if ($val2>$val1 && $val2-100<$val1)
     	$i=$val1;
     	while ($i<=$val2){
     		$new_array[$i]=$i;
     		$i=$i+1; 
     	}
     	
     	$this->track_method_exit("Renderer","custom_range");
     	
     	return $new_array;
     }
     
     
     
     
     
     function custom_range_by_n($val1, $val2, $n){
     	
     	$this->track_method_entry("Renderer","custom_range_by_n");
     	
     	$new_array= array();
     	if ($n>0 && $val2>$val1 && $val2-100*$n<$val1){
     		$i=$val1;
     		while ($i<=$val2){
     			$new_array[$i]=$i;
     			$i=$i+$n; 
     		}
     	}
     	
     	$this->track_method_exit("Renderer","custom_range_by_n");
     	
     	return $new_array;
     }
     
     
     
     
     function create_dropdown($list_of_objs, $selected_item ){
     
     	$this->track_method_entry("Renderer","create_dropdown");
     	
		//renders dropdown for event updates
		$return_val="";
		foreach($list_of_objs as $key=>$value){
			$return_val.="<option value=\"";
			$return_val.=$key;
			$return_val.="\"";
			if (strlen($selected_item)>0){
			if ($selected_item==$key){
				$return_val.=" selected=\"selected\" ";
			}
                        }
			$return_val.=">".$value;
			$return_val.="</option>";
		}
		
		$this->track_method_exit("Renderer","create_dropdown");
		
		return $return_val;
	}
     
     
     
     
	function get_days_of_month(){
		
		$this->track_method_entry("Renderer","get_days_of_month");
		
		//returns an an array of obejcts with ids and names set to numbers from 1 to 31
		$days_of_month=array();
		for ($i=1;$i<32;$i=$i+1){
			$days_of_month[$i]=$i;
		}
		
		$this->track_method_exit("Renderer","get_days_of_month");
		
		return $days_of_month;
	}





	function get_months(){
	
		$this->track_method_entry("Renderer","get_months");
		
		//returns an an array of obejcts with ids and names set to numbers from 1 to 12
		$months=array();
		for ($i=1;$i<13;$i=$i+1){
			$months[$i]=$i;
		}
		
		$this->track_method_exit("Renderer","get_months");
		
		return $months;
	}
	
	
	
	
	
	function get_years(){
	
		$this->track_method_entry("Renderer","get_years");
		
		$this->track_method_entry("Renderer","get_months");
		//returns an an array of obejcts with ids and names set to numbers
		//between gloabl var $dropdown_min_year and $dropdown_max_year
                $dropdown_max_year=$GLOBALS['dropdown_max_year'];
                $dropdown_min_year=$GLOBALS['dropdown_min_year'];
		$now_array=getdate(time());
		if (strlen($dropdown_min_year)<1){
			$dropdown_min_year=2000;
		}
		if (strlen($dropdown_max_year)<1){
			$dropdown_max_year=$now_array["year"]+2;
		}
		$years=array();
		for ($i=$dropdown_min_year;$i<$dropdown_max_year;$i=$i+1){
			$years[$i]=$i;
		}
		
		$this->track_method_exit("Renderer","get_years");
		
		return $years;
	}
	
	
	
	
	
	function get_hours(){
		$this->track_method_entry("Renderer","get_hours");
		
		$hours=array();
		for ($i=1;$i<13;$i=$i+1){
			$hours[$i]=$i;
		}
		
		$this->track_method_exit("Renderer","get_hours");
		
		return $hours;
	}





	function get_minutes(){
		$this->track_method_entry("Renderer","get_minutes");
		
		//returns an an array of objects with ids 0 and 30 and names "00" and "30"
		$minutes=array();
		$minutes[0]="00";
		$minutes[30]="30";
		
		$this->track_method_exit("Renderer","get_minutes");
		
		return $minutes;
	}
	
	
	
	

	function get_ampm(){
		$this->track_method_entry("Renderer","get_ampm");
		
		//returns an an array of objects with ids 0 and 12 and names "AM" and "PM"
		$ampm=array();
		$new_object=new DefaultObject();
		$new_object->set_id(0);
		$new_object->set_name("AM");
		array_unshift($ampm,$new_object);
		$new_object=new DefaultObject();
		$new_object->set_id(12);
		$new_object->set_name("PM");
		array_unshift($ampm,$new_object);
		
		$this->track_method_exit("Renderer","get_ampm");
		
		return $ampm;
	}	
	
	function convert_smart_quotes($string) { 
    $search = array(chr(145), 
                    chr(146), 
                    chr(147), 
                    chr(148), 
                    chr(151)); 
 
    $replace = array("'", 
                     "'", 
                     '"', 
                     '"', 
                     '-'); 
 
    return str_replace($search, $replace, $string); 
} 

}
