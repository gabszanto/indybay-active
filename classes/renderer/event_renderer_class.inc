<?php
//--------------------------------------------
//Indybay EventRenderer Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"EventRenderer");

require_once(CLASS_PATH."/renderer/article_renderer_class.inc");
require_once(CLASS_PATH."/renderer/calendar/indycalendar.inc");
require_once(CLASS_PATH."/db/calendar_db_class.inc");
require_once(CLASS_PATH."/db/feature_page_db_class.inc");

//used to render event details and week views for events
//as well as the upcomming events sections on the 
//feature pages
class EventRenderer extends ArticleRenderer{



	function render_html_for_day_in_week_list($day_datetime){

		$this->track_method_entry("EventRenderer","render_html_for_day_in_week_list", "day_datetime", $day_datetime);

		$html=" ";
		$calendar_db_class= new CalendarDB();
		$start_of_day= strftime("%Y-%m-%d", $day_datetime)." 0:00";
		$end_of_day= strftime("%Y-%m-%d", $day_datetime)." 23:59:59";
		$events_for_day=$calendar_db_class->get_events_between_dates($start_of_day, $end_of_day);
		foreach($events_for_day as $event_info){
			$html.=$this->render_row_in_weekview($event_info);
		}
		$html.="\r\n";
		$this->track_method_exit("EventRenderer","render_html_for_day_in_week_list");
		return $html;
	}
	
	
	
	
	
	function render_row_in_weekview($event_info){
		
		$this->track_method_entry("EventRenderer","render_row_in_day_in_weekview");
		
		$calendar_db_class= new CalendarDB;
		$feature_page_db_class = new FeaturePageDB;
		
		$type_info=$calendar_db_class->get_event_type_info_for_newsitem($event_info["news_item_id"]);
		$keyword=$type_info["keyword"];
		
		$associated_pages=$feature_page_db_class->get_associated_pages_info($event_info["news_item_id"]);
       
		$html="\r\n<event ";
		$i=0;
		$topics_and_regions="";
	 	foreach ($associated_pages as $page_info){
        	if ($i>0){
        		$html .=", ";
        		$topics_and_regions .=" | ";
        	}
        	$topics_and_regions.=$page_info['long_display_name'];
		
		$cat_ids=$feature_page_db_class->get_category_ids_for_page($page_info["page_id"]);
        	foreach ($cat_ids as $cat_id){
			$html.=" category_id=\"".$cat_id."\" ";
		}
		$i=$i+1;
        }
		
		$cache_class= new Cache;
		$html.=" />";
		$html.="<strong><a href=\"";
		$html.="/newsitems/".$cache_class->get_rel_dir_from_date($event_info["creation_timestamp"]);
		$html.=$event_info["news_item_id"].".php\">";
		$html .= $this->check_plain($event_info['title1']);
		$html.="</a></strong><br />";
		if ($keyword!=""){
			$html.=$keyword;
			$html.="<br />";
		}
		if ($topics_and_regions!=""){
			$html.=$topics_and_regions;
			$html.="<br />";
		}
		$start_long= $event_info["event_date_timestamp"];
		$duration=$event_info["event_duration"]+0;
		$end_long=$start_long+($duration*60*60);
		$html.=strftime("%l:%M %p",$start_long);
		$html.= " - ";
		$html.=strftime("%l:%M %p",$end_long);
		$html.="<br/><br/>";
		
		$this->track_method_exit("EventRenderer","render_row_in_day_in_weekview");
		
		return $html;
	}
	



	function filter_cached_html($html, $topic_id, $region_id, $news_item_status_restriction){
		
		$this->track_method_entry("EventRenderer", "filter_cached_html");
		$tokenized_string=explode("\r\n",$html);
		$filtered_html="";
		foreach($tokenized_string as $line){
			if (strpos(" ".$line, "<event")==1){
				if ($this->verify_line_in_cached_file($line,$topic_id,$region_id,$news_item_status_restriction)==1){
					$filtered_html.=$line;
				}
			}else{
				$filtered_html.=$line;
			}
		}
		

		$this->track_method_exit("EventRenderer", "filter_cached_html");

		return $filtered_html;
	}	
	



	function verify_line_in_cached_file($line, $topic_id, $region_id, $news_item_status_restriction){
		$this->track_method_entry("EventRenderer", "verify_line_in_cached_file");
		
		$ret=1;
		if ($topic_id!=0){
			if (strpos($line,"category_id=\"".$topic_id."\"")==0){
				$ret=0;
			}	
		}
		
		if ($region_id!=0){
			if (strpos($line,"category_id=\"".$region_id."\"")==0){
				$ret=0;
			}
		}		

		$this->track_method_exit("EventRenderer", "verify_line_in_cached_file");
		return $ret;
	}	



	
		// Render Functions	
	function render_short_month_view_prev ($date, $datenow, $topic_id, $region_id, $news_item_status_restriction) {

		$this->track_method_entry("EventRenderer","render_short_month_view_prev");
		
		$calendar = new IndyCalendar($topic_id, $region_id, $news_item_status_restriction);
		if ($date->get_month() == $datenow->get_month() ||
			$date->get_month() == $datenow->get_month()+1) {
			$date_to_use = $datenow;
		} else {
				$date_to_use=$date;
		}

		$month = $date_to_use->get_month();
		$year = $date_to_use->get_year();
			
		$mv=$calendar->getMonthView($month, $year, $date);
		
		$this->track_method_exit("EventRenderer","render_short_month_view_prev");
		
		return $mv;

	}





	function render_short_month_view_next ($date, $datenow, $topic_id, $region_id, $news_item_status_restriction) {
	
		$this->track_method_entry("EventRenderer","render_short_month_view_next");
		
		$calendar = new IndyCalendar($topic_id, $region_id, $news_item_status_restriction);
		if ($date->get_month() == $datenow->get_month() ||
			$date->get_month() == $datenow->get_month()+1) {
			$date_to_use = $datenow;
		} else {
				$date_to_use=$date;
		}

		$month = $date_to_use->get_month();
		$year = $date_to_use->get_year();
		$month = $month+1;		
		
		$mv=$calendar->getMonthView($month, $year, $date);
		
		$this->track_method_exit("EventRenderer","render_short_month_view_next");
		
		return $mv;

	}
	
	
	
	
	
	function render_week_html($date_as_long, $topic_id, $region_id) {
		
		$sunday=0;
		$saturday=6;
		
		$event_renderer = new EventRenderer();
		
		$html_for_week = "<tr>";
		$tmpday = $sunday;
		$day_datetime=$date_as_long;
		while ( $tmpday < $saturday + 1 )
		{
			$html_for_week = $html_for_week."<td valign=\"top\" class=\"eventText\">";
			$html_for_week = $html_for_week.$event_renderer->render_html_for_day_in_week_list($day_datetime, $topic_id, $region_id);
			$html_for_week = $html_for_week."</td>";
			$day_datetime=$this->addDay($day_datetime);			
			$tmpday=$tmpday+1;
		}
		
		$html_for_week = $html_for_week."</tr>";

		return $html_for_week;
	}
	
	
	function addDay($timeStamp){
        //adds a day to the timstamp submitted through $timeStamp
        $tS = $timeStamp;
        $timeStamp = mktime (date('H',$tS),
                             date('i',$tS),
                             date('s',$tS),
                             date('n',$tS),
                             date('j',$tS)+1,
                             date('Y',$tS));

        return $timeStamp;
} 
	
	
	
} //end class
?>
