<?php
/**
 *  class to deal with spam.
 *  @package	sf-active
 *  @subpackage	articles
 */

/**
 *  class to deal with spam.
 *  @package    sf-active
 *  @subpackage articles
*/
class Spam
{

    /**
     *	class constructor, does nothing.
     */
    function Spam ()
    {
		// Nothing
    }
	
	/**
	 *  returns 1 if a post is spam.
	 *  @var    int	$spam_publish	set to 1 if the post contains data.
	 *  @return bool
	 */
	function Detect ($spam_publish)	// Return 1 for a Spam Block
	{
	
		$ret = 0;
		$cspam = 0;
		$ipspam = 0;
		// antispam filter code - st3
		// it'll work only if globals spam_filter_articles and spam_filter_time
		// are set in the local configuration files

		if (isset($GLOBALS['spam_filter_articles']) && isset($GLOBALS['spam_filter_time'])) {

			if ( $GLOBALS['spam_filter_time'] < (time()-filectime(CACHE_PATH.'/hashes_time')) ) {
				unlink(CACHE_PATH.'/hashes.txt');
				touch(CACHE_PATH.'/hashes.txt');
				touch(CACHE_PATH.'/hashes_time');
				unlink(CACHE_PATH.'/hashes_content.txt');
				touch(CACHE_PATH.'/hashes_content.txt');
			}

			if ($spam_publish) {
				$hashes = fopen(CACHE_PATH.'/hashes.txt','a');
				fputs($hashes,md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].$_SERVER['HTTP_ACCEPT_LANGUAGE'])."\n");
				fclose ($hashes);
			}

			$hashes = file(CACHE_PATH.'/hashes.txt','r');
			for ( $i=0; $i<count($hashes);$i++)
			{
	        	if (strcmp(md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].$_SERVER['HTTP_ACCEPT_LANGUAGE']),rtrim($hashes[$i])) == 0) 
				{
				  $matched_hashes++;				  
				}
			}
			if ($matched_hashes > $GLOBALS['spam_filter_articles']) {
			   $ipspam=1;
			   $this->Log("IP Spam Detected");
			}

			// content block by occam
			if (isset($GLOBALS['spam_filter_content']) ) {

				if ($spam_publish) {
				   $chashes = fopen(CACHE_PATH.'/hashes_content.txt','a');
				   fputs($chashes, md5($_POST['heading'].$_POST['author'].$_POST['summary'].$_POST['article'] )."\n");
				   fclose ($chashes);
				}

			   $chashes = file(CACHE_PATH.'/hashes_content.txt','r');
			   for ( $i=0; $i<count($chashes);$i++)
			   {
	        	   if (strcmp( md5($_POST['heading'].$_POST['author'].$_POST['summary'].$_POST['article']) , rtrim($chashes[$i])) == 0) 
				   {
				   	 $matched_chashes++;					 
				   }
			   }
			   if ($matched_chashes > $GLOBALS['spam_filter_content']) { 
			     $cspam=1;
				 $this->Log("Content Spam Detected");
			   }

			}
			
			if ($cspam == 1 || $ipspam == 1) $ret=1;			

		}

		if (file_exists(CACHE_PATH.'/next_ip_to_block.txt')){
		$lines = file(CACHE_PATH.'/next_ip_to_block.txt' ,1); 	
		$user_ip=trim($_SERVER['REMOTE_ADDR']);
		for ( $i=0 ; $i < count($lines) ; $i++)
		{ 
			$saved_ip = trim($lines[$i]);
		
			//echo strpos( "a".$user_ip,$saved_ip);

			if ( strlen( trim($saved_ip) ) > 0 )
			{
				if 	(strpos("a".$user_ip,$saved_ip)==1) 
				{
					$ret= 1;
					$logline = $_SERVER['REMOTE_ADDR']."|".date("m-d-y g:ia")."|".$_SERVER['HTTP_USER_AGENT']."|".$_SERVER['HTTP_REFERER']."|".$_SERVER['REQUEST_URI'];

					$this->Log($logline);

				} 
			}
		
		}
                if ($GLOBALS['client_deny']) $ret=1;	
       }else{
       	$ret=0;
       }	
		
		return $ret;
	}
	
	/**
	 * empty method.
	 */	
	function Add ($ip) {
	// comming soon for admin interface
	
	}
	
	/**
	 *  empty method.
	 */
	function Remove ($ip) {
	// comming soon for admin interface	
	
	}
	
	/**
	 *  adds an ip to the list of blocked ips.
	 *  @param  string  $text
	 */
	function Log ($text) {

		$log = fopen(CACHE_PATH."/ipblock.log","a");
		fwrite($log, date("m-d-y g:ia").": ".$text."\n");
		fclose($log);	
	
	}

	/**
	 *  logs information about the published article.
	 *  @var    object  $article	article object
	 */
	function log_ip ($article){
		$ip_log_name=CACHE_PATH."/ip_log.txt"; 
		if($GLOBALS['ip_log'] == "ip")
		{
                    if ($_SERVER['HTTP_CLIENT_IP']) $user_ip = $_SERVER['HTTP_CLIENT_IP'];
                    else if ($_SERVER['HTTP_X_FORWARDED_FOR']) $user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    else if ($_SERVER['REMOTE_ADDR']) $user_ip = $_SERVER['REMOTE_ADDR'];
                    else $user_ip = 'UNKNOWN';
		}else{
			$user_ip = "\t";
		}
		$id=$article->article['id'];
		$parent_id=$article->article['parent_id'];
		$title=$article->article['heading'];
		$author=$article->article['author'];
		$time=$article->article['format_created'];
		
		$spamf=fopen($ip_log_name,"a");
		$write="\n$time $user_ip\t$id\t$parent_id\t$author\t$title\n";
		fwrite($spamf, $write);
		fclose($spamf);
	}

}
?>
