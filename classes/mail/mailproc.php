<?php
require_once 'Mail/mimeDecode.php';
$params = array(
  'include_bodies' => TRUE,
  'decode_bodies' => TRUE,
  'decode_headers' => TRUE,
  'input' => file_get_contents('php://stdin'),
  'crlf' => "\r\n",
);
$structure = Mail_mimeDecode::decode($params);
print_r($structure);

function convert_if_not_utf8($string, $charset) {
  if ($charset != 'UTF-8' && $charset != 'UTF8' && $charset != 'utf-8' && $charset != 'utf8') {
    return iconv('CP1252', 'UTF-8//TRANSLIT', $string);
  }
  else {
    return $string;
  }
}

function parse_structure($structure, &$parts, $depth = 0) {
  if ($structure->ctype_primary == 'text') {
    if ($structure->ctype_secondary == 'html') {
      $parts->text->is_html = 1;
      $parts->text->html = convert_if_not_utf8($structure->body, $structure->ctype_parameters['charset']);
    }
    elseif ($structure->ctype_secondary == 'plain') {
      $parts->text->plain = convert_if_not_utf8($structure->body, $structure->ctype_parameters['charset']);
    }
  }
  else {
    $filename = ($structure->ctype_parameters['name']) ? $structure->ctype_parameters['name'] : $structure->d_parameters['filename'];
    $filename = $filename ? $filename : $structure->headers['content-location'];
    if ($filename) {
      $extension = array_pop(explode('.', $filename));
      $filepath = '/home/newswire/Tmp/' . $depth . '_' . preg_replace('/[^a-zA-Z0-9]/', '_', $filename) . '.' . $extension;
      file_put_contents($filepath, $structure->body);
      $parts->files[] = $filepath;
    }
  }
  foreach ((array)$structure->parts as $index => $part) {
    parse_structure($part, $parts, $depth . '_' . $index);
  }
}

$parts->files = array();
parse_structure($structure, $parts);


$formvars = array(
  'title1' => convert_if_not_utf8($structure->headers['subject'], $structure->ctype_parameters['charset']),
  'displayed_author_name' => 'via e-mail',
  'email' =>  convert_if_not_utf8($structure->headers['from'], $structure->ctype_parameters['charset']),
  'summary' =>  convert_if_not_utf8($structure->headers['subject'], $structure->ctype_parameters['charset']),
  'MAX_FILE_SIZE' => 67108864,
);
$formvars['title1'] = $formvars['title1'] ? $formvars['title1'] : 'Via e-mail';
$formvars['summary'] = $formvars['summary'] ? $formvars['summary'] : 'Via e-mail';
if ($parts->text->is_html) {
  $formvars['is_text_html'] = 1; 
  $formvars['text'] = $parts->text->html;
}
else {
  $formvars['text'] = $parts->text->plain;
}
$formvars['text'] = str_replace(array(
  '<p align="center"><font face="Comic Sans MS" color="#0000FF" size="4">This message was sent using picture-talk messaging service from MetroPCS.</font></p>',
  '<p align="center"><a href="http://www.metropcs.com"><img border="0" src="http://www.metropcs.com/images/metroLogo2.gif" alt="metroLogo2.gif" /></a></p>',
), '', $formvars['text']);
foreach ($parts->files as $file => $filename) {
  $count = $file + 1;
  $formvars['linked_file_' . $count] = '@' . $filename;
  $formvars['linked_file_title_' . $count] = 'attachment ' . $file;
}
$formvars['file_count'] = count($parts->files);
$formvars['publish'] = 'Publish';

include('/home/indybay/indybay/indybay-active/classes/config/indybay.cfg');
$formvars['captcha_math'] = '1';
$formvars['captcha_verbal'] = 'A';
$formvars['token'] = hash_hmac('sha256', $formvars['captcha_verbal'] . mktime(date('H'), 0, 0) . $formvars['captcha_math'], INDYBAY_PRIVATE_KEY);

$pub = curl_init();
//curl_setopt($pub, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($pub, CURLOPT_POSTFIELDS, $formvars);
curl_setopt($pub, CURLOPT_URL, 'http://www.indybay.org/publish.php');
curl_exec($pub);
curl_close($pub);
