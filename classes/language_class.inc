<?php
/**
 *  class for language related code.
 *
 *  This class contains all the methods you need to edit the languages of your site.
 *  It contains methods to add, hide, edit and reorder languages. It also caches the arrays
 *  used in the {@link cache::make_selec_form()} method to display forms.
 *
 *  @package	sf-active
 *  @subpackage	language
 */

/**
 *  class for language related code.
 */
class language extends Page 
{
    /**
     * class constructor
     */
    function language()
    {
	return 1 ;
    }

    /**
     *	get a list of all the languages.
     */
    function language_list()
    {
	$db = new DB();
	$sql = "select id, name, language_code, order_num, display, build from language order by order_num asc" ;
	$result = $db->query($sql);
	return $result;
    }

    /**
     *	get all the info on a certain language.
     */
    function language_edit($id)
    {
    	$db = new DB;
	$sql = "select name, language_code, order_num, display, build from language where id = '".$id."'";
	$result_num = $db->query($sql);
	return $result_num ;
    }

    /**
     *	update a certain language.
     */
  function update($languagefields)
  {
	$db = new DB;
	$sql = "update language set name = '".$languagefields[name]."', language_code = '".$languagefields[language_code]."', order_num = '".$languagefields[order_num]."', display = '".$languagefields[display]."', build = '".$languagefields[build]."' where id = '".$languagefields[id]."'";
	$result = $db->execute_statement($sql);
	return $result;
  }

    /**
     *	Add a new language to the system.
     */
  function add($languagefields)
  {
    $db = new DB;
	$sql = "insert into language values ('', '".$languagefields[name]."', '".$languagefields[language_code]."', '".$languagefields[order_num]."', '".$languagefields[display]."', '".$languagefields[build]."')";
	$result = $db->execute_statement($sql);
	return $result;
  }

    /**
     *	hide a language.
     *
     *	This will hide a language in all fields where users can see it.
     *	(like publish forms, search, ....).
     *	The reason we don't delete langauges is that this could make certain
     *	queries acting nasty if a certain language isn't there anymore.
     */
  function hide($languagefields)
  {
    $db = new DB;
	$sql = "update language set display = 'f' where id = '".$languagefields[id]."'";
	$result = $db->execute_statement($sql);
	return $result;
  }

    /**
     *	unhide a language
     */
  function show($languagefields)
  {
    $db = new DB;
	$sql = "update language set display = 't' where id = '".$languagefields[id]."'";
	$result = $db->execute_statement($sql);
	return $result;
  }

    /**
     *	this will cache a $language_option['language_id'] = 'name'; array to disk.
     *
     *	This will cache an array to disk that can be used in select forms.
     *	We save them on disk so we don't have to query the database for it everytime.
     */
    function cache_language_select()
    {
    $db = new DB;
	$sql = "select name, id from language where display = 't' order by order_num asc";
	$result = $db->query($sql);
	$file = "<?\n $"."language_options = array(\n";
	while($row = array_pop($result))
	{
	  $file .= "  ".$row['id']." => \"".$row["name"]."\",\n";
	}
	$file .= ");\n?>";
	$this->cache_file($file, CACHE_PATH."/language_options.inc");
    }

    /**
     *	reorder the langauges.
     */
    function reorder($posted_fields)
    {
	$db = new DB();
	$query1="update language set order_num=";
	$query2=" where id=";
	$querykeys=array_keys($posted_fields);
	$nextkey=array_pop($querykeys);
	while (strlen($nextkey)!=0)
	{
		if (preg_match('/order_num/', $nextkey)) {
			$query=$query1.$posted_fields["$nextkey"];
			$query=$query.$query2.substr($nextkey,9,strlen($nextkey)-9);
			$db->execute_statement($query);
		}
		$nextkey=array_pop($querykeys);
	}
	return 0;
    }

    /**
     *	caches an array of language you wish to create subsites for. (unused atm).
     */
    function cache_builds()
    {
	$db = new DB ;
	$query = "select id, name from language where display = 't' and build = 'y' order by order_num asc";
        $result = $db->query($query);
        $file = "<?\n";
        while($row = array_pop($result))
        {
            $language_id = $row['id'];
	    $language = $row['name'];
            $file .= "\t$"."GLOBALS['build_sites'][$language_id] = \"".$language."\";\n";
        }
        $file .= "\n?>\n";
        $this->cache_file($file, CACHE_PATH."/build_sites.inc");
        return 1 ;
    }

    /**
     *	This will cache the language codes in an array to disk.
     */
    function cache_language_codes()
    {
        $db = new DB ;
        $query = "select id, language_code from language where display = 't' order by order_num asc";
        $result = $db->query($query);
        $file = "<?\n";
        while($row = array_pop($result))
        {
            $file .= "\t$"."language_codes[$row[id]] = \"".$row['language_code']."\";\n";
        }
        $file .= "\n?>\n";
        $this->cache_file($file, CACHE_PATH."/language_codes.inc");
        return 1 ;
    }

    /**
     *	returns the language code associated with a certain language_id.
     */
    function get_language_code($language_id)
    {
	global $language_codes ;
	return $language_codes[$language_id];
    }
	
}
?>
