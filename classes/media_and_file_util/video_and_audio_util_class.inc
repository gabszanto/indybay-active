     function cache_ram_file($upload_dir,$new_file_name,$upload_ext, $media_attachment_id)
     {
          // Cache a RAM file to disk during uploads
          $ram_filename = preg_replace('/\.r[avm]$/i', '.ram', basename($upload_dir . $new_file_name));
          $rm_file=basename($upload_target);
          // construct a rtsp: or pnm: url with: base_url + clean_basename
          $rtsp_url = $GLOBALS['rtsp_base_url'] . "$rm_file";
          $pnm_url  = $GLOBALS['pnm_base_url'] . "$rm_file";
          $target_filename = UPLOAD_PATH . "/$target_filename";
          $ram_dest = UPLOAD_PATH."/".$ram_filename;

          // now assemble the file and cache it
          $ramfile = $rtsp_url . "\n--stop--\n" . $pnm_url;
          $this->cache_file($ram_dest,$ramfile);
          return 1;
     }
