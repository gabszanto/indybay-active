<?php
//--------------------------------------------
//Indybay ImageUtil Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"ImageUtil");

require_once(CLASS_PATH."/common_class.inc");



//main class for fuilemanagement
//this is mainly just a series of wrappers around php functions
//that may be useful if we ever want to change which functions are used
class ImageUtil extends Common
{

	function scale_image_by_width_if_too_large($file_name, $copy_from, $copy_to, $max_width,  $max_height=0, $crop_height_and_fix_width=0, $rotate=0){
	
			$this->track_method_entry("ImageUtil","scale_image_by_width_if_too_large");
			
			$new_file_info=array();;
			$new_file_info["file_name"]=$file_name;
			if (!file_exists($copy_from)){
				echo $copy_from." does not exist!<br />";
				exit;
			}
			if (file_exists($copy_to)){
				echo $copy_to." already exists!<br />";
				exit;
			}
				
			list($width_orig, $height_orig, $type, $attr) = getimagesize($copy_from);
			   $types = array(
			       1 => 'GIF',
			       2 => 'JPG',
			       3 => 'PNG',
			       4 => 'SWF',
			       5 => 'PSD',
			       6 => 'BMP',
			       7 => 'TIFF(intel byte order)',
			       8 => 'TIFF(motorola byte order)',
			       9 => 'JPC',
			       10 => 'JP2',
			       11 => 'JPX',
			       12 => 'JB2',
			       13 => 'SWC',
			       14 => 'IFF',
			       15 => 'WBMP',
			       16 => 'XBM'
			      );
			$new_file_info["image_width"]=$width_orig;
			$new_file_info["image_height"]=$height_orig;
			$new_file_info["file_size"]=filesize($copy_from);
			if ($max_width==0)
				$max_width=$width_orig;
			if ($max_height==0)
				$max_height=$height_orig;
				if ($width_orig > $max_width || $height_orig>$max_height || 
				($crop_height_and_fix_width==1 && ($width_orig!=$max_width || $height_orig!=$max_height)) || $rotate>0) {
					$image_orig="";
					if (strpos(strtolower($copy_to),".jpeg")>0 || strpos(strtolower($copy_to),".jpg")>0){
						$image_orig = imagecreatefromjpeg($copy_from);
					}else if (strpos($copy_to,".png")>0 || strpos($copy_to,".png")>0){
						$image_orig = imagecreatefrompng($copy_from);
						if ($image_orig!=""){
							$copy_to=str_replace(".png", ".jpg",$copy_to);
							$file_name=str_replace(".png", ".jpg",$file_name);
						}
					}else if (strpos($copy_to,".gif")>0 || strpos($copy_to,".gif")>0){
						if ($type==3){
							$image_orig = imagecreatefrompng($copy_from);
							if ($image_orig!=""){
								$copy_to=str_replace(".png", ".jpg",$copy_to);
								$file_name=str_replace(".png", ".jpg",$file_name);
							}
						}else{
							$image_orig = imagecreatefromgif($copy_from);
							if ($image_orig!=""){
								$copy_to=str_replace(".gif", ".jpg",$copy_to);
								$file_name=str_replace(".gif", ".jpg",$file_name);
							}
						}
					}
					if ($image_orig!=""){
						if ($rotate>0){
							$image_orig = $this->ImageRotateRightAngle($image_orig, $rotate) ;
							if ($rotate==90 || $rotate==270){
								$width_orig2=$height_orig;
								$height_orig=$width_orig;
								$width_orig=$width_orig2;
								$new_file_info["image_width"]=$width_orig;
								$new_file_info["image_height"]=$height_orig;
							}
						}
						if ($crop_height_and_fix_width==1){
							$width_new=$max_width;
							$height_new=$height_orig*($max_width/$width_orig);
							if ($height_new<$max_height){
								$height_new=$max_height;
								$width_new=$width_orig*($max_height/$height_orig);
							}
						}else{
							$height_new=$height_orig*($max_width/$width_orig);
							if ($height_new>$max_height){
								$height_new=$max_height;
								$width_new=$width_orig*($max_height/$height_orig);
							}else{
								$width_new=$max_width;
								$height_new=$height_orig*($max_width/$width_orig);
		            		}
		            	}
	            		$new_file_info=array();
	            		$new_file_info["image_width"]=$width_new;
	            		$new_file_info["image_height"]=$height_new;
	            		// fill background white for converted gifs
	            		$image_new = imagecreatetruecolor($width_new, $height_new);
	            		$kek=imagecolorallocate($image_new,
	                 			255,255,255);
	  					imagefill($image_new,0,0,$kek);
	                	imagecopyresampled($image_new,$image_orig,0,0,0,0,$width_new,$height_new,$width_orig,$height_orig);
	                	imageinterlace($image_new, 1);
	                	imagejpeg($image_new, $copy_to, 100);
	                	
	                	if ($crop_height_and_fix_width==1 && ($max_height!=$height_new||$max_width!=$width_new)){
	                		$image_new2 = imagecreatetruecolor($max_width, $max_height);
	            			$kek=imagecolorallocate($image_new2,
	                 			255,255,255);
	  						imagefill($image_new,0,0,$kek);
	                		imagecopy($image_new2,$image_new,0,0,($width_new-$max_width)/2,($height_new-$max_height)/2,$max_width,$max_height);
	                		imageinterlace($image_new2, 1);
	                		imagejpeg($image_new2, $copy_to, 100);
	                		$new_file_info["image_width"]=$max_width;
	            			$new_file_info["image_height"]=$max_height;
						}
						
	                	$new_file_info["file_size"]=filesize($copy_to);
	                	$new_file_info["file_name"]=$file_name;
	             	}else{
	            		copy($copy_from,$copy_to);
	           	 	}
           	 }else{
           	 	copy($copy_from,$copy_to);
           	 }
           	 
           	 $this->track_method_exit("ImageUtil","scale_image_by_width_if_too_large");
           	 
           	 return $new_file_info;
			
	}
	
	function get_image_info_from_full_file_path($full_file_path){
		
		$this->track_method_entry("ImageUtil","get_image_info_from_full_file_path","full_file_path", $full_file_path);
		
		if (!file_exists($full_file_path)){
			$image_info="";
		}else{
			$image_size_info = getimagesize($full_file_path);
			$image_width = $image_size_info[0] ;
			$image_height= $image_size_info[1];
			$file_size=filesize($full_file_path);
			$image_info=  array();
			$image_info["image_width"]=$image_width;
			$image_info["image_height"]=$image_height;
			$image_info["file_size"]=$file_size;
		}
		
		$this->track_method_exit("ImageUtil","get_image_info_from_full_file_path");
		
		return $image_info;
	}
	
	
     function make_image_for_pdf($media_attachment_id, $file_info){
     	$media_attachment_db_class= new MediaAttachmentDB;   		
     	$upload_path=UPLOAD_PATH."/";
	    $date_rel_path=$file_info["relative_path"];
	    $upload_dir=$upload_path.$date_rel_path;
	    $existing_full_file_path=$upload_dir.$file_info["file_name"];
	    $small_worked=false;
	    $large_worked=false;
	    
	    $out="";
     	$ret="";
	    
	    if (file_exists($existing_full_file_path)){
	    	if (!is_readable($existing_full_file_path)){
	    		sleep(5);
	    	}
	    	if (!is_readable($existing_full_file_path)){
	    		echo "<!--Couldnt read file to convert to pdf-->";
     			return "";
	    	}
     	}else{
     		echo "<!--Couldnt find file to convert to pdf-->";
     		return "";
     	}
	$exec_command=IMAGE_MAGICK_CONVERT." -colorspace rgb \"".escapeshellcmd($existing_full_file_path)."[0]\" -bordercolor white -border 0 -alpha remove -background white -resize \"140x140\" -trim  \"".escapeshellcmd($existing_full_file_path)."_140_.jpg\" ";

     	exec($exec_command, $out, $ret);
     	
     	$fs=0;
     	$fs=@filesize($existing_full_file_path."_140_.jpg");
     	if ($fs+0>10){
     		list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path."_140_.jpg");
	     	if ($type==2 && $testw+0>0){
	     		$image_orig = imagecreatefromjpeg($existing_full_file_path."_140_.jpg");
	     		$image_new = imagecreatetruecolor($testw, $testh);
	            $kek=imagecolorallocate($image_new,255,255,255);
	  			imagefill($image_new,0,0,$kek);
	            imagecopyresampled($image_new,$image_orig,0,0,0,0,$testw,$testh,$testw,$testh);
	            imageinterlace($image_new, 1);
	            imagejpeg($image_new, $existing_full_file_path."_140_.jpg", 100);
	     		if (filesize($existing_full_file_path."_140_.jpg")+0>10){
     				list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path."_140_.jpg");
	     			if ($type==2 && $testw+0>0){
	     				$small_worked=true;
	     			}
	     		}
	     	}
     	}
	$exec_command=IMAGE_MAGICK_CONVERT." -colorspace rgb \"".escapeshellcmd($existing_full_file_path)."[0]\" -bordercolor white -border 0 -alpha remove -background white -resize \"600x900\" -trim \"".escapeshellcmd($existing_full_file_path)."_600_.jpg\" ";
     	exec($exec_command);

     	$fs=0;
     	$fs=@filesize($existing_full_file_path."_600_.jpg");

     	if ($fs+0>10){
  
     		list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path."_600_.jpg");
	     	if ($type==2 && $testw+0>0){
	     		$image_orig = imagecreatefromjpeg($existing_full_file_path."_600_.jpg");
	     		$image_new = imagecreatetruecolor($testw, $testh);
	            $kek=imagecolorallocate($image_new,255,255,255);
	  			imagefill($image_new,0,0,$kek);
	            imagecopyresampled($image_new,$image_orig,0,0,0,0,$testw,$testh,$testw,$testh);
	            imageinterlace($image_new, 1);
	            imagejpeg($image_new, $existing_full_file_path."_600_.jpg", 100);
	     		if (filesize($existing_full_file_path."_600_.jpg")+0>10){
     				list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path."_600_.jpg");
	     			if ($type==2 && $testw+0>0){
	     				$large_worked=true;
	     			}
	     		}
	     	}
     	}else{
     		echo "\n<!--Couldn't generate thumbnail for pdf-->\n";
     	}
     	$associated_attachment_ids=array();

		$alt_tag=$file_info["file_name"];
     	if ($large_worked){
		
     		$renderered_pdf_image_id=$media_attachment_db_class->add_media_attachment($file_info["file_name"]."_600_.jpg", $file_info["file_name"]."_600_.jpg", $date_rel_path, $alt_tag,  $file_info["original_file_name"],
		    				   15, 0,UPLOAD_TYPE_THUMBNAIL_MEDIUM, UPLOAD_STATUS_VALIDATED, $media_attachment_id, 0, 0,0);
			$associated_attachment_ids["renderered_pdf_image"]=$renderered_pdf_image_id;
		}
		if ($small_worked){

			$small_thumbnail_media_attachment_id=$media_attachment_db_class->add_media_attachment($file_info["file_name"]."_140_.jpg", $file_info["file_name"]."_140_.jpg", $date_rel_path, $alt_tag,  $file_info["original_file_name"],
		    				   15, 0,UPLOAD_TYPE_THUMBNAIL_SMALL, UPLOAD_STATUS_VALIDATED, $media_attachment_id, 0, 0,0);
			$associated_attachment_ids["thumbnail_media_attachment_id"]=$small_thumbnail_media_attachment_id;
		}		   	   	
     }

    function make_image_for_video($media_attachment_id, $file_info) {
		$media_attachment_db_class = new MediaAttachmentDB; 		
	 	$upload_path = UPLOAD_PATH . '/';
		$date_rel_path = $file_info['relative_path'];
		$upload_dir = $upload_path . $date_rel_path;
		$existing_full_file_path = $upload_dir.$file_info["file_name"];
		$small_worked = false;
		$large_worked = false;
	 	$exec_command = '/usr/local/bin/ffmpegthumbnailer -f -s 160 -i "' . $existing_full_file_path . '" -o ' . $existing_full_file_path . '_160_.png';
	 	exec($exec_command);
	 	$fs = 0;
	 	$fs = @filesize($existing_full_file_path . '_160_.png');
	 	if ($fs+0 > 10) {
			list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path . '_160_.png');
		 	if ($type == 3 && $testw+0 > 0) {
		 		$image_orig = imagecreatefrompng($existing_full_file_path . '_160_.png');
		 		$image_new = imagecreatetruecolor($testw, $testh);
				$kek = imagecolorallocate($image_new, 255, 255, 255);
				imagefill($image_new, 0, 0, $kek);
		 		imagecopyresampled($image_new, $image_orig, 0, 0, 0, 0, $testw, $testh, $testw, $testh);
	 		imageinterlace($image_new, 1);
		 		imagepng($image_new, $existing_full_file_path . '_160_.png');
		 		if (filesize($existing_full_file_path . '_160_.png')+0 > 10) {
 					list($small_testw, $small_testh, $type, $attr) = getimagesize($existing_full_file_path . '_160_.png');
		 			if ($type == 3 && $small_testw+0 > 0) {
		 				$small_worked = true;
		 			}
		 		}
		 	}
 		}
 		$exec_command = '/usr/local/bin/ffmpegthumbnailer -f -s 600 -i "' . $existing_full_file_path . '" -o ' . $existing_full_file_path . '_600_.png';
 		exec($exec_command);
 		$fs = 0;
 		$fs = @filesize($existing_full_file_path . '_600_.png');
 		if ($fs+0 > 10) {
 			list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path . '_600_.png');
		 	if ($type == 3 && $testw+0 > 0) {
		 		$image_orig = imagecreatefrompng($existing_full_file_path . '_600_.png');
		 		$image_new = imagecreatetruecolor($testw, $testh);
		 		$kek = imagecolorallocate($image_new, 255, 255, 255);
		 		imagefill($image_new, 0, 0, $kek);
		 		imagecopyresampled($image_new, $image_orig, 0, 0, 0, 0, $testw, $testh, $testw, $testh);
		 		imageinterlace($image_new, 1);
		 		imagepng($image_new, $existing_full_file_path . '_600_.png');
		 		if (filesize($existing_full_file_path . '_600_.png')+0 > 10) {
 					list($testw, $testh, $type, $attr) = getimagesize($existing_full_file_path . '_600_.png');
		 			if ($type == 3 && $testw+0 > 0) {
		 				$large_worked = true;
		 			}
		 		}
		 	}
	 	}
	 	$associated_attachment_ids = array();
                // @fixme: 'alt_tag' not defined but should be, so define it here:
                $file_info['alt_tag'] = isset($file_info['alt_tag']) ? $file_info['alt_tag'] : '';
	 	if ($large_worked) {
			$renderered_video_image_id = $media_attachment_db_class->add_media_attachment($file_info['file_name'] . '_600_.png',
				$file_info['file_name'] . '_600_.png', $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
				16, 0, UPLOAD_TYPE_THUMBNAIL_MEDIUM, UPLOAD_STATUS_VALIDATED, $media_attachment_id, 0, $testw, $testh);
			$associated_attachment_ids['renderered_video_image'] = $renderered_video_image_id;
		}
		if ($small_worked) {
			$small_thumbnail_media_attachment_id = $media_attachment_db_class->add_media_attachment($file_info['file_name'] . '_160_.png',
				$file_info['file_name'] . '_160_.png', $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
				16, 0, UPLOAD_TYPE_THUMBNAIL_SMALL, UPLOAD_STATUS_VALIDATED, $media_attachment_id, 0, $small_testw, $small_testh);
			$associated_attachment_ids['thumbnail_media_attachment_id'] = $small_thumbnail_media_attachment_id;
		}
                if (!file_exists($existing_full_file_path .'_preview_.flv') && $file_info['media_type_id'] != 35 && $file_info['media_type_id'] != 45) {
                  exec('/usr/local/bin/ffmpeg -i '. escapeshellarg($existing_full_file_path) .' -ar 22050 -f flv -qscale 13 '. escapeshellarg($existing_full_file_path) .'_preview_.flv');
                  if (file_exists($existing_full_file_path .'_preview_.flv') && filesize($existing_full_file_path .'_preview_.flv')) {
                    $media_attachment_db_class->add_media_attachment($file_info['file_name'] . '_preview_.flv',
                    $file_info['file_name'] . '_preview_.flv', $date_rel_path, $file_info['alt_tag'], $file_info['original_file_name'],
                    35, 0, UPLOAD_TYPE_FLV, UPLOAD_STATUS_VALIDATED, $media_attachment_id, 0, 0, 0);
                  }
                }
    }
    
    
    
// $imgSrc - GD image handle of source image
// $angle - angle of rotation. Needs to be positive integer
// angle shall be 0,90,180,270, but if you give other it
// will be rouned to nearest right angle (i.e. 52->90 degs,
// 96->90 degs)
// returns GD image handle of rotated image.
function ImageRotateRightAngle( $imgSrc, $angle )
{
    // ensuring we got really RightAngle (if not we choose the closest one)
    $angle = min( ( (int)(($angle+45) / 90) * 90), 270 );

    // no need to fight
    if( $angle == 0 )
        return( $imgSrc );

    // dimenstion of source image
    $srcX = imagesx( $imgSrc );
    $srcY = imagesy( $imgSrc );

    switch( $angle )
        {
        case 90:
            $imgDest = imagecreatetruecolor( $srcY, $srcX );
            for( $x=0; $x<$srcX; $x++ )
                for( $y=0; $y<$srcY; $y++ )
                    imagecopy($imgDest, $imgSrc, $srcY-$y-1, $x, $x, $y, 1, 1);
            break;

        case 180:
            $imgDest = $this->imageFlip( $imgSrc, IMAGE_FLIP_BOTH );
            break;

        case 270:
            $imgDest = imagecreatetruecolor( $srcY, $srcX );
            for( $x=0; $x<$srcX; $x++ )
                for( $y=0; $y<$srcY; $y++ )
                    imagecopy($imgDest, $imgSrc, $y, $srcX-$x-1, $x, $y, 1, 1);
            break;
        }

    return( $imgDest );
} 
	
	function imageflip($image, $mode) {
        $w = imagesx($image);
        $h = imagesy($image);
        $flipped = imagecreate($w, $h);
        if ($mode) {
                for ($y = 0; $y < $h; $y++) {
                        imagecopy($flipped, $image, 0, $y, 0, $h - $y - 1, $w, 1);
                }
        } else {
                for ($x = 0; $x < $w; $x++) {
                        imagecopy($flipped, $image, $x, 0, $w - $x - 1, 0, 1, $h);
                }
        }
        return $flipped;
}

	
} //end class
?>
