<?php
//--------------------------------------------
//Indybay UploadUtil Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"UploadUtil");

require_once(CLASS_PATH."/media_and_file_util/file_util_class.inc");
require_once(CLASS_PATH."/media_and_file_util/image_util_class.inc");
require_once(CLASS_PATH."/cache/cache_class.inc");
require_once(CLASS_PATH."/common_class.inc");
require_once(CLASS_PATH."/db/media_attachment_db_class.inc");
require_once(CLASS_PATH."/legacy_support/legacy_attachment_migration_class.inc");

//class which handles moving and managing uploadced files
class UploadUtil extends Common
{


    //when a file is uploadded with a post, attachment or comment this code is run
    function process_anonymous_upload($tmp_file_path,$original_file_name,$mime_type_from_post, $file_isnt_in_tmp_location=0){

    	$this->track_method_entry("UploadUtil","process_anonymous_upload", "original_file_name", $original_file_name);
    	
    	$file_info=array();
        $cache_class= new Cache;

    	if (!isset($original_file_name) || trim($original_file_name)==""){
	     		echo "original file name was null";
	     		exit;
	     }
	    		
        $original_file_name = preg_replace('/[^a-zA-Z0-9._-]/', '_', $original_file_name);
        $original_file_name = strtolower($original_file_name);
        preg_match('/([a-z0-9._-]+)\.([a-z0-9]+)/', $original_file_name, $regs);
	
        $upload_ext = $regs[2];
        $upload_filepath="";
		if (strlen($upload_ext) == 0)
        {
        	$upload_ext = $mime_extensions[chop($mime_type_from_post)];
            $original_file_name.="." . $upload_ext;
        }
        if (strlen($upload_ext) == 0){
        		echo "<strong>FATAL ERROR</strong>: Unable To Determine File Type Of Upload
        		 (Please make sure it ends in an extension like .jpg, mp3 etc...)";
	        	exit;
        }
        //get media_type_id and grouping id and if not found reject
        $media_attachment_db_class= new MediaAttachmentDB();
        $media_type_id=$media_attachment_db_class->get_media_type_id_from_file_extension_and_mime($upload_ext,$mime_type_from_post);
  
        if ($media_type_id==0){
        	$file_util_class=new FileUtil;
        	$file_util_class->delete_file($tmp_file_path);
        	$file_info=0;
        }else{

	        $upload_path=UPLOAD_PATH."/";
	        $news_item_created_stamp=time();
	        $date_rel_path=$cache_class->create_date_based_relative_directory($upload_path, $news_item_created_stamp);
	        $upload_dir=$upload_path.$date_rel_path;
	        
	        $new_file_name = $original_file_name;
	     	$full_file_path_and_name=$upload_dir.$new_file_name;

	     	$i=0;
	     	while (file_exists($full_file_path_and_name)){
	     		if ($i==200){
	     			echo "problem renaming file".$full_file_path_and_name;
	     			exit;
	     		}
	     		 $new_file_name=$this->make_file_name_original($new_file_name);
	     		 $full_file_path_and_name=$upload_dir.$new_file_name;
	     		$i=$i+1;
	     	}

	     	if ($file_isnt_in_tmp_location!=0){
	     		if (!rename($tmp_file_path,$full_file_path_and_name)
	     		){
		            echo "<strong>FATAL ERROR</strong>: Move failed For non-temp File. No disk space? :( \n";
		        	exit;
		        }
	     	}else{
		     	if (!move_uploaded_file($tmp_file_path,$full_file_path_and_name)){
		            echo "<strong>FATAL ERROR</strong>: Move failed! No disk space? :( \n";
		        	exit;
		        }else{
		        	chmod ($full_file_path_and_name, 0644);
		        }
	        }
	        $file_info["media_type_id"]=$media_type_id;
	        $file_info["file_extension"]= $upload_ext;
	        $file_info["file_name"]=$new_file_name;
	        $file_info["original_file_name"]=$original_file_name;
	        $file_info["relative_path"]=$date_rel_path;
	    }
        
        $this->track_method_exit("UploadUtil","process_anonymous_upload");
        
		return $file_info;
    }
    
    
    
    
    //needed so we can rename images uploaded with same name for same day
    function make_file_name_original($new_file_name){
    	
    	$this->track_method_entry("UploadUtil","make_file_name_original","new_file_name", $new_file_name);
    	
    	if (trim($new_file_name)==""){
    		echo $new_file_name." is not a valid file name ";
    		exit;
    	}
    	$i=strrpos($new_file_name,".");
    	$j=strrpos($new_file_name,"_")+0;
    	$k=0;
    	$suffix=substr($new_file_name,$i);
    	if ($j>$i){
    		$prefix=substr($new_file_name,0,$j);
    		$k=substr($new_file_name, $j+1,$i-$j-1);
    		if ("$k"!="".($k+0).""){
    			$k=0;
    			$prefix=substr($new_file_name,0,$i);
    		}
    	}else{
    		$prefix=substr($new_file_name,0,$i);
    	}
    	$k=$k+1;
    	$new_file_name=$prefix."_".$k.$suffix;
    	
    	$this->track_method_exit("UploadUtil","make_file_name_original", "new_file_name", $new_file_name);
    	
    	return $new_file_name;
    
    }
    
    
    
    
    //handles secondary files like thumnails, torrents and streaming files
    function make_secondary_files_and_associate($media_attachment_id, $file_info){
    	$this->track_method_entry("UploadUtil","make_secondary_files_and_associate");
    	
    	$media_attachment_db_class= new MediaAttachmentDB();
    	$associated_attachment_ids=array();
    	$associated_attachment_ids["media_attachment_id"]=$media_attachment_id;
    	
    	$media_type_id=$file_info["media_type_id"];
    	$media_type_grouping_id=$media_attachment_db_class->get_media_type_grouping_id_from_type_id($media_type_id);
		
		$image_util= new ImageUtil();
		
    	if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_IMAGE) {
    		//rotate image if camera has identified that the image was taken sideways
            if ($file_info['media_type_id'] == 15 || $file_info['media_type_id'] == 25) {
                exec('PATH=$PATH:/usr/local/bin /usr/local/bin/exifautotran ' . escapeshellarg(UPLOAD_PATH .'/'. $file_info['relative_path'] . $file_info['file_name']));
            }
    	    $associated_attachment_ids=$this->make_secondary_image_files($media_attachment_id, $file_info);
    	}
		else if ($media_type_id == 2) {
    	 	$image_util->make_image_for_pdf($media_attachment_id, $file_info);
    	}
		else if ($media_type_grouping_id == MEDIA_TYPE_GROUPING_VIDEO) {
			$image_util->make_image_for_video($media_attachment_id, $file_info);
    	}
    	$associated_attachment_ids["media_type_grouping_id"]=$media_type_grouping_id;
    	
    	$this->track_method_exit("UploadUtil","make_secondary_files_and_associate");
    	
    	return $associated_attachment_ids;
    }
    
	function make_secondary_image_files($media_attachment_id, $file_info){
   			
   		$this->track_method_entry("UploadUtil","make_secondary_image_files");
    		
    	if (!isset($file_info["file_name"]) || trim($file_info["file_name"])==""){
    		echo "make_secondary_image_files called ewith null file name";
    		exit;
    	}
    	
    	$upload_path=UPLOAD_PATH."/";
	    $date_rel_path=$file_info["relative_path"];
	    $upload_dir=$upload_path.$date_rel_path;
	    $existing_full_file_path=$upload_dir.$file_info["file_name"];
	    $image_util_class= new ImageUtil;
    	$media_type_id=$file_info["media_type_id"];
 		$media_attachment_db_class= new MediaAttachmentDB;   		
		
		$associated_attachment_ids=array();
		$associated_attachment_ids["media_attachment_id"]=$media_attachment_id;
    		$image_info=$image_util_class->get_image_info_from_full_file_path($existing_full_file_path);
			
			$media_attachment_db_class->update_image_and_file_size($media_attachment_id, $image_info["file_size"], $image_info["image_width"], $image_info["image_height"]);
			
	    		if ($image_info['image_width'] > POSTED_IMAGE_MAX_WIDTH || $image_info['image_height'] > POSTED_IMAGE_MAX_HEIGHT) {
	    			$new_file_name = POSTED_IMAGE_MAX_WIDTH."_".$file_info["file_name"];
	    			$full_file_path=$upload_dir.$new_file_name;
					$i=0;
	    			while (file_exists($full_file_path)){
	    				$new_file_name=$this->make_file_name_original($new_file_name);
	    				$full_file_path=$upload_dir.$new_file_name;
	    				$i=$i+1;
			     		if ($i==200){
			     			echo "problem renaming file".$full_file_path_and_name;
			     			exit;
			     		}
	    			}
	    			
	    			$new_file_info=$image_util_class->scale_image_by_width_if_too_large($new_file_name,
	    				$existing_full_file_path, $full_file_path, POSTED_IMAGE_MAX_WIDTH, POSTED_IMAGE_MAX_HEIGHT);
	    
	    			if (is_array($new_file_info)){
	    				$new_file_name=$new_file_info["file_name"];
	    				if (!array_key_exists("upload_name",$file_info))
	    					$file_info["upload_name"]="";
	    				if (!array_key_exists("alt_tag",$file_info))
	    					$file_info["alt_tag"]="";
		    			$resized_media_attachment_id=$media_attachment_db_class->add_media_attachment($file_info["upload_name"], $new_file_name, $date_rel_path, $file_info["alt_tag"],  $file_info["original_file_name"],
		    				   $media_type_id, 0,UPLOAD_TYPE_POST, UPLOAD_STATUS_VALIDATED, 0, $new_file_info["file_size"], $new_file_info["image_width"], $new_file_info["image_height"]);
		    			$media_attachment_db_class->update_type_and_parent($media_attachment_id, UPLOAD_TYPE_POST_ORIGINAL_BEFORE_RESIZE, $resized_media_attachment_id);
		    			$associated_attachment_ids["media_attachment_id"]=$resized_media_attachment_id;
		    			$associated_attachment_ids["original_media_attachment_id"]=$media_attachment_id;
		    		}
		    	}
		    	$thumbnail_info=$this->generate_thumbnails_for_media_attachment_from_info($associated_attachment_ids["media_attachment_id"], $image_info, $file_info);
				$associated_attachment_ids["thumbnail_media_attachment_id"]=$thumbnail_info["small_thumbnail_media_attachment_id"];
    	
    	$this->track_method_exit("UploadUtil","make_secondary_image_files");

	return $associated_attachment_ids;
    }
    
    
    
    
    
    
    function generate_thumbnails_for_media_attachment_from_id($media_attachment_id){
    	
    	$this->track_method_entry("UploadUtil","generate_thumbnails_for_media_attachment_from_id", "media_attachment_id", $media_attachment_id);
    	
    	$thumbnail_info=array();
    	
    	if ($media_attachment_id+0==0){
    		echo "generate_thumbnails_for_media_attachment_from_id called with media_attachment_id=0<br />";
    		$thumbnail_info=0;
    	}else{
	    	$media_attachment_db= new MediaAttachmentDB();
	    	$upload_path=UPLOAD_PATH."/";
	    	$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
	    	if (!is_array($media_attachment_info)){
	    		echo "error occured getting media_attachment_id ".$media_attachment_id;
	    		$thumbnail_info=0;
	    	}else if (sizeof($media_attachment_info)==0){
	    		$thumbnail_info=0;
	    	}else{
		    	if ($media_attachment_info["upload_status_id"]!=UPLOAD_STATUS_VALIDATED){
		    		$media_attachment_info=$this->verify_attachment($media_attachment_info);
		        }
			    $date_rel_path=$media_attachment_info["relative_path"];
			    $upload_dir=$upload_path.$date_rel_path;
			    $existing_full_file_path=$upload_dir.$media_attachment_info["file_name"];
			    $image_util_class= new ImageUtil;
		    	$image_info=$image_util_class->get_image_info_from_full_file_path($existing_full_file_path);
		    	$thumbnail_info=$this->generate_thumbnails_for_media_attachment_from_info($media_attachment_id,$image_info,$media_attachment_info);
	    		
	    	}
    	}
    	$this->track_method_exit("UploadUtil","generate_thumbnails_for_media_attachment_from_id");
    	return $thumbnail_info;
    }
    
    
    
    
    
    
    
    function generate_thumbnails_for_media_attachment_from_info($media_attachment_id,$image_info, $file_info){
    	
    	$this->track_method_entry("UploadUtil","generate_thumbnails_for_media_attachment_from_info", "media_attachment_id", $media_attachment_id);
    	if (!is_array($image_info) || !is_array($file_info) || sizeof($image_info)==0 || sizeof($file_info)==0)
    		return "";
    	
    	if (!isset($file_info["file_name"]) || trim($file_info["file_name"])==""){
    		echo "generate_thumbnails_for_media_attachment_from_info called with null filename";
    		exit;
    	}
    	
    	$media_attachment_db_class= new MediaAttachmentDB();
    	$image_util_class= new ImageUtil();
    	$upload_path=UPLOAD_PATH."/";
	    $date_rel_path=$file_info["relative_path"];
	    $upload_dir=$upload_path.$date_rel_path;
	    $existing_full_file_path=$upload_dir.$file_info["file_name"];
	    $image_util_class= new ImageUtil;
    	$media_type_id=$file_info["media_type_id"];
    	
    		
		$thumbnail_info["medium_thumbnail_media_attachment_id"]=$media_attachment_id;
		$thumbnail_info["small_thumbnail_media_attachment_id"]=$media_attachment_id;
		
		
		$medium_thumbnail_id=$media_attachment_db_class->get_thumbnail_given_parent_id_and_type($media_attachment_id,UPLOAD_TYPE_THUMBNAIL_MEDIUM);
    	
    	if ($medium_thumbnail_id!=0){
    		$thumbnail_info["medium_thumbnail_media_attachment_id"]=$medium_thumbnail_id;
    	}else{
	    	if ($image_info["image_width"]>THUMBNAIL_WIDTH_MEDIUM || $image_info["image_height"]>THUMBNAIL_HEIGHT_MEDIUM){

					$new_file_name=THUMBNAIL_WIDTH_MEDIUM."_".$file_info["file_name"];
		    			$full_file_path=$upload_dir.$new_file_name;
						$i=0;
		    			while (file_exists($full_file_path)){
		    				$new_file_name=$this->make_file_name_original($new_file_name);
		    				$full_file_path=$upload_dir.$new_file_name;
		    				$i=$i+1;
				     		if ($i==300){
				     			echo "generate_thumbnails_for_media_attachment_from_info: problem renaming file".$full_file_path;
				     			exit;
				     		}
		    			}
					$new_file_info=$image_util_class->scale_image_by_width_if_too_large($new_file_name,
		    				$existing_full_file_path, $full_file_path, THUMBNAIL_WIDTH_MEDIUM, THUMBNAIL_HEIGHT_MEDIUM, 1);
					if (is_array($new_file_info)){
						$new_file_name=$new_file_info["file_name"];
						if (!array_key_exists("upload_name",$file_info))
							$file_info["upload_name"]=$file_info["file_name"];
						if (!array_key_exists("alt_tag",$file_info))
							$file_info["alt_tag"]=$file_info["file_name"];
						$resized_media_attachment_id=$media_attachment_db_class->add_media_attachment($file_info["upload_name"], $new_file_name, $date_rel_path, $file_info["alt_tag"],  $file_info["original_file_name"],
			    				   $media_type_id, 0,UPLOAD_TYPE_THUMBNAIL_MEDIUM, UPLOAD_STATUS_VALIDATED, $media_attachment_id, $new_file_info["file_size"], $new_file_info["image_width"],$new_file_info["image_height"]);
			    		}
			    		$thumbnail_info["medium_thumbnail_media_attachment_id"]=$resized_media_attachment_id;
			    	}
		}
		
		$small_thumbnail_id=$media_attachment_db_class->get_thumbnail_given_parent_id_and_type($media_attachment_id,UPLOAD_TYPE_THUMBNAIL_SMALL);
    	if ($small_thumbnail_id!=0){
    		$thumbnail_info["small_thumbnail_media_attachment_id"]=$small_thumbnail_id;
    	}else{
			if ($image_info["image_width"]>THUMBNAIL_WIDTH_SMALL || $image_info["image_height"]>THUMBNAIL_HEIGHT_SMALL ){
			
				$new_file_name=THUMBNAIL_WIDTH_SMALL."_".$file_info["file_name"];
				$full_file_path=$upload_dir.$new_file_name;
				$i=0;
				while (file_exists($full_file_path)){
					$new_file_name=$this->make_file_name_original($new_file_name);
					$full_file_path=$upload_dir.$new_file_name;
					$i=$i+1;
		     		if ($i==300){
		     			echo "generate_thumbnails_for_media_attachment_from_info: problem renaming file".$full_file_path;
		     			exit;
		     		}
				}
				$new_file_info=$image_util_class->scale_image_by_width_if_too_large($new_file_name,
	    				$existing_full_file_path, $full_file_path, THUMBNAIL_WIDTH_SMALL, THUMBNAIL_HEIGHT_SMALL,1);
				if (is_array($new_file_info)){
					$new_file_name=$new_file_info["file_name"];
					if (!array_key_exists("upload_name",$file_info))
						$file_info["upload_name"]="";
					if (!array_key_exists("alt_tag",$file_info))
						$file_info["alt_tag"]="";
					$resized_media_attachment_id=$media_attachment_db_class->add_media_attachment($file_info["upload_name"], $new_file_name, $date_rel_path, $file_info["alt_tag"],  $file_info["original_file_name"],
		    				   $media_type_id, 0,UPLOAD_TYPE_THUMBNAIL_SMALL, UPLOAD_STATUS_VALIDATED, $media_attachment_id, $new_file_info["file_size"], $new_file_info["image_width"],$new_file_info["image_height"]);
					$thumbnail_info["small_thumbnail_media_attachment_id"]=$resized_media_attachment_id;
		    		}
		    	}
	    }	
	    	
    	$this->track_method_exit("UploadUtil","generate_thumbnails_for_media_attachment_from_info");
    
    	return $thumbnail_info;
    }
    
    
    //determines if an upload that is to be rendered actually exists
    //updates info in the DB if it does and calls legacy migration classes to
    //get a copy of the file if it does not
    function verify_attachment($media_attachment_info){
    	
    	$this->track_method_entry("UploadUtil","verify_attachment");
    	
    	$sucess=0;
    	
    	if (!is_array($media_attachment_info) || $media_attachment_info["media_attachment_id"]+0==0){
    		print_r($media_attachment_info);
    		echo "invalid media_attachment_info handed to verify_attachment<br/>";
    		$success=0;
    	}else{
			$success=1;
			$media_attachment_db_class= new MediaAttachmentDB;
			$cache_class= new Cache();
			
	    	//check to see if the attachment is legacy not loaded status and if so call
	    	//legacy attachment class to get upload
	   		$upload_status_id=$media_attachment_info["upload_status_id"];
			$upload_type_id=$media_attachment_info["upload_type_id"];
			$media_attachment_id=$media_attachment_info["media_attachment_id"];
			$creation_timestamp=$media_attachment_info["creation_timestamp"];
			
			if ($upload_status_id==UPLOAD_STATUS_NOT_VALIDATED_MIGRATED || DEBUG_MIGRATION){
			
				$legacy_attachment_migration_class=new LegacyAttachmentMigration();
				$correct_new_file_name=$media_attachment_info["file_name"];
				$i=strripos($correct_new_file_name, "/");
				if ($i>0)
					$correct_new_file_name=substr($correct_new_file_name,$i+1);

				if ($upload_type_id==UPLOAD_TYPE_ADMIN_UPLOAD ){
					$relative_dir="admin_uploads/".$cache_class->create_date_based_relative_directory(UPLOAD_PATH."/admin_uploads/", $creation_timestamp);
					$sucess=$legacy_attachment_migration_class->attempt_to_migrate_legacy_blurb_attachment
						($media_attachment_info, UPLOAD_ROOT.$relative_dir);
				}else{
					$creation_timestamp=$media_attachment_info["creation_timestamp"];
					$relative_dir=$cache_class->create_date_based_relative_directory(UPLOAD_PATH."/", $creation_timestamp);
					$success=$legacy_attachment_migration_class->attempt_to_migrate_legacy_post_attachment
						($media_attachment_info, UPLOAD_ROOT.$relative_dir);
				}	
				if ($success==1){
					$media_attachment_db_class->update_file_location_and_status
						($media_attachment_id, $relative_dir,$correct_new_file_name, UPLOAD_STATUS_VALIDATED);
					$media_attachment_info["upload_status_id"]=UPLOAD_STATUS_VALIDATED;
					$media_attachment_info["relative_path"]=$relative_dir;
					$media_attachment_info["file_name"]=$correct_new_file_name;
				}else{
					$media_attachment_db_class->update_upload_status($media_attachment_id, UPLOAD_STATUS_FAILED);
					$media_attachment_info["upload_status_id"]=UPLOAD_STATUS_FAILED;
				}
	 		}
	 		
			if ($sucess==1){
	    	
	    		//check to see if all info is available for file and
	    		//if not fill in the missing info
	    		$relative_path=$media_attachment_info["relative_path"];
	    		$file_name=$media_attachment_info["file_name"];
				if (strpos($relative_path, "uploads/")>0)
					$intpath=UPLOAD_ROOT.substr($relative_path,8);    	
	    		else
	    			$intpath="/uploads";
	    		if (substr($relative_path, 0,1)!="/"){
	    			$intpath.="/";
	    		}
	    		$full_path=INDYBAY_BASE_PATH.$intpath.$relative_path.$file_name;
	    		if (file_exists($full_path) && filesize($full_path)>0){
	    			$file_size=filesize($full_path);
	    			$media_attachment_db_class = new MediaAttachmentDB();
	    			$media_attachment_db_class->
	    				update_upload_status($media_attachment_info["media_attachment_id"], 1);
	    			$media_attachment_info["upload_status_id"]=1;
	    		}
	    	}
	    }
    	$this->track_method_exit("UploadUtil","verify_attachment");
    	
    	return $media_attachment_info;
    }
    
    
    function process_admin_media_upload(
			$temp_file_name,
			$original_file_name,
			$mime_type,
			$restrict_to_width,$restrict_to_height,
			$image_name, $alt_text
		){
		
			
			$this->track_method_entry("UploadUtil","process_admin_media_upload", "original_file_name", $original_file_name);
    	
    		$media_attachment_db_class=  new MediaAttachmentDB;
			$cache_class=  new Cache;
    		$file_size=0;
    		$image_width=0;
    		$image_height=0;
            $copy_from = $temp_file_name;
            $upload_partial_rel_path="/admin_uploads/";

            $original_file_name = str_replace(" ", "", $original_file_name); 
	    	
	    	if (!isset($original_file_name) || trim($original_file_name)==""){
		     		echo "original file name was null";
		     		exit;
		     }
            preg_match('/([a-zA-Z0-9._-]+)\.([a-zA-Z0-9]+)/', $original_file_name, $regs);
        	$upload_ext = $regs[2];
        	
        	$media_attachment_db_class= new MediaAttachmentDB();
        	$media_type_id=$media_attachment_db_class->get_media_type_id_from_file_extension_and_mime($upload_ext,$mime_type);

			$created_by_id=$_SESSION['session_user_id'];
			$new_file_name=$original_file_name;
			
			$date_rel_path=$cache_class->create_date_based_relative_directory(UPLOAD_PATH ."/". $upload_partial_rel_path, time());
			if ($date_rel_path==0){
				$had_error=1;
			}else{
				$upload_rel_path=$upload_partial_rel_path.$date_rel_path;
				$upload_web_path="/uploads/".$upload_rel_path;
				$copy_to = UPLOAD_PATH . $upload_rel_path.$new_file_name;
				
	     		while (file_exists( $copy_to)){
	     		 	$new_file_name=$this->make_file_name_original($new_file_name);
	     		 	$copy_to=UPLOAD_PATH . $upload_rel_path.$new_file_name;
	     		}
				$image_util_class= new ImageUtil();
            	if ($restrict_to_height>0){
            		$file_info=$image_util_class->scale_image_by_width_if_too_large($new_file_name,$copy_from, $copy_to,$restrict_to_width,$restrict_to_height, 1);
            	}else{
            		$file_info=$image_util_class->scale_image_by_width_if_too_large($new_file_name,$copy_from, $copy_to,$restrict_to_width,$restrict_to_height);
            	}if (is_array($file_info)) 
            	{
					$media_attachment_id=$media_attachment_db_class->add_media_attachment(
					$image_name, $file_info["file_name"],
                	 $upload_rel_path, $alt_text, $original_file_name,   $media_type_id, $created_by_id,
     					UPLOAD_TYPE_ADMIN_UPLOAD, UPLOAD_STATUS_NOT_VALIDATED_ADMIN,
     					0,$file_info["file_size"],$file_info["image_width"], $file_info["image_height"]);
     					if ($media_attachment_id<=0){
                    		echo "an error occured adding image to database";
                    		$had_error=1;
                    	}
            	} else{
            		$had_error=1;
            	}
    		}
    	if (isset($had_error))
    		$ret=0;
    	else
    		$ret=$media_attachment_id;         

		$this->track_method_exit("UploadUtil","process_admin_media_upload");
		
		return $ret;
      }
    
    
   
  
  
	  function generate_secondary_files_on_change($version_info){
	  		$media_attachment_id=$version_info["media_attachment_id"];
			if ($media_attachment_id+0!=0){
				$media_attachment_db = new MediaAttachmentDB();
				$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
				$media_type_grouping_id=$media_attachment_info["media_type_grouping_id"];
				$news_item_status_id=$version_info["news_item_status_id"];
				if ($news_item_status_id==NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED ||
					$news_item_status_id==NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED){
					if ($media_type_grouping_id>2){
						$bittorrent_info=$this->verify_associated_bittorent_info($media_attachment_id);
						if (sizeof($bittorrent_info)==0){
							$this->generate_associated_bittorent($media_attachment_id, $media_attachment_info["relative_path"], $media_attachment_info["file_name"]);
						}else{
						 	$torrent_file_name=$bittorrent_info["file_name"];
					    	$torrent_upload_name=$bittorrent_info["upload_name"];
					    	$relative_torrent_path=$bittorrent_info["relative_path"];
					    	$torrent_file_dir=UPLOAD_PATH."/".$bittorrent_info["relative_path"];
					    	$full_torrent_file_path=$torrent_file_dir.$torrent_file_name;
						 	if (!file_exists($full_torrent_file_path)){
						 		$this->regenerate_associated_bittorent($media_attachment_info["relative_path"], $media_attachment_info["file_name"]);
						 	}
						}
					}
				}
			}

  	 }
    
	  function verify_associated_bittorent_info($media_attachment_id){
	  	$media_attachment_db= new MediaAttachmentDB();
	  	$torrent_info=$media_attachment_db->get_associated_torrent_info($media_attachment_id);
	  
	  	if (!is_array($torrent_info)){
	  		//echo "bittorent not found in db<br/>";
	  		$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
	  		$full_file_path=UPLOAD_PATH."/".$media_attachment_info["relative_path"].$media_attachment_info["file_name"];
	  		//echo "full_file_path=".$full_file_path;
	  		if (file_exists($full_file_path)){
	  			//echo "original file exists<br/>";
	  			$this->generate_associated_bittorent($media_attachment_id, $media_attachment_info["relative_path"], $media_attachment_info["file_name"]);
	  		}else{
	  			//echo "original file does not exist";
	  		}
	  		
	  	}else{
	  		//echo "bittorent found in db<br/>";
	  	}
	  	return $torrent_info;
	  }
	  
	  function generate_associated_bittorent($media_attachment_id, $relative_path, $file_name){
	  	
	  	$this->generate_associated_bittorent_helper( $relative_path, $file_name);
	  	$media_attachment_db= new MediaAttachmentDB();
	  	$media_attachment_id=$media_attachment_db->add_media_attachment($file_name.".torrent", $file_name.".torrent",
	  		 $relative_path, "", $file_name.".torrent",   MEDIA_TYPE_GROUPING_OTHER, 0,
     		UPLOAD_TYPE_TORRENT, UPLOAD_STATUS_VALIDATED, $media_attachment_id);
	  	
	  }
	  
	  function regenerate_associated_bittorent($relative_path, $file_name){
	  		$this->generate_associated_bittorent_helper($relative_path, $file_name);
	  }
	  
	  
	  function generate_associated_bittorent_helper($relative_path, $file_name){
	  	$original_file_path=UPLOAD_PATH."/".$relative_path.$file_name;
	  	$desired_torrent_name=$original_file_path.".torrent";
	  	$make_torrent_bin="/usr/local/bin/maketorrent-console";
	  	$torrent_webroot="http://www.indybay.org:6969";
	  	$exec_command= $make_torrent_bin." ".$torrent_webroot. " " .
            escapeshellarg($original_file_path) . " --target " .
            $desired_torrent_name." > /dev/null 2>&1 &";
        exec($exec_command);
	  	return desired_torrent_name;
	  }
	  
	  function delete_upload($media_attachment_id){
	  	$media_attachment_db= new MediaAttachmentDB();
	  	
	  	//delete all child uploads first
		$child_image_info_list=$media_attachment_db->get_child_attachment_info_list($media_attachment_id);
		foreach($child_image_info_list as $next_child){
			$this->delete_upload($next_child["media_attachment_id"]);
		}  
		  	
	  	$media_attachment_info=$media_attachment_db->get_media_attachment_info($media_attachment_id);
	  	if (is_array($media_attachment_info)){
		  	$upload_path=UPLOAD_PATH."/";
		    $date_rel_path=$media_attachment_info["relative_path"];
			$upload_dir=$upload_path.$date_rel_path;
			$existing_full_file_path=$upload_dir.$media_attachment_info["file_name"];

		  	//delete actual upload file
		  	$file_util= new FileUtil();
		  	$file_util->delete_file($existing_full_file_path);
		  	//delete DB entry
		  	
		  	$media_attachment_db->delete_media_attachment($media_attachment_id);
	  	}
	  }
  
  }
// end class
