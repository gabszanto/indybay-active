<?php
//--------------------------------------------
//FileUtil Class
//Written December 2005
//
//Modification Log:
//12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"FileUtil");

require_once(CLASS_PATH."/common_class.inc");



//main class for filemanagement
//this is mainly just a series of wrappers around php functions
//that may be useful if we ever want to change which functions are used
class FileUtil extends Common
{

	//saves a string to a file and shuld also cleanly handle errors
	function save_string_as_file($file_path, $string){
		
		$this->track_method_entry("FileUtil","get_cached_include_article_html","file_path",$file_path);
		
		$fp = fopen($file_path,"w");
        flock($fp,2);
        fwrite($fp,$string);
        flock($fp,3);
        fclose($fp);
        
    	$this->track_method_exit("FileUtil","get_cached_include_article_html");
    	
    	return 1;
	}



	//this function loads a file asa string
	//it should have error handling to cleanly deal with missing files
	//setting a global variable and returning "" without actually
	//showing the error on the screen
	function load_file_as_string($file_path){
		
		$this->track_method_entry("FileUtil","load_file_as_string","file_path",$file_path);
		
		if (file_exists($file_path)){
			$fh=fopen($file_path,"r") or $fail=1;
			$str="";
			$i=0;
			while (!feof($fh)){
	    			$str.=fread($fh,1048576);
	    			$i=$i+1;
	    			if ($i>3){
	    				exit;
	    			}
	    	}	
	    	$this->track_method_exit("FileUtil","load_file_as_string");
    	}else{
    		$str="File Not Found: ".$file_path;
    	}
    	
    	
    	return $str;
	}
	
	function create_directory_structure_if_doesnt_exist($dir_path){
		$this->track_method_entry("ArticleCache","create_directory_structure_if_doesnt_exist","file_path",$dir_path);
    	$this->track_method_exit("ArticleCache","create_directory_structure_if_doesnt_exist");
	}
	
	function get_file_time_stamp($file_path){
		$this->track_method_entry("FileUtil","get_file_time_stamp","file_path",$file_path);
    	$this->track_method_exit("FileUtil","get_file_time_stamp");
	}
	
	function move_file($old_file_path, $new_file_path){
		$this->track_method_entry("FileUtil","move_file","old_file_path",$file_path,"new_file_path",$new_file_path);
    	$this->track_method_exit("FileUtil","move_file");
	}
	
	
	function delete_file($file_path){
		$this->track_method_entry("FileUtil","delete_file","file_path",$file_path);
    	if (file_exists($file_path) && is_file($file_path))
    		unlink($file_path);
    	$this->track_method_exit("FileUtil","delete_file");
	}

    function download_url_to_file($url, $file){
    	$this->track_method_entry("FileUtil","download_url_to_file","url",$url, "file", $file);
    	$fail=0;
    	@$fh1=fopen($url,rb) or $fail=1;
    	if ($fail==0){
    		$fh2=fopen( $file,"wb") or $fail=1;
    		if ($fail==1)
    			echo "error writing to ".$file."<br />";
    		else{
	    		while (!feof($fh1)){
	    			fwrite($fh2, fread($fh1,1048576));
	    		}
	    		fclose($fh1);
	    		fclose($fh2);
    		}
    	}
    	$this->track_method_exit("FileUtil","download_url_to_file", "success", !$fail );
    	return !$fail;
    }
    
    function copy_file($old_location, $file){
    	$this->track_method_entry("FileUtil","copy_file","old_location",$old_location, "file", $file);
    	$fail=0;
    	$fh1=fopen($old_location,rb) or $fail=1;
    	if ($fail==0){
    		$fh2=fopen( $file,wb) or $fail=1;
    		if ($fail==1)
    			echo "error writing to ".$file."<br />";
    		else{
	    		while (!feof($fh1)){
	    			fwrite($fh2, fread($fh1,1048576));
	    		}
	    		fclose($fh1);
	    		fclose($fh2);
    		}
    	}
    	$this->track_method_exit("FileUtil","copy_file", "success", !$fail );
    	return !$fail;
    }
    
    function get_file_age_in_seconds($file_path){
    	$this->track_method_entry("FileUtil","get_file_age_in_seconds","file_path", $file_path);
    	
    	clearstatcache();
    	$file_m_time=filemtime($file_path);
    	$c_time=time();
    	$diff=$c_time-$file_m_time;
    	
    	$this->track_method_exit("FileUtil","get_file_age_in_seconds", "file_age", $diff);
    	
    	return $diff;
    	
    }

}
?>