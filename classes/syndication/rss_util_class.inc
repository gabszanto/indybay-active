<?php
//--------------------------------------------
//Indybay RSSUtil Class
//Written January 2006
//
//Modification Log:
//1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"RSSUtil");

require_once(CLASS_PATH . '/rss10.inc');
require_once(CLASS_PATH . '/rss20.inc');
require_once(CLASS_PATH . '/db/search_db_class.inc');
require_once(CLASS_PATH . '/db/news_item_version_db_class.inc');
require_once(CLASS_PATH . '/db/media_attachment_db_class.inc');
require_once(CLASS_PATH . '/db/feature_page_db_class.inc');
require_once(CLASS_PATH . '/renderer/news_item_version_renderer_class.inc');

class RSSUtil extends NewsItemVersionRenderer
{
	
	//test function to be phased out
	function make_news_item_rss_string($num_results, $news_item_status_restriction ,
        	$include_posts, $include_events, $include_blurbs, $media_type_grouping_id,$topic_id,$region_id, $include_full_text, $rss_version, $search="")
    {
    	
    	$this->track_method_entry("RSSUtil","make_news_item_rss_string");
    	
    	//dont syndicate nonpushed blurbs
    	if ($include_posts+0==0 && $include_events+0==0 && $include_blurbs+0==1){
        	$news_item_status_restriction=NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED;
        }
        
		$page_number=0;
		$parent_item_id=0;
		$page_size=$num_results*2;
		$search_db_class= new SearchDB();
		
		$include_attachments=0;
                $include_comments = 0;
		if ( $media_type_grouping_id+0==3 || $media_type_grouping_id+0==4){
			$include_attachments=1;
		}
		
		$results=$search_db_class->standard_search($page_size, $page_number, $search,$news_item_status_restriction ,
        	$include_posts, $include_comments,$include_attachments,$include_events,$include_blurbs,$media_type_grouping_id,$topic_id,$region_id,$parent_item_id );
        	
        if ($topic_id+0==FRONT_PAGE_CATEGORY_ID)
        	$topic_id=0;
        if ($region_id+0==FRONT_PAGE_CATEGORY_ID)
        	$region_id=0;
        $page_id = 0;
        if ($topic_id+0==0 && $region_id+0==0){
        	$shortname=SHORT_SITE_NAME;
        	$longname=SITE_NAME;
        	$rss_url=FULL_ROOT_URL;
        }else{
        	if ($region_id+0!=0)
        		$page_id=$region_id;
        	if ($topic_id+0!=0)
        		$page_id=$topic_id;
        	if ($page_id!=0){

        		$feature_page_class= new FeaturePageDB();
        		$page_info=$feature_page_class->get_feature_page_info($page_id);

        		$shortname=strtolower(SHORT_SITE_NAME." ");
        		$longname=SHORT_SITE_NAME." ";
        		if ($page_id==60){
        			$shortname="";
        			$longname="";
        		}
        		$shortname.=$page_info["short_display_name"];
        		$longname.=$page_info["long_display_name"];

        		$rss_url=FULL_ROOT_URL.$page_info["relative_path"];
        		
        	}
        }
        if ($include_posts+0==0 && $include_events+0==0 && $include_blurbs+0==1){
        	$shortname.=" features";
        	$longname.=" Features";
        }else if ($include_posts+0==1){
        	$shortname.=" newswire";
        	$longname.=" Newswire";
        }else if ($include_events+0==1 && $include_blurbs+0==0){
        	$shortname.=" events";
        	$longname.=" Events";
        }
        
        if ($media_type_grouping_id+0==2){
        	$shortname.=" (photos)";
        	$longname.=" (photos)";
        }else if ($media_type_grouping_id+0==3){
        	$shortname.=" (audio)";
        	$longname.=" (audio)";	
        }else if ($media_type_grouping_id+0==4){
        	$shortname.=" (video)";
        	$longname.=" (video)";
        }
        	
        $author=SITE_NAME;
    	if ($page_id+0==60){
        	$author="Santa Cruz Indymedia";
        }
    	
    	$str=$this->convert_news_item_list_into_rss_string($shortname, $longname, $rss_url, $author, $results, $include_full_text, $rss_version,$num_results);
    	
    	$this->track_method_exit("RSSUtil","make_news_item_rss_string");
    
    	return $str;
    }
    
    
 	/**
     *	creates and caches an rss feed (without full content) containing the features of a given category.
     */
    function convert_news_item_list_into_rss_string($rss_shortname, $rss_long_name, $rss_url, $author, $results, $include_full_text, $rss_version,$num_results)
    {
    	$news_item_renderer =new NewsItemRenderer();
    	$rdf_string="";
    	
    	$this->track_method_entry("RSSUtil","convert_news_item_list_into_rss");
        
		if (!is_array($results) || sizeof($results)==0){
			return "";
		}
		
		$language = 'en-US';
		$webroot_url = SERVER_URL;
		$GLOBALS['print_rss'] = "" ;
		$lang_obj = new language ;		
		
		$xml_logo = $GLOBALS['xml_logo'];
		
		if ($rss_version==1){
			$rss=new RSSWriter($rss_url, $rss_shortname, $rss_long_name, $rss_url."/syn/", array("dc:publisher" =>  $author, "dc:creator" =>  $author, "dc:language" => $language));
			$rss->useModule("dcterms", "http://purl.org/dc/terms/");
			$rss->useModule("content", "http://purl.org/rss/1.0/modules/content/");
		}else{
			$rss=new RSS2Writer($rss_url, $rss_shortname, $rss_long_name, $webroot_url."/syn/", array("dc:publisher" =>  $author, "dc:creator" =>  $author, "dc:language" => $language));
			$rss->useModule("dcterms", "http://purl.org/dc/terms/");
			$rss->useModule("content", "http://purl.org/rss/1.0/modules/content/");
		}
		
		$rss->setImage($xml_logo, $rss_shortname);
		$news_item_version_db_class= new NewsItemVersionDB;
		
		$already_added_title_array= array();
		
		 $numrows=0;
		foreach ($results as $row)
		{
	        $row=str_replace(array('src="/','href="/'),array("src=\"$webroot_url/","href=\"$webroot_url/"),$row);
	        $link = FULL_ROOT_URL.substr($news_item_renderer->get_relative_web_path_from_item_info($row),1);
	        $title1 = $this->xmlentities(trim(strip_tags($row['title1'])));
	        $title2 = $this->xmlentities(trim(strip_tags($row['title2'])));
	        
	        if (($title1 && isset($already_added_title_array[$title1])) || ($title2 && isset($already_added_title_array[$title2]))) {
	        	continue;
	        }else{
	        	$already_added_title_array[$title1]=1;
	        	$already_added_title_array[$title2]=1;
	        }
	        $numrows++;
	        if ($numrows>$num_results)
	        	break;
	        
	        if (strlen(trim($title2))==0){
	        	$title2=$title1;
	        }
	        if (strlen(trim($title1))==0){
	        	$title2=$title1;
	        }
	        $displayed_author_name = $news_item_renderer->convert_smart_quotes(trim($this->xmlentities($row['displayed_author_name'])));
	            
	        $news_item_id=$row['news_item_id'];
	            
	        //$a=str_replace(array('&','<','>',chr(19),chr(20),chr(24),chr(25),chr(28),chr(29),chr(128),chr(145),chr(146),chr(147),chr(148),chr(149),chr(150),chr(151),chr(133)),array('&amp;','&lt;','&gt;','&#8211;','&#8212;','"',"'",'"','"','&#8364;','&#8216;','&#8217;','&#8220;','&#8221;','&#8226;','&#8211;','&#8212;','&#8230;'),$row['summary']);
           	
           	if ($row['news_item_type_id']==2 && $row['summary']=="")
           		$row['summary']=$row['text'];
           	$summary=str_replace("href=\"/","href=\"".FULL_ROOT_URL,$row['summary']);
           	$summary=str_replace("src=\"/","src=\"".FULL_ROOT_URL,$summary);
           	$summary = $this->xmlentities(trim($summary));
           	
	    	$text = $include_full_text ? $row['text'] : '';
                if (strlen($text) > 0) {
                  if (!preg_match('/^./us', $text)) {
                    $text = @iconv('Windows-1252', 'utf-8', $text);
                  }
                  if ($row['is_text_html']) {
                    $text = $this->html_purify($text);
                  }
                  else {
                    $text = $this->cleanup_text($text);
                  }
                }

	        $date = gmdate('Y-m-d\TH:i:s\Z',$row['creation_timestamp']);
		    $modified = gmdate('Y-m-d\TH:i:s\Z',$row['creation_timestamp']);
                    // fix me: we used to put the $file url here but..?
                    $file = '';
		    if ($rss_version==1){
		    	$item_array=array('dcterms:alternative' => $title2, "description" => $summary, "dc:date" => $date, "dcterms:modified" => $modified, "dc:creator" => $displayed_author_name, "dc:language" => $language, "content:encoded" => $text, "dcterms:hasPart" => $file);
		    }else{
		    	$item_array=array('dcterms:alternative' => $title2, "description" => $summary, "dc:date" => $date, "dcterms:modified" => $modified, "dc:creator" => $displayed_author_name, "dc:language" => $language, "dcterms:hasPart" => $file, "guid" =>$link, 'content:encoded' => $text);
		    	
		    }
		    $category_list=$news_item_version_db_class->get_news_item_category_info($row['news_item_id'],0);
				$subject_array= array();
				foreach($category_list as $catrow){
					array_push($subject_array, $this->xmlentities($catrow["name"]));
				}
				$item_array["dc:subject"]=$subject_array;
				
			if ($row['media_attachment_id']!=0){
				$media_attachment_db_class= new MediaAttachmentDB();
				$media_attachment_info=$media_attachment_db_class->get_media_attachment_info($row['media_attachment_id']);
				$mime_type=$media_attachment_info['mime_type'];
				$item_array["dc:format"]=$mime_type;
				
				if ($rss_version!=1){
                                  $rendered_video_info = $media_attachment_db_class->get_renderered_video_info($media_attachment_info['media_attachment_id'], UPLOAD_TYPE_THUMBNAIL_MEDIUM);
                                  if (is_array($rendered_video_info)) {
                                    $item_array['media:thumbnail'] = SERVER_URL . UPLOAD_URL . $rendered_video_info['relative_path'] . $rendered_video_info['file_name'];
                                  }
				$file_name=$media_attachment_info['file_name'];
			    	$relative_path=$media_attachment_info['relative_path'];
			    	$file_dir=UPLOAD_PATH."/".$media_attachment_info['relative_path'];
			    	$full_file_path=$file_dir.$file_name;
			    	$file_url=FULL_ROOT_URL."uploads/" . $relative_path.$file_name;
			    	$file_size=0;
			    	if (file_exists($full_file_path)){
			    		$file_size=filesize($full_file_path);
			    	}
			    	if ($file_size>0)
						$item_array["enclosure"] = $file_url.",".$mime_type.",".$file_size;
				}
			}
		
		    $rss->addItem($link, $title1,$item_array );
	        }
	        $print=$rss->serialize();
	     
		$this->track_method_exit("RSSUtil","convert_news_item_list_into_rss");
		return $print;
    }


    /**
     *  cleans up html for export to xml.
     */
    function xmlentities($text) {
    	$badchr = array(
		   "\xe2\x80\xa6",        // ellipsis
		   "\xe2\x80\x93",        // long dash
		   "\xe2\x80\x94",        // long dash
		   "\xe2\x80\x98",        // single quote opening
		   "\xe2\x80\x99",        // single quote closing
		   "\xe2\x80\x9c",        // double quote opening
		   "\xe2\x80\x9d",        // double quote closing
		   "\xe2\x80\xa2"        // dot used for bullet points
		   );

		$goodchr = array(
			   '...',
			   '-',
			   '-',
			   '\'',
			   '\'',
			   '"',
			   '"',
			   '*'
			   );
   
    	$text = str_replace($badchr,$goodchr,$text);
        $search = array(chr(7), chr(128),chr(130),chr(131),chr(132),chr(133),chr(134),chr(135),chr(136),chr(137),chr(138),chr(139),chr(140),chr(142),chr(145),chr(146),chr(147),chr(148),chr(149),chr(150),chr(151),chr(152),chr(153),chr(154),chr(155),chr(156),chr(158),chr(159));
        $replace = array('', '&#8364;','&#8218;','&#402;','&#8222;','&#8230;','&#8224;','&#8225;','&#710;','&#8240;','&#352;','&#8249;','&#338;','&#381;','&#8216;','&#8217;','&#8220;','&#8221;','&#8226;','&#8211;','&#8212;','&#732;','&#8482;','&#353;','&#8250;','&#339;','&#382;','&#376;');
        $return = str_replace($search,$replace,$text);
        return $return;
    }




	

}
