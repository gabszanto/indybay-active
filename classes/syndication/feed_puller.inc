<?php
require_once(CLASS_PATH .'/simplepie/simplepie.inc');
require_once("db/rss_pull_db_class.inc");

define('FEED_FORMAT_RSS',    2);
define('FEED_FORMAT_ICAL',    3);
define('FEED_FORMAT_HTML',    3);

class FeedPuller 
{

	function pull_feed_into_staging_table($feed_id){
		list($msec, $time1) = explode(" ", microtime());
		$time1=$msec*1000000.0000+$time1*1000000.000;
		$rss_pull_db = new RSSPullDB();
		
		$rss_pull_db->delete_old_items($feed_id);
		
		$rss_item_list=$this->pull_rss_item_list_from_feed_id($feed_id);
		
		
		
		if (is_array($rss_item_list)){
			
			$i=0;
			foreach($rss_item_list as $rss_item){
				$next_item_id=0;

				if(isset($rss_item["old_item_id"]) && $rss_item["old_item_id"]+0!=0){
						$rss_item["rss_item_id"]=$rss_item["old_item_id"];
						$next_item_id=$rss_item["old_item_id"];
						$rss_pull_db->update_item($rss_item,true);
				}else{
					$next_item_id=$rss_pull_db->add_item($rss_item);
					$rss_item["rss_item_id"]=$next_item_id;
					$rss_pull_db->update_if_duplicate($rss_item);
				}
				if ($next_item_id==0){
					echo "error adding item to db<br/>";
				}
				$i=$i+1;
				if ($i>8)
					break;
			}
		}
		
		list($msec, $time2) = explode(" ", microtime());
		$time2=$msec*1000000.0000+$time2*1000000.000;
		
		$rss_pull_db->update_feed_pull_time_and_duration($feed_id,round($time2-$time1));
	    
	}

	function pull_rss_item_list_from_feed_id($feed_id){
		$rss_pull_db = new RSSPullDB();
		$feed_info = $rss_pull_db->get_feed_info($feed_id);
		$feed_item_list=array();

		if ($feed_info["feed_format_id"]==1){
			$raw_rss_list=$this->pull_raw_rss_like_feed_from_html($feed_info['url'],$feed_info['restrict_urls']);
		}else{
			$raw_rss_list=$this->pull_raw_rss_from_url($feed_info['url']);
		}
		if (is_array($raw_rss_list)){
			$i=1;
			foreach($raw_rss_list as $rss_item){
				
				$next_item=$this->convert_rss_item_to_feed_item($rss_item, $feed_info, true, $i);
				if ($next_item=="")
					continue;
				$i=$i+1;
				$next_item['rss_feed_id']=$feed_id;
				$categories="";
				if ($feed_info["default_topic_id"]+0!=0){
					$categories.=($feed_info["default_topic_id"]+0);
				}
				if ($feed_info["default_region_id"]+0!=0){
					if ($categories!=""){
						$categories.=",";
					}
					$categories.=($feed_info["default_region_id"]+0);
				}
				$next_item['categories']=$categories;
				$next_item['news_item_status_id']=$feed_info["default_status_id"];
				$next_item['rss_feed_id']=$feed_id;
				array_push($feed_item_list,$next_item);
				if ($i>10)
					break;
			}
		}
		return $feed_item_list;
	}
	
	function convert_rss_item_to_feed_item($rss_item, $feed_info, $dont_repull_non_problem_items,$item_num_in_list) {
		$html = '';		
		$feed_item = array();
		$renderer = new Renderer();
		// Initialize the feed item.
		$feed_item['creation_date'] = '';
		$feed_item['author'] = '';
		$feed_item['summary'] = $feed_info['summary_template'];
		$feed_item['text'] = $feed_info['text_template'];
			
                if (is_object($rss_item)) {
			// New SimplePie-based code is here.
			$feed_item['title'] = $rss_item->get_title();
			$feed_item['related_url'] = $rss_item->get_permalink();
                        $feed_item['creation_date'] = $rss_item->get_date();
 			if ($author = $rss_item->get_author()) {
                          $feed_item['author'] = $author->get_name();
                        }
			$summary = $rss_item->get_description();
			$text = $rss_item->get_content();
                }
                else {
			// Old array-based code is here.
			// Most could probably be removed?
			// Except for HTML scraped items?
			// Which only have a link I believe?
			if (isset($rss_item["title"])) {
				$feed_item['title'] = trim($renderer->html_purify($rss_item['title'], ''));
                        }
			$feed_item['related_url']=trim($rss_item["link"]);
			if (isset($rss_item['published']))
				$feed_item['creation_date']=$rss_item['published'];
			if ($feed_item['creation_date']=="" && isset($rss_item['pubdate'])){
				$feed_item['creation_date']=$rss_item['pubdate'];
			}
			if ($feed_item['creation_date']=="" && isset($rss_item['dc']) && is_array($rss_item['dc'])){
				$feed_item['creation_date']=$rss_item['dc']['date'];
			}
			if (isset($rss_item['author_name']))
				$feed_item['author']=$rss_item['author_name'];
			if ($feed_item['author']=="" && isset($rss_item['dc']) && isset($rss_item['dc']['creator'])){
				$feed_item['author']=$rss_item['dc']['creator'];
			}
			if (isset($rss_item['summary'])) {
				$summary=trim($rss_item['summary']);
			}
			else {
				$summary="";
			}
			if (isset($rss_item['atom_content'])) {
				$text = $rss_item['atom_content'];
			}
			elseif (isset($rss_item['description'])) {
				$text = $rss_item['description'];
			}
			elseif (isset($rss_item['summary'])) {
				$text = $rss_item['summary'];
			}
			else {
				$text = "";
			}
		}

		if (isset($feed_item['title']) && mb_strpos(" ".$feed_item['title'],"??")==1) {
			return "";
		}
		if ($feed_info["replace_url"]!=""){
			$feed_item['related_url']=str_replace($feed_info["replace_url"],$feed_info["replace_url_with"],$feed_item['related_url']);
		}
		if (isset($feed_item['related_url'])){
			$jj=mb_strpos($feed_item['related_url'],"//");
			if ($jj>0){
				$ii=mb_strpos($feed_item['related_url'],"/",$jj+2);
				if ($ii>0){
					$base_url=mb_substr($feed_item['related_url'],0,$ii);
				}
			}
		}
		if ($feed_item["related_url"]!=""){
			$rss_pull_db = new RSSPullDB();
			$old_item_id=$rss_pull_db->get_rss_item_id_from_related_url_and_feed_id($feed_item["related_url"],$feed_info["rss_feed_id"]);
			if ($old_item_id!=0){
				$old_item_info=$rss_pull_db->get_item_info($old_item_id+0);
				if (isset($old_item_info["is+published"])) {
					$rss_pull_db->update_if_duplicate($old_item_info);
				}
				$feed_item["old_item_id"]=$old_item_id;
				if ($dont_repull_non_problem_items && !$this->item_has_problems($old_item_info)) {
					return "";
				}
			}
		}
		$creation_date_timestamp=0;
		if ($feed_item['creation_date']!=""){
			$creation_date_timestamp=strtotime($feed_item['creation_date']);
			if (($item_num_in_list>2 && (time()-$creation_date_timestamp)/(60*60*24)>3) && ($creation_date_timestamp-time())/(60*60*24)>-2000) {
				return "";
                        }
			if ($creation_date_timestamp>time()-60*60*20) {
				$creation_date_timestamp=time();
			}
			$feed_item['creation_date']=date("Y-m-d H:i",$creation_date_timestamp);
		}

		$fields=array();
		$fields["SHORTENED_TEXT"]=$text;
		if (trim($summary)!="")
			$fields["SHORTENED_SUMMARY"]=$this->shorten_summary($summary);
		$fields["AUTHOR"]=$feed_item['author'];
		$fields["FEED_NAME"]=$feed_info['name'];
		$fields["FULL_SUMMARY"]=$summary;
		$fields["FULL_TEXT"]=$this->clean_text($text);

		$fields["RELATED_URL"]=$feed_item['related_url'];
		
		if ($creation_date_timestamp>strtotime("1/1/2000")){
			$fields["CREATION_DATE"]=date("l, F j, Y ",$creation_date_timestamp);
		}else{
			$fields["CREATION_DATE"]="No Date Found";
		}
	
		$guess_text=false;
		if ($summary=="" && $feed_info['text_scrape_start']==""){
		
			$feed_info['summary_scrape_start']="<body&&".$feed_item['title']."&&".$feed_item['title']."||<body&&".$feed_item['title']."||<BODY&&".$feed_item['title'];
		}

		if ((strlen($text)<20 || trim($text)==trim($summary)) && $feed_info['text_scrape_start']==""){
			$jstr3=$feed_item['title'];
			if (mb_strlen($jstr3)>5){
				$jstr3=trim(mb_substr($feed_item['title'],mb_strlen($feed_item['title'])-7,7));
				if (mb_substr($jstr3,mb_strlen($jstr3)-1,1)==".")
					$jstr3=trim(mb_substr($jstr3,1,mb_strlen($jstr3)-1));
			}
			if (mb_strlen($summary)>35){
				$jstr=mb_substr($summary,mb_strlen($summary)-35,35);
				$jstr2=mb_substr($summary,mb_strlen($summary)-15,15);
				$feed_info['text_scrape_start']="<body&&".$jstr2."&&".$jstr2."||<body&&".$jstr;
				$feed_info['text_scrape_start'].="||<body&&".$jstr2."||<body&&".$feed_item['title']."||<body&&".$jstr3;
				$guess_text=true;
			}else{
				$feed_info['text_scrape_start']="<body&&".$feed_item['title']."||<body&&".$jstr3;
				$guess_text=true;
			}
		}
		if ($feed_info['text_scrape_start']!="" || 
			$feed_info['summary_scrape_start']!="" ||
			$feed_info['author_scrape_start']!=""
			|| $feed_info['date_scrape_start']!=""
			|| $feed_info['title_scrape_start']!=""){
			$feed_item['related_url']=str_replace("&amp;","&",$feed_item['related_url']);
			$headers=get_headers($feed_item['related_url'],1);
			if (is_array($headers)){
			
				$content_type=$headers["Content-Type"];
				if (is_array($content_type))
					$content_type=$headers["Content-Type"][0];
				if (mb_strpos(' '.$content_type, "text/html")>0 || mb_strpos(' '.$content_type, "text/plain")){

					$html="";
					
					$html=file_get_contents($feed_item['related_url']);
					
					
				}
			}
		}

		if ($html!=""){

			if ($feed_info['title_scrape_start']!=""){
				$scrape_title=$this->get_scrape($html,$feed_info['title_scrape_start'],$feed_info['title_scrape_end']);
				if ($scrape_title!="")
					$feed_item['title'] = trim($renderer->html_purify($scrape_title, ''));
			}
			
			
			if ($feed_info['author_scrape_start']!=""){
				$scrape_author=$this->get_scrape($html,$feed_info['author_scrape_start'],$feed_info['author_scrape_end']);
				if ($scrape_author!="")
					$fields['AUTHOR'] = trim($renderer->html_purify($scrape_author, ''));
			}			
	
			
			if ($feed_info['date_scrape_start']!=""){
				
				$scrape_date=$this->get_scrape($html,$feed_info['date_scrape_start'],$feed_info['date_scrape_end']);
				
				if ($scrape_date!=""){
					$datestr = trim($renderer->html_purify($scrape_date, ''));
					$datestr=str_replace(".","",$datestr);
					$datestr=str_replace("&sbquo;","",$datestr);
					$datestr=str_replace("&nbsp;"," ",$datestr);
					$datestr=str_replace("\n","",$datestr);
					$datestr=str_replace("\r","",$datestr);

					$jj=mb_strpos($datestr, " / ");
					if ($jj>0){
						$oo=mb_strpos($datestr, " ",$jj+4);
						$datestr=mb_substr($datestr,0,$jj).mb_substr($datestr,$oo,mb_strlen($datestr)-$oo);
					}
					$creation_date_timestamp=strtotime($datestr);
				
					$feed_item['creation_date']=date("Y-m-d H:i",$creation_date_timestamp);

					if ($creation_date_timestamp>strtotime("1/1/2000")){

						if ($item_num_in_list>2 && (time()-$creation_date_timestamp)/(60*60*24)>3)
							return "";
						if ($creation_date_timestamp>time()-60*60*20){
							$creation_date_timestamp=time();
							$feed_item['creation_date']=date("Y-m-d H:i",$creation_date_timestamp);
						}
						$fields["CREATION_DATE"]=date("l, F j, Y ",$creation_date_timestamp);
					}else{
						$fields["CREATION_DATE"]="No Date Found";
					}
				}else{
					$fields["CREATION_DATE"]="No Date Found";
				}
			}

			if ($feed_info['summary_scrape_start']!=""){

				$scrape_summary=$this->get_scrape($html,$feed_info['summary_scrape_start'],$feed_info['summary_scrape_end']);
			
									
		
				if (trim($scrape_summary)!=""){		
					$fields["FULL_SUMMARY"]=$scrape_summary;
					$fields["SHORTENED_SUMMARY"]=$this->shorten_summary($scrape_summary);
				}
			}

			if ($feed_info['text_scrape_start']!=""){
			
				$scrape_text=$this->get_scrape($html,$feed_info['text_scrape_start'],$feed_info['text_scrape_end']);		
				
				if ($scrape_text!=""){

					$fields["FULL_TEXT"]=$this->clean_text($scrape_text);
					
					$fields["SHORTENED_TEXT"]=$scrape_text;
					

				}else{

					if ($guess_text==true && $scrape_summary!=""){
						if (mb_strlen($scrape_summary)>20){
							$jstr=mb_substr($scrape_summary,mb_strlen($scrape_summary)-20,20);
							$feed_info['text_scrape_start']=$jstr;
						}
						$scrape_text=$this->get_scrape($html,$feed_info['text_scrape_start'],$feed_info['text_scrape_end']);
						if ($scrape_text!=""){
							$fields["FULL_TEXT"]=$scrape_text;
							$fields["SHORTENED_TEXT"]=$scrape_text;
						}
					}
				}		
				
			}
		}


		$feed_item['summary']=$this->process_template($feed_info['summary_template'], $fields);

		$fields['SHORTENED_TEXT']=$this->shorten_text($fields['SHORTENED_TEXT']);
		if (isset($base_url)){
			$fields['SHORTENED_TEXT']=str_replace("href=\"/","href=\"".$base_url."/",$fields['SHORTENED_TEXT']);
			$fields['SHORTENED_TEXT']=str_replace("src=\"/","src=\"".$base_url."/",$fields['SHORTENED_TEXT']);
			$fields['FULL_TEXT']=str_replace("href=\"/","href=\"".$base_url."/",$fields['FULL_TEXT']);
			$fields['FULL_TEXT']=str_replace("src=\"/","src=\"".$base_url."/",$fields['FULL_TEXT']);
		}
		
		$feed_item['text']=$this->process_template($feed_info['text_template'], $fields);


		$feed_item['author']=$this->process_template($feed_info['author_template'], $fields);
		if (mb_strlen($feed_item['summary'])>10){
		
			$jstr=mb_substr($feed_item['summary'],mb_strlen($feed_item['summary'])-10,10);
			$k=mb_strpos($feed_item['text'],$jstr);
			if ($k>0 && $k<700){
				$feed_item['text']=mb_substr($feed_item['text'],$k+mb_strlen($jstr), mb_strlen($feed_item['text'])-$k-mb_strlen($jstr));
				$feed_item['text']=$this->clean_leading_spaces($feed_item['text']);
			}
		}


		$feed_item=$this->clean_field_lengths($feed_item);
	

		return $feed_item;
	}
	
	function clean_field_lengths($feed_item){
		if (mb_strlen($feed_item["author"])>40)
			$feed_item["author"]=mb_substr($feed_item["author"],0,60);
		
		if (isset($feed_item["title"]) && mb_strlen($feed_item["title"])>125)
			$feed_item["title"]=mb_substr($feed_item["title"],0,125);
		if (isset($feed_item["title"]) && $feed_item["title"]==strtoupper($feed_item["title"]))
			$feed_item["title"]=ucwords(strtolower($feed_item["title"]));
		if (isset($feed_item["related_url"]) && mb_strlen($feed_item["related_url"])>512)
			$feed_item["related_url"]="";
		
		return $feed_item;
	}
	
	
	function get_scrape($html, $start, $end){

		$start_search_or_strings=array();
		if (mb_strpos($start,"||")+0==0){
			$start_search_or_strings[0]=$start;
		}else{
			$start_search_or_strings = explode("||", $start);
		}
		$ret="";

		foreach($start_search_or_strings as $nextorstr){
			$start_search_and_strings=array();
			if (mb_strpos($nextorstr,"&&")+0==0){
				$start_search_and_strings[0]=$nextorstr;
			}else{
				$start_search_and_strings = explode("&&", $nextorstr);
			}
			$i=0;
			$and_failed=false;
			foreach($start_search_and_strings as $nextstr){
				if(trim($nextstr)!=""){
					$i=mb_strpos($html,$nextstr,$i)+0;
					if ($i+0>0){
						$i=$i+mb_strlen($nextstr);
					}else{
						$and_failed=true;
					}
				}
			}
			if (!$and_failed)
				break;
		}
		$ret="";
		if ($i!=0){
		
			if (mb_strlen(trim($end))==0){
				$j=mb_strlen($html);
				$end_search_or_strings=array();
			}else{
			
				$end_search_or_strings=array();
				if (mb_strpos($end,"||")+0==0){
					$end_search_or_strings[0]=$end;
				}else{
					$end_search_or_strings = explode("||",$end);
				}
				$j=$i;
			}
				
			foreach($end_search_or_strings as $nextorstr){
				
				$end_search_and_strings=array();
				if (mb_strpos($nextorstr,"&&")+0==0){
					$end_search_and_strings[0]=$nextorstr;
				}else{
					$end_search_and_strings = explode("&&", $nextorstr);
				}
				$j=$i;
	
				$and_failed=false;
				foreach($end_search_and_strings as $nextstr){
					if (trim($nextstr)!=""){
						$j=mb_strpos($html,$nextstr,$j)+0;
		
						if ($j>0){
							$j=$j+mb_strlen($nextstr);
						}else{
							$and_failed=true;
						}
						if (!$and_failed)
							break;
					}
				}
				if ($j>0){
					$j=$j-mb_strlen($nextstr);
				}
				if (!$and_failed)
					break;
			}
		}
		if ($i!=0 && $j>0 && $j>$i){
			$ret=mb_substr($html,$i,$j-$i);
			return $ret;
		}else{
			return "";
		}
	}
	
	function shorten_text($html_str){
		$html_str=$this->clean_text($html_str);
		$i=1500;
		$j=-1;
		if ($i<mb_strlen($html_str)){
			$j=mb_strpos($html_str,"<p", $i)+0;
			$k=mb_strpos($html_str,"<br", $i)+0;
			if ($j==0  || ($j>1500 && $k<$j && $k!=0))
				$j=$k;	
			$k=mb_strpos($html_str,". ", $i)+0;
			if ($j==0  || ($j>2000  && $k<$j && $k!=0))
				$j=$k;
			if ($j==0){
				$ret=$html_str;
			}
		}else{
			$ret=$html_str;
		}
		if ($j>0)
			$ret=mb_substr($html_str,0,$j);

		if (mb_strripos($ret,"<blockquote") > mb_strripos($ret,"</blockquote")){
			$ret.="</blockquote>";
		}
		return $ret;
			
	}
	
   function shorten_summary($html_str){
                $renderer = new Renderer();
   		$html_str=$this->clean_text($html_str);
		$html_str = $renderer->html_purify($html_str, 'p,br');
		
   		$html_str=trim($html_str);
   		
		if (mb_strlen($html_str)<5)
			return "";
 
		if (mb_strlen($html_str)<300)
			return $html_str;
		$j=mb_strpos($html_str,"<p", 150)+0;
		$k=mb_strpos($html_str,"<br", 150)+0;
		if ($j==0  || ($k<$j && $k!=0))
				$j=$k;
		$k=mb_strpos($html_str,". ", 300)+0;
		if ($j==0  || ($k<$j && $k!=0))
				$j=$k+1;
   
		if ($j<10){
			if (mb_strlen($html_str)<400)
				return $html_str;
			else
				return "";
		}
		$html_str = $renderer->html_purify(mb_substr($html_str, 0, $j), '');
		$html_str=trim($html_str);
		
		return $html_str;
			
	}
	
	
	function pull_raw_rss_from_url($url){
		$rsslist = new SimplePie($url, CACHE_PATH . '/SimplePie');
		if ($rsslist and !$rsslist->error()) {
			return $rsslist->get_items();
		} else {
		    	echo "Error: " . $rsslist->error();
		    	return "";
	    	}
	}
	
	
	function pull_raw_rss_like_feed_from_html($url, $urls){
		$html_string = file_get_contents($url);
		
		$result_array=array();
		$i=0;
		$existing_links=array();
		$num_added=0;
		while ($i>-1){
			$i0=$i;
			$i=mb_strpos($html_string, $urls , $i);
			if ($i=="" || $i<$i0)
				$i=-1;
			if ($i!=-1){
				$j=mb_strpos($html_string,"\"", $i);
				$j2=mb_strpos($html_string,"'", $i+1);
				if ($j=="" || ($j2>0 && $j2<$j))
					$j=$j2;	
				$j2=mb_strpos($html_string,"<", $i+1);
				if ($j=="" || ($j2>0 && $j2<$j))
					$j=$j2;	
				$j2=mb_strpos($html_string," ", $i+1);
				if ($j=="" || ($j2>0 && $j2<$j))
					$j=$j2;	
				if ($j+0>$i){
					
					$next_link=mb_substr($html_string, $i,$j-$i);
					if (!isset($existing_links[$next_link])){
						$row_array=array();
						$row_array["link"]=$next_link;
						array_push($result_array, $row_array);
						$existing_links[$next_link]=1;
						$num_added=$num_added+1;
						if ($num_added==10)
							break;
					}
					$i=$j;
				}
			}
			if ($i!=-1)
				$i=$i+1;
		}
		return $result_array;
	}
	

	function item_has_problems($rss_item){
		if (
				mb_strlen($rss_item['title'])<6
				   || mb_strlen(strip_tags($rss_item['summary']))<30 
				   || mb_strlen(strip_tags($rss_item['text']))<150
		           || trim($rss_item['author'])==""|| mb_strpos(' '.trim($rss_item['author']),',')==1 
		           || mb_strlen(strip_tags(trim($rss_item['summary'])))>700
		           || $rss_item["creation_date_timestamp"]<strtotime("1/1/2000")
		           || $rss_item["creation_date_timestamp"]>time()+60*60*23*2
		           ){
		       
		            return true;
		}else{
		 
		    $i=mb_strpos(" ".$rss_item['title'],"?");
           	if ($i!="" && $i!=mb_strlen($rss_item['title'])-1){
           		if (mb_substr($rss_item['title'],$i+1,1)!=" "){
           			return true;
           		}
           		$i=mb_strpos(" ".$rss_item['title'],"?",$i);
           		if ($i!="" && $i!=mb_strlen($rss_item['title'])-1){
           			if (mb_substr($rss_item['title'],$i,1)!=" "){
           				return true;
           			}
           		}
           	}
			return false;
		}
	}
	
	function process_template($template_str, $fields){
		$result=$template_str;
		foreach($fields as $name=>$value){
			$result=str_replace($name,$value,$result);
		}
		return $result;
	}
	
	
		
	
	function clean_text($html){
		if (trim($html)=="")
			return "";
		$renderer = new Renderer();

   		
		$html=$this->removeBetweenTags("<script","</script>",$html);
		$html=$this->removeBetweenTags("<SCRIPT","</SCRIPT>",$html);
		$html=$this->removeBetweenTags("<Script","</Script>",$html);

		$html = $renderer->html_purify($html, 'b,p,br,strong,hr,ul,li,img[src|alt],h1,h2,h3,blockquote,a[href]');

		$html=$this->remove_more_stuff($html);

		return $this->clean_leading_spaces($html);
	}


function clean_leading_spaces($html){
		$whitespace = array("  ", "\r", "\n", "\t", "  ");
		while (mb_strlen($html)>1){
			$a=mb_substr($html,0,1);
			if ($a=="\n" || $a==" " || $a=="\t" || $a=="\r" ){
				$html=mb_substr($html,1,mb_strlen($html)-1);
				continue;
			}
			if (mb_strlen($html)>2){
				$b=mb_substr($html,0,2);
				if ($b=="<p"){
					$i=mb_strpos($html, ">");
					if ($i>0){
						$html=mb_substr($html,$i+1,mb_strlen($html)-$i);
						continue;
					}
				}
				if ($b=="</"){
					$i=mb_strpos($html, ">");
					if ($i>0){
						$html=mb_substr($html,$i+1,mb_strlen($html)-$i);
						continue;
					}
				}		
			}
			if (mb_strlen($html)>3){
				$c=mb_substr($html,0,3);
				if ($c=="<br"){
					$i=mb_strpos($html, ">");
					if ($i>0){
						$html=mb_substr($html,$i+1,mb_strlen($html)-$i);
						continue;
					}
				}	
				
			}
			break;
		}
		return $html;
	}



function remove_more_stuff($htmlText)
{
		$htmlText=str_replace("\n"," ",$htmlText);
		$htmlText=str_replace("\r"," ",$htmlText);
		$htmlText=str_replace("\t"," ",$htmlText);
		$htmlText=str_replace("  "," ",$htmlText);
		$htmlText=str_replace("  "," ",$htmlText);
		$htmlText=str_replace("</p>","",$htmlText);
		$htmlText=str_replace("</P>","",$htmlText);
		$htmlText=str_replace("<P","<p ",$htmlText);
		$htmlText=str_replace("<BR  ","<br",$htmlText);
		$htmlText=str_replace("> <br","><br",$htmlText);
		$htmlText=str_replace("> <p","><p",$htmlText);
		
		$htmlText=str_replace("<p><br>","<p>",$htmlText);
		$htmlText=str_replace("<br><br>","<p>",$htmlText);
		
		$htmlText=str_replace("<br><p>","<p>",$htmlText);
		
		$htmlText=str_replace("<p><p>","<p>",$htmlText);
		
		return $htmlText;
}


function removeBetweenTags($tag1,$tag2, $html, $iter=0){

	if ($iter>500){
		echo "infinite loop?<br>";
		return "";
	}
	$i=mb_strpos(' '.$html,$tag1)+0;
	$i2=mb_strpos(' '.$html,$tag2)+0;
	if ($i2<$i && $i2>0){
		$html=$html=mb_substr($html, $i2+mb_strlen($tag2));
		$html=$this->removeBetweenTags($tag1,$tag2, $html, $iter+1);
	}else{
		if ($i!=0){
			$i=$i-1;
			$j=	mb_strpos($html, $tag2, $i+mb_strlen($tag1))+0;
			if ($j>$i){
				$html=mb_substr($html,0,$i).mb_substr($html, mb_strlen($tag2)+$j-1);
			}else{
				$html=mb_substr($html, 0,$i);
				
			}
			$html=$this->removeBetweenTags($tag1,$tag2, $html, $iter+1);
		}
	}

	return $html;
}



}
?>
