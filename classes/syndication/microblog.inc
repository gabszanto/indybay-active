<?php
// $Id$

require('Services/Twitter.php');
require('HTTP/OAuth/Consumer.php');

/*
 * Syndicate status updates across multiple microblogging services.
 *
 * @todo: We need a job queue for submitting status updates across multiple
 * services. Otherwise if Twitter hangs, all other status updates and current
 * request hang. For now, quick and dirty, just loop thru the services...
 */
class Microblog {

  /*
   * Return array of available services.
   */
  public static function services() {
    $options = array('use_ssl' => TRUE, 'source' => 'Indybay');
    return array(
      'Identi.ca' => array(
        'consumer_key' => IDENTICA_CONSUMER_KEY,
        'consumer_secret' => IDENTICA_CONSUMER_SECRET,
        'access_key' => IDENTICA_ACCESS_KEY,
        'access_secret' => IDENTICA_ACCESS_SECRET,
        'options' => $options,
        'uri' => 'https://identi.ca/api',
        'base' => 'https://identi.ca/notice/',
      ),
      'Twitter' => array(
        'consumer_key' => TWITTER_CONSUMER_KEY,
        'consumer_secret' => TWITTER_CONSUMER_SECRET,
        'access_key' => TWITTER_ACCESS_KEY,
        'access_secret' => TWITTER_ACCESS_SECRET,
        'options' => $options,
        'uri' => 'https://api.twitter.com/1',
        'base' => 'https://twitter.com/' . TWITTER_USER . '/status/',
      ),
      'Facebook' => array(
        'base' => 'https://www.facebook.com/' . FACEBOOK_USER . '?story_fbid=',
      ),
    );
  }

  public function wallpost() {
    $request = new HTTP_Request2('https://graph.facebook.com/' . FACEBOOK_PAGE_ID . '/feed', HTTP_Request2::METHOD_POST, array('ssl_verify_peer' => FALSE));
    $request->addPostParameter('access_token', FACEBOOK_PAGE_ACCESS_TOKEN);
    $request->addPostParameter('uid', FACEBOOK_PAGE_ID);
    $request->addPostParameter('message', 'Blurb text');
    $request->addPostParameter('picture', 'http://icanhascheezburger.files.wordpress.com/2009/03/funny-pictures-kitten-finished-his-milk-and-wants-a-cookie.jpg');
    $request->addPostParameter('link', 'http://www.myvideosite/videopage.html');
    $status = json_decode($request->send()->getBody());
    $services[$key]['status']->id = strtok('_');
  }


  /*
   * Update status on all services.
   *
   * @return array of services, each with a status or exception object.
   */
  public static function update($status = '') {
    $services = self::services();
    foreach ($services as $key => $service) {
      if ($key == 'Facebook') {
        $request = new HTTP_Request2('https://graph.facebook.com/me/feed', HTTP_Request2::METHOD_POST, array('ssl_verify_peer' => FALSE));
        $request->addPostParameter('access_token', FACEBOOK_ACCESS_TOKEN);
        $request->addPostParameter('message', $status);
        // Well yes we should probably check the status code and such but let's just get the body for now...
        $services[$key]['status'] = json_decode($request->send()->getBody());
        if (isset($services[$key]['status']->error)) {
          $services[$key]['exception'] = $services[$key]['status']->error;
        }
        // Facebook returns {user_id}_{status_id} rather than just {status_id}.
        strtok($services[$key]['status']->id, '_');
        $services[$key]['status']->id = strtok('_');
      }
      else {
        Services_Twitter::$uri = $service['uri'];
        try {
          $microblog = new Services_Twitter(NULL, NULL, $service['options']);
          $consumer = new HTTP_OAuth_Consumer($service['consumer_key'], $service['consumer_secret'], $service['access_key'], $service['access_secret']);
          $microblog->setOAuth($consumer);
          $services[$key]['status'] = $microblog->statuses->update($status);
        }
        catch (Exception $e) {
          $services[$key]['exception'] = $e;
        }
      }
    }
    return $services;
  }

}
