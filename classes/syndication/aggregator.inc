<?php

class RSSAggregator {

  function simple_pie($feed_url) {
    require_once(CLASS_PATH .'/simplepie/simplepie.inc');
    $feed = new SimplePie();
    $feed->set_cache_location(CACHE_PATH .'/SimplePie');
    $feed->set_feed_url($feed_url);
    $feed->init();
    return $feed;
  }

  function renderForJScript($rss_file_features) {
    $rss_file_features = 'generate_rss.php?include_posts=0&include_attachments=0&include_events=0&include_blurbs=1';
    $feed_features = $this->simple_pie(FULL_ROOT_URL.'syn/'. $rss_file_features);	
    $rss_file_newswire = 'generate_rss.php?include_posts=1&include_attachments=0&include_events=1&news_item_status_restriction=15&include_blurbs=0';
    $feed_newswire = $this->simple_pie(FULL_ROOT_URL.'syn/'. $rss_file_newswire);	
    if ($feed_features && $feed_newswire) {
      foreach ($feed_features->get_items() as $item) {
        $allstories[] = array('title' => $item->get_title(), 'link' => $item->get_permalink(), 'date' => $item->get_date('c'));
      }
      foreach ($feed_newswire->get_items() as $item) {
        $allstories[] = array('title' => $item->get_title(), 'link' => $item->get_permalink(), 'date' => $item->get_date('c'));
      }
      $sortedstories = $this->SortArray($allstories, 'date', DESC);
      $sortedstories = array_slice($sortedstories,0,10);
    }
    return $sortedstories;
  }

  function SortArray() {
    $arguments = func_get_args();
    $array = $arguments[0];
    $code = '';
    for ($c = 1; $c < count($arguments); $c += 2) {
        if (in_array($arguments[$c + 1], array("ASC", "DESC"))) {
            $code .= 'if ($a["'.$arguments[$c].'"] != $b["'.$arguments[$c].'"]) {';
            if ($arguments[$c + 1] == "ASC") {
                $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? -1 : 1); }';
            }
            else {
                $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? 1 : -1); }';
            }
        }
    }
    $code .= 'return 0;';
    $compare = create_function('$a,$b', $code);
    usort($array, $compare);
    return $array;
  }

}
