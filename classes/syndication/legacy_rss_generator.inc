<?php
//looks for archived files and if they dont exist or are old
//maps old rss names to new searchs, runs them, generates the files
//and then returns the text of thefile

include_once(CLASS_PATH."/pages/rss/rss_generator.inc");

class legacy_rss_generator
{

	function generate_rss($file_name){
		
		//look for existing file and get date
		$full_path=WEB_PATH."/syn/cache/".$file_name;
		$file_util= new FileUtil();
		
		$recentFileExists=true;
		
		if (file_exists($full_path)){
			$age_in_secs=$file_util->get_file_age_in_seconds($full_path);
			if ($age_in_secs>600){
				$recentFileExists=false;
			}else{
				$recentFileExists=true;
			}
		}else{
			$recentFileExists=false;
		}
		
		if ($recentFileExists){
			//return string from file
			$str=$file_util->load_file_as_string($full_path);
		}else{
		
		}
		
		$search_fields= array();
		$search_fields['rss_version']=2;
		if (strrpos($file_name,".rdf")>0){
			$search_fields['rss_version']=1;
		}
		//set search params based off old file name (or new name)
		$search_fields['include_comments']=0;
		$search_fields['include_posts']=0;
		$search_fields['include_blurbs']=0;
		$search_fields['include_events']=0;
		$search_fields['include_full_text']=0;
		$search_fields['news_item_status_restriction']=NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED*NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
		switch ($file_name){
			case "production_features_long.rdf":
				$search_fields['include_blurbs']=1;
				$search_fields['news_item_status_restriction']=NEWS_ITEM_STATUS_ALL_NONHIDDEN;
				$search_fields['include_full_text']=1;
				break;
			case "features.rdf":
				$search_fields['include_blurbs']=1;
				$search_fields['news_item_status_restriction']=NEWS_ITEM_STATUS_ALL_NONHIDDEN;
				break;
			case "newswire.rss":
				$search_fields['include_posts']=1;
				$search_fields['include_full_text']=0;
				break;
			case "newswire.2.rss":
				$search_fields['include_posts']=1;
				$search_fields['include_full_text']=0;
				break;
			break;
		}
		$rss_generator= new rss_generator();
		//runs search and creates rss or rdf
		$str=$rss_generator->generate($search_fields);
		
		//saves rss or rdf to file
		$file_util->save_string_as_file($full_path, $str);
		
		// return string
		return $str;
	}

}
