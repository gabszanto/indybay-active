<?php
//--------------------------------------------
//Indybay LegacyAttachmentMigration Class
//Written December 2005 - January 2006
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"LegacyAttachmentMigration");
require_once(CLASS_PATH."/common_class.inc");
require_once(CLASS_PATH."/db/news_item_db_class.inc");

//helps migrate media from old sites
class LegacyAttachmentMigration extends Common{

	var $old_center_column_locations = array (
						"http://indybay.org/imcenter/",
						"http://indybay.org/uploads/",
						"http://indybay.org/im/",
						"http://indybay.org/images/"
					);	
					
					
	function attempt_to_migrate_legacy_post_attachment($media_attachment_info, $correct_new_location){
				
				$media_atachment_db_class= new MediaAttachmentDB();
				$original_info=$media_atachment_db_class->get_original_attachment_info_from_attachment_id($media_attachment_info["media_attachment_id"]);
		        if (is_array($original_info) && $original_info["media_attachment_id"]!=$media_attachment_info["media_attachment_id"]){
					$this->attempt_to_migrate_legacy_post_attachment($original_info, $correct_new_location);
				}
				
				$this->track_method_entry("LegacyAttachmentMigration",
					"attempt_to_migrate_legacy_post_attachment","file_name", $media_attachment_info["file_name"], "correct_new_location", $correct_new_location);
				
				$success=0;
				$file_util_class=new FileUtil;
				$file_name=	$media_attachment_info["file_name"];
				
				$correct_full_path=$correct_new_location.$file_name;
				
				$i=strripos($file_name, "/");
				if ($i>0)
					$file_name=substr($file_name,$i+1);
		
				if (trim($file_name)!=""){
	
					if (file_exists($correct_full_path)){
				
						$success=1;
					}else if (file_exists(LEGACY_UPLOAD_DIR.$file_name)){
					
							$success=$file_util_class->copy_file(LEGACY_UPLOAD_DIR.$file_name, $correct_new_location.$file_name);
					}else if ($GLOBALS['is_mirror_site']){
							$url=$GLOBALS['originating_site_url']."/uploads/".$file_name;
							$success=$file_util_class->download_url_to_file($url, $correct_new_location.$file_name);
							if ($success=="" or $sucess="0"){
								$j=strrpos($correct_new_location,UPLOAD_ROOT );
								if ($j==0){
									$partpath=substr($correct_new_location,$j+strlen(UPLOAD_ROOT),strlen($correct_new_location));
									$url="http://indybay.org/uploads/".$partpath.$file_name;
									$success=$file_util_class->download_url_to_file($url, $correct_new_location.$file_name);
								}
							}

					}
				}else{
						echo "couldnt get file_name for media_attachment_id ".$media_attachment_info["media_attachment_id"]."<br />";
						$success=0;
				}
				
				$this->track_method_exit("LegacyAttachmentMigration",
								"attempt_to_migrate_legacy_post_attachment");
								
				return $success;
	}
	
	
	
	
	
	function attempt_to_migrate_legacy_blurb_attachment($media_attachment_info, $correct_new_location){

				$this->track_method_entry("LegacyAttachmentMigration",
					"attempt_to_migrate_legacy_blurb_attachment", "file_name", $media_attachment_info["file_name"],"correct_new_location", $correct_new_location);
				
				$success=0;
				$file_util_class=new FileUtil;
				$file_name=	$media_attachment_info["file_name"];
				
				
				$i=strripos($file_name, "/");
				if ($i>0)
					$file_name=substr($file_name,$i+1);
				
				$correct_full_path=$correct_new_location.$file_name;
				
				if (file_exists($correct_full_path)){
					$success=1;
				}else if (file_exists(LEGACY_CENTERCOL_UPLOAD_DIR.$file_name)){
					$success=$file_util_class->copy_file(LEGACY_CENTERCOL_UPLOAD_DIR.$file_name, $correct_full_path);
				}else if ($GLOBALS['is_mirror_site']){
					$url=$GLOBALS['originating_site_url']."/uploads/".$file_name;
							$success=$file_util_class->download_url_to_file($url, $correct_new_location.$file_name);
							if ($success=="" or $sucess="0"){
								$j=strrpos($correct_new_location,UPLOAD_ROOT );
								if ($j==0){
									$partpath=substr($correct_new_location,$j+strlen(UPLOAD_ROOT),strlen($correct_new_location));
									$url="http://indybay.org/uploads/".$partpath.$file_name;
									$success=$file_util_class->download_url_to_file($url, $correct_new_location.$file_name);
								}
							}
				}
				$this->track_method_exit("LegacyAttachmentMigration",
					"attempt_to_migrate_legacy_blurb_attachment");
				
				return $success;
	}

}
?>
