<?php

class GoogleVoice
{
	// Our credentials
	private $_login;
	private $_pass;
	private $_rnr_se; // some crazy google thing that we need later

	// Our private curl handle
	private $_ch;

	// The location of our cookies
	private $_cookieFile;

	// Are we logged in already?
	private $_loggedIn = FALSE;

	public function __construct($login, $pass)
	{
		$this->_login = $login;
		$this->_pass = $pass;

		$this->_cookieFile = '/tmp/gvCookies.txt';

		$this->_ch = curl_init();
		curl_setopt($this->_ch, CURLOPT_COOKIEJAR, $this->_cookieFile);
		curl_setopt($this->_ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($this->_ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)");
	}
	
	private function _logIn()
	{
		global $conf;

		if( $this->_loggedIn )
			return TRUE;

		// fetch the login page
		curl_setopt($this->_ch, CURLOPT_URL, 'https://accounts.google.com/ServiceLogin?passive=true&service=grandcentral');
		$html = curl_exec($this->_ch);

		if(preg_match('/name="GALX"\s*value="([^"]+)"/', $html, $match))
			$GALX = $match[1];
		else
			throw new Exception('Could not parse for GALX token');

		curl_setopt($this->_ch, CURLOPT_URL, 'https://accounts.google.com/ServiceLoginAuth?service=grandcentral');
		curl_setopt($this->_ch, CURLOPT_POST, TRUE);
		curl_setopt($this->_ch, CURLOPT_POSTFIELDS, array(
			'Email' => $this->_login,
			'Passwd' => $this->_pass,
			'continue' => 'https://www.google.com/voice/account/signin',
			'service' => 'grandcentral',
			'GALX' => $GALX
			));
	
		$html = curl_exec($this->_ch);
		if( preg_match('/name="_rnr_se".*?value="(.*?)"/', $html, $match) )
		{
			$this->_rnr_se = $match[1];
		}
		else
		{
			throw new Exception("Could not log in to Google Voice with username: " . $this->_login);
		}
	}

	public function sendSMS($number, $message)
	{
		$this->_logIn();

		curl_setopt($this->_ch, CURLOPT_URL, 'https://www.google.com/voice/sms/send/');
		curl_setopt($this->_ch, CURLOPT_POST, TRUE);
		curl_setopt($this->_ch, CURLOPT_POSTFIELDS, array(
			'_rnr_se'=>$this->_rnr_se,
			'phoneNumber'=>$number,
			'text'=>$message
			));
		curl_exec($this->_ch);
	}
	
}
