<?php
//--------------------------------------------
//Page Class
//Written December 2005 - January 2006
//modified from an sf-active class
//
//Modification Log:
//12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------

//used so we can limit # of includes by tracking which files got included
//by a page
array_push($GLOBALS["included_classes"],"Page");

require_once(CLASS_PATH."/common_class.inc");



class Page extends Common
{
    var $pageid;
   	var $page;
   	var $script_file;
   	var $forced_template_file;
    
    // The Page class is used to compile and build pages
	
    function Page($pageid = '',$relative_dir='')
    {
    	$this->track_method_entry("Page","Page");
    	if ($pageid=="")
    		return 1;
    	
    	if ($relative_dir!="")
    		$relative_dir="/".$relative_dir;
    		
        global $template_obj;
        $this->error = "";
		$tr = new Translate;

		$tr->create_translate_table('page');

        	// Constructor which begins setting page properties
        	if (!is_string($pageid))
        	{
				$this->error = $tr->trans('string_error');
        	} else
        	{
            	$this->pageid = $pageid;

            	// First, check for a template file
            	$this->template_file = $pageid . ".tpl";
            	if (!file_exists(TEMPLATE_PATH . "/pages".$relative_dir."/" . $this->template_file))
            	{
            		echo "UNABLE TO LOAD TEMPLATE: ". TEMPLATE_PATH . "/pages".$relative_dir."/" . $this->template_file."<br />";
                	$this->error = $tr->trans('template_error') ;
            	} else
            	{
            		
                	$this->template = new FastTemplate(TEMPLATE_PATH . "/pages".$relative_dir);
                	// Next, check for a script file
                	$this->script_file = INDYBAY_BASE_PATH . "/classes/pages".$relative_dir."/" . $pageid . ".inc";
                	

                	if (!file_exists("$this->script_file"))
                	{
                		echo "UNABLE TO LOAD SCRIPT ".$this->script_file."<br />";
                    	$this->error = $tr->trans('script_error');
                	} else
                	{
                    	include_once("$this->script_file");

                    	// Next, try to instantiate the script class
                    	if (!class_exists($pageid))
                    	{
                        	$this->error = $tr->trans('class_error');
                        	echo "UNABLE TO LOAD PAGEID $pageid"."<br />";;
                    	} else
                    	{
                        	$this->page = new $pageid();
                    	}
                	}
            	}
	        }   
                         

        // Last, check for a dictionary file
        $this->translate = new Translate();
        
        $dict_file= str_replace("/","_",$relative_dir);
        if (sizeof($dict_file)>0)
        	$dict_file=substr($dict_file,1);
        $this->translate->create_translate_table($dict_file);

		$this->track_method_exit("Page","Page");
        return 1; 
    }
    

    

    function get_error()
    {
        if (strlen($this->error) > 0)
        {
            return $this->error;
        } else
        {
            return false;
        }
    }

    function build_page($content_page = '')
    {
		$this->track_method_entry("Page","build_page");
        unset ($this->page->forced_template_file);
	
        if (isset($this->page)) {
          $this->page->execute();
        }
        
        if (isset($this->page->forced_template_file))
        {
            $this->force_new_template($this->page->forced_template_file);
        }

        if (isset($this->template)) {
          $this->template->clear_all();
          $this->template->define(array("page" => $this->template_file));
        }

        $defaults = array();

        while(list($key, $value) = each($GLOBALS['dict']))
        {
            $keyid = "TPL_" . strtoupper($key);
            $defaults[$keyid] = $value;
        }

        if (isset($this->page->tkeys) && is_array($this->page->tkeys))
        {
            while(list($key, $value) = each($this->page->tkeys))
            {
                $keyid = "TPL_" . strtoupper($key);
                $pagevars["$keyid"] = $value;
            }
            $defaults = array_merge($defaults, $pagevars);
        }
        $defaults["TPL_VALIDATION_MESSAGES"]=$this->get_formatted_validation_messages();
        $defaults["TPL_STATUS_MESSAGES"]=$this->get_formatted_status_messages();

		krsort($defaults);

        if (isset($this->template)) {
          $this->template->assign($defaults);
          $this->template->parse("CONTENT","page");
          $this->html = $this->template->fetch("CONTENT");
        }
		
		$this->track_method_exit("Page","build_page");
        return 1; 
    }

    function get_html()
    {
        if (isset($this->html)) {
          return $this->html;
        }
    }

    function force_new_template($template_name)
    {
        // forces a new template if you need to do it in mid-stream
        if (!file_exists(TEMPLATE_PATH . "/pages/" . $template_name))
        {	
			$tr = new Translate ;
			$tr->create_translate_table('page');
            $this->error = $tr->trans('template_error');
        } else{
            $this->template_file = $template_name;
            $this->template = new FastTemplate(TEMPLATE_PATH . "/pages");         
        }
    }
    
    function clear_validation_messages(){
    	$GLOBALS['validation_messages']=array();
    }
    
    function add_validation_message($message){
    	if (!isset($GLOBALS['validation_messages']) ){
    			$GLOBALS['validation_messages']=array();
    	}
    	array_push($GLOBALS['validation_messages'], $message);
    }
    
    function add_status_message($message){
    	if (!isset($GLOBALS['status_messages']) ){
    			$GLOBALS['status_messages']=array();
    	}
    	array_push($GLOBALS['status_messages'], $message);
    }
    
    function get_formatted_validation_messages(){
    	$tr = new Translate;
       	if (!isset($GLOBALS['validation_messages'])){
    		return "";
    	}
    	$formatted_messages="";
    	
    	foreach($GLOBALS['validation_messages'] as $next_message){
    		$formatted_messages.="<li>".$next_message."</li>";
    	}
    	
    	if (strlen($formatted_messages)>0){
    	    $html = "<p class=\"error\"><strong>";
        	$html .= $tr->trans('error_detected') . "</strong><br />";
        	$html .= $tr->trans('fill_out_form_right') . ":<br />";
        	$formatted_messages=$html."<ul>".$formatted_messages."</ul>";
    	}
    	return $formatted_messages;
    }
   	
   	function get_formatted_status_messages(){
       	if (!isset($GLOBALS['status_messages']))
    		return "";
    	$formatted_messages="";
    	
    	foreach($GLOBALS['status_messages'] as $next_message){
    		$formatted_messages.="<br/><strong>".$next_message."</strong>";
    	}
    	return $formatted_messages."<br/><br />";
    }
    


   


	  function array_merge_without_renumbering($array1, $array2){
	  		$temp_array=$array1;
	  		foreach ($temp_array as $key=>$value){
	  			$array2[$key]=$value;
	  		}
	  		return $array2;
	  
	  }
	      
    function redirect($url, $time_delay=0){
    	 $this->tkeys['redirect_url']=$url;
    	 $this->tkeys['delay_time']=$time_delay;
    	 $this->forced_template_file = 'misc/redirect.tpl';
    
    }
    
    
} // end class
