<?php

// This class sets up translation and internationalization

class Translate {

    function Translate ()
    {
        // Constructor creates common language table
        $global_filename = INDYBAY_BASE_PATH . '/dictionary/' . $GLOBALS['lang'] . '/common.dict';

        if (file_exists($global_filename))
        {
            include($global_filename);
        } else
        {
            return 0;
        }

        return 1;
    }

    function trans ($key)
    {
        if (array_key_exists($key, $GLOBALS['dict'])) return $GLOBALS['dict'][$key];
    }

    function create_translate_table ($keyword)
    {
    	$lang = isset($GLOBALS['lang']) ? $GLOBALS['lang'] : 'en';
        if (strlen($keyword) > 0)
        {
            $template_filename  = INDYBAY_BASE_PATH . '/dictionary/' .$lang. "/".$keyword.".dict";

            // adding check for local dictionary giving precedence over global dictionary
			global $local;
            if (file_exists($template_filename))
            {
            
                include($template_filename);
            } 

            if ($local!="" && is_array($local))
            {
                $GLOBALS['dict'] = array_merge($GLOBALS['dict'], $local);
            }

        } else
        {
            return 0;
        }
        return 1;
    }

}

?>
