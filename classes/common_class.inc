<?php
//--------------------------------------------
//Indybay Common Class
//Written January 2005
//
//Modification Log:
//12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com  
//initial development
//--------------------------------------------


//this class is responsible for common functions used by any class
//in the system
class Common 
{

function track_method_entry($class_name, $method_name, $key1="", $value1="",
											$key2="", $value2=""){
		//check security and exit if not allowed
		if (!array_key_exists("profile_string",$GLOBALS))
			$GLOBALS["class_method_dict"]=array();

		//in future will only be run if profiling is on
		$str="\nEntry\t$class_name\t$method_name";
		$str.="\t$key1";	
                if (is_object($value1)) {
                  $value1="";
                  @$value1 = spl_object_hash($value1);
                }
		$str .= "\t$value1";	
		$str.="\t$key2";	
		$str.="\t$value2\n";
		if (!array_key_exists("profile_string",$GLOBALS))
			$GLOBALS["profile_string"]="";
		$GLOBALS["profile_string"].=$str;	
		$GLOBALS["class_method_dict"][$class_name.".".$method_name]=time();
}


function track_method_exit($class_name, $method_name, $key1="", $value1="",
											$key2="", $value2=""){
		//in future will only be run if profiling is on
		
		$str="<tr><td>Exit</td><td>$class_name</td><td>$method_name</td>";
			$str="\nExit\t$class_name\t$method_name";
			$str.="\t$key1";	
			$str.="\t$value1";	
			$str.="\t$key2";	
			$str.="\t$value2\n";	
			
		if (isset($GLOBALS["class_method_dict"][$class_name.".".$method_name])){
			$entry_time=$GLOBALS["class_method_dict"][$class_name.".".$method_name];
			$timechange=time()-$entry_time;
			if ($timechange>0){
				$str.="(ExitTime-EntryTime=".$timechange.")\n************\n";
			}
		}else{
			$str.="(entry wasn't trackeked)\n*************\n";
		}
		$GLOBALS["profile_string"].=$str;	
}

}
