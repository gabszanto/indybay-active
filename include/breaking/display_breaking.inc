<?php
$page = new Page('breaking_index');
if ($page->get_error()) {
    echo "Fatal error: " . $page->get_error();
} else {
    $page->build_page();
    echo $page->get_html();
}
?>
