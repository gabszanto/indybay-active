<!-- publishing instructions -->

<p><b>EVENT PUBLISHING POLICY:</b> Please use this form to contribute 
events to the Indybay calendar. Events should be political in nature, 
such as social and environmental justice events, and conform with 
Indybay's <a href="/newsitems/2003/12/08/16643971.php">principles of 
unity</a>. It is preferable if events are free, "no one turned away for 
lack of funds," or at most require a small fee or donation. If there is 
a charge for admission, the price should be stated in the text of the 
post. Do not UPPERCASE the event title. Calendar submissions that do not 
meet these criteria may be hidden.</p>

<p><b>EVENT EDITORIAL PROCESS:</b> After events have been published, 
they can be edited, linked or hidden by the collective running this 
site. Once we notice an event, we can highlight it. As the event 
approaches, highlighted events will appear as a red link at the top of 
the pages associated with the region and topic of the event.</p>

<!-- /publishing instructions -->
