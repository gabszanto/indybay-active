<!-- publishing instructions -->

<div><big><strong>Publish to Indybay</strong></big></div>

<p><b>If this is an event announcement, please instead use our <a 
href="/calendar/event_add.php?page_id=TPL_LOCAL_PAGE_ID">add event</a> form for the <a 
href="/calendar/?page_id=TPL_LOCAL_PAGE_ID">calendar</a>.</b></p>

<script type="text/javascript">
  $(function() {
    $(".clicktip .target").hide();
    $(".fasttip .target").hide();
    $(".clicktip .trigger").click(function() { 
      $(".clicktip .target").toggle("slow");
      $(".clicktip .trigger").toggle("slow");
    });
    $(".fasttip .trigger").click(function() { 
      $(".fasttip .target").toggle("fast");
      $(".fasttip .trigger").toggle("fast");
    });
  });
</script>

<div class="clicktip">

<strong class="trigger">Never published before? Read the publishing instructions!</strong>

<div class="target">

<p>Want to show off your photos of an event? Got audio or video that people just
have to see? This is the place to put it. We want to hear your story. Use any format you want, from journalistic, to academic, to
personal account.</p>

<p><span class="hed"><b>WHAT TO PUBLISH:</b></span> Please use this form to contribute new stories and ideas. We think comments
belong with the story being discussed. So to have your say in response to a story on the site, please use the &quot;add your
comments&quot; link at the bottom of each story. Instead of reposting corporate media articles to the site, try posting your own
critique or summary, short quotes, and links.</p>

<p><strong><span class="hed">MULTIMEDIA:</span></strong> If your image width or height exceeds 640 pixels, it 
will be resized to this maximum.  We suggest that you crop and resize your images yourself to obtain the 
results you're looking for; a variety of free (libre) open-source software is available for this purpose.  
If you need help converting your sounds or video to a compressed digital file, consult our <a 
href="/news/2003/12/1665907.php">tutorial</a> page.  We serve RealMedia via HTTP so you should encode your clip as 
single-rate, not SureStream.  QuickTime should be encoded as fast start, compressed header.</p>

<p><strong><span class="hed">EDITORIAL POLICY:</span></strong> After
stories have been published, they can be edited, linked or hidden by the
collective running this site. We have a lot of respect for the people
publishing on our site, so we generally only use these features to fix
obvious mistakes, such as typos in a web address or a duplicate copy of a
story. Very rarely, someone abuses our trust by posting a story that is
way outside what the website is set up to do, and we may remove it. 
Please read our full <b><a href="/news/2002/08/139500.php">editorial 
policy</a></b>.</p>

<p><span class="hed"><b>LEGALESE:</b></span> Unless otherwise stated, all content contributed to this site is free for
non-commercial reuse, reprint or rebroadcast. If you want to specify different conditions, please do so in the summary, including any
&copy; copyright (or copyleft) statement. Please read our <a href="/news/2003/12/1665906.php">privacy</a>, <a href="/newsitems/2014/10/05/18762449.php">copyright policy</a>, and <a
href="/news/2003/12/1665905.php">disclaimer</a> statements before continuing.</p>

</div></div>

<br clear="all" />
<!-- /publishing instructions -->
