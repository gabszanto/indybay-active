test <?php
include INCLUDE_PATH."/common/headers_and_links.cfg";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<?php
if ($GLOBALS['browser_type']!='pda'){
?>
<head>
<!-- test page -->
<title><?php 
	if (array_key_exists('page_title', $GLOBALS)) { 
	echo $GLOBALS['page_title'] . " : ".$GLOBALS['site_nick']; } 
	else { echo $GLOBALS['site_name']; } 
?></title>

<?php readfile(INCLUDE_PATH . '/common/javascript.html'); ?>
<?php include(INCLUDE_PATH .'/common/javascript.inc'); ?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="geo.position" content="37.765;-122.4183" /><meta name="ICBM" content="37.765,-122.4183" />

<?php 
if (
	array_key_exists('page_display',$GLOBALS)
 	&& (($GLOBALS['page_display']=='f') || ($_GET['display']=='f'))) { 
?>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOARCHIVE" />
<?php } 
//the following lines should be mixed into code that determines banner
$use_sc_theme=0;
if (in_array('60',$GLOBALS['page_ids'])){
	$use_sc_theme=1;
}

if (
		in_array('49',$GLOBALS['page_ids'])
		||
		in_array('51',$GLOBALS['page_ids'])
		||
		in_array('53',$GLOBALS['page_ids'])
		||
		in_array('50',$GLOBALS['page_ids'])
		||
		in_array('32',$GLOBALS['page_ids'])
		||
		in_array('41',$GLOBALS['page_ids'])
		||
		in_array('35',$GLOBALS['page_ids'])
		||
		in_array('40',$GLOBALS['page_ids'])
		||
		in_array('38',$GLOBALS['page_ids'])
		||
		in_array('38',$GLOBALS['page_ids'])
		||
		in_array('37',$GLOBALS['page_ids'])
		||
		in_array('36',$GLOBALS['page_ids'])
		||
		in_array('39',$GLOBALS['page_ids'])
	) {
	$use_sc_theme=0;
}

if ($use_sc_theme==1)
{
?>
<link rel="stylesheet" type="text/css" href="/themes/theme08santacruz.css" title="indybay blues" />
<?php
}else{
?>
<link rel="stylesheet" type="text/css" href="/themes/theme08blu.css" title="indybay blues" />
<?php
} 
if (array_key_exists('rssautodiscover',$GLOBALS ) && $GLOBALS['rssautodiscover']) { ?><link rel="alternate" type="application/rss+xml" title="RSS" href="<?php
echo $GLOBALS['rssautodiscover']; ?>" />
<?php } 
?>

<link rel="alternate" type="application/rss+xml" title="Indybay Newswire RSS 1.0" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155&rss_version=1" />
<link rel="alternate" type="application/rss+xml" title="Indybay Newswire RSS 2.0" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Features" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?include_blurbs=1&include_posts=0" />
<link rel="alternate" type="application/rss+xml" title="Indybay Videoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=4&news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Photoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=2&news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Podcast" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=3&news_item_status_restriction=1155" />

</head>



<body>


<div class="mast" style="background-color: black; background-image: url(<?php
echo $GLOBALS['header_bg_image'];
?>); background-repeat: no-repeat;"><table cellpadding="0" cellspacing="0" width="100%" border="0">

<td class="mastleft" align="left">



<!-- DONATION LAYER START -->
<style type="text/css">
#donatebanner { position: absolute; z-index:10; width: 420px; height: 36px; margin: 2px 0 0 400px; padding: 1px 0 2px 5px; background-color:#fff; border: 1px solid #000; 
			font-family:arial, sans-serif; font-size:12px; line-height:13px; font-weight:normal; color:#000; }
#donatebanner big { font-size:13px; line-height:13px; font-weight:bold; }
#donatebanner small { font-size:10px; line-height:9px; font-weight:normal; }
#donatebanner a {  color:#ff0000; }
#donatebanner  a.black {  color:#000000; }
#donatebanner #anigif {  margin: 1px 18px 0 10px; float:left; }
#donatebanner #donategraph {  margin: 4px 10px 0 15px; float:right; }
#donatebanner #donategraph .indybay-donation {  margin: 0; width: 60px; font-size:9px; text-align: center;  }
#donatebanner #donategraph img {  border: 1px solid #000;  }
</style>

<div id="donatebanner">
<div id="anigif"><a href="/newsitems/2008/02/29/18482685.php"><img src="/uploads/admin_uploads/2008/03/03/indymedia_ani-sm.gif" /></a></div>
<div id="donategraph"><div class="indybay-donation"></div> <a href="/newsitems/2008/02/29/18482685.php">
<img src="/images/donate.bar.png" height="10" width="60"></a></div>
<big>Indybay Needs Your Help: <a href="/donate/">Donate Now</a>!</big><br />
We <a href="/newsitems/2008/02/29/18482685.php" class="black">need to raise $5000</a> to continue operating.<br />
<small>All donations go directly to operating costs.</small>
</div>
<!-- DONATION LAYER END -->



<a 
href="<?php
	echo $GLOBALS['header_link'];
?>">

<img src="/im/bump.gif" width="350" height="45" border="0" alt="SF Bay Area Indymedia" /></a></td>
<td class="mastright" align="right"><a title="Indybay front page" href="/"><img src="/im/banner8-logo.gif" 
width="96" height="45" alt="indymedia" border="0" /></a></td></tr></table></div>

<?php
if (in_array('60',$GLOBALS['page_ids'])) {

include "about-sc.inc";

}else{
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr>
<th class="navbarleft"><a class="navbar" href="/news/2003/12/1664397.php" title="About the IMC">About</a></th>
<th class="navbar"><a class="navbar" href="/news/2003/12/1665899.php" title="Talk to us">Contact</a></th>
<th class="navbar"><a class="navbar" href="/subscribe/" title="News services">Subscribe</a></th>
<th class="navbar"><a class="navbar" href="/calendar/?page_id=<?php
$current_url=$_SERVER['PHP_SELF']; 
$page_id="";
if (strpos($current_url,"news")<1){
	$page_id=$GLOBALS["page_ids"][0];
	echo $page_id;
}
?>" title="Event listings">Calendar</a></th>
<th class="navbar"><a class="navbar" href="/publish.php<?php
if (isset($GLOBALS["page_ids"][0])){
	echo "?page_id=".$GLOBALS["page_ids"][0];
}
?>" title="Publish your news">Publish</a></th>
<th class="navbar"><a class="navbar" href="/faultlines/" title="Fault Lines newspaper">Print</a></th>
<!-- <th class="navbar"><a class="navbar" rel="friend" href="http://www.enemycombatantradio.net/" title="Enemy Combatant Radio">Radio</a></th>
<th class="navbarright"><a class="navbar" rel="friend" href="http://www.streetleveltv.org/" title="Street Level TV">TV</a></th> -->
<th class="navbar"><a class="navbar" href="/newsitems/2007/01/03/18343756.php" title="Help support indpendent media">Donate</a></th>

</tr></table>
<?php
}
?>
<table border="0" cellpadding="0" cellspacing="0"><tr valign="top">
<td class="bgleft" >
<?php include(INCLUDE_PATH . '/common/index-left.inc'); ?>
</td>
<td class="page" width="100%"><div class="pagecontent">

<?php

}else{
	$page_id = isset($GLOBALS['page_ids'][0]) ? $GLOBALS['page_ids'][0] : 12;
}
