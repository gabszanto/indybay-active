<?php 

include_once(INDYBAY_BASE_PATH . '/classes/date_class.inc');

$date = new Date; $date->set_time_zone(); $fdate = $date->get_formatted_date(); 
$ftime = $date->get_formatted_time(); 
$username = $_SESSION['session_username'];
 
if (array_key_exists("time_diff", $GLOBALS) && $GLOBALS['time_diff'] > 0) {
	 $tz = "+" . $GLOBALS['time_diff']; 
}else if (array_key_exists("time_diff", $GLOBALS)){ 
	$tz = $GLOBALS['time_diff'];
}else{
	$tz = 0;
}
 
if ($GLOBALS['browser_type']!='pda'){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html><head><title>Indybay Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(INCLUDE_PATH . '/common/javascript.inc'); ?>
<link rel="stylesheet" type="text/css" href="/themes/theme0.css" />
</head><body bgcolor="#ffffff" text="#000000" style="margin: 6px">
<small>
<A href="/admin/article/?include_posts=1&include_events=1&include_comments=1&news_item_status_restriction=0&news_item_status_restriction=874&pda=true">View Mobile Version Of Admin</A>
</small>

<table width="100%" background="/im/banner8blu-home.jpg" cellspacing=0 celpadding=0>
<tr height="45"><td>&nbsp;</td>
</tr>
</table>
<!-- main table for page --><div style="margin-bottom: 6px; padding: 3px; border: 1px solid; background-color: #ffcc00"><b>
<?php
} else {
?>
  <head>
    <title><?php echo Renderer::page_title(); ?></title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
  </head>
  <style>
body{margin:0;font-family:Helvetica;overflow-x:hidden;-webkit-user-select:none;-webkit-text-size-adjust:none}
body > .fullScreen{z-index:1001;top:0 !important;min-height:419px;max-height:419px;overflow:hidden}
body > .fullPage{top:34px;min-height:417px}
body > .formPage{z-index:3;top:64px;border-top:1px solid #6d84b4}
body[orient="landscape"] .fullScreen{min-height:270px;max-height:270px}
</style>

<?php
}
echo $sftr->trans('adminsite'); 

?>

 : <a class="bar" href="/admin/logout.php"><?php echo $sftr->trans('adminlogout'); ?></a>
 : <a class="bar" target="_blank" href="http://www.indybay.org/"><?php echo $sftr->trans('adminviewsite'); ?></a>
 
 
 
<?php
if ($GLOBALS['browser_type']=='pda'){
?>

:
<A href="/admin/index.php?pda=false">View Standard Admin</A>

<?php
}
?>
 
 </b></div>



<?php 
if ($GLOBALS['browser_type']!='pda'){
?>
<div style="float: left; background-color: #e0e0e0; width: 15%; border: 1px solid">

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/feature_page/feature_page_list.php"><?php echo $sftr->trans('adminpages'); ?></a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/article/?include_posts=1&amp;include_events=1&amp;include_comments=1&amp;news_item_status_restriction=0&amp;news_item_status_restriction=874"><?php echo $sftr->trans('adminunclassified'); ?></a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/article/?include_posts=1&amp;include_events=1&amp;news_item_status_restriction=0"><?php echo $sftr->trans('adminallarticles'); ?></a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/media/upload_list.php">MANAGE UPLOADS</a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/user/user_list.php"><?php echo $sftr->trans('adminusers'); ?></a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/email/admin_email.php"><?php echo $sftr->trans('adminemail'); ?></a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/stats/">SITE STATS</a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/article/?news_item_status_restriction=0&amp;parent_item_id=0&amp;submitted_search=Go">BREAKING NEWS ITEMS</a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/design/index.php"><?php echo $sftr->trans('admingraphics'); ?></a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/logs/?method=DB">LOGS</a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/logs/spam_identification.php">SPAM IDENTIFICATION</a></div>

<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/status/">SERVER STATUS</a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/paypal/">PAYPAL DONATIONS</a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/feed_pull/inbound_feed_list.php">INBOUND FEEDS</a></div>
<div style="border-bottom: 1px solid; padding: 3px"><a class="nav" href="/admin/cities/">REFRESH CITIES LIST</a></div>

<div style="border-bottom: 1px solid; padding: 3px"><small style="font-size: xx-small"> <?php 
echo $sftr->trans('adminloggedin'); 
?>: <span style="color: #990000"><?php
 echo $username;
  ?></span></small></div>
<div style="border-bottom: 1px solid; padding: 3px"><small style="font-size: xx-small"><?php 
echo $fdate;
 ?><br /><?php 
 echo $ftime; 
 ?></small></div>
<div style="padding: 3px"><a class="nav" href="/admin/logout.php">Logout</a></div>
</div>
<?php
}
?>

<div style="float: left; padding-left: 6px; width: 83%">
