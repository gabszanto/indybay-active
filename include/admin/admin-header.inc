<?php




		
$sftr = new Translate();

$error_num = 0;

if (in_array("HTTPS", $_SERVER) && $_SERVER["HTTPS"]=="" && strpos(" ".ADMIN_ROOT_URL, "https")>0){
	header("Location: ".ADMIN_ROOT_URL.$_SERVER['REQUEST_URI']);
	exit();
}

if ( !isset($_SESSION['session_last_activity_time'])
		|| (time()-$_SESSION['session_last_activity_time'])>SESSION_TIMEOUT_SECONDS){
	$_SESSION['session_is_editor']=false;
}

if ($_SESSION['session_is_editor'] == true)
{

	require_once(CLASS_PATH."/db/user_db_class.inc");

   if (isset($display)) {
	   include(INCLUDE_PATH.'/admin/admin-header-html.inc'); 
	   $user_db_class= new UserDB;
	   $user_db_class->update_last_activity_date();
	   $_SESSION["session_last_activity_time"]=time();
	
?>

<script language="JavaScript" type="text/javascript">
<!--

function forceReload() {
    location.href = location.href + '?' + (new Date()).getTime();
    var lastTime = location.search.substring(1) - 0;
    if ((new Date()).getTime() - lastTime > 1000) {
        forceReload();
    }
}
//-->
</script>

<?php


    }
} else {
        $goto=urlencode($_SERVER['REQUEST_URI']);
	header("Location: ".ADMIN_ROOT_URL."admin/authentication/authenticate_display_logon.php?goto=$goto");
	exit();
}

?>
