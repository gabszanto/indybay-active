
<?php
require_once(CLASS_PATH."/cache/feature_page_cache_class.inc");

if (array_key_exists('preview',$_GET) && strlen($_GET['preview']) > 0)
{
    	include_once(CLASS_PATH . '/renderer/feature_page_renderer_class.inc');
    	$feature_obj= new FeaturePageRenderer;
		if (array_key_exists('page_id',$_GET) && $_GET["page_id"]!=""){
			$page_id=$_GET["page_id"]+0;
		}
    	$feature_page_db_class= new FeaturePageDB;
		$feature_page_renderer_class= new FeaturePageRenderer;
        $page_info=$feature_page_db_class->get_feature_page_info($page_id);
        $blurb_list = $feature_page_db_class->get_current_blurb_list($page_id);
		$html= $feature_page_renderer_class->render_page($blurb_list,$page_info );
		if (strpos($html,"INDYBAY_HIGHLIGHTED_EVENTS")>0){
				$event_list_cache_class= new EventListCache();
				$cached_events=$event_list_cache_class->load_cached_highlighted_events_cache_for_page($page_id);
				$html=str_replace("INDYBAY_HIGHLIGHTED_EVENTS",$cached_events, $html);
		}
		echo $html;
} else{
		$page_id=$GLOBALS["page_id"];
		$feature_page_cache_class= new FeaturePageCache();
		$html=$feature_page_cache_class->load_cache_for_feature_page($page_id);
		echo $html;
}
?><div class="archivelink"><a href="/syn/generate_rss.php?page_id=<?php echo $GLOBALS['page_id']; ?>&amp;include_posts=0&amp;include_blurbs=1">
<img src="/images/feed-icon-16x16.png" width="16" 
height="16" alt="feed" title="feed" align="left" border="0" /></a><a 
href="/archives/archived_blurb_list.php?page_id=<?php 
echo $GLOBALS['page_id']; ?>">older stories &gt;&gt;</a></div>
