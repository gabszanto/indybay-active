<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/flowplayer/flowplayer.min.js"></script>
<script type="text/javascript" src="/js/flowplayer/flowplayer.embed.min.js"></script>
<?php if (isset($GLOBALS['jquery'])): ?>
  <?php foreach ((array)$GLOBALS['jquery'] as $plugin): ?>
    <script type="text/javascript" src="/js/jquery.<?php echo $plugin; ?>.min.js"></script>
  <?php endforeach; ?>
<?php endif; ?>
<?php if (isset($GLOBALS['ui'])): ?>
  <script type="text/javascript" src="/js/jquery.ui/jquery-ui.min.js"></script>
  <style type="text/css" media="all">@import "/js/jquery.ui/jquery-ui.min.css";</style>
  <style type="text/css" media="all">@import "/js/jquery.ui/jquery-ui.structure.min.css";</style>
  <style type="text/css" media="all">@import "/js/jquery.ui/jquery-ui.theme.min.css";</style>
<?php endif; ?>
<?php if (isset($GLOBALS['js'])): ?>
  <?php foreach ((array)$GLOBALS['js'] as $js): ?>
    <script type="text/javascript" src="/js/<?php echo $js; ?>.js"></script>
  <?php endforeach; ?>
<?php endif; ?>
