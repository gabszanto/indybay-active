&copy; 2000&#8211;<?php echo date("Y"); ?> <?php print $GLOBALS['site_name']; ?>. 
Unless otherwise stated by the author, all content is free for
non-commercial reuse, reprint, and rebroadcast, on the net and
elsewhere. Opinions are those of the contributors and are not
necessarily endorsed by the SF Bay Area IMC.  
<a href="/news/2003/12/1665905.php">Disclaimer</a> | 
<a href="/newsitems/2014/10/05/18762449.php">Copyright Policy</a> | 
<a href="/news/2003/12/1665906.php">Privacy</a> | 
<a href="/news/2003/12/1665899.php">Contact</a>
