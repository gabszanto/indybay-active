<?php
if ($GLOBALS['browser_type']!='pda'){
?>
<div class="bgleftcol">
<?php
if ($GLOBALS['is_front_page']!=1){
?>
<div class="leftheadertop"><strong>
<a class="leftmf" href="/news/2005/12/1789457.php" title="Regional Pages Pages">Regions</a></strong>
</div>

<div  class="<?php if (in_array('41',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/northcoast/" title="North Coast">north coast</a></div>

<div  class="<?php if (in_array('35',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/centralvalley/" title="Central Valley">central valley</a></div>

<div  class="<?php if (in_array('40',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/northbay/" title="North Bay">north bay</a></div>

<div  class="<?php if (in_array('38',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/eastbay/" title="East Bay">east bay</a></div>

<div  class="<?php if (in_array('37',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/southbay/" title="South Bay">south bay</a></div>

<div  class="<?php if (in_array('36',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/sf/" title="San Francisco">san francisco</a></div>

<div  class="<?php if (in_array('39',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/peninsula/" title="Peninsula">peninsula</a></div>

<div  class="<?php if (in_array('60',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/santacruz/" title="Santa Cruz">santa cruz</a></div>

<div  class="<?php if (in_array('42',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/california/" title="California">california</a></div>

<div  class="<?php if (in_array('43',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/us/" title="U.S.">us</a></div>

<div  class="<?php if (in_array('44',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/" title="International">international</a></div>


<div class="leftheader"><strong>

<?php
}else{
?>
<div class="leftheadertop"><strong>
<?php
}
?>

<a class="leftmf" href="/#features" title="Topic Pages">Topics</a></strong>
</div>

<div  class="<?php if (in_array('58',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/animalliberation/" title="Animal Liberation News">animal lib</a></div>

<div  class="<?php if (in_array('18',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/antiwar/" title="Anti-War and Militarism News">anti-war</a></div>

<div  class="<?php if (in_array('34',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/arts/" title="Arts + Action News">arts + action</a></div>

<div  class="<?php if (in_array('27',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/drugwar/" title="Drug War News">drug war</a></div>

<div  class="<?php if (in_array('30',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/education/" title="Education &amp; Student Activism News">education</a></div>

<div  class="<?php if (in_array('33',$GLOBALS['page_ids'])) {
$tag=true; ?>thisfeaturelistitem<?php } else { $tag=false;
?>featurelistitem<?php } ?>"><a class="leftf" <?php if ($tag) {
?>rel="tag"<?php } ?> href="/espanol/" title="Noticias en Español">en español</a></div>

<div  class="<?php if (in_array('14',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/environment/" title="Environment &amp; Forest Defense News">environment</a></div>

<div  class="<?php if (in_array('22',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/globalization/" title="Global Justice &amp; Anti-Capitalism News">global justice</a></div>

<div  class="<?php if (in_array('45',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/government/" title="Government &amp; Elections News">government</a></div>

<div  class="<?php if (in_array('16',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/poverty/" title="Health, Housing, &amp; Public Services News">health/housing</a></div>

<div  class="<?php if (in_array('56',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/immigrant/" title="Immigrant Rights News">immigrant</a></div>

<div  class="<?php if (in_array('32',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/indymedia/" title="Media Activism &amp; Independent Media News">media</a></div>

<div  class="<?php if (in_array('19',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/labor/" title="Labor &amp; Workers News">labor</a></div>

<div  class="<?php if (in_array('29',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/lgbtqi/" title="LGBTI / Queer News">lgbti / queer</a></div>

<div  class="<?php if (in_array('13',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/police/" title="Police State &amp; Prisons News">police state</a></div>

<div  class="<?php if (in_array('15',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/race/" title="Racial Justice News">racial justice</a></div>

<div  class="<?php if (in_array('31',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/womyn/" title="Womyn's News">womyn</a></div>



<?php
if ($GLOBALS['is_front_page']!=1){
?>

<div class="leftheader"><strong>
<a class="leftmf" href="/international/" title="International Pages">International</a></strong>
</div>

<div  class="<?php if (in_array('53',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/americas/" title="Americas">americas</a></div>

<div  class="<?php if (in_array('50',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/haiti/" title="Haiti">haiti</a></div>

<div  class="<?php if (in_array('48',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/iraq/" title="Iraq">iraq</a></div>

<div  class="<?php if (in_array('49',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/palestine/" title="Palestine">palestine</a></div>

<div  class="<?php if (in_array('51',$GLOBALS['page_ids'])) { $tag=true;
?>thisfeaturelistitem<?php } else { $tag=false; ?>featurelistitem<?php } ?>"><a 
class="leftf" <?php if ($tag) { ?>rel="tag"<?php } ?> href="/international/afghanistan/" title="Afghanistan">afghanistan</a></div>
<?php
}
?>
<div class="leftheader"><div class="restofleft">More
</div></div>

<div class="restofleft">

<div style="margin-top:3px;" class="left"><small><a class="left" href="/news/2003/12/1665902.php" title="make media &amp; publish your own news"><b>make media</b></a></small></div>
<div  class="left"><small><a class="left" href="/news/2003/12/1665901.php" title="get involved with Indybay"><b>get involved</b></a></small></div>
<div  class="left"><small><a class="left" href="/calendar/" title="Indybay calendar"><b>calendar</b></a></small></div>
<div class="left"><small><a class="left" href="/gallery/" title="photo gallery"><b>gallery</b></a></small></div>
<div class="left"><small><a class="left" href="/archives/archived_blurb_list.php?page_id=12" title="center-column feature archives"><b>archives</b></a></small></div>

<div class="left"><small><a class="left" href="http://chat.indymedia.org/" title="irc chat"><b>chat</b></a></small></div>
<div class="left"><small><a class="left" href="/news/2003/12/1665900.php" title="links to other sites"><b>links</b></a></small></div>

<?php if (0): ?>
<div id="stop-sopa" style="display: none; font-size: 13px"> 
<center><img src="/uploads/admin_uploads/2011/12/18/stop-sopa-200.png" width="200" height="197" alt="Stop SOPA" /></center>
<br />
If <b><a 
href="https://www.eff.org/issues/coica-internet-censorship-and-copyright-bill">SOPA/PIPA</a></b> Internet blacklist legislation 
passes, the government would have the power to shut down Indybay.org.
<br /> <br />This act would allow Indybay's domain name to be hijacked, 
forcing Internet providers to stop serving Indybay's pages, and could 
allow for deep packet inspection to determine who posts to the site, 
taking away community member's anonymity.<br /> <br /> <b>EFF suggests 
how you can <a href="/newsitems/2011/12/18/18703033.php">help stop 
SOPA/PIPA</a>.</b></div>
<?php endif; ?>

</div>


<div class="left" style="margin-bottom: 6px;"><center>
<form style="margin: 9px 0 6px 0" action="/search/search_results.php" method="get">
<input name="search" type="text" size="10" />
<input type="submit" value="Search" />
</form></center>
</div>

<?php
if (array_key_exists('60',$GLOBALS["page_ids"])){
?>
<div class="leftheader"><a href="http://www.freakradio.org" class="leftmf">Freak Radio</a></div>
<div class="restofleft" style="margin-top: 3px; margin-bottom: 3px;"><small><a class="left" href="http://www.freakradio.org/listen.html">FRSC live stream</a></small><br />
<center style="margin-top: 3px; margin-bottom: 3px;"><a href="http://www.freakradio.org/listen.html"><img 
src="/images/meter.gif" alt="radio" width="65" height="30" border="0" /></a></center></div>
<?php
}else{
?>
<?php
}
?>

<div class="leftheader"><a href="/newsitems/2003/12/15/16659041.php" class="leftmf">Donate</a></div>
<div class="restofleft" style="margin-top: 3px; margin-bottom: 3px;"><small><a class="left" href="/newsitems/2003/12/15/16659041.php">Help support grassroots independent media.</a></small></div>
<center><a href="/newsitems/2003/12/15/16659041.php"><img src="/images/paypaldonate.gif" alt="donate" border="0" /></a></center>
<div class="restofleft" style="margin-top: 3px; margin-bottom: 3px;"><small>$<?php include(CACHE_PATH . '/paypal.txt'); ?> donated in past month</small></div>

<div class="leftheader"><a id="indy-link" href="http://www.indymedia.org" class="leftmf">IMC Network</a></div>
<div id="cities-list" class="restofleft" style="margin-top: 3px; margin-bottom: 3px;"></div>

</div>
<?php
}
