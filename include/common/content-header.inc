<?php
include INCLUDE_PATH."/common/headers_and_links.cfg";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php
if ($GLOBALS['browser_type']!='pda'){

?>
<head>

<title><?php echo Renderer::page_title(); ?></title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="geo.position" content="37.765;-122.4183" /><meta name="ICBM" content="37.765,-122.4183" />


<?php if (isset($GLOBALS['page_summary'])): ?>

    <meta name="description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'], ENT_QUOTES, 'UTF-8')); ?>" />
    <meta itemprop="name" content="<?php echo Renderer::page_title(); ?>">
    <meta itemprop="description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'], ENT_QUOTES, 'UTF-8')); ?>">
    <?php if (isset($GLOBALS['image_url']) && preg_match('/.*mp4$/', $GLOBALS['image_url'])): ?>
      <meta name="og:video" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="og:video:secure_url" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="og:video:type" content="video/mp4" />
      <meta name="og:video:width" content="<?php echo $GLOBALS['image_width']; ?>" />
      <meta name="og:video:height" content="<?php echo $GLOBALS['image_height']; ?>" />
      <meta name="twitter:card" content="player" />
      <meta name="twitter:player:stream" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="twitter:player:stream:content_type" content="video/mp4" />
      <meta name="twitter:player" content="https://www.indybay.org/video.php/<?php echo $GLOBALS["news_item_id"]; ?>" />
      <meta name="twitter:player:width" content="<?php echo $GLOBALS['image_width']; ?>" />
      <meta name="twitter:player:height" content="<?php echo $GLOBALS['image_height']; ?>" />
      <?php if (!empty($GLOBALS['poster_url'])): ?>
        <meta name="twitter:image" content="<?php echo $GLOBALS['poster_url']; ?>" />
        <meta name="og:image" content="<?php echo $GLOBALS['poster_url']; ?>" />
      <?php endif; ?>
    <?php elseif (isset($GLOBALS['image_url']) && preg_match('/.*(mp3|ogg)$/', $GLOBALS['image_url'])): ?>
      <meta name="og:audio" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="og:audio:secure_url" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="og:audio:type" content="<?php echo $GLOBALS['mime_type']; ?>" />
      <meta name="twitter:card" content="player" />
      <meta name="twitter:player:stream" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="twitter:player:stream:content_type" content="audio/mpeg" />
      <meta name="twitter:player" content="https://www.indybay.org/audio.php/<?php echo $GLOBALS["news_item_id"]; ?>" />
      <meta name="twitter:player:width" content="300" />
      <meta name="twitter:player:height" content="28" />
    <?php elseif (isset($GLOBALS['image_url']) && preg_match('/.*(jpg|jpeg|gif|png)$/', $GLOBALS['image_url'])): ?>
      <meta itemprop="image" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta name="twitter:image:src" content="<?php echo $GLOBALS['image_url']; ?>" />
      <meta property="og:image" content="<?php echo $GLOBALS['image_url']; ?>?4321" />
      <?php if (!empty($GLOBALS['image_width'])): ?>
        <meta name="twitter:image:width" content="<?php echo $GLOBALS['image_width']; ?>" />
        <meta property="og:image:width" content="<?php echo $GLOBALS['image_width']; ?>" />
      <?php endif; ?>
      <?php if (!empty($GLOBALS['image_height'])): ?>
        <meta name="twitter:image:height" content="<?php echo $GLOBALS['image_height']; ?>" />
        <meta property="og:image:height" content="<?php echo $GLOBALS['image_height']; ?>" />
      <?php endif; ?>
      <meta name="twitter:card" content="summary_large_image">
    <?php else: ?>
      <meta name="twitter:card" content="summary">
    <?php endif; ?>
    <meta name="twitter:site" content="@indybay">
    <meta name="twitter:title" content="<?php echo Renderer::page_title(); ?>">
    <meta name="twitter:description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'], ENT_QUOTES, 'UTF-8')); ?>">
    <meta property="og:title" content="<?php echo Renderer::page_title(); ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?php echo Renderer::htmlPageUrl(); ?>" />
    <meta property="og:description" content="<?php echo trim(htmlspecialchars($GLOBALS['page_summary'], ENT_QUOTES, 'UTF-8')); ?>" />
    <meta property="og:site_name" content="Indybay" />
    <meta property="article:publisher" content="https://www.facebook.com/indybay.org" />

	<link rel="canonical" href="<?php echo Renderer::htmlPageUrl(); ?>" />
  
<?php else : ?>
 
	<meta name="description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network." />

    <meta itemprop="name" content="<?php echo Renderer::page_title(); ?>">
    <meta itemprop="description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network.">
    <meta itemprop="image" content="https://www.indybay.org/uploads/admin_uploads/2014/11/07/im_LOGO-and-MakeTrouble.jpg?4321">
    <meta itemprop="author" content="Indybay" />
    
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@indybay">
    <meta name="twitter:title" content="<?php echo Renderer::page_title(); ?>">
    <meta name="twitter:description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network.">
    <meta name="twitter:image:src" content="https://www.indybay.org/uploads/admin_uploads/2014/11/07/im_LOGO-and-MakeTrouble.jpg?4321">
    <meta name="twitter:image:width" content="200">
    <meta name="twitter:image:height" content="150">
    
    <meta property="og:title" content="<?php echo Renderer::page_title(); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo Renderer::htmlPageUrl(); ?>" />
    <meta property="og:image" content="https://www.indybay.org/uploads/admin_uploads/2014/11/07/im_LOGO-and-MakeTrouble.jpg?4321" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="150" />
    <meta property="og:description" content="The SF Bay Area Independent Media Center is a non-commercial, democratic collective serving as the local organizing unit of the global IMC network." />
    <meta property="og:site_name" content="Indybay" />
    <meta property="article:author" content="Indybay" />
    <meta property="article:publisher" content="https://www.facebook.com/indybay.org" />

	<link rel="canonical" href="<?php echo Renderer::htmlPageUrl(); ?>" />
  
<?php endif; ?>


<?php if ((isset($GLOBALS['page_display']) && $GLOBALS['page_display'] == 'f') || (isset($_GET['news_item_status_restriction']) && $_GET['news_item_status_restriction'] == 323)): ?>
  <meta name="ROBOTS" content="NOINDEX, NOFOLLOW, NOARCHIVE" />
<?php endif; ?>

<link rel="stylesheet" type="text/css" href="/themes/theme08blu.css?ff" title="indybay blues" /><?php
//the following lines should be mixed into code that determines banner
$use_sc_theme=0;
if (in_array('60',$GLOBALS['page_ids'])){
	$use_sc_theme=1;
}

if (
		in_array('49',$GLOBALS['page_ids'])
		||
		in_array('51',$GLOBALS['page_ids'])
		||
		in_array('53',$GLOBALS['page_ids'])
		||
		in_array('50',$GLOBALS['page_ids'])
		||
		in_array('32',$GLOBALS['page_ids'])
		||
		in_array('41',$GLOBALS['page_ids'])
		||
		in_array('35',$GLOBALS['page_ids'])
		||
		in_array('40',$GLOBALS['page_ids'])
		||
		in_array('38',$GLOBALS['page_ids'])
		||
		in_array('38',$GLOBALS['page_ids'])
		||
		in_array('37',$GLOBALS['page_ids'])
		||
		in_array('36',$GLOBALS['page_ids'])
		||
		in_array('39',$GLOBALS['page_ids'])
		||
		in_array('42',$GLOBALS['page_ids'])
	) {
	$use_sc_theme=0;
}

if ($use_sc_theme==1) {
  ?><link rel="stylesheet" type="text/css" href="/themes/theme08santacruz.css" title="indybay blues" /><?php
}
if (array_key_exists('rssautodiscover',$GLOBALS ) && $GLOBALS['rssautodiscover']) { ?><link rel="alternate" type="application/rss+xml" title="RSS" href="<?php
echo $GLOBALS['rssautodiscover']; ?>" />
<?php } 
?>

<link rel="alternate" type="application/rss+xml" title="Indybay Newswire" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Features" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?include_blurbs=1&amp;include_posts=0" />
<link rel="alternate" type="application/rss+xml" title="Indybay Podcast" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=3&amp;news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Videoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=4&amp;news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Photoblog" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?media_type_grouping_id=2&amp;news_item_status_restriction=1155" />
<link rel="alternate" type="application/rss+xml" title="Indybay Newswire RSS 1.0" href="<?php echo FULL_ROOT_URL?>syn/generate_rss.php?news_item_status_restriction=1155&amp;rss_version=1" />
<?php include(INCLUDE_PATH .'/common/javascript.inc'); ?>
</head>

<body<?php if (!empty($GLOBALS['body_class'])):?> class="<?php echo $GLOBALS['body_class'];?>"<?php endif; ?>>
<!--
<small>
<A href="/mobile/">View Mobile Version Of Site</A>
</small>
-->
<div class="mast" style="background-color: black; background-image: url(<?php
echo $GLOBALS['header_bg_image'];
?>); background-repeat: no-repeat;"><table cellpadding="0" cellspacing="0" width="100%" border="0">

<tr><td class="mastleft" align="left">

<a 
href="<?php
	echo $GLOBALS['header_link'];
?>">

<img src="/im/bump.gif" width="350" height="45" border="0" alt="SF Bay Area Indymedia" /></a></td>
<td class="mastright" align="right"><a title="Indybay front page" href="/"><img src="/im/banner8-logo.gif" 
width="96" height="45" alt="indymedia" border="0" /></a></td></tr></table></div>

<?php
if ($use_sc_theme) {

include "about-sc.inc";

}else{
?>

<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr>
<th class="navbarleft"><a class="navbar" href="/news/2003/12/1664397.php" title="About the IMC">About</a></th>
<th class="navbar"><a class="navbar" href="/news/2003/12/1665899.php" title="Talk to us">Contact</a></th>
<th class="navbar"><a class="navbar" href="/subscribe/" title="News services">Subscribe</a></th>
<th class="navbar"><a class="navbar" href="/calendar/?page_id=<?php
$current_url = $_SERVER['SCRIPT_NAME']; 
$page_id="";
if (strpos($current_url,"news")<1 && count($GLOBALS['page_ids']) == 1){
	$page_id=$GLOBALS["page_ids"][0];
	echo $page_id;
}
?>" title="Event listings">Calendar</a></th>
<th class="navbar"><a class="navbar" href="/publish.php<?php
if (isset($GLOBALS["page_ids"][0]) && count($GLOBALS['page_ids']) == 1){
	echo "?page_id=".$GLOBALS["page_ids"][0];
}
?>" title="Publish your news">Publish</a></th>
<th class="navbar"><a class="navbar" href="/faultlines/" title="Fault Lines newspaper">Print</a></th>
<th class="navbar"><a class="navbar" href="/donate" title="Help support indpendent media">Donate</a></th>

</tr></table>
<?php
}
?>
<table border="0" cellpadding="0" cellspacing="0"><tr valign="top">
<td class="bgleft" >
<?php include(INCLUDE_PATH . '/common/index-left.inc'); ?>
</td>
<td class="page" width="100%"><div class="pagecontent">

<?php
} else {
?>
  <head>
    <title><?php echo Renderer::page_title(); ?></title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
  </head>
  <style>
body{margin:0;font-family:Helvetica;overflow-x:hidden;-webkit-user-select:none;-webkit-text-size-adjust:none}
body > .fullScreen{z-index:1001;top:0 !important;min-height:419px;max-height:419px;overflow:hidden}
body > .fullPage{top:34px;min-height:417px}
body > .formPage{z-index:3;top:64px;border-top:1px solid #6d84b4}
body[orient="landscape"] .fullScreen{min-height:270px;max-height:270px}
h3 { font-size: 9px; font-weight: bold; }

</style>
  <body>
  <h2><a href="/mobile/">((i)) Indybay Mobile</a></h2>
  
<?php
  $page_id = isset($GLOBALS['page_ids'][0]) ? $GLOBALS['page_ids'][0] : 12;
}
