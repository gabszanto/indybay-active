<?php
if ($GLOBALS['browser_type']!='pda'){
?>

<br clear="all" />
<div class="archivelink2"><span class="headlines1"><a 
href="/syn/generate_rss.php?include_blurbs=1&amp;include_posts=0"><img src="/images/feed-icon-16x16.png" 
width="16" height="16" alt="feed" title="feed" align="right" 
border="0" /></a><a name="features">Latest headlines from all sections of the site:</a></span></div>

<?php
require_once(CLASS_PATH."/db/feature_page_db_class.inc");
require_once(CLASS_PATH."/db/blurb_db_class.inc");
require_once(CLASS_PATH."/cache/article_cache_class.inc");
$feature_page_db_class= new FeaturePageDB();
$blurb_db_class= new BlurbDB();
$article_cache_class= new ArticleCache();
$results=$feature_page_db_class->get_recent_blurb_version_ids();
$i=0;
$previous_titles=array();
foreach ($results as $version_id){
	$blurb_info=$blurb_db_class->get_blurb_info_from_version_id($version_id);
	
	$title2=$blurb_info["title2"];
	$title1=$blurb_info["title1"];
	if (trim($title1)=="" || trim($title2)=="")
		continue;
	if (!isset($previous_titles[$title2])){
		$pages=$feature_page_db_class->get_pages_with_pushed_long_versions_of_blurb($blurb_info["news_item_id"]);
		if (sizeof($pages)!=0 && !(sizeof($pages)==1 && $pages[0]["page_id"]==FRONT_PAGE_CATEGORY_ID)){
		
			$create_time_formatted=strftime("%D",$blurb_info["creation_timestamp"]);
			
			$page_link="";
			$pi=0;
			$page_link="";
			foreach ($pages as $page_info){
				$pi=$pi+1;
				if ($pi>1)
					$page_link.=" | ";
				$page_link.="<a href=\"/".$page_info["relative_path"]."\">";
				$page_link .= Renderer::check_plain($page_info['long_display_name']);
				$page_link.="</a>";
			}
			
			$previous_titles[$title2]=1;
			$searchlink=$article_cache_class->get_web_cache_path_for_news_item_given_date($blurb_info["news_item_id"], $blurb_info["creation_timestamp"]);
			$row=$create_time_formatted;
			$row.=" <a href=\"".$searchlink;
			$row.="\">";
			$row .= Renderer::check_plain($blurb_info['title2']);
			$row.="</a> &nbsp;&nbsp;<strong>";
			$row.=$page_link;
			$row.="</strong><br />";
			echo $row;
			$i=$i+1;
			if ($i>10)
				break;
		}
	}
}
}
