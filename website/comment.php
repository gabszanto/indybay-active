<?php
//This file is used for displaying, adding and confirming
//the adding of commnets to posts (and other comments)

include_once("config/indybay.cfg");

$GLOBALS['page_title']='Add Comment';
$GLOBALS['page_display']='f';
$GLOBALS['jquery'][] = 'validate';
$GLOBALS['js'] = array('publish','fileupload');

include(INCLUDE_PATH.'/common/content-header.inc');

$page = new Page('comment', "article");

if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();
    echo $page->get_html();
}

include(INCLUDE_PATH.'/common/footer.inc');

?>
