<?php
setcookie("pda", "true", time() + 3600,"/");
include_once("config/indybay.cfg");
header('Last-Modified: ' . gmdate(DATE_RFC1123, getlastmod()));
?>
<html>
<head><title>Indybay</title>
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
</head>
<style>
body{margin:0;font-family:Helvetica;overflow-x:hidden;-webkit-user-select:none;-webkit-text-size-adjust:none}
body > .fullScreen{z-index:1001;top:0 !important;min-height:419px;max-height:419px;overflow:hidden}
body > .fullPage{top:34px;min-height:417px}
body > .formPage{z-index:3;top:64px;border-top:1px solid #6d84b4}
body[orient="landscape"] .fullScreen{min-height:270px;max-height:270px}

</style>
<body>
<center>
<h2>((i)) Indybay Mobile </h2>
<hr />
<h4>
<a accesskey="1" href="/search/pda_search.php?include_blurbs=1">1. Featured Articles</a>
<br /><br />
<a accesskey="2" href="/search/pda_search.php?include_posts=1&amp;news_item_status_restriction=3">2. Latest Local News</a>
<br /><br />
<a accesskey="3" href="/search/pda_search.php?include_posts=1&amp;news_item_status_restriction=5">3. US/World News</a>
<br /><br />
<a accesskey="4" href="/search/pda_search.php?news_item_status_restriction=1155&amp;include_events=1&amp;search_date_type=displayed_date">4. Upcoming Events</a>
<br /><br />
<a accesskey="5" href="/search/pda_search.php?news_item_status_restriction=690690&amp;include_events=1&amp;include_posts=1">5. Unfiltered/Breaking News</a>
<br /><br />
<a accesskey="6" href="/publish.php">6. Publish</a>
<br /><br />
<a accesskey="7" href="/donate">7. Donate</a>
<br /><br />
<a href="/index.php?pda=false">View Full Site</a>
</h4>

<small>
Some phones do not allow uploading images or video via the web browser.
If you wish to upload via such phones you can use several applications:
<br />

<a href="http://itunes.apple.com/us/app/indybay-publisher/id350053274?mt=8">iPhone Indybay Publisher (via iTunes)</a><br />

<a href="https://dev.indybay.org/websvn/filedetails.php?repname=indybay&path=%2Fphoneapps%2Fandroid%2FIndybayAndroidPublisher%2FIndyPublish.apk">
Android Indybay Publisher Beta (link to apk file)</a><br />

More applications for other phones are being developed. To help develop or test email 
<br />
<!-- <a href="mailto:sfbay-tech@lists.indymedia.org">sfbay-tech@lists.indymedia.org</a> -->
<a href="mailto:indybay@lists.riseup.net">indybay@lists.riseup.net</a>

</ul>
</small>
</center>
<br /><br />
<hr />
<br />&copy; 2000-<?php print date('Y'); ?> SF Bay Area Independent Media Center.
Unless otherwise stated by the author, all content is free for non-commercial reuse, reprint, and rebroadcast, on the net and elsewhere. Opinions are those of the contributors and are not necessarily endorsed by the SF Bay Area IMC. 
</body>
</html>
