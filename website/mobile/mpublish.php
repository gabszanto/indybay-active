<?php
// This is the page that handles content publishing from users

include_once('config/indybay.cfg');

//include(INCLUDE_PATH .'/common/content-header.inc');

$page = new Page('mpublish', 'article');

if ($page->get_error()) {
  echo 'Fatal error: '. $page->get_error();
}
else {
  $page->build_page();
  echo $page->get_html();
}

//include(INCLUDE_PATH .'/common/footer.inc');
