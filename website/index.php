<?php
//Index.php is the main page on the indymedia site
//It displays a cached center column and a cached summary list
//of the latest posts.

include_once("config/indybay.cfg");

if (array_key_exists('browser_type',$GLOBALS) && $GLOBALS['browser_type']=='pda')
{
	header("HTTP/1.0 301 Moved Permanently");
	header("Location: ".FULL_ROOT_URL."mobile/index.php");
	exit;
}

$sftr = new Translate();
$bottom = "cities";
$left   = INCLUDE_PATH . "/left.inc";
$GLOBALS['is_front_page']=1;
$GLOBALS['body_class'] = 'front';
$GLOBALS['page_ids']=array(12);

include_once(INCLUDE_PATH.'/common/content-header.inc');

include_once(INCLUDE_PATH.'/common/index_center.inc');

include_once(INCLUDE_PATH.'/feature_page/feature_list.inc');

include_once(INCLUDE_PATH.'/common/center_right.inc'); 

include_once(INCLUDE_PATH.'/common/footer.inc');
?>
