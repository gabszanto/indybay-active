// $Id$

function readLinked() { 
    $("#linkedDates").val($("#displayed_date_month").val() + "/" + 
        $("#displayed_date_day").val() + "/" + $("#displayed_date_year").val()); 
    return {}; 
} 
 
// Update three select controls to match a date picker selection 
function updateLinked(date) { 
        $("#displayed_date_month").val(parseInt(date.substring(0, 2), 10));
        $("#displayed_date_day").val(parseInt(date.substring(3, 5), 10));
        $("#displayed_date_year").val(date.substring(6, 10));
} 
 
// Prevent selection of invalid dates through the select controls 
function checkLinkedDays() { 
    var daysInMonth = 32 - new Date($("#displayed_date_year").val(), 
        $("#displayed_date_month").val() - 1, 32).getDate(); 
    $("#displayed_date_day option").removeAttr('disabled');
    $("#displayed_date_day option:gt(" + (daysInMonth - 1) +")").attr("disabled", "disabled"); 
    if ($("#displayed_date_day").val() > daysInMonth) { 
        $("#displayed_date_day").val(daysInMonth); 
    } 
} 

$(function() {
  $("#linkedDates").datepicker({ 
    minDate: new Date(),
    maxDate: new Date(new Date().getFullYear() + 1, 12 - 1, 31), 
    beforeShow: readLinked, 
    onSelect: updateLinked, 
    showOn: "both", 
    buttonImage: "/im/calendar.gif", 
    buttonImageOnly: true 
  }); 
  $("#displayed_date_month, #displayed_date_year").change(checkLinkedDays);
});
