$(function(){
 $("#files_select_template").hide();
  $("#upload_submit_button").hide();
  $("#nonjscript_file_boxes").hide();
  $("#upload_warning").hide();
  if ($('input[name=captcha_verbal]').attr('type') != 'hidden') {
    $('input[name=publish]').attr('disabled', 'disabled');
    $('#file_count').attr('disabled', 'disabled');
    $('#file_count').after(' <strong>You may attach files after completing the CAPTCHA questions and previewing your post.</strong>');
  }
  $("#file_count").change(
	  	function(){ var numSelected=parseInt($("#file_count option:selected").val()); 
	  				$("nonjscript_file_boxes").hide();
					
					if (numSelected>0){
						$("#preview_button").hide();
					}else{
						$("#preview_button").show();
					}
					if (numSelected>1){
						var filebox=$("#files_select_template").clone();
					}
					for (var i=1;i<=20;i++){
						if (i<=numSelected){
							
							if ($("#files_select_"+i).length == 0){
								var fileboxstr="";
			  					if (i==1){var k=fileboxstr.indexOf("<!--endoffirstupload-->");
									fileboxstr=$("#files_select_template_1").html();
			  					}else{
			  						fileboxstr=filebox.html().replace(/::uploadnum::/g,i);
			  					}
			  					fileboxstr="<div id='files_select_"+i+"'>"+fileboxstr+"</div>";
			  					$("#file_boxes2").append(fileboxstr);
							}
						}else{
							if ($("#files_select_"+i).length > 0){
								$("#files_select_"+i).remove();						
							}
						}
					}
	  			}
	  			  
	  	);
  var numSelected=$("#file_count option:selected").val(); 
  if (numSelected>0){ $("#file_count").trigger("change"); }
});
