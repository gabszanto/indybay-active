
$(function() {
  $("#publish-form").validate({
    onkeyup: false,
    onfocusout: false,
    wrapper: "div",
    rules: {
      title1: {
        minlength: 5,
        required: true
      },
      displayed_author_name: {
        required: true
      },
      captcha_math: {
        required: true
      },
      captcha_verbal: {
        required: true
      },
      summary: {
        maxlength: 65535,
        minlength: 3,
        required: true
      },
      text: {
        maxlength: 65535,
        required: function() {
	  return $("#file_count").val() < 1;
	}
      }
    }
  });
  
});

