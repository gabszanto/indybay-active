<?php
// Paging newswire & search
include_once("config/indybay.cfg");
$page = new Page('search_results', "article");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
}
else
{
    $page->build_page();
    $GLOBALS['body_class'] = 'search-results';
    include(INCLUDE_PATH.'/common/content-header.inc');
    echo $page->get_html();
    include(INCLUDE_PATH.'/common/footer.inc');
}
?>
