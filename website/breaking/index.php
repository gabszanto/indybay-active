<?php
include_once("config/indybay.cfg");

$page = new Page('breaking_index', "breaking");
$page->build_page();

include (INCLUDE_PATH.'/common/content-header.inc');

if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    echo $page->get_html();
}
include (INCLUDE_PATH.'/common/center_right.inc');
include (INCLUDE_PATH.'/common/footer.inc');
?>
