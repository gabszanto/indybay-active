<?php

// This page shows a list of calendars for the archives
include_once("config/indybay.cfg");
$page = new Page('archived_blurb_list', "feature_page");
if ($page->get_error()) {
  echo "Fatal error: " . $page->get_error();
}
else {
  $page->build_page();
  $GLOBALS['body_class'] = 'feature-archive';
  include(INCLUDE_PATH."/common/content-header.inc");
  echo $page->get_html();
  include(INCLUDE_PATH."/common/footer.inc");
}
