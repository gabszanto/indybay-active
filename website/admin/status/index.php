<?php
//This page displays the main list of admin options

$display=true;
include_once("config/indybay.cfg");

$page = new Page('status');

if ($page->get_error()) {
    echo "Fatal error: " . $page->get_error();
} else {
	include(INCLUDE_PATH."/admin/admin-header.inc");
    $page->build_page();
    echo $page->get_html();
	include(INCLUDE_PATH."/admin/admin-footer.inc"); 
}

?>
