<?php
// displays a list of features for a given category

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$display=true;
include_once("config/indybay.cfg");
include_once(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('article_list',"admin/article");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();	
    echo $page->get_html('article_display_list');
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
