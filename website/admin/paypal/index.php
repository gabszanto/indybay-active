<?php
//This page displays the main list of admin options
define('DB_DATABASE',   'drupal');
$display=true;
include_once("config/indybay.cfg");
include(INCLUDE_PATH."/admin/admin-header.inc");
require_once(CLASS_PATH."/db/db_class.inc");
?>
<a href="/admin/">Admin</a> : Paypal | <small><b><a href="labels.php">Print donor mailing labels</a></b></small>
<br />
<table class="paypal">
<tr class="paypal">
<th class="paypal">id</th>
<th class="paypal">timestamp</th>
<th class="paypal">email</th>
<th class="paypal">type</th>
<th class="paypal">currency</th>
<th class="paypal">gross</th>
<th class="paypal">fee</th>
<th class="paypal">status</th>
<th class="paypal">payment date</th>
<th class="paypal">item</th>
<th class="paypal">memo</th>
<th class="paypal">subscr date</th>
<th class="paypal">subscr amount</th>
<th class="paypal">subscr period</th>
<th class="paypal">name</th>
<th class="paypal">street</th>
<th class="paypal">city</th>
<th class="paypal">state</th>
<th class="paypal">zip</th>
<th class="paypal">country</th>
</tr>
<?php
$query = 'select * from lm_paypal_ipns order by id desc';
$db_obj = new DB;
$result = $db_obj->query($query);
foreach ($result as $row) echo '
<tr class="paypal">
<td class="paypal">' . $row['id'] . '</td>
<td class="paypal">' . date(r, $row['timestamp']) . '</td>
<td class="paypal">' . htmlspecialchars($row['payer_email']) . '</td>
<td class="paypal">' . htmlspecialchars($row['txn_type']) . '</td>
<td class="paypal">' . htmlspecialchars($row['mc_currency']) . '</td>
<td class="paypal">' . htmlspecialchars($row['payment_gross']) . '</td>
<td class="paypal">' . htmlspecialchars($row['payment_fee']) . '</td>
<td class="paypal">' . htmlspecialchars($row['payment_status']) . '</td>
<td class="paypal">' . htmlspecialchars($row['payment_date']) . '</td>
<td class="paypal">' . htmlspecialchars($row['item_name']) . '</td>
<td class="paypal">' . htmlspecialchars($row['memo']) . '</td>
<td class="paypal">' . htmlspecialchars($row['subscr_date']) . '</td>
<td class="paypal">' . htmlspecialchars($row['amount3']) . '</td>
<td class="paypal">' . htmlspecialchars($row['period3']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_name']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_street']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_city']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_state']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_zip']) . '</td>
<td class="paypal">' . htmlspecialchars($row['address_country_code']) . '</td>
</tr>';
?></table><?php
include(INCLUDE_PATH."/admin/admin-footer.inc");
?>
