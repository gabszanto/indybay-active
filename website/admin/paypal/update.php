<?php
define('BAR_WIDTH', 128);
define('DONATION_GOAL', 2010);
define('DONATION_START', '2010-05-01 00:00:00');
define('DB_DATABASE', 'drupal');
include_once('config/indybay.cfg');
require_once(CLASS_PATH . '/db/db_class.inc');
$query1 = 'SELECT SUM(payment_gross) AS amount FROM 
  lm_paypal_ipns WHERE DATE_SUB(now(), INTERVAL 30 DAY) 
  <= FROM_UNIXTIME(timestamp) AND payment_status = "Completed"';
$query2 = 'SELECT SUM(payment_gross) AS amount FROM lm_paypal_ipns WHERE payment_status = "Completed"';
$query3 = 'SELECT SUM(payment_gross) AS amount FROM lm_paypal_ipns WHERE payment_status = "Completed" AND timestamp > UNIX_TIMESTAMP("' . DONATION_START . '")';
$db_obj = new DB;
$result = $db_obj->query($query1);
$result2 = $db_obj->query($query2);
$result3 = $db_obj->query($query3);
foreach($result as $row) $amount = sprintf("%01.2f", $row['amount']);
foreach($result2 as $row) $amount2 = sprintf("%01.2f", $row['amount']);
foreach($result3 as $row) $amount3 = round($row['amount']);

if ($amount != file_get_contents(CACHE_PATH . '/paypal.txt')) {
  file_put_contents(CACHE_PATH . '/paypal.txt', $amount);
}
if ($amount2 != file_get_contents(CACHE_PATH . '/paypaltotal.txt')) {
  file_put_contents(CACHE_PATH . '/paypaltotal.txt', $amount2);
}
$contents3 = '$'. number_format($amount3);
if ($contents3 != file_get_contents(CACHE_PATH .'/paypal201005.txt')) {
  file_put_contents(CACHE_PATH . '/paypal201005.txt', $contents3);
}

require 'Image/Graph.php';
$Graph =& Image_Graph::factory('graph', array(300, 150));
$Graph->add(
  Image_Graph::vertical(
    Image_Graph::factory('title', array('', 0)),        
    Image_Graph::vertical(
      $Plotarea = Image_Graph::factory('plotarea',array('axis', 'axis')),
      $Legend = Image_Graph::factory('legend'), 100
    ), 0)
);
$AxisX =& $Plotarea->getAxis(IMAGE_GRAPH_AXIS_X); 
$AxisY =& $Plotarea->getAxis(IMAGE_GRAPH_AXIS_Y); 
$AxisX->forceMaximum(time());
$AxisX->setDataPreprocessor(Image_Graph::factory('Image_Graph_DataPreprocessor_Date', 'n-j'));
//$AxisX->setLabelInterval(500000);
//$AxisY->forceMaximum(4000); 

$Dataset =& Image_Graph::factory('dataset');

$query = 'SELECT payment_gross,timestamp FROM lm_paypal_ipns WHERE payment_status = "Completed" order by timestamp asc';
$result = $db_obj->query($query);
$total = 0;
foreach ($result as $row) {
  $total = $total + $row['payment_gross'];
  $Dataset->addPoint($row['timestamp'], $total);
}

$Plot =& $Plotarea->addNew('Image_Graph_Plot_Area', $Dataset);
$Plot->setLineColor('gray');
//    $Plot->setFillColor('blue@0.2');
$Plot->setFillStyle(Image_Graph::factory('gradient', array(IMAGE_GRAPH_GRAD_VERTICAL, 'red', 'green')));
$Grid =& $Plotarea->addNew('bar_grid', null, IMAGE_GRAPH_AXIS_Y);
$Grid->setFillColor('gray@0.2'); 
$Graph->done(array('filename' => WEB_PATH .'/images/donate.graph.png'));

$Canvas =& Image_Canvas::factory('png', array('width' => BAR_WIDTH, 'height' => 1));
$Canvas->setGradientFill(array('direction' => 'horizontal', 'start' => 'red', 'end' => 'black'));
$Canvas->rectangle(array('x0' => 0, 'y0' => 0, 'x1' => $amount3/(DONATION_GOAL/BAR_WIDTH), 'y1' => 0));
$Canvas->save(array('filename' => WEB_PATH .'/images/donate.bar.png'));
