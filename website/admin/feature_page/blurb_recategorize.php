<?php
//This page is for editing articles

$display=true;
include_once('config/indybay.cfg');
include(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('blurb_recategorize',"admin/feature_page");
if ($page->get_error()) 
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();	
    echo $page->get_html();
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
