<?php
//This file is used to add a feature to the DB (features are called Categories in the DB)
$display=false;
include_once('config/indybay.cfg');
include_once(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('feature_page_add', "admin/feature_page");
if ($page->get_error()) 
{
    echo "Fatal error: " . $page->get_error();
} else 
{
    $page->build_page();	
    echo $page->get_html();
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
