<?php
//This file displays an edit page for a category
//this only includes things like the name and description and default
//values for new stories

$display=true;
include_once("config/indybay.cfg");
include_once(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('feature_page_edit' ,"admin/feature_page");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();	
    echo $page->get_html();
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
