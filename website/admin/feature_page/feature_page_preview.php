<?php
//This page displays an exmaple page of what the feature will look liek when pushed live
//this page also includes the link to push the page live

$display=true;
include_once("config/indybay.cfg");
include(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('feature_page_preview', "admin/feature_page");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();	
    echo $page->get_html();
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
