<?php
// displays a list of form to edit or add a template, include or css

$display=true;
include_once("config/indybay.cfg");
include_once(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('logedit_index', "admin/misc");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();	
    echo $page->get_html('logedit_index');
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
