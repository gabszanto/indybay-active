<?php
//category_display_list.php displays a list of the features in the DB
//with drilldowns to lists of stories for each feature

$display=true;
include_once("config/indybay.cfg");
include_once(INCLUDE_PATH."/admin/admin-header.inc");
$page = new Page('inbound_feed_list', "admin/feed_pull");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();
    echo $page->get_html();
}
include(INCLUDE_PATH."/admin/admin-footer.inc");

?>
