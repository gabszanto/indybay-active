<?php
// This is the page that handles content publishing from users

include_once('config/indybay.cfg');

$GLOBALS['body_class'] = 'publish';
$GLOBALS['page_title'] = 'Publish';
$GLOBALS['jquery'][] = 'validate';
$GLOBALS['js']= array('publish','fileupload');

include(INCLUDE_PATH .'/common/content-header.inc');

$page = new Page('publish', 'article');

if ($page->get_error()) {
  echo 'Fatal error: '. $page->get_error();
}
else {
  $page->build_page();
  echo $page->get_html();
}

include(INCLUDE_PATH .'/common/footer.inc');
