<?php
//This file is used for displaying, adding and confirming
//the adding of commnets to posts (and other comments)

include_once("config/indybay.cfg");

$page = new Page('missing', "misc");

if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page();
    echo $page->get_html();
}


?>
