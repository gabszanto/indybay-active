<?php
// Paging newswire & search
include_once("config/indybay.cfg");
$GLOBALS['rssautodiscover']='http://www.indybay.org/syn/newswire.rss';
$page = new Page('newswire', "article");
if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
}
else
{
    $page->build_page();
    include(INCLUDE_PATH.'/common/content-header.inc');
    echo $page->get_html();
    include(INCLUDE_PATH.'/common/footer.inc');
}
?>
