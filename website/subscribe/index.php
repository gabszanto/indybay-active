<?php
//This page shows the mailing list page for sending
//out email to the indymedia weely email list

include_once("config/indybay.cfg");

$GLOBALS['page_title'] = 'News Services';

include(INCLUDE_PATH.'/common/content-header.inc');

$page = new Page('mailinglist', "email");

if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else {
    $page->build_page();
    echo $page->get_html();
}

include(INCLUDE_PATH.'/common/footer.inc');

?>
