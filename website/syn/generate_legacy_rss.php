<?php
// This is the page that handles content publishing from users

include_once('config/indybay.cfg');
include_once(CLASS_PATH . '/syndication/legacy_rss_generator.inc');

$legacy_rss_generator = new legacy_rss_generator();

if (isset($_GET['old_url'])) {
  $old_file = urldecode($_GET['old_url']);
  $i = strrpos($old_file, '/');
  $old_file = substr($old_file, $i + 1);
  header('Content-Type: application/rss+xml');
  echo $legacy_rss_generator->generate_rss($old_file);
}
else {
  header('Location: ' . SERVER_URL . '/syn/generate_rss.php?rss_version=' . intval($_GET['rss_version']), TRUE, 301);
}
