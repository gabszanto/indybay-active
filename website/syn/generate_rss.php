<?php
// This is the page that handles content publishing from users
header("Content-Type: application/rss+xml");

include_once("config/indybay.cfg");
include_once(CLASS_PATH."/pages/rss/rss_generator.inc");

$rss_generator= new rss_generator();

echo $rss_generator->generate($_GET);

?>