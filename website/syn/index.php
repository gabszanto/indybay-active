<?php
//This page gives an overview of the different syndication files (per category). It is served from cache to avoid dbase overload.

$display=true;
include_once("config/indybay.cfg");
include(INCLUDE_PATH.'/common/content-header.inc');

$page = new Page('syndication_index', "rss");

if ($page->get_error())
{
    echo "Fatal error: " . $page->get_error();
} else
{
    $page->build_page('syndication_index');
    echo $page->get_html();
}

    include(INCLUDE_PATH.'/common/footer.inc');

?>

