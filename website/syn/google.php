<?php
include("shared/global.cfg");

include('google_aggregator.inc');

$document='<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
        "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>SF Bay Area Indymedia</title>
</head>
<body>' . "\n";
if (is_array($sortedstories)) {
    foreach ($sortedstories as $li) {
        $document .= '<div><a href="' . 
        htmlspecialchars($li['link'],ENT_QUOTES,'UTF-8') . 
        '">' . 
        htmlspecialchars($li['title'],ENT_QUOTES,'UTF-8') .
       '</a></div>' . "\n";
    }
}
$document .= '</body></html>';
$filename = 'news.html';
if (is_writable($filename)) {
   if (!$handle = fopen($filename, 'w')) {
         echo "Cannot open file ($filename)";
         exit;
   }
   if (fwrite($handle, $document) === FALSE) {
       echo "Cannot write to file ($filename)";
       exit;
   }
   echo "Success, wrote ($document) to file ($filename)";
   fclose($handle);
} else { echo "The file $filename is not writable"; }
?>
