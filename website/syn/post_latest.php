<?php
include("shared/global.cfg");
include_once(CLASS_PATH.'/newswire_class.inc');

function get_latest_comments() {
    $db_obj = new DB;
    $query = "select display,id,numcomment,linked_file,heading,mime_type,created,modified,summary,heading,author,article 
    from webcast where parent_id=0 ORDER BY modified DESC limit 15";
    $resultset = $db_obj->query($query);
    return $resultset;
}

function make_latest_comments() {
    include_once(CLASS_PATH.'/rss10.inc');
    $db_obj = new DB;
    $resultset = get_latest_comments();
    $rows = sizeof($resultset);
    $xml_logo = $GLOBALS['xml_logo'];
    $rdf_file = WEB_PATH . '/syn/post_latest.rdf';
    $site_nick = $GLOBALS['site_nick'];
    $site_name = $GLOBALS['site_name'];
    $root_url = ROOT_URL.'/';
    $news_url = NEWS_URL.'/';
    $uploads_url = UPLOAD_URL.'/';
    $rss=new RSSWriter($root_url, $site_nick, $site_name, $rdf_url, array("dc:publisher" => $site_nick, "dc:creator" => $site_nick));
    $rss->useModule("content", "http://purl.org/rss/1.0/modules/content/");
    $rss->setImage($xml_logo, $site_nick);
    foreach(array_reverse($resultset) as $row) {
        $id = $row['id'];
        $post = '';
        if ($row[numcomment]>0) {
            $postquery = "select id,author,article from webcast where parent_id=$row[id] order by id desc limit 1";
            $post=$db_obj->query($postquery);
            $post=array_pop($post);
            $row['author']=$post['author'];
            $row['article']=$post['article'];
        }
        $imagelink = "";
        $topic = "";
        if (ereg("image",$row[mime_type])) {
            $imagebase = basename(trim($row['linked_file']));
            $imagelink = $uploads_url . $imagebase;
        }
        $created = $row['modified'];
        $date_array  = substr($created,0,4) . "-" . substr($created,4,2) . "-" . substr($created,6,2) . " ";
        $date_array .= substr($created,8,2) . ":" . substr($created,10,2) . ":" . substr($created,12,2);
        $rdf_date_array = gmdate('Y-m-d\TH:i:s\Z',strtotime($date_array));
        $pathtofile = MakeCacheDir($row['created']);
        $articlelink =  $news_url . $pathtofile . $row['id'];
        if ($post[id]) { $articlelink .= '_comment.php' . "#$post[id]"; }
        else { $articlelink .= '.php'; }
        $catquery = "select c.name as name from catlink l,category c where l.id=$id and c.category_id=l.catid";
        $catset = $db_obj->query($catquery);
        while ($catrow = array_pop($catset))
        {
            $topic .= utf8_encode(htmlspecialchars($catrow[name])) . "\t";
        }
        $topic=trim($topic);
        switch ($row['display'])
        {
            case 'f':
                $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_hidden']));
                break;
            case 'l':    
                $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_local']));
                break;
            case 't':
                $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_display']));
                break;
            case 'g':
                $section=utf8_encode(htmlspecialchars($GLOBALS['dict']['status_global']));
                break;
        }
        $heading = trim(utf8_encode(htmlspecialchars($row[heading])));
        $author= trim(utf8_encode(htmlspecialchars($row[author])));
        $title = utf8_encode(trim(htmlspecialchars($row['heading'], ENT_QUOTES)));
        $summary = utf8_encode(trim(htmlspecialchars($row['summary'], ENT_QUOTES)));
        $date = $rdf_date_array;
        $mime = utf8_encode(trim($row['mime_type']));
        $author = utf8_encode(trim(htmlspecialchars($row['author'], ENT_QUOTES)));
        $article = utf8_encode(str_replace(array('&lt;', '&gt;','&amp;', '&quot;'), array('<', '>','&', '"'), htmlentities($row['article'], ENT_QUOTES, cp1252)));
        $rss->addItem($articlelink, $title, array("description" => $summary, "dc:date" => $date, "dc:subject" => $topic, "dc:creator" => $author, "dc:format" => $mime, "content:encoded" => $article));
    }
    $rdf=$rss->serialize();
    $rdf=str_replace(array("&amp;#","&amp;amp;","&amp;gt;","&amp;lt;","&amp;quot;"),array("&#","&amp;","&gt;","&lt;","&quot;"),$rdf);
    $fffp = fopen($rdf_file, "w");
    fwrite($fffp, $rdf, strlen($rdf));
    fclose($fffp);
}

if ((time() - filemtime(WEB_PATH.'/syn/post_latest.rdf')) > 60) {
    make_latest_comments();
}

header('Location: post_latest.rdf');

?>
