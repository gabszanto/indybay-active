<?php
require_once("config/indybay.cfg");

require_once("syndication/rss_util_class.inc");
require_once("renderer/renderer_class.inc");
$rss_util_class= new RSSUtil();

$num_results=10;
$news_item_status_restriction=3;
$include_posts=1;
$include_events=0;
$include_blurbs=0;
$media_type_grouping_id=0;
$topic_id=0;
$region_id=0;

$include_full_text=$_GET[include_full_text]+0;
$rss_version=$_GET[rss_version]+0;

$result=$rss_util_class->make_news_item_rss_string($num_results, $news_item_status_restriction ,
        	$include_posts, $include_events, $include_blurbs,
        	$media_type_grouping_id,$topic_id,$region_id,$include_full_text, $rss_version);
$renderer= new Renderer();
        	
echo $renderer->cleanup_text($result);
