<?php
header('Content-type: application/javascript; charset: UTF-8');
include_once("config/indybay.cfg");
include(CLASS_PATH."/syndication/aggregator.inc");

$aggregator = new RSSAggregator();

$sorted_stories=$aggregator->renderForJScript();

if (is_array($sorted_stories)) {
    foreach ($sorted_stories as $li) {

        ?>document.write( '<li class="indybay_newsfeed"><a class="indybay_newsfeed" target="_blank" href="<?php echo $li['link']; ?>"><?php 
        echo str_replace(array('"',"'"),array('&quot;','&#039;'),$li['title']);
        ?></a></li>' );
<?php  }
}
