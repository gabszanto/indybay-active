<?php
include_once(CLASS_PATH . '/magpierss/rss_parse.inc');

if (!$rss_file_features) $rss_file_features = 'features.rdf';
$rss_string = file_get_contents(WEB_PATH.'/syn/'.$rss_file_features);
$rssfeatures = new MagpieRSS( $rss_string );

if ( $rssfeatures and !$rssfeatures->ERROR ) {

while ($thisstory=array_pop($rssfeatures->items)) { 
    
$allstories[]=array('title'=>$thisstory['title'],'link'=>$thisstory['link'],'date'=>$thisstory['dc']['date'],'format'=>$thisstory['dc']['format']);
}

function SortArray() {
    $arguments = func_get_args();
    $array = $arguments[0];
    $code = '';
    for ($c = 1; $c < count($arguments); $c += 2) {
        if (in_array($arguments[$c + 1], array("ASC", "DESC"))) {
            $code .= 'if ($a["'.$arguments[$c].'"] != $b["'.$arguments[$c].'"]) {';
            if ($arguments[$c + 1] == "ASC") {
                $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? -1 : 1); }';
            }
            else {
                $code .= 'return ($a["'.$arguments[$c].'"] < $b["'.$arguments[$c].'"] ? 1 : -1); }';
            }
        }
    }
    $code .= 'return 0;';
    $compare = create_function('$a,$b', $code);
    usort($array, $compare);
    return $array;
}

$sortedstories = SortArray($allstories, 'date', DESC);
$sortedstories = array_slice($sortedstories,0,10);

} else {
    echo "Error: " . $rss->ERROR;
}

?>
