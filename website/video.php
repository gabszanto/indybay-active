<?php
/**
 * @file
 * This file is used for displaying a video element.
 */

include_once("config/indybay.cfg");

$page = new Page('video', 'article');

if ($page->get_error()) {
  echo "Fatal error: " . $page->get_error();
}
else {
  $page->build_page();
  echo $page->get_html();
}
