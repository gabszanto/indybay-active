<?php
// $Id$

/**
 * @file
 * Generate an icalendar event feed.
 */

include_once('config/indybay.cfg');

$sftr = new Translate();
header('Content-Type: text/calendar');
$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : '0';
$region_id = isset($_GET['region_id']) ? intval($_GET['region_id']) : '0';
header('Content-Disposition: inline; filename="indybay_event_'. $topic_id .'_'. $region_id .'.ics"');

$page = new Page('ical_feed', 'calendar');

if ($page->get_error()) {
  echo 'Fatal error: '. $page->get_error();
}
else {
  $page->build_page();
  echo $page->get_html();
}
