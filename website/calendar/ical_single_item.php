<?php

// Add and Update a Event

include_once("config/indybay.cfg");

$sftr = new Translate();
header("Content-Type: text/calendar");
$news_item_id=$_GET["news_item_id"]+0;
header("Content-Disposition: inline; filename=\"indybay_event_".$news_item_id.".ics\"");

$page = new Page("ical_single_item", "calendar");

if ($page->get_error()) {
    echo "Fatal error: " . $page->get_error();
} else {
    $page->build_page();
    echo $page->get_html();
}



?>
