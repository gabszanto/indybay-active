<?php

// Add and Update a Event
include_once("config/indybay.cfg");
$sftr = new Translate();
$page = new Page("event_add", "calendar");
if ($page->get_error()) {
  echo "Fatal error: " . $page->get_error();
}
else {
  $GLOBALS['jquery'][] = 'validate';
  $GLOBALS['js'] = array('event_add','fileupload');
  $GLOBALS['ui']['datepicker'] = TRUE;
  $GLOBALS['body_class'] = 'event-add';
  include(INCLUDE_PATH."/common/content-header.inc");
  $page->build_page();
  echo $page->get_html();
  include(INCLUDE_PATH."/common/footer.inc"); 
}
