<?php

include_once("config/indybay.cfg");
$sftr = new Translate();
$Title = $sftr->trans('calendar');
$page = new Page('event_week', "calendar");
if ($page->get_error()) {
  echo "Fatal error: " . $page->get_error();
}
else {
  $page->build_page();
  $GLOBALS['body_class'] = 'event-week';
  include(INCLUDE_PATH."/common/content-header.inc");
  echo $page->get_html();
  include(INCLUDE_PATH."/common/footer.inc"); 
}
